using System;
using System.Reflection;
using System.Runtime.Serialization;

using BenchmarkDotNet.Attributes;

using FastEnums.Benchmark.Models;

using _FastEnum = FastEnums.FastEnum;



namespace FastEnums.Benchmark.Scenarios
{

    /// <summary>   An enum member attribute benchmark. </summary>
    /// <remarks>   David, 2021-02-20. <para>
    /// |   Method |          Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
    /// |--------- |--------------:|------:|------:|-------:|------:|------:|----------:| </para><para>
    /// |  NetCore | 1,462.1807 ns |    NA | 1.000 | 0.0305 |     - |     - |     264 B | </para><para>
    /// | EnumsNet |     7.0265 ns |    NA | 0.005 |      - |     - |     - |         - | </para><para>
    /// | FastEnum |     0.7346 ns |    NA | 0.001 |      - |     - |     - |         - | </para><para>
    /// </para> </remarks>
    public class EnumMemberAttributeBenchmark
    {
        private const Fruits _Value = Fruits.Apple;


        [GlobalSetup]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void Setup()
        {
            _ = Enum.GetNames( typeof( Fruits ) );
            _ = EnumsNET.Enums.GetValues<Fruits>();
            _ = _FastEnum.GetNames<Fruits>();
        }


        [Benchmark( Baseline = true )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public string? NetCore()
        {
            var type = typeof( Fruits );
            var name = Enum.GetName( type, _Value )!;
            var info = type.GetField( name );
            var attr = info!.GetCustomAttribute<EnumMemberAttribute>();
            return attr?.Value;
        }


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public string? EnumsNet()
        {
            var attr = EnumsNET.Enums.GetAttributes( _Value )!.Get<EnumMemberAttribute>();
            return attr?.Value;
        }


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public string? FastEnum()
            => _Value.GetEnumMemberValue();
    }
}
