using System;

using BenchmarkDotNet.Attributes;

namespace FastEnums.Benchmark.Scenarios
{

    /// <summary>   A string equals benchmark. </summary>
    /// <remarks>   David, 2021-02-20. <para>
    /// |                            Method |      Mean | Error | Ratio | Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
    /// |---------------------------------- |----------:|------:|------:|------:|------:|------:|----------:| </para><para>
    /// |          String_OrdinalIgnoreCase |  16.50 ns |    NA |  1.00 |     - |     - |     - |         - | </para><para>
    /// |            Span_OrdinalIgnoreCase |  16.79 ns |    NA |  1.02 |     - |     - |     - |         - | </para><para>
    /// | String_InvariantCultureIgnoreCase | 312.58 ns |    NA | 18.95 |     - |     - |     - |         - | </para><para>
    /// |   Span_InvariantCultureIgnoreCase | 322.10 ns |    NA | 19.52 |     - |     - |     - |         - | </para><para>
    /// </para></remarks>
    public class StringEqualsBenchmark
    {
#pragma warning disable CS8618
        private string Lower { get; set; }
        private string Upper { get; set; }
#pragma warning restore CS8618


        [GlobalSetup]
        public void Setup()
        {
            var text = Guid.NewGuid().ToString();
            this.Lower = text.ToLower();
            this.Upper = text.ToUpper();
        }


        [Benchmark( Baseline = true )]
        public bool String_OrdinalIgnoreCase()
            => this.Lower.Equals( this.Upper, StringComparison.OrdinalIgnoreCase );


        [Benchmark]
        public bool Span_OrdinalIgnoreCase()
        {
            var lower = this.Lower.AsSpan();
            var upper = this.Upper.AsSpan();
            return lower.Equals( upper, StringComparison.OrdinalIgnoreCase );
        }


        [Benchmark]
        public bool String_InvariantCultureIgnoreCase()
            => this.Lower.Equals( this.Upper, StringComparison.InvariantCultureIgnoreCase );


        [Benchmark]
        public bool Span_InvariantCultureIgnoreCase()
        {
            var lower = this.Lower.AsSpan();
            var upper = this.Upper.AsSpan();
            return lower.Equals( upper, StringComparison.InvariantCultureIgnoreCase );
        }
    }
}
