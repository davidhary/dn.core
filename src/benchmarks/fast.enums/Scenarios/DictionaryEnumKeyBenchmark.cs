using System.Collections.Generic;
using System.Linq;

using BenchmarkDotNet.Attributes;

using FastEnums.Benchmark.Models;
using FastEnums.Internals;



namespace FastEnums.Benchmark.Scenarios
{

    /// <summary>   A dictionary enum key benchmark. </summary>
    /// <remarks>   David, 2021-02-20. <para>
    /// |           Method |      Mean | Error | Ratio | Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
    /// |----------------- |----------:|------:|------:|------:|------:|------:|----------:| </para><para>
    /// |       Dictionary | 4.9081 ns |    NA |  1.00 |     - |     - |     - |         - | </para><para>
    /// | FrozenDictionary | 0.7296 ns |    NA |  0.15 |     - |     - |     - |         - | </para><para>
    /// </para> </remarks>
    public class DictionaryEnumKeyBenchmark
    {
        private const Fruits _LookupKey = Fruits.Pear;

#pragma warning disable CS8618
        private Dictionary<Fruits, Member<Fruits>> Standard { get; set; }
        private FrozenDictionary<Fruits, Member<Fruits>> Frozen { get; set; }
#pragma warning restore CS8618


        [GlobalSetup]
        public void Setup()
        {
            var members = FastEnum.GetMembers<Fruits>();
            this.Standard = members.ToDictionary( x => x.Value );
            this.Frozen = members.ToFrozenDictionary( x => x.Value );
        }


        [Benchmark( Baseline = true )]
        public bool Dictionary()
            => this.Standard.TryGetValue( _LookupKey, out _ );


        [Benchmark]
        public bool FrozenDictionary()
            => this.Frozen.TryGetValue( _LookupKey, out _ );
    }
}
