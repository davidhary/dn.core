using System;

using BenchmarkDotNet.Attributes;

using FastEnums.Benchmark.Models;

using _FastEnum = FastEnums.FastEnum;



namespace FastEnums.Benchmark.Scenarios
{

    /// <summary>   A try parse benchmark. </summary>
    /// <remarks>   David, 2021-02-19. <para>
    /// |       Method |     Mean | Error | Ratio | Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
    /// |------------- |---------:|------:|------:|------:|------:|------:|----------:| </para><para>
    /// |      NetCore | 77.01 ns |    NA |  1.00 |     - |     - |     - |         - | </para><para>
    /// |     EnumsNet | 31.77 ns |    NA |  0.41 |     - |     - |     - |         - | </para><para>
    /// |     FastEnum | 17.01 ns |    NA |  0.22 |     - |     - |     - |         - | </para><para>
    /// Legacy:  </para><para>
    /// | EnumExtender | 23.71 ns |    NA |  0.31 |     - |     - |     - |         - | </para><para>
    /// |  MelodyEnums | 51.76 ns |    NA |  0.67 |     - |     - |     - |         - | </para><para>
    /// </para></remarks>
    public class TryParseBenchmark
    {

        private const string _Value = nameof( Fruits.WaterMelon );

        [GlobalSetup]
        public void Setup()
        {
            _ = Enum.GetNames( typeof( Fruits ) );
            _ = EnumsNET.Enums.GetValues<Fruits>();
            _ = _FastEnum.GetValues<Fruits>();
        }


        [Benchmark( Baseline = true )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool NetCore()
            => Enum.TryParse<Fruits>( _Value, out _ );


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool EnumsNet()
            => EnumsNET.Enums.TryParse<Fruits>( _Value, out _ );


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool FastEnum()
            => _FastEnum.TryParse<Fruits>( _Value, out _ );

    }
}
