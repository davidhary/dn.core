using System;
using System.Collections.Generic;

using BenchmarkDotNet.Attributes;

using FastEnums.Benchmark.Models;

using _FastEnum = FastEnums.FastEnum;



namespace FastEnums.Benchmark.Scenarios
{
    /// <summary>   A get values benchmark. </summary>
    /// <remarks>   David, 2021-02-19. <para>
    /// |       Method |          Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
    /// |------------- |--------------:|------:|------:|-------:|------:|------:|----------:| </para><para>
    /// |      NetCore | 1,281.2128 ns |    NA | 1.000 | 0.0420 |     - |     - |     352 B | </para><para>
    /// |     EnumsNet |     2.1256 ns |    NA | 0.002 |      - |     - |     - |         - | </para><para>
    /// |     FastEnum |     0.0000 ns |    NA | 0.000 |      - |     - |     - |         - | </para><para>
    /// Legacy:  </para><para>
    /// | EnumExtender |     0.0000 ns |    NA | 0.000 |      - |     - |     - |         - | </para><para>
    /// |  MelodyEnums |     0.0815 ns |    NA | 0.000 |      - |     - |     - |         - | </para><para>
    /// </para></remarks>
    public class GetValuesBenchmark
    {

        [GlobalSetup]
        public void Setup()
        {
            _ = Enum.GetValues( typeof( Fruits ) );
            _ = EnumsNET.Enums.GetValues<Fruits>();
            _ = _FastEnum.GetValues<Fruits>();
        }

        [Benchmark( Baseline = true )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public IReadOnlyList<Fruits> NetCore()
            => ( IReadOnlyList<Fruits> ) Enum.GetValues( typeof( Fruits ) );


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public IReadOnlyList<Fruits> EnumsNet()
            => EnumsNET.Enums.GetValues<Fruits>();


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public IReadOnlyList<Fruits> FastEnum()
            => _FastEnum.GetValues<Fruits>();

    }
}
