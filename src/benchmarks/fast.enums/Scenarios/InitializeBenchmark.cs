using System;
using System.Linq;

using BenchmarkDotNet.Attributes;

using FastEnums.Benchmark.Models;
using FastEnums.Internals;



namespace FastEnums.Benchmark.Scenarios
{

    /// <summary>   An initialize benchmark. </summary>
    /// <remarks>   David, 2021-02-20. <para>
    /// |                            Method |         Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
    /// |---------------------------------- |-------------:|------:|------:|-------:|------:|------:|----------:| </para><para>
    /// |                 FastEnum_Init_All | 18,353.05 ns |    NA | 1.000 | 0.9155 |     - |     - |    7810 B | </para><para>
    /// |                FastEnum_Init_Type |     35.70 ns |    NA | 0.002 |      - |     - |     - |         - | </para><para>
    /// |              FastEnum_Init_Values |  1,215.20 ns |    NA | 0.066 | 0.0439 |     - |     - |     376 B | </para><para>
    /// |               FastEnum_Init_Names |     65.05 ns |    NA | 0.004 | 0.0181 |     - |     - |     152 B | </para><para>
    /// |             FastEnum_Init_Members | 11,707.83 ns |    NA | 0.638 | 0.3815 |     - |     - |    3265 B | </para><para>
    /// |        FastEnum_Init_MinMaxValues |  1,667.81 ns |    NA | 0.091 | 0.0629 |     - |     - |     536 B | </para><para>
    /// |             FastEnum_Init_IsFlags |  1,951.05 ns |    NA | 0.106 | 0.0343 |     - |     - |     312 B | </para><para>
    /// |       FastEnum_Init_MembersByName | 12,322.42 ns |    NA | 0.671 | 0.5035 |     - |     - |    4297 B | </para><para>
    /// | FastEnum_Init_UnderlyingOperation | 15,356.57 ns |    NA | 0.837 | 0.7324 |     - |     - |    6257 B | </para><para>
    /// |                    Enum_GetValues |  1,222.48 ns |    NA | 0.067 | 0.0420 |     - |     - |     352 B | </para><para>
    /// |                     Enum_GetNames |     54.55 ns |    NA | 0.003 | 0.0153 |     - |     - |     128 B | </para><para>
    /// |                      Enum_GetName |     65.07 ns |    NA | 0.004 | 0.0029 |     - |     - |      24 B | </para><para>
    /// |                    Enum_IsDefined |    145.30 ns |    NA | 0.008 | 0.0029 |     - |     - |      24 B | </para><para>
    /// |                        Enum_Parse |    131.59 ns |    NA | 0.007 | 0.0029 |     - |     - |      24 B | </para><para>
    /// |                     Enum_ToString |     32.82 ns |    NA | 0.002 | 0.0029 |     - |     - |      24 B | </para><para>
    /// </para> </remarks>
    public class InitializeBenchmark
    {

        #region FastEnum

        [Benchmark( Baseline = true )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void FastEnum_Init_All()
        {
            Cache<Fruits>();

            static void Cache<T>()
                where T : struct, Enum
            {
                var type = typeof( T );
                var underlyingType = Enum.GetUnderlyingType( type );
                var values = (( T[] ) Enum.GetValues( type )).AsReadOnly();
                var names = Enum.GetNames( type ).ToReadOnlyArray();
                var members = names.Select( x => new Member<T>( x ) ).ToReadOnlyArray();
                var minValue = values.DefaultIfEmpty().Min();
                var maxValue = values.DefaultIfEmpty().Max();
                var isEmpty = values.Count == 0;
                var isFlags = Attribute.IsDefined( type, typeof( FlagsAttribute ) );
                var distinctedMembers = members.Distinct( new Member<T>.ValueComparer() ).ToArray();
                var memberByValue = distinctedMembers.ToFrozenDictionary( x => x.Value );
                var memberByName = members.ToFrozenStringKeyDictionary( x => x.Name );
                var underlyingOperation
                    = Type.GetTypeCode( type ) switch {
                        TypeCode.SByte => SByteOperation<T>.Create( minValue, maxValue, distinctedMembers ),
                        TypeCode.Byte => ByteOperation<T>.Create( minValue, maxValue, distinctedMembers ),
                        TypeCode.Int16 => Int16Operation<T>.Create( minValue, maxValue, distinctedMembers ),
                        TypeCode.UInt16 => UInt16Operation<T>.Create( minValue, maxValue, distinctedMembers ),
                        TypeCode.Int32 => Int32Operation<T>.Create( minValue, maxValue, distinctedMembers ),
                        TypeCode.UInt32 => UInt32Operation<T>.Create( minValue, maxValue, distinctedMembers ),
                        TypeCode.Int64 => Int64Operation<T>.Create( minValue, maxValue, distinctedMembers ),
                        TypeCode.UInt64 => UInt64Operation<T>.Create( minValue, maxValue, distinctedMembers ),
                        _ => throw new InvalidOperationException(),
                    };
            }
        }


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void FastEnum_Init_Type()
        {
            Cache<Fruits>();

            static void Cache<T>()
                where T : struct, Enum
            {
                var type = typeof( T );
                _ = Enum.GetUnderlyingType( type );
            }
        }


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void FastEnum_Init_Values()
        {
            Cache<Fruits>();

            static void Cache<T>()
                where T : struct, Enum
            {
                var type = typeof( T );
                var values = ( T[] ) Enum.GetValues( type );
                _ = values.AsReadOnly();
                _ = values.Length == 0;
            }
        }


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void FastEnum_Init_Names()
        {
            Cache<Fruits>();

            static void Cache<T>()
                where T : struct, Enum
            {
                var type = typeof( T );
                _ = Enum.GetNames( type ).ToReadOnlyArray();
            }
        }


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void FastEnum_Init_Members()
        {
            Cache<Fruits>();

            static void Cache<T>()
                where T : struct, Enum
            {
                var type = typeof( T );
                var names = Enum.GetNames( type ).ToReadOnlyArray();
                var members
                    = names
                    .Select( x => new Member<T>( x ) )
                    .ToReadOnlyArray();
            }
        }


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void FastEnum_Init_MinMaxValues()
        {
            Cache<Fruits>();

            static void Cache<T>()
                where T : struct, Enum
            {
                var type = typeof( T );
                var values = ( T[] ) Enum.GetValues( type );
                var values2 = values.AsReadOnly();
                _ = values.Length == 0;
                _ = values2.DefaultIfEmpty().Min();
                _ = values2.DefaultIfEmpty().Max();
            }
        }


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void FastEnum_Init_IsFlags()
        {
            Cache<Fruits>();

            static void Cache<T>()
                where T : struct, Enum
            {
                var type = typeof( T );
                _ = Attribute.IsDefined( type, typeof( FlagsAttribute ) );
            }
        }


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void FastEnum_Init_MembersByName()
        {
            Cache<Fruits>();

            static void Cache<T>()
                where T : struct, Enum
            {
                var type = typeof( T );
                var names = Enum.GetNames( type ).ToReadOnlyArray();
                var members
                    = names
                    .Select( x => new Member<T>( x ) )
                    .ToReadOnlyArray();

                var memberByName = members.ToFrozenStringKeyDictionary( x => x.Name );
            }
        }


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void FastEnum_Init_UnderlyingOperation()
        {
            Cache<Fruits>();

            static void Cache<T>()
                where T : struct, Enum
            {
                var type = typeof( T );
                var values = ( T[] ) Enum.GetValues( type );
                var values2 = values.AsReadOnly();
                var isEmpty = values.Length == 0;
                var min = values2.DefaultIfEmpty().Min();
                var max = values2.DefaultIfEmpty().Max();
                var names = Enum.GetNames( type ).ToReadOnlyArray();
                var members
                    = names
                    .Select( x => new Member<T>( x ) )
                    .ToReadOnlyArray();
                var distincted = members.OrderBy( x => x.Value ).Distinct( new Member<T>.ValueComparer() ).ToArray();
                var underlyingOperation
                    = Type.GetTypeCode( type ) switch {
                        TypeCode.SByte => SByteOperation<T>.Create( min, max, distincted ),
                        TypeCode.Byte => ByteOperation<T>.Create( min, max, distincted ),
                        TypeCode.Int16 => Int16Operation<T>.Create( min, max, distincted ),
                        TypeCode.UInt16 => UInt16Operation<T>.Create( min, max, distincted ),
                        TypeCode.Int32 => Int32Operation<T>.Create( min, max, distincted ),
                        TypeCode.UInt32 => UInt32Operation<T>.Create( min, max, distincted ),
                        TypeCode.Int64 => Int64Operation<T>.Create( min, max, distincted ),
                        TypeCode.UInt64 => UInt64Operation<T>.Create( min, max, distincted ),
                        _ => throw new InvalidOperationException(),
                    };
            }
        }

        #endregion

        #region Enum

        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void Enum_GetValues()
            => _ = Enum.GetValues( typeof( Fruits ) ) as Fruits[];


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void Enum_GetNames()
            => _ = Enum.GetNames( typeof( Fruits ) );


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void Enum_GetName()
            => _ = Enum.GetName( typeof( Fruits ), Fruits.Lemon );


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void Enum_IsDefined()
            => _ = Enum.IsDefined( typeof( Fruits ), Fruits.Melon );


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void Enum_Parse()
            => _ = Enum.Parse( typeof( Fruits ), "Apple" );


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void Enum_ToString()
            => _ = Fruits.Grape.ToString();
        #endregion
    }
}
