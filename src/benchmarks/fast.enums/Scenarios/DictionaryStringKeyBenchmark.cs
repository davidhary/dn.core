using System.Collections;
using System.Collections.Generic;
using System.Linq;

using BenchmarkDotNet.Attributes;

using FastEnums.Benchmark.Models;
using FastEnums.Internals;



namespace FastEnums.Benchmark.Scenarios
{

    /// <summary>   A dictionary string key benchmark. </summary>
    /// <remarks>   David, 2021-02-20. <para>
    /// |                    Method |     Mean | Error | Ratio | Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
    /// |-------------------------- |---------:|------:|------:|------:|------:|------:|----------:| </para><para>
    /// |                Dictionary | 16.79 ns |    NA |  1.00 |     - |     - |     - |         - | </para><para>
    /// |          FrozenDictionary | 12.85 ns |    NA |  0.77 |     - |     - |     - |         - | </para><para>
    /// | FrozenStringKeyDictionary | 11.00 ns |    NA |  0.66 |     - |     - |     - |         - | </para><para>
    /// |                 HashTable | 23.73 ns |    NA |  1.41 |     - |     - |     - |         - | </para><para>
    /// </para> </remarks>
    public class DictionaryStringKeyBenchmark
    {
        private const string _LookupKey = nameof( Fruits.Apple );


#pragma warning disable CS8618
        private Dictionary<string, Member<Fruits>> Standard { get; set; }
        private FrozenDictionary<string, Member<Fruits>> GenericsKeyFrozen { get; set; }
        private FrozenStringKeyDictionary<Member<Fruits>> StringKeyFrozen { get; set; }
        private Hashtable Table { get; set; }
#pragma warning restore CS8618


        [GlobalSetup]
        public void Setup()
        {
            var members = FastEnum.GetMembers<Fruits>();
            this.Standard = members.ToDictionary( x => x.Name );
            this.GenericsKeyFrozen = members.ToFrozenDictionary( x => x.Name );
            this.StringKeyFrozen = members.ToFrozenStringKeyDictionary( x => x.Name );
            this.Table = new Hashtable( members.Count );
            foreach ( var x in members )
                this.Table[x.Name] = x;
        }


        [Benchmark( Baseline = true )]
        public bool Dictionary()
            => this.Standard.TryGetValue( _LookupKey, out _ );


        [Benchmark]
        public bool FrozenDictionary()
            => this.GenericsKeyFrozen.TryGetValue( _LookupKey, out _ );


        [Benchmark]
        public bool FrozenStringKeyDictionary()
            => this.StringKeyFrozen.TryGetValue( _LookupKey, out _ );


        [Benchmark]
        public Member<Fruits> HashTable()
            => ( Member<Fruits> ) this.Table[_LookupKey]!;
    }
}
