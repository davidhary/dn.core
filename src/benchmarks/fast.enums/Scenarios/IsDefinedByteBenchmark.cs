using System;

using BenchmarkDotNet.Attributes;

using FastEnums.Benchmark.Models;

using _FastEnum = FastEnums.FastEnum;



namespace FastEnums.Benchmark.Scenarios
{

    /// <summary>   The is defined byte benchmark. </summary>
    /// <remarks>   David, 2021-02-20. <para>
    /// |   Method |       Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
    /// |--------- |-----------:|------:|------:|-------:|------:|------:|----------:| </para><para>
    /// |  NetCore | 120.134 ns |    NA |  1.00 | 0.0029 |     - |     - |      24 B | </para><para>
    /// | FastEnum |   3.362 ns |    NA |  0.03 |      - |     - |     - |         - | </para><para>
    /// </para> </remarks>
    public class IsDefinedByteBenchmark
    {

        private const byte _Value = ( byte ) Fruits.Cherry;

        [GlobalSetup]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void Setup()
        {
            _ = Enum.GetNames( typeof( Fruits ) );
            _ = _FastEnum.GetValues<Fruits>();
        }


        [Benchmark( Baseline = true )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool NetCore()
            => Enum.IsDefined( typeof( Fruits ), _Value );


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void FastEnum()
            => _FastEnum.IsDefined<Fruits>( _Value );
    }
}
