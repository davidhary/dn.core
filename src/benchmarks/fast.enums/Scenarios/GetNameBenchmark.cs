using System;

using BenchmarkDotNet.Attributes;

using FastEnums.Benchmark.Models;

using _FastEnum = FastEnums.FastEnum;



namespace FastEnums.Benchmark.Scenarios
{
    /// <summary>   A get name benchmark. </summary>
    /// <remarks>   David, 2021-02-19. <para>
    /// |       Method |        Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
    /// |------------- |------------:|------:|------:|-------:|------:|------:|----------:| </para><para>
    /// |      NetCore |  72.2193 ns |    NA | 1.000 | 0.0029 |     - |     - |      24 B | </para><para>
    /// |     EnumsNet |   1.1490 ns |    NA | 0.016 |      - |     - |     - |         - | </para><para>
    /// |     FastEnum |   0.2430 ns |    NA | 0.003 |      - |     - |     - |         - | </para><para>
    /// Legacy:  </para><para>
    /// | EnumExtender | 252.2026 ns |    NA | 3.492 | 0.0839 |     - |     - |     704 B | </para><para>
    /// |  MelodyEnums |   6.8719 ns |    NA | 0.095 |      - |     - |     - |         - | </para><para>
    /// </para> </remarks>
    public class GetNameBenchmark
    {

        private const Fruits _Value = Fruits.Pineapple;

        [GlobalSetup]
        public void Setup()
        {
            _ = Enum.GetNames( typeof( Fruits ) );
            _ = EnumsNET.Enums.GetNames<Fruits>();
            _ = _FastEnum.GetNames<Fruits>();
            _ = _Value.HasAnyFlags<Fruits>();
        }


        [Benchmark( Baseline = true )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public string? NetCore()
            => Enum.GetName( typeof( Fruits ), _Value );


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public string? EnumsNet()
            => EnumsNET.Enums.GetName( _Value );


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public string FastEnum()
            => _FastEnum.GetName( _Value );

    }
}
