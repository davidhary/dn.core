using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using BenchmarkDotNet.Attributes;

using FastEnums.Internals;



namespace FastEnums.Benchmark.Scenarios
{

    /// <summary>   for each benchmark. </summary>
    /// <remarks>   David, 2021-02-20. <para>
    /// |             Method |      Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
    /// |------------------- |----------:|------:|------:|-------:|------:|------:|----------:| </para><para>
    /// |              Array |  35.04 ns |    NA |  1.00 |      - |     - |     - |         - | </para><para>
    /// |               List | 196.77 ns |    NA |  5.62 |      - |     - |     - |         - | </para><para>
    /// | ReadOnlyCollection | 595.99 ns |    NA | 17.01 | 0.0048 |     - |     - |      40 B | </para><para>
    /// |      IReadOnlyList | 380.08 ns |    NA | 10.85 | 0.0038 |     - |     - |      32 B | </para><para>
    /// |      ReadOnlyArray |  40.88 ns |    NA |  1.17 |      - |     - |     - |         - | </para><para>
    /// </para> </remarks>
    public class ForEachBenchmark
    {
#pragma warning disable CS8618
#pragma warning disable IDE1006 // Naming Styles
        private ReadOnlyArray<int> _ReadOnlyArray { get; set; }
        private ReadOnlyCollection<int> _ReadOnlyCollection { get; set; }
        private IReadOnlyList<int> _IReadOnlyList { get; set; }
        private List<int> _List { get; set; }
        private int[] _Array { get; set; }
#pragma warning restore CS8618
#pragma warning restore IDE1006 // Naming Styles

        [GlobalSetup]
        public void Setup()
        {
            var raw = Enumerable.Range( 1, 100 );
            this._ReadOnlyArray = new ReadOnlyArray<int>( raw.ToArray() );
            this._ReadOnlyCollection = new ReadOnlyCollection<int>( raw.ToList() );
            this._IReadOnlyList = raw.ToArray();
            this._List = raw.ToList();
            this._Array = raw.ToArray();
        }


        [Benchmark( Baseline = true )]
        public int Array()
        {
            var sum = 0;
            foreach ( var x in this._Array )
                sum += x;
            return sum;
        }


        [Benchmark]
        public int List()
        {
            var sum = 0;
            foreach ( var x in this._List )
                sum += x;
            return sum;
        }


        [Benchmark]
        public int ReadOnlyCollection()
        {
            var sum = 0;
            foreach ( var x in this._ReadOnlyCollection )
                sum += x;
            return sum;
        }


        [Benchmark]
        public int IReadOnlyList()
        {
            var sum = 0;
            foreach ( var x in this._IReadOnlyList )
                sum += x;
            return sum;
        }


        [Benchmark]
        public int ReadOnlyArray()
        {
            var sum = 0;
            foreach ( var x in this._ReadOnlyArray )
                sum += x;
            return sum;
        }
    }
}
