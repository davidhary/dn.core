using System;

using BenchmarkDotNet.Attributes;

using FastEnums.Benchmark.Models;

using _FastEnum = FastEnums.FastEnum;



namespace FastEnums.Benchmark.Scenarios
{
    /// <summary>   The is defined enum benchmark. </summary>
    /// <remarks>   David, 2021-02-20. <para>
    /// |       Method |        Mean | Error | Ratio |  Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
    /// |------------- |------------:|------:|------:|-------:|------:|------:|----------:| </para><para>
    /// |      NetCore | 144.1818 ns |    NA | 1.000 | 0.0029 |     - |     - |      24 B | </para><para>
    /// |     EnumsNet |   0.6131 ns |    NA | 0.004 |      - |     - |     - |         - | </para><para>
    /// |     FastEnum |   0.0000 ns |    NA | 0.000 |      - |     - |     - |         - | </para><para>
    /// Legacy:  </para><para>
    /// | EnumExtender |  47.1552 ns |    NA | 0.327 | 0.0057 |     - |     - |      48 B | </para><para>
    /// |  MelodyEnums |   8.9550 ns |    NA | 0.062 |      - |     - |     - |         - | </para><para>
    /// </para></remarks>
    public class IsDefinedEnumBenchmark
    {

        private const Fruits _Value = Fruits.Pineapple;


        [GlobalSetup]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void Setup()
        {
            _ = Enum.GetNames( typeof( Fruits ) );
            _ = EnumsNET.Enums.GetValues<Fruits>();
            _ = _FastEnum.GetValues<Fruits>();
        }


        [Benchmark( Baseline = true )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool NetCore()
            => Enum.IsDefined( typeof( Fruits ), _Value );


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool EnumsNet()
            => EnumsNET.Enums.IsDefined( _Value );


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void FastEnum()
            => _FastEnum.IsDefined( _Value );

    }
}
