using System;

using BenchmarkDotNet.Attributes;

using FastEnums.Benchmark.Models;

using _FastEnum = FastEnums.FastEnum;


namespace FastEnums.Benchmark.Scenarios
{

    /// <summary>   The is defined string benchmark. </summary>
    /// <remarks>   David, 2021-02-20. <para>
    /// |   Method |     Mean | Error | Ratio | Gen 0 | Gen 1 | Gen 2 | Allocated | </para><para>
    /// |--------- |---------:|------:|------:|------:|------:|------:|----------:| </para><para>
    /// |  NetCore | 99.52 ns |    NA |  1.00 |     - |     - |     - |         - | </para><para>
    /// | FastEnum | 16.38 ns |    NA |  0.16 |     - |     - |     - |         - | </para><para>
    /// </para> </remarks>
    public class IsDefinedStringBenchmark
    {
        private const string _Value = nameof( Fruits.Pineapple );


        [GlobalSetup]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void Setup()
        {
            _ = Enum.GetNames( typeof( Fruits ) );
            _ = _FastEnum.GetValues<Fruits>();
        }


        [Benchmark( Baseline = true )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool NetCore()
            => Enum.IsDefined( typeof( Fruits ), _Value );


        [Benchmark]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool FastEnum()
            => _FastEnum.IsDefined<Fruits>( _Value );
    }
}
