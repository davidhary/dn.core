using System;

namespace isr.Core.NumericExtensions
{
    /// <summary> Includes extensions for <see cref="Random">random number generation</see>. </summary>
    /// <remarks> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-11-14, 2.0.5066 </para></remarks>
    public static partial class NumericExtensionMethods
    {

        /// <summary> Compute the distance between the point and the origin. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="point"> The point. </param>
        /// <returns> The distance between a point and the origin. </returns>
        public static double Hypotenuse( this System.Windows.Point point )
        {
            return point.X.Hypotenuse( point.Y );
        }

        /// <summary> Computes the distance between two points. </summary>
        /// <remarks>
        /// <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
        /// would overflow.</para>
        /// </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns>
        /// The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>).
        /// </returns>
        public static double Hypotenuse( this System.Windows.Point left, System.Windows.Point right )
        {
            return (right.X - left.X).Hypotenuse( right.Y - left.Y );
        }

        /// <summary> Computes the length of a right triangle's hypotenuse. </summary>
        /// <remarks>
        /// <para>The length is computed accurately, even in cases where x<sup>2</sup> or y<sup>2</sup>
        /// would overflow.</para>
        /// </remarks>
        /// <param name="value">      The length of one side. </param>
        /// <param name="orthogonal"> The length of orthogonal side. </param>
        /// <returns>
        /// The length of the hypotenuse, square root of (x<sup>2</sup> + y<sup>2</sup>).
        /// </returns>
        public static double Hypotenuse( this double value, double orthogonal )
        {
            if ( value == 0.0d && orthogonal == 0.0d )
            {
                return 0.0d;
            }
            else
            {
                double ax = Math.Abs( value );
                double ay = Math.Abs( orthogonal );
                if ( ax > ay )
                {
                    double r = orthogonal / value;
                    return ax * Math.Sqrt( 1.0d + r * r );
                }
                else
                {
                    double r = value / orthogonal;
                    return ay * Math.Sqrt( 1.0d + r * r );
                }
            }
        }

    }
}
