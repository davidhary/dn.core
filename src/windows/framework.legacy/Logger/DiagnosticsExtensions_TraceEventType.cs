using System.Collections.Generic;
using System.Diagnostics;

namespace isr.Core.DiagnosticsExtensions
{
    public static partial class DiagnosticsExtensionMethods
    {

        #region " TRACE EVENT TO AN ID "

        /// <summary> Builds trace event type identifier hash. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A Dictionary for translating trace events to Id. </returns>
        public static IDictionary<TraceEventType, int> BuildTraceEventTypeIdHash()
        {
            var dix2 = new Dictionary<TraceEventType, int>();
            var dix3 = dix2;
            dix3.Add( TraceEventType.Information, 0 );
            dix3.Add( TraceEventType.Warning, 1 );
            dix3.Add( TraceEventType.Error, 2 );
            dix3.Add( TraceEventType.Critical, 3 );
            dix3.Add( TraceEventType.Start, 4 );
            dix3.Add( TraceEventType.Stop, 5 );
            dix3.Add( TraceEventType.Suspend, 6 );
            dix3.Add( TraceEventType.Resume, 7 );
            dix3.Add( TraceEventType.Verbose, 8 );
            dix3.Add( TraceEventType.Transfer, 9 );
            return dix2;
        }

        /// <summary> The trace event type identifier hash. </summary>
        private static IDictionary<TraceEventType, int> _TraceEventTypeIdHash;

        /// <summary> Trace event type identifier hash. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> An IDictionary(Of TraceEventType, Integer) </returns>
        private static IDictionary<TraceEventType, int> TraceEventTypeIdHash()
        {
            if ( _TraceEventTypeIdHash is null )
            {
                _TraceEventTypeIdHash = BuildTraceEventTypeIdHash();
            }

            return _TraceEventTypeIdHash;
        }

        /// <summary> Derives an id given the <see cref="TraceEventType">trace event type</see>. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> A <see cref="TraceEventType">event type</see>. </param>
        /// <returns> Ids. </returns>
        public static int ToId( this TraceEventType value )
        {
            return TraceEventTypeIdHash().ContainsKey( value ) ? TraceEventTypeIdHash()[value] : 0;
        }

        #endregion

        #region " TRACE LEVELS AND TRACE EVENT TYPES "

        /// <summary> Builds trace event type trace level hash. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A Dictionary for translating trace events to trace levels. </returns>
        public static IDictionary<TraceEventType, TraceLevel> BuildTraceEventTypeTraceLevelHash()
        {
            var dix2 = new Dictionary<TraceEventType, TraceLevel>();
            var dix3 = dix2;
            dix3.Add( TraceEventType.Critical, System.Diagnostics.TraceLevel.Error );
            dix3.Add( TraceEventType.Error, System.Diagnostics.TraceLevel.Error );
            dix3.Add( TraceEventType.Information, System.Diagnostics.TraceLevel.Info );
            dix3.Add( TraceEventType.Resume, System.Diagnostics.TraceLevel.Info );
            dix3.Add( TraceEventType.Start, System.Diagnostics.TraceLevel.Info );
            dix3.Add( TraceEventType.Suspend, System.Diagnostics.TraceLevel.Off );
            dix3.Add( TraceEventType.Transfer, System.Diagnostics.TraceLevel.Info );
            dix3.Add( TraceEventType.Verbose, System.Diagnostics.TraceLevel.Verbose );
            dix3.Add( TraceEventType.Warning, System.Diagnostics.TraceLevel.Warning );
            return dix2;
        }

        /// <summary> The trace event type trace level hash. </summary>


        private static IDictionary<TraceEventType, TraceLevel> _TraceEventTypeTraceLevelHash;

        /// <summary> Trace event type trace level hash. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> An IDictionary(Of TraceEventType, TraceLevel) </returns>
        private static IDictionary<TraceEventType, TraceLevel> TraceEventTypeTraceLevelHash()
        {
            if ( _TraceEventTypeTraceLevelHash is null )
            {
                _TraceEventTypeTraceLevelHash = BuildTraceEventTypeTraceLevelHash();
            }

            return _TraceEventTypeTraceLevelHash;
        }

        /// <summary>
        /// Derives a <see cref="System.Diagnostics.TraceLevel">trace level</see> from the
        /// <see cref="TraceEventType">trace event type</see>.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> A <see cref="TraceEventType">event type</see>. </param>
        /// <returns>
        /// A <see cref="System.Diagnostics.TraceLevel">trace level</see> of a trace switch. Specifies what messages to
        /// output for the <see cref="System.Diagnostics.Debug">debug</see>
        /// and <see cref="System.Diagnostics.Trace">trace</see> and
        /// <see cref="System.Diagnostics.TraceSwitch">trace switch</see> classes.
        /// </returns>
        public static TraceLevel ToTraceLevel( this TraceEventType value )
        {
            return TraceEventTypeTraceLevelHash().ContainsKey( value ) ? TraceEventTypeTraceLevelHash()[value] : System.Diagnostics.TraceLevel.Info;
        }

        #endregion

        #region " SOURCE LEVELS AND TRACE EVENT TYPES "

        /// <summary> Parse trace event type. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> A <see cref="TraceEventType">event type</see>. </param>
        /// <returns> The SourceLevels. </returns>
        public static SourceLevels ParseTraceEventType( TraceEventType value )
        {
            var result = SourceLevels.Information;
            switch ( value )
            {
                case TraceEventType.Critical:
                    {
                        result = SourceLevels.Critical;
                        break;
                    }

                case TraceEventType.Error:
                    {
                        result = SourceLevels.Error;
                        break;
                    }

                case TraceEventType.Information:
                    {
                        result = SourceLevels.Information;
                        break;
                    }

                case TraceEventType.Verbose:
                    {
                        result = SourceLevels.Verbose;
                        break;
                    }

                case TraceEventType.Warning:
                    {
                        result = SourceLevels.Warning;
                        break;
                    }
            }

            return result;
        }

        /// <summary> Builds trace event type source levels hash. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A Dictionary for translating trace events to source levels. </returns>
        public static IDictionary<TraceEventType, SourceLevels> BuildTraceEventTypeSourceLevelsHash()
        {
            var dix2 = new Dictionary<TraceEventType, SourceLevels>();
            var dix3 = dix2;
            dix3.Add( TraceEventType.Critical, ParseTraceEventType( TraceEventType.Critical ) );
            dix3.Add( TraceEventType.Error, ParseTraceEventType( TraceEventType.Error ) );
            dix3.Add( TraceEventType.Information, ParseTraceEventType( TraceEventType.Information ) );
            dix3.Add( TraceEventType.Verbose, ParseTraceEventType( TraceEventType.Verbose ) );
            dix3.Add( TraceEventType.Warning, ParseTraceEventType( TraceEventType.Warning ) );
            return dix2;
        }

        /// <summary> Source levels trace event type source levels hash. </summary>
        private static IDictionary<TraceEventType, SourceLevels> _SourceLevelsTraceEventTypeSourceLevelsHash;

        /// <summary> Source levels trace event type source levels hash. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> An IDictionary(Of SourceLevels, TraceEventType) </returns>
        private static IDictionary<TraceEventType, SourceLevels> SourceLevelsTraceEventTypeSourceLevelsHash()
        {
            if ( _SourceLevelsTraceEventTypeSourceLevelsHash is null )
            {
                _SourceLevelsTraceEventTypeSourceLevelsHash = BuildTraceEventTypeSourceLevelsHash();
            }

            return _SourceLevelsTraceEventTypeSourceLevelsHash;
        }

        /// <summary>
        /// Derives a <see cref="SourceLevels">source level</see> given the
        /// <see cref="TraceEventType">trace event type</see>.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> A <see cref="TraceEventType">event type</see>. </param>
        /// <returns> SourceLevels. </returns>
        public static SourceLevels ToSourceLevel( this TraceEventType value )
        {
            return SourceLevelsTraceEventTypeSourceLevelsHash().ContainsKey( value ) ? SourceLevelsTraceEventTypeSourceLevelsHash()[value] : SourceLevels.Information;
        }

        #endregion

    }
}
