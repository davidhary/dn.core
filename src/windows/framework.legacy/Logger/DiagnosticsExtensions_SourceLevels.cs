using System.Collections.Generic;
using System.Diagnostics;

namespace isr.Core.DiagnosticsExtensions
{

    /// <summary> A diagnostics extensions. </summary>
    /// <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static partial class DiagnosticsExtensionMethods
    {

        #region " SOURCE LEVELS AND TRACE EVENT TYPES "

        /// <summary> Builds source level trace event type hash. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> A Dictionary for translating source level to trace event type. </returns>
        public static IDictionary<SourceLevels, TraceEventType> BuildSourceLevelsTraceEventTypeHash()
        {
            var dix2 = new Dictionary<SourceLevels, TraceEventType>();
            var dix3 = dix2;
            dix3.Add( SourceLevels.Critical, TraceEventType.Critical );
            dix3.Add( SourceLevels.Error, TraceEventType.Error );
            dix3.Add( SourceLevels.Information, TraceEventType.Information );
            dix3.Add( SourceLevels.Off, TraceEventType.Stop );
            dix3.Add( SourceLevels.Verbose, TraceEventType.Verbose );
            dix3.Add( SourceLevels.Warning, TraceEventType.Warning );
            return dix2;
        }

        /// <summary> Source levels trace event type hash. </summary>
        private static IDictionary<SourceLevels, TraceEventType> _SourceLevelsTraceEventTypeHash;

        /// <summary> Source levels trace event type hash. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> An IDictionary(Of SourceLevels, TraceEventType) </returns>
        private static IDictionary<SourceLevels, TraceEventType> SourceLevelsTraceEventTypeHash()
        {
            if ( _SourceLevelsTraceEventTypeHash is null )
            {
                _SourceLevelsTraceEventTypeHash = BuildSourceLevelsTraceEventTypeHash();
            }

            return _SourceLevelsTraceEventTypeHash;
        }

        /// <summary>
        /// Derives a <see cref="TraceEventType">trace event type</see> given the
        /// <see cref="SourceLevels">source level</see> of a trace switch.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="value"> The <see cref="SourceLevels">levels</see> of the trace message filtered
        /// by the source switch and event type filter. </param>
        /// <returns> The Trace <see cref="TraceEventType">event type</see>. </returns>
        public static TraceEventType ToTraceEventType( this SourceLevels value )
        {
            return SourceLevelsTraceEventTypeHash().ContainsKey( value ) ? SourceLevelsTraceEventTypeHash()[value] : TraceEventType.Information;
        }

        #endregion

    }
}
