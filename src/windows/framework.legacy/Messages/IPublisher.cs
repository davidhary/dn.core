using System.Collections.Generic;
using System.Diagnostics;

namespace isr.Core
{
    /// <summary> Interface for a publisher. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IPublisher
    {

        /// <summary> Adds a listener. </summary>
        /// <param name="listener"> The listener. </param>
        void AddListener( IMessageListener listener );

        /// <summary> Adds the listeners such as the top level trace messages box and log. </summary>
        /// <param name="talker"> The talker. </param>
        void AddListeners( ITraceMessageTalker talker );

        /// <summary> Adds the listeners such as the top level trace messages box and log. </summary>
        /// <param name="listeners"> The listeners. </param>
        void AddListeners( IList<IMessageListener> listeners );

        /// <summary> Applies the trace level to all listeners of the specified type. </summary>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        void ApplyListenerTraceLevel( ListenerType listenerType, TraceEventType value );

        /// <summary> Gets or sets the private listeners. </summary>
        /// <value> The private listeners. </value>
        IList<IMessageListener> PrivateListeners { get; }

        /// <summary> Adds a private listener. </summary>
        /// <param name="listener"> The listener. </param>
        void AddPrivateListener( IMessageListener listener );

        /// <summary> Adds private listeners. </summary>
        /// <param name="listeners"> The listeners. </param>
        void AddPrivateListeners( IList<IMessageListener> listeners );

        /// <summary> Adds private listeners. </summary>
        /// <param name="talker"> The talker. </param>
        void AddPrivateListeners( ITraceMessageTalker talker );

        /// <summary> Removes the private listener described by listener. </summary>
        /// <param name="listener"> The listener. </param>
        void RemovePrivateListener( IMessageListener listener );

        /// <summary> Removes the private listeners. </summary>
        void RemovePrivateListeners();

        /// <summary> Removes the listeners if the talker was not assigned. </summary>
        void RemoveListeners();

        /// <summary> Removes a listeners if the talker was not assigned. </summary>
        /// <param name="listener"> The listener. </param>
        void RemoveListener( IMessageListener listener );

        /// <summary> Removes the listeners if the talker was not assigned. </summary>
        /// <param name="listeners"> The listeners. </param>
        void RemoveListeners( IList<IMessageListener> listeners );

        /// <summary> Applies the trace level type to all talkers. </summary>
        /// <param name="listenerType"> Type of listener. </param>
        /// <param name="value">        The value. </param>
        void ApplyTalkerTraceLevel( ListenerType listenerType, TraceEventType value );

        /// <summary> Applies the talker trace levels described by talker. </summary>
        /// <param name="talker"> The talker. </param>
        void ApplyTalkerTraceLevels( ITraceMessageTalker talker );

        /// <summary> Applies the talker listener trace levels described by talker. </summary>
        /// <param name="talker"> The talker. </param>
        void ApplyListenerTraceLevels( ITraceMessageTalker talker );
    }
}
