using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace isr.Core
{
    /// <summary> Interface for trace message listener. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IMessageListener : IEquatable<IMessageListener>
    {

        /// <summary> Writes. </summary>
        /// <param name="message"> The message to write. </param>
        void Write( string message );

        /// <summary> Writes a line. </summary>
        /// <param name="message"> The message to write. </param>
        void WriteLine( string message );

        /// <summary> Registers this object. </summary>
        /// <param name="level"> The level. </param>
        void Register( TraceEventType level );

        /// <summary> Gets or sets the type of the listener. </summary>
        /// <value> The type of the listener. </value>
        ListenerType ListenerType { get; }

        /// <summary> Gets or sets the trace level. </summary>
        /// <value> The trace level. </value>
        TraceEventType TraceLevel { get; set; }

        /// <summary> Applies the trace level described by Trace Level. </summary>
        /// <param name="value"> The trace level. </param>
        void ApplyTraceLevel( TraceEventType value );

        /// <summary> Determine if we should trace. </summary>
        /// <param name="value"> The event message. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        bool ShouldTrace( TraceEventType value );

        /// <summary> Gets or sets a unique identifier. </summary>
        /// <value> The identifier of the unique. </value>
        Guid UniqueId { get; }

        /// <summary> Gets or sets the is disposed. </summary>
        /// <value> The is disposed. </value>
        bool IsDisposed { get; }

        /// <summary> Gets or sets the is thread safe. </summary>
        /// <value> The is thread safe. </value>
        bool IsThreadSafe { get; }
    }

    /// <summary> Collection of trace listeners. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-29 </para>
    /// </remarks>
    public class MessageListenerCollection : System.Collections.ObjectModel.KeyedCollection<Guid, IMessageListener>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public MessageListenerCollection() : base()
        {
            this.ListenerDix = new Dictionary<ListenerType, ListenerCollection>();
        }

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns> The key for the specified element. </returns>
        protected override Guid GetKeyForItem( IMessageListener item )
        {
            return item is null ? throw new ArgumentNullException( nameof( item ) ) : item.UniqueId;
        }

        /// <summary> Collection of listeners. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        private class ListenerCollection : System.Collections.ObjectModel.KeyedCollection<Guid, IMessageListener>
        {

            /// <summary>
            /// When implemented in a derived class, extracts the key from the specified element.
            /// </summary>
            /// <remarks> David, 2020-09-17. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="item"> The element from which to extract the key. </param>
            /// <returns> The key for the specified element. </returns>
            protected override Guid GetKeyForItem( IMessageListener item )
            {
                return item is null ? throw new ArgumentNullException( nameof( item ) ) : item.UniqueId;
            }
        }

        /// <summary> Gets or sets the listener dictionary. </summary>
        /// <value> The listener dictionary. </value>
        private IDictionary<ListenerType, ListenerCollection> ListenerDix { get; set; }

        /// <summary> Adds items. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="items"> The items to add. </param>
        public void Add( IList<IMessageListener> items )
        {
            if ( items?.Any() == true )
            {
                foreach ( IMessageListener item in items )
                {
                    this.Add( item );
                }
            }
        }

        /// <summary> Gets or sets the contains logger listener. </summary>
        /// <value> The contains logger listener. </value>
        public bool ContainsLoggerListener { get; private set; }

        /// <summary> Query if the collection contains a listener. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool ContainsListener( ListenerType listenerType )
        {
            return this.ListenerDix.ContainsKey( listenerType ) && this.ListenerDix[listenerType].Any();
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="item"> The object to add to the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        public new void Add( IMessageListener item )
        {
            if ( item is null )
            {
                throw new ArgumentNullException( nameof( item ) );
            }

            if ( !this.Contains( item ) )
            {
                base.Add( item );
                if ( !this.ListenerDix.ContainsKey( item.ListenerType ) )
                {
                    this.ListenerDix.Add( item.ListenerType, new ListenerCollection() );
                }

                if ( !this.ListenerDix[item.ListenerType].Contains( item.UniqueId ) )
                {
                    this.ListenerDix[item.ListenerType].Add( item );
                }

                this.ContainsLoggerListener = this.ContainsListener( ListenerType.Logger );
            }
        }

        /// <summary> Removes the given item. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="item"> The object to add to the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        public new void Remove( IMessageListener item )
        {
            if ( item is null )
            {
                throw new ArgumentNullException( nameof( item ) );
            }

            if ( this.Contains( item ) )
            {
                _ = base.Remove( item );
                if ( this.ListenerDix[item.ListenerType].Contains( item.UniqueId ) )
                {
                    _ = this.ListenerDix[item.ListenerType].Remove( item );
                }

                this.ContainsLoggerListener = this.ContainsListener( ListenerType.Logger );
            }
        }

        /// <summary> Adds a range. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="items"> The items to add. </param>
        public void AddRange( IList<IMessageListener> items )
        {
            if ( items?.Any() == true )
            {
                foreach ( IMessageListener item in items )
                {
                    this.Add( item );
                }
            }
        }

        /// <summary> Adds a range. </summary>
        /// <remarks> David, 2020-09-14. </remarks>
        /// <param name="items"> The items to add. </param>
        public void AddRange( IList<ITraceMessageListener> items )
        {
            if ( items?.Any() == true )
            {
                foreach ( ITraceMessageListener item in items )
                {
                    this.Add( item );
                }
            }
        }

        /// <summary> Applies the trace level. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        public void ApplyTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            if ( this.ListenerDix.ContainsKey( listenerType ) )
            {
                foreach ( IMessageListener listener in this.ListenerDix[listenerType] )
                {
                    listener.ApplyTraceLevel( value );
                }
            }
        }
    }

    /// <summary> Values that represent listener types. </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    public enum ListenerType
    {

        /// <summary> An enum constant representing a logger type listener. </summary>
        Logger,

        /// <summary> An enum constant representing a display type listener. </summary>
        Display
    }
}
