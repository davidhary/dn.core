using System;
using System.Diagnostics;

namespace isr.Core
{
    /// <summary>
    /// Defines an event arguments class for <see cref="TraceMessage">trace messages</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class TraceMessageEventArgs : EventArgs
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="TraceMessageEventArgs" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="traceMessage"> A message describing the trace. </param>
        public TraceMessageEventArgs( TraceMessage traceMessage ) : base()
        {
            this.TraceMessage = traceMessage;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TraceMessageEventArgs" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="id">      The identifier. </param>
        /// <param name="details"> The details. </param>
        public TraceMessageEventArgs( int id, string details ) : this( TraceEventType.Information, id, details )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TraceMessageEventArgs" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="severity"> The severity. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="details">  The details. </param>
        public TraceMessageEventArgs( TraceEventType severity, int id, string details ) : this( new TraceMessage( severity, id, details ) )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TraceMessageEventArgs" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="severity"> The severity. </param>
        /// <param name="id">       The identifier. </param>
        /// <param name="format">   The format. </param>
        /// <param name="args">     The arguments for the format statement. </param>
        public TraceMessageEventArgs( TraceEventType severity, int id, string format, params object[] args ) : this( new TraceMessage( severity, id, format, args ) )
        {
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets a message describing the trace. </summary>
        /// <value> A message describing the trace. </value>
        public TraceMessage TraceMessage { get; set; }

        /// <summary> Gets the message severity. </summary>
        /// <value> The severity. </value>
        public TraceEventType Severity => this.TraceMessage.EventType;

        /// <summary> Gets the message details. </summary>
        /// <value> The details. </value>
        public string Details => this.TraceMessage.Details;

        #endregion

    }
}
