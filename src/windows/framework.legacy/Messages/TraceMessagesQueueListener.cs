using System;

namespace isr.Core
{
    /// <summary> A trace messages queue listener. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-23 </para>
    /// </remarks>
    public partial class TraceMessagesQueueListener : TraceMessagesQueue, IDisposable
    {

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public TraceMessagesQueueListener() : base()
        {
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Logger and optionally releases
        /// the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        ~TraceMessagesQueueListener()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }
    }
}
