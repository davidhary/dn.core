using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace isr.Core.TimeSpanExtensions
{
    /// <summary> Includes extensions for <see cref="TimeSpan"/> calculations. </summary>
    /// <remarks> Requires: DispatcherExtensions; Reference to Windows Base DLL. <para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class TimeSpanExtensionMethods
    {

        /// <summary>   Static constructor. </summary>
        /// <remarks>   David, 2021-02-10. </remarks>
        static TimeSpanExtensionMethods()
        {
            TimeSpanExtensionMethods.SystemClockResolution = TimeSpanExtensionMethods.EstimateSystemClockResolution( TimeSpanExtensionMethods.SystemClockResolution );
        }

        #region " EQUALS "

        /// <summary>
        /// A TimeSpan extension method that checks if the two timespan values are equal within
        /// <paramref name="epsilon"/>.
        /// </summary>
        /// <remarks>   David, 2020-11-25. </remarks>
        /// <param name="leftHand">     The leftHand to act on. </param>
        /// <param name="rightHand">    The right hand. </param>
        /// <param name="epsilon">      The epsilon. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool Approximates( this TimeSpan leftHand, TimeSpan rightHand, TimeSpan epsilon )
        {
            return Math.Abs( leftHand.Subtract( rightHand ).Ticks ) <= epsilon.Ticks;
        }

        #endregion

        #region " RESOLUTIONS "

        /// <summary> The system clock resolution. </summary>
        /// <value> The system clock resolution. </value>
        public static TimeSpan SystemClockResolution { get; private set; } = (1000d / 64).FromMilliseconds();

        /// <summary>   The thread clock resolution. </summary>
        /// <remarks>
        /// This might have changed from previous test results with the upgrade to Windows 20H2. Test
        /// results consistently show the thread sleep resolution at 15.6 ms.
        /// https://stackoverflow.com/questions/7614936/can-i-improve-the-resolution-of-thread-sleep The
        /// Thread.Sleep cannot be expected to provide reliable timing. It is notorious for behaving
        /// differently on different hardware Thread.Sleep(1) could sleep for 15.6 ms.
        /// https://social.msdn.microsoft.com/Forums/vstudio/en-US/facc2b57-9a27-4049-bb32-ef093fbf4c29/threadsleep1-sleeps-for-156-ms?forum=clr.
        /// </remarks>
        /// <value> The thread clock resolution. </value>
        public static TimeSpan ThreadClockResolution { get; private set; } = 15.6001d.FromMilliseconds();

        /// <summary> The high resolution clock resolution. </summary>
        /// <value> The high resolution clock resolution. </value>
        public static TimeSpan HighResolutionClockResolution { get; private set; } = (1d / Stopwatch.Frequency).FromSeconds();

        /// <summary>   Estimate system clock resolution. </summary>
        /// <remarks>   David, 2021-02-10. </remarks>
        /// <param name="guess">    The guess. </param>
        /// <returns>   A TimeSpan. </returns>
        private static TimeSpan EstimateSystemClockResolution( TimeSpan guess )
        {
            TimeSpan resolution = guess;
            for ( int i = 0; i < 3; i++ )
            {
                Stopwatch sw = Stopwatch.StartNew();
                Thread.Sleep( 1 );
                sw.Stop();
                if ( resolution < sw.Elapsed )
                    resolution = sw.Elapsed;
            }
            return resolution;
        }
        #endregion

        #region " EXACT TIMES "

        /// <summary> Gets or sets the microseconds per tick. </summary>
        /// <value> The microseconds per tick. </value>
        public static double MicrosecondsPerTick { get; private set; } = 1000000.0d / Stopwatch.Frequency;

        /// <summary> Gets or sets the millisecond per tick. </summary>
        /// <value> The millisecond per tick. </value>
        public static double MillisecondsPerTick { get; private set; } = 1000.0d / Stopwatch.Frequency;

        /// <summary> Gets or sets the seconds per tick. </summary>
        /// <value> The seconds per tick. </value>
        public static double SecondsPerTick { get; private set; } = 1.0d / TimeSpan.TicksPerSecond;

        /// <summary> Gets or sets the ticks per microseconds. </summary>
        /// <value> The ticks per microseconds. </value>
        public static double TicksPerMicroseconds { get; private set; } = 0.001d * TimeSpan.TicksPerMillisecond;

        /// <summary> Converts seconds to time span with tick timespan accuracy. </summary>
        /// <remarks>
        /// <code>
        /// Dim actualTimespan As TimeSpan = TimeSpan.Zero.FromSecondsPrecise(42.042)
        /// </code>
        /// </remarks>
        /// <param name="seconds"> The number of seconds. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan FromSeconds( this double seconds )
        {
            return TimeSpan.FromTicks( ( long ) (TimeSpan.TicksPerSecond * seconds) );
        }

        /// <summary> Converts a timespan to the seconds with tick timespan accuracy. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="timespan"> The timespan. </param>
        /// <returns> Timespan as a Double. </returns>
        public static double ToSeconds( this TimeSpan timespan )
        {
            return timespan.Ticks * SecondsPerTick;
        }

        /// <summary> Converts milliseconds to time span with tick timespan accuracy. </summary>
        /// <remarks>
        /// <code>
        /// Dim actualTimespan As TimeSpan = TimeSpan.Zero.FromMillisecondsPrecise(42.042)
        /// </code>
        /// </remarks>
        /// <param name="milliseconds"> The number of milliseconds. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan FromMilliseconds( this double milliseconds )
        {
            return TimeSpan.FromTicks( ( long ) (TimeSpan.TicksPerMillisecond * milliseconds) );
        }

        /// <summary> Converts a timespan to an exact milliseconds with tick timespan accuracy. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="timespan"> The timespan. </param>
        /// <returns> Timespan as a Double. </returns>
        public static double ToMilliseconds( this TimeSpan timespan )
        {
            return timespan.Ticks * MillisecondsPerTick;
        }

        /// <summary> Converts microseconds to time span with tick timespan accuracy. </summary>
        /// <remarks>
        /// <code>
        /// Dim actualTimespan As TimeSpan = TimeSpan.Zero.FromMicroseconds(42.2)
        /// </code>
        /// </remarks>
        /// <param name="microseconds"> The value. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan FromMicroseconds( this double microseconds )
        {
            return TimeSpan.FromTicks( ( long ) (TicksPerMicroseconds * microseconds) );
        }

        /// <summary> Converts a timespan to an exact microseconds with tick timespan accuracy. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="timespan"> The timespan. </param>
        /// <returns> Timespan as a Double. </returns>
        public static double ToMicroseconds( this TimeSpan timespan )
        {
            return timespan.Ticks * MicrosecondsPerTick;
        }

        /// <summary>
        /// A TimeSpan extension method that adds the microseconds to 'microseconds'.
        /// </summary>
        /// <remarks>   David, 2020-12-07. </remarks>
        /// <param name="self">         The self to act on. </param>
        /// <param name="microseconds"> The value. </param>
        /// <returns>   A TimeSpan. </returns>
        public static TimeSpan AddMicroseconds( this TimeSpan self, double microseconds )
        {
            return self.Add( TimeSpan.FromTicks( ( long ) (microseconds * TicksPerMicroseconds) ) );
        }

        #endregion

        #region " DELAY: ENCAPSULATION OF ASYNCHRONOUS WAIT  "

        /// <summary>
        /// Delays operations by the given delay time selecting the delay clock which resolution exceeds
        /// 0.2 times the delay time. T.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="delayMilliseconds"> The delay in milliseconds. </param>
        public static void Delay( double delayMilliseconds )
        {
            Delay( TimeSpanExtensions.TimeSpanExtensionMethods.FromMilliseconds( delayMilliseconds ) );
        }

        /// <summary>
        /// Delays operations by the given delay time on another thread.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="delayTime"> The delay time. </param>
        public static void Delay( TimeSpan delayTime )
        {
            TimeSpanExtensions.TimeSpanExtensionMethods.AsyncWait( delayTime );
        }

        #endregion

        #region " DO EVENTS WAITS: ENCAPSULATION OF ASYNCHRONOUS WAIT  "

        /// <summary>
        /// A TimeSpan extension method that Lets Windows process all the messages currently in the
        /// message queue during a wait by invoking the wait on a task. 
        /// </summary>
        /// <remarks>
        /// David, 2020-11-05. <para>
        /// DoEventsWait(1ms)  waits: 00:00:00.0010066s </para><para>
        /// DoEventsWait(2ms)  waits: 00:00:00.0020038s </para><para>
        /// DoEventsWait(5ms)  waits: 00:00:00.0050051s </para><para>
        /// DoEventsWait(10ms) waits: 00:00:00.0100138s </para><para>
        /// DoEventsWait(20ms) waits: 00:00:00.0200103s </para><para>
        /// DoEventsWait(50ms) waits: 00:00:00.0500064s </para><para>
        /// DoEventsWait(100ms) waits: 00:00:00.1000037s </para>
        /// </remarks>
        /// <param name="delayTime">    The delay time. </param>
        public static void DoEventsWait( this TimeSpan delayTime )
        {
            TimeSpanExtensionMethods.AsyncWait( delayTime );
        }

        /// <summary>
        /// Lets Windows process all the messages currently in the message queue during a wait.
        /// </summary>
        /// <remarks>   David, 2020-11-20. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        /// <returns>   A TimeSpan. </returns>
        public static TimeSpan DoEventsWaitElapsed( TimeSpan delayTime )
        {
            Stopwatch sw = Stopwatch.StartNew();
            TimeSpanExtensions.TimeSpanExtensionMethods.DoEventsWait( delayTime );
            return sw.Elapsed;
        }

        /// <summary>
        /// Lets Windows process all the messages currently in the message queue during a wait after each
        /// sleep interval.
        /// </summary>
        /// <remarks>   David, 2020-11-20. </remarks>
        /// <param name="sleepInterval">    The sleep interval. </param>
        /// <param name="timeout">          The maximum wait time. </param>
        /// <returns>   A TimeSpan. </returns>
        public static TimeSpan DoEventsWaitElapsed( TimeSpan sleepInterval, TimeSpan timeout )
        {

            Stopwatch sw = Stopwatch.StartNew();
            while ( sw.Elapsed <= timeout )
            {
                TimeSpanExtensionMethods.Delay( sleepInterval );
            }
            return sw.Elapsed;

        }

        /// <summary>
        /// Lets Windows process all the messages currently in the message queue during a wait for
        /// a timeout or an affirmative result.
        /// </summary>
        /// <remarks>   David, 2020-11-14. </remarks>
        /// <param name="timeout">      The maximum wait time. </param>
        /// <param name="predicate">    A predicate function returning true upon affirmation of a
        ///                             condition. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static (bool Success, TimeSpan Elapse) DoEventsWaitUntil( TimeSpan timeout, Func<bool> predicate )
        {
            return TimeSpanExtensionMethods.DoEventsLetElapseUntil( Stopwatch.StartNew(), timeout, predicate );
        }

        /// <summary>
        /// Lets Windows process all the messages currently in the message queue during a wait for
        /// a timeout or an affirmative result.
        /// </summary>
        /// <remarks>   David, 2020-11-14. </remarks>
        /// <param name="stopwatch">    The stopwatch. </param>
        /// <param name="timeout">      The maximum wait time. </param>
        /// <param name="predicate">    A predicate function returning true upon affirmation of a
        ///                             condition. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static (bool Success, TimeSpan Elapse) DoEventsLetElapseUntil( Stopwatch stopwatch, TimeSpan timeout, Func<bool> predicate )
        {
            return isr.Core.TimeSpanExtensions.TimeSpanExtensionMethods.AsyncLetElapseUntil( stopwatch, timeout, TimeSpan.Zero, predicate );
        }

        /// <summary>
        /// Lets Windows process all the messages currently in the message queue during a wait for
        /// a timeout or an affirmative result polling every poll interval.
        /// </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="pollInterval"> The time between <paramref name="predicate"/> actions. </param>
        /// <param name="timeout">      The maximum wait time. </param>
        /// <param name="predicate">    A predicate function returning true upon affirmation of a
        ///                             condition. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static (bool Success, TimeSpan Elapsed) DoEventsWaitUntil( TimeSpan pollInterval, TimeSpan timeout, Func<bool> predicate )
        {
            return TimeSpanExtensionMethods.DoEventsLetElapseUntil( Stopwatch.StartNew(), pollInterval, timeout, predicate );
        }

        /// <summary>
        /// Lets Windows process all the messages currently in the message queue during a wait for
        /// a timeout or an affirmative result polling every poll interval.
        /// </summary>
        /// <remarks>   David, 2020-11-16. </remarks>
        /// <param name="stopwatch">    The stopwatch. </param>
        /// <param name="pollInterval"> The time between <paramref name="predicate"/> actions. </param>
        /// <param name="timeout">      The maximum wait time. </param>
        /// <param name="predicate">    A predicate function returning true upon affirmation of a
        ///                             condition. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static (bool Success, TimeSpan Elapse) DoEventsLetElapseUntil( Stopwatch stopwatch, TimeSpan pollInterval, TimeSpan timeout, Func<bool> predicate )
        {
            return isr.Core.TimeSpanExtensions.TimeSpanExtensionMethods.AsyncLetElapseUntil( stopwatch, timeout, pollInterval, predicate );
        }

        #endregion

        #region " SYNCHRONOUS AND ASYNCHRONOUS WAITS (DELAYS) "

        /// <summary>
        /// A TimeSpan extension method that synchronously waits for a specific time delay that is
        /// accurate up to the high resolution clock resolution.
        /// </summary>
        /// <remarks>   David, 2021-01-30. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        /// <param name="yield">        (Optional) True to yield between <see cref="Thread.Sleep(int)"/>
        ///                             and <see cref="Thread.SpinWait(int)"/>. </param>
        /// <returns>   The actual wait time. </returns>
        public static TimeSpan SyncWait( this TimeSpan delayTime, bool yield = false )
        {
            Stopwatch sw = Stopwatch.StartNew();
            var yieldCount = 100;
            var counter = yieldCount;
            int systemClockCycles = ( int ) Math.Floor( delayTime / TimeSpanExtensionMethods.SystemClockResolution );
            if ( systemClockCycles > 0 )
            {
                if ( yield )
                {
                    while ( delayTime.Subtract( sw.Elapsed ) < TimeSpanExtensionMethods.SystemClockResolution )
                    {
                        var ms = ( int ) Math.Floor( TimeSpanExtensionMethods.SystemClockResolution.TotalMilliseconds );
                        Thread.Sleep( ms );
                        _ = Thread.Yield();
                    }
                }
                else
                {
                    Thread.Sleep( delayTime );
                    _ = Thread.Yield();
                }
            }
            while ( sw.Elapsed < delayTime )
            {
                if ( counter >= yieldCount )
                {
                    Thread.SpinWait( 1 );
                    if ( yield )
                    {
                        _ = Thread.Yield();
                    }
                    counter = 0;
                }
                counter += 1;
            }
            return sw.Elapsed;
        }

        /// <summary>   A Stopwatch extension method that synchronizes the let elapse. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        /// <param name="stopwatch">    The stopwatch to act on. </param>
        /// <param name="timeout">      The timeout to act on. </param>
        /// <returns>   A TimeSpan. </returns>
        public static TimeSpan SyncLetElapse( this Stopwatch stopwatch, TimeSpan timeout )
        {
            while ( stopwatch.Elapsed <= timeout )
            {
            }
            return stopwatch.Elapsed;
        }


        /// <summary>   A TimeSpan extension method that synchronizes the wait until. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        /// <param name="timeout">      The timeout to act on. </param>
        /// <param name="pollInterval"> The time between <paramref name="predicate"/> actions. </param>
        /// <param name="predicate">    The predicate. </param>
        /// <returns>   A (bool Success,TimeSpan Elapsed) </returns>
        public static (bool Success, TimeSpan Elapsed) SyncWaitUntil( this TimeSpan timeout, TimeSpan pollInterval, Func<bool> predicate )
        {
            return Stopwatch.StartNew().SyncLetElapseUntil( timeout, pollInterval, predicate );
        }

        /// <summary>   A Stopwatch extension method that synchronizes the let elapse until. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        /// <param name="stopwatch">    The stopwatch to act on. </param>
        /// <param name="timeout">      The timeout to act on. </param>
        /// <param name="pollInterval"> The time between <paramref name="predicate"/> actions. </param>
        /// <param name="predicate">    The predicate. </param>
        /// <returns>   A (bool Success,TimeSpan Elapsed) </returns>
        public static (bool Success, TimeSpan Elapsed) SyncLetElapseUntil( this Stopwatch stopwatch, TimeSpan timeout, TimeSpan pollInterval, Func<bool> predicate )
        {
            while ( stopwatch.Elapsed <= timeout && !predicate() )
            {
                if ( pollInterval > TimeSpan.Zero )
                    _ = TimeSpanExtensionMethods.SyncWait( pollInterval );
            }
            return (stopwatch.Elapsed < timeout, stopwatch.Elapsed);
        }

        /// <summary>   A TimeSpan extension method that starts a wait task. </summary>
        /// <remarks>   David, 2021-01-30. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        /// <param name="yield">        (Optional) True to yield between spin waits. </param>
        /// <returns>   A Task. </returns>
        public static Task StartWaitTask( this TimeSpan delayTime, bool yield = false )
        {
            return Task.Factory.StartNew( () => SyncWait( delayTime, yield ) );
        }

        /// <summary>   A TimeSpan extension method that asynchronously waits for a specific time delay that is
        /// accurate up to the high resolution clock resolution. </summary>
        /// <remarks>   David, 2021-01-30. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        /// <param name="yield">        (Optional) True to yield between spin waits. </param>
        public static void AsyncWait( this TimeSpan delayTime, bool yield = false )
        {
            StartWaitTask( delayTime, yield ).Wait();
        }

        /// <summary>   A TimeSpan extension method that starts wait task returning the elapsed time. </summary>
        /// <remarks>   David, 2021-01-30. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        /// <param name="yield">        (Optional) True to yield between spin waits. </param>
        /// <returns>   A TimeSpan Task. </returns>
        public static Task<TimeSpan> StartWaitElpasedTask( this TimeSpan delayTime, bool yield = false )
        {
            return Task<TimeSpan>.Factory.StartNew( () => { return SyncWait( delayTime, yield ); } );
        }

        /// <summary>
        /// A TimeSpan extension method that asynchronously waits for a specific time delay that is
        /// accurate up to the high resolution clock resolution and returning the elapsed time.
        /// </summary>
        /// <remarks>   David, 2021-01-30. </remarks>
        /// <param name="delayTime">    The delay time. </param>
        /// <param name="yield">        (Optional) True to yield between spin waits. </param>
        /// <returns>   A TimeSpan. </returns>
        public static TimeSpan AsyncWaitElapsed( this TimeSpan delayTime, bool yield = false )
        {
            System.Threading.Tasks.Task<TimeSpan> t = StartWaitElpasedTask( delayTime, yield );
            t.Wait();
            return t.Result;
        }

        /// <summary>   A TimeSpan extension method that starts wait until. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        /// <param name="timeout">      The timeout to act on. </param>
        /// <param name="pollInterval"> The time between <paramref name="predicate"/> actions. </param>
        /// <param name="predicate">    The predicate. </param>
        /// <returns>   A (bool Success,TimeSpan Elapsed) </returns>
        public static Task<(bool Success, TimeSpan Elapsed)> StartWaitUntil( this TimeSpan timeout, TimeSpan pollInterval, Func<bool> predicate )
        {
            return Task<(bool Success, TimeSpan Elapsed)>.Factory.StartNew( () => { return SyncWaitUntil( timeout, pollInterval, predicate ); } );
        }

        /// <summary>   A TimeSpan extension method that asynchronous wait until. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        /// <param name="timeout">      The timeout to act on. </param>
        /// <param name="pollInterval"> The time between <paramref name="predicate"/> actions. </param>
        /// <param name="predicate">    The predicate. </param>
        /// <returns>   A (bool Success,TimeSpan Elapsed) </returns>
        public static (bool Success, TimeSpan Elapsed) AsyncWaitUntil( this TimeSpan timeout, TimeSpan pollInterval, Func<bool> predicate )
        {
            System.Threading.Tasks.Task<(bool Success, TimeSpan Elapsed)> t = StartWaitUntil( timeout, pollInterval, predicate );
            t.Wait();
            return t.Result;
        }

        /// <summary>   A Stopwatch extension method that starts let elapse until. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        /// <param name="stopwatch">    The stopwatch to act on. </param>
        /// <param name="timeout">      The timeout to act on. </param>
        /// <param name="pollInterval"> The time between <paramref name="predicate"/> actions. </param>
        /// <param name="predicate">    The predicate. </param>
        /// <returns>   A (bool Success,TimeSpan Elapsed) </returns>
        public static Task<(bool Success, TimeSpan Elapsed)> StartLetElapseUntil( this Stopwatch stopwatch, TimeSpan timeout, TimeSpan pollInterval, Func<bool> predicate )
        {
            return Task<(bool Success, TimeSpan Elapsed)>.Factory.StartNew( () => { return SyncLetElapseUntil( stopwatch, timeout, pollInterval, predicate ); } );
        }

        /// <summary>   A Stopwatch extension method that asynchronous let elapse until. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        /// <param name="stopwatch">    The stopwatch to act on. </param>
        /// <param name="timeout">      The timeout to act on. </param>
        /// <param name="pollInterval"> The time between <paramref name="predicate"/> actions. </param>
        /// <param name="predicate">    The predicate. </param>
        /// <returns>   A Tuple. </returns>
        public static (bool Success, TimeSpan Elapsed) AsyncLetElapseUntil( this Stopwatch stopwatch, TimeSpan timeout, TimeSpan pollInterval, Func<bool> predicate )
        {
            System.Threading.Tasks.Task<(bool Success, TimeSpan Elapsed)> t = StartLetElapseUntil( stopwatch, timeout, pollInterval, predicate );
            t.Wait();
            return t.Result;
        }

        /// <summary>   A Stopwatch extension method that starts let elapse. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        /// <param name="stopwatch">    The stopwatch to act on. </param>
        /// <param name="timeout">      The timeout to act on. </param>
        /// <returns>   A TimeSpan </returns>
        public static Task<TimeSpan> StartLetElapse( this Stopwatch stopwatch, TimeSpan timeout )
        {
            return Task<TimeSpan>.Factory.StartNew( () => { return SyncLetElapse( stopwatch, timeout ); } );
        }

        /// <summary>   A Stopwatch extension method that asynchronous let elapse. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        /// <param name="stopwatch">    The stopwatch to act on. </param>
        /// <param name="timeout">      The timeout to act on. </param>
        /// <returns>   A Tuple. </returns>
        public static TimeSpan AsyncLetElapse( this Stopwatch stopwatch, TimeSpan timeout )
        {
            System.Threading.Tasks.Task<TimeSpan> t = StartLetElapse( stopwatch, timeout );
            t.Wait();
            return t.Result;
        }


        #endregion

    }
}
