using System;

namespace isr.Core.Assembly
{
    /// <summary>   Manager for application domains. </summary>
    /// <remarks>   David, 2021-02-16. </remarks>
    internal class AppDomainManager
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-02-11. </remarks>
        /// <param name="propertyName"> Name of the property. </param>
        public AppDomainManager( string propertyName )
        {
            this.AppDomainProperty = propertyName;
        }

        /// <summary>   Gets or sets the application domain property. </summary>
        /// <value> The application domain property. </value>
        public string AppDomainProperty { get; }

        /// <summary> The name of the data directory application domain property used by MS Test. </summary>
        public const string DirectoryPropertyName = "DataDirectory";

        /// <summary> Modify application domain data directory path. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public void ModifyApplicationDomainDataDirectoryPath()
        {
            this.ModifyApplicationDomainDataDirectoryPath( System.IO.Path.GetDirectoryName( System.Reflection.Assembly.GetExecutingAssembly().Location ) );
        }

        /// <summary> Modify application domain data directory path. </summary>
        /// <remarks>
        /// https://stackoverflow.com/questions/1833640/connection-string-with-relative-path-to-the-database-file.
        /// </remarks>
        /// <param name="path"> Full pathname of the file. </param>
        public void ModifyApplicationDomainDataDirectoryPath( string path )
        {
            AppDomain.CurrentDomain.SetData( this.AppDomainProperty, path );
        }

        /// <summary> Reads application domain data directory path. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <returns> The application domain data directory path. </returns>
        public string ReadApplicationDomainDataDirectoryPath()
        {
            string value = AppDomain.CurrentDomain.GetData( this.AppDomainProperty ) as string;
            return value ?? string.Empty;
        }

    }
}
