using System;
using System.Diagnostics;

namespace isr.Core.AssemblyInfoExtensions
{
    /// <summary> Includes extensions for <see cref="T:System.Reflection.Assembly">Assembly</see>
    /// and <see cref="T:System.Reflection.Assembly">assembly info</see>. </summary>
    /// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para> </remarks>
    [System.CLSCompliant( false )]
    public static class AssemblyInfoExtensionMethods
    {

        /// <summary> Gets the assembly. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <returns> The application <see cref="T:System.Reflection.Assembly">Assembly</see>. </returns>
        [System.CLSCompliant( false )]
        public static System.Reflection.Assembly GetAssembly( this Microsoft.VisualBasic.ApplicationServices.AssemblyInfo assemblyInfo )
        {
            if ( assemblyInfo is null )
            {
                throw new ArgumentNullException( nameof( assemblyInfo ) );
            }

            System.Reflection.Assembly value = null;
            foreach ( System.Reflection.Assembly a in assemblyInfo.LoadedAssemblies )
            {
                if ( a.GetName().Name.Equals( assemblyInfo.AssemblyName ) )
                {
                    value = a;
                    break;
                }
            }

            return value;
        }

        /// <summary> Gets the folder where application files are located. </summary>
        /// <remarks>
        /// <examples>
        /// <code>
        /// Dim folder As String = My.Application.Info.ApplicationFolder()
        /// </code></examples>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <returns> The application folder path. </returns>
        public static string ApplicationFolder( this Microsoft.VisualBasic.ApplicationServices.AssemblyInfo assemblyInfo )
        {
            return assemblyInfo is null ? throw new ArgumentNullException( nameof( assemblyInfo ) ) : assemblyInfo.DirectoryPath;
        }

        /// <summary> The default version format. </summary>
        public const string DefaultVersionFormat = "{0}.{1:00}.{2:0000}";

        /// <summary> Gets the product version. </summary>
        /// <remarks>
        /// <examples>
        /// <code>
        /// Dim version As String = My.Application.Info.ProducVersion("")
        /// </code></examples>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <param name="format">       The version format. Set to empty to use the
        /// <see cref="DefaultVersionFormat">default version format</see>.
        /// </param>
        /// <returns> The application version. </returns>
        public static string ProductVersion( this Microsoft.VisualBasic.ApplicationServices.AssemblyInfo assemblyInfo, string format )
        {
            if ( assemblyInfo is null )
            {
                throw new ArgumentNullException( nameof( assemblyInfo ) );
            }

            if ( string.IsNullOrWhiteSpace( format ) )
            {
                format = DefaultVersionFormat;
            }

            return string.Format( System.Globalization.CultureInfo.CurrentCulture, format, assemblyInfo.Version.Major, assemblyInfo.Version.Minor, assemblyInfo.Version.Build );
        }

        #region " PRODUCT PROCESS CAPTION "

        /// <summary> Prefix process name to the product name. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <returns> A String. </returns>
        public static string PrefixProcessName( this Microsoft.VisualBasic.ApplicationServices.AssemblyInfo assemblyInfo )
        {
            if ( assemblyInfo is null )
            {
                throw new ArgumentNullException( nameof( assemblyInfo ) );
            }

            string name = assemblyInfo.ProductName;
            string processName = Process.GetCurrentProcess().ProcessName;
            if ( !name.StartsWith( processName, StringComparison.OrdinalIgnoreCase ) )
            {
                name = $"{processName}.{name}";
            }

            return name;
        }

        /// <summary> Builds product time caption. </summary>
        /// <remarks>
        /// <list type="bullet">Use the following format options: <item>
        /// u - UTC - 2019-09-10 19:27:04Z</item><item>
        /// r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
        /// o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
        /// s - ISO - 2019-09-10T12:24:47</item><item>
        /// empty   - 2019-09-10 16:57:24 -07:00</item><item>
        /// s + zzz - 2019-09-10T12:24:47-07:00</item></list>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo">      Information describing the assembly. </param>
        /// <param name="versionElements">   The version elements. </param>
        /// <param name="timeCaptionFormat"> The time caption format. </param>
        /// <param name="kindFormat">        The kind format. </param>
        /// <returns> A String. </returns>
        public static string BuildProductTimeCaption( this Microsoft.VisualBasic.ApplicationServices.AssemblyInfo assemblyInfo, int versionElements, string timeCaptionFormat, string kindFormat )
        {
            return assemblyInfo is null
                ? throw new ArgumentNullException( nameof( assemblyInfo ) )
                : $"{assemblyInfo.PrefixProcessName()}.r.{assemblyInfo.Version.ToString( versionElements )} {MyAssemblyInfo.BuildLocalTimeCaption( timeCaptionFormat, kindFormat )}";
        }

        /// <summary>
        /// Builds product time caption using full version and local time plus kind format.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <returns> A String. </returns>
        public static string BuildProductTimeCaption( this Microsoft.VisualBasic.ApplicationServices.AssemblyInfo assemblyInfo )
        {
            return assemblyInfo.BuildProductTimeCaption( 4, string.Empty, string.Empty );
        }

        #endregion

    }
}
