using System;
using System.Linq;

namespace isr.Core
{
    /// <summary> A sealed class designed to provide application log access to the library. </summary>
    /// <remarks>
    /// (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2011-02-02, x.x.4050 </para>
    /// </remarks>
    public sealed class EmbeddedResourceManager
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="EmbeddedResourceManager" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        private EmbeddedResourceManager() : base()
        {
        }

        #endregion

        #region " EMBEDDED RESOURCES  "

        /// <summary> Builds full resource name. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="assembly">     The assembly. </param>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> The full resource name, which starts with the Assembly name. </returns>
        public static string BuildFullResourceName( System.Reflection.Assembly assembly, string resourceName )
        {
            if ( string.IsNullOrWhiteSpace( resourceName ) )
            {
                return string.Empty;
            }

            if ( assembly is null )
            {
                assembly = System.Reflection.Assembly.GetExecutingAssembly();
            }

            string assemblyName = assembly.GetName().Name;
            return resourceName.StartsWith( assemblyName, StringComparison.OrdinalIgnoreCase ) ? resourceName : $"{assembly.GetName().Name}.{resourceName}";
        }

        /// <summary> Builds full resource name. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> The full resource name, which starts with the Assembly name. </returns>
        public static string BuildFullResourceName( string resourceName )
        {
            return BuildFullResourceName( System.Reflection.Assembly.GetExecutingAssembly(), resourceName );
        }

        /// <summary> Queries if a given embedded resource exists. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="assembly">     The assembly. </param>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> <c>True</c> if the resource exists; otherwise, <c>False</c>. </returns>
        public static bool EmbeddedResourceExists( System.Reflection.Assembly assembly, string resourceName )
        {
            if ( assembly is null )
            {
                assembly = System.Reflection.Assembly.GetExecutingAssembly();
            }
            // Retrieve a list of resource names contained by the assembly.
            var resourceNames = assembly.GetManifestResourceNames();
            return resourceNames.Contains( BuildFullResourceName( assembly, resourceName ), StringComparer.OrdinalIgnoreCase );
        }

        /// <summary> Queries if a given embedded resource exists. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> <c>True</c> if the resource exists; otherwise, <c>False</c>. </returns>
        public static bool EmbeddedResourceExists( string resourceName )
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            return EmbeddedResourceExists( assembly, BuildFullResourceName( assembly, resourceName ) );
        }

        /// <summary> Read text from an embedded resource file. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> The embedded text resource. </returns>
        public static string ReadEmbeddedTextResource( string resourceName )
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            string contents = string.Empty;
            using ( var resourceStream = assembly.GetManifestResourceStream( BuildFullResourceName( assembly, resourceName ) ) )
            {
                using var sr = new System.IO.StreamReader( resourceStream );
                contents = sr.ReadToEnd();
            }

            return contents;
        }

        /// <summary> Read text from an embedded resource file. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> The embedded image resource. </returns>
        public static System.Drawing.Image ReadEmbeddedImageResource( string resourceName )
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            using var resourceStream = assembly.GetManifestResourceStream( BuildFullResourceName( assembly, resourceName ) );
            return System.Drawing.Image.FromStream( resourceStream );
        }

        /// <summary>
        /// Tries reading text from an embedded resource file. Returns empty if not found.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assembly">     The assembly. </param>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> The embedded text resource. </returns>
        public static string TryReadEmbeddedTextResource( System.Reflection.Assembly assembly, string resourceName )
        {
            if ( assembly is null )
            {
                throw new ArgumentNullException( nameof( assembly ) );
            }

            string contents = string.Empty;
            using ( var resourceStream = assembly.GetManifestResourceStream( BuildFullResourceName( assembly, resourceName ) ) )
            {
                if ( resourceStream is object )
                {
                    using var sr = new System.IO.StreamReader( resourceStream );
                    contents = sr.ReadToEnd();
                }
            }

            return contents;
        }

        /// <summary> Tries to read embedded image resource. Returns nothing if not found. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assembly">     The assembly. </param>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> A System.Drawing.Image. </returns>
        public static System.Drawing.Image TryReadEmbeddedImageResource( System.Reflection.Assembly assembly, string resourceName )
        {
            if ( assembly is null )
            {
                throw new ArgumentNullException( nameof( assembly ) );
            }

            using var resourceStream = assembly.GetManifestResourceStream( BuildFullResourceName( assembly, resourceName ) );
            return resourceStream is null ? null : System.Drawing.Image.FromStream( resourceStream );
        }

        /// <summary> Try read embedded GIF resource. Returns nothing if not found. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assembly">     The assembly. </param>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> A System.Drawing.Image. </returns>
        public static System.Drawing.Image TryReadEmbeddedGifResource( System.Reflection.Assembly assembly, string resourceName )
        {
            if ( assembly is null )
            {
                throw new ArgumentNullException( nameof( assembly ) );
            }

            var resourceStream = assembly.GetManifestResourceStream( BuildFullResourceName( assembly, resourceName ) );
            return resourceStream is null ? null : System.Drawing.Image.FromStream( resourceStream );
        }

        /// <summary> Try read embedded icon resource. Returns nothing if not found. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assembly">     The assembly. </param>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> A System.Drawing.Icon. </returns>
        public static System.Drawing.Icon TryReadEmbeddedIconResource( System.Reflection.Assembly assembly, string resourceName )
        {
            if ( assembly is null )
            {
                throw new ArgumentNullException( nameof( assembly ) );
            }

            using var resourceStream = assembly.GetManifestResourceStream( BuildFullResourceName( assembly, resourceName ) );
            return resourceStream is null ? null : new System.Drawing.Icon( resourceStream );
        }

        #endregion

    }
}
