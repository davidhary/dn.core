using System;

// in project file: [assembly: AssemblyDescription( isr.Core.My.MyLibrary.AssemblyDescription )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( isr.Core.My.MyLibrary.TestAssemblyStrongName )]
