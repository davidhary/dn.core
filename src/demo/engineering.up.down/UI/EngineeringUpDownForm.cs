using System;
using System.Collections.Generic;

using FastEnums;

namespace isr.Core.Testers
{

    /// <summary>   Form for viewing the engineering up down. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-04-05 </para>
    /// </remarks>
    public partial class EngineeringUpDownForm
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public EngineeringUpDownForm()
        {
            this.InitializeComponent();
        }

        /// <summary>   Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="e">    An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            try
            {
                this._EngineeringScaleComboBox.DataSource = null;
                this._EngineeringScaleComboBox.Items.Clear();
                this._EngineeringScaleComboBox.DataSource = typeof( isr.Core.WinControls.EngineeringScale ).ValueDescriptionPairs().ToBindingList();
                this._EngineeringScaleComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
                this._EngineeringScaleComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            }
            catch
            {
                throw;
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        /// <summary>   Enter value numeric value changed. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void EnterValueNumeric_ValueChanged( object sender, EventArgs e )
        {
            this._EditedEngineeringUpDown.ScaledValue = this._EnterValueNumeric.Value;
            this._NumericUpDown.Value = this._EnterValueNumeric.Value;
        }

        /// <summary>   Engineering scale combo box selection change committed. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void EngineeringScaleComboBox_SelectionChangeCommitted( object sender, EventArgs e )
        {
            this._EditedEngineeringUpDown.EngineeringScale = ( isr.Core.WinControls.EngineeringScale ) Convert.ToInt32( (( KeyValuePair<Enum, string> ) this._EngineeringScaleComboBox.SelectedItem).Key );
        }

        /// <summary>   Edited engineering up down value changed. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void EditedEngineeringUpDown_ValueChanged( object sender, EventArgs e )
        {
            this._ScaledValueTextBox.Text = this._EditedEngineeringUpDown.ScaledValue.ToString( "0.#########E-00 " + this._UnitTextBox.Text );
        }

        /// <summary>   Decimal places numeric value changed. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void DecimalPlacesNumeric_ValueChanged( object sender, EventArgs e )
        {
            this._EditedEngineeringUpDown.DecimalPlaces = ( int ) Math.Round( this._DecimalPlacesNumeric.Value );
        }

        /// <summary>   Unit text box text changed. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void UnitTextBox_TextChanged( object sender, EventArgs e )
        {
            this._EditedEngineeringUpDown.Unit = this._UnitTextBox.Text;
        }

        /// <summary>   Reads only check box checked changed. </summary>
        /// <remarks>   David, 2020-10-25. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void ReadOnlyCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this._EditedEngineeringUpDown.ReadOnly = this._ReadOnlyCheckBox.Checked;
        }

    }
}
