using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Testers
{
    [DesignerGenerated()]
    public partial class EngineeringUpDownForm : Form
    {

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _DecimalPlacesNumericLabel = new Label();
            _DecimalPlacesNumeric = new NumericUpDown();
            _DecimalPlacesNumeric.ValueChanged += new EventHandler(DecimalPlacesNumeric_ValueChanged);
            _EngineeringScaleComboBox = new ComboBox();
            _EngineeringScaleComboBox.SelectionChangeCommitted += new EventHandler(EngineeringScaleComboBox_SelectionChangeCommitted);
            _EngineeringScaleComboBoxLabel = new Label();
            _UnitTextBoxLabel = new Label();
            _UnitTextBox = new TextBox();
            _UnitTextBox.TextChanged += new EventHandler(UnitTextBox_TextChanged);
            _ToolTip = new ToolTip(components);
            _ScaledValueTextBox = new TextBox();
            _EnterValueNumeric = new isr.Core.WinControls.NumericUpDown();
            _EnterValueNumeric.ValueChanged += new EventHandler(EnterValueNumeric_ValueChanged);
            _ReadOnlyCheckBox = new CheckBox();
            _ReadOnlyCheckBox.CheckedChanged += new EventHandler(ReadOnlyCheckBox_CheckedChanged);
            _EditedEngineeringUpDown = new isr.Core.WinControls.EngineeringUpDown();
            _EditedEngineeringUpDown.ValueChanged += new EventHandler(EditedEngineeringUpDown_ValueChanged);
            _EnterValueNumericLabel = new Label();
            _EditedEngineeringUpDownLabel = new Label();
            _ScaledValueTextBoxLabel = new Label();
            _NumericUpDown = new isr.Core.WinControls.NumericUpDown();
            _NumericUpDown.ValueChanged += new EventHandler(EnterValueNumeric_ValueChanged);
            ((System.ComponentModel.ISupportInitialize)_DecimalPlacesNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_EnterValueNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_EditedEngineeringUpDown).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_NumericUpDown).BeginInit();
            SuspendLayout();
            // 
            // _DecimalPlacesNumericLabel
            // 
            _DecimalPlacesNumericLabel.AutoSize = true;
            _DecimalPlacesNumericLabel.Location = new Point(48, 191);
            _DecimalPlacesNumericLabel.Name = "_DecimalPlacesNumericLabel";
            _DecimalPlacesNumericLabel.Size = new Size(97, 17);
            _DecimalPlacesNumericLabel.TabIndex = 1;
            _DecimalPlacesNumericLabel.Text = "Decimal Places:";
            _DecimalPlacesNumericLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _DecimalPlacesNumeric
            // 
            _DecimalPlacesNumeric.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _DecimalPlacesNumeric.Location = new Point(146, 188);
            _DecimalPlacesNumeric.Maximum = new decimal(new int[] { 6, 0, 0, 0 });
            _DecimalPlacesNumeric.Name = "_DecimalPlacesNumeric";
            _DecimalPlacesNumeric.Size = new Size(58, 25);
            _DecimalPlacesNumeric.TabIndex = 2;
            _ToolTip.SetToolTip(_DecimalPlacesNumeric, "Enter decimal places");
            // 
            // _EngineeringScaleComboBox
            // 
            _EngineeringScaleComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _EngineeringScaleComboBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _EngineeringScaleComboBox.FormattingEnabled = true;
            _EngineeringScaleComboBox.Location = new Point(147, 150);
            _EngineeringScaleComboBox.Name = "_EngineeringScaleComboBox";
            _EngineeringScaleComboBox.Size = new Size(121, 25);
            _EngineeringScaleComboBox.TabIndex = 3;
            _ToolTip.SetToolTip(_EngineeringScaleComboBox, "Select engineering scale");
            // 
            // _EngineeringScaleComboBoxLabel
            // 
            _EngineeringScaleComboBoxLabel.AutoSize = true;
            _EngineeringScaleComboBoxLabel.Location = new Point(31, 153);
            _EngineeringScaleComboBoxLabel.Name = "_EngineeringScaleComboBoxLabel";
            _EngineeringScaleComboBoxLabel.Size = new Size(114, 17);
            _EngineeringScaleComboBoxLabel.TabIndex = 1;
            _EngineeringScaleComboBoxLabel.Text = "Engineering Scale:";
            _EngineeringScaleComboBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _UnitTextBoxLabel
            // 
            _UnitTextBoxLabel.AutoSize = true;
            _UnitTextBoxLabel.Location = new Point(111, 118);
            _UnitTextBoxLabel.Name = "_UnitTextBoxLabel";
            _UnitTextBoxLabel.Size = new Size(34, 17);
            _UnitTextBoxLabel.TabIndex = 1;
            _UnitTextBoxLabel.Text = "Unit:";
            _UnitTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _UnitTextBox
            // 
            _UnitTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _UnitTextBox.Location = new Point(147, 114);
            _UnitTextBox.Name = "_UnitTextBox";
            _UnitTextBox.Size = new Size(58, 25);
            _UnitTextBox.TabIndex = 5;
            _ToolTip.SetToolTip(_UnitTextBox, "Enter the unit, e.g., A, V, Ohm.");
            // 
            // _ScaledValueTextBox
            // 
            _ScaledValueTextBox.Location = new Point(147, 73);
            _ScaledValueTextBox.Name = "_ScaledValueTextBox";
            _ScaledValueTextBox.Size = new Size(121, 25);
            _ScaledValueTextBox.TabIndex = 4;
            _ToolTip.SetToolTip(_ScaledValueTextBox, "Scaled value");
            // 
            // _EnterValueNumeric
            // 
            _EnterValueNumeric.Location = new Point(148, 12);
            _EnterValueNumeric.Maximum = new decimal(new int[] { 2000000000, 0, 0, 0 });
            _EnterValueNumeric.Minimum = new decimal(new int[] { 2000000000, 0, 0, (int)-2147483648L });
            _EnterValueNumeric.Name = "_EnterValueNumeric";
            _EnterValueNumeric.ReadOnlyBackColor = Color.Empty;
            _EnterValueNumeric.ReadOnlyForeColor = Color.Empty;
            _EnterValueNumeric.ReadWriteBackColor = Color.Empty;
            _EnterValueNumeric.ReadWriteForeColor = Color.Empty;
            _EnterValueNumeric.Size = new Size(120, 25);
            _EnterValueNumeric.TabIndex = 9;
            _ToolTip.SetToolTip(_EnterValueNumeric, "Set this value");
            // 
            // _ReadOnlyCheckBox
            // 
            _ReadOnlyCheckBox.AutoSize = true;
            _ReadOnlyCheckBox.Location = new Point(146, 228);
            _ReadOnlyCheckBox.Name = "_ReadOnlyCheckBox";
            _ReadOnlyCheckBox.Size = new Size(87, 21);
            _ReadOnlyCheckBox.TabIndex = 8;
            _ReadOnlyCheckBox.Text = "Read Only";
            _ReadOnlyCheckBox.UseVisualStyleBackColor = true;
            // 
            // _EditedEngineeringUpDown
            // 
            _EditedEngineeringUpDown.ForeColor = Color.Black;
            _EditedEngineeringUpDown.Location = new Point(16, 73);
            _EditedEngineeringUpDown.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
            _EditedEngineeringUpDown.Minimum = new decimal(new int[] { 1000, 0, 0, (int)-2147483648L });
            _EditedEngineeringUpDown.Name = "_EditedEngineeringUpDown";
            _EditedEngineeringUpDown.ReadOnlyBackColor = Color.FromArgb(224, 224, 224);
            _EditedEngineeringUpDown.ReadOnlyForeColor = Color.Black;
            _EditedEngineeringUpDown.ReadWriteBackColor = Color.Empty;
            _EditedEngineeringUpDown.ReadWriteForeColor = Color.Black;
            _EditedEngineeringUpDown.ScaledValue = new decimal(new int[] { 0, 0, 0, 0 });
            _EditedEngineeringUpDown.Size = new Size(120, 25);
            _EditedEngineeringUpDown.TabIndex = 6;
            _EditedEngineeringUpDown.UnscaledValue = new decimal(new int[] { 0, 0, 0, 0 });
            _EditedEngineeringUpDown.UpDownCursor = Cursors.Hand;
            _EditedEngineeringUpDown.UpDownDisplayMode = isr.Core.WinControls.UpDownButtonsDisplayMode.WhenMouseOver;
            // 
            // _EnterValueNumericLabel
            // 
            _EnterValueNumericLabel.AutoSize = true;
            _EnterValueNumericLabel.Location = new Point(10, 16);
            _EnterValueNumericLabel.Name = "_EnterValueNumericLabel";
            _EnterValueNumericLabel.Size = new Size(112, 17);
            _EnterValueNumericLabel.TabIndex = 1;
            _EnterValueNumericLabel.Text = "Enter value to set:";
            _EnterValueNumericLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _EditedEngineeringUpDownLabel
            // 
            _EditedEngineeringUpDownLabel.AutoSize = true;
            _EditedEngineeringUpDownLabel.Location = new Point(13, 53);
            _EditedEngineeringUpDownLabel.Name = "_EditedEngineeringUpDownLabel";
            _EditedEngineeringUpDownLabel.Size = new Size(69, 17);
            _EditedEngineeringUpDownLabel.TabIndex = 1;
            _EditedEngineeringUpDownLabel.Text = "Edit Value:";
            _EditedEngineeringUpDownLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _ScaledValueTextBoxLabel
            // 
            _ScaledValueTextBoxLabel.AutoSize = true;
            _ScaledValueTextBoxLabel.Location = new Point(145, 53);
            _ScaledValueTextBoxLabel.Name = "_ScaledValueTextBoxLabel";
            _ScaledValueTextBoxLabel.Size = new Size(104, 17);
            _ScaledValueTextBoxLabel.TabIndex = 1;
            _ScaledValueTextBoxLabel.Text = "Observed Value:";
            _ScaledValueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // NumericUpDown1
            // 
            _NumericUpDown.Location = new Point(12, 116);
            _NumericUpDown.Maximum = new decimal(new int[] { 2000000000, 0, 0, 0 });
            _NumericUpDown.Minimum = new decimal(new int[] { 2000000000, 0, 0, (int)-2147483648L });
            _NumericUpDown.Name = "_NumericUpDown1";
            _NumericUpDown.ReadOnlyBackColor = Color.Empty;
            _NumericUpDown.ReadOnlyForeColor = Color.Empty;
            _NumericUpDown.ReadWriteBackColor = Color.Empty;
            _NumericUpDown.ReadWriteForeColor = Color.Empty;
            _NumericUpDown.Size = new Size(80, 25);
            _NumericUpDown.TabIndex = 9;
            // 
            // EngineeringUpDownForm
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(291, 257);
            Controls.Add(_NumericUpDown);
            Controls.Add(_EnterValueNumeric);
            Controls.Add(_ReadOnlyCheckBox);
            Controls.Add(_EditedEngineeringUpDown);
            Controls.Add(_ScaledValueTextBox);
            Controls.Add(_ScaledValueTextBoxLabel);
            Controls.Add(_EditedEngineeringUpDownLabel);
            Controls.Add(_EnterValueNumericLabel);
            Controls.Add(_EngineeringScaleComboBoxLabel);
            Controls.Add(_EngineeringScaleComboBox);
            Controls.Add(_DecimalPlacesNumericLabel);
            Controls.Add(_DecimalPlacesNumeric);
            Controls.Add(_UnitTextBoxLabel);
            Controls.Add(_UnitTextBox);
            Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            Margin = new Padding(3, 4, 3, 4);
            Name = "EngineeringUpDownForm";
            Text = "Engineering Up Down Form";
            ((System.ComponentModel.ISupportInitialize)_DecimalPlacesNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_EnterValueNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_EditedEngineeringUpDown).EndInit();
            ((System.ComponentModel.ISupportInitialize)_NumericUpDown).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private Label _DecimalPlacesNumericLabel;
        private NumericUpDown _DecimalPlacesNumeric;
        private ComboBox _EngineeringScaleComboBox;
        private Label _EngineeringScaleComboBoxLabel;
        private ToolTip _ToolTip;
        private Label _UnitTextBoxLabel;
        private TextBox _UnitTextBox;
        private isr.Core.WinControls.EngineeringUpDown _EditedEngineeringUpDown;
        private TextBox _ScaledValueTextBox;
        private CheckBox _ReadOnlyCheckBox;
        private isr.Core.WinControls.NumericUpDown _EnterValueNumeric;
        private Label _EnterValueNumericLabel;
        private Label _EditedEngineeringUpDownLabel;
        private Label _ScaledValueTextBoxLabel;
        private isr.Core.WinControls.NumericUpDown _NumericUpDown;
    }
}
