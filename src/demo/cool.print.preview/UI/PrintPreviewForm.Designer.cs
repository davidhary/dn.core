using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Testers
{
    [DesignerGenerated()]
    public partial class PrintPreviewForm : Form
    {

        /// <summary>   Form overrides dispose to clean up the component list. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    components?.Dispose();
                    if (_Font is object)
                        _Font.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }


        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "PrintPreviewDialog")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "CoolPrintPreviewDialog")]
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new Container();
            _LongDocumentCheckBox = new CheckBox();
            _CoolCodeLabel = new Label();
            _StandardCodeLabel = new Label();
            _OpenCoolPreviewDialogButton = new Button();
            _OpenCoolPreviewDialogButton.Click += new EventHandler(OpenCoolPreviewDialogButtonClick);
            _OpenStandardPreviewDialogButton = new Button();
            _OpenStandardPreviewDialogButton.Click += new EventHandler(OpenStandardPreviewDialogButtonClick);
            _PrintDocument = new System.Drawing.Printing.PrintDocument();
            _PrintDocument.BeginPrint += new System.Drawing.Printing.PrintEventHandler(PrintDocumentBeginPrint);
            _PrintDocument.EndPrint += new System.Drawing.Printing.PrintEventHandler(PrintDocumentEndPrint);
            _PrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(PrintDocumentPrintPage);
            SuspendLayout();
            // 
            // _LongDocumentCheckBox
            // 
            _LongDocumentCheckBox.AutoSize = true;
            _LongDocumentCheckBox.Location = new Point(232, 238);
            _LongDocumentCheckBox.Name = "_LongDocumentCheckBox";
            _LongDocumentCheckBox.Size = new Size(162, 21);
            _LongDocumentCheckBox.TabIndex = 15;
            _LongDocumentCheckBox.Text = "Create Long Document";
            _LongDocumentCheckBox.UseVisualStyleBackColor = true;
            // 
            // _CoolCodeLabel
            // 
            _CoolCodeLabel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _CoolCodeLabel.Font = new Font("Courier New", 7.8f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _CoolCodeLabel.Location = new Point(229, 124);
            _CoolCodeLabel.Name = "_CoolCodeLabel";
            _CoolCodeLabel.Size = new Size(388, 102);
            _CoolCodeLabel.TabIndex = 13;
            // 
            // _StandardCodeLabel
            // 
            _StandardCodeLabel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _StandardCodeLabel.Font = new Font("Courier New", 7.8f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _StandardCodeLabel.Location = new Point(229, 13);
            _StandardCodeLabel.Name = "_StandardCodeLabel";
            _StandardCodeLabel.Size = new Size(388, 102);
            _StandardCodeLabel.TabIndex = 14;
            // 
            // _OpenCoolPreviewDialogButton
            // 
            _OpenCoolPreviewDialogButton.Location = new Point(13, 124);
            _OpenCoolPreviewDialogButton.Margin = new Padding(4);
            _OpenCoolPreviewDialogButton.Name = "_OpenCoolPreviewDialogButton";
            _OpenCoolPreviewDialogButton.Size = new Size(209, 102);
            _OpenCoolPreviewDialogButton.TabIndex = 12;
            _OpenCoolPreviewDialogButton.Text = "Enhanced" + '\r' + '\n' + "CoolPrintPreviewDialog";
            _OpenCoolPreviewDialogButton.UseVisualStyleBackColor = true;
            // 
            // _OpenStandardPreviewDialogButton
            // 
            _OpenStandardPreviewDialogButton.Location = new Point(13, 13);
            _OpenStandardPreviewDialogButton.Margin = new Padding(4);
            _OpenStandardPreviewDialogButton.Name = "_OpenStandardPreviewDialogButton";
            _OpenStandardPreviewDialogButton.Size = new Size(209, 102);
            _OpenStandardPreviewDialogButton.TabIndex = 11;
            _OpenStandardPreviewDialogButton.Text = "Standard" + '\r' + '\n' + "PrintPreviewDialog";
            _OpenStandardPreviewDialogButton.UseVisualStyleBackColor = true;
            // 
            // PrintDocument1
            // 
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(120.0f, 120.0f);
            AutoScaleMode = AutoScaleMode.Dpi;
            ClientSize = new Size(650, 275);
            Controls.Add(_LongDocumentCheckBox);
            Controls.Add(_CoolCodeLabel);
            Controls.Add(_StandardCodeLabel);
            Controls.Add(_OpenCoolPreviewDialogButton);
            Controls.Add(_OpenStandardPreviewDialogButton);
            Name = "Form1";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Compare Print Preview Dialogs";
            ResumeLayout(false);
            PerformLayout();
        }

        private CheckBox _LongDocumentCheckBox;
        private Label _CoolCodeLabel;
        private Label _StandardCodeLabel;
        private Button _OpenCoolPreviewDialogButton;
        private Button _OpenStandardPreviewDialogButton;
        private System.Drawing.Printing.PrintDocument _PrintDocument;
    }
}
