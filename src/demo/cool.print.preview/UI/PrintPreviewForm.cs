using System;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.Testers
{

    /// <summary>   Form for viewing the print preview. </summary>
    /// <remarks>
    /// (c) 2009 Bernardo Castilho. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-08-05" by="Bernardo Castilho" revision=""&gt;
    /// http://www.codeproject.com/Articles/38758/An-Enhanced-PrintPreviewDialog. </para>
    /// </remarks>
    public partial class PrintPreviewForm
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public PrintPreviewForm()
        {

            // This call is required by the designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            this._CoolCodeLabel.Text = @"Using dialog As CoolPrintPreviewDialog = New CoolPrintPreviewDialog
    dialog.Document = Me._PrintDocument
    dialog.ShowDialog(Me)
End Using";
            this._StandardCodeLabel.Text = @"Using dialog As PrintPreviewDialog = New PrintPreviewDialog
    dialog.Document = Me._PrintDocument
    dialog.ShowDialog(Me)
End Using";
        }

        /// <summary>   The font. </summary>
        private readonly Font _Font = new( "Segoe UI", 14.0f );
        /// <summary>   The page. </summary>
        private int _Page = 0;
        /// <summary>   The start. </summary>
        private DateTimeOffset _Start;

        /// <summary>
        /// Event handler. Called by _OpenStandardPreviewDialogButton for click events. Show standard
        /// print preview.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void OpenStandardPreviewDialogButtonClick( object sender, EventArgs e )
        {
            using var dialog = new PrintPreviewDialog();
            dialog.Document = this._PrintDocument;
            _ = dialog.ShowDialog( this );
        }

        /// <summary>
        /// Event handler. Called by _OpenCoolPreviewDialogButton for click events. Show cool print
        /// preview.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void OpenCoolPreviewDialogButtonClick( object sender, EventArgs e )
        {
            using var dialog = new WinForms.CoolPrintPreviewDialog();
            dialog.Document = this._PrintDocument;
            _ = dialog.ShowDialog( this );
        }

        /// <summary>
        /// Event handler. Called by _PrintDocument for begin print events. Render document.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Print event information. </param>
        private void PrintDocumentBeginPrint( object sender, System.Drawing.Printing.PrintEventArgs e )
        {
            this._Start = DateTimeOffset.Now;
            this._Page = 0;
        }

        /// <summary>   Event handler. Called by _PrintDocument for end print events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Print event information. </param>
        private void PrintDocumentEndPrint( object sender, System.Drawing.Printing.PrintEventArgs e )
        {
            Console.WriteLine( $"Document rendered in {DateTimeOffset.Now.Subtract( this._Start ).TotalMilliseconds} ms" );
        }

        /// <summary>   Event handler. Called by _PrintDocument for print page events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Print page event information. </param>
        private void PrintDocumentPrintPage( object sender, System.Drawing.Printing.PrintPageEventArgs e )
        {
            var rc = e.MarginBounds;
            rc.Height = this._Font.Height + 10;
            int i = 0;
            while ( true )
            {
                string text = $"line {i + 1} on page {this._Page + 1}";
                e.Graphics.DrawString( text, this._Font, Brushes.Black, rc );
                rc.Y += rc.Height;
                if ( rc.Bottom > e.MarginBounds.Bottom )
                {
                    this._Page += 1;
                    e.HasMorePages = this._LongDocumentCheckBox.Checked ? this._Page < 3000 : this._Page < 30;
                    return;
                }

                i += 1;
            }
        }
    }
}
