# Command Line Demo

This program uses .NET 5.0 Command Line Utilities for parsing the command line.

The program demonstrates the following implementations:

## Command Line Options
The program define a few command line options. The 'Option' command line arguments
is used to control how the program is ran whereas other option defines 
optional settings. Alternatively, options can be implemented as command line arguments. 
Also, the run option could be implemented using a single or complex command.

## Command Line Arguments
The command line could consist of an ordered list of arguments where the argument values
determine the functionality.

## Command Line Commands
The program demonstrates using a simple or complex command.

## Program Execution Plan
Actual implementation is defined in the execution function of the configuration application.

## Program Execution
Finally, upon execution, the configuration application runs per the defined execution plan.
