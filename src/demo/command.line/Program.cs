using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Microsoft.Extensions.CommandLineUtils;

namespace isr.Core.CommandLine.Demo
{
    /// <summary>   A program. </summary>
    /// <remarks>   David, 2021-03-13. </remarks>
    internal class Program
    {

        /// <summary>   Main entry-point for this application. </summary>
        /// <remarks>   David, 2021-03-13. </remarks>
        /// <param name="args"> An array of command-line argument strings. </param>
        private static void Main( string[] args )
        {
            Console.WriteLine( "Hello World!" );

            var app = Program.CreateApp();
            Program.DefineOptions( app );
            Program.DefineArguments( app );
            Program.DefineExecution( app );
            try
            {
                // This begins the actual execution of the application
                Console.WriteLine( "Command Line Tester started..." );
                int outcome = app.Execute( args );
                Console.WriteLine( $"\nCommand Line Tester exited with code {outcome}" );
            }
            catch ( CommandParsingException ex )
            {
                // You'll always want to catch this exception, otherwise it will generate a messy and confusing error for the end user.
                // the message will usually be something like:
                // "Unrecognized command or argument '<invalid-command>'"
                Console.WriteLine( $"Exception; {Environment.NewLine}{ex.Message }." );
            }
            catch ( Exception ex )
            {
                Console.WriteLine( $"Unable to execute application: {Environment.NewLine}{ex.Message }." );
            }
        }

        /// <summary>   Define execution. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        private static void DefineExecution( CommandLineApplication app )
        {

            // When no commands are specified, this block will execute.
            // This is the main "command"
            app.OnExecute( () => {

                if ( (app.Arguments?.Any()).GetValueOrDefault( false ) )
                    Console.WriteLine( "Command line arguments:" );
                // You can also use the Arguments collection to iterate through the supplied arguments
                int index = 0;
                foreach ( CommandArgument arg in app.Arguments )
                {
                    // assign values based on argument values and index.
                    Console.WriteLine( $"Arguments collection [{index}] = {arg.Value ?? "null"}" );
                    index += 1;
                }
                if ( (app.Arguments?.Any()).GetValueOrDefault( false ) )
                    Console.WriteLine();

                if ( (app.GetOptions()?.Any()).GetValueOrDefault( false ) )
                    Console.WriteLine( "Command line arguments:" );
                foreach ( CommandOption opt in app.GetOptions() )
                {
                    // assign values based on argument values and index.
                    Console.WriteLine( $"Option collection [{opt.LongName}] = {opt.Value() ?? "null"}" );
                }
                if ( (app.GetOptions()?.Any()).GetValueOrDefault( false ) )
                    Console.WriteLine();

                // select the 'option' command line argument
                CommandOption basicOption = app.GetOptions().Where( x => x.ShortName == "o" ).FirstOrDefault();

                // Use the HasValue() method to check if the option was specified
                if ( basicOption is object && basicOption.HasValue() )
                {
                    Console.WriteLine( $"{nameof( basicOption )} selected value: {basicOption.Value()}" );
                    Console.WriteLine( $"{nameof( basicOption )}: {basicOption.Value()} Done; Press Key to exit: " );
                    _ = Console.ReadKey();
                    Console.WriteLine( $"{nameof( basicOption )}: {basicOption.Value()} exiting" );

                }
                else
                {
                    Console.WriteLine( $"{nameof( basicOption )} option not specified; {Environment.NewLine}{app.GetHelpText()}." );
                }
                return 0;
            } );

        }

        #region " COMMAND LINE PARSING "

        /// <summary>   Creates the application. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <returns>   The new application. </returns>
        private static CommandLineApplication CreateApp()
        {
            // Instantiate the command line application
            var app = new CommandLineApplication {
                // This should be the name of the executable itself. the help text line "Usage: ConsoleArgs" uses this
                Name = "CommandLineTester",
                Description = ".NET Core Command Line Tester",
                ExtendedHelpText = @"This program demonstrates the command line parsing functionality of .NET 5.0."
            };

            // Set the arguments to display the description and help text
            _ = app.HelpOption( "-?|-h|--help" );

            // This is a helper/shortcut method to display version info - it is creating a regular Option, with some defaults.
            // The default help text is "Show version Information"
            _ = app.VersionOption( "-v|--version", () => {
                return $"Version {Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion}";
            } );

            return app;
        }

        /// <summary>   Define application options. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        /// <returns>   A CommandOption. </returns>
        private static void DefineOptions( CommandLineApplication app )
        {
            // Arguments: -o listener -l error -d ..\_log -f Column

            // The first argument is the option template.
            // It starts with a pipe-delimited list of option flags/names to use
            // Optionally, It is then followed by a space and a short description of the value to specify.
            // e.g. here we could also just use "-o|--option"
            _ = app.Option( "-o|--option <value>", "Listener or Provider Logging implementation", CommandOptionType.SingleValue );
            _ = app.Option( "-l|--level <value>", "Logging level: Information, Warning, Error", CommandOptionType.SingleValue );
            _ = app.Option( "-d|--dir <value>", "Relative Directory, e.g., ..\\_log", CommandOptionType.SingleValue );
            _ = app.Option( "-f|--form <value>", "Logging format: Column, Row, Tab, Comma", CommandOptionType.SingleValue );
        }

        /// <summary>   Enumerates define command line arguments in this collection. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process define command arguments in this
        /// collection.
        /// </returns>
        private static void DefineArguments( CommandLineApplication app )
        {
            // Arguments are basic arguments, that are parsed in the order they are given
            // e.g ConsoleArgs "first value" "second value"
            // This is OK for really simple tasks, but generally you're better off using Options
            // since they avoid confusion
            _ = app.Argument( "Option", "Listener or Provider Logging implementation" );
            _ = app.Argument( "Level", "Logging Level" );
            _ = app.Argument( "Directory", "Logging relative folder" );
            _ = app.Argument( "Format", "Logging format: Column, Rows, Tab, Comma" );
        }

        /// <summary>   Defines a simple command execution. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private static void DefineSimpleCommand( CommandLineApplication app )
        {
            // This is a command with no arguments - it just does default action.
            _ = app.Command( "simple-command", ( command ) => {
                //description and help text of the command.
                command.Description = "This is the description for simple-command.";
                command.ExtendedHelpText = "This is the extended help text for simple-command.";
                _ = command.HelpOption( "-?|-h|--help" );

                command.OnExecute( () => {
                    Console.WriteLine( "simple-command is executing" );

                    //Do the command's work here, or via another object/method

                    Console.WriteLine( "simple-command has finished." );
                    return 0; //return 0 on a successful execution
                } );

            } );
        }

        /// <summary>   Defines a complex command. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private static void DefineComplexCommand( CommandLineApplication app )
        {

            _ = app.Command( "complex-command", ( command ) => {
                // This is a command that has it's own options.
                command.ExtendedHelpText = "This is the extended help text for complex-command.";
                command.Description = "This is the description for complex-command.";
                _ = command.HelpOption( "-?|-h|--help" );

                // There are 3 possible option types:
                // NoValue
                // SingleValue
                // MultipleValue

                // MultipleValue options can be supplied as one or multiple arguments
                // e.g. -m valueOne -m valueTwo -m valueThree
                var multipleValueOption = command.Option( "-m|--multiple-option <value>",
                    "A multiple-value option that can be specified multiple times",
                    CommandOptionType.MultipleValue );

                // SingleValue: A basic Option with a single value
                // e.g. -s sampleValue
                var singleValueOption = command.Option( "-s|--single-option <value>",
                    "A basic single-value option",
                    CommandOptionType.SingleValue );

                // NoValue are basically booleans: true if supplied, false otherwise
                var booleanOption = command.Option( "-b|--boolean-option",
                    "A true-false, no value option",
                    CommandOptionType.NoValue );

                command.OnExecute( () => {
                    Console.WriteLine( "complex-command is executing" );

                    // Do the command's work here, or via another object/method                    

                    // Grab the values of the various options. when not specified, they will be null.

                    // The NoValue type has no Value property, just the HasValue() method.
                    bool booleanOptionValue = booleanOption.HasValue();

                    // MultipleValue returns a List<string>
                    List<string> multipleOptionValues = multipleValueOption.Values;

                    // SingleValue returns a single string
                    string singleOptionValue = singleValueOption.Value();

                    // Check if the various options have values and display them.
                    // Here we're checking HasValue() to see if there is a value before displaying the output.
                    // Alternatively, you could just handle nulls from the Value properties
                    if ( booleanOption.HasValue() )
                    {
                        Console.WriteLine( "booleanOption option: {0}", booleanOptionValue.ToString() );
                    }

                    if ( multipleValueOption.HasValue() )
                    {
                        Console.WriteLine( "multipleValueOption option(s): {0}", string.Join( ",", multipleOptionValues ) );
                    }

                    if ( singleValueOption.HasValue() )
                    {
                        Console.WriteLine( "singleValueOption option: {0}", singleOptionValue ?? "null" );
                    }

                    Console.WriteLine( "complex-command has finished." );
                    return 0; // return 0 on a successful execution
                } );
            } );

        }

        #endregion


    }
}
