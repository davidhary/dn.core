using System;
using System.Windows.Forms;

namespace isr.Core.Testers
{

    /// <summary>   A dashboard. </summary>
    /// <remarks>   David, 2021-03-16. 
    /// Created/modified in 2011 by Simon Baer
    /// Licensed under the Code Project Open License (CPOL).
    /// </remarks>
    public partial class Dashboard : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks> David, 2021-03-12. </remarks>
        public Dashboard()
        {
            this.InitializeComponent();
        }

        /// <summary> Shows the button click. </summary>
        /// <remarks> David, 2021-03-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ShowButton_Click( object sender, EventArgs e )
        {
            this._PopupNotifier.TitleText = this._NotificationTitleTextBox.Text;
            this._PopupNotifier.ContentText = this._NotificationTextTextBox.Text;
            this._PopupNotifier.ShowCloseButton = this._ShowNotificationCloseButtonCheckBox.Checked;
            this._PopupNotifier.ShowOptionsButton = this._ShowNotificationOptionMenuCheckBox.Checked;
            this._PopupNotifier.ShowGrip = this._ShowNotificationGripCheckBox.Checked;
            this._PopupNotifier.Delay = int.Parse( this._NotificationDelayTextBox.Text );
            this._PopupNotifier.AnimationInterval = int.Parse( this._NotificationAnimationIntervalTextBox.Text );
            this._PopupNotifier.AnimationDuration = int.Parse( this._AnimationDurationTextBox.Text );
            this._PopupNotifier.TitlePadding = new Padding( int.Parse( this._NotificationTitlePaddingTextBox.Text ) );
            this._PopupNotifier.ContentPadding = new Padding( int.Parse( this._NotificationContentPaddingTextBox.Text ) );
            this._PopupNotifier.ImagePadding = new Padding( int.Parse( this._NotificationIconPaddingTextBox.Text ) );
            this._PopupNotifier.Scroll = this._NotificationScrollCheckBox.Checked;
            this._PopupNotifier.Image = this._ShowNotificationIconCheckBox.Checked ? Properties.Resources._157_GetPermission_48x48_72 : null;
            this._PopupNotifier.Popup();
        }

        /// <summary> Exit button click. </summary>
        /// <remarks> David, 2021-03-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ExitButton_Click( object sender, EventArgs e )
        {
            this.Close();
            Application.Exit();
        }
    }
}
