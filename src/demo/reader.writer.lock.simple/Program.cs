using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

using isr.Core.Concurrent;

/// <summary>   A program. </summary>
/// <remarks>   David, 2022-02-02. </remarks>
internal class Program
{
    /// <summary>   Main entry-point for this application. </summary>
    /// <remarks>   
    /// Demonstrates how <see cref="ReadWriteLockSimple"/> works, The application runs multiple
    /// threads accessing the same resource at the same time.
    /// </remarks>
    /// <param name="args"> An array of command-line argument strings. </param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
    private static void Main( string[] args )
    {
        var readWriteLockSimple = new ReadWriteLockSimple();
        //var readWriteLockSimple = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        var items = new int[100];
        var t = Stopwatch.StartNew();

        _ = Parallel.For( 0, items.Length, i => {
            bool write = ((i + 1) % 5) == 0; // Every 5th item will require an exclusive lock.

            if ( write )
            {
                readWriteLockSimple.EnterWriteLock();
            }
            else
            {
                readWriteLockSimple.EnterReadLock();
            }

            try
            {
                if ( write )
                {
                    Console.WriteLine( $"{i}: Exclusive lock acquired at {t.ElapsedMilliseconds}." );
                    Thread.Sleep( 300 );
                }
                else
                {
                    Console.WriteLine( $"{i}: Read-only lock acquired at {t.ElapsedMilliseconds}." );
                }
            }
            finally
            {
                if ( write )
                {
                    readWriteLockSimple.ExitWriteLock();
                }
                else
                {
                    readWriteLockSimple.ExitReadLock();
                }
            }
        } );

        t.Stop();
        Console.WriteLine( t.ElapsedMilliseconds );
    }
}

