using System;
using System.Threading.Tasks;
using System.Threading;
using isr.Core.Threading;

namespace ReplacingThreadAbortDemo
{
    internal static class Program
    {
        internal static void Main()
        {
            _ProcessDuration = 1000;
            int msTimeout = _ProcessDuration / 2;
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            bool isExcecuted = WaitFor.TryCallWithTimeout(
               ProcessMethod,
               TimeSpan.FromMilliseconds( msTimeout ), // Timeout < Process Duration => ProcessMethod() gets Canceled
                                                       // Timeout > Process Duration => ProcessMethod() gets Executed
               out int result );
            Console.WriteLine( $"{nameof( ProcessMethod)}() with timeout of {msTimeout}ms and cancellation polling of {_PollInterval}ms {(isExcecuted ? "Executed" : "Cancelled")} after {sw.ElapsedMilliseconds}ms with result = {result}" );

            msTimeout = _ProcessDuration * 2;
            sw.Restart();
            isExcecuted = WaitFor.TryCallWithTimeout(
               ProcessMethod,
               TimeSpan.FromMilliseconds( msTimeout ), // Timeout < Process Duration => ProcessMethod() gets Canceled
                                                       // Timeout > Process Duration => ProcessMethod() gets Executed
               out result );
            Console.WriteLine();
            Console.WriteLine( $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_PollInterval}ms {(isExcecuted ? "Executed" : "Cancelled")} after {sw.ElapsedMilliseconds}ms with result = {result}" );

            _ProcessDuration = 200;
            msTimeout = _ProcessDuration / 2;
            sw.Restart();
            isExcecuted = WaitFor.TryCallWithTimeout(
               ProcessMethod,
               TimeSpan.FromMilliseconds( msTimeout ), // Timeout < Process Duration => ProcessMethod() gets Canceled
                                                       // Timeout > Process Duration => ProcessMethod() gets Executed
               out result );
            Console.WriteLine( $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_PollInterval}ms {(isExcecuted ? "Executed" : "Cancelled")} after {sw.ElapsedMilliseconds}ms with result = {result}" );

            msTimeout = _ProcessDuration * 2;
            sw.Restart();
            isExcecuted = WaitFor.TryCallWithTimeout(
               ProcessMethod,
               TimeSpan.FromMilliseconds( msTimeout ), // Timeout < Process Duration => ProcessMethod() gets Canceled
                                                       // Timeout > Process Duration => ProcessMethod() gets Executed
               out result );
            Console.WriteLine();
            Console.WriteLine( $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_PollInterval}ms {(isExcecuted ? "Executed" : "Cancelled")} after {sw.ElapsedMilliseconds}ms with result = {result}" );

        }

        private static int _PollInterval = 100;
        private static int _ProcessDuration = 200;

        private static int ProcessMethod( CancellationToken ct )
        {
            _PollInterval = _ProcessDuration / 10;
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            while ( sw.ElapsedMilliseconds < _ProcessDuration )
            {
                Thread.Sleep( _PollInterval );
                // co-operative cancellation implies periodically check IsCancellationRequested 
                if ( ct.IsCancellationRequested ) { throw new TaskCanceledException(); }
            }
            return 123; // the result 
        }

    }

}
