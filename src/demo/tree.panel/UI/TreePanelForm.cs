using System.Windows.Forms;

namespace isr.Core.Testers
{

    /// <summary>   Form for viewing the micro timer. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public partial class TreePanelForm : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks> David, 2021-03-12. </remarks>
        public TreePanelForm()
        {

            base.FormClosing += this.FormMainFormClosing;
            this.InitializeComponent();

        }

        private void FormMainFormClosing( object sender, FormClosingEventArgs e )
        {
        }
    }
}
