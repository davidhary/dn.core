using System;
using System.ComponentModel;

namespace JsonSettingsEditor.Demo
{
    /// <summary>   A settings. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    [isr.Json.SettingsSection( nameof( Settings ) )]
    internal class Settings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public Settings() : base( System.Reflection.Assembly.GetAssembly( typeof( Settings ) ) )
        { }


        private bool _Verbose;
        /// <summary>   Gets or sets a value enabling verbose messages. </summary>
        /// <value> True if verbose, false if not. </value>
        [Description( "Enables or disables verbose test messages" )]
        public bool Verbose
        {
            get => this._Verbose;
            set {
                if ( !bool.Equals( value, this.Verbose ) )
                {
                    this._Verbose = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _Enabled;
        /// <summary>   Gets or sets a value indicating whether this object is enabled. </summary>
        /// <value> True if enabled, false if not. </value>
        [ Description("True if testing is enabled for this test class") ]
        public bool Enabled
        {
            get => this._Enabled;
            set {
                if ( !bool.Equals( value, this.Enabled ) )
                {
                    this._Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _ResourceName;

        /// <summary>   Gets or sets the name of the resource. </summary>
        /// <value> The name of the resource. </value>
        [Description("Specifies the VISA resource name")]
        public string ResourceName
        {
            get => this._ResourceName;
            set {
                if ( !string.Equals( value, this.ResourceName ) )
                {
                    this._ResourceName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _IPv4Prefixes;
        /// <summary>   Gets or sets the candidate IPv4 prefixes for this location. </summary>
        /// <value> The IPv4 prefixes. </value>
        [Description("Specifies the IPv4 prefixes of the Internet addresses of known test sites; used to select settings that are test-site specific")]
        public string IPv4Prefixes
        {
            get => this._IPv4Prefixes;
            set {
                if ( !String.Equals( value, this.IPv4Prefixes ) )
                {
                    this._IPv4Prefixes = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _TimeZones;
        /// <summary> Gets or sets the candidate time zones of this location. </summary>
        /// <value> The candidate time zones of the test site. </value>
        [Description( "Specifies the time zone identities of known test sites corresponding to the set of IPv4 prefixes" )]
        public string TimeZones
        {
            get => this._TimeZones;
            set {
                if ( !String.Equals( value, this.TimeZones ) )
                {
                    this._TimeZones = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _TimeZoneOffsets;
        /// <summary> Gets or sets the candidate time zone offsets of this location. </summary>
        /// <value> The time zone offsets. </value>
        [Description( "Specifies the time zone offsets of known test sites corresponding to the set of IPv4 prefixes" )]
        public string TimeZoneOffsets
        {
            get => this._TimeZoneOffsets;
            set {
                if ( !String.Equals( value, this.TimeZoneOffsets ) )
                {
                    this._TimeZoneOffsets = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _ElementCount;
        /// <summary>   Gets or sets the number of elements. </summary>
        /// <value> The number of elements. </value>
        [Description("Specifies the element count")]
        public int ElementCount
        {
            get => this._ElementCount;
            set {
                if ( !int.Equals( value, this.ElementCount ) )
                {
                    this._ElementCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }


        /// <summary> The time zone. </summary>
        private string _TimeZone;

        /// <summary> Gets the time zone of the test site. </summary>
        /// <value> The time zone of the test site. </value>
        [Description("Gets the time zone identity of the test site")]
        public string TimeZone()
        {
            if ( string.IsNullOrWhiteSpace( this._TimeZone ) )
            {
                this._TimeZone = this.TimeZones.Split('|')[this.HostInfoIndex()];
            }
            return this._TimeZone;
        }

        /// <summary> The time zone offset. </summary>
        private double _TimeZoneOffset = double.MinValue;

        /// <summary> Gets the time zone offset of the test site. </summary>
        /// <value> The time zone offset of the test site. </value>
        [Description( "Gets the time zone offset of the test site" )]
        public double TimeZoneOffset()
        {
            if ( this._TimeZoneOffset == double.MinValue )
            {
                this._TimeZoneOffset = double.Parse( this.TimeZoneOffsets.Split( '|')[this.HostInfoIndex()] );
            }
            return this._TimeZoneOffset;
        }

        /// <summary> Gets the host name of the test site. </summary>
        /// <value> The host name of the test site. </value>
        [Description("Gets the host name of the test site")]
        public static string HostName()
        {
            return System.Net.Dns.GetHostName();
        }

        /// <summary> The host address. </summary>
        private System.Net.IPAddress _HostAddress;

        /// <summary> Gets the IP address of the test site. </summary>
        /// <value> The IP address of the test site. </value>
        [Description("Gets the host address of the test site")]
        public System.Net.IPAddress HostAddress()
        {
            if ( this._HostAddress is null )
            {
                foreach ( System.Net.IPAddress value in System.Net.Dns.GetHostEntry( HostName() ).AddressList )
                {
                    if ( value.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork )
                    {
                        this._HostAddress = value;
                        break;
                    }
                }
            }
            return this._HostAddress;
        }

        /// <summary> Zero-based index of the host information. </summary>
        private int _HostInfoIndex = -1;

        /// <summary> Gets the index into the host information strings. </summary>
        /// <value> The index into the host information strings. </value>
        [Description( "Gets the host info index of the test site that corresponds to the IPv4 prefix that matches the host address" )]
        public int HostInfoIndex()
        {
            if ( this._HostInfoIndex < 0 )
            {
                string ip = this.HostAddress().ToString();
                int i = -1;
                foreach ( string value in this.IPv4Prefixes.Split( '|' ) )
                {
                    i += 1;
                    if ( ip.StartsWith( value, StringComparison.OrdinalIgnoreCase ) )
                    {
                        this._HostInfoIndex = i;
                        break;
                    }
                }
            }

            return this._HostInfoIndex;
        }
    }
}
