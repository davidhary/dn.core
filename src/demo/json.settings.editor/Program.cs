using System;
using System.Windows.Forms;

namespace JsonSettingsEditor.Demo
{

    /// <summary>   A program. </summary>
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
#if NET5_0_OR_GREATER
            Application.SetHighDpiMode( HighDpiMode.SystemAware );
#endif
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            Settings currentSettings = new();
            currentSettings.ReadSettings();
            Application.Run( new isr.Json.JsonSettingsEditorForm( "Settings editor demo", currentSettings ) );
        }
    }
}
