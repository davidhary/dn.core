using System;
using System.Linq;
using System.Reflection;

using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Serilog;
using Serilog.Sinks.File.Header;
using isr.Core.Tracing;


namespace isr.Core.Logging.Tester
{
    /// <summary>   A program. </summary>
    /// <remarks>   David, 2021-02-04. </remarks>
    internal class Program
    {

        internal static CommandLineApplication CommandLineApp { get; set; }

        /// <summary>   Main entry-point for this application. </summary>
        /// <remarks>   David, 2021-02-04. </remarks>
        /// <param name="args"> An array of command-line argument strings. </param>
        private static void Main( string[] args )
        {
            Program.CommandLineApp = Program.CreateApp();
            Program.DefineOptions( Program.CommandLineApp );
            Program.DefineExecution( Program.CommandLineApp );
            try
            {
                // This begins the actual execution of the application
                Console.WriteLine( "Logging Tester executing..." );
                int outcome = Program.CommandLineApp.Execute( args );
                Console.WriteLine( $"Logging Tester exited with code {outcome}" );
            }
            catch ( CommandParsingException ex )
            {
                // You'll always want to catch this exception, otherwise it will generate a messy and confusing error for the end user.
                // the message will usually be something like:
                // "Unrecognized command or argument '<invalid-command>'"
                Console.WriteLine( $"Exception; {Environment.NewLine}{ex.Message }." );
            }
            catch ( Exception ex )
            {
                Console.WriteLine( $"Exception; {Environment.NewLine}{ex.Message }." );
            }

        }

        /// <summary>   Define execution. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        private static void DefineExecution( CommandLineApplication app )
        {

            // When no commands are specified, this block will execute.
            // This is the main "command"
            app.OnExecute( () => {

                // Use the HasValue() method to check if the option was specified
                CommandOption basicOption = app.GetOptions().Where( x => x.ShortName == "o" ).FirstOrDefault();
                if ( basicOption is object && basicOption.HasValue() )
                {
                    Console.WriteLine( $"Running '{basicOption.Value()}' option..." );
                    switch ( basicOption.Value() )
                    {
                        case "serilog":
                            RunSerilogImplementation();
                            break;

                        default:
                            break;
                    }
                    Console.Write( "\r\n\r\nDone. Enter key: " );
                    _ = Console.ReadKey();

                }
                else
                {
                    Console.WriteLine( $"Logger option not specified; {Environment.NewLine}{app.GetHelpText()}." );
                }
                return 0;
            } );

        }

        #region " COMMAND LINE PARSING "

        /// <summary>   Creates the application. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <returns>   The new application. </returns>
        private static CommandLineApplication CreateApp()
        {
            // Instantiate the command line application
            var app = new CommandLineApplication {
                // This should be the name of the executable itself. the help text line "Usage: ConsoleArgs" uses this
                Name = "LoggingTester",
                Description = ".NET Core logging tester",
                ExtendedHelpText = @"This program demonstrates the logging functionality of the isr.Core.Logging.Logger."
            };

            // Set the arguments to display the description and help text
            _ = app.HelpOption( "-?|-h|--help" );

            // This is a helper/shortcut method to display version info - it is creating a regular Option, with some defaults.
            // The default help text is "Show version Information"
            _ = app.VersionOption( "-v|--version", () => {
                return $"Version {Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion}";
            } );

            return app;
        }

        /// <summary>   Define application options. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        /// <returns>   A CommandOption. </returns>
        private static void DefineOptions( CommandLineApplication app )
        {
            // Arguments: -o listener -l error -d ..\_log -f Column

            // The first argument is the option template.
            // It starts with a pipe-delimited list of option flags/names to use
            // Optionally, It is then followed by a space and a short description of the value to specify.
            // e.g. here we could also just use "-o|--option"
            _ = app.Option( "-o|--option <value>", "Listener or Provider Logging implementation", CommandOptionType.SingleValue );
            _ = app.Option( "-l|--level <value>", "Logging level: Information, Warning, Error", CommandOptionType.SingleValue );
            _ = app.Option( "-d|--dir <value>", "Relative Directory, e.g., ..\\_log", CommandOptionType.SingleValue );
            _ = app.Option( "-q|--queue <value>", "Queue Listener: Sync, Async", CommandOptionType.SingleValue );
        }

        #endregion

        #region " SERILOG IMPLEMENTATION "

        /// <summary>   Configure logging services. </summary>
        /// <remarks>   David, 2021-02-09. </remarks>
        /// <param name="serviceCollection">    Collection of services. </param>
        private static void ConfigureLoggingServices( IServiceCollection serviceCollection )
        {
#if false
            // Shows how to set up the Logging rather than the Serilog sinks.
            isr.Core.Logging.ILogger.Platform.Get().ConfigureLoggingServices( serviceCollection,
                                                                              Logging.ILogger.LoggingProviderKinds.Serilog
                                                                              | Logging.ILogger.LoggingProviderKinds.ConsoleLog
                                                                              | Logging.ILogger.LoggingProviderKinds.DebugLog
                                                                              | Logging.ILogger.LoggingProviderKinds.TraceLog );
#endif

            isr.Core.Logging.ILogger.LoggingPlatform.Instance.RuntimeSerilogLevel = Microsoft.Extensions.Logging.LogLevel.Trace;
            isr.Core.Logging.ILogger.LoggingPlatform.Instance.ConfigureLoggingServices( ILogger.LoggingPlatform.BuildEntryAssemblySettingsFileName(), serviceCollection,
                                                                                        Logging.ILogger.LoggingProviderKinds.Serilog );

            CommandOption queueOption = Program.CommandLineApp.GetOptions().Where( x => x.ShortName == "q" ).FirstOrDefault();
            if ( queueOption.HasValue() && String.Equals( queueOption.Value(), "Async", StringComparison.OrdinalIgnoreCase ) )
            {
                Console.WriteLine();
                Console.WriteLine( $"Queue option: {queueOption.Value()}" );
                Console.WriteLine();
                TracingPlatform.Instance.AddTraceEventWriter( TracingPlatform.Instance.CriticalTraceEventTraceListener );
                // comment out to see all messages.
                // TracingPlatform.Instance.CriticalTraceEventSourceLevel = TracingPlatform.Instance.TraceEventSourceLevel;
            }
            else
            {
                // Add Concurrent Queue trace listener
                TracingPlatform.Instance.AddTraceListener( TracingPlatform.Instance.CriticalTraceEventTraceListener );
            }

            // Add services
            _ = serviceCollection.AddTransient<ITestService, TestService>();

            // Add the application
            _ = serviceCollection.AddTransient<App>();
        }

        /// <summary>   Gets or sets the number of expected trace messages. </summary>
        /// <value> The number of expected trace messages. </value>
        internal static int ExpectedTraceMessagesCount { get; set; }

        /// <summary>   Executes the 'provider implementation' operation. </summary>
        /// <remarks>   David, 2021-02-04. </remarks>
        private static void RunSerilogImplementation()
        {
            // Create service collection
            var serviceCollection = new ServiceCollection();
            ConfigureLoggingServices( serviceCollection );

            // a logger can be instantiated at this point. 
            ILogger<Program> logger = ILogger.LoggingPlatform.Instance.CreateLogger<Program>();

            logger.LogInformation( "Starting application..." );

            // count expected trace messages based on the listener level.
            if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
                Program.ExpectedTraceMessagesCount += 1;

            // entry to run the application
            serviceCollection.BuildServiceProvider().GetService<App>().Run();

        }

        #endregion

    }

    #region " LOGGER-INJECTED APPLICATION CLASSES "

    /// <summary>
    /// To maintain a separation of concern between our business based logic and the logic we use to
    /// configure and run the actual console application, let's create a new class called App.cs. The
    /// goal being that we will use Program.cs to bootstrap everything need to support our
    /// application and then fire off all the logic that is needed to support out "business" needs by
    /// way of executing the Run() method in App.cs.
    /// </summary>
    /// <remarks>   David, 2021-02-04. </remarks>
    public class App
    {
        private readonly ITestService _TestService;
        private readonly ILogger<App> _Logger;

        public App( ITestService testService, ILogger<App> logger )
        {
            this._TestService = testService;
            this._Logger = logger;
        }

        public void Run()
        {
            this._Logger.LogInformation( "Running application." );
            // count expected trace messages based on the listener level.
            if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
                Program.ExpectedTraceMessagesCount += 1;

            TimeSpan timeout = TimeSpan.FromMilliseconds( 100 );
            bool fileOpened = HeaderWriter.AwaitFullFileName( timeout, TimeSpan.FromMilliseconds( 20 ), TimeSpan.FromMilliseconds( 20 ) );
            if ( fileOpened )
            {
                this._Logger.LogInformation( $"Logging into {HeaderWriter.FullFileName}." );
            }
            else
            {
                this._Logger.LogInformation( $"File not open after {timeout.TotalMilliseconds}ms." );
            }
            // count expected trace messages based on the listener level.
            if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
                Program.ExpectedTraceMessagesCount += 1;

            try
            {
                this._TestService.Run();
            }
            catch ( Exception ex )
            {
                ex.Data.Add( $"data {ex.Data.Count}", "internal exception data" );
                ex.Data.Add( $"data {ex.Data.Count}", "internal exception data 2" );
                InvalidOperationException reportEx = new( $"Caught in {nameof( App )}", ex );
                reportEx.Data.Add( $"data {reportEx.Data.Count}", "parent exception data" );
                reportEx.Data.Add( $"data {reportEx.Data.Count}", "parent exception data 2" );
                this._Logger.LogWarning( "warning; exception" );
                this._Logger.LogError( reportEx, "reporting exception" );
                if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Information )
                    Program.ExpectedTraceMessagesCount += 1;
            }

            Console.WriteLine();
            Console.WriteLine( "--- Issue 184: Debug and trace events are filtered in even under warnings level." );
            Console.WriteLine( $"--- Queue Trace Listener Records (expecting {Program.ExpectedTraceMessagesCount} or {Program.ExpectedTraceMessagesCount + 1}):" );
            // Console write line missed the CRLF preceding the stack trace line.
            string formatted = TracingPlatform.Instance.CriticalTraceEventTraceListener.DequeueAllMessages();
            Console.WriteLine( formatted );
            Console.WriteLine( "--- End of Queue Trace Listener Records" );
            Console.WriteLine();

            // report number of elements in the Queue Trace Listener
            Console.WriteLine( $"Queue Trace Listener includes {TracingPlatform.Instance.CriticalTraceEventTraceListener.Queue.Count} records (expecting {Program.ExpectedTraceMessagesCount} or {Program.ExpectedTraceMessagesCount + 1})." );
            Console.WriteLine( $"Flushing the Queue Trace Listener ." );
            TracingPlatform.Instance.CriticalTraceEventTraceListener.Flush();
            Console.WriteLine( $"Queue Trace Listener includes {TracingPlatform.Instance.CriticalTraceEventTraceListener.Queue.Count} records after flush." );

            Console.WriteLine();
            Console.WriteLine( "--- Queue Trace Listener Records after flush:" );
            // Console write line missed the CRLF preceding the stack trace line.
            formatted = TracingPlatform.Instance.CriticalTraceEventTraceListener.DequeueAllMessages();
            Console.WriteLine( formatted );
            Console.WriteLine( "--- End of Queue Trace Listener Records" );
            Console.WriteLine();


            Log.CloseAndFlush();

        }
    }


    /// <summary>   Interface for test service. </summary>
    /// <remarks>   David, 2021-02-04. </remarks>
    public interface ITestService
    {
        void Run();
    }

    /// <summary>   A service for accessing tests information. </summary>
    /// <remarks>   David, 2021-02-04. </remarks>
    internal class TestService : ITestService
    {
        private readonly ILogger<TestService> _Logger;

        public TestService( ILogger<TestService> logger )
        {
            this._Logger = logger;
        }

        public void Run()
        {
            this._Logger.LogDebug( $"Running logging service." );
            if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Verbose )
                Program.ExpectedTraceMessagesCount += 1;
            this._Logger.LogTrace( $"Throwing divide by zero exception." );
            if ( TracingPlatform.Instance.CriticalTraceEventSourceLevel >= System.Diagnostics.SourceLevels.Verbose )
                Program.ExpectedTraceMessagesCount += 1;
            throw new DivideByZeroException();
        }
    }

    #endregion

}
