using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Testers
{
    /// <summary>   Form for viewing the selector. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    [DesignerGenerated()]
    public partial class SelectorsForm : Form
    {

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                                                         resources; <see langword="false" /> to
        ///                                                         release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        /// <summary>   Required by the Windows Form Designer. </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// NOTE: The following procedure is required by the Windows Form Designer It can be modified
        /// using the Windows Form Designer.  
        /// Do not modify it using the code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectorsForm));
            _ToolTip = new ToolTip(components);
            _SelectedTextBox = new TextBox();
            _ReadOnlyCheckBox = new CheckBox();
            _ReadOnlyCheckBox.CheckedChanged += new EventHandler(ReadOnlyCheckBox_CheckedChanged);
            _EnterValueNumericLabel = new Label();
            _EditedEngineeringUpDownLabel = new Label();
            _ScaledValueTextBoxLabel = new Label();
            _ApplyButton = new Button();
            _ApplyButton.Click += new EventHandler(ApplyButton_Click);
            _EnterValueLabel = new ToolStripTextBox();
            _SelectedValueLabel = new ToolStripTextBox();
            _SelectorComboBox = new isr.Core.WinControls.SelectorComboBox();
            _SelectorComboBox.ValueSelected += new EventHandler<EventArgs>(SelectorComboBox_ValueSelected);
            _SelectButton = new Button();
            _SelectButton.Click += new EventHandler(SelectButton_Click);
            _EnterNumeric = new isr.Core.WinControls.EngineeringUpDown();
            _SelectorNumeric = new isr.Core.WinControls.SelectorNumeric();
            _SelectorNumeric.ValueSelected += new EventHandler<EventArgs>(SelectorNumeric_ValueSelected);
            _ToolStrip = new ToolStrip();
            _ToolStripSelectorComboBox = new isr.Core.WinControls.ToolStripSelectorComboBox();
            _ToolStripSelectorComboBox.ValueSelected += new EventHandler<EventArgs>(ToolStripSelectorComboBox_ValueSelected);
            _ToolStripSelectorNumeric = new isr.Core.WinControls.ToolStripSelectorNumeric();
            _ToolStripSelectorNumeric.ValueSelected += new EventHandler<EventArgs>(ToolStripSelectorNumeric_ValueSelected);
            ((System.ComponentModel.ISupportInitialize)_EnterNumeric).BeginInit();
            _ToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _SelectedTextBox
            // 
            _SelectedTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _SelectedTextBox.ForeColor = SystemColors.ActiveCaption;
            _SelectedTextBox.Location = new Point(147, 122);
            _SelectedTextBox.Name = "_SelectedTextBox";
            _SelectedTextBox.Size = new Size(121, 25);
            _SelectedTextBox.TabIndex = 4;
            _SelectedTextBox.Text = "ABC";
            _ToolTip.SetToolTip(_SelectedTextBox, "Scaled value");
            // 
            // _ReadOnlyCheckBox
            // 
            _ReadOnlyCheckBox.AutoSize = true;
            _ReadOnlyCheckBox.Location = new Point(16, 171);
            _ReadOnlyCheckBox.Name = "_ReadOnlyCheckBox";
            _ReadOnlyCheckBox.Size = new Size(87, 21);
            _ReadOnlyCheckBox.TabIndex = 8;
            _ReadOnlyCheckBox.Text = "Read Only";
            _ReadOnlyCheckBox.UseVisualStyleBackColor = true;
            // 
            // _EnterValueNumericLabel
            // 
            _EnterValueNumericLabel.AutoSize = true;
            _EnterValueNumericLabel.Location = new Point(10, 65);
            _EnterValueNumericLabel.Name = "_EnterValueNumericLabel";
            _EnterValueNumericLabel.Size = new Size(112, 17);
            _EnterValueNumericLabel.TabIndex = 1;
            _EnterValueNumericLabel.Text = "Enter value to set:";
            _EnterValueNumericLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _EditedEngineeringUpDownLabel
            // 
            _EditedEngineeringUpDownLabel.AutoSize = true;
            _EditedEngineeringUpDownLabel.Location = new Point(13, 102);
            _EditedEngineeringUpDownLabel.Name = "_EditedEngineeringUpDownLabel";
            _EditedEngineeringUpDownLabel.Size = new Size(69, 17);
            _EditedEngineeringUpDownLabel.TabIndex = 1;
            _EditedEngineeringUpDownLabel.Text = "Edit Value:";
            _EditedEngineeringUpDownLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _ScaledValueTextBoxLabel
            // 
            _ScaledValueTextBoxLabel.AutoSize = true;
            _ScaledValueTextBoxLabel.Location = new Point(145, 102);
            _ScaledValueTextBoxLabel.Name = "_ScaledValueTextBoxLabel";
            _ScaledValueTextBoxLabel.Size = new Size(104, 17);
            _ScaledValueTextBoxLabel.TabIndex = 1;
            _ScaledValueTextBoxLabel.Text = "Observed Value:";
            _ScaledValueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _ApplyButton
            // 
            _ApplyButton.Location = new Point(146, 163);
            _ApplyButton.Name = "_ApplyButton";
            _ApplyButton.Size = new Size(122, 35);
            _ApplyButton.TabIndex = 10;
            _ApplyButton.Text = "Apply Edit Value";
            _ApplyButton.UseVisualStyleBackColor = true;
            // 
            // _EnterValueLabel
            // 
            _EnterValueLabel.Name = "_EnterValueLabel";
            _EnterValueLabel.Size = new Size(47, 28);
            _EnterValueLabel.Text = "Entered";
            // 
            // _SelectedValueLabel
            // 
            _SelectedValueLabel.Name = "_SelectedValueLabel";
            _SelectedValueLabel.Size = new Size(51, 28);
            _SelectedValueLabel.Text = "Selected";
            // 
            // _SelectorComboBox
            // 
            _SelectorComboBox.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _SelectorComboBox.DirtyBackColor = Color.Orange;
            _SelectorComboBox.DirtyForeColor = SystemColors.ActiveCaption;
            _SelectorComboBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _SelectorComboBox.Location = new Point(311, 122);
            _SelectorComboBox.Margin = new Padding(3, 4, 3, 4);
            _SelectorComboBox.Name = "_SelectorComboBox";
            _SelectorComboBox.SelectedText = null;
            _SelectorComboBox.SelectorIcon = (Image)resources.GetObject("_SelectorComboBox.SelectorIcon");
            _SelectorComboBox.Size = new Size(99, 25);
            _SelectorComboBox.TabIndex = 11;
            // 
            // _SelectButton
            // 
            _SelectButton.Location = new Point(311, 162);
            _SelectButton.Name = "_SelectButton";
            _SelectButton.Size = new Size(75, 36);
            _SelectButton.TabIndex = 12;
            _SelectButton.Text = "Select";
            _SelectButton.UseVisualStyleBackColor = true;
            // 
            // _EnterNumeric
            // 
            _EnterNumeric.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _EnterNumeric.ForeColor = SystemColors.WindowText;
            _EnterNumeric.Location = new Point(16, 122);
            _EnterNumeric.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
            _EnterNumeric.Minimum = new decimal(new int[] { 1000, 0, 0, (int)-2147483648L });
            _EnterNumeric.Name = "_EnterNumeric";
            _EnterNumeric.NullValue = new decimal(new int[] { 0, 0, 0, 0 });
            _EnterNumeric.ReadOnlyBackColor = Color.FromArgb(224, 224, 224);
            _EnterNumeric.ReadOnlyForeColor = Color.Black;
            _EnterNumeric.ReadWriteBackColor = SystemColors.Window;
            _EnterNumeric.ReadWriteForeColor = Color.Black;
            _EnterNumeric.ScaledValue = new decimal(new int[] { 0, 0, 0, 0 });
            _EnterNumeric.Size = new Size(120, 25);
            _EnterNumeric.TabIndex = 6;
            _EnterNumeric.UnscaledValue = new decimal(new int[] { 0, 0, 0, 0 });
            _EnterNumeric.UpDownDisplayMode = isr.Core.WinControls.UpDownButtonsDisplayMode.WhenMouseOver;
            _EnterNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _SelectorNumeric
            // 
            _SelectorNumeric.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _SelectorNumeric.DirtyBackColor = Color.Orange;
            _SelectorNumeric.DirtyForeColor = SystemColors.ActiveCaption;
            _SelectorNumeric.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _SelectorNumeric.Location = new Point(442, 122);
            _SelectorNumeric.Margin = new Padding(3, 4, 3, 4);
            _SelectorNumeric.Name = "_SelectorNumeric";
            _SelectorNumeric.SelectedText = null;
            _SelectorNumeric.SelectedValue = default;
            _SelectorNumeric.SelectorIcon = (Image)resources.GetObject("_SelectorNumeric.SelectorIcon");
            _SelectorNumeric.Size = new Size(76, 25);
            _SelectorNumeric.TabIndex = 13;
            _SelectorNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // ToolStrip1
            // 
            _ToolStrip.Items.AddRange(new ToolStripItem[] { _ToolStripSelectorComboBox, _ToolStripSelectorNumeric });
            _ToolStrip.Location = new Point(0, 0);
            _ToolStrip.Name = "ToolStrip1";
            _ToolStrip.Size = new Size(622, 28);
            _ToolStrip.TabIndex = 14;
            _ToolStrip.Text = "ToolStrip1";
            // 
            // _ToolStripSelectorComboBox
            // 
            _ToolStripSelectorComboBox.AutoSize = false;
            _ToolStripSelectorComboBox.DirtyBackColor = Color.Orange;
            _ToolStripSelectorComboBox.DirtyForeColor = SystemColors.ActiveCaption;
            _ToolStripSelectorComboBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _ToolStripSelectorComboBox.Name = "_ToolStripSelectorComboBox";
            _ToolStripSelectorComboBox.SelectedText = null;
            _ToolStripSelectorComboBox.SelectorIcon = (Image)resources.GetObject("_ToolStripSelectorComboBox.SelectorIcon");
            _ToolStripSelectorComboBox.Size = new Size(91, 25);
            // 
            // _ToolStripSelectorNumeric
            // 
            _ToolStripSelectorNumeric.AutoSize = false;
            _ToolStripSelectorNumeric.DirtyBackColor = Color.Orange;
            _ToolStripSelectorNumeric.DirtyForeColor = SystemColors.ActiveCaption;
            _ToolStripSelectorNumeric.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _ToolStripSelectorNumeric.Name = "_ToolStripSelectorNumeric";
            _ToolStripSelectorNumeric.SelectedText = null;
            _ToolStripSelectorNumeric.SelectorIcon = (Image)resources.GetObject("_ToolStripSelectorNumeric.SelectorIcon");
            _ToolStripSelectorNumeric.Size = new Size(74, 25);
            _ToolStripSelectorNumeric.Text = "0";
            _ToolStripSelectorNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // SelectorForm
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(622, 263);
            Controls.Add(_ToolStrip);
            Controls.Add(_SelectorNumeric);
            Controls.Add(_SelectButton);
            Controls.Add(_SelectorComboBox);
            Controls.Add(_ApplyButton);
            Controls.Add(_ReadOnlyCheckBox);
            Controls.Add(_EnterNumeric);
            Controls.Add(_SelectedTextBox);
            Controls.Add(_ScaledValueTextBoxLabel);
            Controls.Add(_EditedEngineeringUpDownLabel);
            Controls.Add(_EnterValueNumericLabel);
            Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            Margin = new Padding(3, 4, 3, 4);
            Name = "SelectorsForm";
            Text = "Selectors Form";
            ((System.ComponentModel.ISupportInitialize)_EnterNumeric).EndInit();
            _ToolStrip.ResumeLayout(false);
            _ToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolTip _ToolTip;
        private isr.Core.WinControls.EngineeringUpDown _EnterNumeric;
        private TextBox _SelectedTextBox;
        private CheckBox _ReadOnlyCheckBox;
        private Label _EnterValueNumericLabel;
        private Label _EditedEngineeringUpDownLabel;
        private Label _ScaledValueTextBoxLabel;
        private Button _ApplyButton;
        private ToolStripTextBox _EnterValueLabel;
        private ToolStripTextBox _SelectedValueLabel;
        private isr.Core.WinControls.SelectorComboBox _SelectorComboBox;
        private Button _SelectButton;
        private isr.Core.WinControls.SelectorNumeric _SelectorNumeric;
        private ToolStrip _ToolStrip;
        private isr.Core.WinControls.ToolStripSelectorComboBox _ToolStripSelectorComboBox;
        private isr.Core.WinControls.ToolStripSelectorNumeric _ToolStripSelectorNumeric;
    }
}
