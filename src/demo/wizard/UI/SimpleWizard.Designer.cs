using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.Testers
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class SimpleWizard
    {


        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._LicenseWizardPage = new isr.Core.WinControls.WizardPage();
            this._AgreeCheckBox = new System.Windows.Forms.CheckBox();
            this._LicenseTextBox = new System.Windows.Forms.TextBox();
            this._OptionsWizardPage = new isr.Core.WinControls.WizardPage();
            this._OptionsGroupBox = new System.Windows.Forms.GroupBox();
            this._SkipOptionRadioButton = new System.Windows.Forms.RadioButton();
            this._CheckOptionRadioButton = new System.Windows.Forms.RadioButton();
            this._FinishWizardPage = new isr.Core.WinControls.WizardPage();
            this._SampleWizard = new isr.Core.WinControls.Wizard();
            this._WelcomeWizardPage = new isr.Core.WinControls.WizardPage();
            this._ProgressWizardPage = new isr.Core.WinControls.WizardPage();
            this._ProgressLabel = new System.Windows.Forms.Label();
            this._LongTaskProgressBar = new System.Windows.Forms.ProgressBar();
            this._CheckWizardPage = new isr.Core.WinControls.WizardPage();
            this._PlaceholderLabel = new System.Windows.Forms.Label();
            this._LongTaskTimer = new System.Windows.Forms.Timer(this.components);
            this._LicenseWizardPage.SuspendLayout();
            this._OptionsWizardPage.SuspendLayout();
            this._OptionsGroupBox.SuspendLayout();
            this._SampleWizard.SuspendLayout();
            this._ProgressWizardPage.SuspendLayout();
            this._CheckWizardPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // _LicenseWizardPage
            // 
            this._LicenseWizardPage.Controls.Add(this._AgreeCheckBox);
            this._LicenseWizardPage.Controls.Add(this._LicenseTextBox);
            this._LicenseWizardPage.Description = "Please read the following license agreement and confirm that you agree with all terms and conditions.";
            this._LicenseWizardPage.Location = new System.Drawing.Point(0, 0);
            this._LicenseWizardPage.Name = "_LicenseWizardPage";
            this._LicenseWizardPage.Size = new System.Drawing.Size(428, 208);
            this._LicenseWizardPage.TabIndex = 10;
            this._LicenseWizardPage.Title = "License Agreement";
            // 
            // _AgreeCheckBox
            // 
            this._AgreeCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._AgreeCheckBox.Location = new System.Drawing.Point(14, 335);
            this._AgreeCheckBox.Name = "_AgreeCheckBox";
            this._AgreeCheckBox.Size = new System.Drawing.Size(346, 19);
            this._AgreeCheckBox.TabIndex = 0;
            this._AgreeCheckBox.Text = "I agree with this license\'s terms and conditions.";
            this._AgreeCheckBox.CheckedChanged += new System.EventHandler(this.CheckIAgree_CheckedChanged);
            // 
            // _LicenseTextBox
            // 
            this._LicenseTextBox.BackColor = System.Drawing.SystemColors.Window;
            this._LicenseTextBox.Location = new System.Drawing.Point(14, 94);
            this._LicenseTextBox.Multiline = true;
            this._LicenseTextBox.Name = "_LicenseTextBox";
            this._LicenseTextBox.ReadOnly = true;
            this._LicenseTextBox.Size = new System.Drawing.Size(528, 231);
            this._LicenseTextBox.TabIndex = 1;
            this._LicenseTextBox.Text = "Some long and boring license text...";
            // 
            // _OptionsWizardPage
            // 
            this._OptionsWizardPage.Controls.Add(this._OptionsGroupBox);
            this._OptionsWizardPage.Description = "Please select an option from the available list.";
            this._OptionsWizardPage.Location = new System.Drawing.Point(0, 0);
            this._OptionsWizardPage.Name = "_OptionsWizardPage";
            this._OptionsWizardPage.Size = new System.Drawing.Size(428, 208);
            this._OptionsWizardPage.TabIndex = 11;
            this._OptionsWizardPage.Title = "Task Options";
            // 
            // _OptionsGroupBox
            // 
            this._OptionsGroupBox.Controls.Add(this._SkipOptionRadioButton);
            this._OptionsGroupBox.Controls.Add(this._CheckOptionRadioButton);
            this._OptionsGroupBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._OptionsGroupBox.Location = new System.Drawing.Point(19, 103);
            this._OptionsGroupBox.Name = "_OptionsGroupBox";
            this._OptionsGroupBox.Size = new System.Drawing.Size(523, 138);
            this._OptionsGroupBox.TabIndex = 0;
            this._OptionsGroupBox.TabStop = false;
            this._OptionsGroupBox.Text = "Available Options";
            // 
            // _SkipOptionRadioButton
            // 
            this._SkipOptionRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._SkipOptionRadioButton.Location = new System.Drawing.Point(24, 44);
            this._SkipOptionRadioButton.Name = "_SkipOptionRadioButton";
            this._SkipOptionRadioButton.Size = new System.Drawing.Size(312, 25);
            this._SkipOptionRadioButton.TabIndex = 0;
            this._SkipOptionRadioButton.Text = "Skip any checks and proceed.";
            // 
            // _CheckOptionRadioButton
            // 
            this._CheckOptionRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._CheckOptionRadioButton.Location = new System.Drawing.Point(24, 89);
            this._CheckOptionRadioButton.Name = "_CheckOptionRadioButton";
            this._CheckOptionRadioButton.Size = new System.Drawing.Size(312, 24);
            this._CheckOptionRadioButton.TabIndex = 1;
            this._CheckOptionRadioButton.Text = "Check for something first.";
            // 
            // _FinishWizardPage
            // 
            this._FinishWizardPage.Description = "Thank you for using the Sample Wizard.\nPress OK to exit.";
            this._FinishWizardPage.Location = new System.Drawing.Point(0, 0);
            this._FinishWizardPage.Name = "_FinishWizardPage";
            this._FinishWizardPage.Size = new System.Drawing.Size(559, 375);
            this._FinishWizardPage.TabIndex = 12;
            this._FinishWizardPage.Title = "Sample Wizard has finished";
            this._FinishWizardPage.WizardPageStyle = isr.Core.WinControls.WizardPageStyle.Finish;
            // 
            // _SampleWizard
            // 
            this._SampleWizard.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._SampleWizard.Controls.Add(this._WelcomeWizardPage);
            this._SampleWizard.Controls.Add(this._FinishWizardPage);
            this._SampleWizard.Controls.Add(this._ProgressWizardPage);
            this._SampleWizard.Controls.Add(this._CheckWizardPage);
            this._SampleWizard.Controls.Add(this._OptionsWizardPage);
            this._SampleWizard.Controls.Add(this._LicenseWizardPage);
            this._SampleWizard.Dock = System.Windows.Forms.DockStyle.None;
            this._SampleWizard.FinishText = null;
            this._SampleWizard.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._SampleWizard.HeaderImage = global::isr.Core.Testers.Properties.Resources.HeaderIcon;
            this._SampleWizard.HelpVisible = true;
            this._SampleWizard.Location = new System.Drawing.Point(0, 0);
            this._SampleWizard.Name = "_SampleWizard";
            this._SampleWizard.Pages.AddRange(new isr.Core.WinControls.WizardPage[] {
            this._WelcomeWizardPage,
            this._LicenseWizardPage,
            this._OptionsWizardPage,
            this._CheckWizardPage,
            this._ProgressWizardPage,
            this._FinishWizardPage});
            this._SampleWizard.Size = new System.Drawing.Size(456, 326);
            this._SampleWizard.TabIndex = 0;
            this._SampleWizard.WelcomeImage = global::isr.Core.Testers.Properties.Resources.WelcomeImage;
            this._SampleWizard.PageChanging += new System.EventHandler<isr.Core.WinControls.PageChangingEventArgs>(this.SampleWizard_BeforeSwitchPages);
            this._SampleWizard.PageChanged += new System.EventHandler<isr.Core.WinControls.PageChangedEventArgs>(this.SampleWizard_AfterSwitchPages);
            this._SampleWizard.Cancel += new System.ComponentModel.CancelEventHandler(this.SampleWizard_Cancel);
            this._SampleWizard.Finish += new System.EventHandler(this.SampleWizard_Finish);
            this._SampleWizard.Help += new System.EventHandler(this.SampleWizard_Help);
            // 
            // _WelcomeWizardPage
            // 
            this._WelcomeWizardPage.Description = "This wizard will guide you through the steps of performing a sample task.";
            this._WelcomeWizardPage.Location = new System.Drawing.Point(0, 0);
            this._WelcomeWizardPage.Name = "_WelcomeWizardPage";
            this._WelcomeWizardPage.Size = new System.Drawing.Size(413, 254);
            this._WelcomeWizardPage.TabIndex = 9;
            this._WelcomeWizardPage.Title = "Welcome to the Sample  Wizard";
            this._WelcomeWizardPage.WizardPageStyle = isr.Core.WinControls.WizardPageStyle.Welcome;
            // 
            // _ProgressWizardPage
            // 
            this._ProgressWizardPage.Controls.Add(this._ProgressLabel);
            this._ProgressWizardPage.Controls.Add(this._LongTaskProgressBar);
            this._ProgressWizardPage.Description = "This simulates a long running sample task.";
            this._ProgressWizardPage.Location = new System.Drawing.Point(0, 0);
            this._ProgressWizardPage.Name = "_ProgressWizardPage";
            this._ProgressWizardPage.Size = new System.Drawing.Size(428, 208);
            this._ProgressWizardPage.TabIndex = 10;
            this._ProgressWizardPage.Title = "Task Running";
            // 
            // _ProgressLabel
            // 
            this._ProgressLabel.Location = new System.Drawing.Point(24, 103);
            this._ProgressLabel.Name = "_ProgressLabel";
            this._ProgressLabel.Size = new System.Drawing.Size(302, 20);
            this._ProgressLabel.TabIndex = 1;
            this._ProgressLabel.Text = "Please wait while the wizard does a long task...";
            // 
            // _LongTaskProgressBar
            // 
            this._LongTaskProgressBar.Location = new System.Drawing.Point(19, 128);
            this._LongTaskProgressBar.Name = "_LongTaskProgressBar";
            this._LongTaskProgressBar.Size = new System.Drawing.Size(523, 25);
            this._LongTaskProgressBar.TabIndex = 0;
            // 
            // _CheckWizardPage
            // 
            this._CheckWizardPage.Controls.Add(this._PlaceholderLabel);
            this._CheckWizardPage.Description = "Please enter required information.";
            this._CheckWizardPage.Location = new System.Drawing.Point(0, 0);
            this._CheckWizardPage.Name = "_CheckWizardPage";
            this._CheckWizardPage.Size = new System.Drawing.Size(428, 208);
            this._CheckWizardPage.TabIndex = 13;
            this._CheckWizardPage.Title = "Check Something";
            // 
            // _PlaceholderLabel
            // 
            this._PlaceholderLabel.ForeColor = System.Drawing.Color.Red;
            this._PlaceholderLabel.Location = new System.Drawing.Point(34, 123);
            this._PlaceholderLabel.Name = "_PlaceholderLabel";
            this._PlaceholderLabel.Size = new System.Drawing.Size(460, 20);
            this._PlaceholderLabel.TabIndex = 0;
            this._PlaceholderLabel.Text = "Place some validation controls here.";
            // 
            // _LongTaskTimer
            // 
            this._LongTaskTimer.Tick += new System.EventHandler(this.TimerTask_Tick);
            // 
            // SimpleWizard
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 16);
            this.ClientSize = new System.Drawing.Size(468, 338);
            this.Controls.Add(this._SampleWizard);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SimpleWizard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simple Wizard";
            this._LicenseWizardPage.ResumeLayout(false);
            this._LicenseWizardPage.PerformLayout();
            this._OptionsWizardPage.ResumeLayout(false);
            this._OptionsGroupBox.ResumeLayout(false);
            this._SampleWizard.ResumeLayout(false);
            this._ProgressWizardPage.ResumeLayout(false);
            this._CheckWizardPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private CheckBox _AgreeCheckBox;
        private TextBox _LicenseTextBox;
        private isr.Core.WinControls.WizardPage _LicenseWizardPage;
        private isr.Core.WinControls.WizardPage _OptionsWizardPage;
        private isr.Core.WinControls.WizardPage _FinishWizardPage;
        private isr.Core.WinControls.WizardPage _WelcomeWizardPage;
        private isr.Core.WinControls.WizardPage _ProgressWizardPage;
        private isr.Core.WinControls.Wizard _SampleWizard;
        private ProgressBar _LongTaskProgressBar;
        private Label _ProgressLabel;
        private Timer _LongTaskTimer;
        private GroupBox _OptionsGroupBox;
        private RadioButton _SkipOptionRadioButton;
        private RadioButton _CheckOptionRadioButton;
        private isr.Core.WinControls.WizardPage _CheckWizardPage;
        private Label _PlaceholderLabel;
    }
}
