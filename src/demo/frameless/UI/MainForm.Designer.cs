using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using isr.Core.WinControls;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Testers
{
    public partial class MainForm
    {
        /// <summary>
    /// Required designer variable.
    /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing )
            {
                components?.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            _TopPanel = new Panel();
            _TopPanel.MouseDown += new MouseEventHandler(TopPanel_MouseDown);
            _TopPanel.MouseMove += new MouseEventHandler(TopPanel_MouseMove);
            _TopPanel.MouseUp += new MouseEventHandler(TopPanel_MouseUp);
            _WindowTextLabel = new Label();
            _WindowTextLabel.MouseDown += new MouseEventHandler(WindowTextLabel_MouseDown);
            _WindowTextLabel.MouseMove += new MouseEventHandler(WindowTextLabel_MouseMove);
            _WindowTextLabel.MouseUp += new MouseEventHandler(WindowTextLabel_MouseUp);
            _MinButton = new ZopeButton();
            _MinButton.Click += new System.EventHandler(MinButton_Click);
            _CloseButton = new ZopeButton();
            _CloseButton.Click += new System.EventHandler(CloseButton_Click);
            _RightPanel = new Panel();
            _LeftPanel = new Panel();
            _BottomPanel = new Panel();
            _ExitButton = new ZopeButton();
            _ExitButton.Click += new System.EventHandler(ExitButton_Click);
            _OpenDashboardShapedButton = new ShapedButton();
            _OpenDashboardShapedButton.Click += new System.EventHandler(OpenDashboardShapedButton_Click);
            _OpenDarkFormShapedButton = new ShapedButton();
            _OpenDarkFormShapedButton.Click += new System.EventHandler(OpenDarkFormShapedButton_Click);
            _OpenBlueFormShapedButton = new ShapedButton();
            _OpenBlueFormShapedButton.Click += new System.EventHandler(OpenBlueFormShapedButton_Click);
            _TopPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _TopPanel
            // 
            _TopPanel.BackColor = Color.FromArgb(10, 100, 60);
            _TopPanel.Controls.Add(_WindowTextLabel);
            _TopPanel.Controls.Add(_MinButton);
            _TopPanel.Controls.Add(_CloseButton);
            _TopPanel.Dock = DockStyle.Top;
            _TopPanel.Location = new Point(0, 0);
            _TopPanel.Name = "_TopPanel";
            _TopPanel.Size = new Size(355, 52);
            _TopPanel.TabIndex = 0;
            // 
            // _WindowTextLabel
            // 
            _WindowTextLabel.AutoSize = true;
            _WindowTextLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _WindowTextLabel.ForeColor = Color.White;
            _WindowTextLabel.Location = new Point(21, 14);
            _WindowTextLabel.Name = "_WindowTextLabel";
            _WindowTextLabel.Size = new Size(80, 20);
            _WindowTextLabel.TabIndex = 4;
            _WindowTextLabel.Text = "Main Form";
            // 
            // _MinButton
            // 
            _MinButton.BusyBackColor = Color.FromArgb(10, 100, 60);
            _MinButton.DisplayText = "_";
            _MinButton.FlatStyle = FlatStyle.Flat;
            _MinButton.Font = new Font("Microsoft YaHei UI", 20.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _MinButton.ForeColor = Color.White;
            _MinButton.Location = new Point(289, 3);
            _MinButton.MouseClickColor = Color.FromArgb(160, 180, 200);
            _MinButton.MouseColorsEnabled = true;
            _MinButton.MouseHoverColor = Color.FromArgb(10, 80, 40);
            _MinButton.Name = "_MinButton";
            _MinButton.Size = new Size(31, 24);
            _MinButton.TabIndex = 3;
            _MinButton.Text = "_";
            _MinButton.TextLocationLeft = 6;
            _MinButton.TextLocationTop = -20;
            _MinButton.UseVisualStyleBackColor = true;
            // 
            // _CloseButton
            // 
            _CloseButton.BusyBackColor = Color.FromArgb(10, 100, 60);
            _CloseButton.DisplayText = "X";
            _CloseButton.FlatStyle = FlatStyle.Flat;
            _CloseButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _CloseButton.ForeColor = Color.White;
            _CloseButton.Location = new Point(320, 3);
            _CloseButton.MouseClickColor = Color.FromArgb(100, 150, 220);
            _CloseButton.MouseColorsEnabled = true;
            _CloseButton.MouseHoverColor = Color.FromArgb(10, 80, 40);
            _CloseButton.Name = "_CloseButton";
            _CloseButton.Size = new Size(31, 24);
            _CloseButton.TabIndex = 2;
            _CloseButton.Text = "X";
            _CloseButton.TextLocationLeft = 6;
            _CloseButton.TextLocationTop = -1;
            _CloseButton.UseVisualStyleBackColor = true;
            // 
            // _RightPanel
            // 
            _RightPanel.BackColor = Color.FromArgb(10, 100, 60);
            _RightPanel.Dock = DockStyle.Right;
            _RightPanel.Location = new Point(343, 52);
            _RightPanel.Name = "_RightPanel";
            _RightPanel.Size = new Size(12, 375);
            _RightPanel.TabIndex = 1;
            // 
            // _LeftPanel
            // 
            _LeftPanel.BackColor = Color.FromArgb(10, 100, 60);
            _LeftPanel.Dock = DockStyle.Left;
            _LeftPanel.Location = new Point(0, 52);
            _LeftPanel.Name = "_LeftPanel";
            _LeftPanel.Size = new Size(12, 375);
            _LeftPanel.TabIndex = 2;
            // 
            // _BottomPanel
            // 
            _BottomPanel.BackColor = Color.FromArgb(10, 100, 60);
            _BottomPanel.Dock = DockStyle.Bottom;
            _BottomPanel.Location = new Point(12, 415);
            _BottomPanel.Name = "_BottomPanel";
            _BottomPanel.Size = new Size(331, 12);
            _BottomPanel.TabIndex = 3;
            // 
            // _ExitButton
            // 
            _ExitButton.BusyBackColor = Color.FromArgb(10, 100, 60);
            _ExitButton.DisplayText = "Exit";
            _ExitButton.FlatStyle = FlatStyle.Flat;
            _ExitButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _ExitButton.ForeColor = Color.White;
            _ExitButton.Location = new Point(227, 367);
            _ExitButton.MouseClickColor = Color.FromArgb(100, 0, 0);
            _ExitButton.MouseColorsEnabled = true;
            _ExitButton.MouseHoverColor = Color.FromArgb(180, 30, 40);
            _ExitButton.Name = "_ExitButton";
            _ExitButton.Size = new Size(93, 29);
            _ExitButton.TabIndex = 7;
            _ExitButton.Text = "Exit";
            _ExitButton.TextLocationLeft = 26;
            _ExitButton.TextLocationTop = 3;
            _ExitButton.UseVisualStyleBackColor = true;
            // 
            // _OpenDashboardShapedButton
            // 
            _OpenDashboardShapedButton.BackColor = Color.Transparent;
            _OpenDashboardShapedButton.BorderColor = Color.Transparent;
            _OpenDashboardShapedButton.BorderWidth = 2;
            _OpenDashboardShapedButton.ButtonShape = ButtonShape.RoundRect;
            _OpenDashboardShapedButton.ButtonText = "Dashboard UI Form";
            _OpenDashboardShapedButton.EndColor = Color.FromArgb(80, 20, 40);
            _OpenDashboardShapedButton.EndOpacity = 250;
            _OpenDashboardShapedButton.FlatAppearance.BorderSize = 0;
            _OpenDashboardShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            _OpenDashboardShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _OpenDashboardShapedButton.FlatStyle = FlatStyle.Flat;
            _OpenDashboardShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _OpenDashboardShapedButton.ForeColor = Color.White;
            _OpenDashboardShapedButton.GradientAngle = 90;
            _OpenDashboardShapedButton.Location = new Point(25, 269);
            _OpenDashboardShapedButton.MouseClickStartColor = Color.FromArgb(20, 20, 40);
            _OpenDashboardShapedButton.MouseClickEndColor = Color.FromArgb(10, 100, 60);
            _OpenDashboardShapedButton.MouseHoverStartColor = Color.FromArgb(250, 100, 180);
            _OpenDashboardShapedButton.MouseHoverEndColor = Color.FromArgb(100, 40, 60);
            _OpenDashboardShapedButton.Name = "_OpenDashboardShapedButton";
            _OpenDashboardShapedButton.ShowButtonText = true;
            _OpenDashboardShapedButton.Size = new Size(312, 75);
            _OpenDashboardShapedButton.StartColor = Color.FromArgb(220, 40, 120);
            _OpenDashboardShapedButton.StartOpacity = 250;
            _OpenDashboardShapedButton.TabIndex = 6;
            _OpenDashboardShapedButton.Text = "Dashboard UI Form";
            _OpenDashboardShapedButton.TextLocation = new Point(90, 25);
            _OpenDashboardShapedButton.UseVisualStyleBackColor = false;
            // 
            // _OpenDarkFormShapedButton
            // 
            _OpenDarkFormShapedButton.BackColor = Color.Transparent;
            _OpenDarkFormShapedButton.BorderColor = Color.Transparent;
            _OpenDarkFormShapedButton.BorderWidth = 2;
            _OpenDarkFormShapedButton.ButtonShape = ButtonShape.RoundRect;
            _OpenDarkFormShapedButton.ButtonText = "Dark Custom Form";
            _OpenDarkFormShapedButton.EndColor = Color.FromArgb(10, 10, 10);
            _OpenDarkFormShapedButton.EndOpacity = 250;
            _OpenDarkFormShapedButton.FlatAppearance.BorderSize = 0;
            _OpenDarkFormShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            _OpenDarkFormShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _OpenDarkFormShapedButton.FlatStyle = FlatStyle.Flat;
            _OpenDarkFormShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _OpenDarkFormShapedButton.ForeColor = Color.White;
            _OpenDarkFormShapedButton.GradientAngle = 90;
            _OpenDarkFormShapedButton.Location = new Point(25, 172);
            _OpenDarkFormShapedButton.MouseClickStartColor = Color.FromArgb(20, 20, 40);
            _OpenDarkFormShapedButton.MouseClickEndColor = Color.FromArgb(10, 100, 60);
            _OpenDarkFormShapedButton.MouseHoverStartColor = Color.FromArgb(150, 150, 150);
            _OpenDarkFormShapedButton.MouseHoverEndColor = Color.FromArgb(50, 50, 50);
            _OpenDarkFormShapedButton.Name = "_OpenDarkFormShapedButton";
            _OpenDarkFormShapedButton.ShowButtonText = true;
            _OpenDarkFormShapedButton.Size = new Size(312, 75);
            _OpenDarkFormShapedButton.StartColor = Color.FromArgb(100, 100, 100);
            _OpenDarkFormShapedButton.StartOpacity = 250;
            _OpenDarkFormShapedButton.TabIndex = 5;
            _OpenDarkFormShapedButton.Text = "shapedButton2";
            _OpenDarkFormShapedButton.TextLocation = new Point(90, 25);
            _OpenDarkFormShapedButton.UseVisualStyleBackColor = false;
            // 
            // _OpenBlueFormShapedButton
            // 
            _OpenBlueFormShapedButton.BackColor = Color.Transparent;
            _OpenBlueFormShapedButton.BorderColor = Color.Transparent;
            _OpenBlueFormShapedButton.BorderWidth = 2;
            _OpenBlueFormShapedButton.ButtonShape = ButtonShape.RoundRect;
            _OpenBlueFormShapedButton.ButtonText = "Simple Blue Form";
            _OpenBlueFormShapedButton.EndColor = Color.MidnightBlue;
            _OpenBlueFormShapedButton.EndOpacity = 250;
            _OpenBlueFormShapedButton.FlatAppearance.BorderSize = 0;
            _OpenBlueFormShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            _OpenBlueFormShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _OpenBlueFormShapedButton.FlatStyle = FlatStyle.Flat;
            _OpenBlueFormShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _OpenBlueFormShapedButton.ForeColor = Color.White;
            _OpenBlueFormShapedButton.GradientAngle = 90;
            _OpenBlueFormShapedButton.Location = new Point(25, 77);
            _OpenBlueFormShapedButton.MouseClickStartColor = Color.FromArgb(20, 20, 40);
            _OpenBlueFormShapedButton.MouseClickEndColor = Color.FromArgb(10, 100, 60);
            _OpenBlueFormShapedButton.MouseHoverStartColor = Color.Turquoise;
            _OpenBlueFormShapedButton.MouseHoverEndColor = Color.DarkSlateGray;
            _OpenBlueFormShapedButton.Name = "_OpenBlueFormShapedButton";
            _OpenBlueFormShapedButton.ShowButtonText = true;
            _OpenBlueFormShapedButton.Size = new Size(312, 75);
            _OpenBlueFormShapedButton.StartColor = Color.DodgerBlue;
            _OpenBlueFormShapedButton.StartOpacity = 250;
            _OpenBlueFormShapedButton.TabIndex = 4;
            _OpenBlueFormShapedButton.Text = "Simple Blue Form";
            _OpenBlueFormShapedButton.TextLocation = new Point(90, 26);
            _OpenBlueFormShapedButton.UseVisualStyleBackColor = false;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(40, 180, 100);
            ClientSize = new Size(355, 427);
            Controls.Add(_ExitButton);
            Controls.Add(_OpenDashboardShapedButton);
            Controls.Add(_OpenDarkFormShapedButton);
            Controls.Add(_OpenBlueFormShapedButton);
            Controls.Add(_BottomPanel);
            Controls.Add(_LeftPanel);
            Controls.Add(_RightPanel);
            Controls.Add(_TopPanel);
            FormBorderStyle = FormBorderStyle.None;
            Name = "MainForm";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Main Form";
            _TopPanel.ResumeLayout(false);
            _TopPanel.PerformLayout();
            ResumeLayout(false);
        }

        private Panel _TopPanel;
        private ZopeButton _MinButton;
        private ZopeButton _CloseButton;
        private Label _WindowTextLabel;
        private Panel _RightPanel;
        private Panel _LeftPanel;
        private Panel _BottomPanel;
        private ShapedButton _OpenBlueFormShapedButton;
        private ShapedButton _OpenDarkFormShapedButton;
        private ShapedButton _OpenDashboardShapedButton;
        private ZopeButton _ExitButton;
    }
}
