using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.WinControls;

namespace isr.Core.Testers
{
    public partial class BlackForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing )
            {
                components?.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _TopBorderPanel = new Panel();
            _TopBorderPanel.MouseDown += new MouseEventHandler(TopBorderPanel_MouseDown);
            _TopBorderPanel.MouseMove += new MouseEventHandler(TopBorderPanel_MouseMove);
            _TopBorderPanel.MouseUp += new MouseEventHandler(TopBorderPanel_MouseUp);
            _RightBorderPanel = new Panel();
            _RightBorderPanel.MouseDown += new MouseEventHandler(RightPanel_MouseDown);
            _RightBorderPanel.MouseMove += new MouseEventHandler(RightPanel_MouseMove);
            _RightBorderPanel.MouseUp += new MouseEventHandler(RightPanel_MouseUp);
            _LeftBorderPanel = new Panel();
            _LeftBorderPanel.MouseDown += new MouseEventHandler(LeftPanel_MouseDown);
            _LeftBorderPanel.MouseMove += new MouseEventHandler(LeftPanel_MouseMove);
            _LeftBorderPanel.MouseUp += new MouseEventHandler(LeftPanel_MouseUp);
            _BottomBorderPanel = new Panel();
            _BottomBorderPanel.MouseDown += new MouseEventHandler(BottomPanel_MouseDown);
            _BottomBorderPanel.MouseMove += new MouseEventHandler(BottomPanel_MouseMove);
            _BottomBorderPanel.MouseUp += new MouseEventHandler(BottomPanel_MouseUp);
            _TopPanel = new Panel();
            _TopPanel.MouseDown += new MouseEventHandler(TopPanel_MouseDown);
            _TopPanel.MouseMove += new MouseEventHandler(TopPanel_MouseMove);
            _TopPanel.MouseUp += new MouseEventHandler(TopPanel_MouseUp);
            _MinButton = new ZopeButton();
            _MinButton.Click += new System.EventHandler(MinButton_Click);
            _MaxButton = new MinMaxButton();
            _MaxButton.Click += new System.EventHandler(MaxButton_Click);
            _WindowTextLabel = new Label();
            _WindowTextLabel.MouseDown += new MouseEventHandler(WindowTextLabel_MouseDown);
            _WindowTextLabel.MouseMove += new MouseEventHandler(WindowTextLabel_MouseMove);
            _WindowTextLabel.MouseUp += new MouseEventHandler(WindowTextLabel_MouseUp);
            _CloseButton = new ZopeButton();
            _CloseButton.Click += new System.EventHandler(CloseButton_Click);
            _MenuStrip = new ZopeMenuStrip();
            _FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _NewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _OpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ToolStripSeparator1 = new ToolStripSeparator();
            _SaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _SaveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ToolStripSeparator2 = new ToolStripSeparator();
            _CloseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _CloseAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ToolStripSeparator3 = new ToolStripSeparator();
            _PrintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _PrintPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ToolStripSeparator4 = new ToolStripSeparator();
            _CloseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            _EditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _CutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _CopyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _PasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ToolStripSeparator5 = new ToolStripSeparator();
            _UnduToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _RedoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ToolStripSeparator6 = new ToolStripSeparator();
            _FindToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ReplaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _SelectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _HelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _HelpContentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _OnlineHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _BuyNowShapedButton = new ShapedButton();
            _BottomBorderRightPanel = new Panel();
            _BottomBorderRightPanel.MouseDown += new MouseEventHandler(RightBottomPanel_1_MouseDown);
            _BottomBorderRightPanel.MouseMove += new MouseEventHandler(RightBottomPanel_1_MouseMove);
            _BottomBorderRightPanel.MouseUp += new MouseEventHandler(RightBottomPanel_1_MouseUp);
            _BottomPanel = new Panel();
            _TimesShapedButton = new ShapedButton();
            _TestShapedButton = new ShapedButton();
            _AddShapedButton = new ShapedButton();
            _ToolsShapedButton = new ShapedButton();
            _ToolTip = new ToolTip(components);
            _RightBorderBottomPanel = new Panel();
            _RightBorderBottomPanel.MouseDown += new MouseEventHandler(RightBottomPanel_2_MouseDown);
            _RightBorderBottomPanel.MouseMove += new MouseEventHandler(RightBottomPanel_2_MouseMove);
            _RightBorderBottomPanel.MouseUp += new MouseEventHandler(RightBottomPanel_2_MouseUp);
            _BottomBorderLeftPanel = new Panel();
            _BottomBorderLeftPanel.MouseDown += new MouseEventHandler(LeftBottomPanel_1_MouseDown);
            _BottomBorderLeftPanel.MouseMove += new MouseEventHandler(LeftBottomPanel_1_MouseMove);
            _BottomBorderLeftPanel.MouseUp += new MouseEventHandler(LeftBottomPanel_1_MouseUp);
            _LeftBorderBottomPanel = new Panel();
            _LeftBorderBottomPanel.MouseDown += new MouseEventHandler(LeftBottomPanel_2_MouseDown);
            _LeftBorderBottomPanel.MouseMove += new MouseEventHandler(LeftBottomPanel_2_MouseMove);
            _LeftBorderBottomPanel.MouseUp += new MouseEventHandler(LeftBottomPanel_2_MouseUp);
            _RightBorderTopPanel = new Panel();
            _RightBorderTopPanel.MouseDown += new MouseEventHandler(RightTopPanel_1_MouseDown);
            _RightBorderTopPanel.MouseMove += new MouseEventHandler(RightTopPanel_1_MouseMove);
            _RightBorderTopPanel.MouseUp += new MouseEventHandler(RightTopPanel_1_MouseUp);
            _TopBorderRightPanel = new Panel();
            _TopBorderRightPanel.MouseDown += new MouseEventHandler(RightTopPanel_2_MouseDown);
            _TopBorderRightPanel.MouseMove += new MouseEventHandler(RightTopPanel_2_MouseMove);
            _TopBorderRightPanel.MouseUp += new MouseEventHandler(RightTopPanel_2_MouseUp);
            _TopBorderLeftPanel = new Panel();
            _TopBorderLeftPanel.MouseDown += new MouseEventHandler(LeftTopPanel_1_MouseDown);
            _TopBorderLeftPanel.MouseMove += new MouseEventHandler(LeftTopPanel_1_MouseMove);
            _TopBorderLeftPanel.MouseUp += new MouseEventHandler(LeftTopPanel_1_MouseUp);
            _LeftBorderTopPanel = new Panel();
            _LeftBorderTopPanel.MouseDown += new MouseEventHandler(LeftTopPanel_2_MouseDown);
            _LeftBorderTopPanel.MouseMove += new MouseEventHandler(LeftTopPanel_2_MouseMove);
            _LeftBorderTopPanel.MouseUp += new MouseEventHandler(LeftTopPanel_2_MouseUp);
            _LeftPanel = new Panel();
            _HelpButton = new ZopeButton();
            _HelpButton.Click += new System.EventHandler(Help_button_Click);
            _RunButton = new ZopeButton();
            _RunButton.Click += new System.EventHandler(Run_button_Click);
            _ViewButton = new ZopeButton();
            _ViewButton.Click += new System.EventHandler(View_button_Click);
            _EditButton = new ZopeButton();
            _EditButton.Click += new System.EventHandler(Edit_button_Click);
            _FileButton = new ZopeButton();
            _FileButton.Click += new System.EventHandler(File_button_Click);
            _SeparatorPanel = new Panel();
            _InformationLabel = new Label();
            _TopPanel.SuspendLayout();
            _MenuStrip.SuspendLayout();
            _BottomPanel.SuspendLayout();
            _LeftPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _TopBorderPanel
            // 
            _TopBorderPanel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _TopBorderPanel.BackColor = Color.Black;
            _TopBorderPanel.Cursor = Cursors.SizeNS;
            _TopBorderPanel.Location = new Point(20, 0);
            _TopBorderPanel.Name = "_TopBorderPanel";
            _TopBorderPanel.Size = new Size(690, 2);
            _TopBorderPanel.TabIndex = 0;
            // 
            // _RightBorderPanel
            // 
            _RightBorderPanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            _RightBorderPanel.BackColor = Color.Black;
            _RightBorderPanel.Cursor = Cursors.SizeWE;
            _RightBorderPanel.Location = new Point(728, 22);
            _RightBorderPanel.Name = "_RightBorderPanel";
            _RightBorderPanel.Size = new Size(2, 430);
            _RightBorderPanel.TabIndex = 1;
            // 
            // _LeftBorderPanel
            // 
            _LeftBorderPanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
            _LeftBorderPanel.BackColor = Color.Black;
            _LeftBorderPanel.Cursor = Cursors.SizeWE;
            _LeftBorderPanel.Location = new Point(0, 20);
            _LeftBorderPanel.Name = "_LeftBorderPanel";
            _LeftBorderPanel.Size = new Size(2, 430);
            _LeftBorderPanel.TabIndex = 2;
            // 
            // _BottomBorderPanel
            // 
            _BottomBorderPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _BottomBorderPanel.BackColor = Color.Black;
            _BottomBorderPanel.Cursor = Cursors.SizeNS;
            _BottomBorderPanel.Location = new Point(15, 471);
            _BottomBorderPanel.Name = "_BottomBorderPanel";
            _BottomBorderPanel.Size = new Size(692, 2);
            _BottomBorderPanel.TabIndex = 3;
            // 
            // _TopPanel
            // 
            _TopPanel.BackColor = Color.FromArgb(30, 30, 30);
            _TopPanel.Controls.Add(_MinButton);
            _TopPanel.Controls.Add(_MaxButton);
            _TopPanel.Controls.Add(_WindowTextLabel);
            _TopPanel.Controls.Add(_CloseButton);
            _TopPanel.Controls.Add(_MenuStrip);
            _TopPanel.Dock = DockStyle.Top;
            _TopPanel.Location = new Point(0, 0);
            _TopPanel.Name = "_TopPanel";
            _TopPanel.Size = new Size(730, 76);
            _TopPanel.TabIndex = 4;
            // 
            // _MinButton
            // 
            _MinButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _MinButton.BusyBackColor = Color.FromArgb(30, 30, 30);
            _MinButton.DisplayText = "_";
            _MinButton.FlatStyle = FlatStyle.Flat;
            _MinButton.Font = new Font("Microsoft YaHei UI", 20.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _MinButton.ForeColor = Color.White;
            _MinButton.Location = new Point(632, 6);
            _MinButton.MouseClickColor = Color.FromArgb(60, 60, 160);
            _MinButton.MouseColorsEnabled = true;
            _MinButton.MouseHoverColor = Color.FromArgb(50, 50, 50);
            _MinButton.Name = "_MinButton";
            _MinButton.Size = new Size(31, 24);
            _MinButton.TabIndex = 4;
            _MinButton.Text = "_";
            _MinButton.TextLocationLeft = 6;
            _MinButton.TextLocationTop = -20;
            _ToolTip.SetToolTip(_MinButton, "Minimize");
            _MinButton.UseVisualStyleBackColor = true;
            // 
            // _MaxButton
            // 
            _MaxButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _MaxButton.BusyBackColor = Color.FromArgb(30, 30, 30);
            _MaxButton.CustomFormState = CustomFormState.Normal;
            _MaxButton.DisplayText = "_";
            _MaxButton.FlatStyle = FlatStyle.Flat;
            _MaxButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _MaxButton.ForeColor = Color.White;
            _MaxButton.Location = new Point(663, 6);
            _MaxButton.MouseClickColor = Color.FromArgb(60, 60, 160);
            _MaxButton.MouseHoverColor = Color.FromArgb(50, 50, 50);
            _MaxButton.Name = "_MaxButton";
            _MaxButton.Size = new Size(31, 24);
            _MaxButton.TabIndex = 2;
            _MaxButton.Text = "minMaxButton1";
            _MaxButton.TextLocation = new Point(8, 6);
            _ToolTip.SetToolTip(_MaxButton, "Maximize");
            _MaxButton.UseVisualStyleBackColor = true;
            // 
            // _WindowTextLabel
            // 
            _WindowTextLabel.AutoSize = true;
            _WindowTextLabel.Font = new Font("Microsoft Sans Serif", 26.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _WindowTextLabel.ForeColor = Color.White;
            _WindowTextLabel.Location = new Point(28, 21);
            _WindowTextLabel.Name = "_WindowTextLabel";
            _WindowTextLabel.Size = new Size(134, 39);
            _WindowTextLabel.TabIndex = 1;
            _WindowTextLabel.Text = "My App";
            // 
            // _CloseButton
            // 
            _CloseButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _CloseButton.BusyBackColor = Color.FromArgb(30, 30, 30);
            _CloseButton.DisplayText = "X";
            _CloseButton.FlatStyle = FlatStyle.Flat;
            _CloseButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _CloseButton.ForeColor = Color.White;
            _CloseButton.Location = new Point(694, 6);
            _CloseButton.MouseClickColor = Color.FromArgb(60, 60, 160);
            _CloseButton.MouseColorsEnabled = true;
            _CloseButton.MouseHoverColor = Color.FromArgb(50, 50, 50);
            _CloseButton.Name = "_CloseButton";
            _CloseButton.Size = new Size(31, 24);
            _CloseButton.TabIndex = 0;
            _CloseButton.Text = "X";
            _CloseButton.TextLocationLeft = 6;
            _CloseButton.TextLocationTop = 1;
            _ToolTip.SetToolTip(_CloseButton, "Close");
            _CloseButton.UseVisualStyleBackColor = true;
            // 
            // _MenuStrip
            // 
            _MenuStrip.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _MenuStrip.Dock = DockStyle.None;
            _MenuStrip.Font = new Font("Segoe UI", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _MenuStrip.Items.AddRange(new ToolStripItem[] { _FileToolStripMenuItem, _EditToolStripMenuItem, _HelpToolStripMenuItem });
            _MenuStrip.Location = new Point(421, 32);
            _MenuStrip.Name = "_MenuStrip";
            _MenuStrip.Size = new Size(200, 28);
            _MenuStrip.TabIndex = 19;
            _MenuStrip.Text = "menuStripZ1";
            // 
            // _FileToolStripMenuItem
            // 
            _FileToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _NewToolStripMenuItem, _OpenToolStripMenuItem, _ToolStripSeparator1, _SaveToolStripMenuItem, _SaveAsToolStripMenuItem, _ToolStripSeparator2, _CloseToolStripMenuItem, _CloseAllToolStripMenuItem, _ToolStripSeparator3, _PrintToolStripMenuItem, _PrintPreviewToolStripMenuItem, _ToolStripSeparator4, _CloseToolStripMenuItem1 });
            _FileToolStripMenuItem.ForeColor = Color.White;
            _FileToolStripMenuItem.Name = "_FileToolStripMenuItem";
            _FileToolStripMenuItem.Size = new Size(60, 24);
            _FileToolStripMenuItem.Text = "  File  ";
            // 
            // _NewToolStripMenuItem
            // 
            _NewToolStripMenuItem.ForeColor = Color.White;
            _NewToolStripMenuItem.Name = "_NewToolStripMenuItem";
            _NewToolStripMenuItem.Size = new Size(240, 24);
            _NewToolStripMenuItem.Text = "New                                 ";
            // 
            // _OpenToolStripMenuItem
            // 
            _OpenToolStripMenuItem.ForeColor = Color.White;
            _OpenToolStripMenuItem.Name = "_OpenToolStripMenuItem";
            _OpenToolStripMenuItem.Size = new Size(240, 24);
            _OpenToolStripMenuItem.Text = "Open";
            // 
            // _ToolStripSeparator1
            // 
            _ToolStripSeparator1.Name = "_ToolStripSeparator1";
            _ToolStripSeparator1.Size = new Size(237, 6);
            // 
            // _SaveToolStripMenuItem
            // 
            _SaveToolStripMenuItem.ForeColor = Color.White;
            _SaveToolStripMenuItem.Name = "_SaveToolStripMenuItem";
            _SaveToolStripMenuItem.Size = new Size(240, 24);
            _SaveToolStripMenuItem.Text = "Save";
            // 
            // _SaveAsToolStripMenuItem
            // 
            _SaveAsToolStripMenuItem.ForeColor = Color.White;
            _SaveAsToolStripMenuItem.Name = "_SaveAsToolStripMenuItem";
            _SaveAsToolStripMenuItem.Size = new Size(240, 24);
            _SaveAsToolStripMenuItem.Text = "Save As";
            // 
            // _ToolStripSeparator2
            // 
            _ToolStripSeparator2.Name = "_ToolStripSeparator2";
            _ToolStripSeparator2.Size = new Size(237, 6);
            // 
            // _CloseToolStripMenuItem
            // 
            _CloseToolStripMenuItem.ForeColor = Color.White;
            _CloseToolStripMenuItem.Name = "_CloseToolStripMenuItem";
            _CloseToolStripMenuItem.Size = new Size(240, 24);
            _CloseToolStripMenuItem.Text = "Close";
            // 
            // _CloseAllToolStripMenuItem
            // 
            _CloseAllToolStripMenuItem.ForeColor = Color.White;
            _CloseAllToolStripMenuItem.Name = "_CloseAllToolStripMenuItem";
            _CloseAllToolStripMenuItem.Size = new Size(240, 24);
            _CloseAllToolStripMenuItem.Text = "Close All";
            // 
            // _ToolStripSeparator3
            // 
            _ToolStripSeparator3.Name = "_ToolStripSeparator3";
            _ToolStripSeparator3.Size = new Size(237, 6);
            // 
            // _PrintToolStripMenuItem
            // 
            _PrintToolStripMenuItem.ForeColor = Color.White;
            _PrintToolStripMenuItem.Name = "_PrintToolStripMenuItem";
            _PrintToolStripMenuItem.Size = new Size(240, 24);
            _PrintToolStripMenuItem.Text = "Print";
            // 
            // _PrintPreviewToolStripMenuItem
            // 
            _PrintPreviewToolStripMenuItem.ForeColor = Color.White;
            _PrintPreviewToolStripMenuItem.Name = "_PrintPreviewToolStripMenuItem";
            _PrintPreviewToolStripMenuItem.Size = new Size(240, 24);
            _PrintPreviewToolStripMenuItem.Text = "Print Preview";
            // 
            // _ToolStripSeparator4
            // 
            _ToolStripSeparator4.Name = "_ToolStripSeparator4";
            _ToolStripSeparator4.Size = new Size(237, 6);
            // 
            // _CloseToolStripMenuItem1
            // 
            _CloseToolStripMenuItem1.ForeColor = Color.White;
            _CloseToolStripMenuItem1.Name = "_CloseToolStripMenuItem1";
            _CloseToolStripMenuItem1.Size = new Size(240, 24);
            _CloseToolStripMenuItem1.Text = "Close";
            // 
            // _EditToolStripMenuItem
            // 
            _EditToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _CutToolStripMenuItem, _CopyToolStripMenuItem, _PasteToolStripMenuItem, _ToolStripSeparator5, _UnduToolStripMenuItem, _RedoToolStripMenuItem, _ToolStripSeparator6, _FindToolStripMenuItem, _ReplaceToolStripMenuItem, _SelectAllToolStripMenuItem });
            _EditToolStripMenuItem.ForeColor = Color.White;
            _EditToolStripMenuItem.Name = "_EditToolStripMenuItem";
            _EditToolStripMenuItem.Size = new Size(63, 24);
            _EditToolStripMenuItem.Text = "  Edit  ";
            // 
            // _CutToolStripMenuItem
            // 
            _CutToolStripMenuItem.ForeColor = Color.White;
            _CutToolStripMenuItem.Name = "_CutToolStripMenuItem";
            _CutToolStripMenuItem.Size = new Size(216, 24);
            _CutToolStripMenuItem.Text = "Cut                             ";
            // 
            // _CopyToolStripMenuItem
            // 
            _CopyToolStripMenuItem.ForeColor = Color.White;
            _CopyToolStripMenuItem.Name = "_CopyToolStripMenuItem";
            _CopyToolStripMenuItem.Size = new Size(216, 24);
            _CopyToolStripMenuItem.Text = "Copy";
            // 
            // _PasteToolStripMenuItem
            // 
            _PasteToolStripMenuItem.ForeColor = Color.White;
            _PasteToolStripMenuItem.Name = "_PasteToolStripMenuItem";
            _PasteToolStripMenuItem.Size = new Size(216, 24);
            _PasteToolStripMenuItem.Text = "Paste";
            // 
            // _ToolStripSeparator5
            // 
            _ToolStripSeparator5.Name = "_ToolStripSeparator5";
            _ToolStripSeparator5.Size = new Size(213, 6);
            // 
            // _UnduToolStripMenuItem
            // 
            _UnduToolStripMenuItem.ForeColor = Color.White;
            _UnduToolStripMenuItem.Name = "_UnduToolStripMenuItem";
            _UnduToolStripMenuItem.Size = new Size(216, 24);
            _UnduToolStripMenuItem.Text = "Undo";
            // 
            // _RedoToolStripMenuItem
            // 
            _RedoToolStripMenuItem.ForeColor = Color.White;
            _RedoToolStripMenuItem.Name = "_RedoToolStripMenuItem";
            _RedoToolStripMenuItem.Size = new Size(216, 24);
            _RedoToolStripMenuItem.Text = "Redo";
            // 
            // _ToolStripSeparator6
            // 
            _ToolStripSeparator6.Name = "_ToolStripSeparator6";
            _ToolStripSeparator6.Size = new Size(213, 6);
            // 
            // _FindToolStripMenuItem
            // 
            _FindToolStripMenuItem.ForeColor = Color.White;
            _FindToolStripMenuItem.Name = "_FindToolStripMenuItem";
            _FindToolStripMenuItem.Size = new Size(216, 24);
            _FindToolStripMenuItem.Text = "Find";
            // 
            // _ReplaceToolStripMenuItem
            // 
            _ReplaceToolStripMenuItem.ForeColor = Color.White;
            _ReplaceToolStripMenuItem.Name = "_ReplaceToolStripMenuItem";
            _ReplaceToolStripMenuItem.Size = new Size(216, 24);
            _ReplaceToolStripMenuItem.Text = "Replace";
            // 
            // _SelectAllToolStripMenuItem
            // 
            _SelectAllToolStripMenuItem.ForeColor = Color.White;
            _SelectAllToolStripMenuItem.Name = "_SelectAllToolStripMenuItem";
            _SelectAllToolStripMenuItem.Size = new Size(216, 24);
            _SelectAllToolStripMenuItem.Text = "Select All";
            // 
            // _HelpToolStripMenuItem
            // 
            _HelpToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { _HelpContentsToolStripMenuItem, _OnlineHelpToolStripMenuItem, _AboutToolStripMenuItem });
            _HelpToolStripMenuItem.ForeColor = Color.White;
            _HelpToolStripMenuItem.Name = "_HelpToolStripMenuItem";
            _HelpToolStripMenuItem.Size = new Size(69, 24);
            _HelpToolStripMenuItem.Text = "  Help  ";
            // 
            // _HelpContentsToolStripMenuItem
            // 
            _HelpContentsToolStripMenuItem.ForeColor = Color.White;
            _HelpContentsToolStripMenuItem.Name = "_HelpContentsToolStripMenuItem";
            _HelpContentsToolStripMenuItem.Size = new Size(172, 24);
            _HelpContentsToolStripMenuItem.Text = "Help Contents";
            // 
            // _OnlineHelpToolStripMenuItem
            // 
            _OnlineHelpToolStripMenuItem.ForeColor = Color.White;
            _OnlineHelpToolStripMenuItem.Name = "_OnlineHelpToolStripMenuItem";
            _OnlineHelpToolStripMenuItem.Size = new Size(172, 24);
            _OnlineHelpToolStripMenuItem.Text = "Online Help";
            // 
            // _AboutToolStripMenuItem
            // 
            _AboutToolStripMenuItem.ForeColor = Color.White;
            _AboutToolStripMenuItem.Name = "_AboutToolStripMenuItem";
            _AboutToolStripMenuItem.Size = new Size(172, 24);
            _AboutToolStripMenuItem.Text = "About";
            // 
            // _BuyNowShapedButton
            // 
            _BuyNowShapedButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _BuyNowShapedButton.BackColor = Color.Transparent;
            _BuyNowShapedButton.BorderColor = Color.Transparent;
            _BuyNowShapedButton.BorderWidth = 2;
            _BuyNowShapedButton.ButtonShape = ButtonShape.RoundRect;
            _BuyNowShapedButton.ButtonText = "Buy Now";
            _BuyNowShapedButton.EndColor = Color.FromArgb(20, 20, 40);
            _BuyNowShapedButton.EndOpacity = 250;
            _BuyNowShapedButton.FlatAppearance.BorderSize = 0;
            _BuyNowShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            _BuyNowShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _BuyNowShapedButton.FlatStyle = FlatStyle.Flat;
            _BuyNowShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _BuyNowShapedButton.ForeColor = Color.White;
            _BuyNowShapedButton.GradientAngle = 90;
            _BuyNowShapedButton.Location = new Point(427, 10);
            _BuyNowShapedButton.MouseClickStartColor = Color.Black;
            _BuyNowShapedButton.MouseClickEndColor = Color.Black;
            _BuyNowShapedButton.MouseHoverStartColor = Color.FromArgb(10, 10, 30);
            _BuyNowShapedButton.MouseHoverEndColor = Color.FromArgb(20, 20, 80);
            _BuyNowShapedButton.Name = "_BuyNowShapedButton";
            _BuyNowShapedButton.ShowButtonText = true;
            _BuyNowShapedButton.Size = new Size(145, 54);
            _BuyNowShapedButton.StartColor = Color.FromArgb(30, 70, 190);
            _BuyNowShapedButton.StartOpacity = 250;
            _BuyNowShapedButton.TabIndex = 3;
            _BuyNowShapedButton.Text = "shapedButton2";
            _BuyNowShapedButton.TextLocation = new Point(40, 18);
            _BuyNowShapedButton.UseVisualStyleBackColor = false;
            // 
            // _BottomBorderRightPanel
            // 
            _BottomBorderRightPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _BottomBorderRightPanel.BackColor = Color.Black;
            _BottomBorderRightPanel.Cursor = Cursors.SizeNWSE;
            _BottomBorderRightPanel.Location = new Point(710, 471);
            _BottomBorderRightPanel.Name = "_BottomBorderRightPanel";
            _BottomBorderRightPanel.Size = new Size(19, 2);
            _BottomBorderRightPanel.TabIndex = 5;
            // 
            // _BottomPanel
            // 
            _BottomPanel.BackColor = Color.FromArgb(30, 30, 30);
            _BottomPanel.Controls.Add(_TimesShapedButton);
            _BottomPanel.Controls.Add(_BuyNowShapedButton);
            _BottomPanel.Controls.Add(_TestShapedButton);
            _BottomPanel.Controls.Add(_AddShapedButton);
            _BottomPanel.Controls.Add(_ToolsShapedButton);
            _BottomPanel.Dock = DockStyle.Bottom;
            _BottomPanel.Location = new Point(0, 402);
            _BottomPanel.Name = "_BottomPanel";
            _BottomPanel.Size = new Size(730, 71);
            _BottomPanel.TabIndex = 8;
            // 
            // _TimesShapedButton
            // 
            _TimesShapedButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _TimesShapedButton.BackColor = Color.Transparent;
            _TimesShapedButton.BorderColor = Color.Transparent;
            _TimesShapedButton.BorderWidth = 2;
            _TimesShapedButton.ButtonShape = ButtonShape.Circle;
            _TimesShapedButton.ButtonText = "X";
            _TimesShapedButton.EndColor = Color.FromArgb(30, 30, 30);
            _TimesShapedButton.EndOpacity = 250;
            _TimesShapedButton.FlatAppearance.BorderSize = 0;
            _TimesShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            _TimesShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _TimesShapedButton.FlatStyle = FlatStyle.Flat;
            _TimesShapedButton.Font = new Font("Microsoft Sans Serif", 15.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _TimesShapedButton.ForeColor = Color.White;
            _TimesShapedButton.GradientAngle = 90;
            _TimesShapedButton.Location = new Point(643, 8);
            _TimesShapedButton.MouseClickStartColor = Color.Black;
            _TimesShapedButton.MouseClickEndColor = Color.Black;
            _TimesShapedButton.MouseHoverStartColor = Color.FromArgb(150, 0, 0);
            _TimesShapedButton.MouseHoverEndColor = Color.FromArgb(150, 0, 0);
            _TimesShapedButton.Name = "_TimesShapedButton";
            _TimesShapedButton.ShowButtonText = true;
            _TimesShapedButton.Size = new Size(75, 55);
            _TimesShapedButton.StartColor = Color.FromArgb(30, 30, 30);
            _TimesShapedButton.StartOpacity = 250;
            _TimesShapedButton.TabIndex = 9;
            _TimesShapedButton.TextLocation = new Point(28, 16);
            _TimesShapedButton.UseVisualStyleBackColor = false;
            // 
            // _TestShapedButton
            // 
            _TestShapedButton.BackColor = Color.Transparent;
            _TestShapedButton.BorderColor = Color.Transparent;
            _TestShapedButton.BorderWidth = 2;
            _TestShapedButton.ButtonShape = ButtonShape.RoundRect;
            _TestShapedButton.ButtonText = "Test";
            _TestShapedButton.EndColor = Color.FromArgb(30, 30, 30);
            _TestShapedButton.EndOpacity = 250;
            _TestShapedButton.FlatAppearance.BorderSize = 0;
            _TestShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            _TestShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _TestShapedButton.FlatStyle = FlatStyle.Flat;
            _TestShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _TestShapedButton.ForeColor = Color.White;
            _TestShapedButton.GradientAngle = 90;
            _TestShapedButton.Location = new Point(257, 10);
            _TestShapedButton.MouseClickStartColor = Color.Black;
            _TestShapedButton.MouseClickEndColor = Color.Black;
            _TestShapedButton.MouseHoverStartColor = Color.FromArgb(80, 80, 80);
            _TestShapedButton.MouseHoverEndColor = Color.FromArgb(80, 80, 80);
            _TestShapedButton.Name = "_TestShapedButton";
            _TestShapedButton.ShowButtonText = true;
            _TestShapedButton.Size = new Size(136, 55);
            _TestShapedButton.StartColor = Color.FromArgb(30, 30, 30);
            _TestShapedButton.StartOpacity = 250;
            _TestShapedButton.TabIndex = 8;
            _TestShapedButton.TextLocation = new Point(46, 18);
            _TestShapedButton.UseVisualStyleBackColor = false;
            // 
            // _AddShapedButton
            // 
            _AddShapedButton.BackColor = Color.Transparent;
            _AddShapedButton.BorderColor = Color.Transparent;
            _AddShapedButton.BorderWidth = 2;
            _AddShapedButton.ButtonShape = ButtonShape.Circle;
            _AddShapedButton.ButtonText = "+";
            _AddShapedButton.EndColor = Color.FromArgb(30, 30, 30);
            _AddShapedButton.EndOpacity = 250;
            _AddShapedButton.FlatAppearance.BorderSize = 0;
            _AddShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            _AddShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _AddShapedButton.FlatStyle = FlatStyle.Flat;
            _AddShapedButton.Font = new Font("Microsoft Sans Serif", 27.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _AddShapedButton.ForeColor = Color.White;
            _AddShapedButton.GradientAngle = 90;
            _AddShapedButton.Location = new Point(10, 10);
            _AddShapedButton.MouseClickStartColor = Color.Black;
            _AddShapedButton.MouseClickEndColor = Color.Black;
            _AddShapedButton.MouseHoverStartColor = Color.FromArgb(80, 80, 80);
            _AddShapedButton.MouseHoverEndColor = Color.FromArgb(80, 80, 80);
            _AddShapedButton.Name = "_AddShapedButton";
            _AddShapedButton.ShowButtonText = true;
            _AddShapedButton.Size = new Size(59, 55);
            _AddShapedButton.StartColor = Color.FromArgb(30, 30, 30);
            _AddShapedButton.StartOpacity = 250;
            _AddShapedButton.TabIndex = 6;
            _AddShapedButton.TextLocation = new Point(13, 6);
            _AddShapedButton.UseVisualStyleBackColor = false;
            // 
            // _ToolsShapedButton
            // 
            _ToolsShapedButton.BackColor = Color.Transparent;
            _ToolsShapedButton.BorderColor = Color.Transparent;
            _ToolsShapedButton.BorderWidth = 2;
            _ToolsShapedButton.ButtonShape = ButtonShape.RoundRect;
            _ToolsShapedButton.ButtonText = "Tools";
            _ToolsShapedButton.EndColor = Color.FromArgb(30, 30, 30);
            _ToolsShapedButton.EndOpacity = 250;
            _ToolsShapedButton.FlatAppearance.BorderSize = 0;
            _ToolsShapedButton.FlatAppearance.MouseDownBackColor = Color.Transparent;
            _ToolsShapedButton.FlatAppearance.MouseOverBackColor = Color.Transparent;
            _ToolsShapedButton.FlatStyle = FlatStyle.Flat;
            _ToolsShapedButton.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _ToolsShapedButton.ForeColor = Color.White;
            _ToolsShapedButton.GradientAngle = 90;
            _ToolsShapedButton.Location = new Point(96, 10);
            _ToolsShapedButton.MouseClickStartColor = Color.Black;
            _ToolsShapedButton.MouseClickEndColor = Color.Black;
            _ToolsShapedButton.MouseHoverStartColor = Color.FromArgb(80, 80, 80);
            _ToolsShapedButton.MouseHoverEndColor = Color.FromArgb(80, 80, 80);
            _ToolsShapedButton.Name = "_ToolsShapedButton";
            _ToolsShapedButton.ShowButtonText = true;
            _ToolsShapedButton.Size = new Size(136, 55);
            _ToolsShapedButton.StartColor = Color.FromArgb(30, 30, 30);
            _ToolsShapedButton.StartOpacity = 250;
            _ToolsShapedButton.TabIndex = 7;
            _ToolsShapedButton.TextLocation = new Point(45, 18);
            _ToolsShapedButton.UseVisualStyleBackColor = false;
            // 
            // _RightBorderBottomPanel
            // 
            _RightBorderBottomPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _RightBorderBottomPanel.BackColor = Color.Black;
            _RightBorderBottomPanel.Cursor = Cursors.SizeNWSE;
            _RightBorderBottomPanel.Location = new Point(728, 452);
            _RightBorderBottomPanel.Name = "_RightBorderBottomPanel";
            _RightBorderBottomPanel.Size = new Size(2, 19);
            _RightBorderBottomPanel.TabIndex = 9;
            // 
            // _BottomBorderLeftPanel
            // 
            _BottomBorderLeftPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _BottomBorderLeftPanel.BackColor = Color.Black;
            _BottomBorderLeftPanel.Cursor = Cursors.SizeNESW;
            _BottomBorderLeftPanel.Location = new Point(0, 471);
            _BottomBorderLeftPanel.Name = "_BottomBorderLeftPanel";
            _BottomBorderLeftPanel.Size = new Size(15, 2);
            _BottomBorderLeftPanel.TabIndex = 10;
            // 
            // _LeftBorderBottomPanel
            // 
            _LeftBorderBottomPanel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _LeftBorderBottomPanel.BackColor = Color.Black;
            _LeftBorderBottomPanel.Cursor = Cursors.SizeNESW;
            _LeftBorderBottomPanel.Location = new Point(0, 453);
            _LeftBorderBottomPanel.Name = "_LeftBorderBottomPanel";
            _LeftBorderBottomPanel.Size = new Size(2, 19);
            _LeftBorderBottomPanel.TabIndex = 11;
            // 
            // _RightBorderTopPanel
            // 
            _RightBorderTopPanel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _RightBorderTopPanel.BackColor = Color.Black;
            _RightBorderTopPanel.Cursor = Cursors.SizeNESW;
            _RightBorderTopPanel.Location = new Point(728, 2);
            _RightBorderTopPanel.Name = "_RightBorderTopPanel";
            _RightBorderTopPanel.Size = new Size(2, 20);
            _RightBorderTopPanel.TabIndex = 12;
            // 
            // _TopBorderRightPanel
            // 
            _TopBorderRightPanel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _TopBorderRightPanel.BackColor = Color.Black;
            _TopBorderRightPanel.Cursor = Cursors.SizeNESW;
            _TopBorderRightPanel.Location = new Point(710, 0);
            _TopBorderRightPanel.Name = "_TopBorderRightPanel";
            _TopBorderRightPanel.Size = new Size(20, 2);
            _TopBorderRightPanel.TabIndex = 13;
            // 
            // _TopBorderLeftPanel
            // 
            _TopBorderLeftPanel.BackColor = Color.Black;
            _TopBorderLeftPanel.Cursor = Cursors.SizeNWSE;
            _TopBorderLeftPanel.Location = new Point(0, 0);
            _TopBorderLeftPanel.Name = "_TopBorderLeftPanel";
            _TopBorderLeftPanel.Size = new Size(20, 2);
            _TopBorderLeftPanel.TabIndex = 14;
            // 
            // _LeftBorderTopPanel
            // 
            _LeftBorderTopPanel.BackColor = Color.Black;
            _LeftBorderTopPanel.Cursor = Cursors.SizeNWSE;
            _LeftBorderTopPanel.Location = new Point(0, 0);
            _LeftBorderTopPanel.Name = "_LeftBorderTopPanel";
            _LeftBorderTopPanel.Size = new Size(2, 20);
            _LeftBorderTopPanel.TabIndex = 15;
            // 
            // _LeftPanel
            // 
            _LeftPanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
            _LeftPanel.BackColor = Color.FromArgb(60, 60, 60);
            _LeftPanel.Controls.Add(_HelpButton);
            _LeftPanel.Controls.Add(_RunButton);
            _LeftPanel.Controls.Add(_ViewButton);
            _LeftPanel.Controls.Add(_EditButton);
            _LeftPanel.Controls.Add(_FileButton);
            _LeftPanel.Location = new Point(2, 77);
            _LeftPanel.Name = "_LeftPanel";
            _LeftPanel.Size = new Size(280, 325);
            _LeftPanel.TabIndex = 16;
            // 
            // _HelpButton
            // 
            _HelpButton.BusyBackColor = Color.FromArgb(60, 60, 60);
            _HelpButton.DisplayText = "Help";
            _HelpButton.Dock = DockStyle.Top;
            _HelpButton.FlatStyle = FlatStyle.Flat;
            _HelpButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _HelpButton.ForeColor = Color.White;
            _HelpButton.Location = new Point(0, 140);
            _HelpButton.MouseClickColor = Color.Black;
            _HelpButton.MouseColorsEnabled = true;
            _HelpButton.MouseHoverColor = Color.FromArgb(30, 30, 30);
            _HelpButton.Name = "_HelpButton";
            _HelpButton.Size = new Size(280, 35);
            _HelpButton.TabIndex = 4;
            _HelpButton.Text = "Help";
            _HelpButton.TextLocationLeft = 110;
            _HelpButton.TextLocationTop = 6;
            _HelpButton.UseVisualStyleBackColor = true;
            // 
            // _RunButton
            // 
            _RunButton.BusyBackColor = Color.FromArgb(60, 60, 60);
            _RunButton.DisplayText = "Run";
            _RunButton.Dock = DockStyle.Top;
            _RunButton.FlatStyle = FlatStyle.Flat;
            _RunButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _RunButton.ForeColor = Color.White;
            _RunButton.Location = new Point(0, 105);
            _RunButton.MouseClickColor = Color.Black;
            _RunButton.MouseColorsEnabled = true;
            _RunButton.MouseHoverColor = Color.FromArgb(30, 30, 30);
            _RunButton.Name = "_RunButton";
            _RunButton.Size = new Size(280, 35);
            _RunButton.TabIndex = 3;
            _RunButton.Text = "Run";
            _RunButton.TextLocationLeft = 110;
            _RunButton.TextLocationTop = 6;
            _RunButton.UseVisualStyleBackColor = true;
            // 
            // _ViewButton
            // 
            _ViewButton.BusyBackColor = Color.FromArgb(60, 60, 60);
            _ViewButton.DisplayText = "View";
            _ViewButton.Dock = DockStyle.Top;
            _ViewButton.FlatStyle = FlatStyle.Flat;
            _ViewButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _ViewButton.ForeColor = Color.White;
            _ViewButton.Location = new Point(0, 70);
            _ViewButton.MouseClickColor = Color.Black;
            _ViewButton.MouseColorsEnabled = true;
            _ViewButton.MouseHoverColor = Color.FromArgb(30, 30, 30);
            _ViewButton.Name = "_ViewButton";
            _ViewButton.Size = new Size(280, 35);
            _ViewButton.TabIndex = 2;
            _ViewButton.Text = "View";
            _ViewButton.TextLocationLeft = 110;
            _ViewButton.TextLocationTop = 6;
            _ViewButton.UseVisualStyleBackColor = true;
            // 
            // _EditButton
            // 
            _EditButton.BusyBackColor = Color.FromArgb(60, 60, 60);
            _EditButton.DisplayText = "Edit";
            _EditButton.Dock = DockStyle.Top;
            _EditButton.FlatStyle = FlatStyle.Flat;
            _EditButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _EditButton.ForeColor = Color.White;
            _EditButton.Location = new Point(0, 35);
            _EditButton.MouseClickColor = Color.Black;
            _EditButton.MouseColorsEnabled = true;
            _EditButton.MouseHoverColor = Color.FromArgb(30, 30, 30);
            _EditButton.Name = "_EditButton";
            _EditButton.Size = new Size(280, 35);
            _EditButton.TabIndex = 1;
            _EditButton.Text = "Edit";
            _EditButton.TextLocationLeft = 110;
            _EditButton.TextLocationTop = 6;
            _EditButton.UseVisualStyleBackColor = true;
            // 
            // _FileButton
            // 
            _FileButton.BusyBackColor = Color.FromArgb(60, 60, 60);
            _FileButton.DisplayText = "File";
            _FileButton.Dock = DockStyle.Top;
            _FileButton.FlatStyle = FlatStyle.Flat;
            _FileButton.Font = new Font("Microsoft YaHei UI", 12.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _FileButton.ForeColor = Color.White;
            _FileButton.Location = new Point(0, 0);
            _FileButton.MouseClickColor = Color.Black;
            _FileButton.MouseColorsEnabled = true;
            _FileButton.MouseHoverColor = Color.FromArgb(30, 30, 30);
            _FileButton.Name = "_FileButton";
            _FileButton.Size = new Size(280, 35);
            _FileButton.TabIndex = 0;
            _FileButton.Text = "File";
            _FileButton.TextLocationLeft = 110;
            _FileButton.TextLocationTop = 6;
            _FileButton.UseVisualStyleBackColor = true;
            // 
            // _SeparatorPanel
            // 
            _SeparatorPanel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
            _SeparatorPanel.BackColor = Color.Black;
            _SeparatorPanel.Location = new Point(282, 78);
            _SeparatorPanel.Name = "_SeparatorPanel";
            _SeparatorPanel.Size = new Size(2, 324);
            _SeparatorPanel.TabIndex = 17;
            // 
            // _InformationLabel
            // 
            _InformationLabel.AutoSize = true;
            _InformationLabel.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _InformationLabel.ForeColor = Color.White;
            _InformationLabel.Location = new Point(309, 204);
            _InformationLabel.Name = "_InformationLabel";
            _InformationLabel.Size = new Size(378, 24);
            _InformationLabel.TabIndex = 18;
            _InformationLabel.Text = "Resizable Black colored Custom Form in C#";
            // 
            // BlackForm
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(60, 60, 60);
            ClientSize = new Size(730, 473);
            Controls.Add(_InformationLabel);
            Controls.Add(_SeparatorPanel);
            Controls.Add(_LeftPanel);
            Controls.Add(_LeftBorderTopPanel);
            Controls.Add(_TopBorderLeftPanel);
            Controls.Add(_TopBorderRightPanel);
            Controls.Add(_RightBorderTopPanel);
            Controls.Add(_LeftBorderBottomPanel);
            Controls.Add(_BottomBorderLeftPanel);
            Controls.Add(_RightBorderBottomPanel);
            Controls.Add(_BottomBorderRightPanel);
            Controls.Add(_BottomBorderPanel);
            Controls.Add(_LeftBorderPanel);
            Controls.Add(_RightBorderPanel);
            Controls.Add(_TopBorderPanel);
            Controls.Add(_TopPanel);
            Controls.Add(_BottomPanel);
            FormBorderStyle = FormBorderStyle.None;
            MainMenuStrip = _MenuStrip;
            Name = "BlackForm";
            Text = "My App";
            _TopPanel.ResumeLayout(false);
            _TopPanel.PerformLayout();
            _MenuStrip.ResumeLayout(false);
            _MenuStrip.PerformLayout();
            _BottomPanel.ResumeLayout(false);
            _LeftPanel.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        private Panel _TopBorderPanel;
        private Panel _RightBorderPanel;
        private Panel _LeftBorderPanel;
        private Panel _BottomBorderPanel;
        private Panel _TopPanel;
        private ZopeButton _CloseButton;
        private Panel _BottomBorderRightPanel;
        private ShapedButton _AddShapedButton;
        private Label _WindowTextLabel;
        private MinMaxButton _MaxButton;
        private ShapedButton _BuyNowShapedButton;
        private ShapedButton _ToolsShapedButton;
        private Panel _BottomPanel;
        private ZopeButton _MinButton;
        private ToolTip _ToolTip;
        private Panel _RightBorderBottomPanel;
        private Panel _BottomBorderLeftPanel;
        private Panel _LeftBorderBottomPanel;
        private Panel _RightBorderTopPanel;
        private Panel _TopBorderRightPanel;
        private Panel _TopBorderLeftPanel;
        private Panel _LeftBorderTopPanel;
        private Panel _LeftPanel;
        private ZopeButton _FileButton;
        private Panel _SeparatorPanel;
        private ZopeButton _EditButton;
        private ZopeButton _ViewButton;
        private ZopeButton _HelpButton;
        private ZopeButton _RunButton;
        private Label _InformationLabel;
        private ShapedButton _TestShapedButton;
        private ShapedButton _TimesShapedButton;
        private ZopeMenuStrip _MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem _FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _NewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _OpenToolStripMenuItem;
        private ToolStripSeparator _ToolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem _SaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _SaveAsToolStripMenuItem;
        private ToolStripSeparator _ToolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem _CloseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _CloseAllToolStripMenuItem;
        private ToolStripSeparator _ToolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem _PrintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _PrintPreviewToolStripMenuItem;
        private ToolStripSeparator _ToolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem _CloseToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem _EditToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _CutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _CopyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _PasteToolStripMenuItem;
        private ToolStripSeparator _ToolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem _UnduToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _RedoToolStripMenuItem;
        private ToolStripSeparator _ToolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem _FindToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _ReplaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _SelectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _HelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _HelpContentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _OnlineHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _AboutToolStripMenuItem;
    }
}
