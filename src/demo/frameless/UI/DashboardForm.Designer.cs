using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using isr.Core.WinControls;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Testers
{
    public partial class DashboardForm
    {
        /// <summary>
    /// Required designer variable.
    /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing )
            {
                components?.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            _LeftTopPanel = new Panel();
            _LeftTopPanelLabel = new Label();
            _TopPanel = new Panel();
            _TopPanel.MouseDown += new MouseEventHandler(TopPanel_MouseDown);
            _TopPanel.MouseMove += new MouseEventHandler(TopPanel_MouseMove);
            _TopPanel.MouseUp += new MouseEventHandler(TopPanel_MouseUp);
            _DashboardLabel = new Label();
            _DashboardLabel.MouseDown += new MouseEventHandler(DashboardLabel_mouseDown);
            _DashboardLabel.MouseMove += new MouseEventHandler(DashboardLabel_mouseMove);
            _DashboardLabel.MouseUp += new MouseEventHandler(DashboardLabel_mouseUp);
            _IconPanel = new Panel();
            _MinButton = new ZopeButton();
            _MinButton.Click += new System.EventHandler(MinButton_Click);
            _CloseButton = new ZopeButton();
            _CloseButton.Click += new System.EventHandler(CloseButton_Click);
            _LeftPanel = new Panel();
            _LeftBottomPanel = new Panel();
            _LeftBottomPanelLabel = new Label();
            _SettingsButton = new ZopeButton();
            _SettingsButton.Click += new System.EventHandler(Settings_button_Click);
            _ThemeButton = new ZopeButton();
            _ThemeButton.Click += new System.EventHandler(Theme_button_Click);
            _LayoutButton = new ZopeButton();
            _LayoutButton.Click += new System.EventHandler(Layout_button_Click);
            _PagesButton = new ZopeButton();
            _PagesButton.Click += new System.EventHandler(Pages_button_Click);
            _StatsButton = new ZopeButton();
            _StatsButton.Click += new System.EventHandler(Stats_button_Click);
            _DashboardButton = new ZopeButton();
            _DashboardButton.Click += new System.EventHandler(Dashboard_button_Click);
            _VisitorsPanel = new Panel();
            _MonthVisitorsCountLabel = new Label();
            _MonthVisitorsCountLabelLabel = new Label();
            _TodayVisitorsCountLabel = new Label();
            _TodayVisitorsCountLabelLabel = new Label();
            _VisitorsCountPanel = new Panel();
            _VisitorsCountLabel = new Label();
            _VisitorsCountLabelLabel = new Label();
            _AdminPanel = new Panel();
            _AdminNameLabel = new Label();
            _AdminLabelLabel = new Label();
            _AdminIconPanel = new Panel();
            _StoragePanel = new Panel();
            _TotalStorageLabel = new Label();
            _TotalStorageLabelLabel = new Label();
            _FreeStorageLabel = new Label();
            _FreeStorageLabelLabel = new Label();
            _UsedStorageLabel = new Label();
            _UsedStorageLabelLabel = new Label();
            _StorageTopPanel = new Panel();
            _StoratgeTitleLabel = new Label();
            _ControlPanel = new Panel();
            _ContrlPanelLabel = new Label();
            _DeleteBlogButton = new ZopeButton();
            _ViewBlogButton = new ZopeButton();
            _CampainsPanel = new Panel();
            _CampaignsLabel = new Label();
            _LeftTopPanel.SuspendLayout();
            _TopPanel.SuspendLayout();
            _LeftPanel.SuspendLayout();
            _LeftBottomPanel.SuspendLayout();
            _VisitorsPanel.SuspendLayout();
            _VisitorsCountPanel.SuspendLayout();
            _AdminPanel.SuspendLayout();
            _StoragePanel.SuspendLayout();
            _StorageTopPanel.SuspendLayout();
            _ControlPanel.SuspendLayout();
            _CampainsPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _LeftTopPanel
            // 
            _LeftTopPanel.BackColor = Color.FromArgb(30, 150, 80);
            _LeftTopPanel.Controls.Add(_LeftTopPanelLabel);
            _LeftTopPanel.Location = new Point(0, 0);
            _LeftTopPanel.Name = "_LeftTopPanel";
            _LeftTopPanel.Size = new Size(245, 60);
            _LeftTopPanel.TabIndex = 0;
            // 
            // _LeftTopPanelLabel
            // 
            _LeftTopPanelLabel.AutoSize = true;
            _LeftTopPanelLabel.Font = new Font("Microsoft Sans Serif", 18.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _LeftTopPanelLabel.ForeColor = Color.White;
            _LeftTopPanelLabel.Location = new Point(45, 13);
            _LeftTopPanelLabel.Name = "_LeftTopPanelLabel";
            _LeftTopPanelLabel.Size = new Size(131, 29);
            _LeftTopPanelLabel.TabIndex = 0;
            _LeftTopPanelLabel.Text = "Extreme UI";
            // 
            // _TopPanel
            // 
            _TopPanel.BackColor = Color.White;
            _TopPanel.Controls.Add(_DashboardLabel);
            _TopPanel.Controls.Add(_IconPanel);
            _TopPanel.Controls.Add(_MinButton);
            _TopPanel.Controls.Add(_CloseButton);
            _TopPanel.Location = new Point(245, 0);
            _TopPanel.Name = "_TopPanel";
            _TopPanel.Size = new Size(605, 60);
            _TopPanel.TabIndex = 1;
            // 
            // _DashboardLabel
            // 
            _DashboardLabel.AutoSize = true;
            _DashboardLabel.Font = new Font("Microsoft Sans Serif", 18.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _DashboardLabel.ForeColor = Color.FromArgb(50, 50, 50);
            _DashboardLabel.Location = new Point(60, 13);
            _DashboardLabel.Name = "_DashboardLabel";
            _DashboardLabel.Size = new Size(131, 29);
            _DashboardLabel.TabIndex = 3;
            _DashboardLabel.Text = "Dashboard";
            // 
            // _IconPanel
            // 
            _IconPanel.BackgroundImage = Testers.Properties.Resources.home;
            _IconPanel.Location = new Point(6, 13);
            _IconPanel.Name = "_IconPanel";
            _IconPanel.Size = new Size(30, 30);
            _IconPanel.TabIndex = 2;
            // 
            // _MinButton
            // 
            _MinButton.BusyBackColor = Color.White;
            _MinButton.DisplayText = "_";
            _MinButton.FlatStyle = FlatStyle.Flat;
            _MinButton.Font = new Font("Microsoft YaHei UI", 20.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _MinButton.ForeColor = Color.Black;
            _MinButton.Location = new Point(540, 4);
            _MinButton.MouseClickColor = Color.FromArgb(160, 180, 200);
            _MinButton.MouseColorsEnabled = true;
            _MinButton.MouseHoverColor = Color.FromArgb(100, 150, 220);
            _MinButton.Name = "_MinButton";
            _MinButton.Size = new Size(31, 24);
            _MinButton.TabIndex = 1;
            _MinButton.Text = "_";
            _MinButton.TextLocationLeft = 6;
            _MinButton.TextLocationTop = -20;
            _MinButton.UseVisualStyleBackColor = true;
            // 
            // _CloseButton
            // 
            _CloseButton.BusyBackColor = Color.White;
            _CloseButton.DisplayText = "X";
            _CloseButton.FlatStyle = FlatStyle.Flat;
            _CloseButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _CloseButton.ForeColor = Color.Black;
            _CloseButton.Location = new Point(571, 4);
            _CloseButton.MouseClickColor = Color.FromArgb(100, 150, 220);
            _CloseButton.MouseColorsEnabled = true;
            _CloseButton.MouseHoverColor = Color.FromArgb(180, 40, 50);
            _CloseButton.Name = "_CloseButton";
            _CloseButton.Size = new Size(31, 24);
            _CloseButton.TabIndex = 0;
            _CloseButton.Text = "X";
            _CloseButton.TextLocationLeft = 6;
            _CloseButton.TextLocationTop = -1;
            _CloseButton.UseVisualStyleBackColor = true;
            // 
            // _LeftPanel
            // 
            _LeftPanel.BackColor = Color.FromArgb(30, 30, 40);
            _LeftPanel.Controls.Add(_LeftBottomPanel);
            _LeftPanel.Controls.Add(_SettingsButton);
            _LeftPanel.Controls.Add(_ThemeButton);
            _LeftPanel.Controls.Add(_LayoutButton);
            _LeftPanel.Controls.Add(_PagesButton);
            _LeftPanel.Controls.Add(_StatsButton);
            _LeftPanel.Controls.Add(_DashboardButton);
            _LeftPanel.Location = new Point(0, 60);
            _LeftPanel.Name = "_LeftPanel";
            _LeftPanel.Size = new Size(245, 440);
            _LeftPanel.TabIndex = 2;
            // 
            // _LeftBottomPanel
            // 
            _LeftBottomPanel.BackColor = Color.FromArgb(20, 20, 30);
            _LeftBottomPanel.Controls.Add(_LeftBottomPanelLabel);
            _LeftBottomPanel.Location = new Point(0, 380);
            _LeftBottomPanel.Name = "_LeftBottomPanel";
            _LeftBottomPanel.Size = new Size(245, 60);
            _LeftBottomPanel.TabIndex = 6;
            // 
            // _LeftBottomPanelLabel
            // 
            _LeftBottomPanelLabel.AutoSize = true;
            _LeftBottomPanelLabel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _LeftBottomPanelLabel.ForeColor = Color.FromArgb(200, 200, 200);
            _LeftBottomPanelLabel.Location = new Point(12, 14);
            _LeftBottomPanelLabel.Name = "_LeftBottomPanelLabel";
            _LeftBottomPanelLabel.Size = new Size(102, 16);
            _LeftBottomPanelLabel.TabIndex = 0;
            _LeftBottomPanelLabel.Text = "Copyright   2016";
            // 
            // _SettingsButton
            // 
            _SettingsButton.BusyBackColor = Color.FromArgb(30, 30, 40);
            _SettingsButton.Cursor = Cursors.Hand;
            _SettingsButton.DisplayText = "Settings";
            _SettingsButton.FlatStyle = FlatStyle.Flat;
            _SettingsButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _SettingsButton.ForeColor = Color.White;
            _SettingsButton.Location = new Point(0, 250);
            _SettingsButton.MouseClickColor = Color.FromArgb(20, 20, 20);
            _SettingsButton.MouseColorsEnabled = true;
            _SettingsButton.MouseHoverColor = Color.FromArgb(20, 20, 20);
            _SettingsButton.Name = "_SettingsButton";
            _SettingsButton.Size = new Size(245, 50);
            _SettingsButton.TabIndex = 5;
            _SettingsButton.Text = "Settings";
            _SettingsButton.TextLocationLeft = 80;
            _SettingsButton.TextLocationTop = 10;
            _SettingsButton.UseVisualStyleBackColor = true;
            // 
            // _ThemeButton
            // 
            _ThemeButton.BusyBackColor = Color.FromArgb(30, 30, 40);
            _ThemeButton.Cursor = Cursors.Hand;
            _ThemeButton.DisplayText = "Theme";
            _ThemeButton.FlatStyle = FlatStyle.Flat;
            _ThemeButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _ThemeButton.ForeColor = Color.White;
            _ThemeButton.Location = new Point(0, 200);
            _ThemeButton.MouseClickColor = Color.FromArgb(20, 20, 20);
            _ThemeButton.MouseColorsEnabled = true;
            _ThemeButton.MouseHoverColor = Color.FromArgb(20, 20, 20);
            _ThemeButton.Name = "_ThemeButton";
            _ThemeButton.Size = new Size(245, 50);
            _ThemeButton.TabIndex = 4;
            _ThemeButton.Text = "Theme";
            _ThemeButton.TextLocationLeft = 82;
            _ThemeButton.TextLocationTop = 10;
            _ThemeButton.UseVisualStyleBackColor = true;
            // 
            // _LayoutButton
            // 
            _LayoutButton.BusyBackColor = Color.FromArgb(30, 30, 40);
            _LayoutButton.Cursor = Cursors.Hand;
            _LayoutButton.DisplayText = "Layout";
            _LayoutButton.FlatStyle = FlatStyle.Flat;
            _LayoutButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _LayoutButton.ForeColor = Color.White;
            _LayoutButton.Location = new Point(0, 150);
            _LayoutButton.MouseClickColor = Color.FromArgb(20, 20, 20);
            _LayoutButton.MouseColorsEnabled = true;
            _LayoutButton.MouseHoverColor = Color.FromArgb(20, 20, 20);
            _LayoutButton.Name = "_LayoutButton";
            _LayoutButton.Size = new Size(245, 50);
            _LayoutButton.TabIndex = 3;
            _LayoutButton.Text = "Layout";
            _LayoutButton.TextLocationLeft = 80;
            _LayoutButton.TextLocationTop = 10;
            _LayoutButton.UseVisualStyleBackColor = true;
            // 
            // _PagesButton
            // 
            _PagesButton.BusyBackColor = Color.FromArgb(30, 30, 40);
            _PagesButton.Cursor = Cursors.Hand;
            _PagesButton.DisplayText = "Pages";
            _PagesButton.FlatStyle = FlatStyle.Flat;
            _PagesButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _PagesButton.ForeColor = Color.White;
            _PagesButton.Location = new Point(0, 100);
            _PagesButton.MouseClickColor = Color.FromArgb(20, 20, 20);
            _PagesButton.MouseColorsEnabled = true;
            _PagesButton.MouseHoverColor = Color.FromArgb(20, 20, 20);
            _PagesButton.Name = "_PagesButton";
            _PagesButton.Size = new Size(245, 50);
            _PagesButton.TabIndex = 2;
            _PagesButton.Text = "Pages";
            _PagesButton.TextLocationLeft = 82;
            _PagesButton.TextLocationTop = 10;
            _PagesButton.UseVisualStyleBackColor = true;
            // 
            // _StatsButton
            // 
            _StatsButton.BusyBackColor = Color.FromArgb(30, 30, 40);
            _StatsButton.Cursor = Cursors.Hand;
            _StatsButton.DisplayText = "Stats";
            _StatsButton.FlatStyle = FlatStyle.Flat;
            _StatsButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _StatsButton.ForeColor = Color.White;
            _StatsButton.Location = new Point(0, 50);
            _StatsButton.MouseClickColor = Color.FromArgb(20, 20, 20);
            _StatsButton.MouseColorsEnabled = true;
            _StatsButton.MouseHoverColor = Color.FromArgb(20, 20, 20);
            _StatsButton.Name = "_StatsButton";
            _StatsButton.Size = new Size(245, 50);
            _StatsButton.TabIndex = 1;
            _StatsButton.Text = "Stats";
            _StatsButton.TextLocationLeft = 84;
            _StatsButton.TextLocationTop = 10;
            _StatsButton.UseVisualStyleBackColor = true;
            // 
            // _DashboardButton
            // 
            _DashboardButton.BusyBackColor = Color.FromArgb(30, 30, 40);
            _DashboardButton.Cursor = Cursors.Hand;
            _DashboardButton.DisplayText = "Dashboard";
            _DashboardButton.FlatStyle = FlatStyle.Flat;
            _DashboardButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _DashboardButton.ForeColor = Color.White;
            _DashboardButton.Location = new Point(0, 0);
            _DashboardButton.MouseClickColor = Color.FromArgb(20, 20, 20);
            _DashboardButton.MouseColorsEnabled = true;
            _DashboardButton.MouseHoverColor = Color.FromArgb(20, 20, 20);
            _DashboardButton.Name = "_DashboardButton";
            _DashboardButton.Size = new Size(245, 50);
            _DashboardButton.TabIndex = 0;
            _DashboardButton.Text = "Dashboard";
            _DashboardButton.TextLocationLeft = 60;
            _DashboardButton.TextLocationTop = 10;
            _DashboardButton.UseVisualStyleBackColor = true;
            // 
            // _VisitorsPanel
            // 
            _VisitorsPanel.BackColor = Color.FromArgb(50, 80, 160);
            _VisitorsPanel.Controls.Add(_MonthVisitorsCountLabel);
            _VisitorsPanel.Controls.Add(_MonthVisitorsCountLabelLabel);
            _VisitorsPanel.Controls.Add(_TodayVisitorsCountLabel);
            _VisitorsPanel.Controls.Add(_TodayVisitorsCountLabelLabel);
            _VisitorsPanel.Controls.Add(_VisitorsCountPanel);
            _VisitorsPanel.Location = new Point(251, 66);
            _VisitorsPanel.Name = "_VisitorsPanel";
            _VisitorsPanel.Size = new Size(590, 109);
            _VisitorsPanel.TabIndex = 3;
            // 
            // _MonthVisitorsCountLabel
            // 
            _MonthVisitorsCountLabel.AutoSize = true;
            _MonthVisitorsCountLabel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _MonthVisitorsCountLabel.ForeColor = Color.Azure;
            _MonthVisitorsCountLabel.Location = new Point(457, 61);
            _MonthVisitorsCountLabel.Name = "_MonthVisitorsCountLabel";
            _MonthVisitorsCountLabel.Size = new Size(36, 16);
            _MonthVisitorsCountLabel.TabIndex = 4;
            _MonthVisitorsCountLabel.Text = "1035";
            // 
            // _MonthVisitorsCountLabelLabel
            // 
            _MonthVisitorsCountLabelLabel.AutoSize = true;
            _MonthVisitorsCountLabelLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _MonthVisitorsCountLabelLabel.ForeColor = Color.White;
            _MonthVisitorsCountLabelLabel.Location = new Point(446, 23);
            _MonthVisitorsCountLabelLabel.Name = "_MonthVisitorsCountLabelLabel";
            _MonthVisitorsCountLabelLabel.Size = new Size(64, 20);
            _MonthVisitorsCountLabelLabel.TabIndex = 3;
            _MonthVisitorsCountLabelLabel.Text = "Monthly";
            // 
            // _TodayVisitorsCountLabel
            // 
            _TodayVisitorsCountLabel.AutoSize = true;
            _TodayVisitorsCountLabel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _TodayVisitorsCountLabel.ForeColor = Color.Azure;
            _TodayVisitorsCountLabel.Location = new Point(285, 61);
            _TodayVisitorsCountLabel.Name = "_TodayVisitorsCountLabel";
            _TodayVisitorsCountLabel.Size = new Size(29, 16);
            _TodayVisitorsCountLabel.TabIndex = 2;
            _TodayVisitorsCountLabel.Text = "568";
            // 
            // _TodayVisitorsCountLabelLabel
            // 
            _TodayVisitorsCountLabelLabel.AutoSize = true;
            _TodayVisitorsCountLabelLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _TodayVisitorsCountLabelLabel.ForeColor = Color.White;
            _TodayVisitorsCountLabelLabel.Location = new Point(274, 23);
            _TodayVisitorsCountLabelLabel.Name = "_TodayVisitorsCountLabelLabel";
            _TodayVisitorsCountLabelLabel.Size = new Size(52, 20);
            _TodayVisitorsCountLabelLabel.TabIndex = 1;
            _TodayVisitorsCountLabelLabel.Text = "Today";
            // 
            // _VisitorsCountPanel
            // 
            _VisitorsCountPanel.BackColor = Color.FromArgb(10, 30, 60);
            _VisitorsCountPanel.Controls.Add(_VisitorsCountLabel);
            _VisitorsCountPanel.Controls.Add(_VisitorsCountLabelLabel);
            _VisitorsCountPanel.Location = new Point(0, 0);
            _VisitorsCountPanel.Name = "_VisitorsCountPanel";
            _VisitorsCountPanel.Size = new Size(200, 109);
            _VisitorsCountPanel.TabIndex = 0;
            // 
            // _VisitorsCountLabel
            // 
            _VisitorsCountLabel.AutoSize = true;
            _VisitorsCountLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _VisitorsCountLabel.ForeColor = Color.Cyan;
            _VisitorsCountLabel.Location = new Point(67, 61);
            _VisitorsCountLabel.Name = "_VisitorsCountLabel";
            _VisitorsCountLabel.Size = new Size(49, 20);
            _VisitorsCountLabel.TabIndex = 1;
            _VisitorsCountLabel.Text = "5,274";
            // 
            // _VisitorsCountLabelLabel
            // 
            _VisitorsCountLabelLabel.AutoSize = true;
            _VisitorsCountLabelLabel.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _VisitorsCountLabelLabel.ForeColor = Color.White;
            _VisitorsCountLabelLabel.Location = new Point(55, 20);
            _VisitorsCountLabelLabel.Name = "_VisitorsCountLabelLabel";
            _VisitorsCountLabelLabel.Size = new Size(70, 24);
            _VisitorsCountLabelLabel.TabIndex = 0;
            _VisitorsCountLabelLabel.Text = "Visitors";
            // 
            // _AdminPanel
            // 
            _AdminPanel.BackColor = Color.FromArgb(240, 130, 40);
            _AdminPanel.Controls.Add(_AdminNameLabel);
            _AdminPanel.Controls.Add(_AdminLabelLabel);
            _AdminPanel.Controls.Add(_AdminIconPanel);
            _AdminPanel.Location = new Point(251, 190);
            _AdminPanel.Name = "_AdminPanel";
            _AdminPanel.Size = new Size(341, 130);
            _AdminPanel.TabIndex = 4;
            // 
            // _AdminNameLabel
            // 
            _AdminNameLabel.AutoSize = true;
            _AdminNameLabel.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _AdminNameLabel.ForeColor = Color.White;
            _AdminNameLabel.Location = new Point(195, 69);
            _AdminNameLabel.Name = "_AdminNameLabel";
            _AdminNameLabel.Size = new Size(81, 16);
            _AdminNameLabel.TabIndex = 2;
            _AdminNameLabel.Text = "Pritam Zope";
            // 
            // _AdminLabelLabel
            // 
            _AdminLabelLabel.AutoSize = true;
            _AdminLabelLabel.Font = new Font("Microsoft Sans Serif", 14.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _AdminLabelLabel.ForeColor = Color.White;
            _AdminLabelLabel.Location = new Point(202, 19);
            _AdminLabelLabel.Name = "_AdminLabelLabel";
            _AdminLabelLabel.Size = new Size(65, 24);
            _AdminLabelLabel.TabIndex = 1;
            _AdminLabelLabel.Text = "Admin";
            // 
            // _AdminIconPanel
            // 
            _AdminIconPanel.BackgroundImage = Testers.Properties.Resources.pritam12345;
            _AdminIconPanel.Location = new Point(0, 0);
            _AdminIconPanel.Name = "_AdminIconPanel";
            _AdminIconPanel.Size = new Size(152, 130);
            _AdminIconPanel.TabIndex = 0;
            // 
            // _StoragePanel
            // 
            _StoragePanel.BackColor = Color.FromArgb(200, 40, 30);
            _StoragePanel.Controls.Add(_TotalStorageLabel);
            _StoragePanel.Controls.Add(_TotalStorageLabelLabel);
            _StoragePanel.Controls.Add(_FreeStorageLabel);
            _StoragePanel.Controls.Add(_FreeStorageLabelLabel);
            _StoragePanel.Controls.Add(_UsedStorageLabel);
            _StoragePanel.Controls.Add(_UsedStorageLabelLabel);
            _StoragePanel.Controls.Add(_StorageTopPanel);
            _StoragePanel.Location = new Point(251, 337);
            _StoragePanel.Name = "_StoragePanel";
            _StoragePanel.Size = new Size(341, 151);
            _StoragePanel.TabIndex = 7;
            // 
            // _TotalStorageLabel
            // 
            _TotalStorageLabel.AutoSize = true;
            _TotalStorageLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _TotalStorageLabel.ForeColor = Color.White;
            _TotalStorageLabel.Location = new Point(268, 103);
            _TotalStorageLabel.Name = "_TotalStorageLabel";
            _TotalStorageLabel.Size = new Size(55, 20);
            _TotalStorageLabel.TabIndex = 6;
            _TotalStorageLabel.Text = "50 GB";
            // 
            // _TotalStorageLabelLabel
            // 
            _TotalStorageLabelLabel.AutoSize = true;
            _TotalStorageLabelLabel.Font = new Font("Microsoft Sans Serif", 12.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _TotalStorageLabelLabel.ForeColor = Color.White;
            _TotalStorageLabelLabel.Location = new Point(268, 64);
            _TotalStorageLabelLabel.Name = "_TotalStorageLabelLabel";
            _TotalStorageLabelLabel.Size = new Size(46, 20);
            _TotalStorageLabelLabel.TabIndex = 5;
            _TotalStorageLabelLabel.Text = "Total";
            // 
            // _FreeStorageLabel
            // 
            _FreeStorageLabel.AutoSize = true;
            _FreeStorageLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _FreeStorageLabel.ForeColor = Color.White;
            _FreeStorageLabel.Location = new Point(146, 103);
            _FreeStorageLabel.Name = "_FreeStorageLabel";
            _FreeStorageLabel.Size = new Size(68, 20);
            _FreeStorageLabel.TabIndex = 4;
            _FreeStorageLabel.Text = "26.5 GB";
            // 
            // _FreeStorageLabelLabel
            // 
            _FreeStorageLabelLabel.AutoSize = true;
            _FreeStorageLabelLabel.Font = new Font("Microsoft Sans Serif", 12.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _FreeStorageLabelLabel.ForeColor = Color.White;
            _FreeStorageLabelLabel.Location = new Point(157, 64);
            _FreeStorageLabelLabel.Name = "_FreeStorageLabelLabel";
            _FreeStorageLabelLabel.Size = new Size(43, 20);
            _FreeStorageLabelLabel.TabIndex = 3;
            _FreeStorageLabelLabel.Text = "Free";
            // 
            // _UsedStorageLabel
            // 
            _UsedStorageLabel.AutoSize = true;
            _UsedStorageLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _UsedStorageLabel.ForeColor = Color.White;
            _UsedStorageLabel.Location = new Point(27, 103);
            _UsedStorageLabel.Name = "_UsedStorageLabel";
            _UsedStorageLabel.Size = new Size(68, 20);
            _UsedStorageLabel.TabIndex = 2;
            _UsedStorageLabel.Text = "23.5 GB";
            // 
            // _UsedStorageLabelLabel
            // 
            _UsedStorageLabelLabel.AutoSize = true;
            _UsedStorageLabelLabel.Font = new Font("Microsoft Sans Serif", 12.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _UsedStorageLabelLabel.ForeColor = Color.White;
            _UsedStorageLabelLabel.Location = new Point(35, 64);
            _UsedStorageLabelLabel.Name = "_UsedStorageLabelLabel";
            _UsedStorageLabelLabel.Size = new Size(48, 20);
            _UsedStorageLabelLabel.TabIndex = 1;
            _UsedStorageLabelLabel.Text = "Used";
            // 
            // _StorageTopPanel
            // 
            _StorageTopPanel.BackColor = Color.FromArgb(120, 0, 0);
            _StorageTopPanel.Controls.Add(_StoratgeTitleLabel);
            _StorageTopPanel.Location = new Point(0, 0);
            _StorageTopPanel.Name = "_StorageTopPanel";
            _StorageTopPanel.Size = new Size(341, 49);
            _StorageTopPanel.TabIndex = 0;
            // 
            // _StoratgeTitleLabel
            // 
            _StoratgeTitleLabel.AutoSize = true;
            _StoratgeTitleLabel.Font = new Font("Microsoft Sans Serif", 15.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _StoratgeTitleLabel.ForeColor = Color.White;
            _StoratgeTitleLabel.Location = new Point(119, 11);
            _StoratgeTitleLabel.Name = "_StoratgeTitleLabel";
            _StoratgeTitleLabel.Size = new Size(81, 25);
            _StoratgeTitleLabel.TabIndex = 0;
            _StoratgeTitleLabel.Text = "Storage";
            // 
            // _ControlPanel
            // 
            _ControlPanel.BackColor = Color.FromArgb(100, 30, 120);
            _ControlPanel.Controls.Add(_ContrlPanelLabel);
            _ControlPanel.Location = new Point(598, 385);
            _ControlPanel.Name = "_ControlPanel";
            _ControlPanel.Size = new Size(240, 103);
            _ControlPanel.TabIndex = 8;
            // 
            // _ContrlPanelLabel
            // 
            _ContrlPanelLabel.AutoSize = true;
            _ContrlPanelLabel.Font = new Font("Microsoft Sans Serif", 15.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _ContrlPanelLabel.ForeColor = Color.White;
            _ContrlPanelLabel.Location = new Point(48, 40);
            _ContrlPanelLabel.Name = "_ContrlPanelLabel";
            _ContrlPanelLabel.Size = new Size(142, 25);
            _ContrlPanelLabel.TabIndex = 0;
            _ContrlPanelLabel.Text = "Control Panel";
            // 
            // _DeleteBlogButton
            // 
            _DeleteBlogButton.BusyBackColor = Color.FromArgb(180, 50, 60);
            _DeleteBlogButton.Cursor = Cursors.Hand;
            _DeleteBlogButton.DisplayText = "Delete Blog";
            _DeleteBlogButton.FlatStyle = FlatStyle.Flat;
            _DeleteBlogButton.Font = new Font("Microsoft YaHei UI", 12.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _DeleteBlogButton.ForeColor = Color.White;
            _DeleteBlogButton.Location = new Point(598, 260);
            _DeleteBlogButton.MouseClickColor = Color.FromArgb(100, 10, 10);
            _DeleteBlogButton.MouseColorsEnabled = true;
            _DeleteBlogButton.MouseHoverColor = Color.FromArgb(150, 30, 30);
            _DeleteBlogButton.Name = "_DeleteBlogButton";
            _DeleteBlogButton.Size = new Size(240, 60);
            _DeleteBlogButton.TabIndex = 6;
            _DeleteBlogButton.Text = "Delete Blog";
            _DeleteBlogButton.TextLocationLeft = 65;
            _DeleteBlogButton.TextLocationTop = 16;
            _DeleteBlogButton.UseVisualStyleBackColor = true;
            // 
            // _ViewBlogButton
            // 
            _ViewBlogButton.BusyBackColor = Color.FromArgb(30, 130, 110);
            _ViewBlogButton.Cursor = Cursors.Hand;
            _ViewBlogButton.DisplayText = "View Blog";
            _ViewBlogButton.FlatStyle = FlatStyle.Flat;
            _ViewBlogButton.Font = new Font("Microsoft YaHei UI", 14.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _ViewBlogButton.ForeColor = Color.White;
            _ViewBlogButton.Location = new Point(599, 191);
            _ViewBlogButton.MouseClickColor = Color.FromArgb(10, 80, 50);
            _ViewBlogButton.MouseColorsEnabled = true;
            _ViewBlogButton.MouseHoverColor = Color.FromArgb(10, 110, 90);
            _ViewBlogButton.Name = "_ViewBlogButton";
            _ViewBlogButton.Size = new Size(240, 60);
            _ViewBlogButton.TabIndex = 5;
            _ViewBlogButton.Text = "View Blog";
            _ViewBlogButton.TextLocationLeft = 65;
            _ViewBlogButton.TextLocationTop = 16;
            _ViewBlogButton.UseVisualStyleBackColor = true;
            // 
            // _CampainsPanel
            // 
            _CampainsPanel.BackColor = Color.FromArgb(10, 40, 80);
            _CampainsPanel.Controls.Add(_CampaignsLabel);
            _CampainsPanel.Location = new Point(599, 337);
            _CampainsPanel.Name = "_CampainsPanel";
            _CampainsPanel.Size = new Size(239, 42);
            _CampainsPanel.TabIndex = 9;
            // 
            // _CampaignsLabel
            // 
            _CampaignsLabel.AutoSize = true;
            _CampaignsLabel.Font = new Font("Microsoft Sans Serif", 12.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _CampaignsLabel.ForeColor = Color.White;
            _CampaignsLabel.Location = new Point(73, 11);
            _CampaignsLabel.Name = "_CampaignsLabel";
            _CampaignsLabel.Size = new Size(89, 20);
            _CampaignsLabel.TabIndex = 0;
            _CampaignsLabel.Text = "Campaigns";
            // 
            // DashboardForm
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.Gainsboro;
            ClientSize = new Size(873, 515);
            Controls.Add(_CampainsPanel);
            Controls.Add(_ControlPanel);
            Controls.Add(_StoragePanel);
            Controls.Add(_DeleteBlogButton);
            Controls.Add(_ViewBlogButton);
            Controls.Add(_AdminPanel);
            Controls.Add(_VisitorsPanel);
            Controls.Add(_LeftPanel);
            Controls.Add(_TopPanel);
            Controls.Add(_LeftTopPanel);
            FormBorderStyle = FormBorderStyle.None;
            Name = "DashboardForm";
            Text = "Dashboard";
            _LeftTopPanel.ResumeLayout(false);
            _LeftTopPanel.PerformLayout();
            _TopPanel.ResumeLayout(false);
            _TopPanel.PerformLayout();
            _LeftPanel.ResumeLayout(false);
            _LeftBottomPanel.ResumeLayout(false);
            _LeftBottomPanel.PerformLayout();
            _VisitorsPanel.ResumeLayout(false);
            _VisitorsPanel.PerformLayout();
            _VisitorsCountPanel.ResumeLayout(false);
            _VisitorsCountPanel.PerformLayout();
            _AdminPanel.ResumeLayout(false);
            _AdminPanel.PerformLayout();
            _StoragePanel.ResumeLayout(false);
            _StoragePanel.PerformLayout();
            _StorageTopPanel.ResumeLayout(false);
            _StorageTopPanel.PerformLayout();
            _ControlPanel.ResumeLayout(false);
            _ControlPanel.PerformLayout();
            _CampainsPanel.ResumeLayout(false);
            _CampainsPanel.PerformLayout();
            ResumeLayout(false);
        }

        private Panel _LeftTopPanel;
        private Panel _TopPanel;
        private ZopeButton _CloseButton;
        private ZopeButton _MinButton;
        private Label _LeftTopPanelLabel;
        private Panel _IconPanel;
        private Label _DashboardLabel;
        private Panel _LeftPanel;
        private ZopeButton _DashboardButton;
        private ZopeButton _StatsButton;
        private ZopeButton _LayoutButton;
        private ZopeButton _PagesButton;
        private ZopeButton _SettingsButton;
        private ZopeButton _ThemeButton;
        private Panel _LeftBottomPanel;
        private Label _LeftBottomPanelLabel;
        private Panel _VisitorsPanel;
        private Panel _VisitorsCountPanel;
        private Label _VisitorsCountLabel;
        private Label _VisitorsCountLabelLabel;
        private Label _MonthVisitorsCountLabel;
        private Label _MonthVisitorsCountLabelLabel;
        private Label _TodayVisitorsCountLabel;
        private Label _TodayVisitorsCountLabelLabel;
        private Panel _AdminPanel;
        private Label _AdminNameLabel;
        private Label _AdminLabelLabel;
        private Panel _AdminIconPanel;
        private ZopeButton _ViewBlogButton;
        private ZopeButton _DeleteBlogButton;
        private Panel _StoragePanel;
        private Label _TotalStorageLabel;
        private Label _TotalStorageLabelLabel;
        private Label _FreeStorageLabel;
        private Label _FreeStorageLabelLabel;
        private Label _UsedStorageLabel;
        private Label _UsedStorageLabelLabel;
        private Panel _StorageTopPanel;
        private Label _StoratgeTitleLabel;
        private Panel _ControlPanel;
        private Label _ContrlPanelLabel;
        private Panel _CampainsPanel;
        private Label _CampaignsLabel;
    }
}
