using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using isr.Core.WinControls;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Testers
{
    public partial class BlueForm
    {
        /// <summary>
    /// Required designer variable.
    /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing )
            {
                components?.Dispose();
            }

            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify the contents of this method with the
        /// code editor.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _TopBorderPanel = new Panel();
            _TopBorderPanel.MouseDown += new MouseEventHandler(TopBorderPanel_MouseDown);
            _TopBorderPanel.MouseMove += new MouseEventHandler(TopBorderPanel_MouseMove);
            _TopBorderPanel.MouseUp += new MouseEventHandler(TopBorderPanel_MouseUp);
            _RightBorderPanel = new Panel();
            _RightBorderPanel.MouseDown += new MouseEventHandler(RightPanel_MouseDown);
            _RightBorderPanel.MouseMove += new MouseEventHandler(RightPanel_MouseMove);
            _RightBorderPanel.MouseUp += new MouseEventHandler(RightPanel_MouseUp);
            _LeftBorderPanel = new Panel();
            _LeftBorderPanel.MouseDown += new MouseEventHandler(LeftPanel_MouseDown);
            _LeftBorderPanel.MouseMove += new MouseEventHandler(LeftPanel_MouseMove);
            _LeftBorderPanel.MouseUp += new MouseEventHandler(LeftPanel_MouseUp);
            _BottomBorderPanel = new Panel();
            _BottomBorderPanel.MouseDown += new MouseEventHandler(BottomPanel_MouseDown);
            _BottomBorderPanel.MouseMove += new MouseEventHandler(BottomPanel_MouseMove);
            _BottomBorderPanel.MouseUp += new MouseEventHandler(BottomPanel_MouseUp);
            _TopPanel = new Panel();
            _TopPanel.MouseDown += new MouseEventHandler(TopPanel_MouseDown);
            _TopPanel.MouseMove += new MouseEventHandler(TopPanel_MouseMove);
            _TopPanel.MouseUp += new MouseEventHandler(TopPanel_MouseUp);
            _IconPanel = new Panel();
            _WindowTextLabel = new Label();
            _MaxButton = new MinMaxButton();
            _MaxButton.Click += new System.EventHandler(MaxButton_Click);
            _MinButton = new ZopeButton();
            _MinButton.Click += new System.EventHandler(MinButton_Click);
            _CloseButton = new ZopeButton();
            _CloseButton.Click += new System.EventHandler(CloseButton_Click);
            _ToolTip = new ToolTip(components);
            _TopPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _TopBorderPanel
            // 
            _TopBorderPanel.BackColor = Color.FromArgb(10, 20, 50);
            _TopBorderPanel.Cursor = Cursors.SizeNS;
            _TopBorderPanel.Dock = DockStyle.Top;
            _TopBorderPanel.Location = new Point(0, 0);
            _TopBorderPanel.Name = "_TopBorderPanel";
            _TopBorderPanel.Size = new Size(684, 2);
            _TopBorderPanel.TabIndex = 0;
            // 
            // _RightBorderPanel
            // 
            _RightBorderPanel.BackColor = Color.FromArgb(10, 20, 50);
            _RightBorderPanel.Cursor = Cursors.SizeWE;
            _RightBorderPanel.Dock = DockStyle.Right;
            _RightBorderPanel.Location = new Point(682, 2);
            _RightBorderPanel.Name = "_RightBorderPanel";
            _RightBorderPanel.Size = new Size(2, 459);
            _RightBorderPanel.TabIndex = 1;
            // 
            // _LeftBorderPanel
            // 
            _LeftBorderPanel.BackColor = Color.FromArgb(10, 20, 50);
            _LeftBorderPanel.Cursor = Cursors.SizeWE;
            _LeftBorderPanel.Dock = DockStyle.Left;
            _LeftBorderPanel.Location = new Point(0, 2);
            _LeftBorderPanel.Name = "_LeftBorderPanel";
            _LeftBorderPanel.Size = new Size(2, 459);
            _LeftBorderPanel.TabIndex = 2;
            // 
            // _BottomBorderPanel
            // 
            _BottomBorderPanel.BackColor = Color.FromArgb(10, 20, 50);
            _BottomBorderPanel.Cursor = Cursors.SizeNS;
            _BottomBorderPanel.Dock = DockStyle.Bottom;
            _BottomBorderPanel.Location = new Point(2, 459);
            _BottomBorderPanel.Name = "_BottomBorderPanel";
            _BottomBorderPanel.Size = new Size(680, 2);
            _BottomBorderPanel.TabIndex = 3;
            // 
            // _TopPanel
            // 
            _TopPanel.BackColor = Color.FromArgb(20, 40, 80);
            _TopPanel.Controls.Add(_IconPanel);
            _TopPanel.Controls.Add(_WindowTextLabel);
            _TopPanel.Controls.Add(_MaxButton);
            _TopPanel.Controls.Add(_MinButton);
            _TopPanel.Controls.Add(_CloseButton);
            _TopPanel.Dock = DockStyle.Top;
            _TopPanel.Location = new Point(2, 2);
            _TopPanel.Name = "_TopPanel";
            _TopPanel.Size = new Size(680, 35);
            _TopPanel.TabIndex = 4;
            // 
            // _IconPanel
            // 
            _IconPanel.BackgroundImage = Testers.Properties.Resources.Crazy;
            _IconPanel.Location = new Point(10, 3);
            _IconPanel.Name = "_IconPanel";
            _IconPanel.Size = new Size(26, 26);
            _IconPanel.TabIndex = 5;
            // 
            // _WindowTextLabel
            // 
            _WindowTextLabel.AutoSize = true;
            _WindowTextLabel.Font = new Font("Microsoft Sans Serif", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _WindowTextLabel.ForeColor = Color.White;
            _WindowTextLabel.Location = new Point(57, 7);
            _WindowTextLabel.Name = "_WindowTextLabel";
            _WindowTextLabel.Size = new Size(77, 18);
            _WindowTextLabel.TabIndex = 6;
            _WindowTextLabel.Text = "Blue Form";
            // 
            // _MaxButton
            // 
            _MaxButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _MaxButton.BusyBackColor = Color.FromArgb(20, 40, 80);
            _MaxButton.CustomFormState = CustomFormState.Normal;
            _MaxButton.DisplayText = "_";
            _MaxButton.FlatStyle = FlatStyle.Flat;
            _MaxButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _MaxButton.ForeColor = Color.White;
            _MaxButton.Location = new Point(604, 3);
            _MaxButton.MouseClickColor = Color.FromArgb(10, 20, 60);
            _MaxButton.MouseHoverColor = Color.FromArgb(40, 80, 180);
            _MaxButton.Name = "_MaxButton";
            _MaxButton.Size = new Size(35, 25);
            _MaxButton.TabIndex = 5;
            _MaxButton.Text = "minMaxButton1";
            _MaxButton.TextLocation = new Point(11, 8);
            _ToolTip.SetToolTip(_MaxButton, "Maximize");
            _MaxButton.UseVisualStyleBackColor = true;
            // 
            // _MinButton
            // 
            _MinButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _MinButton.BusyBackColor = Color.FromArgb(20, 40, 80);
            _MinButton.DisplayText = "_";
            _MinButton.FlatStyle = FlatStyle.Flat;
            _MinButton.Font = new Font("Microsoft YaHei UI", 18.0f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _MinButton.ForeColor = Color.White;
            _MinButton.Location = new Point(569, 3);
            _MinButton.MouseClickColor = Color.FromArgb(10, 20, 60);
            _MinButton.MouseColorsEnabled = true;
            _MinButton.MouseHoverColor = Color.FromArgb(40, 80, 180);
            _MinButton.Name = "_MinButton";
            _MinButton.Size = new Size(35, 25);
            _MinButton.TabIndex = 1;
            _MinButton.Text = "_";
            _MinButton.TextLocationLeft = 8;
            _MinButton.TextLocationTop = -14;
            _ToolTip.SetToolTip(_MinButton, "Minimize");
            _MinButton.UseVisualStyleBackColor = true;
            // 
            // _CloseButton
            // 
            _CloseButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _CloseButton.BusyBackColor = Color.FromArgb(20, 40, 80);
            _CloseButton.DisplayText = "X";
            _CloseButton.FlatStyle = FlatStyle.Flat;
            _CloseButton.Font = new Font("Microsoft YaHei UI", 11.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _CloseButton.ForeColor = Color.White;
            _CloseButton.Location = new Point(639, 3);
            _CloseButton.MouseClickColor = Color.FromArgb(150, 0, 0);
            _CloseButton.MouseColorsEnabled = true;
            _CloseButton.MouseHoverColor = Color.FromArgb(40, 80, 180);
            _CloseButton.Name = "_CloseButton";
            _CloseButton.Size = new Size(35, 25);
            _CloseButton.TabIndex = 0;
            _CloseButton.Text = "X";
            _CloseButton.TextLocationLeft = 10;
            _CloseButton.TextLocationTop = 4;
            _ToolTip.SetToolTip(_CloseButton, "Close");
            _CloseButton.UseVisualStyleBackColor = true;
            // 
            // BlueForm
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(30, 60, 150);
            ClientSize = new Size(684, 461);
            Controls.Add(_TopPanel);
            Controls.Add(_BottomBorderPanel);
            Controls.Add(_LeftBorderPanel);
            Controls.Add(_RightBorderPanel);
            Controls.Add(_TopBorderPanel);
            FormBorderStyle = FormBorderStyle.None;
            Name = "BlueForm";
            Text = "Blue Form";
            _TopPanel.ResumeLayout(false);
            _TopPanel.PerformLayout();
            ResumeLayout(false);
        }

        private Panel _TopBorderPanel;
        private Panel _RightBorderPanel;
        private Panel _LeftBorderPanel;
        private Panel _BottomBorderPanel;
        private Panel _TopPanel;
        private ZopeButton _CloseButton;
        private MinMaxButton _MaxButton;
        private ZopeButton _MinButton;
        private ToolTip _ToolTip;
        private Label _WindowTextLabel;
        private Panel _IconPanel;
    }
}
