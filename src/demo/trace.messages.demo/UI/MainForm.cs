using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.WinControls.ComboBoxEnumExtensions;

namespace TraceMessagesDemo.UI
{
    public partial class MainForm : Form
    {

        #region " CONSTRUCTION and CLEANUP "

        public MainForm()
        {
            this.InitializeComponent();

            // Microsoft Log Levels
            // Trace:
            //     Logs that contain the most detailed messages. These messages may contain sensitive
            //     application data. These messages are disabled by default and should never be
            //     enabled in a production environment.
            // 
            //
            // Debug:
            //     Logs that are used for interactive investigation during development. These logs
            //     should primarily contain information useful for debugging and have no long-term
            //     value.
            //            Debug,
            //
            // Information:
            //     Logs that track the general flow of the application. These logs should have long-term
            //     value.
            //            Information,
            //
            // Warning:
            //     Logs that highlight an abnormal or unexpected event in the application flow,
            //     but do not otherwise cause the application execution to stop.
            //            Warning,
            //
            // Error:
            //     Logs that highlight when the current flow of execution is stopped due to a failure.
            //     These should indicate a failure in the current activity, not an application-wide
            //     failure.
            //            Error,
            //
            // Critical:
            //     Logs that describe an unrecoverable application or system crash, or a catastrophic
            //     failure that requires immediate attention.
            //            Critical,
            //
            // None:
            //     Not used for writing log messages. Specifies that a logging category should not
            //     write any messages.
            //            None
            // serilog log levels:
            //     Verbose: Anything and everything you might want to know about a running block of code.
            //       Debug:  Internal system events that aren't necessarily observable from the outside.
            // Information: The lifeblood of operational intelligence - things happen.
            //     Warning: Service is degraded or endangered.
            //       Error: Functionality is unavailable, invariants are broken or data is lost.
            //       Fatal: If you have a pager, it goes off when one of these occurs.
            // 
            _ = this.ShowLevelComboBox.ComboBox.ListEnumNames<TraceEventType>();
            _ = this.EventLevelComboBox.ComboBox.ListEnumNames<Microsoft.Extensions.Logging.LogLevel>();
            _ = this.LogLevelComboBox.ComboBox.ListEnumNames<Microsoft.Extensions.Logging.LogLevel>();
            this.ShowLevelComboBox.ComboBox.SelectedValue = TraceEventType.Information;
            this.EventLevelComboBox.ComboBox.SelectedValue = Microsoft.Extensions.Logging.LogLevel.Error;
            this.LogLevelComboBox.ComboBox.SelectedValue = Microsoft.Extensions.Logging.LogLevel.Information;

            // add a node so we can hide the messages box.
            TextBox hideTextBox = new();
            hideTextBox.Multiline = true;
            hideTextBox.WordWrap = true;
            hideTextBox.ReadOnly = true;
            hideTextBox.BackColor = System.Drawing.Color.AliceBlue;
            hideTextBox.Text = @"This panel is used to hide the Message Box in order to test the change of node caption when the messages box changes visibility";
            _ = this.TreePanel.AddNode( "Messages Not Visible", "Invisible", hideTextBox );

            this._MessagesBox = new();
            TreeNode messagesNode = this.TreePanel.AddNode( "Message Box", "Log", this._MessagesBox );

            this.TextBoxTextWriter = new( this._MessagesBox );
            this.TextBoxTextWriter.ContainerTreeNode = messagesNode;
            this.TextBoxTextWriter.TabCaption = "Log";
            this.TextBoxTextWriter.CaptionFormat = "{0} " + Convert.ToChar( 0x1C2 );
            this.TextBoxTextWriter.ResetCount = 1000;
            this.TextBoxTextWriter.PresetCount = 500;
            this.TextBoxTextWriter.TraceLevel = this.ShowLevelComboBox.SelectedEnumValue( TraceEventType.Error );
            isr.Core.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.TextBoxTextWriter );

            this.ErrorButtonTraceAlertContainer = new( this.ErrorAlertToolStripButton, this._MessagesBox );
            this.ErrorButtonTraceAlertContainer.AlertAnnunciatorText = "Error";
            this.ErrorButtonTraceAlertContainer.AlertLevel = TraceEventType.Error;
            this.ErrorButtonTraceAlertContainer.AlertSoundEnabled = true;
            isr.Core.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.ErrorButtonTraceAlertContainer );

            this.WarningButtonTraceAlertContainer = new( this.WarningAlertToolStripButton, this._MessagesBox );
            this.WarningButtonTraceAlertContainer.AlertAnnunciatorText = "Warning";
            this.WarningButtonTraceAlertContainer.AlertLevel = TraceEventType.Warning;
            this.WarningButtonTraceAlertContainer.AlertSoundEnabled = true;
            isr.Core.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.WarningButtonTraceAlertContainer );

            this.InformationButtonTraceAlertContainer = new( this.InformationAlertToolStripButton, this._MessagesBox );
            this.InformationButtonTraceAlertContainer.AlertAnnunciatorText = "Information";
            this.InformationButtonTraceAlertContainer.AlertLevel = TraceEventType.Information;
            this.InformationButtonTraceAlertContainer.AlertSoundEnabled = true;
            isr.Core.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.InformationButtonTraceAlertContainer );
        }

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                this.components?.Dispose();
            }
            base.Dispose( disposing );
        }

        #endregion

        #region " MESSAGES BOX "

        private readonly MessagesBox _MessagesBox;

        #endregion

        #region " TEXT BOX TRACE EVENT WRITER  "

        private isr.Core.Tracing.WinForms.TextBoxTraceEventWriter TextBoxTextWriter { get; set; }

        #endregion

        #region " TRACE ALERT CONTAINERS "

        private isr.Core.Tracing.WinForms.TraceAlertContainer ErrorButtonTraceAlertContainer { get; set; }

        private isr.Core.Tracing.WinForms.TraceAlertContainer WarningButtonTraceAlertContainer { get; set; }

        private isr.Core.Tracing.WinForms.TraceAlertContainer InformationButtonTraceAlertContainer { get; set; }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        private void TraceLevelComboBox_Validated( object sender, EventArgs e )
        {
            this.TextBoxTextWriter.TraceLevel = this.ShowLevelComboBox.SelectedEnumValue( TraceEventType.Error );
        }
        private void EventLevelComboBox_Validated( object sender, EventArgs e )
        {
        }

        private void LogLevelComboBox_Validated( object sender, EventArgs e )
        {
            isr.Core.TraceLog.TraceLogger.Instance.MinimumLogLevel = this.LogLevelComboBox.SelectedEnumValue( Microsoft.Extensions.Logging.LogLevel.Error );
        }

        private int _MessageNumber;
        private void SendLogMessageButton_Click( object sender, EventArgs e )
        {
            this._MessageNumber += 1;
            Microsoft.Extensions.Logging.LogLevel logLevel = this.EventLevelComboBox.SelectedEnumValue( Microsoft.Extensions.Logging.LogLevel.Trace );
            string message = $"{logLevel} Message #{this._MessageNumber} @ {DateTime.Now}";
            _ = isr.Core.TraceLog.TraceLogger.LogCallerMessage( logLevel, message );
        }

        #endregion

    }

}
