using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace TraceMessagesDemo.UI
{

    /// <summary>
    /// Messages display text box with context menu for clearing a trace, display of the log file or
    /// opening the log file folder.
    /// </summary>
    /// <remarks>
    /// (c) 2002 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2002-09-21, 1.0.839 </para>
    /// </remarks>
    [Description( "Messages Text Box" )]
    public partial class MessagesBox : TextBox
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Constructor for this class. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        public MessagesBox() : base()
        {
            this.WordWrap = false;
            this.Multiline = true;
            this.ReadOnly = true;
            this.CausesValidation = false;
            this.ScrollBars = ScrollBars.Both;
            this.Size = new System.Drawing.Size( 150, 150 );
            this.BackColor = System.Drawing.SystemColors.Info;
            this.Font = new System.Drawing.Font( "Consolas", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0 );
            base.ContextMenuStrip = this.CreateContextMenuStrip();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this._MyContextMenuStrip?.Dispose();
                    this._MyContextMenuStrip = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " CONTEXT MENU STRIP "

        /// <summary> my context menu strip. </summary>
        private ContextMenuStrip _MyContextMenuStrip;

        /// <summary> Creates a context menu strip. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> The new context menu strip. </returns>
        private ContextMenuStrip CreateContextMenuStrip()
        {

            // Create a new ContextMenuStrip control.
            this._MyContextMenuStrip = new ContextMenuStrip();

            // Attach an event handler for the 
            // ContextMenuStrip control's Opening event.
            this._MyContextMenuStrip.Opening += this.ContectMenuOpeningHandler;
            return this._MyContextMenuStrip;
        }

        /// <summary> Adds menu items. </summary>
        /// <remarks>
        /// This event handler is invoked when the <see cref="ContextMenuStrip"/> control's Opening event
        /// is raised.
        /// </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        private void ContectMenuOpeningHandler( object sender, CancelEventArgs e )
        {
            this._MyContextMenuStrip = sender as ContextMenuStrip;

            // Clear the ContextMenuStrip control's Items collection.
            this._MyContextMenuStrip.Items.Clear();

            // Populate the ContextMenuStrip control with its default items.
            // myContextMenuStrip.Items.Add("-")
            _ = this._MyContextMenuStrip.Items.Add( new ToolStripMenuItem( "Clear &All", null, this.ClearAllHandler, "Clear" ) );
            _ = this._MyContextMenuStrip.Items.Add( new ToolStripMenuItem( "Flush &Queue", null, this.FlushQueuesHandler, "Flush" ) );
            _ = this._MyContextMenuStrip.Items.Add( new ToolStripMenuItem( "&Open Log File", null, this.RequestOpeningLogFile, "Open Log File" ) );
            _ = this._MyContextMenuStrip.Items.Add( new ToolStripMenuItem( "Open Log &Folder", null, this.RequestOpeningLogFolder, "Open Log Folder" ) );

            // Set Cancel to false. 
            // It is optimized to true based on empty entry.
            e.Cancel = false;
        }

        /// <summary> Applies the high point Output. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ClearAllHandler( object sender, EventArgs e )
        {
            this.Clear();
        }

        /// <summary> Flush messages and content. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FlushQueuesHandler( object sender, EventArgs e )
        {
            Trace.Flush();
        }

        #endregion

        #region " LOG FILE HANDLING "

        /// <summary> Request opening log File. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RequestOpeningLogFile( object sender, EventArgs e )
        {
            var (success, details, _) = isr.Core.TraceLog.TraceLogger.OpenLogFile();
            if ( !success )
                this.Text = details;
        }

        /// <summary> Request opening log folder. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RequestOpeningLogFolder( object sender, EventArgs e )
        {
            var (success, details, _) = isr.Core.TraceLog.TraceLogger.OpenFolderLocation();
            if ( !success )
                this.Text = details;
        }

        #endregion

    }
}
