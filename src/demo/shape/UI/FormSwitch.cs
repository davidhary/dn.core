using System;
using System.Windows.Forms;

namespace isr.Core.Testers
{
    /// <summary>   A form switch. </summary>
    /// <remarks>   David, 2021-03-12. </remarks>
    public partial class FormSwitch : Form
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        public FormSwitch()
        {
            this.InitializeComponent();
        }

        /// <summary>   Event handler. Called by Button1 for click events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Button1_Click( object sender, EventArgs e )
        {
            var f1 = new Form1();
            _ = f1.ShowDialog();
        }

        /// <summary>   Event handler. Called by Button2 for click events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Button2_Click( object sender, EventArgs e )
        {
            var f2 = new Form2();
            _ = f2.ShowDialog();
        }

        /// <summary>   Event handler. Called by Button3 for click events. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Button3_Click( object sender, EventArgs e )
        {
            var f3 = new Form3();
            _ = f3.ShowDialog();
        }
    }
}
