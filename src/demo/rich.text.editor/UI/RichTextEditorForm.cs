using System;
using System.Windows.Forms;

namespace isr.Core.Testers.UI
{
    public partial class RichTextEditorForm : Form
    {
        public RichTextEditorForm()
        {
            this.InitializeComponent();
            this.RichTextEditor.ExitRequested += new EventHandler( this.Editor_ExitRequestd );
        }

        private void Editor_ExitRequestd( object sender, EventArgs e )
        {
            this.Close();
        }

    }
}
