using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Testers
{
    [DesignerGenerated()]
    public partial class NullableNumericUpDownForm : Form
    {

        /// <summary>   Form overrides dispose to clean up the component list. </summary>
        /// <remarks>   David, 2021-03-12. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ValueTextBoxLabel = new Label();
            _SpinningValueTextBox = new TextBox();
            _ChangedValueTextBoxLabel = new Label();
            _ChangedValueTextBox = new TextBox();
            _ValidatedValueTextBoxLabel = new Label();
            _ValidatedValueTextBox = new TextBox();
            _SpinningNullableValueTextBox = new TextBox();
            _ValidatedNullableValueTextBox = new TextBox();
            _ChangedNullableValueTextBox = new TextBox();
            Label1 = new Label();
            _NullableNumericUpDownLabel = new Label();
            _NullableNumericUpDown = new isr.Core.WinControls.NullableNumericUpDown();
            _NullableTextTextBoxLabel = new Label();
            _NullableTextTextBox = new TextBox();
            _NilifyButton = new Button();
            _NilifyButton.Click += new EventHandler(NilifyButton_Click);
            _NullableValueTextBox = new TextBox();
            _Timer = new Timer(components);
            _Timer.Tick += new EventHandler(Timer_Tick);
            ((System.ComponentModel.ISupportInitialize)_NullableNumericUpDown).BeginInit();
            SuspendLayout();
            // 
            // _ValueTextBoxLabel
            // 
            _ValueTextBoxLabel.AutoSize = true;
            _ValueTextBoxLabel.Location = new Point(40, 75);
            _ValueTextBoxLabel.Name = "_ValueTextBoxLabel";
            _ValueTextBoxLabel.Size = new Size(51, 13);
            _ValueTextBoxLabel.TabIndex = 4;
            _ValueTextBoxLabel.Text = "Spinning:";
            _ValueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _SpinningValueTextBox
            // 
            _SpinningValueTextBox.Location = new Point(93, 72);
            _SpinningValueTextBox.Name = "_SpinningValueTextBox";
            _SpinningValueTextBox.Size = new Size(100, 20);
            _SpinningValueTextBox.TabIndex = 5;
            // 
            // _ChangedValueTextBoxLabel
            // 
            _ChangedValueTextBoxLabel.AutoSize = true;
            _ChangedValueTextBoxLabel.Location = new Point(38, 113);
            _ChangedValueTextBoxLabel.Name = "_ChangedValueTextBoxLabel";
            _ChangedValueTextBoxLabel.Size = new Size(53, 13);
            _ChangedValueTextBoxLabel.TabIndex = 7;
            _ChangedValueTextBoxLabel.Text = "Changed:";
            _ChangedValueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _ChangedValueTextBox
            // 
            _ChangedValueTextBox.Location = new Point(93, 110);
            _ChangedValueTextBox.Name = "_ChangedValueTextBox";
            _ChangedValueTextBox.Size = new Size(100, 20);
            _ChangedValueTextBox.TabIndex = 8;
            // 
            // _ValidatedValueTextBoxLabel
            // 
            _ValidatedValueTextBoxLabel.AutoSize = true;
            _ValidatedValueTextBoxLabel.Location = new Point(37, 152);
            _ValidatedValueTextBoxLabel.Name = "_ValidatedValueTextBoxLabel";
            _ValidatedValueTextBoxLabel.Size = new Size(54, 13);
            _ValidatedValueTextBoxLabel.TabIndex = 10;
            _ValidatedValueTextBoxLabel.Text = "Validated:";
            _ValidatedValueTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _ValidatedValueTextBox
            // 
            _ValidatedValueTextBox.Location = new Point(93, 149);
            _ValidatedValueTextBox.Name = "_ValidatedValueTextBox";
            _ValidatedValueTextBox.Size = new Size(100, 20);
            _ValidatedValueTextBox.TabIndex = 11;
            // 
            // _SpinningNullableValueTextBox
            // 
            _SpinningNullableValueTextBox.Location = new Point(199, 72);
            _SpinningNullableValueTextBox.Name = "_SpinningNullableValueTextBox";
            _SpinningNullableValueTextBox.Size = new Size(100, 20);
            _SpinningNullableValueTextBox.TabIndex = 6;
            // 
            // _ValidatedNullableValueTextBox
            // 
            _ValidatedNullableValueTextBox.Location = new Point(199, 149);
            _ValidatedNullableValueTextBox.Name = "_ValidatedNullableValueTextBox";
            _ValidatedNullableValueTextBox.Size = new Size(100, 20);
            _ValidatedNullableValueTextBox.TabIndex = 12;
            // 
            // _ChangedNullableValueTextBox
            // 
            _ChangedNullableValueTextBox.Location = new Point(199, 110);
            _ChangedNullableValueTextBox.Name = "_ChangedNullableValueTextBox";
            _ChangedNullableValueTextBox.Size = new Size(100, 20);
            _ChangedNullableValueTextBox.TabIndex = 9;
            // 
            // Label1
            // 
            Label1.AutoSize = true;
            Label1.Location = new Point(97, 56);
            Label1.Name = "Label1";
            Label1.Size = new Size(37, 13);
            Label1.TabIndex = 2;
            Label1.Text = "Value:";
            Label1.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // _NullableNumericUpDownLabel
            // 
            _NullableNumericUpDownLabel.AutoSize = true;
            _NullableNumericUpDownLabel.Location = new Point(196, 56);
            _NullableNumericUpDownLabel.Name = "_NullableNumericUpDownLabel";
            _NullableNumericUpDownLabel.Size = new Size(78, 13);
            _NullableNumericUpDownLabel.TabIndex = 3;
            _NullableNumericUpDownLabel.Text = "Nullable Value:";
            _NullableNumericUpDownLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // _NullableNumericUpDown
            // 
            _NullableNumericUpDown.Location = new Point(49, 16);
            _NullableNumericUpDown.Maximum = new decimal(new int[] { 100000, 0, 0, 0 });
            _NullableNumericUpDown.Name = "_NullableNumericUpDown";
            _NullableNumericUpDown.ReadOnlyBackColor = Color.Empty;
            _NullableNumericUpDown.ReadOnlyForeColor = Color.Empty;
            _NullableNumericUpDown.ReadWriteBackColor = Color.Empty;
            _NullableNumericUpDown.ReadWriteForeColor = Color.Empty;
            _NullableNumericUpDown.Size = new Size(145, 20);
            _NullableNumericUpDown.TabIndex = 0;
            _NullableNumericUpDown.Validated += new EventHandler( NullableNumericUpDown_Validated );
            _NullableNumericUpDown.ValueChanged += new EventHandler( NullableNumericUpDown_ValueChanged );
            _NullableNumericUpDown.ValueDecrementing += new EventHandler<System.ComponentModel.CancelEventArgs>( NullableNumericUpDown_ValueDecrementing );
            _NullableNumericUpDown.ValueIncrementing += new EventHandler<System.ComponentModel.CancelEventArgs>( NullableNumericUpDown_ValueIncrementing );
            // 
            // _NullableTextTextBoxLabel
            // 
            _NullableTextTextBoxLabel.AutoSize = true;
            _NullableTextTextBoxLabel.Location = new Point(164, 186);
            _NullableTextTextBoxLabel.Name = "_NullableTextTextBoxLabel";
            _NullableTextTextBoxLabel.Size = new Size(31, 13);
            _NullableTextTextBoxLabel.TabIndex = 13;
            _NullableTextTextBoxLabel.Text = "Text:";
            _NullableTextTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _NullableTextTextBox
            // 
            _NullableTextTextBox.Location = new Point(198, 183);
            _NullableTextTextBox.Name = "_NullableTextTextBox";
            _NullableTextTextBox.Size = new Size(100, 20);
            _NullableTextTextBox.TabIndex = 14;
            // 
            // _NilifyButton
            // 
            _NilifyButton.Location = new Point(223, 16);
            _NilifyButton.Name = "_NilifyButton";
            _NilifyButton.Size = new Size(75, 23);
            _NilifyButton.TabIndex = 1;
            _NilifyButton.Text = "Nilify";
            _NilifyButton.UseVisualStyleBackColor = true;
            // 
            // _NullableValueTextBox
            // 
            _NullableValueTextBox.Location = new Point(302, 18);
            _NullableValueTextBox.Name = "_NullableValueTextBox";
            _NullableValueTextBox.Size = new Size(57, 20);
            _NullableValueTextBox.TabIndex = 14;
            // 
            // _Timer
            // 
            // 
            // NullableNumericUpDownForm
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(371, 261);
            Controls.Add(_NilifyButton);
            Controls.Add(_NullableValueTextBox);
            Controls.Add(_NullableTextTextBox);
            Controls.Add(_ValidatedValueTextBox);
            Controls.Add(_NullableTextTextBoxLabel);
            Controls.Add(_ValidatedValueTextBoxLabel);
            Controls.Add(_ChangedNullableValueTextBox);
            Controls.Add(_ChangedValueTextBox);
            Controls.Add(_ChangedValueTextBoxLabel);
            Controls.Add(_ValidatedNullableValueTextBox);
            Controls.Add(_SpinningNullableValueTextBox);
            Controls.Add(_SpinningValueTextBox);
            Controls.Add(_NullableNumericUpDownLabel);
            Controls.Add(Label1);
            Controls.Add(_ValueTextBoxLabel);
            Controls.Add(_NullableNumericUpDown);
            Name = "NullableNumericUpDownForm";
            Text = "Nullable Numeric Up Down Form";
            ((System.ComponentModel.ISupportInitialize)_NullableNumericUpDown).EndInit();
            Shown += new EventHandler(NullableNumericUpDownForm_Shown);
            ResumeLayout(false);
            PerformLayout();
        }

        private isr.Core.WinControls.NullableNumericUpDown _NullableNumericUpDown;
        private Label _ValueTextBoxLabel;
        private TextBox _SpinningValueTextBox;
        private Label _ChangedValueTextBoxLabel;
        private TextBox _ChangedValueTextBox;
        private Label _ValidatedValueTextBoxLabel;
        private TextBox _ValidatedValueTextBox;
        private TextBox _SpinningNullableValueTextBox;
        private TextBox _ValidatedNullableValueTextBox;
        private TextBox _ChangedNullableValueTextBox;
        private Label Label1;
        private Label _NullableNumericUpDownLabel;
        private Label _NullableTextTextBoxLabel;
        private TextBox _NullableTextTextBox;
        private Button _NilifyButton;
        private TextBox _NullableValueTextBox;
        private Timer _Timer;
    }
}
