using System;
using System.Windows.Forms;

namespace isr.Core.Testers
{
    public partial class CustomProgressBarForm : Form
    {
        public CustomProgressBarForm()
        {
            base.Load += this.Form1_Load;
            this.Shown += this.Form1_Shown;
            this.InitializeComponent();
        }

        private void FadeTrackBar_Scroll( object sender, EventArgs e )
        {
            int fadeAlpha = this.FadeTrackBar.Value;
            this.CustomProgressBar.Fade = fadeAlpha;
            this._StatusStripCustomProgressBar.Fade = fadeAlpha;
            this.FadeTrackBarlabel.Text = "Fade Alpha: " + fadeAlpha.ToString();
        }

        private void Form1_Load( object sender, EventArgs e )
        {
        }

        private void Form1_Shown( object sender, EventArgs e )
        {
            this.CustomProgressBar.Value = 75;
            this.StandardProgressBar.Value = 75;
            this._StatusStripCustomProgressBar.Value = 75;
            this.ToolStripProgressBar.Value = 75;
        }
    }
}
