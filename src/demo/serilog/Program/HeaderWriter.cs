using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

using Serilog.Debugging;

namespace Serilog.Sinks.File.Header
{

    /// <inheritdoc />
    /// <summary>
    /// Writes a header at the start of every log file
    /// </summary>
    /// <remarks> Forked from https://github.com/cocowalla/serilog-sinks-file-header <para>
    /// Adds getting the file name.
    /// </para>
    /// </remarks>
    public class HeaderWriter : FileLifecycleHooks
    {
        // Same as the default StreamWriter buffer size
        private const int _DefaultBufferSize = 1024;

        // Factory method to generate the file header
        private readonly Func<string> _HeaderFactory;

        // Whether to always write the header, even for non-empty files
        private readonly bool _AlwaysWriteHeader;

        public HeaderWriter( string header, bool alwaysWriteHeader = false )
        {
            this._HeaderFactory = () => header;
            this._AlwaysWriteHeader = alwaysWriteHeader;
        }

        public HeaderWriter( Func<string> headerFactory, bool alwaysWriteHeader = false )
        {
            this._HeaderFactory = headerFactory;
            this._AlwaysWriteHeader = alwaysWriteHeader;
        }

        /// <summary>   Gets or sets the filename of the full file. </summary>
        /// <value> The filename of the full file. </value>
        public static string FullFileName { get; private set; }

        /// <summary>   Waits for the first setup of the <see cref="FullFileName"/>. </summary>
        /// <remarks>   David, 2021-02-09. </remarks>
        /// <param name="timeout">  The timeout. </param>
        /// <param name="dueTime">  The due time. </param>
        /// <param name="period">   The period. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool AwaitFullFileName( TimeSpan timeout, TimeSpan dueTime, TimeSpan period )
        {
            // Create an AutoResetEvent to signal the timeout threshold in the
            // timer callback has been reached.
            var autoEvent = new AutoResetEvent( false );

            var fullFileNameChecker = new FullFileNameChecker( timeout );

            // Create a timer that invokes the file open checked after 'due time' and every 'period' time thereafter.
            var stateTimer = new Timer( fullFileNameChecker.CheckFullFileName, autoEvent, dueTime, period );

            // When autoEvent signals, dispose of the timer.
            _ = autoEvent.WaitOne();
            stateTimer.Dispose();

            return !string.IsNullOrEmpty( HeaderWriter.FullFileName );
        }

        private class FullFileNameChecker
        {

            private TimeSpan _Timeout;
            private readonly Stopwatch _Sw;

            public FullFileNameChecker( TimeSpan timeout )
            {
                this._Timeout = timeout;
                this._Sw = Stopwatch.StartNew();
            }

            /// <summary>   Restarts the given timeout. </summary>
            /// <remarks>   David, 2021-02-09. </remarks>
            /// <param name="timeout">  The timeout. </param>
            public void Restart( TimeSpan timeout )
            {
                this._Timeout = timeout;
                this._Sw.Restart();
            }

            /// <summary>   Queries if the full file name was set. </summary>
            /// <remarks>   David, 2021-02-09.  This method is called by the timer delegate.
            /// </remarks>
            /// <param name="stateInfo">    Information describing the state. </param>
            public void CheckFullFileName( Object stateInfo )
            {
                AutoResetEvent autoEvent = ( AutoResetEvent ) stateInfo;

                if ( !string.IsNullOrEmpty( HeaderWriter.FullFileName ) || (this._Sw.Elapsed > this._Timeout) )
                {
                    // signal the waiting thread.
                    _ = autoEvent.Set();
                }
            }
        }

        /// <summary>
        /// Initialize or wrap the <paramref name="underlyingStream" /> opened on the log file. This can
        /// be used to write file headers, or wrap the stream in another that adds buffering, compression,
        /// encryption, etc. The underlying file may or may not be empty when this method is called.
        /// </summary>
        /// <remarks>
        /// A value must be returned from overrides of this method. Serilog will flush and/or dispose the
        /// returned value, but will not dispose the stream initially passed in unless it is itself
        /// returned.
        /// </remarks>
        /// <param name="path">             The full path to the log file. </param>
        /// <param name="underlyingStream"> The underlying <see cref="T:System.IO.Stream" /> opened on
        ///                                 the log file. </param>
        /// <param name="encoding">         The encoding to use when reading/writing to the stream. </param>
        /// <returns>
        /// The <see cref="T:System.IO.Stream" /> Serilog should use when writing events to the log file.
        /// </returns>
        public override Stream OnFileOpened( string path, Stream underlyingStream, Encoding encoding )
        {

            try
            {
                HeaderWriter.FullFileName = path;
                if ( this._AlwaysWriteHeader && underlyingStream.Length != 0 )
                {
                    SelfLog.WriteLine( $"File header will not be written, as the stream already contains {underlyingStream.Length} bytes of content" );
                    return base.OnFileOpened( underlyingStream, encoding );
                }
            }
            catch ( NotSupportedException )
            {
                // Not all streams support reading the length - in this case, we always write the header,
                // otherwise we'd *never* write it!
            }

            using ( var writer = new StreamWriter( underlyingStream, encoding, _DefaultBufferSize, true ) )
            {
                var header = this._HeaderFactory();

                writer.WriteLine( header );
                writer.Flush();
                underlyingStream.Flush();
            }

            return base.OnFileOpened( underlyingStream, encoding );
        }
    }
}
