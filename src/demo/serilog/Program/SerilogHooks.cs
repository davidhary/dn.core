using System;
using System.Linq;
using System.Reflection;

using Serilog.Sinks.File.Header;

namespace isr.Core.Serilog.Tester
{
    /// <summary>   A Serilog hooks. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    public class SerilogHooks
    {

        /// <summary>   Builds time caption using <see cref="DateTimeOffset"/> showing local time and offset. </summary>
        /// <remarks>
        /// <list type="bullet">Use the following format options: <item>
        /// u - UTC - 2019-09-10 19:27:04Z</item><item>
        /// r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
        /// o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
        /// s - ISO - 2019-09-10T12:24:47</item><item>
        /// empty   - 2019-09-10 16:57:24 -07:00</item><item>
        /// s + zzz - 2019-09-10T12:24:47-07:00</item></list>
        /// </remarks>
        /// <param name="timeCaptionFormat">    The time caption format. </param>
        /// <param name="kindFormat">           The kind format. </param>
        /// <returns>   A string. </returns>
        public static string BuildLocalTimeCaption( string timeCaptionFormat, string kindFormat )
        {
            string result = string.IsNullOrWhiteSpace( timeCaptionFormat )
                ? $"{DateTimeOffset.Now}"
                : string.IsNullOrWhiteSpace( kindFormat )
                    ? $"{DateTimeOffset.Now.ToString( timeCaptionFormat )}"
                    : $"{DateTimeOffset.Now.ToString( timeCaptionFormat )}{DateTimeOffset.Now.ToString( kindFormat )}";
            return result;
        }

        /// <summary> Prefix process name to the product name. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <returns> A String. </returns>
        public static string PrefixProcessName( Assembly assemblyInfo )
        {
            if ( assemblyInfo is null )
            {
                throw new ArgumentNullException( nameof( assemblyInfo ) );
            }

            var productName = assemblyInfo
                .GetCustomAttributes( typeof( AssemblyProductAttribute ) )
                .OfType<AssemblyProductAttribute>()
                .FirstOrDefault().Product;
            string processName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
            if ( !productName.StartsWith( processName, StringComparison.OrdinalIgnoreCase ) )
            {
                productName = $"{processName}.{productName}";
            }
            return productName;
        }

        /// <summary> Builds product time caption. </summary>
        /// <remarks>
        /// <list type="bullet">Use the following format options: <item>
        /// u - UTC - 2019-09-10 19:27:04Z</item><item>
        /// r - GMT - Tue, 10 May 2019 19:26:42 GMT</item><item>
        /// o - ISO - 2019-09-10T12:12:29.7552627-07:00</item><item>
        /// s - ISO - 2019-09-10T12:24:47</item><item>
        /// empty   - 2019-09-10 16:57:24 -07:00</item><item>
        /// s + zzz - 2019-09-10T12:24:47-07:00</item></list>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="assemblyInfo">      Information describing the assembly. </param>
        /// <param name="versionElements">   The version elements. </param>
        /// <param name="timeCaptionFormat"> The time caption format. </param>
        /// <param name="kindFormat">        The kind format. </param>
        /// <returns> A String. </returns>
        public static string BuildProductTimeCaption( Assembly assemblyInfo, int versionElements, string timeCaptionFormat, string kindFormat )
        {
            return assemblyInfo is null
                ? throw new ArgumentNullException( nameof( assemblyInfo ) )
                : $"{PrefixProcessName( assemblyInfo )}.r.{assemblyInfo.GetName().Version.ToString( versionElements )} {SerilogHooks.BuildLocalTimeCaption( timeCaptionFormat, kindFormat )}";
        }

        /// <summary>   Gets file name. </summary>
        /// <remarks>   David, 2021-02-06. </remarks>
        /// <param name="stream"> The stream. </param>
        /// <returns>   The file name.
        ///             see this for changing to get the return value of the file name,
        ///             https://github.com/cocowalla/serilog-sinks-file-header/blob/master/src/Serilog.Sinks.File.Header/HeaderWriter.cs </returns>
        public static string GetFileName( System.IO.Stream stream )
        {
            return stream is System.IO.FileStream fs
                ? fs.Name
                : string.Empty;
        }

        /// <summary>
        /// Builds product time caption using full version and local time plus kind format.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="assemblyInfo"> Information describing the assembly. </param>
        /// <returns> A String. </returns>
        public static string BuildProductTimeCaption( Assembly assemblyInfo )
        {
            return BuildProductTimeCaption( assemblyInfo, 4, string.Empty, string.Empty );
        }

        /// <summary>
        /// Builds product time caption using full version and local time plus kind format.
        /// </summary>
        /// <remarks>   David, 2021-02-08. </remarks>
        /// <returns>   A String. </returns>
        public static string BuildProductTimeCaption()
        {
            return BuildProductTimeCaption( Assembly.GetEntryAssembly(), 4, string.Empty, string.Empty );
        }

        public static HeaderWriter MyHeaderWriter
        {
            get {
                return new( @$"{BuildProductTimeCaption()}{Environment.NewLine}Timestamp, Level, Source, Message" );
            }
        }
}

}
