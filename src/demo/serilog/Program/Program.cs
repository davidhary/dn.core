using System;
using System.Linq;
using System.Reflection;

using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Serilog;
using Serilog.Debugging;
using Serilog.Sinks.File.Header;

namespace isr.Core.Serilog.Tester
{
    /// <summary>   A program. </summary>
    /// <remarks>   David, 2021-02-04. </remarks>
    internal class Program
    {
        /// <summary>   Main entry-point for this application. </summary>
        /// <remarks>   David, 2021-02-04. </remarks>
        /// <param name="args"> An array of command-line argument strings. </param>
        private static void Main( string[] args )
        {
            var app = Program.CreateApp();
            Program.DefineOptions( app );
            Program.DefineExecution( app );
            try
            {
                // This begins the actual execution of the application
                Console.WriteLine( "Logging Tester executing..." );
                int outcome = app.Execute( args );
                Console.WriteLine( $"Logging Tester exited with code {outcome}" );
            }
            catch ( CommandParsingException ex )
            {
                // You'll always want to catch this exception, otherwise it will generate a messy and confusing error for the end user.
                // the message will usually be something like:
                // "Unrecognized command or argument '<invalid-command>'"
                Console.WriteLine( $"Exception; {Environment.NewLine}{ex}." );
            }
            catch ( Exception ex )
            {
                Console.WriteLine( $"Exception; {Environment.NewLine}{ex}." );
            }

        }

        /// <summary>   Define execution. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        private static void DefineExecution( CommandLineApplication app )
        {

            // When no commands are specified, this block will execute.
            // This is the main "command"
            app.OnExecute( () => {

                // Use the HasValue() method to check if the option was specified
                CommandOption basicOption = app.GetOptions().Where( x => x.ShortName == "o" ).FirstOrDefault();
                if ( basicOption is object && basicOption.HasValue() )
                {
                    Console.WriteLine( $"Running '{basicOption.Value()}' option..." );
                    switch ( basicOption.Value() )
                    {
                        case "serilog":
                            RunSerilogImplementation();
                            break;

                        default:
                            break;
                    }
                    Console.Write( "\r\n\r\nDone. Enter key: " );
                    _ = Console.ReadKey();

                }
                else
                {
                    Console.WriteLine( $"Logger option not specified; {Environment.NewLine}{app.GetHelpText()}." );
                }
                return 0;
            } );

        }

        #region " COMMAND LINE PARSING "

        /// <summary>   Creates the application. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <returns>   The new application. </returns>
        private static CommandLineApplication CreateApp()
        {
            // Instantiate the command line application
            var app = new CommandLineApplication {
                // This should be the name of the executable itself. the help text line "Usage: ConsoleArgs" uses this
                Name = "LoggingTester",
                Description = ".NET Core logging tester",
                ExtendedHelpText = @"This program demonstrates the logging functionality of the isr.Core.Logging.Logger."
            };

            // Set the arguments to display the description and help text
            _ = app.HelpOption( "-?|-h|--help" );

            // This is a helper/shortcut method to display version info - it is creating a regular Option, with some defaults.
            // The default help text is "Show version Information"
            _ = app.VersionOption( "-v|--version", () => {
                return $"Version {Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion}";
            } );

            return app;
        }

        /// <summary>   Define application options. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        /// <returns>   A CommandOption. </returns>
        private static void DefineOptions( CommandLineApplication app )
        {
            // Arguments: -o listener -l error -d ..\_log -f Column

            // The first argument is the option template.
            // It starts with a pipe-delimited list of option flags/names to use
            // Optionally, It is then followed by a space and a short description of the value to specify.
            // e.g. here we could also just use "-o|--option"
            _ = app.Option( "-o|--option <value>", "Listener or Provider Logging implementation", CommandOptionType.SingleValue );
            _ = app.Option( "-l|--level <value>", "Logging level: Information, Warning, Error", CommandOptionType.SingleValue );
            _ = app.Option( "-d|--dir <value>", "Relative Directory, e.g., ..\\_log", CommandOptionType.SingleValue );
            _ = app.Option( "-f|--form <value>", "Logging format: Column, Row, Tab, Comma", CommandOptionType.SingleValue );
        }

        #endregion

        #region " SERILOG IMPLEMENTATION "

        private static void ConfigureSerilogServices( string settingsFileName, IServiceCollection serviceCollection )
        {
            // Add logging
            _ = serviceCollection.AddLogging( loggingBuilder => {
                _ = loggingBuilder.AddSerilog();
                _ = loggingBuilder.AddConsole();
                _ = loggingBuilder.AddDebug();
            } );

            // Build configuration
            var configuration = new ConfigurationBuilder()
                .SetBasePath( AppContext.BaseDirectory )
                .AddJsonFile( settingsFileName, false )
                .Build();

            // Initialize the Serilog logger using Json configuration file.
            Log.Logger = new LoggerConfiguration()
                 .ReadFrom.Configuration( configuration )
                 .CreateLogger();

            // Add access to generic IConfigurationRoot
            _ = serviceCollection.AddSingleton( configuration );

            // Add services
            _ = serviceCollection.AddTransient<ITestService, TestService>();

            // Add the application
            _ = serviceCollection.AddTransient<App>();
        }

        /// <summary>
        /// Builds application settings file title and extension consisting of the
        /// <paramref name="assemblyTitle"/>+<paramref name="suffix"/>+.json.
        /// </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="assemblyTitle">    The assembly file title. </param>
        /// <param name="suffix">           (Optional) The suffix, which defaults to '.logSettings'. </param>
        /// <returns>   A file title and extension. </returns>
        public static string BuildAppSettingsFileName( string assemblyTitle, string suffix = ".SerilogSettings" )
        {
            return $"{assemblyTitle}{suffix}.json";
        }

        /// <summary>
        /// Builds application settings file name consisting of the
        /// 'Calling Assembly Name'+<paramref name="suffix"/>+.json.
        /// </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="suffix">   (Optional) The suffix, which defaults to '.logSettings'. </param>
        /// <returns>   A string. </returns>
        public static string BuildAppSettingsFileName( string suffix = ".SerilogSettings" )
        {
            return Program.BuildAppSettingsFileName( System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, suffix );
        }

        /// <summary>   Executes the 'provider implementation' operation. </summary>
        /// <remarks>   David, 2021-02-04. </remarks>
        private static void RunSerilogImplementation()
        {
            // Create service collection
            var serviceCollection = new ServiceCollection();
            ConfigureSerilogServices( Program.BuildAppSettingsFileName(), serviceCollection );

            // Create service provider
            var serviceProvider = serviceCollection.BuildServiceProvider();

            // a logger can be instantiated at this point. 
            ILogger<Program> logger = serviceProvider.GetService<ILoggerFactory>()
                                       .CreateLogger<Program>();

            SelfLog.Enable( Console.Error );

            logger.LogInformation( "Starting application..." );

            // entry to run the application
            serviceProvider.GetService<App>().Run();
        }

        #endregion

    }

    #region " LOGGER-INJECTED APPLICATION CLASSES "

    /// <summary>
    /// To maintain a separation of concern between our business based logic and the logic we use to
    /// configure and run the actual console application, let's create a new class called App.cs. The
    /// goal being that we will use Program.cs to bootstrap everything need to support our
    /// application and then fire off all the logic that is needed to support out "business" needs by
    /// way of executing the Run() method in App.cs.
    /// </summary>
    /// <remarks>   David, 2021-02-04. </remarks>
    public class App
    {
        private readonly ITestService _TestService;
        private readonly ILogger<App> _Logger;

        public App( ITestService testService, ILogger<App> logger )
        {
            this._TestService = testService;
            this._Logger = logger;
        }

        public void Run()
        {
            this._Logger.LogInformation( "Running application." );
            TimeSpan timeout = TimeSpan.FromMilliseconds( 100 );
            bool fileOpened = HeaderWriter.AwaitFullFileName( timeout, TimeSpan.FromMilliseconds( 20 ), TimeSpan.FromMilliseconds( 20 ) );
            if ( fileOpened )
            {
                this._Logger.LogInformation( $"Logging into {HeaderWriter.FullFileName}." );
            }
            else
            {
                this._Logger.LogInformation( $"File not open after {timeout.TotalMilliseconds}ms." );
            }
            try
            {
                this._TestService.Run();
            }
            catch ( Exception ex )
            {
                InvalidOperationException reportEx = new( $"Caught in {nameof( App )}", ex );
                this._Logger.LogError( reportEx, "reporting exception" );
            }
            Log.CloseAndFlush();
        }
    }


    /// <summary>   Interface for test service. </summary>
    /// <remarks>   David, 2021-02-04. </remarks>
    public interface ITestService
    {
        void Run();
    }

    /// <summary>   A service for accessing tests information. </summary>
    /// <remarks>   David, 2021-02-04. </remarks>
    internal class TestService : ITestService
    {
        private readonly ILogger<TestService> _Logger;

        public TestService( ILogger<TestService> logger )
        {
            this._Logger = logger;
        }

        public void Run()
        {
            this._Logger.LogDebug( $"Running logging service." );
            this._Logger.LogDebug( $"Throwing divide by zero exception." );
            throw new DivideByZeroException();
        }
    }

    #endregion

}
