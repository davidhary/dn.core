using System;

using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Legacy.MSTest
{

    /// <summary> An exception extensions tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-26 </para>
    /// </remarks>
    [TestClass()]
    public class ExceptionExtensionsTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " TO FULL BLOWN STRING "

        /// <summary> Throw argument exception. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="version"> The version. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        private static void ThrowArgumentException( Version version )
        {
            throw new ArgumentException( "Forced argument exception", nameof( version ) );
        }

        /// <summary> (Unit Test Method) tests full blown exception. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void FullBlownExceptionTest()
        {
            var version = new Version( 0, 0, 0, 0 );
            string expectedKey = "0-Name";
            string expectedValue = nameof( version );
            try
            {
                ThrowArgumentException( version );
            }
            catch ( ArgumentException ex )
            {
                string result = isr.Core.ExceptionExtensions.ExceptionExtensionMethods.ToFullBlownString( ex );
                // this checks the exception data.
                Assert.IsTrue( ex.Data.Contains( expectedKey ), $"Data contains key {expectedKey}" );
                Assert.AreEqual( expectedValue, Conversions.ToString( ex.Data[expectedKey] ), $"Data value of {expectedKey}" );
                // this checks the stack trace
                string fullText = ExceptionExtensions.ExceptionExtensionMethods.ToFullBlownString( ex );
                string searchFor = nameof( isr.Core.Framework.Legacy.MSTest.ExceptionExtensionsTests.FullBlownExceptionTest );
                Assert.IsTrue( fullText.Contains( searchFor ), $"{fullText} contains {searchFor}" );
            }
            catch ( Exception ex )
            {
                Assert.Fail( $"Expected exception not caught; actual exception: {ex}" );
            }
        }


        #endregion

    }
}
