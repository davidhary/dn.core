using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Legacy.MSTest
{

    /// <summary>
    /// This is a test class for StringEnumeratorTest and is intended to contain all
    /// StringEnumeratorTest Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class StringEnumeratorTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " STRING ENUMERATOR TESTS "

        /// <summary> A test for TryParse. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="value">             The value. </param>
        /// <param name="enumValueExpected"> The enum value expected. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void TryParseTestHelper<T>( string value, T enumValueExpected ) where T : struct
        {
            var target = new StringEnumerator<T>();
            bool expected = true;
            bool actual;
            T enumValue = default;
            actual = target.TryParse( value, ref enumValue );
            Assert.AreEqual( enumValueExpected, enumValue );
            Assert.AreEqual( expected, actual );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a test value test from the given data, returning a
        /// default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void TryParseTestValueTest()
        {
            this.TryParseTestHelper( "2", TestValue.Error );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a color test from the given data, returning a default
        /// value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void TryParseColorTest()
        {
            var colorStrings = new string[] { "0", "2", "8", "blue", "Blue", "Yellow", "Red, Green" };
            // var colorValues = new Colors[] { Colors.None, Colors.Green, Colors.Red | Colors.Green | Colors.Blue, Colors.Blue, Colors.Blue, Colors.None, Colors.Red | Colors.Green };
            foreach ( string colorString in colorStrings )
            {
                bool actual = Enum.TryParse( colorString, out Colors colorValue );
                if ( actual )
                {
                    if ( Enum.IsDefined( typeof( Colors ), colorValue ) | colorValue.ToString().Contains( "," ) )
                    {
                        Trace.TraceInformation( $"Converted '{colorString}' to {colorValue}." );
                    }
                    else
                    {
                        Trace.TraceInformation( $"{colorString } is not an underlying value of the Colors enumeration." );
                    }
                }
                else
                {
                    Trace.TraceInformation( $"{colorString} is not a member of the Colors enumeration." );
                }
            }
        }

        /// <summary> Values that represent test values. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        private enum TestValue
        {
            /// <summary> An enum constant representing the none option. </summary>
            None,
            /// <summary> An enum constant representing the Information option. </summary>
            Info,
            /// <summary> An enum constant representing the error] option. </summary>
            Error
        }

        /// <summary> A bit-field of flags for specifying colors. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [Flags()]
        public enum Colors : int
        {
            /// <summary> An enum constant representing the red option. </summary>
            Red = 1,
            /// <summary> An enum constant representing the green option. </summary>
            Green = 2,
            /// <summary> An enum constant representing the blue option. </summary>
            Blue = 4
        }
        #endregion

    }
}
