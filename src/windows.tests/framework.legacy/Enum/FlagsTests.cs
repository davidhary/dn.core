#region License and Terms
// Unconstrained Melody
// Copyright (c) 2009-2011 Jonathan Skeet. All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#endregion

using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Legacy.MSTest
{
    /// <summary>   (Unit Test Class) the flags tests. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [TestClass()]
    public class FlagsTests
    {

        /// <summary>   (Unit Test Method) bit flags enum should be flags. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void BitFlagsEnumShouldBeFlags()
        {
            Assert.IsTrue( Flags.IsFlags<BitFlags>() );
        }

        /// <summary>   (Unit Test Method) number enum should not be flags. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void NumberEnumShouldNotBeFlags()
        {
            Assert.IsTrue( Flags.IsFlags<BitFlags>() );
            Assert.IsFalse( Flags.IsFlags<Number>() );
        }

        /// <summary>   (Unit Test Method) flag combination should be valid. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void FlagCombinationShouldBeValid()
        {
            Assert.IsTrue( BitFlags.Flag24.IsValidCombination() );
            Assert.IsTrue( (BitFlags.Flag1 | BitFlags.Flag2).IsValidCombination() );
            Assert.IsTrue( Flags.IsValidCombination<BitFlags>( 0 ) );
        }

        /// <summary>   (Unit Test Method) flag combination shoul not be valid. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void FlagCombinationShoulNotBeValid()
        {
            Assert.IsFalse( (( BitFlags ) 100).IsValidCombination() );
        }


        /// <summary>   (Unit Test Method) ORed value should equal. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void OredValueShouldEqual()
        {
            Assert.AreEqual( BitFlags.Flag1 | BitFlags.Flag2, BitFlags.Flag1.Or( BitFlags.Flag2 ) );
            Assert.AreEqual( BitFlags.Flag1, BitFlags.Flag1.Or( BitFlags.Flag1 ) );
        }

        /// <summary>   (Unit Test Method) and values should equal. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void AndValuesShouldEqual()
        {
            Assert.AreEqual( BitFlags.Flag2 & BitFlags.Flag24, BitFlags.Flag2.And( BitFlags.Flag24 ) );
            Assert.AreEqual( BitFlags.Flag24 & BitFlags.Flag2, BitFlags.Flag24.And( BitFlags.Flag2 ) );
            Assert.AreEqual( BitFlags.Flag1, BitFlags.Flag1.And( BitFlags.Flag1 ) );
        }

        /// <summary>   (Unit Test Method) gets used bits should equal. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetUsedBitsShouldEqual()
        {
            Assert.AreEqual( BitFlags.Flag1 | BitFlags.Flag2 | BitFlags.Flag4, Flags.GetUsedBits<BitFlags>() );
        }

        /// <summary>   (Unit Test Method) inversed used bits should equal. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void InversedUsedBitsShouldEqual()
        {
            Assert.AreEqual( BitFlags.Flag1, BitFlags.Flag24.UsedBitsInverse() );
            Assert.AreEqual( BitFlags.Flag24, BitFlags.Flag1.UsedBitsInverse() );
        }

        /// <summary>   (Unit Test Method) inverse all bits should equal. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void InverseAllBitsShouldEqual()
        {
            Assert.AreEqual( ~BitFlags.Flag1, BitFlags.Flag1.AllBitsInverse() );
            Assert.AreEqual( ~BitFlags.Flag24, BitFlags.Flag24.AllBitsInverse() );
        }

        /// <summary>   (Unit Test Method) has any should be true. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void HasAnyShouldBeTrue()
        {
            Assert.IsTrue( BitFlags.Flag2.HasAny( BitFlags.Flag24 ) );
            Assert.IsTrue( BitFlags.Flag24.HasAny( BitFlags.Flag2 ) );
        }

        /// <summary>   (Unit Test Method) has any should be false. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void HasAnyShouldBeFalse()
        {
            Assert.IsFalse( BitFlags.Flag2.HasAny( BitFlags.Flag1 ) );
        }

        /// <summary>   (Unit Test Method) has all should be true. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void HasAllShouldBeTrue()
        {
            Assert.IsTrue( BitFlags.Flag24.HasAll( BitFlags.Flag2 ) );
            Assert.IsTrue( BitFlags.Flag24.HasAll( BitFlags.Flag24 ) );
        }

        /// <summary>   (Unit Test Method) has all should be false. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void HasAllShouldBeFalse()
        {
            Assert.IsFalse( BitFlags.Flag2.HasAll( BitFlags.Flag24 ) );
            Assert.IsFalse( BitFlags.Flag2.HasAll( BitFlags.Flag1 ) );
        }

        #region " TypeArgumentException tests (all very boring) "

        /// <summary>   Assert type argument exception. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        /// <param name="action">   The action. </param>
        private static void AssertTypeArgumentException( Action action )
        {
            _ = Assert.ThrowsException<TypeArgumentException>( action );
        }

        /// <summary>   (Unit Test Method) is valid combination for non flags should throw. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void IsValidCombinationForNonFlagsShouldThrow()
        {
            AssertTypeArgumentException( () => Number.Two.IsValidCombination() );
        }

        /// <summary>   (Unit Test Method) and for non flags should throw. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void AndForNonFlagsShouldThrow()
        {
            AssertTypeArgumentException( () => Number.Two.And( Number.One ) );
        }

        /// <summary>   (Unit Test Method) or for non flags should throw. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void OrForNonFlagsShouldThrow()
        {
            AssertTypeArgumentException( () => Number.Two.Or( Number.One ) );
        }

        /// <summary>   (Unit Test Method) used bits inverse for non flags should throw. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void UsedBitsInverseForNonFlagsShouldThrow()
        {
            AssertTypeArgumentException( () => Number.Two.UsedBitsInverse() );
        }

        /// <summary>   (Unit Test Method) all bits inverse for non flags should throw. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void AllBitsInverseForNonFlagsShouldThrow()
        {
            AssertTypeArgumentException( () => Number.Two.AllBitsInverse() );
        }

        /// <summary>   (Unit Test Method) is empty for non flags should throw. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void IsEmptyForNonFlagsShouldThrow()
        {
            AssertTypeArgumentException( () => Number.Two.IsEmpty() );
        }

        /// <summary>   (Unit Test Method) is not empty for non flags should throw. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void IsNotEmptyForNonFlagsShouldThrow()
        {
            AssertTypeArgumentException( () => Number.Two.IsNotEmpty() );
        }

        /// <summary>   (Unit Test Method) gets used bits for non flags should throw. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetUsedBitsForNonFlagsShouldThrow()
        {
            AssertTypeArgumentException( () => Flags.GetUsedBits<Number>() );
        }

        /// <summary>   (Unit Test Method) has any for non flags should throw. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void HasAnyForNonFlagsShouldThrow()
        {
            AssertTypeArgumentException( () => Number.One.HasAny( Number.Two ) );
        }

        /// <summary>   (Unit Test Method) has all for non flags should throw. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void HasAllForNonFlagsShouldThrow()
        {
            AssertTypeArgumentException( () => Number.One.HasAll( Number.Two ) );
        }
        #endregion
    }
}
