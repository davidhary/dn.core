using System;
using System.Linq;

using Microsoft.VisualBasic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Legacy.MSTest
{

    /// <summary>
    /// This is a test class for EfficientEnumTest and is intended to contain all EfficientEnumTest
    /// Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class EnumExtenderTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " ENUM EXTENDER TESTS "

        /// <summary> The color enum. </summary>
        private static readonly EnumExtender<Color> ColorEnum = new();

        /// <summary> Values that represent colors. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        private enum Color
        {
            /// <summary> An enum constant representing the white option. </summary>
            [System.ComponentModel.Description( "While Color" )]
            White,
            /// <summary> An enum constant representing the black option. </summary>
            [System.ComponentModel.Description( "Black Color" )]
            Black,
            /// <summary> An enum constant representing the red option. </summary>
            [System.ComponentModel.Description( "Red Color" )]
            Red,
            /// <summary> An enum constant representing the yellow option. </summary>
            [System.ComponentModel.Description( "Yellow Color" )]
            Yellow,
            /// <summary> An enum constant representing the blue option. </summary>
            [System.ComponentModel.Description( "Blue Color" )]
            Blue,
            /// <summary> An enum constant representing the green option. </summary>
            [System.ComponentModel.Description( "Green Color" )]
            Green,
            /// <summary> An enum constant representing the cyan option. </summary>
            [System.ComponentModel.Description( "Cyan Color" )]
            Cyan,
            /// <summary> An enum constant representing the magenta option. </summary>
            [System.ComponentModel.Description( "Magenta Color" )]
            Magenta,
            /// <summary> An enum constant representing the pink option. </summary>
            [System.ComponentModel.Description( "Pink Color" )]
            Pink,
            /// <summary> An enum constant representing the purple option. </summary>
            [System.ComponentModel.Description( "Purple Color" )]
            Purple,
            /// <summary> An enum constant representing the orange option. </summary>
            [System.ComponentModel.Description( "Orange Color" )]
            Orange,
            /// <summary> An enum constant representing the brown option. </summary>
            [System.ComponentModel.Description( "Brown Color" )]
            Brown
        }

        /// <summary> Values that represent Strings. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        private static readonly string[] EnumStrings = new string[] { "White", "Black", "Red", "Yellow", "Blue", "Green", "Cyan", "Magenta", "Pink", "Purple", "Orange", "Brown" };

        /// <summary> A performance monitor. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        internal class PerformanceMonitor : IDisposable
        {
            /// <summary> The time started. </summary>
            private readonly long _Timestarted;

            /// <summary> The name. </summary>
            private readonly string _Name;

            /// <summary> Constructor. </summary>
            /// <remarks> David, 2020-09-18. </remarks>
            /// <param name="name"> The name. </param>
            internal PerformanceMonitor( string name )
            {
                this._Name = name;
                this._Timestarted = DateTimeOffset.Now.Ticks;
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
            /// resources.
            /// </summary>
            /// <remarks> David, 2020-09-18. </remarks>
            public void Dispose()
            {
                Console.WriteLine( "Operation " + this._Name + ":" + Constants.vbTab + Constants.vbTab + (DateTimeOffset.Now.Ticks - this._Timestarted).ToString() );
            }
        }

        /// <summary> (Unit Test Method) converts this object to an object test 1. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ToObjectTest1()
        {
            int value = ( int ) Color.Cyan;
            var expected = Color.Cyan;
            var actual = ColorEnum.ToObject( value );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) converts this object to an object test 2. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ToObjectTest2()
        {
            int value = ( int ) Color.Cyan;
            var expected = Color.Cyan;
            var actual = ColorEnum.ToObject( value );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) parse test 1. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ParseTest1()
        {
            string value = nameof( Color.White );
            bool ignoreCase = false;
            var expected = Color.White;
            var actual = ColorEnum.Parse( value, ignoreCase );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> A test for Parse. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void ParseTestHelper<T>()
        {
            string value = nameof( Color.Cyan );
            var expected = Color.Cyan;
            var actual = ColorEnum.Parse( value );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests parse. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ParseTest()
        {
            ParseTestHelper<Color>();
        }

        /// <summary> A test for IsDefined. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void IsDefinedTestHelper<T>()
        {
            object value = Color.Cyan;
            bool expected = true;
            bool actual = ColorEnum.IsDefined( value );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests is defined. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void IsDefinedTest()
        {
            IsDefinedTestHelper<Color>();
        }

        /// <summary> A test for GetValues. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void GetValuesTestHelper<T>()
        {
            var expected = Enum.GetValues( typeof( Color ) );
            var actual = ColorEnum.Values;
            var a = new int[expected.Length];
            actual.CopyTo( a, 0 );
            var b = new int[actual.Length];
            expected.CopyTo( b, 0 );
            Assert.IsTrue( a.Length == b.Length && a.Intersect( b ).Count() == a.Length, "Values are equal" );
        }

        /// <summary> (Unit Test Method) tests get values. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void GetValuesTest()
        {
            GetValuesTestHelper<Color>();
        }

        /// <summary> A test for GetUnderlyingType. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void GetUnderlyingTypeTestHelper<T>()
        {
            var expected = typeof( int );
            var actual = ColorEnum.UnderlyingType;
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests get underlying type. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void GetUnderlyingTypeTest()
        {
            GetUnderlyingTypeTestHelper<Color>();
        }

        /// <summary> A test for GetNames. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void GetNamesTestHelper<T>()
        {
            var expected = EnumStrings;
            var actual = ColorEnum.Names.ToArray();
            Assert.IsTrue( expected.Length == actual.Length && expected.Intersect( actual ).Count() == expected.Length, "Names are equal" );
        }

        /// <summary> (Unit Test Method) tests get names. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void GetNamesTest()
        {
            GetNamesTestHelper<Color>();
        }

        /// <summary> A test for GetName. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void GetNameTestHelper<T>()
        {
            var value = Color.Black;
            string expected = nameof( Color.Black );
            string actual = ColorEnum.Name( value );
            Assert.AreEqual( expected, actual, $"Name of {value}" );
        }

        /// <summary> (Unit Test Method) tests get name. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void GetNameTest()
        {
            GetNameTestHelper<Color>();
        }

        /// <summary> A test for EfficientEnum`1 Constructor. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public static void EfficientEnumConstructorTestHelper<T>()
        {
            var target = new EnumExtender<T>();
            string expectedDesction = "Black Color";
            string actualDescription = target.Description( Color.Black );
            Assert.AreEqual( expectedDesction, actualDescription, $"Description of {Color.Black}" );
        }

        /// <summary> (Unit Test Method) tests efficient enum constructor. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void EfficientEnumConstructorTest()
        {
            EfficientEnumConstructorTestHelper<Color>();
        }
        #endregion

    }
}
