#region License and Terms
// Unconstrained Melody
// Copyright (c) 2009-2011 Jonathan Skeet. All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#endregion

using System;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Legacy.MSTest
{

    /// <summary>   (Unit Test Class) the enums tests. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    [TestClass()]
    public class EnumsTests
    {
        /// <summary>   (Unit Test Method) gets values array should sequence equal. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetValuesArrayShouldSequenceEqual()
        {
            Number[] numbers = Enums.GetValuesArray<Number>();
            Assert.IsTrue( numbers.SequenceEqual( new[] { Number.One, Number.Two, Number.Three } ) );
        }

        /// <summary>   (Unit Test Method) gets values should return singleton. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetValuesShouldReturnSingleton()
        {
            var numbers = Enums.GetValues<Number>();
            var numbers2 = Enums.GetValues<Number>();
            Assert.AreSame( numbers, numbers2 );
        }

        /// <summary>   (Unit Test Method) gets values should return read only list. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetValuesShouldReturnReadOnlyList()
        {
            var numbers = Enums.GetValues<Number>();
            Assert.IsTrue( numbers.IsReadOnly );
        }

        /// <summary>   (Unit Test Method) gets values should return correct values. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetValuesShouldReturnCorrectValues()
        {
            var numbers = Enums.GetValues<Number>();
            Assert.IsTrue( numbers.SequenceEqual( new[] { Number.One, Number.Two, Number.Three } ) );
        }

        /// <summary>   (Unit Test Method) gets names array should sequence equal. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetNamesArrayShouldSequenceEqual()
        {
            string[] numbers = Enums.GetNamesArray<Number>();
            Assert.IsTrue( numbers.SequenceEqual( new[] { "One", "Two", "Three" } ) );
        }

        /// <summary>   (Unit Test Method) gets names should return singleton. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetNamesShouldReturnSingleton()
        {
            var names = Enums.GetNames<Number>();
            var names2 = Enums.GetNames<Number>();
            Assert.AreSame( names, names2 );
        }

        /// <summary>   (Unit Test Method) gets names should return read only list. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetNamesShouldReturnReadOnlyList()
        {
            var names = Enums.GetNames<Number>();
            Assert.IsTrue( names.IsReadOnly );
        }

        /// <summary>   (Unit Test Method) gets names should return correct values. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetNamesShouldReturnCorrectValues()
        {
            var names = Enums.GetNames<Number>();
            Assert.IsTrue( names.SequenceEqual( new[] { "One", "Two", "Three" } ) );
        }

        /// <summary>   (Unit Test Method) flag should be named value. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void FlagShouldBeNamedValue()
        {
            Assert.IsTrue( BitFlags.Flag24.IsNamedValue() );
        }

        /// <summary>   (Unit Test Method) enum should be named value. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void EnumShouldBeNamedValue()
        {
            Assert.IsTrue( Number.One.IsNamedValue() );
        }

        /// <summary>   (Unit Test Method) flags combination should not be named value. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void FlagsCombinationShouldNotBeNamedValue()
        {
            Assert.IsFalse( (BitFlags.Flag1 | BitFlags.Flag2).IsNamedValue() );
        }

        /// <summary>   (Unit Test Method) undefined enum should not be named value. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void UndefinedEnumShouldNotBeNamedValue()
        {
            Assert.IsFalse( Enums.IsNamedValue<Number>( 0 ) );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a name non flags should parse from the given data,
        /// returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void TryParseNameNonFlagsShouldParse()
        {
            Assert.IsTrue( Enums.TryParseName( "One", out Number number ) );
            Assert.AreEqual( Number.One, number );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a name non flags value should not parse from the given
        /// data, returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void TryParseNameNonFlagsValueShouldNotParse()
        {
            Assert.IsFalse( Enums.TryParseName( "1", out Number _ ) );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a name non flags value should equal zero from the given
        /// data, returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void TryParseNameNonFlagsValueShouldEqualZero()
        {
            _ = Enums.TryParseName( "1", out Number number );
            Assert.AreEqual( ( Number ) 0, number );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a name non flags unknown should not parse from the given
        /// data, returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void TryParseNameNonFlagsUnknownShouldNotParse()
        {
            Assert.IsFalse( Enums.TryParseName( "rubbish", out Number number ) );
            Assert.AreEqual( ( Number ) 0, number );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a name non flags unknown value should be zero from the
        /// given data, returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void TryParseNameNonFlagsUnknownValueShouldBeZero()
        {
            _ = Enums.TryParseName( "rubbish", out Number number );
            Assert.AreEqual( ( Number ) 0, number );
        }


        /// <summary>
        /// (Unit Test Method) attempts to parse a name flags from the given data, returning a default
        /// value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void TryParseNameFlags()
        {
            Assert.IsTrue( Enums.TryParseName( "Flag24", out BitFlags result ) );
            Assert.AreEqual( BitFlags.Flag24, result );
            Assert.IsFalse( Enums.TryParseName( "1", out result ) );
            Assert.AreEqual( ( BitFlags ) 0, result );
            Assert.IsFalse( Enums.TryParseName( "rubbish", out result ) );
            Assert.AreEqual( ( BitFlags ) 0, result );
            Assert.IsFalse( Enums.TryParseName( "Flag2,Flag4", out result ) );
            Assert.AreEqual( ( BitFlags ) 0, result );
        }

        /// <summary>   (Unit Test Method) parse name invalid value. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void ParseNameInvalidValue()
        {
            _ = Assert.ThrowsException<ArgumentException>( () => Enums.ParseName<Number>( "rubish" ) );
        }

        /// <summary>   (Unit Test Method) parse name. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void ParseName()
        {
            Assert.AreEqual( Number.Two, Enums.ParseName<Number>( "Two" ) );
            Assert.AreEqual( BitFlags.Flag24, Enums.ParseName<BitFlags>( "Flag24" ) );
        }

        /// <summary>   (Unit Test Method) gets underlying type shoud equal. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetUnderlyingTypeShoudEqual()
        {
            Assert.AreEqual( typeof( byte ), Enums.GetUnderlyingType<ByteEnum>() );
            Assert.AreEqual( typeof( int ), Enums.GetUnderlyingType<Number>() );
            Assert.AreEqual( typeof( long ), Enums.GetUnderlyingType<Int64Enum>() );
            Assert.AreEqual( typeof( ulong ), Enums.GetUnderlyingType<UInt64Enum>() );
        }

        /// <summary>   (Unit Test Method) gets description should equal. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetDescriptionShouldEqual()
        {
            Assert.AreEqual( "First description", Number.One.GetDescription() );
        }

        /// <summary>
        /// (Unit Test Method) gets description when value has no description should be null.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetDescriptionWhenValueHasNoDescriptionShouldBeNull()
        {
            Assert.IsNull( Number.Two.GetDescription() );
        }

        /// <summary>   (Unit Test Method) gets description for invalid value should throw. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void GetDescriptionForInvalidValueShouldThrow()
        {
            _ = Assert.ThrowsException<ArgumentOutOfRangeException>( () => (( Number ) 4).GetDescription() );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a unique description should succeed from the given data,
        /// returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void TryParseUniqueDescriptionShouldSucceed()
        {
            Assert.IsTrue( Enums.TryParseDescription<Number>( "Third description", out Number number ) );
            Assert.AreEqual( Number.Three, number );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a duplicate description should return the first match
        /// from the given data, returning a default value rather than throwing an exception if it
        /// fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void TryParseDuplicateDescriptionShouldReturnTheFirstMatch()
        {
            Assert.IsTrue( Enums.TryParseDescription<BitFlags>( "Duplicate description", out BitFlags result ) );
            Assert.AreEqual( BitFlags.Flag2, result );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a missing description should be false from the given
        /// data, returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [TestMethod()]
        public void TryParseMissingDescriptionShouldBeFalse()
        {
            Assert.IsFalse( Enums.TryParseDescription<Number>( "Doesn't exist", out _ ) );
        }
    }
}
