using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using isr.Core.EnumExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Legacy.MSTest
{

    /// <summary>
    /// This is a test class for EnumExtensionsTest and is intended to contain all EnumExtensionsTest
    /// Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class EnumExtensionsTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " ENUM EXTENSION TEST: VALUES "

        /// <summary> (Unit Test Method) tests enum values include. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void EnumValuesIncludeTest()
        {
            var allValues = typeof( TraceEventType ).EnumValues();
            Assert.IsTrue( allValues.Any(), $"{typeof( TraceEventType )}.EnumValues(of System.Enum) has values" );
            Assert.AreEqual( Enum.GetValues( typeof( TraceEventType ) ).Length, allValues.Count(), $"{typeof( TraceEventType )} count matches the extension result" );
            Assert.IsTrue( allValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the enum values of {typeof( TraceEventType )}" );

            var allLongs = typeof( TraceEventType ).Values();
            Assert.IsTrue( allLongs.Any(), $"{typeof( TraceEventType )}.EnumValues(of Long) has values" );
            Assert.AreEqual( Enum.GetValues( typeof( TraceEventType ) ).Length, allValues.Count(), $"{typeof( TraceEventType )}.EnumValues(of Long) count matches the extension result" );

            long includeMask = ( long ) (TraceEventType.Critical | TraceEventType.Error);
            var filteredValues = allValues.IncludeFilter( includeMask );
            int expectedCount = 2;
            Assert.IsTrue( filteredValues.Any(), $"{typeof( TraceEventType )} filtered Values has values" );
            Assert.AreEqual( expectedCount, filteredValues.Count(), $"{typeof( TraceEventType )} filtered values has expected count" );
            Assert.IsTrue( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the filtered values of {typeof( TraceEventType )}" );
            Assert.IsFalse( filteredValues.Contains( TraceEventType.Information ), $"{TraceEventType.Information} is not included in the filtered values of {typeof( TraceEventType )}" );

            var filteredLongs = allLongs.IncludeFilter( includeMask );
            Assert.IsTrue( filteredLongs.Any(), $"{typeof( TraceEventType )} filtered Longs has Longs" );
            Assert.AreEqual( expectedCount, filteredLongs.Count(), $"{typeof( TraceEventType )} filtered Longs has expected count" );
            Assert.IsTrue( filteredLongs.Contains( ( long ) TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the filtered Longs of {typeof( TraceEventType )}" );
            Assert.IsFalse( filteredLongs.Contains( ( long ) TraceEventType.Information ), $"{TraceEventType.Information} is not included in the filtered Longs of {typeof( TraceEventType )}" );

            IList<Enum> filter = new List<Enum> { TraceEventType.Critical, TraceEventType.Error };
            filteredValues = allValues.IncludeFilter( filter );
            // filteredValues = allValues.IncludeFilter(new Enum[] { TraceEventType.Critical, TraceEventType.Error });
            expectedCount = 2;
            Assert.IsTrue( filteredValues.Any(), $"{typeof( TraceEventType )} filtered Values has list of values" );
            Assert.AreEqual( expectedCount, filteredValues.Count(), $"{typeof( TraceEventType )} filtered lists of values has expected count" );
            Assert.IsTrue( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the filtered list of values of {typeof( TraceEventType )}" );
            Assert.IsFalse( filteredValues.Contains( TraceEventType.Information ), $"{TraceEventType.Information} is not included in the filtered list of values of {typeof( TraceEventType )}" );
        }

        /// <summary> (Unit Test Method) tests enum values exclude. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void EnumValuesExcludeTest()
        {
            var allValues = typeof( TraceEventType ).EnumValues();
            Assert.IsTrue( allValues.Any(), $"{typeof( TraceEventType )}.EnumValues(of System.Enum) has values" );
            Assert.AreEqual( Enum.GetValues( typeof( TraceEventType ) ).Length, allValues.Count(), $"{typeof( TraceEventType )} count matches the extension result" );
            Assert.IsTrue( allValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the enum values of {typeof( TraceEventType )}" );

            var allLongs = typeof( TraceEventType ).Values();
            Assert.IsTrue( allLongs.Any(), $"{typeof( TraceEventType )}.EnumValues(of Long) has values" );
            Assert.AreEqual( Enum.GetValues( typeof( TraceEventType ) ).Length, allValues.Count(), $"{typeof( TraceEventType )}.EnumValues(of Long) count matches the extension result" );

            long excludeMask = ( long ) (TraceEventType.Critical | TraceEventType.Error);
            var filteredValues = allValues.ExcludeFilter( excludeMask );
            int expectedCount = allValues.Count() - 2;
            Assert.IsTrue( filteredValues.Any(), $"{typeof( TraceEventType )} filtered Values has values" );
            Assert.AreEqual( expectedCount, filteredValues.Count(), $"{typeof( TraceEventType )} filtered values has expected count" );
            Assert.IsFalse( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the filtered values of {typeof( TraceEventType )}" );
            Assert.IsTrue( filteredValues.Contains( TraceEventType.Information ), $"{TraceEventType.Information} is not Excluded in the filtered values of {typeof( TraceEventType )}" );

            var filteredLongs = allLongs.ExcludeFilter( excludeMask );
            Assert.IsTrue( filteredLongs.Any(), $"{typeof( TraceEventType )} filtered Longs has Longs" );
            Assert.AreEqual( expectedCount, filteredLongs.Count(), $"{typeof( TraceEventType )} filtered Longs has expected count" );
            Assert.IsFalse( filteredLongs.Contains( ( long ) TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the filtered Longs of {typeof( TraceEventType )}" );
            Assert.IsTrue( filteredLongs.Contains( ( long ) TraceEventType.Information ), $"{TraceEventType.Information} is not Excluded in the filtered Longs of {typeof( TraceEventType )}" );

            IList<Enum> filter = new List<Enum> { TraceEventType.Critical, TraceEventType.Error };
            filteredValues = allValues.ExcludeFilter( filter );
            // filteredValues = allValues.ExcludeFilter( allValues );
            // filteredValues = allValues.ExcludeFilter( (new TraceEventType[] { TraceEventType.Critical, TraceEventType.Error } );
            expectedCount = allValues.Count() - 2;
            Assert.IsTrue( filteredValues.Any(), $"{typeof( TraceEventType )} filtered Values has list of values" );
            Assert.AreEqual( expectedCount, filteredValues.Count(), $"{typeof( TraceEventType )} filtered lists of values has expected count" );
            Assert.IsFalse( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the filtered list of values of {typeof( TraceEventType )}" );
            Assert.IsTrue( filteredValues.Contains( TraceEventType.Information ), $"{TraceEventType.Information} is not Excluded in the filtered list of values of {typeof( TraceEventType )}" );

            allValues = TraceEventType.Critical.EnumValues();
            Assert.IsTrue( allValues.Any(), $"{typeof( TraceEventType )}.EnumValues(of System.Enum) has values" );
            Assert.AreEqual( Enum.GetValues( typeof( TraceEventType ) ).Length, allValues.Count(), $"{typeof( TraceEventType )} count matches the extension result" );
            Assert.IsTrue( allValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the enum values of {typeof( TraceEventType )}" );

            var inclusionMask = TraceEventType.Critical | TraceEventType.Error | TraceEventType.Information | TraceEventType.Warning;
            var exclusionMask = TraceEventType.Verbose;
            expectedCount = 4;
            filteredValues = TraceEventType.Critical.EnumValues().Filter( inclusionMask, exclusionMask );
            Assert.IsTrue( filteredValues.Any(), $"{typeof( TraceEventType )} Filtered Enum Values has values" );
            Assert.AreEqual( expectedCount, filteredValues.Count(), $"{typeof( TraceEventType )} filtered count matches the expected count" );
            Assert.IsTrue( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the filtered" );
            Assert.IsFalse( filteredValues.Contains( TraceEventType.Verbose ), $"{TraceEventType.Verbose} is Excluded in the filtered" );
        }

        private enum TraceEventTypeWithDescription
        {
            [System.ComponentModel.Description( "Critical Trace Event Type" )] Critical,
            [System.ComponentModel.Description( "Error Trace Event Type" )] Error,
            [System.ComponentModel.Description( "Information Trace Event Type" )] Information,
            [System.ComponentModel.Description( "Verbose Trace Event Type" )] Verbose
        }

        /// <summary> (Unit Test Method) tests value descriptions. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ValueDescriptionsTest()
        {
            var pairs = typeof( TraceEventTypeWithDescription ).ValueDescriptionPairs();
            var expectedValue = TraceEventTypeWithDescription.Critical;
            Assert.IsTrue( pairs.Any(), $"{typeof( TraceEventTypeWithDescription )}.{nameof( isr.Core.EnumExtensions.EnumExtensionsMethods.ValueDescriptionPairs )} has values" );
            Assert.IsTrue( pairs.ContainsKey( ( Enum ) expectedValue ), $"{typeof( TraceEventTypeWithDescription )} contains {expectedValue}" );

            var pair = pairs.SelectPair( expectedValue, expectedValue.Description() );
            Assert.AreEqual( pair.Key, expectedValue, $"{typeof( TraceEventTypeWithDescription )} found {expectedValue}" );

            var defaultValue = TraceEventTypeWithDescription.Error;
            string description = "incorrect Description";
            pair = pairs.SelectPair( defaultValue, "incorrect Description" );
            Assert.AreEqual( defaultValue, pair.Key, $"{typeof( TraceEventTypeWithDescription )} found {defaultValue} due to {description}" );

            pairs = typeof( TraceEventTypeWithDescription ).ValueNamePairs();
            expectedValue = TraceEventTypeWithDescription.Critical;
            Assert.IsTrue( pairs.Any(), $"{typeof( TraceEventTypeWithDescription )}.{nameof( isr.Core.EnumExtensions.EnumExtensionsMethods.ValueDescriptionPairs )} has value names" );
            Assert.IsTrue( pairs.ContainsKey( ( Enum ) expectedValue ), $"{typeof( TraceEventTypeWithDescription )} value names contains {expectedValue}" );

            string expectedDescription = TraceEventTypeWithDescription.Critical.Description();
            var descriptions = TraceEventTypeWithDescription.Critical.Descriptions();
            Assert.IsTrue( descriptions.Any(), $"{typeof( TraceEventTypeWithDescription )}.{nameof( descriptions )} has description" );
            Assert.IsTrue( descriptions.Contains( expectedDescription ), $"{typeof( TraceEventTypeWithDescription )} value descriptions contains {expectedDescription}" );

            var names = pairs.ToValues();
            Assert.IsTrue( names.Any(), $"{typeof( TraceEventTypeWithDescription )}.{nameof( names )} has name" );

            string expectedname = TraceEventTypeWithDescription.Critical.ToString();
            Assert.IsTrue( names.Contains( expectedname ), $"{typeof( TraceEventTypeWithDescription )} value names contains {expectedname}" );
        }

        [TestMethod()]
        public void TraceEventTypeValueDescriptionsTest()
        {
            var pairs = typeof( TraceEventType ).ValueDescriptionPairs();
            var expectedValue = TraceEventType.Critical;
            Assert.IsTrue( pairs.Any(), $"{typeof( TraceEventType )}.{nameof( isr.Core.EnumExtensions.EnumExtensionsMethods.ValueDescriptionPairs )} has values" );
            Assert.IsTrue( pairs.ContainsKey( ( Enum ) expectedValue ), $"{typeof( TraceEventType )} contains {expectedValue}" );

            var pair = pairs.SelectPair( expectedValue, expectedValue.Description() );
            Assert.AreEqual( pair.Key, expectedValue, $"{typeof( TraceEventType )} found {expectedValue}" );

            var defaultValue = TraceEventType.Error;
            string description = "incorrect Description";
            pair = pairs.SelectPair( defaultValue, "incorrect Description" );
            Assert.AreEqual( defaultValue, pair.Key, $"{typeof( TraceEventType )} found {defaultValue} due to {description}" );

            pairs = typeof( TraceEventType ).ValueNamePairs();
            expectedValue = TraceEventType.Critical;
            Assert.IsTrue( pairs.Any(), $"{typeof( System.Diagnostics.TraceEventType )}.{nameof( isr.Core.EnumExtensions.EnumExtensionsMethods.ValueDescriptionPairs )} has value names" );
            Assert.IsTrue( pairs.ContainsKey( ( Enum ) expectedValue ), $"{typeof( TraceEventType )} value names contains {expectedValue}" );

            string expectedDescription = TraceEventType.Critical.Description();
            var descriptions = TraceEventType.Critical.Descriptions();
            Assert.IsTrue( descriptions.Any(), $"{typeof( TraceEventType )}.{nameof( descriptions )} has description" );
            Assert.IsTrue( descriptions.Contains( expectedDescription ), $"{typeof( TraceEventType )} value descriptions contains {expectedDescription}" );

            var names = pairs.ToValues();
            Assert.IsTrue( names.Any(), $"{typeof( TraceEventType )}.{nameof( names )} has name" );

            string expectedname = TraceEventType.Critical.ToString();
            Assert.IsTrue( names.Contains( expectedname ), $"{typeof( TraceEventType )} value names contains {expectedname}" );
        }


        #endregion

        #region " ENUM EXTENSION TESTS: NAME and DESCRIPTION "

        [Flags]
        private enum ArmEvents : long
        {

            /// <summary> An enum constant representing the none option. </summary>
            [System.ComponentModel.Description( "None" )]
            None = 0,

            /// <summary> An enum constant representing the source option. </summary>
            [System.ComponentModel.Description( "Source" )]
            Source = 1 << 1,

            [System.ComponentModel.Description( "Timer" )]
            Timer = 1 << 2

        }


        [Flags]
        private enum TriggerEvents : long
        {

            /// <summary> An enum constant representing the none option. </summary>
            [System.ComponentModel.Description( "None" )]
            None = 1 << 0,

            /// <summary> An enum constant representing the source option. </summary>
            [System.ComponentModel.Description( "Source" )]
            Source = 1 << 1,

            [System.ComponentModel.Description( "Timer" )]
            Timer = 1 << 2,

            [System.ComponentModel.Description( "Blender" )]
            Blender = 1L << 33

        }


        /// <summary>   (Unit Test Method) tests enum names. </summary>
        /// <remarks>   David, 2020-10-28. </remarks>
        [TestMethod()]
        public void EnumNamesTest()
        {
            TraceEventType traceEvent = TraceEventType.Verbose;
            string expectedValue = "Verbose";
            string actualValue = traceEvent.ToString();
            Assert.AreEqual( expectedValue, actualValue, $"ToString() of {nameof( System.Diagnostics.TraceEventType.Verbose )}.{nameof( System.Diagnostics.TraceEventType.Verbose )} should match" );

            actualValue = traceEvent.Names();
            Assert.AreEqual( expectedValue, actualValue, $"{nameof( isr.Core.EnumExtensions.EnumExtensionsMethods.Names )} of {nameof( System.Diagnostics.TraceEventType.Verbose )}.{nameof( System.Diagnostics.TraceEventType.Verbose )} should match" );

            TriggerEvents triggerEvent = TriggerEvents.Blender;

            expectedValue = "Blender";
            actualValue = triggerEvent.Names();
            Assert.AreEqual( expectedValue, actualValue, $"{nameof( isr.Core.EnumExtensions.EnumExtensionsMethods.Names )} of {nameof( TriggerEvents )}.{nameof( TriggerEvents.Blender )} should match" );

            ArmEvents armEvent = ArmEvents.Source;
            expectedValue = "Source";
            actualValue = armEvent.Names();
            Assert.AreEqual( expectedValue, actualValue, $"{nameof( isr.Core.EnumExtensions.EnumExtensionsMethods.Names )} of {nameof( ArmEvents )}.{nameof( ArmEvents.Source )} should match" );

            armEvent = ArmEvents.Source | ArmEvents.Timer;
            expectedValue = "Source, Timer";
            actualValue = armEvent.Names();
            Assert.AreEqual( expectedValue, actualValue,
                $"{nameof( isr.Core.EnumExtensions.EnumExtensionsMethods.Names )} of {nameof( ArmEvents )}.({nameof( ArmEvents.Source )} or {nameof( ArmEvents.Timer )}) should match" );
        }

        /// <summary> (Unit Test Method) tests enum description. </summary>
        /// <remarks> David, 2020-10-14. </remarks>
        [TestMethod()]
        public void EnumNameTest()
        {
            TraceEventType traceEvent = TraceEventType.Verbose;
            string expectedValue = "Verbose";
            string actualValue = traceEvent.ToString();
            Assert.AreEqual( expectedValue, actualValue, $"ToString() of {nameof( System.Diagnostics.TraceEventType.Verbose )}.{nameof( System.Diagnostics.TraceEventType.Verbose )} should match" );

            TriggerEvents triggerEvent = TriggerEvents.Source;

            expectedValue = "Source";
            // this no longer gives the name of Blender as the Enum value is correctly set.
            actualValue = triggerEvent.ToString();
            Assert.AreEqual( expectedValue, actualValue, $"ToString() value of  {nameof( TriggerEvents )}.{nameof( TriggerEvents.Source )} should match" );

            // this gives the name of Source
            actualValue = Enum.GetName( typeof( TriggerEvents ), triggerEvent );
            Assert.AreEqual( expectedValue, actualValue, $"GetName() value of  {nameof( TriggerEvents )}.{nameof( TriggerEvents.Source )} should match" );

        }


        /// <summary> (Unit Test Method) tests enum description. </summary>
        /// <remarks> David, 2020-10-14. </remarks>
        [TestMethod()]
        public void EnumDescriptionTest()
        {
            TraceEventType traceEvent = TraceEventType.Verbose;
            string expectedValue = "Verbose";
            string actualValue = traceEvent.Description();
            Assert.AreEqual( expectedValue, actualValue, $"Description of {nameof( System.Diagnostics.TraceEventType.Verbose )}.{nameof( System.Diagnostics.TraceEventType.Verbose )} should match" );

            TriggerEvents triggerEvent = TriggerEvents.Source;

            expectedValue = "Source";
            actualValue = triggerEvent.Description();
            Assert.AreEqual( expectedValue, actualValue, $"Description of {nameof( TriggerEvents )}.{nameof( TriggerEvents.Source )} should match" );

        }

        #endregion

    }

}

