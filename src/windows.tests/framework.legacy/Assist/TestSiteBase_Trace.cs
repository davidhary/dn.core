using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Legacy.MSTest
{

    /// <summary> Test site base class. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para>
    /// </remarks>
    public abstract partial class TestSiteBase : IDisposable
    {

        #region " TRACE "

        /// <summary> Initializes the trace listener. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public void InitializeTraceListener()
        {
            this.ReplaceTraceListener();
            Console.Out.WriteLine( My.MyProject.Application.Log.DefaultFileLogWriter.FullLogFileName );
        }

        /// <summary> Replace trace listener. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void ReplaceTraceListener()
        {
            My.MyProject.Application.Log.TraceSource.Listeners.Remove( CustomFileLogTraceListener.DefaultFileLogWriterName );
            _ = My.MyProject.Application.Log.TraceSource.Listeners.Add( CustomFileLogTraceListener.CreateListener( false ) );
            My.MyProject.Application.Log.TraceSource.Switch.Level = SourceLevels.Verbose;
        }

        /// <summary> Trace message. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        public void TraceMessage( string format, params object[] args )
        {
            this.TraceMessage( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        /// <summary> Trace message. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="message"> The message. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void TraceMessage( string message )
        {
            My.MyProject.Application.Log.WriteEntry( message );
            // System.Diagnostics.Debug.WriteLine(message)
            Console.Out.WriteLine( message );
        }

        /// <summary> Verbose message. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        public void VerboseMessage( string format, params object[] args )
        {
            if ( AppSettings.Instance.TestSite.Verbose )
            {
                this.TraceMessage( format, args );
            }
        }

        /// <summary> Trace elapsed <see cref="DateTimeOffset"/>. </summary>
        /// <remarks> David, 2020-07-10. </remarks>
        /// <param name="startTime"> The start time. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> Current <see cref="DateTimeOffset"/>. </returns>
        public DateTimeOffset TraceElapsedTime( DateTimeOffset startTime, string activity )
        {
            this.TraceMessage( $"{activity} {DateTimeOffset.Now.Subtract( startTime ).TotalMilliseconds}ms" );
            return DateTimeOffset.Now;
        }

        #endregion

        #region " TRACE MESSAGES QUEUE "

        /// <summary> The trace messages queue listener. </summary>
        private TraceMessagesQueueListener _TraceMessagesQueueListener;

        /// <summary> Gets the trace message queue listener. </summary>
        /// <value> The trace message queue listener. </value>
        public TraceMessagesQueueListener TraceMessagesQueueListener
        {
            get {
                if ( this._TraceMessagesQueueListener is null )
                {
                    this._TraceMessagesQueueListener = new TraceMessagesQueueListener();
                    this._TraceMessagesQueueListener.ApplyTraceLevel( TraceEventType.Warning );
                }

                return this._TraceMessagesQueueListener;
            }
        }

        /// <summary> Assert message. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="traceMessage"> Message describing the trace. </param>
        public void AssertMessage( TraceMessage traceMessage )
        {
            if ( traceMessage is null )
            {
            }
            else if ( traceMessage.EventType == TraceEventType.Warning )
            {
                this.TraceMessage( $"Warning published: {traceMessage}" );
            }
            else if ( traceMessage.EventType == TraceEventType.Error )
            {
                Assert.Fail( $"Error published: {traceMessage}" );
            }
        }

        /// <summary> Assert message queue. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="queue"> The queue listener. </param>
        public void AssertMessageQueue( TraceMessagesQueue queue )
        {
            if ( queue is null )
            {
                throw new ArgumentNullException( nameof( queue ) );
            }

            while ( !queue.IsEmpty() )
            {
                this.AssertMessage( queue.TryDequeue() );
            }
        }

        #endregion

        #region " TRACE MESSAGES QUEUE COLLECTION "

        /// <summary> The trace messages queues. </summary>
        private TraceMessageQueueCollection _TraceMessagesQueues;

        /// <summary> Gets the collection of trace messages queues. </summary>
        /// <value> The trace messages queues. </value>
        public TraceMessageQueueCollection TraceMessagesQueues
        {
            get {
                if ( this._TraceMessagesQueues is null )
                {
                    this._TraceMessagesQueues = new TraceMessageQueueCollection();
                }

                return this._TraceMessagesQueues;
            }
        }

        /// <summary> Adds a trace messages queue. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="queue"> The queue listener. </param>
        public void AddTraceMessagesQueue( TraceMessagesQueue queue )
        {
            this.TraceMessagesQueues.Add( queue );
        }

        /// <summary> Assert message queue. THis clears the queues. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        public void AssertMessageQueue()
        {
            this.TraceMessagesQueues.AssertMessageQueue( this );
        }

        /// <summary> Clears the message queue. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <returns> A String. </returns>
        public string ClearMessageQueue()
        {
            return this.TraceMessagesQueues.ClearMessageQueue();
        }

        #endregion

    }

    /// <summary> Collection of trace message queues. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-10 </para>
    /// </remarks>
    public partial class TraceMessageQueueCollection : System.Collections.ObjectModel.Collection<TraceMessagesQueue>
    {

        /// <summary> Query if this object has queued messages. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <returns> <c>true</c> if queued messages; otherwise <c>false</c> </returns>
        public bool HasQueuedMessages()
        {
            bool result = false;
            foreach ( TraceMessagesQueue traceMessageQueue in this )
            {
                result = result && traceMessageQueue.Any;
            }

            return result;
        }

        /// <summary> Count queued messages. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <returns> The total number of queued messages. </returns>
        public int CountQueuedMessages()
        {
            int result = 0;
            foreach ( TraceMessagesQueue traceMessageQueue in this )
            {
                result += traceMessageQueue.Count;
            }

            return result;
        }

        /// <summary> Assert message queue. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="testSite"> The test site. </param>
        public void AssertMessageQueue( TestSiteBase testSite )
        {
            foreach ( TraceMessagesQueue traceMessageQueue in this )
            {
                testSite?.AssertMessageQueue( traceMessageQueue );
            }
        }

        /// <summary> Appends a line. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="builder"> The builder. </param>
        /// <param name="value">   The value. </param>
        private static void AppendLine( System.Text.StringBuilder builder, string value )
        {
            if ( !string.IsNullOrWhiteSpace( value ) )
            {
                _ = builder.AppendLine( value );
            }
        }

        /// <summary> Clears the message queue. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <returns> A String. </returns>
        public string ClearMessageQueue()
        {
            var builder = new System.Text.StringBuilder();
            foreach ( TraceMessagesQueue traceMessageQueue in this )
            {
                AppendLine( builder, traceMessageQueue.DequeueContent() );
            }

            return builder.ToString();
        }
    }
}
