using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Windows.Trials
{

    /// <summary> A machine information tests. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass()]
    public class MachineInfoTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion


        /// <summary> A test for Machine ID Using insecure MD5 algorithm,. </summary>
        /// <remarks> Causes application domain exception with test agent. </remarks>
        [TestMethod()]
        public void MachineUniqueIdMD5Test()
        {
            string expected = "6f32a7c3d4b883fb67303c53380b7859";
            string computerName = System.Environment.MachineName;
            switch ( true )
            {
                case object _ when string.Equals( computerName, "LimeDev", StringComparison.OrdinalIgnoreCase ):
                    {
                        expected = "f77145622be6795484b53c570731a5fc";
                        break;
                    }

                case object _ when string.Equals( computerName, "Fig10Dev", StringComparison.OrdinalIgnoreCase ):
                    {
                        expected = "7ae637a47a35f672ed013462c7b92649";
                        break;
                    }

                case object _ when string.Equals( computerName, "LimeDevB", StringComparison.OrdinalIgnoreCase ):
                    {
                        expected = "6f32a7c3d4b883fb67303c53380b7859";
                        break;
                    }
            }
            // on FIG10Dev
            string actual = MachineInfo.BuildMachineUniqueIdMD5();
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) machine unique identifier SHA 1. </summary>
        /// <remarks> Causes application domain exception with test agent. </remarks>
        [TestMethod()]
        public void MachineUniqueIdSha1()
        {
            string expected = "be9be0ce1a031b079b0330be7e47f0dbcba2a1cd";
            string computerName = System.Environment.MachineName;
            switch ( true )
            {
                case object _ when string.Equals( computerName, "LimeDev", StringComparison.OrdinalIgnoreCase ):
                    {
                        expected = "e8744184d701a53a060f1502d40e2a8e09a61a31";
                        break;
                    }

                case object _ when string.Equals( computerName, "Fig10Dev", StringComparison.OrdinalIgnoreCase ):
                    {
                        expected = "85f9c966b0a3f128c41186a0160aa0c9cce68e65";
                        break;
                    }

                case object _ when string.Equals( computerName, "LimeDevB", StringComparison.OrdinalIgnoreCase ):
                    {
                        expected = "be9be0ce1a031b079b0330be7e47f0dbcba2a1cd";
                        break;
                    }

            }

            string actual = MachineInfo.BuildMachineUniqueIdSha1();
            Assert.AreEqual( expected, actual );
        }
    }
}
