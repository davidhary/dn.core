using System;
using System.Reflection;

[assembly: AssemblyDescription( "Test Trials for Core Framework Libraries" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
