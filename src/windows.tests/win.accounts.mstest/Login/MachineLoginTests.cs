using System.Collections;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.WinAccounts.MSTest
{

    /// <summary>
    /// This is a test class for MachineLoginTest and is intended to contain all MachineLoginTest
    /// Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass(), TestCategory( "Login" )]
    public class MachineLoginTests
    {


        /// <summary>   The test enabled. Login tests are slow. Therefore this needs to be enabled manually as we cannot set a negative test category. </summary>
        private readonly bool _TestEnabled = false;

        /// <summary> A test for Validate. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ValidateRole()
        {

            if ( !this._TestEnabled ) return;

            string userName = isr.Core.WinForms.Dialogs.TextInputBox.EnterInput( "Enter Admin user name or empty to cancel this test", "User Name:" );
            if ( string.IsNullOrEmpty( userName ) )
            {
                return;
            }

            System.Security.SecureString password = isr.Core.WinForms.Dialogs.TextInputBox.EnterPassword( "Enter Admin password or empty to cancel this test",
                                                                                                          "Password:" );
            if ( password.Length == 0 )
            {
                return;
            }

            System.Net.NetworkCredential networkCredential = new( userName, password );
            using var target = new MachineLogin();
            var roles = new List<string>() { "Administrators" };
            var allowedUserRoles = new ArrayList( roles );
            bool expected = true;
            bool actual = target.Authenticate( networkCredential, allowedUserRoles );
            Assert.AreEqual( expected, actual, target.ValidationMessage );
        }

        /// <summary> A test for Validate. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ValidateUser()
        {

            if ( !this._TestEnabled ) return;

            string userName = isr.Core.WinForms.Dialogs.TextInputBox.EnterInput( "Enter a User name or empty to cancel this test", "User name:" );
            if ( string.IsNullOrEmpty( userName ) )
            {
                return;
            }

            System.Security.SecureString password = isr.Core.WinForms.Dialogs.TextInputBox.EnterPassword( "Enter password or empty to cancel this test", "Password:" );
            if ( password.Length == 0 )
            {
                return;
            }

            System.Net.NetworkCredential networkCredential = new( userName, password );
            using var target = new MachineLogin();
            bool expected = true;
            bool actual;
            actual = target.Authenticate( networkCredential );
            Assert.AreEqual( expected, actual, target.ValidationMessage );
        }
    }
}
