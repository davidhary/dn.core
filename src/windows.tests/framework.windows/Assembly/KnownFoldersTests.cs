using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Windows.MSTest
{

    /// <summary>
    /// This is a test class for KnownFoldersTest and is intended
    /// to contain all KnownFoldersTest Unit Tests
    /// </summary>
    [TestClass()]
    public class KnownFoldersTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }
        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }


        #endregion

        /// <summary> A test for GetPath. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="knownFolder"> A known folder enum value. </param>
        /// <param name="expected">    The expected value. </param>
        public static void TestGetPath( KnownFolder knownFolder, string expected )
        {
            string actual;
            actual = KnownFolders.GetPath( knownFolder );
            Assert.AreEqual( expected, actual, true, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary> A test for Get Path on multiple <see cref="KnownFolder"/> folders. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void GetPathTest()
        {
            TestGetPath( KnownFolder.Documents, @$"C:\Users\{Environment.UserName}\Documents" );
            TestGetPath( KnownFolder.UserProfiles, @"C:\Users" );
            TestGetPath( KnownFolder.ProgramData, @"C:\ProgramData" );
        }

        /// <summary> A test for Get Default Path. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="knownFolder"> A known folder enum value. </param>
        /// <param name="expected">    The expected value. </param>
        public static void TestGetDefaultPath( KnownFolder knownFolder, string expected )
        {
            string actual;
            actual = KnownFolders.GetDefaultPath( knownFolder );
            Assert.AreEqual( expected, actual, true, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary> A test for GetDefaultPath. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void GetDefaultPathTest()
        {
            TestGetDefaultPath( KnownFolder.Documents, @$"C:\Users\{Environment.UserName}\Documents" );
            TestGetDefaultPath( KnownFolder.UserProfiles, @"C:\Users" );
            TestGetDefaultPath( KnownFolder.ProgramData, @"C:\ProgramData" );
        }
    }
}
