using System;

using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Windows.MSTest
{

    /// <summary> This is a test class for event handling. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class ProgramInfoTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " PROGRAM INFO "

        /// <summary> Handles the refresh requested. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void HandleRefreshRequested( object sender, EventArgs e )
        {
            if ( sender is not ProgramInfo programInfoSender )
            {
                return;
            }

            programInfoSender.AppendLine( $"Line #{programInfoSender.Lines.Count}", new System.Drawing.Font( "Consolas", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte( 0 ) ) );
        }

        /// <summary> (Unit Test Method) tests event handler context. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void EventHandlerContextTest()
        {
            var sender = new ProgramInfo();
            sender.RefreshRequested += this.HandleRefreshRequested;
            int expectedLineCount = sender.Lines.Count + 1;
            sender.NotifyRefreshRequested();
            Assert.AreEqual( expectedLineCount, sender.Lines.Count, "line count should equal after handling the refresh requested event" );
        }

        #endregion

    }
}
