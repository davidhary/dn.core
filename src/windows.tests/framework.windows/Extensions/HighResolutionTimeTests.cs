using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Windows.MSTest
{

    /// <summary>   (Unit Test Class) a high resolution time tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-14 </para>
    /// </remarks>
    [TestClass()]
    public class HighResolutionTimeTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " PRECISE DATE TIME "

        /// <summary>   (Unit Test Method) tests high resolution date time resolution. </summary>
        /// <remarks>   David, 2020-12-07. </remarks>
        [TestMethod()]
        public void HighResolutionDateTimeResolutionTest()
        {
            string output = "";
            double sum = 0;
            double resolution;
            long ticks;
            long newTicks;
            int count = 20;
            for ( int ctr = 0; ctr <= count; ctr++ )
            {
                output += String.Format( $"{Win32.HighResolutionDateTime.UtcNow.ToFileTime()}\n" );

                // Introduce a delay loop.
                ticks = Win32.HighResolutionDateTime.UtcNow.ToFileTime();
                do
                { newTicks = Win32.HighResolutionDateTime.UtcNow.ToFileTime(); }
                while ( ticks == newTicks );
                resolution = 0.0001 * (newTicks - ticks);
                sum += resolution;
                output += String.Format( $"Millisecond resolution = {resolution}\n" );

                if ( ctr == 10 )
                {
                    output += "Thread.Sleep called...\n";
                    System.Threading.Thread.Sleep( 5 );
                }

            }
            resolution = sum / count;
            output += String.Format( $"Average resolution = {resolution}\n" );
            Trace.TraceInformation( output );
            double expectedResolution = 2 * isr.Core.TimeSpanExtensions.TimeSpanExtensionMethods.MillisecondsPerTick;
            double resolutionEpsilon = 0.5 * expectedResolution;
            Assert.AreEqual( expectedResolution, resolution, resolutionEpsilon, $"Date time resolution {resolution} ms should match within {resolutionEpsilon} ms" );
        }

        #endregion

    }
}
