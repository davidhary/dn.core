using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.Windows.MSTest
{

    /// <summary>   tests using tracing to assert hidden warnings and errors. </summary>
    /// <remarks>   David, 2021-02-12. </remarks>
    [TestClass()]
    public class TracingTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " UNPUBLISHED TRACING TESTS  "

        /// <summary>   (Unit Test Method) trace listener should be empty. </summary>
        /// <remarks>   David, 2021-02-12. </remarks>
        [TestMethod()]
        public void TraceListenerShouldBeEmpty()
        {
            Assert.IsTrue( TraceListener.Queue.IsEmpty );
        }

        [TestMethod()]
        public void TraceListenerShouldNotBeEmpty()
        {
            System.Diagnostics.Trace.TraceWarning( $"tracing a message from {System.Reflection.MethodBase.GetCurrentMethod().Name}" );
            Assert.IsFalse( TraceListener.Queue.IsEmpty );
            TraceListener.ClearQueue();
        }

        #endregion

    }
}
