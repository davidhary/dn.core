using System;
using System.Diagnostics;

namespace isr.Core.Tracing
{
    /// <summary>   A trace event message. </summary>
    /// <remarks>   David, 2021-03-05. </remarks>
    public struct TraceEventMessage
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-03-05. </remarks>
        /// <param name="eventType">    Type of the event. </param>
        /// <param name="message">      The message. </param>
        public TraceEventMessage( TraceEventType eventType, String message )
        {
            this.EventType = eventType;
            this.Message = message;
        }
        /// <summary>   Gets or sets the type of the event. </summary>
        /// <value> The type of the event. </value>
        public TraceEventType EventType { get; set; }

        /// <summary>   Gets or sets the message. </summary>
        /// <value> The message. </value>
        public string Message { get; set; }
    }

}
