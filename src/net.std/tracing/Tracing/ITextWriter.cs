namespace isr.Core.Tracing
{
    /// <summary>   Interface for text writer. </summary>
    /// <remarks>   David, 2021-02-09. </remarks>
    public interface ITextWriter
    {

        /// <summary>   Gets or sets the name of the text writer. </summary>
        /// <value> The name. </value>
        string Name { get; set; }

        /// <summary>   Writes. </summary>
        /// <param name="message">  The message to write. </param>
        void Write( string message );


        /// <summary>   Writes a line. </summary>
        /// <param name="message">  The message to write. </param>
        void WriteLine( string message );
    }

    /// <summary>   A text writers concurrent dictionary. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    public class TextWritersConcurrentDictionary : System.Collections.Concurrent.ConcurrentDictionary<string, ITextWriter>, ITextWriter
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        public TextWritersConcurrentDictionary() : this( System.Guid.NewGuid().ToString() )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="name"> The name. </param>
        public TextWritersConcurrentDictionary( string name ) : base()
        {
            this.Name = name;
        }

        /// <summary>   Attempts to add. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="textWriter">   The text writer. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool TryAdd( ITextWriter textWriter )
        {
            return this.TryAdd( textWriter.Name, textWriter );
        }

        /// <summary>   Attempts to remove. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="textWriter">   The text writer. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool TryRemove( ITextWriter textWriter )
        {
            return this.TryRemove( textWriter.Name, out _ );
        }

        /// <summary>   Gets the name of the text writer. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary>   Writes. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="message">  The message to write. </param>
        public void Write( string message )
        {
            if ( this.Count > 0 )
                foreach ( ITextWriter textWriter in this.Values )
                {
                    textWriter.Write( message );
                }
        }

        /// <summary>   Writes a line. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="message">  The message to write. </param>
        public void WriteLine( string message )
        {
            if ( this.Count > 0 )
                foreach ( ITextWriter textWriter in this.Values )
                {
                    textWriter.WriteLine( message );
                }
        }

    }

}
