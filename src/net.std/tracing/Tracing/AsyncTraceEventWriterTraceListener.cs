using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace isr.Core.Tracing
{
    /// <summary>   An asynchronous text writer trace listener. This class cannot be inherited. </summary>
    /// <remarks>   David, 2021-02-09. </remarks>
    public sealed class AsyncTraceEventWriterTraceListener : TraceListener, IBufferInspector
    {

        #region " CONSTRUCTION AND CLEANUP "

        private readonly bool _BlockWhenFull;
        private readonly BlockingCollection<TraceEventMessage> _Queue;
        private readonly Task _Worker;
        private readonly IBufferInspectorMonitor _Monitor;


        /// <summary>
        /// Constructs a Text Writer Trace Message Listener to be invoked asynchronously, on a background
        /// worker thread wrapping a <see cref="ITraceEventWriter"/> which consumes the messages.
        /// </summary>
        /// <remarks>   David, 2021-02-09. </remarks>
        /// <exception cref="ArgumentOutOfRangeException">  Thrown when one or more arguments are outside
        ///                                                 the required range. </exception>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <param name="name">                 The <see cref="TraceListener"/> name. </param>
        /// <param name="sourceLevel">          (Optional) Source level. </param>
        /// <param name="ignoreHeader">         (Optional) True if ignore header, false if not. </param>
        /// <param name="bufferCapacity">       (Optional) The size of the concurrent queue used to feed
        ///                                     the background worker thread. If the thread is unable to
        ///                                     process messages quickly enough and the queue is filled,
        ///                                     depending on
        ///                                     <paramref name="blockWhenFull"/> the queue will block or
        ///                                     subsequent messages will be dropped until room is made in the
        ///                                     queue. (10,000) </param>
        /// <param name="blockWhenFull">        (Optional) When True the queue will block or subsequent
        ///                                     messages will be dropped until room is made in the queue
        ///                                     (false). </param>
        /// <param name="monitor">              (Optional) Accepts a reference to a
        ///                                     <paramref name="monitor"/> that will be supplied the internal
        ///                                     state interface for health monitoring purposes (null). </param>
        public AsyncTraceEventWriterTraceListener( string name, System.Diagnostics.SourceLevels sourceLevel = SourceLevels.Information,
            bool ignoreHeader = true,
            int bufferCapacity = 10000, bool blockWhenFull = false, IBufferInspectorMonitor monitor = null ) : base( name )
        {
            if ( bufferCapacity <= 0 ) throw new ArgumentOutOfRangeException( nameof( bufferCapacity ) );
            this._WrappedTraceEventWriters = new TraceEventWritersConcurrentDictionary();
            this._BlockWhenFull = blockWhenFull;
            this._Queue = new BlockingCollection<TraceEventMessage>( bufferCapacity );
            this._Worker = Task.Factory.StartNew( this.Pump, CancellationToken.None,
                                                  TaskCreationOptions.LongRunning | TaskCreationOptions.DenyChildAttach, TaskScheduler.Default );
            this._Monitor = monitor;
            monitor?.StartMonitoring( this );
            this.InitialSourceLevel = sourceLevel;
            this.SourceLevel = sourceLevel;
            this.Filter = new System.Diagnostics.EventTypeFilter( sourceLevel );
            this.IgnoreHeader = ignoreHeader;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the
        /// <see cref="T:System.Diagnostics.TraceListener" /> and optionally releases the managed
        /// resources.
        /// </summary>
        /// <remarks>   David, 2021-02-09. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        protected override void Dispose( bool disposing )
        {
            // Prevents any more messages from being added
            this._Queue.CompleteAdding();

            // Allow queued messages to be flushed; this will throw any internal 
            // exception that might have occurred.
            this._Worker.Wait();

            // this needs to be done at the wrapped listener, because
            // the listener is not created by this class. 
            // this._WrappedTraceListener?.Dispose();
            this._Monitor?.StopMonitoring( this );
        }

        #endregion

        #region " TEXT WRITER IMPLEMENTATION "

        /// <summary>   The wrapped text writers. </summary>
        private readonly TraceEventWritersConcurrentDictionary _WrappedTraceEventWriters;

        /// <summary>   Adds a text writer. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="traceEventWriter">   The text writer. </param>
        public void AddTraceEventWriter( ITraceEventWriter traceEventWriter )
        {
            _ = this._WrappedTraceEventWriters.TryAdd( traceEventWriter );
        }

        /// <summary>   Removes the text writer described by TraceEventWriter. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="traceEventWriter">   The text writer. </param>
        public void RemoveTraceEventWriter( ITraceEventWriter traceEventWriter )
        {
            _ = this._WrappedTraceEventWriters.TryRemove( traceEventWriter );
        }

        /// <summary>
        /// Emits an error message to the listener you create when you implement the
        /// <see cref="T:System.Diagnostics.TraceListener" /> class.
        /// </summary>
        /// <remarks>   David, 2021-02-11. </remarks>
        /// <param name="message">  A message to emit. </param>
        public override void Fail( string message )
        {
            this.EmitFailMessage( message );
        }
        /// <summary>
        /// Emits an error message and a detailed error message to the listener you create when you
        /// implement the <see cref="T:System.Diagnostics.TraceListener" /> class.
        /// </summary>
        /// <remarks>   David, 2021-02-11. </remarks>
        /// <param name="message">          A message to emit. </param>
        /// <param name="detailMessage">    A detailed message to emit. </param>
        public override void Fail( string message, string detailMessage )
        {
            this.EmitFailMessage( $"{message}, detailMessage: {detailMessage}" );
        }

        /// <summary>   Emit fail message. </summary>
        /// <remarks>   David, 2021-02-11. </remarks>
        /// <param name="message">  The message. </param>
        private void EmitFailMessage( string message )
        {
            StackTrace stackTrace = new( 1, true );
            this._WrappedTraceEventWriters.TraceEvent( TraceEventType.Critical, $"{DateTime.UtcNow:HH:mm:ss.fff zzz}, [FAT], ({typeof( AsyncTraceEventWriterTraceListener )}), {message} in {stackTrace}" );
        }

        /// <summary>   Pumps all messages onto the wrapped trace listener. </summary>
        /// <remarks>
        /// David, 2021-02-09. This method removes items from the <see cref="_Queue"/>. The
        /// <see cref="BlockingCollection{T}.GetConsumingEnumerable()"/>
        /// provides items (if any exist) until <see cref="BlockingCollection{T}.IsCompleted()"/> returns
        /// true;
        /// and if <see cref="BlockingCollection{T}.IsCompleted()"/> is false the loop blocks until an
        /// item becomes available or until the CancellationToken is canceled. Any internal exception is
        /// thrown and the task wait on dispose will detect those.
        /// </remarks>
        private void Pump()
        {
            foreach ( var next in this._Queue.GetConsumingEnumerable() )
            {
                if ( !(this.IgnoreHeader && IsHeaderRecord( next.Message )) )
                    this._WrappedTraceEventWriters.TraceEvent( next.EventType, next.Message );
            }
        }

        #endregion

        #region " IGNORE HEADER "

        /// <summary>   Gets or sets a value indicating whether to ignore the source name and level header. </summary>
        /// <value> True if ignore header, false if not. </value>
        public bool IgnoreHeader { get; set; }

        /// <summary>   Query if 'message' is header record. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        /// <param name="message">  The message. </param>
        /// <returns>   True if header record, false if not. </returns>
        private static bool IsHeaderRecord( string message )
        {
            return message.EndsWith( " : " );
        }

        #endregion

        #region " TRACE LISTENER IMPLEMENTATION "

        int IBufferInspector.BufferSize => this._Queue.BoundedCapacity;

        /// <summary>   Current moment-in-time Count of items currently awaiting ingestion. </summary>
        /// <value> The count. </value>
        int IBufferInspector.Count => this._Queue.Count;

        private long _DroppedMessages;

        /// <summary>
        /// Accumulated number of messages dropped due to breaches of <see cref="IBufferInspector.BufferSize"/> limit.
        /// </summary>
        /// <value> The number of dropped messages. </value>
        long IBufferInspector.DroppedMessagesCount => this._DroppedMessages;


        /// <summary>   Enqueues the message. </summary>
        /// <remarks>   David, 2021-02-09. </remarks>
        /// <param name="eventType">    Type of the event. </param>
        /// <param name="message">      The message. </param>
        private void Enqueue( TraceEventType eventType, String message )
        {
            if ( this._Queue.IsAddingCompleted )
                return;

            try
            {
                if ( this._BlockWhenFull )
                {
                    this._Queue.Add( new TraceEventMessage( eventType, message ) );
                }
                else
                {
                    if ( !this._Queue.TryAdd( new TraceEventMessage( eventType, message ) ) )
                    {
                        _ = Interlocked.Increment( ref this._DroppedMessages );
                        this.Fail( $"{typeof( AsyncTraceEventWriterTraceListener )} unable to enqueue, capacity {this._Queue.BoundedCapacity }" );
                    }
                }
            }
            catch ( InvalidOperationException )
            {
                // Thrown in the event of a race condition when we try to add another message after
                // CompleteAdding has been called
            }
        }

        /// <summary>
        /// Writes trace information, a message, and event information to the listener specific output.
        /// </summary>
        /// <remarks>   David, 2021-03-05. </remarks>
        /// <param name="eventCache">   A <see cref="T:System.Diagnostics.TraceEventCache" /> object that
        ///                             contains the current process ID, thread ID, and stack trace
        ///                             information. </param>
        /// <param name="source">       A name used to identify the output, typically the name of the
        ///                             application that generated the trace event. </param>
        /// <param name="eventType">    One of the <see cref="T:System.Diagnostics.TraceEventType" />
        ///                             values specifying the type of event that has caused the trace. </param>
        /// <param name="id">           A numeric identifier for the event. </param>
        /// <param name="message">      A message to write. </param>
        public override void TraceEvent( TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message )
        {
            this.Enqueue( eventType, message );
        }

        /// <summary>
        /// When overridden in a derived class, writes the specified message to the listener you create
        /// in the derived class.
        /// </summary>
        /// <remarks>   David, 2021-03-05. </remarks>
        /// <param name="message">  A message to write. </param>
        public override void Write( string message )
        {
        }

        /// <summary>
        /// When overridden in a derived class, writes a message to the listener you create in the
        /// derived class, followed by a line terminator.
        /// </summary>
        /// <remarks>   David, 2021-03-05. </remarks>
        /// <param name="message">  A message to write. </param>
        public override void WriteLine( string message )
        {
        }

        #endregion

        #region " FILTERING "

        /// <summary>   Gets or sets the initial trace listener source level. </summary>
        /// <value> The initial trace listener source level. </value>
        public System.Diagnostics.SourceLevels InitialSourceLevel { get; private set; }

        /// <summary>   Gets or sets the trace listener source level. </summary>
        /// <value> The trace listener source level. </value>
        public System.Diagnostics.SourceLevels SourceLevel { get; private set; }

        /// <summary>
        /// Applies the trace listener filter described by <paramref name="sourceLevel"/>.
        /// </summary>
        /// <remarks>   David, 2021-02-09. </remarks>
        /// <param name="sourceLevel">  Source level. </param>
        public void ApplyListenerFilter( System.Diagnostics.SourceLevels sourceLevel )
        {
            this.SourceLevel = sourceLevel;
            this.Filter = new System.Diagnostics.EventTypeFilter( this.SourceLevel );
        }

        /// <summary>   Restore the initial trace listener filter. </summary>
        /// <remarks>   David, 2021-02-09. </remarks>
        public void RestoreListenerFilter()
        {
            this.ApplyListenerFilter( this.InitialSourceLevel );
        }

        #endregion

    }
}
