#region License and Terms
// Unconstrained Melody
// Copyright (c) 2009-2011 Jonathan Skeet. All rights reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#endregion

using System;
using System.Linq.Expressions;

namespace FastEnums
{
    /// <summary>
    /// Provides high performance utilities for enum type.
    /// </summary>
    public static partial class FastEnum
    {

        /// <summary>   An enum internals. </summary>
        /// <remarks>   David, 2021-02-20. </remarks>
        /// <typeparam name="TEnum">    Type of the enum. </typeparam>
        internal static class EnumInternals<TEnum> where TEnum : struct, Enum
        {
            public static readonly Type Type;
            public static readonly Type UnderlyingType;
            public static readonly bool IsFlags;
            public static readonly Func<TEnum, TEnum, TEnum> Or;
            public static readonly Func<TEnum, TEnum, TEnum> And;
            public static readonly Func<TEnum, TEnum> Not;
            public static readonly TEnum UsedBits;
            public static readonly TEnum AllBits;
            public static readonly TEnum UnusedBits;
            public static Func<TEnum, TEnum, bool> Equality;
            public static readonly Func<TEnum, bool> IsEmpty;

            /// <summary>   Static constructor. </summary>
            /// <remarks>   David, 2021-02-20. </remarks>
            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0034:Simplify 'default' expression", Justification = "<Pending>" )]
            static EnumInternals()
            {
                Type = typeof( TEnum );
                UnderlyingType = Enum.GetUnderlyingType( Type );
                IsFlags = typeof( TEnum ).IsDefined( typeof( FlagsAttribute ), false );
                // Parameters for various expression trees
                ParameterExpression param1 = Expression.Parameter( typeof( TEnum ), "x" );
                ParameterExpression param2 = Expression.Parameter( typeof( TEnum ), "y" );
                Expression convertedParam1 = Expression.Convert( param1, UnderlyingType );
                Expression convertedParam2 = Expression.Convert( param2, UnderlyingType );
                Equality = Expression.Lambda<Func<TEnum, TEnum, bool>>( Expression.Equal( convertedParam1, convertedParam2 ), param1, param2 ).Compile();
                Or = Expression.Lambda<Func<TEnum, TEnum, TEnum>>( Expression.Convert( Expression.Or( convertedParam1, convertedParam2 ), typeof( TEnum ) ), param1, param2 ).Compile();
                And = Expression.Lambda<Func<TEnum, TEnum, TEnum>>( Expression.Convert( Expression.And( convertedParam1, convertedParam2 ), typeof( TEnum ) ), param1, param2 ).Compile();
                Not = Expression.Lambda<Func<TEnum, TEnum>>( Expression.Convert( Expression.Not( convertedParam1 ), typeof( TEnum ) ), param1 ).Compile();
                IsEmpty = Expression.Lambda<Func<TEnum, bool>>( Expression.Equal( convertedParam1,
                    Expression.Constant( Activator.CreateInstance( UnderlyingType ) ) ), param1 ).Compile();

                UsedBits = default( TEnum );
                var values = ( TEnum[] ) Enum.GetValues( Type );
                foreach ( TEnum value in values )
                {
                    UsedBits = Or( UsedBits, value );
                }
                AllBits = Not( default( TEnum ) );
                UnusedBits = And( AllBits, (Not( UsedBits )) );
            }
        }

    }
}
