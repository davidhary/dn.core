using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;



namespace FastEnums.Internals
{
    /// <summary>
    /// Provides the read only array.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal sealed class ReadOnlyArray<T> : IReadOnlyList<T>
    {
        #region Fields
        private readonly T[] _Source;
        #endregion


        #region Constructors
        /// <summary>
        /// Creates instance.
        /// </summary>
        /// <param name="source"></param>
        public ReadOnlyArray( T[] source )
            => this._Source = source ?? throw new ArgumentNullException( nameof( source ) );
        #endregion


        #region Custom enumerator
        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        public Enumerator GetEnumerator()
            => new( this._Source );
        #endregion


        #region IReadOnlyList<T> implementations
        /// <summary>
        /// Gets the element at the specified index in the read-only list.
        /// </summary>
        /// <param name="index">The zero-based index of the element to get.</param>
        /// <returns>The element at the specified index in the read-only list.</returns>
        public T this[int index]
            => this._Source[index];


        /// <summary>
        /// Gets the number of elements in the collection.
        /// </summary>
        public int Count
            => this._Source.Length;


        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
            => new RefEnumerator( this._Source );


        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="IEnumerator"/> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
            => this._Source.GetEnumerator();
        #endregion


        #region Enumerator
        /// <summary>
        /// Provides an enumerator as value type that iterates through the collection.
        /// </summary>
        public struct Enumerator : IEnumerator<T>
        {
            #region Fields
            private readonly T[] _Source;
            private int _Index;
            #endregion


            #region Constructors
            internal Enumerator( T[] source )
            {
                this._Source = source;
                this._Index = -1;
            }
            #endregion


            #region IEnumerator<T> implementations
            public T Current
                => this._Source[this._Index];


            object? IEnumerator.Current
                => this.Current;


            public void Dispose()
            { }


            public bool MoveNext()
            {
                this._Index++;
                return ( uint ) this._Index < ( uint ) this._Source.Length;
            }


            public void Reset()
                => this._Index = -1;
            #endregion
        }


        /// <summary>
        /// Provides an enumerator as reference type that iterates through the collection.
        /// </summary>
        internal class RefEnumerator : IEnumerator<T>
        {
            #region Fields
            private readonly T[] _Source;
            private int _Index;
            #endregion


            #region Constructors
            internal RefEnumerator( T[] source )
            {
                this._Source = source;
                this._Index = -1;
            }
            #endregion


            #region IEnumerator<T> implementations
            public T Current
                => this._Source[this._Index];


            object? IEnumerator.Current
                => this.Current;


            public void Dispose()
            { }


            public bool MoveNext()
            {
                this._Index++;
                return ( uint ) this._Index < ( uint ) this._Source.Length;
            }


            public void Reset()
                => this._Index = -1;
            #endregion
        }
        #endregion
    }



    /// <summary>
    /// Provides <see cref="ReadOnlyArray{T}"/> extension methods.
    /// </summary>
    internal static class ReadOnlyArrayExtensions
    {
        /// <summary>
        /// Converts to <see cref="ReadOnlyArray{T}"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static ReadOnlyArray<T> ToReadOnlyArray<T>( this IEnumerable<T> source )
        {
            return source is null
                ? throw new ArgumentNullException( nameof( source ) )
                : source is T[] array
                ? new( array )
                : new( source.ToArray() );
        }


        /// <summary>
        /// Converts to <see cref="ReadOnlyArray{T}"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static ReadOnlyArray<T> AsReadOnly<T>( this T[] source )
            => new( source );
    }
}
