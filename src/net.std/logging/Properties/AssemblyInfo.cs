using System;

// [assembly: AssemblyDescription( "Core Logging Library" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Core.Logging.Tester,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey )]
