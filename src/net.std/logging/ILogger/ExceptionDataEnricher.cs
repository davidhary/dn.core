using System.Collections;
using System.Linq;

using Serilog.Core;

namespace isr.Core.Logging.ILogger
{
    /// <summary>   An exception data enricher. </summary>
    /// <remarks>   David, 2021-02-17.
    ///             https://gist.github.com/nblumhardt/dddaa2139bbf4b561fa7 </remarks>
    public class ExceptionDataEnricher : ILogEventEnricher
    {
        /// <summary>   Enrich the log event. </summary>
        /// <remarks>   David, 2021-02-17. </remarks>
        /// <param name="logEvent">         The log event to enrich. </param>
        /// <param name="propertyFactory">  Factory for creating new properties to add to the event. </param>
        public void Enrich( Serilog.Events.LogEvent logEvent, ILogEventPropertyFactory propertyFactory )
        {
            if ( logEvent.Exception == null ||
                logEvent.Exception.Data == null ||
                logEvent.Exception.Data.Count == 0 ) return;

            var dataDictionary = logEvent.Exception.Data
                .Cast<DictionaryEntry>()
                .Where( e => e.Key is string )
                .ToDictionary( e => ( string ) e.Key, e => e.Value );

            var property = propertyFactory.CreateProperty( "ExceptionData", dataDictionary, destructureObjects: true );

            logEvent.AddPropertyIfAbsent( property );
        }

    }

}
