using System;

namespace isr.Core.ComponentModelExtensions
{
    /// <summary> Includes extensions for component model elements. </summary>
    /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-01-14, 3.4.3953 </para></remarks>
    public static class ComponentModelExtensionMethods
    {

        #region " INVOKE ACTIONS "

        /// <summary> Executes the action on a different thread, and waits for the result. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="synchronizer"> Provides a way to synchronously or asynchronously execute a
        /// delegate. </param>
        /// <param name="action">       The action. </param>
        /// <example> control.InvokeAction(Sub() control.Text = "invoked") </example>
        public static void InvokeAction( this System.ComponentModel.ISynchronizeInvoke synchronizer, Action action )
        {
            synchronizer.InvokeAction( action, null );
        }

        /// <summary> Executes the action on a different thread, and waits for the result. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="synchronizer"> Provides a way to synchronously or asynchronously execute a
        /// delegate. </param>
        /// <param name="action">       The action. </param>
        /// <param name="args">         The arguments. </param>
        public static void InvokeAction( this System.ComponentModel.ISynchronizeInvoke synchronizer, Action action, object[] args )
        {
            if ( synchronizer is object & action is object )
            {
                if ( synchronizer.InvokeRequired )
                {
                    _ = synchronizer.Invoke( action, args );
                }
                else
                {
                    action.Invoke();
                }
            }
        }

        /// <summary>
        /// A System.ComponentModel.ISynchronizeInvoke extension method that executes the action on a
        /// different thread, asynchronously.
        /// </summary>
        /// <remarks>   David, 2021-08-05. </remarks>
        /// <param name="synchronizer"> Provides a way to synchronously or asynchronously execute a
        ///                             delegate. </param>
        /// <param name="action">       The action. </param>
        public static void BeginInvokeAction( this System.ComponentModel.ISynchronizeInvoke synchronizer, Action action )
        {
            synchronizer.BeginInvokeAction( action, null );
        }

        /// <summary>
        /// A System.ComponentModel.ISynchronizeInvoke extension method that executes the action on a
        /// different thread, asynchronously.
        /// </summary>
        /// <remarks>   David, 2021-08-05. </remarks>
        /// <param name="synchronizer"> Provides a way to synchronously or asynchronously execute a
        ///                             delegate. </param>
        /// <param name="action">       The action. </param>
        /// <param name="args">         The arguments. </param>
        public static void BeginInvokeAction( this System.ComponentModel.ISynchronizeInvoke synchronizer, Action action, object[] args )
        {
            _ = synchronizer.BeginInvoke( action, args );
        }

        #endregion

    }
}
