using System;

namespace isr.Core.Framework.Strings
{
    /// <summary>   A control characters. </summary>
    /// <remarks>   David, 2021-12-08. </remarks>
    public static class ControlChars
    {
        /// <summary>   (Immutable) the new line. </summary>
        public static readonly string NewLine = Environment.NewLine;

        /// <summary>   Gets the carriage return line feed. </summary>
        /// <value> The carriage return line feed. </value>
        public static string CrLf => $"{Cr}{Lf}";

        /// <summary>   (Immutable) the carriage return. </summary>
        public const char Cr = '\r';

        /// <summary>   (Immutable) the line feed. </summary>
        public const char Lf = '\n';

        /// <summary>   (Immutable) the form feed. </summary>
        public const char FormFeed = '\f';

        /// <summary>   (Immutable) the null. </summary>
        public const char Null = '\0';

        /// <summary>   (Immutable) the back space. </summary>
        public const char BackSpace = '\b';

        /// <summary>   (Immutable) the tab. </summary>
        public const char Tab = '\t';

        /// <summary>   (Immutable) the vertical tab. </summary>
        public const char VertTab = '\v';

        /// <summary>   (Immutable) the single quote. </summary>
        public const char SingleQuote = '\'';

        /// <summary>   (Immutable) the double quote. </summary>
        public const char DoubleQuote = '\"';
    }
}
