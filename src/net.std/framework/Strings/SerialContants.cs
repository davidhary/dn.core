
namespace isr.Core.Framework.Strings
{
    /// <summary>   A serial constants. </summary>
    /// <remarks>
    /// David, 2021-12-08. https://stackoverflow.com/questions/49830303/controlchars-class-in-c-sharp.
    /// </remarks>
    public static class SerialConstants
    {
        /// <summary>   (Immutable) the null 00. </summary>
        public const char Null_00 = '\x00';

        /// <summary>   (Immutable) the soh 01. </summary>
        public const char Soh_01 = '\x01';

        /// <summary>   (Immutable) the stx 02. </summary>
        public const char Stx_02 = '\x02';

        /// <summary>   (Immutable) the etx 03. </summary>
        public const char Etx_03 = '\x03';

        /// <summary>   (Immutable) the eot 04. </summary>
        public const char Eot_04 = '\x04';

        /// <summary>   (Immutable) the enq 05. </summary>
        public const char Enq_05 = '\x05';

        /// <summary>   (Immutable) the acknowledge 06. </summary>
        public const char Ack_06 = '\x06';

        /// <summary>   (Immutable) the line feed 0 a. </summary>
        public const char Lf_0A = '\x0A';

        /// <summary>   (Immutable) the mt 09. </summary>
        public const char Mt_09 = '\x09';

        /// <summary>   (Immutable) the carriage return 0 d. </summary>
        public const char Cr_0D = '\x0D';

        /// <summary>   (Immutable) the device-context 1 11. </summary>
        public const char Dc1_11 = '\x11';

        /// <summary>   (Immutable) the can 18. </summary>
        public const char Can_18 = '\x18';

        /// <summary>   (Immutable) the syn 22. </summary>
        public const char Syn_22 = '\x22';

        /// <summary>   (Immutable) the delete 7 f. </summary>
        public const char Del_7F = '\x7F';

        /// <summary>   (Immutable) the space 20. </summary>
        public const char Space_20 = '\x20';

        /// <summary>   (Immutable) the escape 1 b. </summary>
        public const char ESC_1B = '\x1B';
    }
}
