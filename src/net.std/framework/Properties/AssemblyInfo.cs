using System;

// [assembly: AssemblyDescription( "Core Framework Library" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Core.Framework.MSTest,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Core.Framework.Trials,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey )]
