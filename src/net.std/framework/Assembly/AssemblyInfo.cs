using System;

using System.Reflection;
using System.Runtime.InteropServices;
using System.Resources;

namespace isr.Core
{
    /// <summary>   Information about the assembly. </summary>
    /// <remarks>   David, 2021-02-16. </remarks>
    public class AssemblyInfo
    {

        /// <summary>   Constructors. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        public AssemblyInfo()
            : this( Assembly.GetExecutingAssembly() )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        /// <param name="assembly"> The assembly. </param>
        public AssemblyInfo( Assembly assembly )
        {
            // Get values from the assembly.
            AssemblyTitleAttribute titleAttr =
                GetAssemblyAttribute<AssemblyTitleAttribute>( assembly );
            if ( titleAttr != null ) this.Title = titleAttr.Title;

            AssemblyDescriptionAttribute assemblyAttr =
                GetAssemblyAttribute<AssemblyDescriptionAttribute>( assembly );
            if ( assemblyAttr != null ) this.Description = assemblyAttr.Description;

            AssemblyCompanyAttribute companyAttr =
                GetAssemblyAttribute<AssemblyCompanyAttribute>( assembly );
            if ( companyAttr != null ) this.Company = companyAttr.Company;

            AssemblyProductAttribute productAttr =
                GetAssemblyAttribute<AssemblyProductAttribute>( assembly );
            if ( productAttr != null ) this.Product = productAttr.Product;

            AssemblyCopyrightAttribute copyrightAttr =
                GetAssemblyAttribute<AssemblyCopyrightAttribute>( assembly );
            if ( copyrightAttr != null ) this.Copyright = copyrightAttr.Copyright;

            AssemblyTrademarkAttribute trademarkAttr =
                GetAssemblyAttribute<AssemblyTrademarkAttribute>( assembly );
            if ( trademarkAttr != null ) this.Trademark = trademarkAttr.Trademark;

            this.AssemblyVersion = assembly.GetName().Version;

            AssemblyFileVersionAttribute fileVersionAttr =
                GetAssemblyAttribute<AssemblyFileVersionAttribute>( assembly );
            if ( fileVersionAttr != null ) this.FileVersion = new Version( fileVersionAttr.Version );

            GuidAttribute guidAttr = GetAssemblyAttribute<GuidAttribute>( assembly );
            if ( guidAttr != null ) this.Guid = guidAttr.Value;

            NeutralResourcesLanguageAttribute languageAttr =
                GetAssemblyAttribute<NeutralResourcesLanguageAttribute>( assembly );
            if ( languageAttr != null ) this.NeutralLanguage = languageAttr.CultureName;

            ComVisibleAttribute comAttr =
                GetAssemblyAttribute<ComVisibleAttribute>( assembly );
            if ( comAttr != null ) this.IsComVisible = comAttr.Value;

            this.ProcessName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;

            this.ProcessProductName = (this.Product.StartsWith( this.ProcessName, StringComparison.OrdinalIgnoreCase ))
                ? this.Product
                : $"{this.ProcessName}.{this.Product}";

            this.FileVersionInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo( assembly.Location );

            this.Location = assembly.Location;

            this.DirectoryPath = new System.IO.DirectoryInfo( this.Location ).FullName;
        }

        /// <summary>   Gets an assembly attribute. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="assembly"> The assembly. </param>
        /// <returns>   The assembly attribute. </returns>
        public static T GetAssemblyAttribute<T>( Assembly assembly ) where T : Attribute
        {
            // Get attributes of this type.
            object[] attributes = assembly.GetCustomAttributes( typeof( T ), true );

            // If we didn't get anything, return null.
            if ( (attributes == null) || (attributes.Length == 0) ) return null;

            // Convert the first attribute value into the desired type and return it.
            return ( T ) attributes[0];
        }

        /// <summary>   Gets the title. </summary>
        /// <value> The title. </value>
        public string Title { get; }

        /// <summary>   Gets the description. </summary>
        /// <value> The description. </value>
        public string Description { get; }

        /// <summary>   Gets the company. </summary>
        /// <value> The company. </value>
        public string Company { get; }

        /// <summary>   Gets the product. </summary>
        /// <value> The product. </value>
        public string Product { get; }

        /// <summary>   Gets the copyright. </summary>
        /// <value> The copyright. </value>
        public string Copyright { get; }

        /// <summary>   Gets the trademark. </summary>
        /// <value> The trademark. </value>
        public string Trademark { get; }

        /// <summary>   Gets the assembly version. </summary>
        /// <value> The assembly version. </value>
        public Version AssemblyVersion { get; }

        /// <summary>   Gets the file version. </summary>
        /// <value> The file version. </value>
        public Version FileVersion { get; }

        /// <summary>   Gets a unique identifier. </summary>
        /// <value> The identifier of the unique. </value>
        public string Guid { get; }

        /// <summary>   Gets the neutral language. </summary>
        /// <value> The neutral language. </value>
        public string NeutralLanguage { get; }

        /// <summary>   Gets a value indicating whether the com is visible. </summary>
        /// <value> True if the com is visible, false if not. </value>
        public bool IsComVisible { get; }

        /// <summary>   Gets the name of the process. </summary>
        /// <value> The name of the process. </value>
        public string ProcessName { get; }

        /// <summary>   Gets the name of the process product. </summary>
        /// <value> The name of the process product. </value>
        public string ProcessProductName { get; }

        /// <summary>   Gets information describing the file version. </summary>
        /// <value> Information describing the file version. </value>
        public System.Diagnostics.FileVersionInfo FileVersionInfo { get; }

        /// <summary>   Gets the location. </summary>
        /// <value> The location. </value>
        public string Location { get; }

        /// <summary>   Gets the full pathname of the directory file. </summary>
        /// <value> The full pathname of the directory file. </value>
        public string DirectoryPath { get; }

        /// <summary> Gets the application information caption with ISO 8601 UTC time, e.g., '2019-09-10 19:27:04Z'. </summary>
        /// <value> The UTC caption. </value>
        public string IsoUniversalTimeCaption => $"{this.ProcessProductName}.r.{this.AssemblyVersion.ToString( 4 )} {DateTimeOffset.Now:u)}";

        /// <summary> Gets the GMT caption, e.g., 'Tue, 10 May 2019 19:26:42 GMT'. </summary>
        /// <value> The GMT caption. </value>
        public string GmtCaption => $"{this.ProcessProductName}.r.{this.AssemblyVersion.ToString( 4 )} {DateTimeOffset.Now:r)}";

        /// <summary> Gets the application information caption with ISO complete time, e.g., '2019-09-10T12:12:29.7552627-07:00'. </summary>
        /// <value> The UTC caption. </value>
        public string IsoCompleteCaption => $"{this.ProcessProductName}.r.{this.AssemblyVersion.ToString( 4 )} {DateTimeOffset.Now:o)}";

        /// <summary> Gets the application ISO 8691 Short date and time caption, e.g., 2019-09-10T12:24:47-07:00. </summary>
        /// <value> The time caption. </value>
        public string IsoShortCaption => $"{this.ProcessProductName}.r.{this.AssemblyVersion.ToString( 4 )} {DateTimeOffset.Now:s)}{DateTimeOffset.Now:zzz)}";


    }
}
