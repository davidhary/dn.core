using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using Microsoft.Extensions.Logging;

namespace isr.Core.TraceLog
{
    /// <summary> Defines the contract that must be implemented by View Models
    ///           capable of logging and tracing. </summary>
    /// <remarks>
    /// <list type="bullet">View Model Notify implementation: <item>
    /// Notify Function calls Synchronization Context Post;</item><item>
    /// Async Notify Function calls Synchronization Context Post;</item><item>
    /// Sync Notify function calls Synchronization Context Send;</item><item>
    /// Raise event (custom implementation) calls Synchronization Context Post;</item><item>
    /// Notify is used as the default in property set functions.</item></list> <para>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-10. Created from property notifiers
    /// </para>
    /// </remarks>
    public abstract partial class ViewModelLoggerBase : INotifyPropertyChanged
    {

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION"

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " TEXT WRITER "

        /// <summary>   Adds a display trace event writer. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="textWriter">   The trace Event tWriter. </param>
        public void AddDisplayTextWriter( Tracing.ITraceEventWriter textWriter )
        {
            textWriter.TraceLevel = this.DisplayTraceEventType;
            // deprecated: log level does not map to trace event types, which are used for 
            // filtering the display.  TraceLogger.ToTraceEventType( this.DisplayLogLevel );
            Tracing.TracingPlatform.Instance.AddTraceEventWriter( textWriter );
        }

        /// <summary>   Removes the display text writer described by <paramref name="textWriter"/>. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="textWriter">   The trace Event tWriter. </param>
        public void RemoveDisplayTextWriter( Tracing.ITraceEventWriter textWriter )
        {
            Tracing.TracingPlatform.Instance.RemoveTraceEventWriter( textWriter );
        }

        #endregion

        #region " DISPLAY TRACE EVENT TYPE "

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> value name pair for the global trace event
        /// writer. This level determines the level of all the
        /// <see cref="Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
        /// lower level.
        /// </summary>
        /// <value> The trace event writer trace event value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<TraceEventType, string> TraceEventWriterTraceEventValueNamePair
        {
            get => TraceLogger.Instance.TraceEventWriterTraceEventValueNamePair;

            set {
                if ( !KeyValuePair<TraceEventType, string>.Equals( value, this.TraceEventWriterTraceEventValueNamePair ) )
                {
                    TraceLogger.Instance.TraceEventWriterTraceEventValueNamePair = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> for the global trace event writer. This level
        /// determines the level of all the
        /// <see cref="Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
        /// lower level.
        /// </summary>
        /// <value> The type of the trace event writer trace event. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType TraceEventWriterTraceEventType
        {
            get => TraceLogger.Instance.TraceEventWriterTraceEventType;

            set {
                if ( value != this.TraceEventWriterTraceEventType )
                {
                    TraceLogger.Instance.TraceEventWriterTraceEventType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private KeyValuePair<TraceEventType, string> _DisplayTraceEventTypeValueNamePair;
        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> value name pair for display.
        /// </summary>
        /// <value> The log level value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<TraceEventType, string> DisplayTraceEventTypeValueNamePair
        {
            get => this._DisplayTraceEventTypeValueNamePair;

            set {
                if ( !KeyValuePair<TraceEventType, string>.Equals( value, this.DisplayTraceEventTypeValueNamePair ) )
                {
                    this._DisplayTraceEventTypeValueNamePair = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private TraceEventType _DisplayTraceEventType;
        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> for display.
        /// </summary>
        /// <value> The trace event writer log level. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType DisplayTraceEventType
        {
            get => this._DisplayTraceEventType;

            set {
                if ( value != this.DisplayTraceEventType )
                {
                    this._DisplayTraceEventType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " LOG LEVEL "

        /// <summary>
        /// Gets or sets the <see cref="LogLevel"/> value name pair for logging. This level determines
        /// the level of the <see cref="ILogger"/>.
        /// </summary>
        /// <value> The log level value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<LogLevel, string> LogLevelValueNamePair
        {
            get => TraceLogger.Instance.LoggingLevelValueNamePair;

            set {
                if ( !KeyValuePair<LogLevel, string>.Equals( value, this.LogLevelValueNamePair ) )
                {
                    TraceLogger.Instance.LoggingLevelValueNamePair = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="LogLevel"/> for logging. This level determines the level of the
        /// <see cref="ILogger"/>.
        /// </summary>
        /// <value> The log level. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LogLevel LogLevel
        {
            get => TraceLogger.Instance.MinimumLogLevel;

            set {
                if ( value != this.LogLevel )
                {
                    TraceLogger.Instance.MinimumLogLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " LOG EXCEPTION "

        /// <summary>   Add exception data. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="ex">       The exception. </param>
        /// <returns>   True if exception data was added for this exception; otherwise, false. </returns>
        protected abstract bool AddExceptionData( Exception ex );

        /// <summary>   Log an exception. </summary>
        /// <remarks>   David, 2021-07-03. </remarks>
        /// <param name="ex">               The ex. </param>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
        /// <returns>   A String. </returns>
        protected string LogError( Exception ex, string message, [CallerMemberName] string memberName = "",
                                                                 [CallerFilePath] string sourceFilePath = "",
                                                                 [CallerLineNumber] int sourceLineNumber = 0 )
        {
            _ = this.AddExceptionData( ex );
            return TraceLog.TraceLogger.LogError( ex, message, memberName, sourceFilePath, sourceLineNumber );
        }

        #endregion

    }
}
