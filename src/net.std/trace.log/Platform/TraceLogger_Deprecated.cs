namespace isr.Core.TraceLog.Platform
{
    /// <summary>   A trace logger. </summary>
    /// <remarks>   David, 2021-08-28. </remarks>
    public partial class TraceLogger
    {

        #region " TRACE EVENT WRITER LOG LEVELS "

#if false
        // deprecated:
        // Trace event types and trace source levels, which are used to filter the trace events, do not map to log levels.  
        // Consequently, only trace event types and source levels can be used in conjunctions with trace event writers.

        // trace event levels do not map to trace events.  Because the trace writer filter the Trace,
        // which adheres to the trace event type, the log levels can be deprecated in favor of 
        // using the trace event for the 'Show' Level. The Log Level is used for the Log Levels.

        private KeyValuePair<LogLevel, string> _TraceEventWriterLogLevelValueNamePair;

        /// <summary>
        /// Gets or sets the <see cref="LogLevel"/> value name pair of the global trace event writer.
        /// This level determines the level of all the <see cref="Tracing.ITraceEventWriter"/>s Trace
        /// Listeners. Each listener can listen at a lower trace level.
        /// </summary>
        /// <value> The log level value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<LogLevel, string> TraceEventWriterLogLevelValueNamePair
        {
            get => this._TraceEventWriterLogLevelValueNamePair;

            set {
                if ( value.Key != this.TraceEventWriterLogLevelValueNamePair.Key )
                {
                    this._TraceEventWriterLogLevelValueNamePair = value;
                    this.TraceEventWriterLogLevel = value.Key;
                    this.NotifyPropertyChanged();
                    this.TraceEventWriterTraceEventValueNamePair = TraceLogger.ToTraceEventValueNamePair( this.TraceEventWriterTraceEventType );
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="LogLevel"/>  of the global trace event writer. This level
        /// determines the level of all the
        /// <see cref="Tracing.ITraceEventWriter"/>s Trace Listeners. Each listener can listen at a
        /// lower trace level.
        /// </summary>
        /// <value> The trace level. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LogLevel TraceEventWriterLogLevel
        {
            get => TraceLogger.ToLogLevel( Tracing.TracingPlatform.Instance.TraceEventWriterSourceLevel ); // this.TraceShowEvent.Key;

            set {
                if ( value != this.TraceEventWriterLogLevel )
                {
                    this.NotifyPropertyChanged();
                    this.TraceEventWriterLogLevelValueNamePair = TraceLogger.ToLogLevelValueNamePair( value );
                    Tracing.TracingPlatform.Instance.TraceEventWriterSourceLevel = TraceLogger.ToSourceLevel( this.TraceEventWriterLogLevel );
                }
            }
        }

#endif

        #endregion

        #region " LOGGING TRACE EVENT "
#if false
        // Logging trace event was removed because the trace event level does not map to the
        // the log level missing the Trace and Debug (in case of Microsoft Log Level, or 
        // Debug and Verbose in case of the Serilog log levels.)

        private KeyValuePair<TraceEventType, string> _LoggingTraceEventValueNamePair;

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> value name pair for logging. This level
        /// determines the level of the <see cref="ILogger"/>.
        /// </summary>
        /// <value> The log trace event value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<TraceEventType, string> LoggingTraceEventValueNamePair
        {
            get => this._LoggingTraceEventValueNamePair;

            set {
                if ( value.Key != this.LoggingTraceEventValueNamePair.Key )
                {
                    this._LoggingTraceEventValueNamePair = value;
                    this.LoggingTraceEventType = value.Key;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets or sets <see cref="TraceEventType"/> value for logging. </summary>
        /// <value> The trace event type for logging. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType LoggingTraceEventType
        {
            get => TraceLogger.ToTraceEventType( this.LoggingLogLevel );

            set {
                if ( value != this.LoggingTraceEventType )
                {
                    this.LoggingLogLevel = TraceLogger.ToLogLevel( value );
                    this.NotifyPropertyChanged();
                    this.LoggingTraceEventValueNamePair = TraceLogger.ToTraceEventValueNamePair( value );
                }
            }
        }
#endif

        #endregion

        #region " TRACE EVENTS: STATIC "

#if false
// trace event types do not map to log levels. 


        /// <summary>   Converts a <see cref="System.Diagnostics.TraceEventType"/> level to
        /// <see cref="Microsoft.Extensions.Logging.LogLevel"/>. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="level">    The level. </param>
        /// <returns>   Level as a Microsoft.Extensions.Logging.LogLevel. </returns>
        public static Microsoft.Extensions.Logging.LogLevel ToLogLevel( System.Diagnostics.TraceEventType level )
        {
            return 0 != (level & TraceEventType.Critical)
                ? Microsoft.Extensions.Logging.LogLevel.Critical
                : 0 != (level & TraceEventType.Error)
                ? Microsoft.Extensions.Logging.LogLevel.Error
                : 0 != (level & TraceEventType.Warning)
                ? Microsoft.Extensions.Logging.LogLevel.Warning
                : 0 != (level & TraceEventType.Information)
                ? Microsoft.Extensions.Logging.LogLevel.Information
                : 0 != (level & TraceEventType.Verbose)
                ? Microsoft.Extensions.Logging.LogLevel.Trace
                : Microsoft.Extensions.Logging.LogLevel.Information;
        }

        /// <summary>   Converts a <see cref="Microsoft.Extensions.Logging.LogLevel"/> level to
        /// <see cref="System.Diagnostics.TraceEventType"/>. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="level">    The level. </param>
        /// <returns>   Level as the System.Diagnostics.SourceLevels. </returns>
        public static System.Diagnostics.TraceEventType ToTraceEventType( Microsoft.Extensions.Logging.LogLevel level )
        {
            return level switch {
                Microsoft.Extensions.Logging.LogLevel.Critical => System.Diagnostics.TraceEventType.Critical,
                Microsoft.Extensions.Logging.LogLevel.Debug => System.Diagnostics.TraceEventType.Transfer,
                Microsoft.Extensions.Logging.LogLevel.Error => System.Diagnostics.TraceEventType.Error,
                Microsoft.Extensions.Logging.LogLevel.Information => System.Diagnostics.TraceEventType.Information,
                Microsoft.Extensions.Logging.LogLevel.Trace => System.Diagnostics.TraceEventType.Verbose,
                Microsoft.Extensions.Logging.LogLevel.Warning => System.Diagnostics.TraceEventType.Warning,
                Microsoft.Extensions.Logging.LogLevel.None => System.Diagnostics.TraceEventType.Information,
                _ => System.Diagnostics.TraceEventType.Information,
            };
        }

#endif

        #endregion

        #region " SINGLETON LOGGER "

#if false
        // deprecated: Trace event types do not map to log levels. 

        /// <summary>
        /// Write a log message using the <see cref="Logger"/>
        /// <see cref="Microsoft.Extensions.Logging.ILogger{TraceLogger}"/>.
        /// </summary>
        /// <remarks>   David, 2021-03-13. </remarks>
        /// <param name="level">            The level. </param>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
        /// <returns>   A string. </returns>
        public static string LogCallerMessage( System.Diagnostics.TraceEventType level, string message,
                                                       [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceLogger.LogCallerMessage( TraceLogger.ToLogLevel( level ), message, memberName, sourcePath, sourceLineNumber );
        }

        // deprecated: trace event types do not map to log levels.
        
        /// <summary>   Write a log message using the <see cref="Logger"/> <see cref="Microsoft.Extensions.Logging.ILogger{TraceLogger}"/>. </summary>
        /// <remarks>   David, 2021-03-13. </remarks>
        /// <param name="level">    The level. </param>
        /// <param name="message">  The message. </param>
        public static string LogMessage( System.Diagnostics.TraceEventType level, string message )
        {
            return TraceLogger.LogMessage( TraceLogger.ToLogLevel( level ), message );
        }

#endif

#if false

        // not working when writing from the Serilog Log Logger.
        // These do not work.  Got to figure why.

        /// <summary>   Logs trace (same as verbose for the serilog logger). </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">  The message. </param>
        public static string LogTrace( string message )
        {
            Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Verbose, message );
            return message;
        }

        /// <summary>   Writes a verbose message. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">  The message. </param>
        public static string LogVerbose( string message )
        {
            Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Verbose, message );
            return message;
        }

        /// <summary>   Writes a Debug Serilog message. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">  The message. </param>
        public static string LogDebug( string message )
        {
            Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Debug, message );
            return message;
        }

        /// <summary>   Writes a Information Serilog message. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">  The message. </param>
        public static string LogInformation( string message )
        {
            Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Information, message );
            return message;
        }

        /// <summary>   Writes a Warning Serilog message. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">  The message. </param>
        public static string LogWarning( string message )
        {
            Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Warning, message );
            return message;
        }

        /// <summary>   Writes a Error Serilog message. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">  The message. </param>
        public static string LogError( string message )
        {
            Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Error, message );
            return message;
        }

        /// <summary>   Writes a Error Serilog message. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="ex">       The exception. </param>
        /// <param name="message">  The message. </param>
        public static string LogError( Exception ex, string message )
        {
            Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Error, ex, message );
            return $"{message}: {ex.Message};
        }

        /// <summary>   Writes a Critical (Fatal for the Serilog Logger) Serilog message. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">  The message. </param>
        public static string LogCritical( string message )
        {
            Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Fatal, message );
            return message;
        }

        /// <summary>   Writes a Fatal Serilog message. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">  The message. </param>
        public static string LogFatal( string message )
        {
            Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Fatal, message );
            return message;
        }


        /// <summary>   Writes a Serilog message. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="level">    The level. </param>
        /// <param name="message">  The message. </param>
        public static string LogMessage( LogLevel level, string message )
        {
            if ( Serilog.Log.Logger is not object ) return;
            switch ( level )
            {
                case LogLevel.Trace:
                    Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Verbose, message );
                    break;
                case LogLevel.Debug:
                    Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Debug, message );
                    break;
                case LogLevel.Information:
                    Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Information, message );
                    break;
                case LogLevel.Warning:
                    Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Warning, message );
                    break;
                case LogLevel.Error:
                    Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Error, message );
                    break;
                case LogLevel.Critical:
                    Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Fatal, message );
                    break;
                case LogLevel.None:
                    break;
                default:
                    Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Information, message );
                    break;
            }
            return message;
        }

        /// <summary>   Writes a Serilog message. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="level">    The level. </param>
        /// <param name="message">  The message. </param>
        public static string LogMessage( System.Diagnostics.TraceEventType level, string message )
        {
            if ( Serilog.Log.Logger is not object ) return;
            switch ( level )
            {
                case TraceEventType.Critical:
                    Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Fatal, message );
                    break;
                case TraceEventType.Error:
                    Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Error, message );
                    break;
                case TraceEventType.Information:
                    Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Information, message );
                    break;
                case TraceEventType.Verbose:
                    Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Verbose, message );
                    break;
                case TraceEventType.Warning:
                    Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Warning, message );
                    break;
                default:
                    Serilog.Log.Logger.Write( Serilog.Events.LogEventLevel.Information, message );
                    break;
            return message;
            }
        }

#endif

        #endregion

        #region " SOURCED LEVEL "

#if false
        // Source levels do not map to log levels and were removed

        /// <summary>
        /// Converts a <see cref="System.Diagnostics.SourceLevels"/> level to
        /// <see cref="Microsoft.Extensions.Logging.LogLevel"/>.
        /// </summary>
        /// <remarks>
        /// <see cref="System.Diagnostics.SourceLevels"/> do not fully map to
        /// <see cref="Microsoft.Extensions.Logging.LogLevel"/>
        /// The Debug and Trace Log Levels both map to the Verbose levels. Therefore, when setting the
        /// log level, verbose will map to Trace. When setting the Source Level, both Debug and Trace map
        /// verbose.
        /// </remarks>
        /// <param name="level">    The level. </param>
        /// <returns>   Level as a Microsoft.Extensions.Logging.LogLevel. </returns>
        public static Microsoft.Extensions.Logging.LogLevel ToLogLevel( System.Diagnostics.SourceLevels level )
        {
            return level switch {
                System.Diagnostics.SourceLevels.Critical => Microsoft.Extensions.Logging.LogLevel.Critical,
                System.Diagnostics.SourceLevels.Error => Microsoft.Extensions.Logging.LogLevel.Error,
                System.Diagnostics.SourceLevels.Information => Microsoft.Extensions.Logging.LogLevel.Information,
                System.Diagnostics.SourceLevels.Verbose => Microsoft.Extensions.Logging.LogLevel.Trace,
                System.Diagnostics.SourceLevels.Warning => Microsoft.Extensions.Logging.LogLevel.Warning,
                _ => Microsoft.Extensions.Logging.LogLevel.Information,
            };
        }

        /// <summary>   Converts a <see cref="Microsoft.Extensions.Logging.LogLevel"/> level to
        /// <see cref="System.Diagnostics.SourceLevels"/>. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="level">    The level. </param>
        /// <returns>   Level as the System.Diagnostics.SourceLevels. </returns>
        public static System.Diagnostics.SourceLevels ToSourceLevel( Microsoft.Extensions.Logging.LogLevel level )
        {
            return level switch {
                Microsoft.Extensions.Logging.LogLevel.Critical => System.Diagnostics.SourceLevels.Critical,
                Microsoft.Extensions.Logging.LogLevel.Debug => System.Diagnostics.SourceLevels.Verbose,
                Microsoft.Extensions.Logging.LogLevel.Error => System.Diagnostics.SourceLevels.Error,
                Microsoft.Extensions.Logging.LogLevel.Information => System.Diagnostics.SourceLevels.Information,
                Microsoft.Extensions.Logging.LogLevel.Trace => System.Diagnostics.SourceLevels.Verbose,
                Microsoft.Extensions.Logging.LogLevel.Warning => System.Diagnostics.SourceLevels.Warning,
                Microsoft.Extensions.Logging.LogLevel.None => System.Diagnostics.SourceLevels.Information,
                _ => System.Diagnostics.SourceLevels.Information,
            };
        }
#endif

        #endregion

    }


}
