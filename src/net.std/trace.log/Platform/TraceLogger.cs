using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

using Microsoft.Extensions.Logging;

namespace isr.Core.TraceLog
{

    /// <summary>   A trace logger for emitting messages to file, trace and debug listeners. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    public class TraceLogger : INotifyPropertyChanged
    {

        #region " CONSTRUCTION AND CLEANUP "

        /// <summary>   Static constructor. </summary>
        /// <remarks>   David, 2021-02-22. </remarks>
        private TraceLogger()
        {
            // the logger logs the messages to a file, trace and debug
            this.Logger = isr.Core.Logging.ILogger.LoggingPlatform.Instance.CreateSerilogger<TraceLogger>( TraceLogger.LogSettingsFileName );
        }

        #endregion

        #region " SINGLETON LOGGER "

        /// <summary>   Gets the logger. which emits messages to file, trace (warnings and errors), and debug (fatal messages only). </summary>
        /// <value> The logger. </value>
        public Microsoft.Extensions.Logging.ILogger<TraceLogger> Logger { get; }

        /// <summary>   Reset the Serilog Log Logger to default and dispose the original if possible.. </summary>
        /// <remarks>   David, 2021-03-13. </remarks>
        public static void CloseAndFlush()
        {
            isr.Core.Logging.ILogger.LoggingPlatform.CloseAndFlush();
        }

        /// <summary>   Return a a Serilog.ILogger that adds logging context. </summary>
        /// <remarks>   David, 2021-07-02. 
        /// https://stackoverflow.com/questions/29470863/serilog-output-enrich-all-messages-with-methodname-from-which-log-entry-was-ca
        /// </remarks>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
        /// <returns>   A Serilog.ILogger. </returns>
        public static Serilog.ILogger GetContextLogger( [CallerMemberName] string memberName = "",
                                                        [CallerFilePath] string sourceFilePath = "",
                                                        [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return Serilog.Log.Logger
                .ForContext( "MemberName", memberName )
                .ForContext( "FilePath", sourceFilePath )
                .ForContext( "LineNumber", sourceLineNumber );
        }

        // Writing from the Microsoft ILogger works; writing from the Serilog Log Logger does not work. See deprecated file.

        /// <summary>   Builds a message with caller information. </summary>
        /// <remarks>   David, 2021-07-03. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       Name of the caller member. </param>
        /// <param name="sourcePath">       Full pathname of the caller member source file. </param>
        /// <param name="sourceLineNumber"> Line number in the caller member source file. </param>
        /// <returns>   A string. </returns>
        private static string BuildMessage( string message, string memberName, string sourcePath, int sourceLineNumber )
        {
            return $"[{sourcePath}].{memberName}.Line#{sourceLineNumber}, {message}";
        }

        ///            Critical,
        ///
        /// None:
        ///     Not used for writing log messages. Specifies that a logging category should not
        ///     write any messages.
        ///            None


        /// <summary>
        /// Writes a trace message with caller information. Contains the most detailed messages. These
        /// messages may contain sensitive application data. These messages are disabled by default and
        /// should never be enabled in a production environment.
        /// </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
        /// <returns>   A string. </returns>
        public static string LogTrace( string message, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            message = TraceLogger.BuildMessage( message, memberName, sourcePath, sourceLineNumber );
            TraceLogger.Instance.Logger.LogTrace( message );
            return message;
        }

        /// <summary>
        /// Writes a verbose (trace for the Microsoft ILogger) message with caller information. Contains
        /// the most detailed messages. These messages may contain sensitive application data. These
        /// messages are disabled by default and should never be enabled in a production environment.
        /// </summary>
        /// <remarks>   This is a left over from using trace event types to manage logging. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
        /// <returns>   A string. </returns>
        public static string LogVerbose( string message, [CallerMemberName] string memberName = "",
                                                         [CallerFilePath] string sourcePath = "",
                                                         [CallerLineNumber] int sourceLineNumber = 0 )
        {
            message = TraceLogger.BuildMessage( message, memberName, sourcePath, sourceLineNumber );
            TraceLogger.Instance.Logger.LogTrace( message );
            return message;
        }

        /// <summary>
        /// Writes a Debug message with caller information. Used for interactive investigation during
        /// development. These logs should primarily contain information useful for debugging and have no
        /// long-term value.
        /// </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
        /// <returns>   A string. </returns>
        public static string LogDebug( string message, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            message = TraceLogger.BuildMessage( message, memberName, sourcePath, sourceLineNumber );
            TraceLogger.Instance.Logger.LogDebug( message );
            return message;
        }

        /// <summary>
        /// Writes a Information message with caller information. Logs that track the general flow of the
        /// application. These logs should have long-term value.
        /// </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
        /// <returns>   A string. </returns>
        public static string LogInformation( string message, [CallerMemberName] string memberName = "",
                                                             [CallerFilePath] string sourcePath = "",
                                                             [CallerLineNumber] int sourceLineNumber = 0 )
        {
            message = TraceLogger.BuildMessage( message, memberName, sourcePath, sourceLineNumber );
            TraceLogger.Instance.Logger.LogInformation( message );
            return message;
        }

        /// <summary>
        /// Writes a Warning message with caller information. Logs that highlight an abnormal or
        /// unexpected event in the application flow, but do not otherwise cause the application
        /// execution to stop.
        /// </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the caller source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
        /// <returns>   A string. </returns>
        public static string LogWarning( string message, [CallerMemberName] string memberName = "",
                                                         [CallerFilePath] string sourcePath = "",
                                                         [CallerLineNumber] int sourceLineNumber = 0 )
        {
            message = TraceLogger.BuildMessage( message, memberName, sourcePath, sourceLineNumber );
            TraceLogger.Instance.Logger.LogWarning( message );
            return message;
        }

        /// <summary>
        /// Writes a Error message with caller information. Logs that highlight when the current flow of
        /// execution is stopped due to a failure. These should indicate a failure in the current
        /// activity, not an application-wide failure.
        /// </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
        /// <returns>   A string. </returns>
        public static string LogError( string message, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            message = TraceLogger.BuildMessage( message, memberName, sourcePath, sourceLineNumber );
            TraceLogger.Instance.Logger.LogError( message );
            return message;
        }

        /// <summary>
        /// Writes a Error message with caller information. Logs that highlight when the current flow of
        /// execution is stopped due to a failure. These should indicate a failure in the current
        /// activity, not an application-wide failure.
        /// </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="ex">               The exception. </param>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
        /// <returns>   A string. </returns>
        public static string LogError( Exception ex, string message, [CallerMemberName] string memberName = "",
                                                                     [CallerFilePath] string sourcePath = "",
                                                                     [CallerLineNumber] int sourceLineNumber = 0 )
        {
            message = TraceLogger.BuildMessage( message, memberName, sourcePath, sourceLineNumber );
            TraceLogger.Instance.Logger.LogError( ex, message );
            return message;
        }

        /// <summary>
        /// Writes a Critical (Fatal) Serilog message with caller information. Logs that describe an
        /// unrecoverable application or system crash, or a catastrophic failure that requires immediate
        /// attention.
        /// </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
        /// <returns>   A string. </returns>
        public static string LogCritical( string message, [CallerMemberName] string memberName = "",
                                                          [CallerFilePath] string sourcePath = "",
                                                          [CallerLineNumber] int sourceLineNumber = 0 )
        {
            message = TraceLogger.BuildMessage( message, memberName, sourcePath, sourceLineNumber );
            TraceLogger.Instance.Logger.LogCritical( message );
            return message;
        }

        /// <summary>
        /// Writes a Fatal (Critical for the Microsoft ILogger) message  with caller information.
        /// Logs that describe an unrecoverable application or system crash, or a catastrophic
        /// failure that requires immediate attention.
        /// </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
        /// <returns>   A string. </returns>
        public static string LogFatal( string message, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            message = TraceLogger.BuildMessage( message, memberName, sourcePath, sourceLineNumber );
            TraceLogger.Instance.Logger.LogCritical( message );
            return message;
        }

        /// <summary>
        /// Write a log message with caller information using the <see cref="Logger"/>
        /// <see cref="Microsoft.Extensions.Logging.ILogger{TraceLogger}"/>.
        /// </summary>
        /// <remarks>   David, 2021-03-13. </remarks>
        /// <param name="messageLevel">     The logged message level. </param>
        /// <param name="message">          The message. </param>
        /// <param name="minimumLevel">     (Optional) The minimum level. The message is passed to the
        ///                                 logger if the <paramref name="messageLevel"/> is the same or
        ///                                 higher than this level. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the caller member source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller member source file. </param>
        /// <returns>   A string. </returns>
        public static string LogCallerMessage( LogLevel messageLevel, string message, LogLevel minimumLevel = LogLevel.Trace,
                                               [CallerMemberName] string memberName = "", [CallerFilePath] string sourcePath = "",
                                               [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return messageLevel < minimumLevel
                ? message
                : messageLevel switch {
                LogLevel.Trace => TraceLogger.LogTrace( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Debug => TraceLogger.LogDebug( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Information => TraceLogger.LogInformation( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Warning => TraceLogger.LogWarning( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Error => TraceLogger.LogError( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.Critical => TraceLogger.LogCritical( message, memberName, sourcePath, sourceLineNumber ),
                LogLevel.None => string.Empty,
                _ => TraceLogger.LogInformation( message, memberName, sourcePath, sourceLineNumber ),
            };
        }

        /// <summary>
        /// Write a log message using the <see cref="Logger"/>
        /// <see cref="Microsoft.Extensions.Logging.ILogger{TraceLogger}"/>.
        /// </summary>
        /// <remarks>   David, 2022-02-09. </remarks>
        /// <param name="level">    The level. </param>
        /// <param name="message">  The message. </param>
        /// <returns>   A string. </returns>
        public static string LogEventType( TraceEventType level, string message )
        {
            switch ( level )
            {
                case TraceEventType.Information:
                    Instance.Logger.LogInformation( message );
                    break;
                case TraceEventType.Warning:
                    Instance.Logger.LogWarning( message );
                    break;
                case TraceEventType.Error:
                    Instance.Logger.LogError( message );
                    break;
                case TraceEventType.Critical:
                    Instance.Logger.LogError( message );
                    break;
                case TraceEventType.Verbose:
                    Instance.Logger.LogTrace( message );
                    break;
                default:
                    Instance.Logger.LogInformation( message );
                    break;
            }
            return message;
        }

        /// <summary>   Write a log message using the <see cref="Logger"/> <see cref="Microsoft.Extensions.Logging.ILogger{TraceLogger}"/>. </summary>
        /// <remarks>   David, 2021-03-13. </remarks>
        /// <param name="level">    The level. </param>
        /// <param name="message">  The message. </param>
        public static string LogMessage( LogLevel level, string message )
        {
            switch ( level )
            {
                case LogLevel.Trace:
                    Instance.Logger.LogTrace( message );
                    break;
                case LogLevel.Debug:
                    Instance.Logger.LogDebug( message );
                    break;
                case LogLevel.Information:
                    Instance.Logger.LogInformation( message );
                    break;
                case LogLevel.Warning:
                    Instance.Logger.LogWarning( message );
                    break;
                case LogLevel.Error:
                    Instance.Logger.LogError( message );
                    break;
                case LogLevel.Critical:
                    Instance.Logger.LogCritical( message );
                    break;
                case LogLevel.None:
                    break;
                default:
                    Instance.Logger.LogInformation( message );
                    break;
            }
            return message;
        }

        #endregion

        #region " LAZY SINGLETON "

        private static string _LogSettingsFileName;
        /// <summary>
        /// Gets or sets the filename of the log settings file, which defaults to the Entry assembly name.
        /// </summary>
        /// <value> The filename of the log settings file. </value>
        public static string LogSettingsFileName
        {
            get {
                if ( string.IsNullOrWhiteSpace( TraceLogger._LogSettingsFileName ) )
                    _LogSettingsFileName = isr.Core.Logging.ILogger.LoggingPlatform.BuildEntryAssemblySettingsFileName();
                return _LogSettingsFileName;
            }
            set => _LogSettingsFileName = value;
        }

        /// <summary>   The lazy instance. </summary>
        private static readonly Lazy<TraceLogger> LazyInstance = new( () => new TraceLogger() );

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static TraceLogger Instance => LazyInstance.Value;

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION"

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " LOGGING AND TRACE EVENT WRITER LOG LEVELS "

        private KeyValuePair<LogLevel, string> _LoggingLevelValueNamePair;

        /// <summary>
        /// Gets or sets the <see cref="LogLevel"/> value name pair for logging. This level determines
        /// the level of the <see cref="ILogger"/>.
        /// </summary>
        /// <value> The log level value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<LogLevel, string> LoggingLevelValueNamePair
        {
            get => this._LoggingLevelValueNamePair;

            set {
                if ( value.Key != this.LoggingLevelValueNamePair.Key )
                {
                    this._LoggingLevelValueNamePair = value;
                    this.MinimumLogLevel = value.Key;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ILogger"/> minimum runtime <see cref="LogLevel"/>.
        /// </summary>
        /// <remarks>
        /// Events with a log level lower than both the <see cref="MinimumLogLevel"/> and the initial
        /// Serilog Logger log level, as set when initializing the Serilog Logger, will be logged.
        /// </remarks>
        /// <value> The log level. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LogLevel MinimumLogLevel
        {
            get => isr.Core.Logging.ILogger.LoggingPlatform.Instance.RuntimeSerilogLevel;

            set {
                if ( value != this.MinimumLogLevel )
                {
                    isr.Core.Logging.ILogger.LoggingPlatform.Instance.RuntimeSerilogLevel = value;
                    this.NotifyPropertyChanged();
                    this.LoggingLevelValueNamePair = TraceLogger.ToLogLevelValueNamePair( value );
                    // obsolete: trace event type does not map to log level
                    // using only log level.
                    // this.LoggingTraceEventValueNamePair = TraceLogger.ToTraceEventValueNamePair( this.LoggingTraceEventType );
                }
            }
        }

        #endregion

        #region " LOG LEVELS: STATIC "

        private static List<LogLevel> _LogLevels;

        /// <summary>   Gets the log levels. </summary>
        /// <value> The log levels. </value>
        public static IEnumerable<LogLevel> LogLevels
        {
            get {
                if ( TraceLogger._LogLevels is null || !TraceLogger._LogLevels.Any() )
                {
                    TraceLogger._LogLevels = new List<LogLevel>() { LogLevel.Critical, LogLevel.Error, LogLevel.Warning, LogLevel.Information, LogLevel.Debug, LogLevel.Trace };
                }
                return TraceLogger._LogLevels;
            }
        }

        private static InvokingBindingList<LogLevel> _LogLevelBidningList;

        /// <summary>   Gets a list of log level bindings. </summary>
        /// <value> A list of log level bindings. </value>
        public static InvokingBindingList<LogLevel> LogLevelBindingList
        {
            get {
                if ( _LogLevelBidningList is null || !_LogLevelBidningList.Any() )
                {
                    _LogLevelBidningList = new InvokingBindingList<LogLevel>( TraceLogger.LogLevels.ToArray() );
                }

                return _LogLevelBidningList;
            }
        }

        private static InvokingBindingList<LogLevel> _ShowLevelBidningList;

        /// <summary>   Gets a list of Show level bindings. </summary>
        /// <value> A list of Show level bindings. </value>
        public static InvokingBindingList<LogLevel> ShowLevelBindingList
        {
            get {
                if ( _ShowLevelBidningList is null || !_ShowLevelBidningList.Any() )
                {
                    _ShowLevelBidningList = new InvokingBindingList<LogLevel>( TraceLogger.LogLevels.ToArray() );
                }
                return _ShowLevelBidningList;
            }
        }

        /// <summary>   The log level value name pairs. </summary>
        private static List<KeyValuePair<LogLevel, string>> _LogLevelValueNamePairs;

        /// <summary>   Gets the log level value name pairs. </summary>
        /// <value> The log level value name pairs. </value>
        public static IEnumerable<KeyValuePair<LogLevel, string>> LogLevelValueNamePairs
        {
            get {
                if ( TraceLogger._LogLevelValueNamePairs is null || !TraceLogger._LogLevelValueNamePairs.Any() )
                {
                    TraceLogger._LogLevelValueNamePairs = new List<KeyValuePair<LogLevel, string>>() { ToLogLevelValueNamePair( LogLevel.Critical ), ToLogLevelValueNamePair( LogLevel.Error ),
                                                                              ToLogLevelValueNamePair( LogLevel.Warning ), ToLogLevelValueNamePair( LogLevel.Information ),
                                                                              ToLogLevelValueNamePair( LogLevel.Debug ), ToLogLevelValueNamePair( LogLevel.Trace )  };
                }

                return TraceLogger._LogLevelValueNamePairs;
            }
        }

        /// <summary>   Converts a value to a log level value name pair. </summary>
        /// <remarks>   David, 2020-09-21. </remarks>
        /// <param name="value">    The value. </param>
        /// <returns>   Value as a KeyValuePair(Of LogLevel, String) </returns>
        public static KeyValuePair<LogLevel, string> ToLogLevelValueNamePair( LogLevel value )
        {
            return new KeyValuePair<LogLevel, string>( value, value.ToString() );
        }

        private static InvokingBindingList<KeyValuePair<LogLevel, string>> _LogLevelValueNamePairBindingList;

        /// <summary>   Gets a list of log level value name pair for bindings for selecting the log level. </summary>
        /// <value> A list of log level value name pair for bindings. </value>
        public static InvokingBindingList<KeyValuePair<LogLevel, string>> LogLevelValueNamePairBindingList
        {
            get {
                if ( TraceLogger._LogLevelValueNamePairBindingList is null || !TraceLogger._LogLevelValueNamePairBindingList.Any() )
                {
                    TraceLogger._LogLevelValueNamePairBindingList = new InvokingBindingList<KeyValuePair<LogLevel, string>>( LogLevelValueNamePairs.ToArray() );
                }
                return TraceLogger._LogLevelValueNamePairBindingList;
            }
        }

        private static InvokingBindingList<KeyValuePair<LogLevel, string>> _ShowLevelValueNamePairBindingList;

        /// <summary>   Gets a list of log level value name pair for bindings for selecting the show level. </summary>
        /// <value> A list of log level value name pair for bindings. </value>
        public static InvokingBindingList<KeyValuePair<LogLevel, string>> ShowLevelValueNamePairBindingList
        {
            get {
                if ( TraceLogger._ShowLevelValueNamePairBindingList is null || !TraceLogger._ShowLevelValueNamePairBindingList.Any() )
                {
                    TraceLogger._ShowLevelValueNamePairBindingList = new InvokingBindingList<KeyValuePair<LogLevel, string>>( TraceLogger.LogLevelValueNamePairs.ToArray() );
                }
                return TraceLogger._ShowLevelValueNamePairBindingList;
            }
        }

        #endregion

        #region " TRACE EVENT WRITER TRACE EVENT "

        private KeyValuePair<TraceEventType, string> _TraceEventWriterTraceEventValueNamePair;

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> value name pair for the global trace event
        /// writer. This level determines the level of all the
        /// <see cref="Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
        /// lower level.
        /// </summary>
        /// <value> The trace event writer trace event value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<TraceEventType, string> TraceEventWriterTraceEventValueNamePair
        {
            get => this._TraceEventWriterTraceEventValueNamePair;

            set {
                if ( value.Key != this.TraceEventWriterTraceEventValueNamePair.Key )
                {
                    this._TraceEventWriterTraceEventValueNamePair = value;
                    this.TraceEventWriterTraceEventType = value.Key;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private TraceEventType _TraceEventWriterTraceEventType;
        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> for the global trace event writer. This level
        /// determines the level of all the
        /// <see cref="Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
        /// lower level.
        /// </summary>
        /// <value> The type of the trace event writer trace event. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType TraceEventWriterTraceEventType
        {
            get => this._TraceEventWriterTraceEventType;
                // deprecated: Trace event types do not map to log levels.
                // TraceLogger.ToTraceEventType( this.TraceEventWriterLogLevel );

            set {
                if ( value != this.TraceEventWriterTraceEventType )
                {
                    // deprecated; Log levels do not map to trace
                    // event types. this.TraceEventWriterLogLevel = TraceLogger.ToLogLevel( value );
                    this._TraceEventWriterTraceEventType = value;
                    this.NotifyPropertyChanged();
                    this.TraceEventWriterTraceEventValueNamePair = TraceLogger.ToTraceEventValueNamePair( value );
                }
            }
        }

        #endregion

        #region " TRACE EVENTS: STATIC "

        private static List<TraceEventType> _TraceEventTypes;

        /// <summary>   Gets a list of types of the trace events. </summary>
        /// <value> A list of types of the trace events. </value>
        public static IEnumerable<TraceEventType> TraceEventTypes
        {
            get {
                if ( TraceLogger._TraceEventTypes is null || !TraceLogger._TraceEventTypes.Any() )
                {
                    TraceLogger._TraceEventTypes = new List<TraceEventType>() { TraceEventType.Critical, TraceEventType.Error, TraceEventType.Warning, TraceEventType.Information, TraceEventType.Verbose };
                }
                return TraceLogger._TraceEventTypes;
            }
        }

        private static InvokingBindingList<TraceEventType> _LogTraceEventTypeBidningList;

        /// <summary>   Gets a list of log trace event type bindings. </summary>
        /// <value> A list of log trace event type bindings. </value>
        public static InvokingBindingList<TraceEventType> LogTraceEventTypeBindingList
        {
            get {
                if ( _LogTraceEventTypeBidningList is null || !_LogTraceEventTypeBidningList.Any() )
                {
                    _LogTraceEventTypeBidningList = new InvokingBindingList<TraceEventType>( TraceLogger.TraceEventTypes.ToArray() );
                }

                return _LogTraceEventTypeBidningList;
            }
        }

        private static InvokingBindingList<TraceEventType> _ShowTraceEventTypeBidningList;

        /// <summary>   Gets a list of show trace event type bindings. </summary>
        /// <value> A list of show trace event type bindings. </value>
        public static InvokingBindingList<TraceEventType> ShowTraceEventTypeBindingList
        {
            get {
                if ( _ShowTraceEventTypeBidningList is null || !_ShowTraceEventTypeBidningList.Any() )
                {
                    _ShowTraceEventTypeBidningList = new InvokingBindingList<TraceEventType>( TraceLogger.TraceEventTypes.ToArray() );
                }

                return _ShowTraceEventTypeBidningList;
            }
        }

        private static List<KeyValuePair<TraceEventType, string>> _TraceEventValueNamePairs;

        /// <summary>   Gets the trace event value name pairs. </summary>
        /// <value> The trace event value name pairs. </value>
        public static IEnumerable<KeyValuePair<TraceEventType, string>> TraceEventValueNamePairs
        {
            get {
                if ( TraceLogger._TraceEventValueNamePairs is null || !TraceLogger._TraceEventValueNamePairs.Any() )
                {
                    TraceLogger._TraceEventValueNamePairs = new List<KeyValuePair<TraceEventType, string>>() { ToTraceEventValueNamePair( TraceEventType.Critical ),
                                                                                                   ToTraceEventValueNamePair( TraceEventType.Error ),
                                                                                                   ToTraceEventValueNamePair( TraceEventType.Warning ),
                                                                                                   ToTraceEventValueNamePair( TraceEventType.Information ),
                                                                                                   ToTraceEventValueNamePair( TraceEventType.Verbose ) };
                }

                return TraceLogger._TraceEventValueNamePairs;
            }
        }


        /// <summary> Converts a value to a trace event value name pair. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a KeyValuePair(Of TraceEventType, String) </returns>
        public static KeyValuePair<TraceEventType, string> ToTraceEventValueNamePair( TraceEventType value )
        {
            return new KeyValuePair<TraceEventType, string>( value, value.ToString() );
        }

        private static InvokingBindingList<KeyValuePair<TraceEventType, string>> _LogTraceEventValueNamePairBindingList;

        /// <summary>   Gets a list of trace event value name pair bindings for selecting the log trace event level. </summary>
        /// <value> A list of trace event value name pair bindings. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public static InvokingBindingList<KeyValuePair<TraceEventType, string>> LogTraceEventValueNamePairBindingList
        {
            get {
                if ( TraceLogger._LogTraceEventValueNamePairBindingList is null || !TraceLogger._LogTraceEventValueNamePairBindingList.Any() )
                {
                    TraceLogger._LogTraceEventValueNamePairBindingList = new InvokingBindingList<KeyValuePair<TraceEventType, string>>( TraceLogger.TraceEventValueNamePairs.ToArray() );
                }
                return TraceLogger._LogTraceEventValueNamePairBindingList;
            }
        }

        private static InvokingBindingList<KeyValuePair<TraceEventType, string>> _ShowTraceEventValueNamePairBindingList;

        /// <summary>   Gets a list of trace event value name pair bindings for selecting the Show trace event level. </summary>
        /// <value> A list of trace event value name pair bindings. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public static InvokingBindingList<KeyValuePair<TraceEventType, string>> ShowTraceEventValueNamePairBindingList
        {
            get {
                if ( _ShowTraceEventValueNamePairBindingList is null || !_ShowTraceEventValueNamePairBindingList.Any() )
                {
                    _ShowTraceEventValueNamePairBindingList = new InvokingBindingList<KeyValuePair<TraceEventType, string>>( TraceLogger.TraceEventValueNamePairs.ToArray() );
                }
                return _ShowTraceEventValueNamePairBindingList;
            }
        }

        #endregion

        #region " FILE NAME "

        /// <summary>   Gets the filename of the logger. </summary>
        /// <remarks> This gets updated after the first info goes into the file.</remarks>
        /// <value> The filename logger. </value>
        public static string FullLogFileName => Serilog.Sinks.File.Header.HeaderWriter.FullFileName;

        /// <summary> Opens log file. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The Process. </returns>
        public static (bool Success, string Details, Process process) OpenLogFile()
        {
            if ( string.IsNullOrEmpty( TraceLogger.FullLogFileName ) )
                return (false, "log file name not created", null);

            var fi = new System.IO.FileInfo( TraceLogger.FullLogFileName );
            if ( !fi.Exists )
                return (false, $"log file {fi.FullName} not found", null);

            string proc = "explorer.exe";
            string args = $"\"{TraceLogger.FullLogFileName}\"";
            return (true, string.Empty, Process.Start( proc, args ));
        }

        /// <summary> Opens folder location. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <returns> The Process. </returns>
        public static (bool Success, string Details, Process process) OpenFolderLocation()
        {
            if ( string.IsNullOrEmpty( TraceLogger.FullLogFileName ) )
                return (false, "log file name not created", null);

            var fi = new System.IO.FileInfo( TraceLogger.FullLogFileName );
            if ( !fi.Directory.Exists )
                return (false, $"log directory {fi.FullName} not found", null);

            string proc = "explorer.exe";
            string args = $"\"{fi.DirectoryName}\"";
            return (true, string.Empty, Process.Start( proc, args ));
        }

        #endregion

    }
}
