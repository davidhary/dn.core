using System.Runtime.CompilerServices;

using Microsoft.Extensions.Logging;

namespace isr.Core.TraceLog.ILoggerExtensions
{

    /// <summary>   A logger extensions methods. </summary>
    /// <remarks>   David, 2021-06-28. </remarks>
    public static class ILoggerExtensionsMethods
    {

        /// <summary>   An ILogger extension method that writes a log entry. </summary>
        /// <remarks>   David, 2021-03-13. </remarks>
        /// <param name="logger">   The logger to act on. </param>
        /// <param name="level">    The level. </param>
        /// <param name="message">  The message. </param>
        [System.CLSCompliant(false)]
        public static void WriteLogEntry( this ILogger logger, LogLevel level, string message )
        {
            switch ( level )
            {
                case LogLevel.Trace:
                    logger.LogTrace( message );
                    break;
                case LogLevel.Debug:
                    logger.LogDebug( message );
                    break;
                case LogLevel.Information:
                    logger.LogInformation( message );
                    break;
                case LogLevel.Warning:
                    logger.LogWarning( message );
                    break;
                case LogLevel.Error:
                    logger.LogError( message );
                    break;
                case LogLevel.Critical:
                    logger.LogCritical( message );
                    break;
                case LogLevel.None:
                    break;
                default:
                    logger.LogInformation( message );
                    break;
            }
        }

#if false
        // trace vent types do not map to log levels. Log levels must be used as these do map to the serilog log levels.


        /// <summary>   An ILogger extension method that writes a log entry. </summary>
        /// <remarks>   David, 2021-03-13. </remarks>
        /// <param name="logger">   The logger to act on. </param>
        /// <param name="level">    The level. </param>
        /// <param name="message">  The message. </param>
        public static void WriteLogEntry( this ILogger logger, System.Diagnostics.TraceEventType level, string message )
        {
            logger.WriteLogEntry( TraceLogger.ToLogLevel( level ), message );
        }
#endif
        /// <summary>   A Serilog.ILogger extension method that adds logging context. </summary>
        /// <remarks>   David, 2021-07-02. 
        /// https://stackoverflow.com/questions/29470863/serilog-output-enrich-all-messages-with-methodname-from-which-log-entry-was-ca
        /// </remarks>
        /// <param name="logger">           The logger to act on. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourceFilePath">   (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   A Serilog.ILogger. </returns>
        public static Serilog.ILogger Here( this Serilog.ILogger logger,
               [CallerMemberName] string memberName = "",
               [CallerFilePath] string sourceFilePath = "",
               [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return logger
                .ForContext( "MemberName", memberName )
                .ForContext( "FilePath", sourceFilePath )
                .ForContext( "LineNumber", sourceLineNumber );

        }

    }
}
