# About

isr.Core.TraceLog is a .Net library that encapsulates tracing and logging 
for higher level projects.

# How to Use

Log file handling within a message box control:
```
public partial class MessagesBox : TextBox
{
    private void RequestOpeningLogFile( object sender, EventArgs e )
    {
        var (success, details, _) = isr.Core.TraceLog.TraceLogger.OpenLogFile();
        if ( !success )
            this.Text = details;
    }

    private void RequestOpeningLogFolder( object sender, EventArgs e )
    {
        var (success, details, _) = isr.Core.TraceLog.TraceLogger.OpenFolderLocation();
        if ( !success )
            this.Text = details;
    }
}
```

Emitting a log on exit and flushing the log queue:
```
protected override void OnShutdown()
{
    try
    {
        TraceLog.TraceLogger.LogDebug( "Saving assembly settings" );
        AppSettings.Instance.SaveSettings();
        isr.Core.TraceLog.TraceLogger.CloseAndFlush();
    }
    catch
    {
    }
    finally
    {
    }
}
```

Emitting an error log within a try catch clause:
```
private void ShowDashboard( )
{
    string activity = string.Empty;
    try
    {
        activity = $"attempting to construct the Dashboard form";
    }
    catch ( Exception ex )
    {
        string message = $"An error occurred while {activity}";
        _ = TraceLog.TraceLogger.LogError( ex, message );
    }

}
```

Emitting a log message with caller information:
```
private int _MessageNumber;
private void SendLogMessageButton_Click( object sender, EventArgs e )
{
    this._MessageNumber += 1;
    Microsoft.Extensions.Logging.LogLevel logLevel = this.EventLevelComboBox.SelectedEnumValue( Microsoft.Extensions.Logging.LogLevel.Trace );
    string message = $"{logLevel} Message #{this._MessageNumber} @ {DateTime.Now}";
    _ = isr.Core.TraceLog.TraceLogger.LogCallerMessage( logLevel, message );
}
```

# Key Features

* A global class for tracing and logging.

# Main Types

The main types provided by this library are:

* _ILoggerExtensions_ A logger extensions methods..
* _TraceLogger_ A trace logger for emitting messages to file, trace and debug listeners.
* _InvokingBindingList_ A cross-thread safe binding list.
* _ViewModelLoggerBase_ Defines the contract that must be implemented by View Models 
capable of logging and tracing.

# Feedback

isr.Core.TraceLog is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Core Framework Repository].

[Core Framework Repository]: https://bitbucket.org/davidhary/dn.core

