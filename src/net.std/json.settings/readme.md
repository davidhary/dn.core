# About

isr.Json.Settings is a .Net library supporting configuration settings using Json.

# How to Use

This example is implemented in the [Ohmni Core Repository] MS Test project 
isr.Ohmni.Core.MSTest:

The settings consists of four settings classes:

```
namespace isr.Ohmni.Core.MSTest
{
    /// <summary>   A settings. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    [isr.Json.SettingsSection( nameof( Settings ) )]
    internal class Settings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public Settings() : base( System.Reflection.Assembly.GetAssembly( typeof( Settings ) ) )
        { }

        private bool _CheckUnpublishedMessageLogFileSize;

        public bool CheckUnpublishedMessageLogFileSize
        {
            get => this._CheckUnpublishedMessageLogFileSize;
            set {
                if ( !bool.Equals( this.CheckUnpublishedMessageLogFileSize, value ) )
                {
                    this._CheckUnpublishedMessageLogFileSize = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _Dummy;

        /// <summary>   Gets or sets the dummy. </summary>
        /// <value> The dummy. </value>
        public string Dummy
        {
            get => this._Dummy;
            set {
                if ( !string.Equals( value, this.Dummy ) )
                {
                    this._Dummy = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

    }
}

```
using System;

namespace isr.Ohmni.Core.MSTest
{

    /// <summary>   A test site settings. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    [isr.Json.SettingsSection( nameof( TestSiteSettings ) )]
    public class TestSiteSettings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public TestSiteSettings() : base( System.Reflection.Assembly.GetAssembly( typeof( TestSiteSettings ) ) )
        { }

        #region " TEST SITE CONFIGURATION "

        private bool _Verbose;
        /// <summary>   Gets or sets a value indicating whether test reporting is verbose. </summary>
        /// <value> True if verbose, false if not. </value>
        public bool Verbose
        {
            get => this._Verbose;
            set {
                if ( !bool.Equals( value, this.Verbose ) )
                {
                    this._Verbose = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _Enabled;
        /// <summary>   Gets or sets a value indicating whether testing is enabled. </summary>
        /// <value> True if enabled, false if not. </value>
        public bool Enabled
        {
            get => this._Enabled;
            set {
                if ( !bool.Equals( value, this.Enabled ) )
                {
                    this._Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _All;
        /// <summary>   Gets or sets a value indicating whether all testing is to be ran. </summary>
        /// <value> True if All, false if not. </value>
        public bool All
        {
            get => this._All;
            set {
                if ( !bool.Equals( value, this.All ) )
                {
                    this._All = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " TEST SITE LOCATION IDENTIFICATION "

        private string _IPv4Prefixes;
        /// <summary>   Gets or sets the candidate IPv4 prefixes for this location. </summary>
        /// <value> The IPv4 prefixes. </value>
        public string IPv4Prefixes
        {
            get => this._IPv4Prefixes;
            set {
                if ( !String.Equals( value, this.IPv4Prefixes ) )
                {
                    this._IPv4Prefixes = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _TimeZones;
        /// <summary> Gets or sets the candidate time zones of this location. </summary>
        /// <value> The candidate time zones of the test site. </value>
        public string TimeZones
        {
            get => this._TimeZones;
            set {
                if ( !String.Equals( value, this.TimeZones ) )
                {
                    this._TimeZones = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _TimeZoneOffsets;
        /// <summary> Gets or sets the candidate time zone offsets of this location. </summary>
        /// <value> The time zone offsets. </value>
        public string TimeZoneOffsets
        {
            get => this._TimeZoneOffsets;
            set {
                if ( !String.Equals( value, this.TimeZoneOffsets ) )
                {
                    this._TimeZoneOffsets = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The time zone. </summary>
        private string _TimeZone;

        /// <summary> Gets the time zone of the test site. </summary>
        /// <value> The time zone of the test site. </value>
        public string TimeZone()
        {
            if ( string.IsNullOrWhiteSpace( this._TimeZone ) )
            {
                this._TimeZone = this.TimeZones.Split('|')[this.HostInfoIndex()];
            }
            return this._TimeZone;
        }

        /// <summary> The time zone offset. </summary>
        private double _TimeZoneOffset = double.MinValue;

        /// <summary> Gets the time zone offset of the test site. </summary>
        /// <value> The time zone offset of the test site. </value>
        public double TimeZoneOffset()
        {
            if ( this._TimeZoneOffset == double.MinValue )
            {
                this._TimeZoneOffset = double.Parse( this.TimeZoneOffsets.Split( '|')[this.HostInfoIndex()] );
            }
            return this._TimeZoneOffset;
        }

        /// <summary> Gets the host name of the test site. </summary>
        /// <value> The host name of the test site. </value>
        public static string HostName()
        {
            return System.Net.Dns.GetHostName();
        }

        /// <summary> The host address. </summary>
        private System.Net.IPAddress _HostAddress;

        /// <summary> Gets the IP address of the test site. </summary>
        /// <value> The IP address of the test site. </value>
        public System.Net.IPAddress HostAddress()
        {
            if ( this._HostAddress is null )
            {
                foreach ( System.Net.IPAddress value in System.Net.Dns.GetHostEntry( HostName() ).AddressList )
                {
                    if ( value.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork )
                    {
                        this._HostAddress = value;
                        break;
                    }
                }
            }
            return this._HostAddress;
        }

        /// <summary> Zero-based index of the host information. </summary>
        private int _HostInfoIndex = -1;

        /// <summary> Gets the index into the host information strings. </summary>
        /// <value> The index into the host information strings. </value>
        public int HostInfoIndex()
        {
            if ( this._HostInfoIndex < 0 )
            {
                string ip = this.HostAddress().ToString();
                int i = -1;
                foreach ( string value in this.IPv4Prefixes.Split( '|' ) )
                {
                    i += 1;
                    if ( ip.StartsWith( value, StringComparison.OrdinalIgnoreCase ) )
                    {
                        this._HostInfoIndex = i;
                        break;
                    }
                }
            }

            return this._HostInfoIndex;
        }

        #endregion

    }
}
```
using System;
using System.Collections.Generic;

namespace isr.Ohmni.Core.MSTest
{
    /// <summary>   A case code power tests settings. </summary>
    /// <remarks>   David, 2021-08-17. </remarks>
    [isr.Json.SettingsSection( nameof( CaseCodePowerTestsSettings ) )]
    public class CaseCodePowerTestsSettings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public CaseCodePowerTestsSettings() : base( System.Reflection.Assembly.GetAssembly( typeof( CaseCodePowerTestsSettings ) ) )
        { }

        #region " TEST SITE CONFIGURATION "

        private bool _Verbose;
        /// <summary>   Gets or sets a value indicating whether test reporting is verbose. </summary>
        /// <value> True if verbose, false if not. </value>
        public bool Verbose
        {
            get => this._Verbose;
            set {
                if ( !bool.Equals( value, this.Verbose ) )
                {
                    this._Verbose = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _Enabled;
        /// <summary>   Gets or sets a value indicating whether testing is enabled. </summary>
        /// <value> True if enabled, false if not. </value>
        public bool Enabled
        {
            get => this._Enabled;
            set {
                if ( !bool.Equals( value, this.Enabled ) )
                {
                    this._Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _All;
        /// <summary>   Gets or sets a value indicating whether all testing is to be ran. </summary>
        /// <value> True if All, false if not. </value>
        public bool All
        {
            get => this._All;
            set {
                if ( !bool.Equals( value, this.All ) )
                {
                    this._All = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion
		
		#region " CASE CODE POWERS "

        private string _FileName = "R:\\Ohmni\\Taper\\Files\\CaseCodePower.csv";
        /// <summary>   Gets or sets the filename of the file. </summary>
        /// <value> The name of the file. </value>
        public string FileName
        {
            get => this._FileName;
            set {
                if ( !string.Equals( value, this.FileName ) )
                {
                    this._FileName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private char _Delimiter = ',';
        /// <summary>   Gets or sets the delimiter. </summary>
        /// <value> The delimiter. </value>
        public char Delimiter
        {
            get => this._Delimiter;
            set {
                if ( !char.Equals( value, this.Delimiter ) )
                {
                    this._Delimiter = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _CaseCodes = "1206,0805,0603,0402";
        /// <summary>   Gets or sets the case codes. </summary>
        /// <value> The case codes. </value>
        public string CaseCodes
        {
            get => this._CaseCodes;
            set {
                if ( !string.Equals( value, this.CaseCodes ) )
                {
                    this._CaseCodes = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _Powers = "0.25,0.25,0.1,0.05";
        /// <summary>   Gets or sets the powers. </summary>
        /// <value> The powers. </value>
        public string Powers
        {
            get => this._Powers;
            set {
                if ( !String.Equals( value, this.Powers ) )
                {
                    this._Powers = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private Dictionary<String, Double> _CaseCodePowers;

        /// <summary>   Gets the case code powers. </summary>
        /// <value> The case code powers. </value>
        public Dictionary<String, Double> CaseCodePowers
        {
            get
            {
                if ( this._CaseCodePowers is not object )
                {
                    this._CaseCodePowers = new();
                    Queue<string> codes = new( this.CaseCodes.Split( this.Delimiter ) );
                    Queue<string> powers = new( this.Powers.Split( this.Delimiter ) );
                    while ( codes.Count > 0 )
                    {
                        this._CaseCodePowers.Add( codes.Dequeue(), double.Parse( powers.Dequeue() ) );
                    }
                }
                return this._CaseCodePowers;
            }
        }


        #endregion

    }

}

```

```
using System;
using System.Collections.Generic;

namespace isr.Ohmni.Core.MSTest
{
    /// <summary>   A part historical reference tests settings. </summary>
    /// <remarks>   David, 2021-08-17. </remarks>
    [isr.Json.SettingsSection( nameof( PartHistoricalReference ) )]
    public class PartHistoricalReferenceTestsSettings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public PartHistoricalReferenceTestsSettings() : base( System.Reflection.Assembly.GetAssembly( typeof( PartHistoricalReferenceTestsSettings ) ) )
        { }

        #region " TEST SITE CONFIGURATION "

        private bool _Verbose;
        /// <summary>   Gets or sets a value indicating whether test reporting is verbose. </summary>
        /// <value> True if verbose, false if not. </value>
        public bool Verbose
        {
            get => this._Verbose;
            set {
                if ( !bool.Equals( value, this.Verbose ) )
                {
                    this._Verbose = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _Enabled;
        /// <summary>   Gets or sets a value indicating whether testing is enabled. </summary>
        /// <value> True if enabled, false if not. </value>
        public bool Enabled
        {
            get => this._Enabled;
            set {
                if ( !bool.Equals( value, this.Enabled ) )
                {
                    this._Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _All;
        /// <summary>   Gets or sets a value indicating whether all testing is to be ran. </summary>
        /// <value> True if All, false if not. </value>
        public bool All
        {
            get => this._All;
            set {
                if ( !bool.Equals( value, this.All ) )
                {
                    this._All = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion
		
		#region " PART HISTORICAL REFERENCE "

        private string _FileName = "R:\\Ohmni\\Taper\\Files\\PartHistoricalReference.csv";
        /// <summary>   Gets or sets the filename of the file. </summary>
        /// <value> The name of the file. </value>
        public string FileName
        {
            get => this._FileName;
            set {
                if ( !string.Equals( value, this.FileName ) )
                {
                    this._FileName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private char _Delimiter = ',';
        /// <summary>   Gets or sets the delimiter. </summary>
        /// <value> The delimiter. </value>
        public char Delimiter
        {
            get => this._Delimiter;
            set {
                if ( !char.Equals( value, this.Delimiter ) )
                {
                    this._Delimiter = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _Parts = "PFC-W1206PC-03-2003-B,PFC-W1206PC-03-100R-B";
        /// <summary>   Gets or sets the parts. </summary>
        /// <value> The parts. </value>
        public string Parts
        {
            get => this._Parts;
            set {
                if ( !string.Equals( value, this.Parts ) )
                {
                    this._Parts = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _Dates = "2021-04-28,2021-05-01";
        /// <summary>   Gets or sets the dates. </summary>
        /// <value> The dates. </value>
        public string Dates
        {
            get => this._Dates;
            set {
                if ( !String.Equals( value, this.Dates ) )
                {
                    this._Dates = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private Dictionary<String, DateTime> _PartHistoricalReferenceDates;

        /// <summary>   Gets a list of dates of the part historical references. </summary>
        /// <value> A list of dates of the part historical references. </value>
        public Dictionary<String, DateTime> PartHistoricalReferenceDates
        {
            get
            {
                if ( this._PartHistoricalReferenceDates is not object )
                {
                    this._PartHistoricalReferenceDates = new();
                    Queue<string> names = new( this.Parts.Split( this.Delimiter ) );
                    Queue<string> dates = new( this.Dates.Split( this.Delimiter ) );
                    while ( names.Count > 0 )
                    {
                        this._PartHistoricalReferenceDates.Add( names.Dequeue(), DateTime.Parse( dates.Dequeue() ).Date );
                    }
                }
                return this._PartHistoricalReferenceDates;
            }
        }


        #endregion

    }

}

```

```
using System;

namespace isr.Ohmni.Core.MSTest
{
    /// <summary>   An application settings. </summary>
    /// <remarks>   David, 2021-01-28. </remarks>
    public class AppSettings
    {

        #region " CONSTRUCTION "

        private AppSettings()
        {
            this.Settings = new ();
            this.TestSiteSettings = new ();
            this.CaseCodePowerTestsSettings = new();
            this.PartHistoricalReferenceTestsSettings = new();
            this.ReadSettings();
        }

        /// <summary>   The lazy instance. </summary>
        private static readonly Lazy<AppSettings> LazyInstance = new( () => new AppSettings() );

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static AppSettings Instance => LazyInstance.Value;

        #endregion

        /// <summary>   Restore settings. </summary>
        /// <remarks>   David, 2021-09-14. </remarks>
        public void RestoreSettings()
        {
            this.Settings.RestoreSettings();
            this.ReadSettings();
        }

        /// <summary>   Reads the settings. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        public void ReadSettings()
        {
			this.Settings.ReadSettings();
			this.TestSiteSettings.ReadSettings();
			this.CaseCodePowerTestsSettings.ReadSettings();
			this.PartHistoricalReferenceTestsSettings.ReadSettings();
        }

        /// <summary>   Saves the settings. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public void SaveSettings()
        {
            this.Settings.SaveSettings( new object[] { this.Settings, this.TestSiteSettings, this.CaseCodePowerTestsSettings, this.PartHistoricalReferenceTestsSettings } );
        }

        /// <summary>   Gets the full name of the application settings file. </summary>
        /// <value> The full name of the application settings file. </value>
        public string AppSettingsFullFileName => this.Settings.FullFileName;

        /// <summary>   Gets or sets options for controlling the operation. </summary>
        /// <remarks>
        /// Note that the name of the field must match  class name or the name of the section as defined
        /// in the class <see cref="isr.Json.SettingsSectionAttribute"/> or.
        /// </remarks>
        /// <value> The settings. </value>
        internal Settings Settings { get; }

        /// <summary>   Gets or sets the test site settings. </summary>
        /// <remarks>
        /// Note that the name of the field must match  class name or the name of the section as defined
        /// in the class <see cref="isr.Json.SettingsSectionAttribute"/> or.
        /// </remarks>
        /// <value> The test site settings. </value>
        public TestSiteSettings TestSiteSettings { get; }

        /// <summary>   Gets the case code power tests settings. </summary>
        /// <value> The case code power tests settings. </value>
        internal CaseCodePowerTestsSettings CaseCodePowerTestsSettings { get; }

        /// <summary>   Gets the part historical reference tests settings. </summary>
        /// <value> The part historical reference tests settings. </value>
        internal PartHistoricalReferenceTestsSettings PartHistoricalReferenceTestsSettings { get; }

    }

}
```

and a common singleton settings classes serving the individual settings:

```
TBD
```


The isr.Ohmni.Core.MSTest.Settings.json includes the following data:
```
{
  "Settings": {
    "CheckUnpublishedMessageLogFileSize": false,
    "Dummy": "Dummy"
  },

  "TestSiteSettings": {
    "Exists": true,
    "Verbose": true,
    "All": true,
    "IPv4Prefixes": "192.168|10.1",
    "TimeZones": "Pacific Standard Time|Central Standard Time",
    "TimeZoneOffsets": "-8|-6"
  },

  "CaseCodePowerTestsSettings": {
    "Exists": true,
    "Verbose": true,
    "All": true,
    "Delimiter": ",",
    "FileName": "R:\\Ohmni\\Taper\\Files\\CaseCodePower.csv",
    "CaseCodes": "1206,0805,0603,0402",
    "CaseCodePowers": "0.25,0.25,0.1,0.05"
  },

  "PartHistoricalReferenceTestsSettings": {
    "Exists": true,
    "Verbose": true,
    "All": true,
    "FileName": "R:\\Ohmni\\Taper\\Files\\PartHistoricalReference.csv",
    "Delimiter": ",",
    "Parts": "PFC-W1206PC-03-2003-B,PFC-W1206PC-03-100R-B",
    "Dates": "2021-04-28,2021-05-01"
  }

}
```


# Key Features

* Provides Json-based configuration settings.

# Main Types

The main types provided by this library are:

* _JsonSettingsBase_ a base class for consuming Json configuration settings.

# Feedback

isr.Json.Settings is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Core Framework Repository].

[Core Framework Repository]: https://bitbucket.org/davidhary/dn.core
[Ohmni Core Repository]: https://bitbucket.org/davidhary/dn.Ohmni.core
