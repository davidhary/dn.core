using System;
using System.IO;

namespace isr.Json
{
    /// <summary>   An application settings base class with multiple sections. </summary>
    /// <remarks>   David, 2021-01-28. </remarks>
    public abstract class JsonAppSettingsBase
    {

        #region " CONSTRUCTION "

        /// <summary>   Specialized default constructor for use only by derived class. </summary>
        /// <remarks>   David, 2022-03-24. </remarks>
        protected JsonAppSettingsBase()
        {
            this._SettingsList = new System.Collections.Generic.List<JsonSettingsBase>();
        }

        #endregion

        #region " SETTINGS LIST "

        private readonly System.Collections.Generic.List<JsonSettingsBase> _SettingsList;

        /// <summary>   Adds the settings section. </summary>
        /// <remarks>   David, 2022-03-24. </remarks>
        /// <param name="jsonSettings"> The JSON settings. </param>
        protected void AddSettingsSection( JsonSettingsBase jsonSettings )
        {
            this._SettingsList.Add( jsonSettings );
            if ( this._PrimarySettings is not object)
                this._PrimarySettings = jsonSettings;
            this.AppContextSettingsFullFileName = jsonSettings.AppContextSettingsFullFileName;
            this.AppDataSettingsFullFileName = jsonSettings.AppDataSettingsFullFileName;
        }

        #endregion

        #region " SETTINGS FILES "

        /// <summary>
        /// Gets or sets the filename of the application context settings full file.
        /// </summary>
        /// <value> The filename of the application context settings full file. </value>
        public string AppContextSettingsFullFileName { get; set; }

        /// <summary>   Gets or sets the filename of the application data settings full file. </summary>
        /// <value> The filename of the application data settings full file. </value>
        public string AppDataSettingsFullFileName { get; set; }

        #endregion

        #region " SETTINGS MANAGEMENT "

        private JsonSettingsBase _PrimarySettings;

        /// <summary>   Restore settings. </summary>
        public void RestoreSettings()
        {
            foreach ( JsonSettingsBase jsonSettings in this._SettingsList )
            {
                if ( jsonSettings != null )
                    jsonSettings.RestoreSettings();
            }
        }

        /// <summary>   Reads the settings. </summary>
        public void ReadSettings()
        {
            foreach ( JsonSettingsBase jsonSettings in this._SettingsList )
            {
                if ( jsonSettings != null )
                    jsonSettings.ReadSettings();
            }
        }

        /// <summary>   Saves the settings. </summary>
        public void SaveSettings()
        {
            this._PrimarySettings.SaveSettings( this._SettingsList.ToArray() );
        }

        /// <summary>
        /// Initializes the program setting by copying an existing application context settings to the
        /// application data settings or writing a new application data settings if no application
        /// context settings is provided.
        /// </summary>
        /// <remarks>   David, 2021-09-14. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <exception cref="FileNotFoundException">        Thrown when the requested file is not
        ///                                                 present. </exception>
        /// <param name="applicationContextSettingsMustExist">  (Optional) True if application context
        ///                                                     settings must exist. </param>
        public void Initialize( bool applicationContextSettingsMustExist = false )
        {
            if ( string.IsNullOrEmpty( this.AppContextSettingsFullFileName ) )
            {
                throw new InvalidOperationException( "Application context settings file name not specified" );
            }
            else if ( applicationContextSettingsMustExist && !isr.Json.JsonSettingsBase.Exists( this.AppContextSettingsFullFileName ) )
            {
                throw new FileNotFoundException( $"Application context settings file not found", this.AppContextSettingsFullFileName );
            }
            else if ( isr.Json.JsonSettingsBase.Exists( this.AppContextSettingsFullFileName ) && !isr.Json.JsonSettingsBase.Exists( this.AppDataSettingsFullFileName ) )
            {
                // if this is a new settings, the context settings is copied to the application data folder.
                this.RestoreSettings();
            }
            else if ( !isr.Json.JsonSettingsBase.Exists( this.AppDataSettingsFullFileName ) )
            {
                // if this is a new settings and we have no context file, the context settings is copied to the application data folder.
                this.SaveSettings();
            }

            if ( !isr.Json.JsonSettingsBase.Exists( this.AppContextSettingsFullFileName ) && !isr.Json.JsonSettingsBase.IsRestrictedFolder( this.AppContextSettingsFullFileName ) )
            {
                // if the context settings does not exists but the folder is writable, we can create the file.
                isr.Json.JsonSettingsBase.SaveSettings( this.AppContextSettingsFullFileName, this._SettingsList.ToArray() );
            }

            this.Restorable = isr.Json.JsonSettingsBase.Exists( this.AppContextSettingsFullFileName );

            foreach ( JsonSettingsBase jsonSettings in this._SettingsList )
            {
                if ( jsonSettings != null )
                    jsonSettings.Restorable = this.Restorable;
            }

            this.ReadSettings();
        }

        /// <summary>   Gets or sets a value indicating whether this object is restorable. </summary>
        /// <value> True if restorable, false if not. </value>
        public bool Restorable { get; set; } 

        #endregion

    }

}
