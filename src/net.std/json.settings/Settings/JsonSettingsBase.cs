using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace isr.Json
{

    /// <summary>   A JSON settings base. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    public abstract class JsonSettingsBase : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Specialized constructor for use only by derived class. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        /// <param name="callingAssembly">  The calling assembly. </param>
        /// <param name="scope">            (Optional) The scope. </param>
        protected JsonSettingsBase( Assembly callingAssembly, SettingsScope scope = SettingsScope.ApplicationScope )
        {
            this.Scope = scope;
            this.AppContextSettingsFullFileName = JsonSettingsBase.BuildAppContextSettingsFullFileName( callingAssembly );
            this.AppDataSettingsFullFileName = JsonSettingsBase.BuildApplicationDataSettingsFullFileName( callingAssembly, scope );
            this.Restorable = isr.Json.JsonSettingsBase.Exists( this.AppContextSettingsFullFileName );
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-03-02. </remarks>
        /// <param name="callingAssembly">  The calling assembly. </param>
        /// <param name="sectionName">      Name of the section. </param>
        /// <param name="scope">            (Optional) The scope. </param>
        protected JsonSettingsBase( Assembly callingAssembly, string sectionName, SettingsScope scope = SettingsScope.ApplicationScope ) : this( callingAssembly, scope )
        {
            this._SectionName = sectionName;
        }

        /// <summary>   Specialized constructor for use only by derived class. </summary>
        /// <remarks>
        /// The specified file name is used for both reading, writing and resetting the settings. 
        /// Settings are not saved to the application data folders.
        /// </remarks>
        /// <param name="fileName"> full name of the settings file. </param>
        /// <param name="scope">    (Optional) The scope. </param>
        protected JsonSettingsBase( string fileName, SettingsScope scope = SettingsScope.ApplicationScope )
        {
            this.Scope = scope;
            this.AppContextSettingsFullFileName = fileName;
            this.AppDataSettingsFullFileName = string.Empty;
            this.Restorable = isr.Json.JsonSettingsBase.Exists( this.AppContextSettingsFullFileName );
        }

        /// <summary>   Specialized constructor for use only by derived class. </summary>
        /// <remarks>
        /// The specified file name is used for both reading, writing and resetting the settings. 
        /// Settings are not saved to the application data folders.
        /// </remarks>
        /// <param name="fileName"> full name of the settings file. </param>
        /// <param name="sectionName">  Name of the section. </param>
        /// <param name="scope">        (Optional) The scope. </param>
        protected JsonSettingsBase( string fileName, string sectionName, SettingsScope scope = SettingsScope.ApplicationScope ) : this( fileName, scope )
        {
            this._SectionName = sectionName;
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " SECTION "

        private string _SectionName;

        /// <summary>   Gets the name of the section. </summary>
        /// <value> The name of the section. </value>
        [JsonIgnore]
        [Description( "The name of the settings section" )]
        public string SectionName
        {
            get {
                if ( string.IsNullOrEmpty( this._SectionName ) )
                {
                    var attribe = ( SettingsSectionAttribute ) this.GetType().GetCustomAttributes( typeof( SettingsSectionAttribute ), true ).FirstOrDefault();
                    this._SectionName = attribe is object ? attribe.SectionName : this.GetType().Name;
                }
                return this._SectionName;
            }
        }

        #endregion

        #region " READ; RESTORE; INITIALIZE "

        /// <summary>   Reads the settings. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        public void ReadSettings()
        {
            this.ReadSettings( this.SectionName );
        }

        /// <summary>   Reads the settings. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="settingsSectionName">  Name of the settings section. </param>
        public void ReadSettings( string settingsSectionName )
        {
            this.ReadSettings( settingsSectionName, this.SelectFileName( FileNameScope.Reading ) );
        }

        /// <summary>   Reads context settings. </summary>
        /// <remarks>   David, 2021-09-14. </remarks>
        /// <param name="settingsSectionName">  Name of the settings section. </param>
        public void ReadContextSettings( string settingsSectionName )
        {
            this.ReadSettings( settingsSectionName, this.SelectFileName( FileNameScope.Restoring ) );
        }

        /// <summary>   Reads the settings. </summary>
        /// <remarks>   David, 2021-09-14. </remarks>
        /// <param name="settingsSectionName">  Name of the settings section. </param>
        /// <param name="fileName">             full name of the settings file. </param>
        public void ReadSettings( string settingsSectionName, string fileName )
        {
            if ( JsonSettingsBase.Exists( fileName ) )
            {
                var config = this.BuildConfig( fileName );
                config.GetSection( settingsSectionName ).Bind( this );
            }
            else
            {
                throw new FileNotFoundException( $"Settings file '{fileName}' not found for reading", fileName );
            }
        }


        /// <summary>   Reads section list. </summary>
        /// <remarks>   David, 2021-09-14. </remarks>
        /// <exception cref="FileNotFoundException">    Thrown when the requested file is not present. </exception>
        /// <param name="settingSectionName">   Name of the setting section. </param>
        /// <param name="listName">             Name of the list. </param>
        /// <param name="list">                 The list. </param>
        /// <param name="fileName">             full name of the settings file. </param>
        public void ReadSectionList( string settingSectionName, string listName, object list, string fileName )
        {
            if ( JsonSettingsBase.Exists( fileName ) )
            {
                var config = this.BuildConfig( fileName );
                config.GetSection( $"{settingSectionName}{listName}" ).Bind( list );
            }
            else
            {
                throw new FileNotFoundException( $"Settings file '{fileName}'not found for reading", fileName );
            }
        }


        /// <summary>   Gets or sets a value indicating whether this settings is restorable
        ///             from the application context. </summary>
        /// <remarks> This value is initialized as true or to false if initializing without an 
        ///           application context file. </remarks>
        /// <value> True if restorable, false if not. </value>
        [JsonIgnore]
        public bool Restorable { get; set; } = true;

        /// <summary>   Restore settings from the application context file name if the settings is restorable. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        public void RestoreSettings()
        {
            if ( !this.Restorable )
            {
                return;
            }
            else if ( string.IsNullOrEmpty( this.AppContextSettingsFullFileName ) )
            {
                throw new InvalidOperationException( "Application settings file name not specified" );
            }
            else if ( !System.IO.File.Exists( this.AppContextSettingsFullFileName ) )
            {
                throw new FileNotFoundException( $"Application setting file {this.AppContextSettingsFullFileName} not found", this.AppContextSettingsFullFileName );
            }
            else
            {
                // if the application data file does not exist, we must make a copy from the context file.
                // restore the settings by copying the application context settings file.
                new FileInfo( this.AppDataSettingsFullFileName ).Directory.Create();
                File.Copy( this.AppContextSettingsFullFileName, this.AppDataSettingsFullFileName, true );
                this.ReadSettings();
            }
        }

        /// <summary>   Builds the configuration. </summary>
        /// <remarks>   David, 2021-01-28. </remarks>
        /// <exception cref="System.IO.FileNotFoundException">    Thrown when the requested file is not present. </exception>
        /// <param name="fullFileName"> full file name of the file. </param>
        /// <returns>   An IConfigurationRoot. </returns>
        private IConfigurationRoot BuildConfig( string fullFileName )
        {
            FileInfo fi = new( fullFileName );
            if ( !fi.Exists )
                throw new FileNotFoundException( $"Settings file {fi.FullName} not found", fi.FullName );

            var config = new ConfigurationBuilder()
                .SetBasePath( fi.DirectoryName )
                .AddJsonFile( fi.Name, false, true ).Build();
            return config;
        }

        #endregion

        #region " INITIALIZE "

        /// <summary>
        /// Initializes the program setting by copying an existing application context settings to the
        /// application data settings or writing a new application data settings if no application
        /// context settings is provided.
        /// </summary>
        /// <remarks>   David, 2021-09-14. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <exception cref="FileNotFoundException">        Thrown when the requested file is not
        ///                                                 present. </exception>
        /// <param name="singleSettings">                       a single settings object. </param>
        /// <param name="applicationContextSettingsMustExist">  (Optional) True if application context
        ///                                                     settings must exist. </param>
        public void Initialize( object[] singleSettings, bool applicationContextSettingsMustExist = false )
        {
            if ( string.IsNullOrEmpty( this.AppContextSettingsFullFileName ) )
            {
                throw new InvalidOperationException( "Application context settings file name not specified" );
            }
            else if ( applicationContextSettingsMustExist && !isr.Json.JsonSettingsBase.Exists( this.AppContextSettingsFullFileName ) )
            {
                throw new FileNotFoundException( $"Application context settings file not found" , this.AppContextSettingsFullFileName );
            }
            else if ( isr.Json.JsonSettingsBase.Exists( this.AppContextSettingsFullFileName ) && !isr.Json.JsonSettingsBase.Exists( this.AppDataSettingsFullFileName ) )
            {
                // if this is a new settings, the context settings is copied to the application data folder.
                this.RestoreSettings();
            }
            else if ( !isr.Json.JsonSettingsBase.Exists( this.AppDataSettingsFullFileName ) )
            {
                // if this is a new settings and we have no context file, the context settings is copied to the application data folder.
                this.SaveSettings( singleSettings );
            }

            if ( !isr.Json.JsonSettingsBase.Exists( this.AppContextSettingsFullFileName ) &&  !isr.Json.JsonSettingsBase.IsRestrictedFolder( this.AppContextSettingsFullFileName ) )
            {
                // if the context settings does not exists but the folder is writable, we can create the file.
                isr.Json.JsonSettingsBase.SaveSettings( this.AppContextSettingsFullFileName , singleSettings );
            }

            this.Restorable = isr.Json.JsonSettingsBase.Exists( this.AppContextSettingsFullFileName );
            this.ReadSettings();
        }

        #endregion

        #region " SAVE "

        /// <summary>   Saves the settings. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="allSettings">  all settings. </param>
        public void SaveSettings( object[] allSettings )
        {
            JsonSettingsBase.SaveSettings( this.SelectFileName( FileNameScope.Writing ), allSettings );
        }

        /// <summary>   Saves all settings. </summary>
        /// <remarks>
        /// David, 2021-02-01. <para>
        /// https://www.c-sharpcorner.com/article/object-to-json-converter/</para>
        /// </remarks>
        /// <param name="fileName">     Filename of the file. </param>
        /// <param name="allSettings">  all settings. </param>
        public static void SaveSettings( string fileName, object[] allSettings )
        {
            // default to serialize enums as strings.
            JsonConvert.DefaultSettings = (() => {
                var settings = new JsonSerializerSettings();
                settings.Converters.Add( new StringEnumConverter() );
                return settings;
            });

            StringBuilder jsonBuilder = new();
            _ = jsonBuilder.Append( '{' );
            _ = jsonBuilder.AppendLine();
            int itemCount = allSettings.Length;
            int itemNumber = 0;
            foreach ( var data in allSettings )
            {
                itemNumber += 1;
                _ = jsonBuilder.Append( $"  \"{(( JsonSettingsBase ) data).SectionName}\": {JsonConvert.SerializeObject( data, Formatting.Indented )}" );
                if ( itemNumber < itemCount )
                    _ = jsonBuilder.AppendLine( "," );
                _ = jsonBuilder.AppendLine();
            }
            jsonBuilder = new StringBuilder( jsonBuilder.ToString().Replace( "}", "  }" ) );
            _ = jsonBuilder.AppendLine( "}" );
            FileInfo fi = new( fileName );
            if ( !fi.Directory.Exists )
                fi.Directory.Create();
            File.WriteAllText( fileName, jsonBuilder.ToString() );
        }

        #endregion

        #region " SETTINGS FILE NAME "

        /// <summary>   Gets or sets the scope. </summary>
        /// <value> The scope. </value>
        [JsonIgnore]
        [Description( "The scope of the settings" )]
        public SettingsScope Scope { get; private set; }

        /// <summary>   Gets the file name suffix. </summary>
        /// <value> The file name suffix. </value>
        [JsonIgnore]
        [Description( "The settings file name suffix" )]
        public string FileNameSuffix => JsonSettingsBase.BuildFileNameSuffix( this.Scope );

        /// <summary>   Query if 'folderName' is restricted a folder; where files are read only. </summary>
        /// <remarks>   David, 2021-08-28. </remarks>
        /// <param name="folderName">   . </param>
        /// <returns>   True if restricted folder, false if not. </returns>
        public static bool IsRestrictedFolder( string folderName )
        {
            return !string.IsNullOrEmpty( folderName ) &&
                   (folderName.StartsWith( Environment.GetFolderPath( Environment.SpecialFolder.ProgramFiles ), StringComparison.OrdinalIgnoreCase )
                    || folderName.StartsWith( Environment.GetFolderPath( Environment.SpecialFolder.ProgramFilesX86 ), StringComparison.OrdinalIgnoreCase ));
        }

        /// <summary>   Gets or sets the full filename of the settings file. </summary>
        /// <value> The full filename of the settings file. </value>
        [JsonIgnore]
        [Description( "The full file name for reading settings" )]
        public string FullFileName => this.SelectFileName( FileNameScope.Reading );

        /// <summary>   Gets or sets the filename of the application data settings full file. </summary>
        /// <value> The filename of the application data settings full file. </value>
        [JsonIgnore]
        [Description( "The settings file name for global or roaming Application Data settings" )]
        public string AppDataSettingsFullFileName { get; private set; }

        /// <summary>   The Application Context settings full file name. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <returns>   A string. </returns>
        [JsonIgnore]
        [Description( "The settings file name for application context" )]
        public string AppContextSettingsFullFileName { get; set; }

        /// <summary>   Checks if the settings file exists for reading. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <returns>   True if it exists; otherwise, false. </returns>
        public bool Exists()
        {
            FileInfo fi = new( this.FullFileName );
            return fi.Exists;
        }

        /// <summary>   Checks if the settings file exists and is not empty. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        /// <param name="fileName"> Filename of the file. </param>
        /// <returns>   True if it exists; otherwise, false. </returns>
        public static bool Exists( string fileName )
        {
            FileInfo fi = new( fileName );
            return fi.Exists && fi.Length > 0;
        }

        /// <summary>   Values that represent file name scopes. </summary>
        /// <remarks>   David, 2021-08-28. </remarks>
        private enum FileNameScope
        {
            /// <summary>   An enum constant representing the reading option. </summary>
            Reading,
            /// <summary>   An enum constant representing the writing option. </summary>
            Writing,
            /// <summary>   An enum constant representing the restoring option. </summary>
            Restoring
        }

        /// <summary>   Select file name. </summary>
        /// <remarks>
        /// Settings are read from either the application data or the application context folder.
        /// Settings are written to the application data file if exists. Setting are restored from the
        /// application context.
        /// </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="fileNameScope">    The file name scope. </param>
        /// <returns>   A string. </returns>
        private string SelectFileName( FileNameScope fileNameScope )
        {
            return fileNameScope switch {
                FileNameScope.Reading => (string.IsNullOrEmpty( this.AppDataSettingsFullFileName ) || !JsonSettingsBase.Exists( this.AppDataSettingsFullFileName ))
                                       ? this.AppContextSettingsFullFileName
                                       : this.AppDataSettingsFullFileName,
                FileNameScope.Writing => (string.IsNullOrEmpty( this.AppDataSettingsFullFileName ))
                                       ? this.AppContextSettingsFullFileName
                                       : this.AppDataSettingsFullFileName,
                FileNameScope.Restoring => this.AppContextSettingsFullFileName,
                _ => throw new InvalidOperationException( $"Unhandled case {nameof( JsonSettingsBase.FileNameScope )}='{fileNameScope}' when selecting settings file name" ),
            };
        }

        #endregion

        #region " FILE NAME BUILDERS "

        /// <summary>   (Immutable) the application setting suffix. </summary>
        public const string AppSettingSuffix = ".Settings";

        /// <summary>   (Immutable) the user setting suffix. </summary>
        public const string UserSettingSuffix = ".UserSettings";

        /// <summary>   Builds the file name suffix. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        /// <param name="scope">    The scope. </param>
        /// <returns>   A string. </returns>
        public static string BuildFileNameSuffix( SettingsScope scope )
        {
            return scope == SettingsScope.ApplicationScope ? JsonSettingsBase.AppSettingSuffix : JsonSettingsBase.UserSettingSuffix;
        }

        /// <summary>
        /// Builds application settings file title and extension consisting of the<para>
        /// <paramref name="assemblyName"/>+<see cref="FileNameSuffix"/>+.json.</para>
        /// </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="assemblyName"> The assembly file title. </param>
        /// <param name="scope">        (Optional) The scope. </param>
        /// <returns>   A file title and extension. </returns>
        public static string BuildAppSettingsFileName( string assemblyName, SettingsScope scope = SettingsScope.ApplicationScope )
        {
            return $"{assemblyName}{JsonSettingsBase.BuildFileNameSuffix( scope )}.json";
        }

        /// <summary>
        /// Builds application settings file title and extension consisting of the<para>
        /// 'Assembly Name'+<see cref="FileNameSuffix"/>+.json.</para>
        /// </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="assembly"> The assembly, which could be the Entry, Calling or Executing
        ///                         assembly. </param>
        /// <param name="scope">    (Optional) The scope. </param>
        /// <returns>   A file title and extension. </returns>
        public static string BuildAppSettingsFileName( System.Reflection.Assembly assembly, SettingsScope scope = SettingsScope.ApplicationScope )
        {
            return JsonSettingsBase.BuildAppSettingsFileName( assembly.GetName().Name, scope );
        }

        /// <summary>
        /// Builds application settings full file name consisting of the<para>
        /// <paramref name="folderName"/>\'Assembly Name'+<see cref="FileNameSuffix"/>+.json.</para>
        /// </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="folderName">   Pathname of the folder. </param>
        /// <param name="assembly">     The assembly, which could be the Entry, Calling or Executing
        ///                             assembly. </param>
        /// <param name="scope">        (Optional) The scope. </param>
        /// <returns>   A string. </returns>
        public static string BuildAppSettingsFullFileName( string folderName, System.Reflection.Assembly assembly, SettingsScope scope = SettingsScope.ApplicationScope )
        {
            return Path.Combine( folderName, JsonSettingsBase.BuildAppSettingsFileName( assembly, scope ) );
        }

        /// <summary>   Builds application context settings full file name. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        /// <param name="assembly"> The assembly, which could be the Entry, Calling or Executing
        ///                         assembly. </param>
        /// <param name="scope">    (Optional) The scope. </param>
        /// <returns>   A string. </returns>
        public static string BuildAppContextSettingsFullFileName( System.Reflection.Assembly assembly, SettingsScope scope = SettingsScope.ApplicationScope )
        {
            return Path.Combine( AppContext.BaseDirectory, JsonSettingsBase.BuildAppSettingsFileName( assembly, scope ) );
        }

        /// <summary>   Builds assembly folder name. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        /// <param name="assembly"> The assembly, which could be the Entry, Calling or Executing
        ///                         assembly. </param>
        /// <returns>   A string. </returns>
        public static string BuildAssemblyFolderName( System.Reflection.Assembly assembly )
        {
            var versionInfo = FileVersionInfo.GetVersionInfo( assembly.Location );
            return Path.Combine( versionInfo.CompanyName, versionInfo.ProductName, versionInfo.ProductVersion );
        }

        /// <summary>   Builds application data settings full file name. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        /// <param name="assembly"> The assembly, which could be the Entry, Calling or Executing
        ///                         assembly. </param>
        /// <param name="scope">    (Optional) The scope. </param>
        /// <returns>   A string. </returns>
        public static string BuildApplicationDataSettingsFullFileName( System.Reflection.Assembly assembly, SettingsScope scope = SettingsScope.ApplicationScope )
        {
            return BuildAppSettingsFullFileName( Path.Combine( Environment.GetFolderPath( scope == SettingsScope.ApplicationScope ? Environment.SpecialFolder.CommonApplicationData : Environment.SpecialFolder.ApplicationData ),
                                                                         JsonSettingsBase.BuildAssemblyFolderName( assembly ) ),
                                                  assembly, scope );
        }

        #endregion

        #region " ASSEMBLY TYPES FILE NAME BUILDERS: OBSOLETED "

        /// <summary>
        /// Builds the calling assembly settings full file name consisting of the<para>
        /// <see cref="AppContext.BaseDirectory"/>\'Assembly Name'+<see cref="FileNameSuffix"/>+.json.</para>
        /// </summary>
        /// <remarks>   David, 2021-07-01. </remarks>
        /// <param name="scope">    (Optional) The scope. </param>
        /// <returns>   A string. </returns>
        [Obsolete( "Call this class constructor. Get the assembly from the Type contained in the assembly" )]
        public static string BuildCallingAssemblySettingsFullFileName( SettingsScope scope = SettingsScope.ApplicationScope )
        {
            return JsonSettingsBase.BuildAppContextSettingsFullFileName( System.Reflection.Assembly.GetCallingAssembly(), scope );
        }

        /// <summary>
        /// Builds the entry assembly settings full file name consisting of the<para>
        /// <see cref="AppContext.BaseDirectory"/>\'Assembly Name'+<see cref="FileNameSuffix"/>+.json.</para>
        /// </summary>
        /// <remarks>   David, 2021-07-01. </remarks>
        /// <param name="scope">    (Optional) The scope. </param>
        /// <returns>   A string. </returns>
        [Obsolete( "Call this class constructor. Get the assembly from the Type contained in the assembly" )]
        public static string BuildEntryAssemblySettingsFullFileName( SettingsScope scope = SettingsScope.ApplicationScope )
        {
            return JsonSettingsBase.BuildAppContextSettingsFullFileName( System.Reflection.Assembly.GetEntryAssembly(), scope );
        }

        /// <summary>
        /// Builds the settings full file name consisting of the<para>
        /// <see cref="AppContext.BaseDirectory"/>\'Assembly Name'+<see cref="FileNameSuffix"/>+.json.</para>
        /// for the assembly in which the <paramref name="type"/> is defined.
        /// </summary>
        /// <remarks>   David, 2021-07-01. </remarks>
        /// <param name="type">     The type which assembly is used to set the file name. </param>
        /// <param name="scope">    (Optional) The scope. </param>
        /// <returns>   A string. </returns>
        [Obsolete( "Call this class constructor. Get the assembly from the Type contained in the assembly" )]
        public static string BuildAssemblySettingsFullFileName( Type type, SettingsScope scope = SettingsScope.ApplicationScope )
        {
            return JsonSettingsBase.BuildAppContextSettingsFullFileName( System.Reflection.Assembly.GetAssembly( type ), scope );
        }

        #endregion

    }

    /// <summary>   Attribute for settings section. </summary>
    /// <remarks>   David, 2021-02-02. </remarks>
    [System.AttributeUsage( System.AttributeTargets.Class )]
    public sealed class SettingsSectionAttribute : System.Attribute
    {
        /// <summary>   Gets or sets the name of the section. </summary>
        /// <value> The name of the section. </value>
        public string SectionName { get; private set; }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="sectionName">  The name of the section. </param>
        public SettingsSectionAttribute( string sectionName )
        {
            this.SectionName = sectionName;
        }
    }

    /// <summary>   Values that represent settings scopes. </summary>
    /// <remarks>   David, 2021-07-27. </remarks>
    public enum SettingsScope
    {
        /// <summary>   An enum constant representing the application settings scope option. </summary>
        ApplicationScope,
        /// <summary>   An enum constant representing the user settings scope option. </summary>
        UserScope
    }
}
