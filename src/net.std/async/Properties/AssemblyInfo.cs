using System;

// [assembly: AssemblyDescription( "Core Framework Library" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Core.AsyncMSTest,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Core.AsyncTrials,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey )]
