# About

isr.Core.Lizzie is a fork of the [lizzie] library.

# How to Use

See [lizzie]

# Feedback

isr.Core.Lizzie is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Core Framework Repository] repository.

# Open source
Open source used by this software is described and licensed at the following sites:  
[lizzie]  
[Core Framework Repository]  

[lizzie]: https://github.com/polterguy/lizzie/
[Core Framework Repository]: https://bitbucket.org/davidhary/dn.core
