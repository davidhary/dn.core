using System;
using System.ComponentModel;
using System.Threading;

namespace isr.Core.SyncContext
{
    /// <summary>   A property changed notifier using the synchronization context. </summary>
    /// <remarks>   David, 2021-02-25. </remarks>
    public abstract class PropertyChangedNotifier : INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Specialized default constructor for use only by derived class. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="notificationLevel">    (Optional) The notification level [<see cref="NotificationLevel.Synchronous"/>]. </param>
        protected PropertyChangedNotifier( NotificationLevel notificationLevel = NotificationLevel.Synchronous )
        {
            this.Context = SynchronizationContext.Current;
            this.NotificationLevel = notificationLevel;
        }

        #endregion

        #region " SYNCHRONIZATION CONTEXT "

        /// <summary> Gets or sets the synchronization context. </summary>
        /// <value> The context. </value>
        private SynchronizationContext Context { get; set; }


        /// <summary> Returns the current synchronization context. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
        /// null. </exception>
        /// <returns> A Threading.SynchronizationContext. </returns>
        private static SynchronizationContext CurrentSyncContext()
        {
            if ( SynchronizationContext.Current is null )
            {
                SynchronizationContext.SetSynchronizationContext( new SynchronizationContext() );
            }

            return SynchronizationContext.Current is null
                ? throw new InvalidOperationException( "Current Synchronization Context not set;. Must be set before starting the thread." )
                : SynchronizationContext.Current;
        }

        private NotificationLevel _NotificatioLevel;
        /// <summary>   Gets or sets the notification level. </summary>
        /// <value> The notification level. </value>
        public NotificationLevel NotificationLevel
        {
            get => this._NotificatioLevel;
            set {
                if ( !NotificationLevel.Equals( value, this.NotificationLevel ) )
                {
                    this._NotificatioLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets a context for the active. </summary>
        /// <value> The active context. </value>
        protected SynchronizationContext ActiveContext => this.Context ?? CurrentSyncContext();

        #endregion

        #region " EVENT HANDLING "


        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            if ( this.NotificationLevel == NotificationLevel.Synchronous )
            { this.SyncNotifyPropertyChanged( propertyName ); }
            else if ( this.NotificationLevel == NotificationLevel.Asynchronous )
            { this.AsyncNotifyPropertyChanged( propertyName ); }
        }

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void SyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Asynchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void AsyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
                this.ActiveContext.Post( ( object state ) => handler( this, new PropertyChangedEventArgs( propertyName ) ), null );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

    }

    /// <summary>   Values that represent notification levels. </summary>
    /// <remarks>   David, 2021-02-25. </remarks>
    public enum NotificationLevel
    {
        /// <summary>   An enum constant representing the none option. </summary>
        [Description( "None" )]
        None,
        /// <summary>   An enum constant representing the synchronous option. </summary>
        [Description( "Synchronous" )]
        Synchronous,
        /// <summary>   An enum constant representing the asynchronous option. </summary>
        [Description( "Asynchronous" )]
        Asynchronous
    }
}
