using System;
using System.Collections.Specialized;
using System.ComponentModel;

namespace isr.Core.SyncContext
{

    /// <summary>   An event handler extensions. </summary>
    /// <remarks>   David, 2020-09-15. </remarks>
    public static partial class EventHandlerExtensions
    {

        /// <summary>
        /// Asynchronously notifies an <see cref="EventHandler{EventArgs}">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The event handler. </param>
        /// <param name="sender">  The sender of the event. </param>
        public static void AsyncNotify( this EventHandler<EventArgs> handler, object sender )
        {
            var hndlr = handler;
            if ( hndlr is object )
            {
                AsyncNotifyThis( hndlr, CurrentSyncContext(), sender, EventArgs.Empty );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="EventHandler{EventArgs}">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       T event information. </param>
        public static void AsyncNotify<TEventArgs>( this EventHandler<TEventArgs> handler, System.Threading.SynchronizationContext context, object sender, TEventArgs e ) where TEventArgs : EventArgs
        {
            var hndlr = handler;
            if ( hndlr is object )
            {
                AsyncNotifyThis( hndlr, context, sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="EventHandler{EventArgs}">Event</see>.
        /// </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <typeparam name="TEventArgs">   Type of the event arguments. </typeparam>
        /// <param name="handler">  The handler. </param>
        /// <param name="context">  The synchronization context. </param>
        /// <param name="sender">   The sender of the event. </param>
        /// <param name="e">        T event information. </param>
        private static void AsyncNotifyThis<TEventArgs>( EventHandler<TEventArgs> handler, System.Threading.SynchronizationContext context, object sender, TEventArgs e ) where TEventArgs : EventArgs
        {
            context.Post( ( object status ) => handler( sender, e ), null );
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       T event information. </param>
        public static void AsyncNotify( this PropertyChangedEventHandler handler, object sender, PropertyChangedEventArgs e )
        {
            var hndlr = handler;
            if ( hndlr is object )
            {
                AsyncNotifyThis( hndlr, CurrentSyncContext(), sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property changed event information. </param>
        public static void AsyncNotify( this PropertyChangedEventHandler handler, System.Threading.SynchronizationContext context, object sender, PropertyChangedEventArgs e )
        {
            var hndlr = handler;
            if ( hndlr is object )
            {
                AsyncNotifyThis( hndlr, context, sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="PropertyChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="handler">  The handler. </param>
        /// <param name="context">  The synchronization context. </param>
        /// <param name="sender">   The sender of the event. </param>
        /// <param name="e">        Property changed event information. </param>
        private static void AsyncNotifyThis( PropertyChangedEventHandler handler, System.Threading.SynchronizationContext context, object sender, PropertyChangedEventArgs e )
        {
            context.Post( ( object status ) => handler( sender, e ), null );
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       T event information. </param>
        public static void AsyncNotify( this PropertyChangingEventHandler handler, object sender, PropertyChangingEventArgs e )
        {
            var hndlr = handler;
            if ( hndlr is object )
            {
                AsyncNotifyThis( hndlr, CurrentSyncContext(), sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property Changing event information. </param>
        public static void AsyncNotify( this PropertyChangingEventHandler handler, System.Threading.SynchronizationContext context, object sender, PropertyChangingEventArgs e )
        {
            var hndlr = handler;
            if ( hndlr is object )
            {
                AsyncNotifyThis( hndlr, context, sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="PropertyChangingEventHandler">Event</see>.
        /// </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="handler">  The handler. </param>
        /// <param name="context">  The synchronization context. </param>
        /// <param name="sender">   The sender of the event. </param>
        /// <param name="e">        Property Changing event information. </param>
        private static void AsyncNotifyThis( PropertyChangingEventHandler handler, System.Threading.SynchronizationContext context, object sender, PropertyChangingEventArgs e )
        {
            context.Post( ( object status ) => handler( sender, e ), null );
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       T event information. </param>
        public static void AsyncNotify( this NotifyCollectionChangedEventHandler handler, object sender, NotifyCollectionChangedEventArgs e )
        {
            var hndlr = handler;
            if ( hndlr is object )
            {
                AsyncNotifyThis( hndlr, CurrentSyncContext(), sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="handler"> The handler. </param>
        /// <param name="context"> The synchronization context. </param>
        /// <param name="sender">  The sender of the event. </param>
        /// <param name="e">       Property changed event information. </param>
        public static void AsyncNotify( this NotifyCollectionChangedEventHandler handler, System.Threading.SynchronizationContext context, object sender, NotifyCollectionChangedEventArgs e )
        {
            var hndlr = handler;
            if ( hndlr is object )
            {
                AsyncNotifyThis( hndlr, context, sender, e );
            }
        }

        /// <summary>
        /// Asynchronously notifies an <see cref="NotifyCollectionChangedEventHandler">Event</see>.
        /// </summary>
        /// <remarks>   David, 2020-09-15. </remarks>
        /// <param name="handler">  The handler. </param>
        /// <param name="context">  The synchronization context. </param>
        /// <param name="sender">   The sender of the event. </param>
        /// <param name="e">        Property changed event information. </param>
        private static void AsyncNotifyThis( NotifyCollectionChangedEventHandler handler, System.Threading.SynchronizationContext context, object sender, NotifyCollectionChangedEventArgs e )
        {
            context.Post( ( object status ) => handler( sender, e ), null );
        }
    }
}
