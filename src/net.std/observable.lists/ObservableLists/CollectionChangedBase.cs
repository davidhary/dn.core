using System;
using System.Collections.Specialized;
using System.ComponentModel;

namespace isr.Core.ObservableLists
{

    /// <summary> A collection changed base. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-05-22 </para>
    /// </remarks>
    public abstract class CollectionChangedBase : INotifyCollectionChanged, INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        protected CollectionChangedBase() : base()
        {
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION"

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " I NOTIFY COLLECTION CHANGED IMPLEMENTATION"

        /// <summary>   Event queue for all listeners interested in Collection Changed events. </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// Notifies collection change event. Not thread safe.
        /// </summary>
        /// <param name="e"> Collection Changed event information. </param>
        protected virtual void NotifyCollectionChanged( NotifyCollectionChangedEventArgs e )
        {
            this.CollectionChanged?.Invoke( this, e );
        }

        /// <summary>   Raises the collection changed event. </summary>
        /// <remarks>   David, 2021-08-16. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected void OnCollectionChanged( NotifyCollectionChangedEventArgs e )
        {
            this.NotifyCollectionChanged( e );
        }

        #endregion

    }
}
