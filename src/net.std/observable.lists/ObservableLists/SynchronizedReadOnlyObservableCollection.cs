using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.Core.ObservableLists
{

    /// <summary> Implements a synchronized read only observable keyed collection. </summary>
    /// <remarks>
    /// David, 2017-05-02.  <para>
    /// (c) 2011 MULJADI BUDIMAN. All rights reserved. </para><para>
    /// http://geekswithblogs.net/NewThingsILearned/archive/2010/01/12/make-keyedcollectionlttkey-titemgt-to-work-properly-with-wpf-data-binding.aspx
    /// </para>
    /// </remarks>
    [DebuggerDisplay( "Count = {Count}" )]
    public class SynchronizedReadOnlyObservableCollection<T> : ReadOnlyCollection<T>, INotifyCollectionChanged, INotifyPropertyChanged
    {

        #region " CONSTRCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="list"> The list. </param>
        public SynchronizedReadOnlyObservableCollection( SynchronizedObservableCollection<T> list ) : base( list )
        {
            (( INotifyCollectionChanged ) this.Items).CollectionChanged += this.HandleCollectionChanged;
            (( INotifyPropertyChanged ) this.Items).PropertyChanged += this.HandlePropertyChanged;
        }
        #endregion

        #region " Public Events "

        /// <summary> Event queue for all listeners interested in CollectionChanged events. </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// Notifies collection change event. Not thread safe.
        /// </summary>
        /// <param name="e"> Collection Changed event information. </param>
        protected virtual void NotifyCollectionChanged( NotifyCollectionChangedEventArgs e )
        {
            this.CollectionChanged?.Invoke( this, e );
        }

        /// <summary>   Raises the collection changed event. </summary>
        /// <remarks>   David, 2021-08-16. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected void OnCollectionChanged( NotifyCollectionChangedEventArgs e )
        {
            this.NotifyCollectionChanged( e );
        }

        /// <summary> Handles the collection changed. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Notify collection changed event information. </param>
        private void HandleCollectionChanged( object sender, NotifyCollectionChangedEventArgs e )
        {
            this.OnCollectionChanged( e );
        }

        /// <summary> Event queue for all listeners interested in PropertyChanged events. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary> Raises the property changed event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnPropertyChanged( PropertyChangedEventArgs e )
        {
            this.PropertyChanged?.Invoke( this, e );
        }

        /// <summary> Handles the property changed event. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        private void HandlePropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            this.OnPropertyChanged( e );
        }

        #endregion

    }
}
