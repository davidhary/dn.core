namespace isr.Core.Statistics
{

    /// <summary> Sample quartile calculations. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-05-29 </para>
    /// </remarks>
    public class SampleQuartiles
    {

        #region " CONSTRUCTOR "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public SampleQuartiles() : base()
        {
            this.FenceFactor = 1.5d;
            this.Sample = new SampleStatistics();
            this.ClearKnownStateThis();
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="values"> The values. </param>
        public SampleQuartiles( double[] values ) : this()
        {
            if ( values is object )
            {
                this.Sample.AddValues( values );
            }
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="sample"> The sample. </param>
        public SampleQuartiles( SampleStatistics sample ) : this()
        {
            this.Sample = sample;
        }

        #endregion

        #region " RESET AND CLEAR "

        /// <summary> Clears quartile values to their known (initial) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void ClearQuartilesThis()
        {
            this.Quartiles = new Quartiles();
            this.LowerFence = 0;
            this.UpperFence = this.LowerFence;
            this.FilteredSample = new SampleStatistics();
        }

        /// <summary> Clears values to their known (initial) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private void ClearKnownStateThis()
        {
            this.Sample.ClearKnownState();
            this.ClearQuartilesThis();
        }

        /// <summary> Clears values to their known (initial) state. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public virtual void ClearKnownState()
        {
            this.ClearKnownStateThis();
        }

        #endregion

        #region " SAMPLE STATISTICS "

        /// <summary> Gets the sample. </summary>
        /// <value> The sample. </value>
        public SampleStatistics Sample { get; private set; }

        /// <summary> Gets the filtered sample with outliers removed. </summary>
        /// <value> The filtered sample. </value>
        public SampleStatistics FilteredSample { get; set; }

        /// <summary> Gets the number of outliers. </summary>
        /// <value> The number of outliers. </value>
        public int OutlierCount => this.Sample.Count - this.FilteredSample.Count;

        #endregion

        #region " QUARTILES "

        /// <summary> Gets the fence factor. </summary>
        /// <value> The fence factor. </value>
        public double FenceFactor { get; set; }

        /// <summary> Gets the quartiles. </summary>
        /// <value> The quartiles. </value>
        public Quartiles Quartiles { get; set; }

        /// <summary> Gets the upper fence. </summary>
        /// <value> The upper fence. </value>
        public double UpperFence { get; set; }

        /// <summary> Gets the lower fence. </summary>
        /// <value> The lower fence. </value>
        public double LowerFence { get; set; }

        /// <summary>
        /// Evaluates the <see cref="Quartiles">quartiles</see> and the
        /// <see cref="FilteredSample">filtered sample</see>.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        public void Evaluate()
        {
            this.ClearQuartilesThis();
            this.Quartiles.Evaluate( this.Sample.ValuesArray() );
            this.LowerFence = this.Quartiles.First - this.FenceFactor * this.Quartiles.Range;
            this.UpperFence = this.Quartiles.Third + this.FenceFactor * this.Quartiles.Range;
            this.FilterSample();
        }

        private int[] _OutlierIndexes;
        /// <summary>   Outlier indexes. </summary>
        /// <remarks>   David, 2021-06-01. </remarks>
        /// <returns>   An int[]. </returns>
        public int[] OutlierIndexes()
        {
            return this._OutlierIndexes;
        }

        /// <summary> Filter sample. </summary>
        /// <remarks>
        /// This function is set outside the evaluation to allow recalculation is the fence is updated.
        /// </remarks>
        public void FilterSample()
        {
            // clear the filtered quartiles.
            this._FilteredQuartiles = null;
            this.FilteredSample = new SampleStatistics();
            System.Collections.Generic.List<int> l = new();
            for ( int i = 0; i < this.Sample.ValuesArray().Length; i++ )
            {
                double value = this.Sample.ValuesArray()[i];
                if ( value >= this.LowerFence && value <= this.UpperFence )
                {
                    this.FilteredSample.AddValue( value );
                }
                else
                {
                    l.Add( i );
                }
                this._OutlierIndexes = l.ToArray();
            }
        }

        #endregion

        #region " FILTERED QUARTILES "

        /// <summary> The filtered quartiles. </summary>
        private SampleQuartiles _FilteredQuartiles;

        /// <summary> Gets the filtered sample quartiles after removal of outliers. </summary>
        /// <value> The filtered quartiles. </value>
        public SampleQuartiles FilteredQuartiles
        {
            get {
                if ( this._FilteredQuartiles is null )
                {
                    this._FilteredQuartiles = new SampleQuartiles( this.FilteredSample );
                    this._FilteredQuartiles.Evaluate();
                }

                return this._FilteredQuartiles;
            }
        }

        #endregion

    }
}
