using System;

// [assembly: AssemblyDescription( "Core Framework Library" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Core.Notifiers.MSTest,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey )]
