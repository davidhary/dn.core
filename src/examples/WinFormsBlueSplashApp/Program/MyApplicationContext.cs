using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using isr.Core.TraceLog.ExceptionExtensions;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;

namespace isr.Core.Examples
{

    /// <summary>   The class that handles the creation of the application windows. </summary>
    /// <remarks>
    /// David, 2021-03-15. <para>
    /// You can use the ApplicationContext class to redefine the circumstances that cause a message
    /// loop to exit. By default, the ApplicationContext listens to the Closed event on the
    /// application's main Form, then exits the thread's message loop.
    /// </para><para>
    /// https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.applicationcontext?view=net-5.0
    /// </para>
    /// </remarks>
    internal class MyApplicationContext : ApplicationContext
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-03-15. </remarks>
        internal MyApplicationContext() : base()
        {

            // Handle the ApplicationExit event to know when the application is exiting.
            Application.ApplicationExit += new EventHandler( this.OnApplicationExit );
            Application.EnterThreadModal += new System.EventHandler( this.OnEnterThreadModal );
            Application.ThreadException += new ThreadExceptionEventHandler( this.OnThreadException );
            Application.Idle += new EventHandler( this.OnApplicationIdle );
            Application.LeaveThreadModal += new System.EventHandler( this.OnLeaveThreadModal );
            Application.ThreadExit += new System.EventHandler( this.OnThreadExit );
            NetworkChange.NetworkAvailabilityChanged += new NetworkAvailabilityChangedEventHandler( this.OnNetworkAddressChanged );

            MyApplicationContext.HighDpiModeWasSet = Application.SetHighDpiMode( HighDpiMode.SystemAware );
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            isr.Core.TraceLog.TraceLogger.Instance.MinimumLogLevel = AppSettings.Instance.Settings.ApplicationLogLevel;

            // Read the settings.
            this.ReadSettings();

            // show the splash screen
            this.ShowSplashForm();

            // show the main form.
            this.ShowDashboard( Settings.SplashDuration );
        }

        #region " SPLASH FORM "

        private isr.Core.WinForms.Metro.Blue.BlueSplash SplashForm { get; set; }

        /// <summary>   Shows the splash form. </summary>
        /// <remarks>   David, 2021-03-13. </remarks>
        private void ShowSplashForm()
        {
            try
            {
                // Show the splash form.
                this.SplashForm = new();
                this.SplashForm.TopmostSetter( !Debugger.IsAttached );
                this.SplashForm.Show();
            }
            catch ( Exception ex )
            {
                // Inform the user that an error occurred.
                _ = ex.AddExceptionData();
                string message = "An error occurred while attempting to show the splash form";
                _ = TraceLog.TraceLogger.LogError( ex, message );
                _ = MessageBox.Show( $"{message}. The error is: {ex.BuildMessage()}", "Splash Form error occurred" );

                // Exit the current thread instead of showing the windows.
                this.ExitThread();
            }
        }

        #endregion

        #region " MAIN FORM (DASHBOARD) "

        /// <summary>   Shows the dashboard. </summary>
        /// <remarks>   David, 2021-03-15. </remarks>
        /// <param name="splashDuration">   Duration of the splash. </param>
        private void ShowDashboard( TimeSpan splashDuration )
        {
            string activity = string.Empty;
            try
            {
                activity = $"attempting to construct the {nameof( Dashboard )} form";
                this.SplashForm?.DisplayMessage( activity );
                Dashboard dashboard = new();

                activity = $"awaiting the splash display duration";
                this.SplashForm?.DisplayMessage( activity );
                DateTime endTime = DateTime.UtcNow.Add( splashDuration );
                do
                {
                    // this is required; otherwise the splash form freezes until it is removed. 
                    Application.DoEvents();
                } while ( endTime > DateTime.UtcNow );

                activity = $"attempting to register the {nameof( Dashboard )} form Load and closed events";
                this.SplashForm?.DisplayMessage( activity );

                // this event handler disposes of the splash form.
                dashboard.Load += new EventHandler( this.Dashboard_Load );

                // this is necessary because the context instantiates two forms.
                dashboard.Closed += new EventHandler( this.Dashboard_Closed );

                // Show the dashboard
                activity = $"attempting to show the {nameof( Dashboard )} form";
                this.SplashForm?.DisplayMessage( activity );
                dashboard.Show();
            }
            catch ( Exception ex )
            {
                // Inform the user that an error occurred.
                _ = ex.AddExceptionData();
                string message = $"An error occurred while {activity}";
                this.SplashForm?.DisplayMessage( message );
                _ = TraceLog.TraceLogger.LogError( ex, message );
                _ = MessageBox.Show( $"{message}. The error is: {ex.BuildMessage()}", $"{nameof( Dashboard )} Form error occurred" );

                // Exit the current thread instead of showing the windows.
                this.ExitThread();
            }

        }

        /// <summary>   Event handler. Called by Dashboard for closed events. </summary>
        /// <remarks>   David, 2021-03-15. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Dashboard_Closed( object sender, EventArgs e )
        {
            // this makes sure the application exits when the main form closes.
            // it is necessary because the context instantiates two forms.
            this.ExitThread();
            this.Dispose();
        }

        /// <summary>   Event handler. Called by Dashboard for load events. </summary>
        /// <remarks>   David, 2021-03-13. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Dashboard_Load( object sender, EventArgs e )
        {
            //close splash
            if ( this.SplashForm == null )
            {
                return;
            }

            // the operator could close the program from the splash screen
            bool closeRequested = this.SplashForm.IsCloseRequested;

            // _ = this.SplashForm.Invoke( new Action ( this.SplashForm.Close ) );
            // this.SplashForm.Dispose();
            // this.SplashForm = null;
            _ = this.SplashForm.BeginInvoke( new MethodInvoker( this.SplashForm.Close ) );
            // _ = binding.Control.BeginInvoke( new MethodInvoker( binding.ReadValue ) );

            if ( closeRequested )
            {
                if ( DialogResult.Yes == MessageBox.Show( "User clicked the Splash form close button. Select Yes to terminate the program or No to ignore the close request",
                                                         "User close requested. Terminate the program?", MessageBoxButtons.YesNo,
                                                         MessageBoxIcon.Information, MessageBoxDefaultButton.Button2 ) )
                {
                    // log a warning and exit; no error
                    _ = TraceLog.TraceLogger.LogWarning( "User close requested." );
                    if ( sender is Dashboard dashboard )
                    {
                        _ = dashboard.BeginInvoke( new MethodInvoker( dashboard.Close ) );
                    }

                }
            }

        }

        #endregion

        #region " APPLICATION EVENT HANDLERS "

        /// <summary>   Event handler. Called by Application for thread exception events. </summary>
        /// <remarks>
        /// David, 2021-03-15. <para>
        /// Because <see cref="Application.ThreadException"/> is a static event, you must detach any
        /// event handlers attached to this event in the ApplicationExit event handler itself. If you do
        /// not detach these handlers, they will remain attached to the event and continue to consume
        /// memory.
        /// </para>
        /// </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Thread exception event information. </param>
        private void OnThreadException( object sender, ThreadExceptionEventArgs e )
        {
            string activity;
            if ( e is null )
            {
                activity = "Unhandled exception event occurred with event arguments set to nothing.";
                _ = TraceLog.TraceLogger.LogWarning( activity );

                Debug.Assert( !Debugger.IsAttached, "Unhandled exception event occurred with event arguments set to nothing." );
                return;
            }

            try
            {

                _ = e.Exception.AddExceptionData();
                e.Exception.Data.Add( "@isr", "Unhandled Exception Occurred." );

                this.SplashForm?.DisplayMessage( "Unhandled Exception Occurred." );
                _ = TraceLog.TraceLogger.LogError( e.Exception, $"Unhanded Exception" );

                if ( DialogResult.Abort == MessageBox.Show( $"An error occurred while running the application. The error is: {e.Exception.BuildMessage()}",
                                                         "Unhandled exception occurred", MessageBoxButtons.AbortRetryIgnore,
                                                         MessageBoxIcon.Error, MessageBoxDefaultButton.Button3 ) )
                {
                    // exit with an error code
                    Environment.Exit( -1 );
                    this.ExitThread();
                }
            }
            catch
            {
                if ( System.Windows.Forms.MessageBox.Show( e.Exception.ToString(), "Unhandled Exception occurred.",
                                                           MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error,
                                                           MessageBoxDefaultButton.Button3, MessageBoxOptions.DefaultDesktopOnly ) == DialogResult.Abort )
                {
                }
            }
            finally
            {
            }
        }

        /// <summary>   Event handler. Called by Application for application exit events. </summary>
        /// <remarks>
        /// David, 2021-03-15. <para>You must attach the event handlers to the ApplicationExit event to
        /// perform unhandled, required tasks before the application stops running. You can close files
        /// opened by this application, or dispose of objects that garbage collection did not reclaim.
        /// </para><para>
        /// Because  <see cref="Application.ApplicationExit"/> is a static event, you must detach any event handlers attached to this event in
        /// the ApplicationExit event handler itself. If you do not detach these handlers, they will
        /// remain attached to the event and continue to consume memory.
        /// </para>
        /// </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void OnApplicationExit( object sender, EventArgs e )
        {
            // Because these are static events, you must detach your event handlers when your application
            // is disposed, or memory leaks will result.
            Application.ThreadException -= this.OnThreadException;
            Application.ApplicationExit -= this.OnApplicationExit;
            Application.EnterThreadModal -= this.OnEnterThreadModal;
            Application.Idle -= this.OnApplicationIdle;
            Application.LeaveThreadModal -= this.OnLeaveThreadModal;
            Application.ThreadExit -= this.OnThreadExit;
            NetworkChange.NetworkAvailabilityChanged -= this.OnNetworkAddressChanged;
        }

        /// <summary>   Event handler. Called by Application for enter thread modal events. </summary>
        /// <remarks>
        /// David, 2021-03-15. <para>
        /// Because <see cref="Application.EnterThreadModal"/> is a static event, you must detach any
        /// event handlers attached to this event in the ApplicationExit event handler itself. If you do
        /// not detach these handlers, they will remain attached to the event and continue to consume
        /// memory.
        /// </para>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void OnEnterThreadModal( object sender, EventArgs e )
        {

        }

        /// <summary>   Event handler. Called by Application for leave thread modal events. </summary>
        /// <remarks>   David, 2021-03-15. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnLeaveThreadModal( object sender, EventArgs e )
        {

        }

        /// <summary>   Event handler. Called by Application for entering idle state. </summary>
        /// <remarks>
        /// David, 2021-03-15. <para>
        /// Occurs when the application finishes processing and is about to enter the idle state</para>
        /// </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnApplicationIdle( object sender, EventArgs e )
        {

        }

        /// <summary>   Event handler. Called by Application for exiting the thread. </summary>
        /// <remarks>
        /// David, 2021-03-15. <para>
        /// Occurs when a thread is about to shut down. When the main thread for an application is about
        /// to be shut down, this event is raised first, followed by an ApplicationExit</para>
        /// </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnThreadExit( object sender, EventArgs e )
        {
            try
            {
                _ = TraceLog.TraceLogger.LogInformation( "Saving assembly settings" );
                AppSettings.Instance.SaveSettings();
            }
            catch
            {
            }
            finally
            {
            }
            TraceLog.TraceLogger.CloseAndFlush();
        }

        /// <summary>   Event handler. Called by Network Change for change of network availability. </summary>
        /// <remarks>   David, 2021-03-15. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnNetworkAddressChanged( object sender, NetworkAvailabilityEventArgs e )
        {
        }


        #endregion

        #region " SETTINGS "

        /// <summary>   Reads the settings. </summary>
        /// <remarks>   David, 2021-03-15. </remarks>
        private void ReadSettings()
        {
            try
            {
                // Read the settings.
                AppSettings.Instance.ReadSettings();
            }
            catch ( Exception ex )
            {
                // Inform the user that an error occurred.
                _ = ex.AddExceptionData();
                string message = "An error occurred while attempting to read the program settings";
                _ = TraceLog.TraceLogger.LogError( ex, message );
                _ = MessageBox.Show( $"{message}. The error is: {ex.BuildMessage()}", "Settings error occurred" );

                // Exit the current thread instead of showing the windows.
                this.ExitThread();
            }
        }

        /// <summary>   Gets the <see cref="My.MyProject.Settings"/> configuration information instance. </summary>
        /// <value> The <see cref="Settings"/> configuration information instance. </value>
        [System.ComponentModel.Design.HelpKeyword( "MyApplicationContext.Settings" )]
        internal static Settings Settings => AppSettings.Instance.Settings;

        #endregion

        #region " PROPERTIES "

        /// <summary>   Gets a value indicating whether the high DPI mode was set. </summary>
        /// <value> True if high DPI was set, false if not. </value>
        public static bool HighDpiModeWasSet { get; private set; }

        #endregion

    }

    #region " SINGLE INSTANCE IMPLEMENTATION "

    // this class just wraps some Win32 stuff that we're going to use
    internal class NativeMethods
    {
        public const int HWND_BROADCAST = 0xffff;

        public static readonly int WM_SHOWME = RegisterWindowMessage( "WM_SHOWME" );

        [DllImport( "user32" )]
        public static extern bool PostMessage( IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam );

        [DllImport( "user32" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "<Pending>" )]
        public static extern int RegisterWindowMessage( string message );
    }

    #endregion

}

