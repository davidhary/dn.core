using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.Examples.ExceptionExtensions;

namespace isr.Core.Examples.My
{
    internal partial class MyApplication
    {

        #region " EVENT HANDLING METHODS "

        /// <summary>   Releases the splash. </summary>
        /// <remarks>   David, 2020-09-30. </remarks>
        internal void ReleaseSplashScreen()
        {
            MyProject.Forms.MySplashScreen.Close();
            MyProject.Forms.MySplashScreen.Dispose();
            this.SplashScreen = null;
        }

        /// <summary>   Creates splash screen. </summary>
        /// <remarks>   David, 2020-09-30. </remarks>
        internal void CreateSplashScreen()
        {
            string message = TraceLog.TraceLogger.LogVerbose( "Creating splash screen" );
            (( MySplashScreen ) this.SplashScreen)?.DisplayMessage( message );
            this.MinimumSplashScreenDisplayTime = ( int ) My.MyProject.Settings.SplashFormDisplayDuration.TotalMilliseconds;
            this.SplashScreen = MyProject.Forms.MySplashScreen;
            MySplashScreen.CreateInstance( MyProject.Application.SplashScreen );
        }

        /// <summary>   Determines if user requested a close. </summary>
        /// <remarks>   David, 2020-09-30. </remarks>
        /// <returns>   True if close requested; false otherwise. </returns>
        internal bool UserCloseRequested()
        {
            return ((( MySplashScreen ) this.SplashScreen)?.IsCloseRequested).GetValueOrDefault( false );
        }


        /// <summary> Instantiates the application to its known state. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <returns> <c>True</c> if success or <c>False</c> if failed. </returns>
        private bool TryInitializeKnownState()
        {
            string activity = string.Empty;
            try
            {
                Cursor.Current = Cursors.AppStarting;

                // show status
                activity = Debugger.IsAttached ? "Application is initializing in design mode." : "Application is initializing in runtime mode.";
                _ = TraceLog.TraceLogger.LogVerbose( activity );
                activity = "handle network availability change";
                this.HandleNetworkAvailabilityChanged();

                return true;
            }
            catch ( Exception ex )
            {

                // Turn off the hourglass
                Cursor.Current = Cursors.Default;
                _ = ExceptionDataMethods.AddExceptionData( ex );
                _ = TraceLog.TraceLogger.LogError( ex, $"Exception {activity}" );
                try
                {
                    this.ReleaseSplashScreen();
                }
                finally
                {
                }

                return false;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }


        /// <summary> Processes the shut down. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        private void ProcessShutDown()
        {
            MyProject.Application.SaveMySettingsOnExit = true;
            if ( MyProject.Application.SaveMySettingsOnExit )
            {
                // Save library settings here
            }
        }


        /// <summary>
        /// Processes the startup. Sets the event arguments
        /// <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs"/> cancel
        /// value if failed.
        /// </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="e"> The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" />
        /// instance containing the event data. </param>
        private static (bool Success, string Details) ProcessStartup( Microsoft.VisualBasic.ApplicationServices.StartupEventArgs e )
        {
            (bool Success, string Details) result = (true, string.Empty);
            if ( !e.Cancel )
            {
                string activity = string.Empty;
                try
                {
                    isr.Core.TraceLog.TraceLogger.Instance.MinimumLogLevel = AppSettings.Instance.Settings.ApplicationLogLevel;

                    activity = "creating splash screen instance";
                    MySplashScreen.CreateInstance( MyProject.Application.SplashScreen );

                    activity = "using splash screen";
                    _ = TraceLog.TraceLogger.LogVerbose( activity );

                }
                catch ( Exception ex )
                {
                    _ = TraceLog.TraceLogger.LogError( $"Exception {activity}" );
                    result = (false, $"Exception {activity};. {ex.BuildMessage()}");
                    _ = TraceLog.TraceLogger.LogError( ex, $"Exception {activity}" );
                }
            }
            e.Cancel = !result.Success;
            return result;
        }

        #endregion

        #region " NETWORK EVENTS "

        /// <summary> Occurs when the network connection is connected or disconnected. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Network available event information. </param>
        private void HandleNetworkAvailabilityChanged( object sender, Microsoft.VisualBasic.Devices.NetworkAvailableEventArgs e )
        {
        }

        #endregion

    }
}
