using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.Core.Examples
{
    /// <summary>   A settings. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    [isr.Json.SettingsSection( nameof( Settings ) )]
    internal class Settings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public Settings() : base( System.Reflection.Assembly.GetAssembly( typeof( Settings ) ) )
        { }

        private TimeSpan _SplashFormDisplayDuration;

        /// <summary>   Gets or sets the duration of the splash form display. </summary>
        /// <value> The splash form display duration. </value>
        [Description( "The duration of the splash form display" )]
        public TimeSpan SplashFormDisplayDuration
        {
            get => this._SplashFormDisplayDuration;
            set {
                if ( !TimeSpan.Equals( this.SplashFormDisplayDuration, value ) )
                {
                    this._SplashFormDisplayDuration = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private Microsoft.Extensions.Logging.LogLevel _ApplicationLogLevel = Microsoft.Extensions.Logging.LogLevel.Trace;
        /// <summary>   Gets or sets the application log level. </summary>
        /// <remarks>
        /// The minimal level for logging events at the application level. The logger logs the message if
        /// the message level is the same or higher than this level.
        /// </remarks>
        /// <value> The application log level. </value>
        [Description( "The minimal level for logging events at the application level. The logger logs the message if the message level is the same or higher than this level" )]
        public Microsoft.Extensions.Logging.LogLevel ApplicationLogLevel
        {
            get => this._ApplicationLogLevel;
            set {
                if ( !Microsoft.Extensions.Logging.LogLevel.Equals( value, this.ApplicationLogLevel ) )
                {
                    this._ApplicationLogLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private Microsoft.Extensions.Logging.LogLevel _AssemblyLogLevel = Microsoft.Extensions.Logging.LogLevel.Trace;
        /// <summary>   Gets or sets the assembly log level. </summary>
        /// <remarks>
        /// The minimum log level for sending the message to the logger by this assembly. This
        /// filters the message before it is sent to the Logging program.
        /// </remarks>
        /// <value> The assembly log level. </value>
        [Description( "The minimum log level for sending the message to the logger by this assembly. This filters the message before it is sent to the Logging program." )]
        public Microsoft.Extensions.Logging.LogLevel AssemblyLogLevel
        {
            get => this._AssemblyLogLevel;
            set {
                if ( !Microsoft.Extensions.Logging.LogLevel.Equals( value, this.AssemblyLogLevel ) )
                {
                    this._AssemblyLogLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private TraceEventType _MessageDisplayLevel = TraceEventType.Verbose;
        /// <summary>   Gets or sets the display level for log and trace messages. </summary>
        /// <remarks>
        /// The maximum trace event type for displaying logged and trace events. Only messages with a
        /// message <see cref="System.Diagnostics.TraceEventType"/> level that is same or higher than
        /// this level are displayed.
        /// </remarks>
        /// <value> The message display level. </value>
        [Description( "The maximum trace event type for displaying logged and trace events. Only messages with a message a level that is same or higher than this level are displayed." )]
        public TraceEventType MessageDisplayLevel
        {
            get => this._MessageDisplayLevel;
            set {
                if ( !TraceEventType.Equals( value, this.MessageDisplayLevel ) )
                {
                    this._MessageDisplayLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

    }
}

