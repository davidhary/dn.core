using System;
using System.Threading;
using System.Windows.Forms;

using Microsoft.Extensions.CommandLineUtils;

namespace isr.Core.Examples
{
    internal static class Program
    {
        /// <summary>   The main entry point for the application. </summary>
        /// <remarks>
        /// https://stackoverflow.com/questions/19147/what-is-the-correct-way-to-create-a-single-instance-wpf-application
        /// 
        /// Having a named mutex allows us to stack synchronization across multiple threads and processes
        /// which is just the magic I'm looking for.
        /// 
        /// Mutex.WaitOne has an overload that specifies an amount of time for us to wait. Since we're
        /// not actually wanting to synchronizing our code (more just check if it is currently in use) we
        /// use the overload with two parameters: Mutex.WaitOne(Timespan timeout, bool exitContext). Wait
        /// one returns true if it is able to enter, and false if it wasn't. In this case, we don't want
        /// to wait at all; If our mutex is being used, skip it, and move on, so we pass in TimeSpan.Zero
        /// (wait 0 milliseconds), and set the exitContext to true so we can exit the synchronization
        /// context before we try to acquire a lock on it.
        /// 
        /// So, if our app is running, WaitOne will return false, otherwise, I opted to utilize a little
        /// Win32 to notify my running instance that someone forgot that it was already running( by
        /// bringing itself to the top of all the other windows). To achieve this I used PostMessage to
        /// broadcast a custom message to every window( the custom message was registered with
        /// RegisterWindowMessage by my running application, which means only my application knows what
        /// it is) then my second instance exits.The running application instance would receive that
        /// notification and process it.In order to do that, I overrode WndProc in my main form and
        /// listened for my custom notification.When I received that notification I set the form's
        /// TopMost property to true to bring it up on top.
        /// </remarks>
        /// <param name="args"> An array of command-line argument strings. </param>
        [STAThread]
        private static void Main( string[] args )
        {
            if ( Mutex.WaitOne( TimeSpan.Zero, true ) )
            {
                // create the command line application class, which parses the command
                // line and define the program execution.
                // The command line application may encapsulate and parse the command line 
                // arguments, in which case it should be exposed or configure the parameters of
                // another static class. 
                var app = new MyCommandLineApplication();

                try
                {
                    // This begins the actual execution of the application
                    Console.WriteLine( "Command Line Tester started..." );
                    int outcome = app.Execute( args );
                    Console.WriteLine( $"\nCommand Line Tester exited with code {outcome}" );
                }
                catch ( CommandParsingException ex )
                {
                    // You'll always want to catch this exception, otherwise it will generate a messy and confusing error for the end user.
                    // the message will usually be something like:
                    // "Unrecognized command or argument '<invalid-command>'"
                    _ = MessageBox.Show( $"Exception; {Environment.NewLine}{ex.Message }.", "Command Line Exception", MessageBoxButtons.OK, MessageBoxIcon.Error );
                }
                catch ( Exception ex )
                {
                    _ = MessageBox.Show( $"Unable to execute application: {Environment.NewLine}{ex.Message }.", "Unable to start the application", MessageBoxButtons.OK, MessageBoxIcon.Error );
                }

                Mutex.ReleaseMutex();
            }
            else
            {
                // send our Win32 message to make the currently running instance
                // jump on top of all the other windows
                _ = NativeMethods.PostMessage( ( IntPtr ) NativeMethods.HWND_BROADCAST, NativeMethods.WM_SHOWME, IntPtr.Zero, IntPtr.Zero );
            }

        }

        /// <summary>   The mutex. </summary>
        private static readonly Mutex Mutex = new( true, "{A537FB8D-B9EB-46AD-89C2-6396EB16411A}" );

    }
}
