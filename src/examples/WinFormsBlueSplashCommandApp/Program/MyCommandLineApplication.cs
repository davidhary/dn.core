using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

using Microsoft.Extensions.CommandLineUtils;

namespace isr.Core.Examples
{
    internal class MyCommandLineApplication : CommandLineApplication
    {

        #region " CONSTRUCTION "

        public MyCommandLineApplication() : base()
        {

            // This should be the name of the executable itself. the help text line "Usage: ConsoleArgs" uses this
            this.Name = "WinFormsBlueSplashCommandApp";
            this.Description = "Windows Forms Blue Splash Command Line Application";
            this.ExtendedHelpText = @"This program demonstrates starting a WinForms application with command line, logging and settings.";

            // Set the arguments to display the description and help text
            _ = this.HelpOption( "-?|-h|--help" );

            // This is a helper/shortcut method to display version info - it is creating a regular Option, with some defaults.
            // The default help text is "Show version Information"
            _ = this.VersionOption( "-v|--version", () => {
                return $"Version {Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion}";
            } );

            this.DefineOptions();
            this.DefineArguments();
            this.DefineExecution();
        }

        #endregion 

        #region " DEFINE OPTIONS, ARGUMENTS AND COMMANDS "

        /// <summary>   Define application options. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        /// <returns>   A CommandOption. </returns>
        private void DefineOptions()
        {
            // Arguments: -o listener -l error -d ..\_log -f Column

            // The first argument is the option template.
            // It starts with a pipe-delimited list of option flags/names to use
            // Optionally, It is then followed by a space and a short description of the value to specify.
            // e.g. here we could also just use "-o|--option"
            _ = this.Option( "-o|--option <value>", "Listener or Provider Logging implementation", CommandOptionType.SingleValue );
            _ = this.Option( "-l|--level <value>", "Logging level: Information, Warning, Error", CommandOptionType.SingleValue );
            _ = this.Option( "-d|--dir <value>", "Relative Directory, e.g., ..\\_log", CommandOptionType.SingleValue );
            _ = this.Option( "-f|--form <value>", "Logging format: Column, Row, Tab, Comma", CommandOptionType.SingleValue );
        }

        /// <summary>   Enumerates defined command line arguments in this collection. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process define command arguments in this
        /// collection.
        /// </returns>
        private void DefineArguments()
        {
            // Arguments are basic arguments, that are parsed in the order they are given
            // e.g ConsoleArgs "first value" "second value"
            // This is OK for really simple tasks, but generally you're better off using Options
            // since they avoid confusion
            _ = this.Argument( "Option", "Listener or Provider Logging implementation" );
            _ = this.Argument( "Level", "Logging Level" );
            _ = this.Argument( "Directory", "Logging relative folder" );
            _ = this.Argument( "Format", "Logging format: Column, Rows, Tab, Comma" );
        }

        /// <summary>   Defines a simple command execution. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void DefineSimpleCommand()
        {
            // This is a command with no arguments - it just does default action.
            _ = this.Command( "simple-command", ( command ) => {
                //description and help text of the command.
                command.Description = "This is the description for simple-command.";
                command.ExtendedHelpText = "This is the extended help text for simple-command.";
                _ = command.HelpOption( "-?|-h|--help" );

                command.OnExecute( () => {
                    Console.WriteLine( "simple-command is executing" );

                    //Do the command's work here, or via another object/method

                    Console.WriteLine( "simple-command has finished." );
                    return 0; //return 0 on a successful execution
                } );

            } );
        }

        /// <summary>   Defines a complex command. </summary>
        /// <remarks>   David, 2021-02-03. </remarks>
        /// <param name="app">  The application. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void DefineComplexCommand()
        {

            _ = this.Command( "complex-command", ( command ) => {
                // This is a command that has it's own options.
                command.ExtendedHelpText = "This is the extended help text for complex-command.";
                command.Description = "This is the description for complex-command.";
                _ = command.HelpOption( "-?|-h|--help" );

                // There are 3 possible option types:
                // NoValue
                // SingleValue
                // MultipleValue

                // MultipleValue options can be supplied as one or multiple arguments
                // e.g. -m valueOne -m valueTwo -m valueThree
                var multipleValueOption = command.Option( "-m|--multiple-option <value>",
                    "A multiple-value option that can be specified multiple times",
                    CommandOptionType.MultipleValue );

                // SingleValue: A basic Option with a single value
                // e.g. -s sampleValue
                var singleValueOption = command.Option( "-s|--single-option <value>",
                    "A basic single-value option",
                    CommandOptionType.SingleValue );

                // NoValue are basically booleans: true if supplied, false otherwise
                var booleanOption = command.Option( "-b|--boolean-option",
                    "A true-false, no value option",
                    CommandOptionType.NoValue );

                command.OnExecute( () => {
                    Console.WriteLine( "complex-command is executing" );

                    // Do the command's work here, or via another object/method                    

                    // Grab the values of the various options. when not specified, they will be null.

                    // The NoValue type has no Value property, just the HasValue() method.
                    bool booleanOptionValue = booleanOption.HasValue();

                    // MultipleValue returns a List<string>
                    List<string> multipleOptionValues = multipleValueOption.Values;

                    // SingleValue returns a single string
                    string singleOptionValue = singleValueOption.Value();

                    // Check if the various options have values and display them.
                    // Here we're checking HasValue() to see if there is a value before displaying the output.
                    // Alternatively, you could just handle nulls from the Value properties
                    if ( booleanOption.HasValue() )
                    {
                        Console.WriteLine( "booleanOption option: {0}", booleanOptionValue.ToString() );
                    }

                    if ( multipleValueOption.HasValue() )
                    {
                        Console.WriteLine( "multipleValueOption option(s): {0}", string.Join( ",", multipleOptionValues ) );
                    }

                    if ( singleValueOption.HasValue() )
                    {
                        Console.WriteLine( "singleValueOption option: {0}", singleOptionValue ?? "null" );
                    }

                    Console.WriteLine( "complex-command has finished." );
                    return 0; // return 0 on a successful execution
                } );
            } );

        }

        #endregion 

        #region " DEFINE EXECUTION AND PARSE COMMAND LINE"

        /// <summary>   Define execution. </summary>
        /// <remarks>   David, 2021-02-03. <para>
        /// In this case, the base option executes the 
        /// application context.
        /// </para></remarks>
        /// <param name="app">  The application. </param>
        private void DefineExecution()
        {

            // When no commands are specified, this block will execute.
            // This is the main "command"
            this.OnExecute( () => {

                if ( (this.Arguments?.Any()).GetValueOrDefault( false ) )
                    Console.WriteLine( "Command line arguments:" );
                // You can also use the Arguments collection to iterate through the supplied arguments
                int index = 0;
                foreach ( CommandArgument arg in this.Arguments )
                {
                    // assign values based on argument values and index.
                    Console.WriteLine( $"Arguments collection [{index}] = {arg.Value ?? "null"}" );
                    index += 1;
                }
                if ( (this.Arguments?.Any()).GetValueOrDefault( false ) )
                    Console.WriteLine();

                if ( (this.GetOptions()?.Any()).GetValueOrDefault( false ) )
                    Console.WriteLine( "Command line arguments:" );
                foreach ( CommandOption opt in this.GetOptions() )
                {
                    // assign values based on argument values and index.
                    Console.WriteLine( $"Option collection [{opt.LongName}] = {opt.Value() ?? "null"}" );
                }
                if ( (this.GetOptions()?.Any()).GetValueOrDefault( false ) )
                    Console.WriteLine();

                // select the 'option' command line argument
                CommandOption basicOption = this.GetOptions().Where( x => x.ShortName == "o" ).FirstOrDefault();

                // Use the HasValue() method to check if the option was specified
                if ( basicOption is object && basicOption.HasValue() )
                {
                    Console.WriteLine( $"{nameof( basicOption )} selected value: {basicOption.Value()}" );

                    // Create the MyApplicationContext, that derives from ApplicationContext,
                    // that manages when the application should exit.
                    MyApplicationContext context = new();

                    // Run the application with the specific context. It will exit when
                    // all forms are closed.
                    Application.Run( context );
                    // Application.Run( new Dashboard() );

                }
                else
                {
                    Console.WriteLine( $"{nameof( basicOption )} option not specified; {Environment.NewLine}{this.GetHelpText()}." );
                }
                return 0;
            } );

        }


        #endregion 

    }
}
