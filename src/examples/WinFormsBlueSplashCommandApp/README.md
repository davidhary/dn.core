# Windows Forms Blue Splash Command Application Example

This Windows Forms example application uses .NET 5.0 with JSon settings and 
Serilog Logging using .NET 5.0 Application Command design.

## Logging and Tracing
Logging is defined by the application setting JSon file. Trace listening,
which can be used to display logged commands, is enabled.

## Settings
Settings with saving capabilities is supported using JSon.

## Single Instance
Upon starting a second instance, the application notifies the existing instance and exists.

## Command Line
The program demonstrates the following implementations:

### Command Line Options
The program define a few command line options. The 'Option' command line arguments
is used to control how the program is ran whereas other option defines 
optional settings. Alternatively, options can be implemented as command line arguments. 
Also, the run option could be implemented using a single or complex command.

### Command Line Arguments
The command line could consist of an ordered list of arguments where the argument values
determine the functionality.

### Command Line Commands
The program demonstrates using a simple or complex command.

### Program Execution Plan
Actual implementation is defined in the execution function of the configuration application.

### Program Execution
Finally, upon execution, the configuration application runs per the defined execution plan.


