using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.WinControls.Extended.MSTest
{

    /// <summary>   (Unit Test Class) a drop down text box tests. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [TestClass()]
    public class DropDownTextBoxTests
    {

        /// <summary>   (Unit Test Method) tests drop down text box. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        [TestMethod()]
        public void DropDownTextBoxAutoSizeDefaultShouldBeFalse()
        {
            using var target = new DropDownTextBox();
            bool expected = false;
            bool actual = target.AutoSize;
            Assert.AreEqual( expected, actual, $"{nameof( DropDownTextBox.AutoSize )} should match" );
        }

    }

}
