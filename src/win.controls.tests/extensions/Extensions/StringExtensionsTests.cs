using System;

using isr.Core.WinControls.CompactExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.WinControls.MSTest
{

    /// <summary> String extensions tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-14 </para>
    /// </remarks>
    [TestClass()]
    public class StringExtensionsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " COMPACT TESTS "

        /// <summary> (Unit Test Method) tests compact string. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod]
        public void CompactStringTest()
        {
            string candidate = "Compact this string";
            string compactedValue = string.Empty;
            string expcetedValue;
            using ( var textBox = new System.Windows.Forms.TextBox() { Width = 100, Font = new System.Drawing.Font( System.Drawing.FontFamily.GenericSansSerif, 10f ), AutoSize = false } )
            {
                compactedValue = candidate.Compact( textBox, System.Windows.Forms.TextFormatFlags.EndEllipsis );
                expcetedValue = "Compact thi...";
                Assert.AreEqual( expcetedValue, compactedValue, $"Failed compacting in a text box with {System.Windows.Forms.TextFormatFlags.EndEllipsis}" );
                compactedValue = candidate.Compact( textBox, System.Windows.Forms.TextFormatFlags.PathEllipsis );
                expcetedValue = "pact t...his str";
                Assert.AreEqual( expcetedValue, compactedValue, $"Failed compacting in a text box with {System.Windows.Forms.TextFormatFlags.PathEllipsis}" );
                compactedValue = candidate.Compact( textBox, System.Windows.Forms.TextFormatFlags.WordEllipsis );
                expcetedValue = "...ct this string";
                Assert.AreEqual( expcetedValue, compactedValue, $"Failed compacting in a text box with {System.Windows.Forms.TextFormatFlags.WordEllipsis}" );
            }

            using ( var label = new System.Windows.Forms.ToolStripStatusLabel() { Width = 100, Font = new System.Drawing.Font( System.Drawing.FontFamily.GenericSansSerif, 10f ), AutoSize = false } )
            {
                compactedValue = candidate.Compact( label, System.Windows.Forms.TextFormatFlags.PathEllipsis );
            }

            expcetedValue = "pact t...his str";
            Assert.AreEqual( expcetedValue, compactedValue, $"Failed compacting in a tool strip label with {System.Windows.Forms.TextFormatFlags.PathEllipsis}" );
        }

        #endregion

    }
}
