using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.WinControls.MSTest
{

    /// <summary>
    /// This is a test class for PropertyPublisherBaseTest and is intended to contain all
    /// PropertyPublisherBaseTest Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass()]
    public class PropertyPublisherBaseTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " PROPERTY CHANGE MANAGEMENT "

        /// <summary> A property change wrapper. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        private class PropertyChangeWrapper : INotifyPropertyChanged
        {

            /// <summary>
            /// Initializes a new instance of the <see cref="PropertyChangeWrapper" /> class.
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            public PropertyChangeWrapper() : base()
            {
                this.SyncContext = SynchronizationContext.Current;
            }

            /// <summary> Propagates a sync context. </summary>
            /// <value> The synchronization context. </value>
            public SynchronizationContext SyncContext { get; set; }

            /// <summary> Event that is raised when a property value changes. </summary>
            public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

            public delegate void PropertyChangedEventHandler( object sender, PropertyChangedEventArgs e );

            /// <summary> Gets the test value. </summary>
            /// <value> The test value. </value>
            public string TestValue { get; set; }

            /// <summary>
            /// Synchronously notifies (Invokes) a change
            /// <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
            /// <see cref="SynchronizationContext">sync context</see>
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="e"> Property changed event information. </param>
            private void InvokePropertyChanged( PropertyChangedEventArgs e )
            {
                this.PropertyChanged?.Invoke( this, e );
            }

            /// <summary>
            /// Synchronously notifies (Invokes) a change
            /// <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
            /// <see cref="SynchronizationContext">sync context</see>
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="obj"> The object. </param>
            private void InvokePropertyChanged( object obj )
            {
                this.InvokePropertyChanged( ( PropertyChangedEventArgs ) obj );
            }

            /// <summary>
            /// Synchronously notifies (Invokes or Sends) a change
            /// <see cref="PropertyChanged">event</see> in Property Value.
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="name"> The property name. </param>
            /// <returns> A TimeSpan. </returns>
            public TimeSpan SendPropertyChanged( string name )
            {
                if ( !string.IsNullOrWhiteSpace( name ) )
                {
                    if ( this.SyncContext is null )
                    {
                        // It is possible that the current thread does not have a synchronization context object; or
                        // a synchronization context has not been set for this thread. Creating an instance of a 
                        // synchronization context can yield unexpected results if the Current property of the 
                        // synchronization context is checked from a thread other than the thread on which the UI 
                        // is running, in which case the context will be null. Rather, we try to check the current
                        // property when the event is called. 
                        this.SyncContext = SynchronizationContext.Current;
                    }

                    var timer = Stopwatch.StartNew();
                    if ( this.SyncContext is null )
                    {
                        var sc = new SynchronizationContext();
                        sc.Send( new SendOrPostCallback( this.InvokePropertyChanged ), new PropertyChangedEventArgs( name ) );
                    }
                    else
                    {
                        this.SyncContext.Send( new SendOrPostCallback( this.InvokePropertyChanged ), new PropertyChangedEventArgs( name ) );
                    }

                    timer.Stop();
                    return timer.Elapsed;
                }

                return TimeSpan.Zero;
            }

            /// <summary>
            /// Synchronously notifies (Invokes) a change
            /// <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
            /// <see cref="SynchronizationContext">sync context</see>
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="e"> Property changed event information. </param>
            private void TryInvokePropertyChanged( PropertyChangedEventArgs e )
            {
                try
                {
                    this.PropertyChanged?.Invoke( this, e );
                }
                catch ( Exception ex )
                {
                    Trace.TraceError( $"Exception handling property '{e.PropertyName}';. {ex}" );
                }
            }

            /// <summary>
            /// Synchronously notifies (Invokes) a change
            /// <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
            /// <see cref="SynchronizationContext">sync context</see>
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="obj"> The object. </param>
            private void TryInvokePropertyChanged( object obj )
            {
                this.TryInvokePropertyChanged( ( PropertyChangedEventArgs ) obj );
            }

            /// <summary>
            /// Synchronously notifies (Invokes or Sends) a change
            /// <see cref="PropertyChanged">event</see> in Property Value.
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="name"> The property name. </param>
            /// <returns> A TimeSpan. </returns>
            public TimeSpan TrySendPropertyChanged( string name )
            {
                if ( !string.IsNullOrWhiteSpace( name ) )
                {
                    if ( this.SyncContext is null )
                    {
                        // It is possible that the current thread does not have a synchronization context object; or
                        // a synchronization context has not been set for this thread. Creating an instance of a 
                        // synchronization context can yield unexpected results if the Current property of the 
                        // synchronization context is checked from a thread other than the thread on which the UI 
                        // is running, in which case the context will be null. Rather, we try to check the current
                        // property when the event is called. 
                        this.SyncContext = SynchronizationContext.Current;
                    }

                    var timer = Stopwatch.StartNew();
                    if ( this.SyncContext is null )
                    {
                        var sc = new SynchronizationContext();
                        sc.Send( new SendOrPostCallback( this.TryInvokePropertyChanged ), new PropertyChangedEventArgs( name ) );
                    }
                    else
                    {
                        this.SyncContext.Send( new SendOrPostCallback( this.TryInvokePropertyChanged ), new PropertyChangedEventArgs( name ) );
                    }

                    timer.Stop();
                    return timer.Elapsed;
                }

                return TimeSpan.Zero;
            }

            /// <summary>
            /// Synchronously notifies (Invokes or Sends) a change
            /// <see cref="PropertyChanged">event</see> in Property Value.
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="name"> The property name. </param>
            /// <returns> A TimeSpan. </returns>
            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1825:Avoid zero-length array allocations.", Justification = "<Pending>" )]
            public TimeSpan DynamicInvokePropertyChanged( string name )
            {
                if ( !string.IsNullOrWhiteSpace( name ) )
                {
                    if ( this.SyncContext is null )
                    {
                        // It is possible that the current thread does not have a synchronization context object; or
                        // a synchronization context has not been set for this thread. Creating an instance of a 
                        // synchronization context can yield unexpected results if the Current property of the 
                        // synchronization context is checked from a thread other than the thread on which the UI 
                        // is running, in which case the context will be null. Rather, we try to check the current
                        // property when the event is called. 
                        this.SyncContext = SynchronizationContext.Current;
                    }

                    var timer = Stopwatch.StartNew();
                    if ( this.SyncContext is null )
                    {
                        System.ComponentModel.PropertyChangedEventHandler handler = null;
                        handler = this.PropertyChanged;
                        foreach ( Delegate d in handler is null ? (new Delegate[] { }) : handler.GetInvocationList() )
                        {
                            _ = d.DynamicInvoke( new object[] { this, new PropertyChangedEventArgs( name ) } );
                        }
                    }
                    else
                    {
                        this.SyncContext.Send( new SendOrPostCallback( this.InvokePropertyChanged ), new PropertyChangedEventArgs( name ) );
                    }

                    timer.Stop();
                    return timer.Elapsed;
                }

                return TimeSpan.Zero;
            }

            /// <summary>
            /// Synchronously notifies (Invokes or Sends) a change
            /// <see cref="PropertyChanged">event</see> in Property Value.
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="name"> The property name. </param>
            /// <returns> A TimeSpan. </returns>
            public TimeSpan InvokePropertyChanged( string name )
            {
                if ( !string.IsNullOrWhiteSpace( name ) )
                {
                    if ( this.SyncContext is null )
                    {
                        // It is possible that the current thread does not have a synchronization context object; or
                        // a synchronization context has not been set for this thread. Creating an instance of a 
                        // synchronization context can yield unexpected results if the Current property of the 
                        // synchronization context is checked from a thread other than the thread on which the UI 
                        // is running, in which case the context will be null. Rather, we try to check the current
                        // property when the event is called. 
                        this.SyncContext = SynchronizationContext.Current;
                    }

                    var timer = Stopwatch.StartNew();
                    if ( this.SyncContext is null )
                    {
                        // System.ComponentModel.PropertyChangedEventHandler handler = null;
                        // Debug.Assert(Not Debugger.IsAttached, "This could be slow!")
                        // begin invoke must not have more than one target. 
                        // handler.Be ginInvoke(Me, e, Nothing, Nothing)
                        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( name ) );
                    }
                    else
                    {
                        this.SyncContext.Send( new SendOrPostCallback( this.InvokePropertyChanged ), new PropertyChangedEventArgs( name ) );
                    }

                    timer.Stop();
                    return timer.Elapsed;
                }

                return TimeSpan.Zero;
            }

            /// <summary>
            /// Asynchronously notifies (Invokes or Sends) a change
            /// <see cref="PropertyChanged">event</see> in Property Value.
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="name"> The property name. </param>
            /// <returns> A TimeSpan. </returns>
            public TimeSpan PostPropertyChanged( string name )
            {
                if ( !string.IsNullOrWhiteSpace( name ) )
                {
                    if ( this.SyncContext is null )
                    {
                        // It is possible that the current thread does not have a synchronization context object; or
                        // a synchronization context has not been set for this thread. Creating an instance of a 
                        // synchronization context can yield unexpected results if the Current property of the 
                        // synchronization context is checked from a thread other than the thread on which the UI 
                        // is running, in which case the context will be null. Rather, we try to check the current
                        // property when the event is called. 
                        this.SyncContext = SynchronizationContext.Current;
                    }

                    var timer = Stopwatch.StartNew();
                    if ( this.SyncContext is null )
                    {
                        var sc = new SynchronizationContext();
                        sc.Post( new SendOrPostCallback( this.InvokePropertyChanged ), new PropertyChangedEventArgs( name ) );
                    }
                    else
                    {
                        this.SyncContext.Post( new SendOrPostCallback( this.InvokePropertyChanged ), new PropertyChangedEventArgs( name ) );
                    }

                    timer.Stop();
                    return timer.Elapsed;
                }

                return TimeSpan.Zero;
            }

            /// <summary>
            /// Asynchronously notifies (Invokes or Sends) a change
            /// <see cref="PropertyChanged">event</see> in Property Value.
            /// </summary>
            /// <remarks> David, 2020-09-23. </remarks>
            /// <param name="name"> The property name. </param>
            /// <returns> A TimeSpan. </returns>
            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1825:Avoid zero-length array allocations.", Justification = "<Pending>" )]
            public TimeSpan SaveInvokePropertyChanged( string name )
            {
                if ( !string.IsNullOrWhiteSpace( name ) )
                {
                    if ( this.SyncContext is null )
                    {
                        // It is possible that the current thread does not have a synchronization context object; or
                        // a synchronization context has not been set for this thread. Creating an instance of a 
                        // synchronization context can yield unexpected results if the Current property of the 
                        // synchronization context is checked from a thread other than the thread on which the UI 
                        // is running, in which case the context will be null. Rather, we try to check the current
                        // property when the event is called. 
                        this.SyncContext = SynchronizationContext.Current;
                    }

                    var timer = Stopwatch.StartNew();
                    if ( this.SyncContext is null )
                    {
                        // Although the sync context is nothing, one of the target might still require invocation.
                        // Therefore, save invoke is implemented nonetheless.
                        System.ComponentModel.PropertyChangedEventHandler handler = null;
                        handler = this.PropertyChanged;
                        foreach ( Delegate d in handler is null ? (new Delegate[] { }) : handler.GetInvocationList() )
                        {
                            _ = d.Target is null
                                ? d.DynamicInvoke( new object[] { this, new PropertyChangedEventArgs( name ) } )
                                : d.Target is not ISynchronizeInvoke target
                                    ? d.DynamicInvoke( new object[] { this, new PropertyChangedEventArgs( name ) } )
                                    : target.Invoke( d, new object[] { this, new PropertyChangedEventArgs( name ) } );
                        }
                    }
                    // slower: e vt?.SafeInvoke(Me, New PropertyChangedEventArgs(name))
                    else
                    {
                        this.SyncContext.Send( new SendOrPostCallback( this.InvokePropertyChanged ), new PropertyChangedEventArgs( name ) );
                    }

                    timer.Stop();
                    return timer.Elapsed;
                }

                return TimeSpan.Zero;
            }
        }

        /// <summary>   Gets or sets the property changer. </summary>
        /// <value> The property changer. </value>
        private PropertyChangeWrapper PropertyChanger { get; set; }

        #endregion

        #region " SEND POST  "

        /// <summary> A test for Send Property Changed. </summary>
        /// <remarks> 0.152 ms. </remarks>
        [TestMethod()]
        public void SendPropertyChangedTest()
        {
            string expectedName = "TestValue";
            string expectedValue = "Sync";
            this.PropertyChanger = new PropertyChangeWrapper() { TestValue = expectedValue };
            _ = this.PropertyChanger.SendPropertyChanged( expectedName );
            // Assert.AreEqual( this.PropertyChanger.SyncContext, null );
            // ran twice to make sure things are compiled.
            var ts = this.PropertyChanger.SendPropertyChanged( expectedName );
            double expectedMaximumTimespan = 1d;
            double actualTimespan = ts.TotalMilliseconds;
            Assert.IsTrue( expectedMaximumTimespan > actualTimespan, $"Timespan {actualTimespan} is better than {expectedMaximumTimespan} ms" );
        }

        /// <summary> A test for Try Send Property Changed (handles exceptions). </summary>
        /// <remarks> 0.25 ms. </remarks>
        [TestMethod()]
        public void TrySendPropertyChangedTest()
        {
            string expectedName = "TestValue";
            string expectedValue = "Sync";
            this.PropertyChanger = new PropertyChangeWrapper() { TestValue = expectedValue };
            _ = this.PropertyChanger.TrySendPropertyChanged( expectedName );
            // Assert.AreEqual( this.PropertyChanger.SyncContext, null );
            // ran twice to make sure things are compiled.
            var ts = this.PropertyChanger.TrySendPropertyChanged( expectedName );
            double expectedMaximumTimespan = 1d;
            double actualTimespan = ts.TotalMilliseconds;
            Assert.IsTrue( expectedMaximumTimespan > actualTimespan, $"Timespan {actualTimespan} is better than {expectedMaximumTimespan} ms" );
        }

        /// <summary> A test for Post Property Changed. </summary>
        /// <remarks> 0.035 ms. </remarks>
        [TestMethod()]
        public void PostPropertyChangedTest()
        {
            string expectedName = "TestValue";
            string expectedValue = "Async";
            this.PropertyChanger = new PropertyChangeWrapper() { TestValue = expectedValue };
            _ = this.PropertyChanger.PostPropertyChanged( expectedName );
            // Assert.AreEqual( this.PropertyChanger.SyncContext, null );
            // ran twice to make sure things are compiled.
            var ts = this.PropertyChanger.PostPropertyChanged( expectedName );
            double expectedMaximumTimespan = 1d;
            double actualTimespan = ts.TotalMilliseconds;
            Assert.IsTrue( expectedMaximumTimespan > actualTimespan, $"Timespan {actualTimespan} is better than {expectedMaximumTimespan} ms" );
        }

        #endregion

        #region " INVOKE "

        /// <summary> A test for Dynamic Invoke. </summary>
        /// <remarks> 0.045 ms. </remarks>
        [TestMethod()]
        public void DynamicInvokePropertyChangedTest()
        {
            string expectedName = "TestValue";
            string expectedValue = "Sync";
            this.PropertyChanger = new PropertyChangeWrapper() { TestValue = expectedValue };
            _ = this.PropertyChanger.DynamicInvokePropertyChanged( expectedName );
            // Assert.AreEqual( this.PropertyChanger.SyncContext, null );
            // ran twice to make sure things are compiled.
            var ts = this.PropertyChanger.DynamicInvokePropertyChanged( expectedName );
            double expectedMaximumTimespan = 1d;
            double actualTimespan = ts.TotalMilliseconds;
            Assert.IsTrue( expectedMaximumTimespan > actualTimespan, $"Timespan {actualTimespan} is better than {expectedMaximumTimespan} ms" );
        }

        /// <summary> A test for Invoke Property Changed. </summary>
        /// <remarks> 0.037 ms. </remarks>
        [TestMethod()]
        public void InvokePropertyChangedTest()
        {
            string expectedName = "TestValue";
            string expectedValue = "Sync";
            this.PropertyChanger = new PropertyChangeWrapper() { TestValue = expectedValue };
            _ = this.PropertyChanger.InvokePropertyChanged( expectedName );
            // Assert.AreEqual( this.PropertyChanger.SyncContext, null );
            // ran twice to make sure things are compiled.
            var ts = this.PropertyChanger.InvokePropertyChanged( expectedName );
            double expectedMaximumTimespan = 1d;
            double actualTimespan = ts.TotalMilliseconds;
            Assert.IsTrue( expectedMaximumTimespan > actualTimespan, $"Timespan {actualTimespan} is better than {expectedMaximumTimespan} ms" );
        }

        /// <summary> A test for Safe Begin Invoke Property Changed. </summary>
        /// <remarks> 0.048 ms , 0.38 using extensions! </remarks>
        [TestMethod()]
        public void SafeInvokePropertyChangedTest()
        {
            string expectedName = "TestValue";
            string expectedValue = "Async";
            this.PropertyChanger = new PropertyChangeWrapper() { TestValue = expectedValue };
            _ = this.PropertyChanger.SaveInvokePropertyChanged( expectedName );
            // Assert.AreEqual( this.PropertyChanger.SyncContext, null );
            // ran twice to make sure things are compiled.
            var ts = this.PropertyChanger.SaveInvokePropertyChanged( expectedName );
            double expectedMaximumTimespan = 1d;
            double actualTimespan = ts.TotalMilliseconds;
            Assert.IsTrue( expectedMaximumTimespan > actualTimespan, $"Timespan {actualTimespan} is better than {expectedMaximumTimespan} ms" );
        }

        #endregion

    }
}
