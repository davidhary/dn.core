using System;
using System.Windows.Forms;

using isr.Core.WinControls.ControlExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.WinControls.MSTest
{

    [TestClass()]
    public class SafeSetterTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
            this._TextBox = new TextBox();
            _ = this._TextBox.Handle;
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        private TextBox _TextBox;


        [TestMethod()]
        public void SafeTextSetterTest()
        {
            string expectedValue = "test 1";
            string actualValue = this._TextBox.SafeTextSetter( expectedValue );
            Assert.AreEqual( expectedValue, actualValue, $"{ nameof( isr.Core.WinControls.ControlExtensions.ControlExtensionMethods.SafeTextSetter ) } value should match" );
            actualValue = this._TextBox.Text;
            Assert.AreEqual( expectedValue, actualValue, $"{ nameof( System.Windows.Forms.TextBox ) }.{ nameof( System.Windows.Forms.TextBox.Text ) } value should match" );
        }

        [TestMethod()]
        public void SafeStextSetterThreadTest()
        {
            var oThread = new System.Threading.Thread( new System.Threading.ThreadStart( this.SafeTextSetterTest ) );
            oThread.Start();
            // this blocks completion of this test.
            // oThread.Join();
        }
    }
}
