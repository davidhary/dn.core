using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.WinForms.LogForms
{

    /// <summary> A form that persists user settings in the Application Settings file. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-01-31, 6.1.4779 </para>
    /// </remarks>
    public partial class LoggerUserFormBase : System.Windows.Forms.Form
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Gets the initializing components sentinel. </summary>
        /// <value> The initializing components sentinel. </value>
        protected bool InitializingComponents { get; set; }

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        protected LoggerUserFormBase() : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.InitializingComponents = false;
        }

        /// <summary> Initializes the component. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
            this.AutoScaleMode = AutoScaleMode.Font;
            this.BackColor = SystemColors.Control;
            this.ClientSize = new Size( 331, 341 );
            this.Cursor = Cursors.Default;
            this.Icon = Properties.Resources.favicon;
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point );
            this.Name = nameof( LoggerUserFormBase );
            this.ResumeLayout( false );
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

    }
}
