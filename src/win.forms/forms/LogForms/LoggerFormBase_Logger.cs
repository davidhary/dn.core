using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using isr.Core.TraceLog;

using Microsoft.Extensions.Logging;

namespace isr.Core.WinForms.LogForms
{

    public partial class LoggerFormBase
    {

        #region " TEXT  WRITER "

        /// <summary>   Adds a display trace event writer. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="textWriter">   The trace Event tWriter. </param>
        public void AddDisplayTextWriter( Tracing.ITraceEventWriter textWriter )
        {
            textWriter.TraceLevel = this.DisplayTraceEventType;
            Tracing.TracingPlatform.Instance.AddTraceEventWriter( textWriter );
        }

        /// <summary>   Removes the display text writer described by <paramref name="textWriter"/>. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="textWriter">   The trace Event tWriter. </param>
        public static void RemoveDisplayTextWriter( Tracing.ITraceEventWriter textWriter )
        {
            Tracing.TracingPlatform.Instance.RemoveTraceEventWriter( textWriter );
        }

        #endregion

        #region " DISPLAY TRACE EVENT TYPE "

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> value name pair for the global trace event
        /// writer. This level determines the level of all the
        /// <see cref="Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
        /// lower level.
        /// </summary>
        /// <value> The trace event writer trace event value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<TraceEventType, string> TraceEventWriterTraceEventValueNamePair
        {
            get => TraceLogger.Instance.TraceEventWriterTraceEventValueNamePair;

            set {
                if ( !KeyValuePair<TraceEventType, string>.Equals( value, this.TraceEventWriterTraceEventValueNamePair ) )
                {
                    TraceLogger.Instance.TraceEventWriterTraceEventValueNamePair = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> for the global trace event writer. This level
        /// determines the level of all the
        /// <see cref="Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
        /// lower level.
        /// </summary>
        /// <value> The type of the trace event writer trace event. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType TraceEventWriterTraceEventType
        {
            get => TraceLogger.Instance.TraceEventWriterTraceEventType;

            set {
                if ( value != this.TraceEventWriterTraceEventType )
                {
                    TraceLogger.Instance.TraceEventWriterTraceEventType = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> value name pair for display.
        /// </summary>
        /// <value> The log level value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<TraceEventType, string> DisplayTraceEventTypeValueNamePair { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> for display.
        /// </summary>
        /// <value> The trace event writer log level. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType DisplayTraceEventType { get; set; }

        #endregion

        #region " LOGGING LOG LEVEL "

        /// <summary>
        /// Gets or sets the <see cref="Microsoft.Extensions.Logging.LogLevel"/> value name pair for logging. This level
        /// determines the level of the <see cref="ILogger"/>.
        /// </summary>
        /// <value> The log trace event value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<LogLevel, string> LoggingLevelValueNamePair
        {
            get => TraceLogger.Instance.LoggingLevelValueNamePair;

            set {
                if ( !KeyValuePair<LogLevel, string>.Equals( value, this.LoggingLevelValueNamePair ) )
                {
                    TraceLogger.Instance.LoggingLevelValueNamePair = value;
                }
            }
        }

        /// <summary>   Gets or sets <see cref="Microsoft.Extensions.Logging.LogLevel"/> value for logging. </summary>
        /// <value> The trace event type for logging. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LogLevel LoggingLogLevel
        {
            get => TraceLogger.Instance.MinimumLogLevel;

            set {
                if ( value != this.LoggingLogLevel )
                {
                    TraceLogger.Instance.MinimumLogLevel = value;
                }
            }
        }

        #endregion

        #region " LOG EXCEPTION "

        /// <summary>   Adds an exception data. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="ex">   The exception. </param>
        /// <returns>   True if exception data was added for this exception; otherwise, false. </returns>
        protected virtual bool AddExceptionData( Exception ex )
        {
            return false;
        }

        /// <summary>   Log exception. </summary>
        /// <remarks>
        /// Declared as must override so that the calling method could add exception data before
        /// recording this exception.
        /// </remarks>
        /// <param name="ex">               The ex. </param>
        /// <param name="activity">         The activity. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
        /// <returns>   A String. </returns>
        protected string LogError( Exception ex, string activity, [CallerMemberName] string memberName = "",
                                                                  [CallerFilePath] string sourceFilePath = "",
                                                                  [CallerLineNumber] int sourceLineNumber = 0 )
        {
            _ = this.AddExceptionData( ex );
            return TraceLog.TraceLogger.LogError( ex, activity, memberName, sourceFilePath, sourceLineNumber );
        }

        #endregion

    }
}
