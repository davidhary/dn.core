namespace isr.Core.WinForms.UserForms
{
    /// <summary>
    /// Reads and writes form start information to a Json configuration file.
    /// </summary>
    /// <remarks>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2009-10-21, 1.0.3581 </para>
    /// </remarks>
    public class UserFormInfo : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public UserFormInfo( string sectionName ) : base( System.Reflection.Assembly.GetAssembly( typeof( UserFormInfo ) ) , sectionName )
        { }

        /// <summary>   Gets or sets the start position. </summary>
        /// <value> The start position. </value>
        public System.Windows.Forms.FormStartPosition StartPosition { get; set; }

        /// <summary>   Gets or sets the state of the windows. </summary>
        /// <value> The windows state. </value>
        public System.Windows.Forms.FormWindowState WindowsState { get; set; }

        /// <summary>   Gets or sets the form size. </summary>
        /// <value> The size of the form. </value>
        public System.Drawing.Size Size { get; set; }

        /// <summary>   Gets or sets the location. </summary>
        /// <value> The location. </value>
        public System.Drawing.Point Location { get; set; }

    }
}
