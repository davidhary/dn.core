using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace isr.Core.WinForms.Metro.Blue
{

    /// <summary> Blue splash. </summary>
    /// <remarks>
    /// (c) 2015 Magyar András. All rights reserved.<para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-05-08, from Magyar András  </para><para>
    /// http://www.codeproject.com/Articles/804316/Office-Style-Splash-Screen. </para>
    /// </remarks>
    public partial class BlueSplash : isr.Core.WinForms.Metro.Forms.FormBase
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public BlueSplash()
        {
            this.Shown += this.Form_Shown;
            this.InitializeComponent();
            BlueSplash.Assembly = Assembly.GetCallingAssembly();
            this.UpdateInfo();
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary>
        /// Does all the post processing after all the form controls are rendered as the user expects
        /// them.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Form_Shown( object sender, EventArgs e )
        {
            System.Windows.Forms.Application.DoEvents();
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.Hand;
                this.CurrentTask = "Starting...";

                // instantiate form objects
                this.UpdateInfo();
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception Occurred", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary> Close Application. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Close_Click( object sender, EventArgs e )
        {
            this.IsCloseRequested = true;
        }

        /// <summary> Minimize label effects. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Minimize_Click( object sender, EventArgs e )
        {
            this.IsMinimizeRequested = true;
        }

        /// <summary> Closes label Mouse hover and leave effects </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Close_MouseHover( object sender, EventArgs e )
        {
            this._CloseLabel.ForeColor = Color.Silver;
        }

        /// <summary> Closes mouse leave. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Close_MouseLeave( object sender, EventArgs e )
        {
            this._CloseLabel.ForeColor = Color.White;
        }

        /// <summary> Minimize mouse hover. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Minimize_MouseHover( object sender, EventArgs e )
        {
            this._MinimizeLabel.ForeColor = Color.Silver;
        }

        /// <summary> Minimize mouse leave. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Minimize_MouseLeave( object sender, EventArgs e )
        {
            this._MinimizeLabel.ForeColor = Color.White;
        }
        #endregion

        #region " PROPERTIES and METHODS "

        /// <summary> Gets a value indicating whether this object is minimize requested. </summary>
        /// <value> <c>true</c> if this object is minimize requested; otherwise <c>false</c> </value>
        public bool IsMinimizeRequested { get; private set; }

        /// <summary> Gets a value indicating whether this object is close requested. </summary>
        /// <value> <c>true</c> if this object is close requested; otherwise <c>false</c> </value>
        public bool IsCloseRequested { get; private set; }

        /// <summary> Gets or sets the current task. </summary>
        /// <value> The current task. </value>
        public string CurrentTask
        {
            get => this._CurrentTaskLabel.Text;

            set {
                if ( !string.Equals( value, this.CurrentTask ) )
                {
                    _ = SafeTextSetter( this._CurrentTaskLabel, value );
                }
            }
        }

        /// <summary> true to topmost. </summary>
        private bool _Topmost;

        /// <summary> Sets the top most status in a thread safe way. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> true to value. </param>
        public void TopmostSetter( bool value )
        {
            this._Topmost = value;
            SafeTopMostSetter( this, value );
        }

        /// <summary> Displays a message on the splash screen. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The message. </param>
        public void DisplayMessage( string value )
        {
            this.CurrentTask = value;
        }

        /// <summary> Gets or sets the small application caption. </summary>
        /// <value> The small application caption. </value>
        public string SmallApplicationCaption
        {
            get => this._SmallApplicationCaptionLabel.Text;

            set {
                if ( !string.Equals( value, this.SmallApplicationCaption ) )
                {
                    _ = SafeTextSetter( this._SmallApplicationCaptionLabel, value );
                }
            }
        }

        /// <summary> Gets or sets the large application caption. </summary>
        /// <value> The large application caption. </value>
        public string LargeApplicationCaption
        {
            get => this._LargeApplicationCaptionLabel.Text;

            set {
                if ( !string.Equals( value, this.LargeApplicationCaption ) )
                {
                    _ = SafeTextSetter( this._LargeApplicationCaptionLabel, value );
                }
            }
        }

        /// <summary>   Gets or sets the assembly. </summary>
        /// <value> The assembly. </value>
        public static Assembly Assembly { get; set; }

        private static SizeF MeasureText( Font font, string text )
        {
            if ( font is null )
            {
                throw new ArgumentNullException( nameof( font ) );
            }

            using var ctrl = new System.Windows.Forms.Control();
            using var graphics = ctrl.CreateGraphics();
            return graphics.MeasureString( text, font );
        }

        /// <summary>   Select font. </summary>
        /// <remarks>   David, 2021-03-02. </remarks>
        /// <param name="largestSize">  Size of the largest. </param>
        /// <param name="width">        The width. </param>
        /// <param name="font">         The font. </param>
        /// <param name="text">         The text. </param>
        /// <returns>   A Font. </returns>
        private static Font SelectFont( Font font, float largestSize, float width, string text )
        {
            float increment = 1;
            float size = largestSize + increment;
            do
            {
                size -= increment;
                font = new Font( font.FontFamily, size, font.Style );
            }
            while ( MeasureText( font, text ).Width >= width );

            // this is required to fit the font inside.
            font = new Font( font.FontFamily, ( float ) (size - 0.5), font.Style );

            return font;
        }


        /// <summary> Update the information on screen. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void UpdateInfo()
        {
            string name = BlueSplash.Assembly.GetName().Name;
            Font font = SelectFont( this._LargeApplicationCaptionLabel.Font, 36, this.Width, name );
            this._LargeApplicationCaptionLabel.Font = font;
            this._LargeApplicationCaptionLabel.Text = name;
            this._SmallApplicationCaptionLabel.Text = $"{name} {BlueSplash.Assembly.GetName().Version}";
            this.TopMost = this._Topmost;
        }

        #endregion

        #region " THREAD SAFE METHODS "

        /// <summary> Safe height setter. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="value">   true to value. </param>
        private static void SafeHeightSetter( System.Windows.Forms.Control control, int value )
        {
            if ( control is object )
            {
                if ( control.InvokeRequired )
                {
                    _ = control.BeginInvoke( new Action<System.Windows.Forms.Control, int>( SafeHeightSetter ), new object[] { control, value } );
                }
                else
                {
                    control.Height = value;
                    control.Invalidate();
                    System.Windows.Forms.Application.DoEvents();
                }
            }
        }


        /// <summary>   Reads control text. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="control">   The variable control. </param>
        /// <returns>   The control text. </returns>
        public static string SafeTextGetter( System.Windows.Forms.Control control )
        {
            if ( control.InvokeRequired )
            {
                return ( string ) control.Invoke( new Func<String>( () => SafeTextGetter( control ) ) );
            }
            else
            {
                string varText = control.Text;
                return varText;
            }
        }

        /// <summary>   Writes a control text. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="control">   The variable control. </param>
        /// <param name="value">        The message. </param>
        /// <returns>   A string. </returns>
        public static string SafeTextSetter( System.Windows.Forms.Control control, string value )
        {
            if ( control is object )
            {
                if ( control.InvokeRequired )
                {
                    return ( string ) control.Invoke( new Func<String>( () => SafeTextSetter( control, value ) ) );
                }
                else
                {
                    control.Text = value;
                    control.Invalidate();
                    System.Windows.Forms.Application.DoEvents();
                    return control.Text;
                }
            }
            return value;
        }

        /// <summary>
        /// Sets the <see cref="System.Windows.Forms.Control">control</see> text to the
        /// <paramref name="value">value</paramref>.
        /// This setter is thread safe.
        /// </summary>
        /// <remarks> The value is set to empty if null or empty. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="value">   The value. </param>
        /// <returns> value. </returns>
        private static void SafeTextWriter( System.Windows.Forms.Control control, string value )
        {
            if ( control is object )
            {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }

                if ( control.InvokeRequired )
                {
                    _ = control.BeginInvoke( new Action<System.Windows.Forms.Control, string>( SafeTextWriter ), new object[] { control, value } );
                }
                else
                {
                    control.Text = value;
                    control.Invalidate();
                    System.Windows.Forms.Application.DoEvents();
                }
            }
        }

        /// <summary> Safe top most setter. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="form">  The form. </param>
        /// <param name="value"> true to value. </param>
        private static void SafeTopMostSetter( System.Windows.Forms.Form form, bool value )
        {
            if ( form is object )
            {
                if ( form.InvokeRequired )
                {
                    _ = form.BeginInvoke( new Action<System.Windows.Forms.Form, bool>( SafeTopMostSetter ), new object[] { form, value } );
                }
                else
                {
                    form.TopMost = value;
                    System.Windows.Forms.Application.DoEvents();
                }
            }
        }

        /// <summary> Display info. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void DisplayInfo()
        {
            if ( this.InvokeRequired )
            {
                _ = this.BeginInvoke( new MethodInvoker( this.UpdateInfo ) );
                // _ = this.BeginInvoke( new Action ( this.UpdateInfo ) );
            }
            else
            {
                this.UpdateInfo();
            }
        }

        #endregion

    }
}
