using System;
using System.Runtime.CompilerServices;

namespace isr.Core.WinForms.Metro.Blue
{
    public partial class BlueSplash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region "Windows Form Designer generated code"

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlueSplash));
            this._SmallApplicationCaptionLabel = new System.Windows.Forms.Label();
            this._LargeApplicationCaptionLabel = new System.Windows.Forms.Label();
            this._CurrentTaskLabel = new System.Windows.Forms.Label();
            this._CloseLabel = new System.Windows.Forms.Label();
            this._MinimizeLabel = new System.Windows.Forms.Label();
            this._MetroProgressBar = new isr.Core.WinForms.Metro.Metro.MetroProgressBar();
            this._FolderLabel = new System.Windows.Forms.Label();
            this._TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._AuthorLabel = new System.Windows.Forms.Label();
            this._TopPanel = new System.Windows.Forms.Panel();
            this._ProgressLayout = new System.Windows.Forms.TableLayoutPanel();
            this._TableLayoutPanel.SuspendLayout();
            this._TopPanel.SuspendLayout();
            this._ProgressLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // _SmallApplicationCaptionLabel
            // 
            this._SmallApplicationCaptionLabel.AutoSize = true;
            this._SmallApplicationCaptionLabel.BackColor = System.Drawing.Color.Transparent;
            this._SmallApplicationCaptionLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this._SmallApplicationCaptionLabel.ForeColor = System.Drawing.Color.White;
            this._SmallApplicationCaptionLabel.Location = new System.Drawing.Point(32, 0);
            this._SmallApplicationCaptionLabel.Name = "_SmallApplicationCaptionLabel";
            this._SmallApplicationCaptionLabel.Size = new System.Drawing.Size(124, 20);
            this._SmallApplicationCaptionLabel.TabIndex = 0;
            this._SmallApplicationCaptionLabel.Text = "Application Title";
            // 
            // _LargeApplicationCaptionLabel
            // 
            this._LargeApplicationCaptionLabel.AutoSize = true;
            this._LargeApplicationCaptionLabel.BackColor = System.Drawing.Color.Transparent;
            this._LargeApplicationCaptionLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this._LargeApplicationCaptionLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this._LargeApplicationCaptionLabel.ForeColor = System.Drawing.Color.White;
            this._LargeApplicationCaptionLabel.Location = new System.Drawing.Point(3, 110);
            this._LargeApplicationCaptionLabel.Name = "_LargeApplicationCaptionLabel";
            this._LargeApplicationCaptionLabel.Size = new System.Drawing.Size(506, 65);
            this._LargeApplicationCaptionLabel.TabIndex = 1;
            this._LargeApplicationCaptionLabel.Text = "Application";
            this._LargeApplicationCaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _CurrentTaskLabel
            // 
            this._CurrentTaskLabel.AutoEllipsis = true;
            this._CurrentTaskLabel.AutoSize = true;
            this._CurrentTaskLabel.BackColor = System.Drawing.Color.Transparent;
            this._CurrentTaskLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this._CurrentTaskLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this._CurrentTaskLabel.ForeColor = System.Drawing.Color.White;
            this._CurrentTaskLabel.Location = new System.Drawing.Point(3, 280);
            this._CurrentTaskLabel.Name = "_CurrentTaskLabel";
            this._CurrentTaskLabel.Size = new System.Drawing.Size(506, 21);
            this._CurrentTaskLabel.TabIndex = 2;
            this._CurrentTaskLabel.Text = "current task";
            // 
            // _CloseLabel
            // 
            this._CloseLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._CloseLabel.AutoSize = true;
            this._CloseLabel.BackColor = System.Drawing.Color.Transparent;
            this._CloseLabel.Font = new System.Drawing.Font("Webdings", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._CloseLabel.ForeColor = System.Drawing.Color.White;
            this._CloseLabel.Location = new System.Drawing.Point(467, 0);
            this._CloseLabel.Name = "_CloseLabel";
            this._CloseLabel.Size = new System.Drawing.Size(36, 26);
            this._CloseLabel.TabIndex = 3;
            this._CloseLabel.Text = "r";
            this._CloseLabel.Click += new System.EventHandler(this.Close_Click);
            this._CloseLabel.MouseLeave += new System.EventHandler(this.Close_MouseLeave);
            this._CloseLabel.MouseHover += new System.EventHandler(this.Close_MouseHover);
            // 
            // _MinimizeLabel
            // 
            this._MinimizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._MinimizeLabel.AutoSize = true;
            this._MinimizeLabel.BackColor = System.Drawing.Color.Transparent;
            this._MinimizeLabel.Font = new System.Drawing.Font("Webdings", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._MinimizeLabel.ForeColor = System.Drawing.Color.White;
            this._MinimizeLabel.Location = new System.Drawing.Point(425, 0);
            this._MinimizeLabel.Name = "_MinimizeLabel";
            this._MinimizeLabel.Size = new System.Drawing.Size(36, 26);
            this._MinimizeLabel.TabIndex = 4;
            this._MinimizeLabel.Text = "0";
            this._MinimizeLabel.Click += new System.EventHandler(this.Minimize_Click);
            this._MinimizeLabel.MouseLeave += new System.EventHandler(this.Minimize_MouseLeave);
            this._MinimizeLabel.MouseHover += new System.EventHandler(this.Minimize_MouseHover);
            // 
            // _MetroProgressBar
            // 
            this._MetroProgressBar.BackColor = System.Drawing.Color.Transparent;
            this._MetroProgressBar.Location = new System.Drawing.Point(73, 4);
            this._MetroProgressBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._MetroProgressBar.Name = "_MetroProgressBar";
            this._MetroProgressBar.Size = new System.Drawing.Size(359, 6);
            this._MetroProgressBar.TabIndex = 5;
            // 
            // _FolderLabel
            // 
            this._FolderLabel.AutoSize = true;
            this._FolderLabel.BackColor = System.Drawing.Color.Transparent;
            this._FolderLabel.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._FolderLabel.ForeColor = System.Drawing.Color.White;
            this._FolderLabel.Location = new System.Drawing.Point(3, 3);
            this._FolderLabel.Name = "_FolderLabel";
            this._FolderLabel.Size = new System.Drawing.Size(27, 16);
            this._FolderLabel.TabIndex = 6;
            this._FolderLabel.Text = "1";
            // 
            // _TableLayoutPanel
            // 
            this._TableLayoutPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(86)))), ((int)(((byte)(154)))));
            this._TableLayoutPanel.ColumnCount = 1;
            this._TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._TableLayoutPanel.Controls.Add(this._AuthorLabel, 0, 9);
            this._TableLayoutPanel.Controls.Add(this._TopPanel, 0, 1);
            this._TableLayoutPanel.Controls.Add(this._CurrentTaskLabel, 0, 7);
            this._TableLayoutPanel.Controls.Add(this._LargeApplicationCaptionLabel, 0, 3);
            this._TableLayoutPanel.Controls.Add(this._ProgressLayout, 0, 5);
            this._TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._TableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this._TableLayoutPanel.Name = "_TableLayoutPanel";
            this._TableLayoutPanel.RowCount = 10;
            this._TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this._TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this._TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this._TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._TableLayoutPanel.Size = new System.Drawing.Size(512, 324);
            this._TableLayoutPanel.TabIndex = 7;
            // 
            // _AuthorLabel
            // 
            this._AuthorLabel.AutoSize = true;
            this._AuthorLabel.BackColor = System.Drawing.Color.Transparent;
            this._AuthorLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this._AuthorLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._AuthorLabel.ForeColor = System.Drawing.Color.WhiteSmoke;
            this._AuthorLabel.Location = new System.Drawing.Point(3, 304);
            this._AuthorLabel.Name = "_AuthorLabel";
            this._AuthorLabel.Size = new System.Drawing.Size(506, 17);
            this._AuthorLabel.TabIndex = 9;
            this._AuthorLabel.Text = "A product of Integrated Scientific Resources";
            this._AuthorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _TopPanel
            // 
            this._TopPanel.BackColor = System.Drawing.Color.Transparent;
            this._TopPanel.Controls.Add(this._CloseLabel);
            this._TopPanel.Controls.Add(this._FolderLabel);
            this._TopPanel.Controls.Add(this._MinimizeLabel);
            this._TopPanel.Controls.Add(this._SmallApplicationCaptionLabel);
            this._TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._TopPanel.Location = new System.Drawing.Point(3, 6);
            this._TopPanel.Name = "_TopPanel";
            this._TopPanel.Size = new System.Drawing.Size(506, 26);
            this._TopPanel.TabIndex = 0;
            // 
            // _ProgressLayout
            // 
            this._ProgressLayout.ColumnCount = 3;
            this._ProgressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._ProgressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._ProgressLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._ProgressLayout.Controls.Add(this._MetroProgressBar, 1, 0);
            this._ProgressLayout.Dock = System.Windows.Forms.DockStyle.Top;
            this._ProgressLayout.Location = new System.Drawing.Point(3, 188);
            this._ProgressLayout.Name = "_ProgressLayout";
            this._ProgressLayout.RowCount = 1;
            this._ProgressLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._ProgressLayout.Size = new System.Drawing.Size(506, 14);
            this._ProgressLayout.TabIndex = 8;
            // 
            // BlueSplash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::isr.Core.WinForms.Metro.Properties.Resources.BlueSplash;
            this.ClientSize = new System.Drawing.Size(512, 324);
            this.Controls.Add(this._TableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(512, 324);
            this.MinimumSize = new System.Drawing.Size(512, 324);
            this.Name = "BlueSplash";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Splash";
            this._TableLayoutPanel.ResumeLayout(false);
            this._TableLayoutPanel.PerformLayout();
            this._TopPanel.ResumeLayout(false);
            this._TopPanel.PerformLayout();
            this._ProgressLayout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label _SmallApplicationCaptionLabel;
        private System.Windows.Forms.Label _LargeApplicationCaptionLabel;
        private System.Windows.Forms.Label _CurrentTaskLabel;
        private System.Windows.Forms.Label _CloseLabel;
        private System.Windows.Forms.Label _MinimizeLabel;
        private Metro.MetroProgressBar _MetroProgressBar;
        private System.Windows.Forms.Label _FolderLabel;
        private System.Windows.Forms.TableLayoutPanel _TableLayoutPanel;
        private System.Windows.Forms.Panel _TopPanel;
        private System.Windows.Forms.TableLayoutPanel _ProgressLayout;
        private System.Windows.Forms.Label _AuthorLabel;
    }
}
