namespace isr.Core.WinForms.Dialogs
{
    /// <summary> Dialog for selecting a file name to save. </summary>
    /// <remarks> David, 2019-01-22. </remarks>
    public class SaveFileDialog : FileDialogBase
    {

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public SaveFileDialog() : base()
        {
        }

        /// <summary> Opens the File Save dialog box and selects a file name to save. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> A Windows.Forms.DialogResult. </returns>
        public virtual bool TrySelectFile()
        {
            using var fileDialog = new System.Windows.Forms.SaveFileDialog {
                // Use the common dialog box
                CheckFileExists = false,
                CheckPathExists = true,
                Title = this.FileDialogTitle,
                Filter = this.FileDialogFilter,
                DefaultExt = this.FileExtension
            };
            var fi = new System.IO.FileInfo( this.FullFileName );
            if ( fi.Directory.Exists )
            {
                fileDialog.InitialDirectory = fi.DirectoryName;
            }

            fileDialog.RestoreDirectory = true;
            bool result = System.Windows.Forms.DialogResult.OK == fileDialog.ShowDialog();
            if ( result )
            {
                this.FullFileName = fileDialog.FileName;
            }

            return result;
        }

    }
}
