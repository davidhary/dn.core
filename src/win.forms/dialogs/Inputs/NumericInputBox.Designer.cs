using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.Core.WinForms.Dialogs
{

    public partial class NumericInputBox
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _CancelButton = new System.Windows.Forms.Button();
            _CancelButton.Click += new EventHandler(CancelButton_Click);
            _AcceptButton = new System.Windows.Forms.Button();
            _AcceptButton.Click += new EventHandler(AcceptButton_Click);
            _NumericUpDownLabel = new System.Windows.Forms.Label();
            NumericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)NumericUpDown).BeginInit();
            SuspendLayout();
            // 
            // _CancelButton
            // 
            _CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            _CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _CancelButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _CancelButton.Location = new System.Drawing.Point(11, 59);
            _CancelButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _CancelButton.Name = "_CancelButton";
            _CancelButton.Size = new System.Drawing.Size(87, 30);
            _CancelButton.TabIndex = 27;
            _CancelButton.Text = "&Cancel";
            // 
            // _AcceptButton
            // 
            _AcceptButton.Enabled = false;
            _AcceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _AcceptButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _AcceptButton.Location = new System.Drawing.Point(108, 59);
            _AcceptButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _AcceptButton.Name = "_AcceptButton";
            _AcceptButton.Size = new System.Drawing.Size(87, 30);
            _AcceptButton.TabIndex = 26;
            _AcceptButton.Text = "&OK";
            // 
            // _NumericUpDownLabel
            // 
            _NumericUpDownLabel.BackColor = System.Drawing.SystemColors.Control;
            _NumericUpDownLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _NumericUpDownLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _NumericUpDownLabel.ForeColor = System.Drawing.SystemColors.WindowText;
            _NumericUpDownLabel.Location = new System.Drawing.Point(9, 5);
            _NumericUpDownLabel.Name = "_NumericUpDownLabel";
            _NumericUpDownLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _NumericUpDownLabel.Size = new System.Drawing.Size(156, 21);
            _NumericUpDownLabel.TabIndex = 25;
            _NumericUpDownLabel.Text = "Enter a Value: ";
            _NumericUpDownLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // NumericUpDown
            // 
            NumericUpDown.Location = new System.Drawing.Point(11, 27);
            NumericUpDown.Name = "NumericUpDown";
            NumericUpDown.Size = new System.Drawing.Size(183, 25);
            NumericUpDown.TabIndex = 28;
            // 
            // NumericInputBox
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            ClientSize = new System.Drawing.Size(209, 98);
            ControlBox = false;
            Controls.Add(NumericUpDown);
            Controls.Add(_CancelButton);
            Controls.Add(_AcceptButton);
            Controls.Add(_NumericUpDownLabel);
            Name = "NumericInputBox";
            Text = "Numeric Input Box";
            ((System.ComponentModel.ISupportInitialize)NumericUpDown).EndInit();
            ResumeLayout(false);
        }

        private System.Windows.Forms.Button _CancelButton;
        private System.Windows.Forms.Button _AcceptButton;
        private System.Windows.Forms.Label _NumericUpDownLabel;

        /// <summary>   The numeric up down control. </summary>
        public System.Windows.Forms.NumericUpDown NumericUpDown;
    }
}
