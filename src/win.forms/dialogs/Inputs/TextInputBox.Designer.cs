using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace isr.Core.WinForms.Dialogs
{

    public partial class TextInputBox
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _EnteredValueTextBox = new System.Windows.Forms.TextBox();
            _EnteredValueTextBox.TextChanged += new EventHandler(EnteredValueTextBoxTextChanged);
            _EnteredValueTextBox.Validating += new System.ComponentModel.CancelEventHandler(EnteredValueTextBoxValidating);
            _EnteredValueTextBox.Validated += new EventHandler(EnteredValueTextBoxValidated);
            _CancelButton = new System.Windows.Forms.Button();
            _CancelButton.Click += new EventHandler(CancelButtonClick);
            _AcceptButton = new System.Windows.Forms.Button();
            _AcceptButton.Click += new EventHandler(AcceptButtonClick);
            _EnteredValueTextBoxLabel = new System.Windows.Forms.Label();
            _ValidationErrorProvider = new System.Windows.Forms.ErrorProvider(components);
            ((System.ComponentModel.ISupportInitialize)_ValidationErrorProvider).BeginInit();
            SuspendLayout();
            // 
            // _EnteredValueTextBox
            // 
            _EnteredValueTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;

            _EnteredValueTextBox.Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _EnteredValueTextBox.Location = new Point(9, 27);
            _EnteredValueTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _EnteredValueTextBox.Multiline = true;
            _EnteredValueTextBox.Name = "_EnteredValueTextBox";
            _EnteredValueTextBox.Size = new Size(186, 20);
            _EnteredValueTextBox.TabIndex = 28;
            // 
            // _CancelButton
            // 
            _CancelButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            _CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            _CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _CancelButton.Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _CancelButton.Location = new Point(9, 59);
            _CancelButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _CancelButton.Name = "_CancelButton";
            _CancelButton.Size = new Size(87, 30);
            _CancelButton.TabIndex = 27;
            _CancelButton.Text = "&Cancel";
            // 
            // _AcceptButton
            // 
            _AcceptButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            _AcceptButton.Enabled = false;
            _AcceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _AcceptButton.Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _AcceptButton.Location = new Point(108, 59);
            _AcceptButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _AcceptButton.Name = "_AcceptButton";
            _AcceptButton.Size = new Size(87, 30);
            _AcceptButton.TabIndex = 26;
            _AcceptButton.Text = "&OK";
            // 
            // _EnteredValueTextBoxLabel
            // 
            _EnteredValueTextBoxLabel.AutoSize = true;
            _EnteredValueTextBoxLabel.BackColor = SystemColors.Control;
            _EnteredValueTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _EnteredValueTextBoxLabel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            _EnteredValueTextBoxLabel.ForeColor = SystemColors.WindowText;
            _EnteredValueTextBoxLabel.Location = new Point(9, 7);
            _EnteredValueTextBoxLabel.Name = "_EnteredValueTextBoxLabel";
            _EnteredValueTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _EnteredValueTextBoxLabel.Size = new Size(91, 17);
            _EnteredValueTextBoxLabel.TabIndex = 25;
            _EnteredValueTextBoxLabel.Text = "Enter a Value: ";
            _EnteredValueTextBoxLabel.TextAlign = ContentAlignment.BottomLeft;
            // 
            // _ValidationErrorProvider
            // 
            _ValidationErrorProvider.ContainerControl = this;
            // 
            // TextInputBox
            // 
            AcceptButton = _AcceptButton;
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            CancelButton = _CancelButton;
            ClientSize = new Size(203, 95);
            ControlBox = false;
            Controls.Add(_EnteredValueTextBox);
            Controls.Add(_CancelButton);
            Controls.Add(_AcceptButton);
            Controls.Add(_EnteredValueTextBoxLabel);
            Name = "TextInputBox";
            Text = "InputBox";
            ((System.ComponentModel.ISupportInitialize)_ValidationErrorProvider).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.TextBox _EnteredValueTextBox;
        private System.Windows.Forms.Button _CancelButton;
        private System.Windows.Forms.Button _AcceptButton;
        private System.Windows.Forms.Label _EnteredValueTextBoxLabel;
        private System.Windows.Forms.ErrorProvider _ValidationErrorProvider;
    }
}
