using System;
using System.Windows.Forms;

namespace isr.Core.WinForms.Dialogs
{
    /// <summary> A data entry form for text with a numeric validator. </summary>
    /// <remarks> David, 2006-02-20. </remarks>
    public partial class TextInputBox : FormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of this class. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public TextInputBox() : base()
        {

            // This call is required by the Windows Form Designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call
            // this.ClassStyle = ClassStyleConstants.DropShadow;

            this.NumberStyle = System.Globalization.NumberStyles.None;
            this._EnteredValueTextBox.Name = "_EnteredValueTextBox";
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion 

        #region " PROPERTIES "

        private readonly char _MultilinePasswordCharacter = '\0';

        /// <summary> Gets or sets the use system password character. </summary>
        /// <value> The use system password character. </value>
        public bool UseSystemPasswordChar
        {
            get => this._EnteredValueTextBox.UseSystemPasswordChar;

            set {
                this._EnteredValueTextBox.UseSystemPasswordChar = value;
                this._EnteredValueTextBox.Multiline = !value;
                this._EnteredValueTextBox.PasswordChar = this._EnteredValueTextBox.Multiline ? this._MultilinePasswordCharacter : '*';
            }
        }

        /// <summary> Gets or sets the prompt. </summary>
        /// <value> The prompt. </value>
        public string Prompt
        {
            get => this._EnteredValueTextBoxLabel.Text;

            set => this._EnteredValueTextBoxLabel.Text = value;
        }

        /// <summary> Returns the entered value. </summary>
        /// <value> The entered value. </value>
        public string EnteredValue
        {
            get => this._EnteredValueTextBox.Text;

            set => this._EnteredValueTextBox.Text = value;
        }

        /// <summary> Gets or sets the default value. </summary>
        /// <value> The default value. </value>
        public string DefaultValue { get; set; } = string.Empty;

        /// <summary> Gets or sets the number style requested. </summary>
        /// <value> The total number of style. </value>
        public System.Globalization.NumberStyles NumberStyle { get; set; }

        #endregion

        #region " FORM AND CONTROL EVENT HANDLERS "

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnShown( EventArgs e )
        {
            try
            {
                _ = this._EnteredValueTextBox.Focus();
            }
            catch
            {
            }
            finally
            {
                base.OnShown( e );
            }
        }

        /// <summary>
        /// Closes and returns the <see cref="System.Windows.Forms.DialogResult.OK">OK</see>
        /// dialog result.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AcceptButtonClick( object sender, EventArgs e )
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Closes and returns the <see cref="System.Windows.Forms.DialogResult.Cancel">Cancel</see>
        /// dialog result.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void CancelButtonClick( object sender, EventArgs e )
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        /// <summary> Event handler. Called by _EnteredValueTextBox for text changed events. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void EnteredValueTextBoxTextChanged( object sender, EventArgs e )
        {
            if ( this.NumberStyle == System.Globalization.NumberStyles.None )
            {
                this._AcceptButton.Enabled = !string.IsNullOrEmpty( this._EnteredValueTextBox.Text );
            }
        }

        /// <summary> Validates the entered value. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        private void EnteredValueTextBoxValidating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._AcceptButton.Enabled = false;
            this._ValidationErrorProvider.SetError( sender as System.Windows.Forms.Control, string.Empty );
            if ( this.NumberStyle != System.Globalization.NumberStyles.None )
            {
                e.Cancel = !double.TryParse( this._EnteredValueTextBox.Text, this.NumberStyle, System.Globalization.CultureInfo.CurrentCulture, out _ );
            }
        }

        /// <summary> Enables the OK button. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void EnteredValueTextBoxValidated( object sender, EventArgs e )
        {
            this._AcceptButton.Enabled = true;
        }

        #endregion

        #region " STATIC "

        /// <summary> Enter dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="caption"> The caption. </param>
        /// <returns> A (DialogResult As DialogResult, DialogValue As String) </returns>
        public static (DialogResult DialogResult, string DialogValue) EnterDialog( string caption )
        {
            return EnterDialog( caption, "Enter a value:", string.Empty, false );
        }

        /// <summary> Enter dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="caption">               The caption. </param>
        /// <param name="useSystemPasswordChar"> True to use system password character. </param>
        /// <returns> A (DialogResult As DialogResult, DialogValue As String) </returns>
        public static (DialogResult DialogResult, string DialogValue) EnterDialog( string caption, bool useSystemPasswordChar )
        {
            return EnterDialog( caption, "Enter a value:", string.Empty, useSystemPasswordChar );
        }

        /// <summary> Enter dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="caption">               The caption. </param>
        /// <param name="prompt">                The prompt. </param>
        /// <param name="defaultValue">          The default value. </param>
        /// <param name="useSystemPasswordChar"> True to use system password character. </param>
        /// <returns> A (DialogResult As DialogResult, DialogValue As String) </returns>
        public static (DialogResult DialogResult, string DialogValue) EnterDialog( string caption, string prompt, string defaultValue, bool useSystemPasswordChar )
        {
            using var box = new TextInputBox();
            box.UseSystemPasswordChar = useSystemPasswordChar;
            box.DefaultValue = defaultValue;
            box.Prompt = prompt;
            box.Text = caption;
            // toggling visibility ensures that the form displays in unit testing debug mode
            box.Visible = true;
            System.Windows.Forms.Application.DoEvents();
            box.Visible = false;
            _ = box.ShowDialog();
            return (box.DialogResult, box.EnteredValue);
        }


        /// <summary>   Enter password. </summary>
        /// <remarks>   David, 2021-03-11. </remarks>
        /// <param name="caption">      The caption. </param>
        /// <param name="prompt">       The prompt. </param>
        /// <returns>   A Tuple. </returns>
        public static (DialogResult DialogResult, System.Security.SecureString Password) EnterPasswordDialog( string caption, string prompt )
        {
            using var box = new TextInputBox();
            box.UseSystemPasswordChar = true;
            box.DefaultValue = string.Empty;
            box.Prompt = prompt;
            box.Text = caption;
            // toggling visibility ensures that the form displays in unit testing debug mode
            box.Visible = true;
            System.Windows.Forms.Application.DoEvents();
            box.Visible = false;
            _ = box.ShowDialog();
            return (box.DialogResult, new System.Net.NetworkCredential( string.Empty, box.EnteredValue ).SecurePassword);
        }

        /// <summary>   Enter input. </summary>
        /// <remarks>   David, 2021-03-11. </remarks>
        /// <param name="caption">                  (Optional) The caption. </param>
        /// <param name="prompt">                   (Optional) The prompt. </param>
        /// <param name="defaultValue">             (Optional) The default value. </param>
        /// <param name="useSystemPasswordChar">    (Optional) True to use system password character. </param>
        /// <returns>   A String. </returns>
        public static String EnterInput( string caption = "Enter Value", string prompt = "Value:", string defaultValue = "", bool useSystemPasswordChar = false )
        {
            return TextInputBox.EnterDialog( caption, prompt, defaultValue, useSystemPasswordChar ).DialogValue;
        }

        /// <summary>   Enter password. </summary>
        /// <remarks>   David, 2021-03-11. </remarks>
        /// <param name="caption">  (Optional) The caption. </param>
        /// <param name="prompt">   (Optional) The prompt. </param>
        /// <returns>   A System.Security.SecureString. </returns>
        public static System.Security.SecureString EnterPassword( string caption = "Enter Password", string prompt = "Password:" )
        {
            return TextInputBox.EnterPasswordDialog( caption, prompt ).Password;
        }


        #endregion

    }

}
