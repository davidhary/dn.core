using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.Core.WinForms.Dialogs
{
    public partial class MessageForm
    {

        #region "Windows Form Designer generated code "

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageForm));
            _RichTextBox = new System.Windows.Forms.RichTextBox();
            _StatusStrip = new System.Windows.Forms.StatusStrip();
            _CustomButton = new System.Windows.Forms.ToolStripDropDownButton();
            _CustomButton.Click += new EventHandler(CustomButton_Click);
            _StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._StatusStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _RichTextBox
            // 
            _RichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            _RichTextBox.Location = new System.Drawing.Point(0, 0);
            _RichTextBox.Name = "_RichTextBox";
            _RichTextBox.ReadOnly = true;
            _RichTextBox.Size = new System.Drawing.Size(386, 133);
            _RichTextBox.TabIndex = 0;
            _RichTextBox.Text = string.Empty;
            _RichTextBox.WordWrap = false;
            // 
            // StatusStrip1
            // 
            _StatusStrip.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            _StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _StatusLabel, _ProgressBar, _CustomButton });
            _StatusStrip.Location = new System.Drawing.Point(0, 133);
            _StatusStrip.Name = "StatusStrip1";
            _StatusStrip.Size = new System.Drawing.Size(386, 23);
            _StatusStrip.TabIndex = 2;
            _StatusStrip.Text = "StatusStrip1";
            // 
            // _CustomButton
            // 
            _CustomButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _CustomButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            _CustomButton.Image = (System.Drawing.Image)resources.GetObject("_CustomButton.Image");
            _CustomButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _CustomButton.Name = "_CustomButton";
            _CustomButton.ShowDropDownArrow = false;
            _CustomButton.Size = new System.Drawing.Size(45, 21);
            _CustomButton.Text = "Abort";
            // 
            // _StatusLabel
            // 
            _StatusLabel.Name = "_StatusLabel";
            _StatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            _StatusLabel.Size = new System.Drawing.Size(193, 18);
            _StatusLabel.Spring = true;
            _StatusLabel.Text = "Status";
            // 
            // _ProgressBar
            // 
            _ProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            _ProgressBar.Name = "_ProgressBar";
            _ProgressBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ProgressBar.Size = new System.Drawing.Size(100, 17);
            // 
            // MessageForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            ClientSize = new System.Drawing.Size(386, 156);
            Controls.Add(_RichTextBox);
            Controls.Add(_StatusStrip);
            Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "MessageForm";
            Text = "Status";
            _StatusStrip.ResumeLayout(false);
            _StatusStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.ToolStripStatusLabel _StatusLabel;
        private System.Windows.Forms.ToolStripDropDownButton _CustomButton;
        private System.Windows.Forms.ToolStripProgressBar _ProgressBar;
        private System.Windows.Forms.RichTextBox _RichTextBox;
        private System.Windows.Forms.StatusStrip _StatusStrip;

        #endregion

    }
}
