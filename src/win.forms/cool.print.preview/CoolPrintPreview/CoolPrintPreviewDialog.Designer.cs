using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.WinForms
{

    public partial class CoolPrintPreviewDialog
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(CoolPrintPreviewDialog));
            _ToolStrip = new ToolStrip();
            _PrintButton = new System.Windows.Forms.ToolStripButton();
            _PrintButton.Click += new EventHandler(PrintButton_Click);
            _PageSetupButton = new System.Windows.Forms.ToolStripButton();
            _PageSetupButton.Click += new EventHandler(PageSetupButton_Click);
            _Separator2 = new ToolStripSeparator();
            _ZoomButton = new System.Windows.Forms.ToolStripSplitButton();
            _ZoomButton.ButtonClick += new EventHandler(ZoomButton_ButtonClick);
            _ZoomButton.DropDownItemClicked += new ToolStripItemClickedEventHandler(ZoomButton_DropDownItemClicked);
            _ActualSizeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _FullPageMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _PageWidthMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _TwoPagesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ZoomSeparatorMenuItem = new ToolStripSeparator();
            _Zoom500MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zomm200MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zoom150MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zoom100MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zoom75MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zoom50MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zoom25MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _Zoom10MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _FirstButton = new System.Windows.Forms.ToolStripButton();
            _FirstButton.Click += new EventHandler(FirstButton_Click);
            _PreviousButton = new System.Windows.Forms.ToolStripButton();
            _PreviousButton.Click += new EventHandler(PreviousButton_Click);
            _StartPageTextBox = new System.Windows.Forms.ToolStripTextBox();
            _StartPageTextBox.Enter += new EventHandler(StartPageTextBox_Enter);
            _StartPageTextBox.KeyPress += new KeyPressEventHandler(StartPageTextBox_KeyPress);
            _StartPageTextBox.Validating += new System.ComponentModel.CancelEventHandler(StartPageTextBox_Validating);
            _PageCountLabel = new System.Windows.Forms.ToolStripLabel();
            _NextButton = new System.Windows.Forms.ToolStripButton();
            _NextButton.Click += new EventHandler(NextButton_Click);
            _LastButton = new System.Windows.Forms.ToolStripButton();
            _LastButton.Click += new EventHandler(LastButton_Click);
            _Separator1 = new ToolStripSeparator();
            _CancelButton = new System.Windows.Forms.ToolStripButton();
            _CancelButton.Click += new EventHandler(CancelButton_Click);
            _Preview = new CoolPrintPreviewControl();
            _Preview.PageCountChanged += new EventHandler<EventArgs>(Preview_PageCountChanged);
            _Preview.StartPageChanged += new EventHandler<EventArgs>(Preview_StartPageChanged);
            _ToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _toolStrip
            // 
            _ToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            _ToolStrip.Items.AddRange(new ToolStripItem[] { _PrintButton, _PageSetupButton, _Separator2, _ZoomButton, _FirstButton, _PreviousButton, _StartPageTextBox, _PageCountLabel, _NextButton, _LastButton, _Separator1, _CancelButton });
            _ToolStrip.Location = new Point(0, 0);
            _ToolStrip.Name = "_toolStrip";
            _ToolStrip.Size = new Size(532, 25);
            _ToolStrip.TabIndex = 1;
            _ToolStrip.Text = "toolStrip1";
            // 
            // _PrintButton
            // 
            _PrintButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _PrintButton.Image = (Image)resources.GetObject("PrinterImage");
            _PrintButton.ImageTransparentColor = Color.Magenta;
            _PrintButton.Name = "_PrintButton";
            _PrintButton.Size = new Size(23, 22);
            _PrintButton.Text = "Print Document";
            // 
            // _PageSetupButton
            // 
            _PageSetupButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _PageSetupButton.Image = (Image)resources.GetObject("PageSetupImage");
            _PageSetupButton.ImageTransparentColor = Color.Magenta;
            _PageSetupButton.Name = "_PageSetupButton";
            _PageSetupButton.Size = new Size(23, 22);
            _PageSetupButton.Text = "Page Setup";
            // 
            // _Separator2
            // 
            _Separator2.Name = "_Separator2";
            _Separator2.Size = new Size(6, 25);
            // 
            // _ZoomButton
            // 
            _ZoomButton.AutoToolTip = false;
            _ZoomButton.DropDownItems.AddRange(new ToolStripItem[] { _ActualSizeMenuItem, _FullPageMenuItem, _PageWidthMenuItem, _TwoPagesMenuItem, _ZoomSeparatorMenuItem, _Zoom500MenuItem, _Zomm200MenuItem, _Zoom150MenuItem, _Zoom100MenuItem, _Zoom75MenuItem, _Zoom50MenuItem, _Zoom25MenuItem, _Zoom10MenuItem });
            _ZoomButton.Image = (Image)resources.GetObject("ZoomImage");
            _ZoomButton.ImageTransparentColor = Color.Magenta;
            _ZoomButton.Name = "_ZoomButton";
            _ZoomButton.Size = new Size(77, 22);
            _ZoomButton.Text = "&Zoom";
            // 
            // _ActualSizeMenuItem
            // 
            _ActualSizeMenuItem.Image = (Image)resources.GetObject("ActualSizeImage");
            _ActualSizeMenuItem.Name = "_ActualSizeMenuItem";
            _ActualSizeMenuItem.Size = new Size(150, 22);
            _ActualSizeMenuItem.Text = "Actual Size";
            // 
            // _FullPageMenuItem
            // 
            _FullPageMenuItem.Image = (Image)resources.GetObject("FullPageImage");
            _FullPageMenuItem.Name = "_FullPageMenuItem";
            _FullPageMenuItem.Size = new Size(150, 22);
            _FullPageMenuItem.Text = "Full Page";
            // 
            // _PageWidthMenuItem
            // 
            _PageWidthMenuItem.Image = (Image)resources.GetObject("PageWidthImage");
            _PageWidthMenuItem.Name = "_PageWidthMenuItem";
            _PageWidthMenuItem.Size = new Size(150, 22);
            _PageWidthMenuItem.Text = "Page Width";
            // 
            // _TwoPagesMenuItem
            // 
            _TwoPagesMenuItem.Image = (Image)resources.GetObject("TwoPagesImage");
            _TwoPagesMenuItem.Name = "_TwoPagesMenuItem";
            _TwoPagesMenuItem.Size = new Size(150, 22);
            _TwoPagesMenuItem.Text = "Two Pages";
            // 
            // _ZoomSeparatorMenuItem
            // 
            _ZoomSeparatorMenuItem.Name = "_ZoomSeparatorMenuItem";
            _ZoomSeparatorMenuItem.Size = new Size(147, 6);
            // 
            // _Zoom500MenuItem
            // 
            _Zoom500MenuItem.Name = "_Zoom500MenuItem";
            _Zoom500MenuItem.Size = new Size(150, 22);
            _Zoom500MenuItem.Text = "500%";
            // 
            // _Zomm200MenuItem
            // 
            _Zomm200MenuItem.Name = "_Zomm200MenuItem";
            _Zomm200MenuItem.Size = new Size(150, 22);
            _Zomm200MenuItem.Text = "200%";
            // 
            // _Zoom150MenuItem
            // 
            _Zoom150MenuItem.Name = "_Zoom150MenuItem";
            _Zoom150MenuItem.Size = new Size(150, 22);
            _Zoom150MenuItem.Text = "150%";
            // 
            // _Zoom100MenuItem
            // 
            _Zoom100MenuItem.Name = "_Zoom100MenuItem";
            _Zoom100MenuItem.Size = new Size(150, 22);
            _Zoom100MenuItem.Text = "100%";
            // 
            // _Zoom75MenuItem
            // 
            _Zoom75MenuItem.Name = "_Zoom75MenuItem";
            _Zoom75MenuItem.Size = new Size(150, 22);
            _Zoom75MenuItem.Text = "75%";
            // 
            // _Zoom50MenuItem
            // 
            _Zoom50MenuItem.Name = "_Zoom50MenuItem";
            _Zoom50MenuItem.Size = new Size(150, 22);
            _Zoom50MenuItem.Text = "50%";
            // 
            // _Zoom25MenuItem
            // 
            _Zoom25MenuItem.Name = "_Zoom25MenuItem";
            _Zoom25MenuItem.Size = new Size(150, 22);
            _Zoom25MenuItem.Text = "25%";
            // 
            // _Zoom10MenuItem
            // 
            _Zoom10MenuItem.Name = "_Zoom10MenuItem";
            _Zoom10MenuItem.Size = new Size(150, 22);
            _Zoom10MenuItem.Text = "10%";
            // 
            // _FirstButton
            // 
            _FirstButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _FirstButton.Image = (Image)resources.GetObject("FirstImage");
            _FirstButton.ImageTransparentColor = Color.Red;
            _FirstButton.Name = "_FirstButton";
            _FirstButton.Size = new Size(23, 22);
            _FirstButton.Text = "First Page";
            // 
            // _PreviousButton
            // 
            _PreviousButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _PreviousButton.Image = (Image)resources.GetObject("PreviousImage");
            _PreviousButton.ImageTransparentColor = Color.Red;
            _PreviousButton.Name = "_PreviousButton";
            _PreviousButton.Size = new Size(23, 22);
            _PreviousButton.Text = "Previous Page";
            // 
            // _StartPageTextBox
            // 
            _StartPageTextBox.AutoSize = false;
            _StartPageTextBox.Name = "_StartPageTextBox";
            _StartPageTextBox.Size = new Size(39, 24);
            _StartPageTextBox.TextBoxTextAlign = HorizontalAlignment.Center;
            // 
            // _PageCountLabel
            // 
            _PageCountLabel.Name = "_PageCountLabel";
            _PageCountLabel.Size = new Size(13, 22);
            _PageCountLabel.Text = " ";
            // 
            // _NextButton
            // 
            _NextButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _NextButton.Image = (Image)resources.GetObject("NextImage");
            _NextButton.ImageTransparentColor = Color.Red;
            _NextButton.Name = "_NextButton";
            _NextButton.Size = new Size(23, 22);
            _NextButton.Text = "Next Page";
            // 
            // _LastButton
            // 
            _LastButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _LastButton.Image = (Image)resources.GetObject("LastImage");
            _LastButton.ImageTransparentColor = Color.Red;
            _LastButton.Name = "_LastButton";
            _LastButton.Size = new Size(23, 22);
            _LastButton.Text = "Last Page";
            // 
            // _Separator1
            // 
            _Separator1.Name = "_Separator1";
            _Separator1.Size = new Size(6, 25);
            _Separator1.Visible = false;
            // 
            // _CancelButton
            // 
            _CancelButton.AutoToolTip = false;
            _CancelButton.Image = (Image)resources.GetObject("CancelImage");
            _CancelButton.ImageTransparentColor = Color.Magenta;
            _CancelButton.Name = "_CancelButton";
            _CancelButton.Size = new Size(70, 22);
            _CancelButton.Text = "Cancel";
            // 
            // _Preview
            // 
            _Preview.Dock = DockStyle.Fill;
            _Preview.Document = null;
            _Preview.Location = new Point(0, 25);
            _Preview.Name = "_Preview";
            _Preview.Size = new Size(532, 410);
            _Preview.TabIndex = 2;
            _Preview.ZoomMode = ZoomMode.FullPage;
            // 
            // CoolPrintPreviewDialog
            // 
            AutoScaleDimensions = new SizeF(120.0f, 120.0f);
            AutoScaleMode = AutoScaleMode.Dpi;
            ClientSize = new Size(532, 435);
            Controls.Add(_Preview);
            Controls.Add(_ToolStrip);
            Name = "CoolPrintPreviewDialog";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.CenterParent;
            Text = "Print Preview";
            _ToolStrip.ResumeLayout(false);
            _ToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip _ToolStrip;
        private System.Windows.Forms.ToolStripButton _PrintButton;
        private System.Windows.Forms.ToolStripButton _PageSetupButton;
        private ToolStripSeparator _Separator2;
        private System.Windows.Forms.ToolStripSplitButton _ZoomButton;
        private System.Windows.Forms.ToolStripMenuItem _ActualSizeMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _FullPageMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _PageWidthMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _TwoPagesMenuItem;
        private ToolStripSeparator _ZoomSeparatorMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom500MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zomm200MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom150MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom100MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom75MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom50MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom25MenuItem;
        private System.Windows.Forms.ToolStripMenuItem _Zoom10MenuItem;
        private System.Windows.Forms.ToolStripButton _FirstButton;
        private System.Windows.Forms.ToolStripButton _PreviousButton;
        private System.Windows.Forms.ToolStripTextBox _StartPageTextBox;
        private System.Windows.Forms.ToolStripLabel _PageCountLabel;
        private System.Windows.Forms.ToolStripButton _NextButton;
        private System.Windows.Forms.ToolStripButton _LastButton;
        private ToolStripSeparator _Separator1;
        private System.Windows.Forms.ToolStripButton _CancelButton;
        private CoolPrintPreviewControl _Preview;
    }
}
