using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace isr.Core.Tracing.WinForms
{
    /// <summary>   A base container for a trace event alert Annunciator. </summary>
    /// <remarks>   David, 2021-03-05. </remarks>
    public class TraceAlertContainer : isr.Core.Tracing.ITraceEventWriter, INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        /// <summary>   (Immutable) the alert annunciator. </summary>
        private object AlertAnnunciator{ get; set; }

        private readonly System.Windows.Forms.Control _TraceWriter;

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-02-08. </remarks>
        /// <param name="alertAnnunciator"> The alert annunciator; supported are <see cref="Control"/> or <see cref="ToolStripItem"/>. </param>
        /// <param name="traceWriter">      The trace writer. </param>
        public TraceAlertContainer( object alertAnnunciator, System.Windows.Forms.Control traceWriter )
        {
            this._AlertAnnunciatorEvent = TraceEventType.Verbose;
            this._AlertSoundEvent = TraceEventType.Verbose;
            this.Name = System.Guid.NewGuid().ToString();
            this._TraceWriter = traceWriter;
            this.AlertAnnunciator = alertAnnunciator;
            if ( this.AlertAnnunciator is Control control )
            {
                control.Click += new EventHandler( this.OnClear );
            }
            else if ( this.AlertAnnunciator is ToolStripItem toolStripItem )
            {
                toolStripItem.Click += new EventHandler( this.OnClear );
            }
            else
            {
                throw new InvalidOperationException( $"{ alertAnnunciator?.GetType() } is not supported" );
            }
            this._TraceWriter.VisibleChanged += new EventHandler( this.OnClear );
            if ( this._TraceWriter.GetContainerControl()?.ActiveControl is Control ctrl )
                ctrl.VisibleChanged += new EventHandler( this.OnClear );
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " I TRACE EVENT WRITER IMPLEMENTATION "

        /// <summary>   Gets or sets the name of the text writer. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary>   Gets or sets the trace level. </summary>
        /// <value> The trace level. </value>
        public TraceEventType TraceLevel { get; set; }

        /// <summary>
        /// Writes the specified trace event type to the contained annunciator.
        /// </summary>
        /// <remarks>   David, 2021-03-05. </remarks>
        /// <param name="eventType">    Type of the event. </param>
        /// <param name="message">      The message. </param>
        public void TraceEvent( TraceEventType eventType, string message )
        {
            // the alert annunciator is only interested in the event type.
            this.TraceEvent( eventType );
        }

        #endregion

        #region " ALERT MANAGEMENT "

        /// <summary> Suspend updates and release the Annunciator object. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public virtual void SuspendUpdatesReleaseAnnunciator()
        {
            Thread.Sleep( 10 );
            this.AlertAnnunciator = null;
        }

        /// <summary>   Writes the trace event type to the contained Annunciator. </summary>
        /// <remarks>   David, 2021-07-29. </remarks>
        /// <param name="eventType">    Type of the event. </param>
        public void TraceEvent( TraceEventType eventType )
        {
            try
            {
                this.AlertAnnunciatorEvent = eventType;
                this.AlertSoundEvent = eventType;
            }
            catch ( Exception ex )
            {
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary>   Updates the alert annunciator as described by the alert event type. </summary>
        /// <remarks>   David, 2021-03-05. </remarks>
        /// <param name="alertEvent">   The alert event. </param>
        private void UpdateAlertAnnunciator( TraceEventType alertEvent )
        {
            try
            {
                if ( this.AlertAnnunciator is Control control )
                {
                    this.UpdateAlertAnnunciator( control, alertEvent );
                }
                else if ( this.AlertAnnunciator is ToolStripItem toolStripItem )
                {
                    this.UpdateAlertAnnunciator( toolStripItem, alertEvent );
                }
            }
            catch ( Exception ex )
            {
                this.OnEventHandlerError( ex );
            }
        }

        /// <summary>   Updates the alert annunciator control as described by alertEvent. </summary>
        /// <remarks>   David, 2021-03-05. </remarks>
        /// <param name="control">      The alert annunciator control. </param>
        /// <param name="alertEvent">   The alert event. </param>
        private void UpdateAlertAnnunciator( Control control, TraceEventType alertEvent )
        {
            if ( control.InvokeRequired )
            {
                _ = control.BeginInvoke( new Action<Control, TraceEventType>( this.UpdateAlertAnnunciator ), new object[] { control, alertEvent } );
            }
            else if ( control.IsHandleCreated )
            {
                if ( alertEvent <= this.AlertLevel )
                {
                    control.Text = alertEvent.ToString();
                    switch ( alertEvent )
                    {
                        case var @case when @case <= TraceEventType.Error:
                            {
                                control.BackColor = System.Drawing.Color.Red;
                                break;
                            }

                        case var case1 when case1 <= TraceEventType.Warning:
                            {
                                control.BackColor = System.Drawing.Color.Orange;
                                break;
                            }

                        default:
                            {
                                control.BackColor = System.Drawing.Color.LightBlue;
                                break;
                            }
                    }

                    if ( !(control is TabPage) )
                    {
                        control.Visible = true;
                    }
                }
                else
                {
                    control.BackColor = this.AlertAnnunciatorBackColor;
                    control.Text = this.AlertAnnunciatorText;
                    if ( !(control is TabPage) )
                    {
                        control.Visible = false;
                    }
                }

                control.Invalidate();
            }
        }

        /// <summary>
        /// Updates the alert annunciator <see cref="ToolStripItem"/> as described by alertEvent.
        /// </summary>
        /// <remarks>   David, 2021-07-29. </remarks>
        /// <param name="toolStripItem">    The alert annunciator tool strip item. </param>
        /// <param name="alertEvent">       The alert event. </param>
        private void UpdateAlertAnnunciator( ToolStripItem toolStripItem, TraceEventType alertEvent )
        {
            if ( toolStripItem.GetCurrentParent().InvokeRequired )
            {
                _ = toolStripItem.GetCurrentParent().BeginInvoke( new Action<ToolStripItem, TraceEventType>( this.UpdateAlertAnnunciator ), new object[] { toolStripItem, alertEvent } );
            }
            else if ( toolStripItem.GetCurrentParent().Parent.IsHandleCreated )
            {
                if ( alertEvent <= this.AlertLevel )
                {
                    toolStripItem.Text = alertEvent.ToString();
                    switch ( alertEvent )
                    {
                        case var @case when @case <= TraceEventType.Error:
                            {
                                toolStripItem.BackColor = System.Drawing.Color.Red;
                                break;
                            }

                        case var case1 when case1 <= TraceEventType.Warning:
                            {
                                toolStripItem.BackColor = System.Drawing.Color.Orange;
                                break;
                            }

                        default:
                            {
                                toolStripItem.BackColor = System.Drawing.Color.LightBlue;
                                break;
                            }
                    }
                    toolStripItem.Visible = true;
                }
                else
                {
                    toolStripItem.BackColor = this.AlertAnnunciatorBackColor;
                    toolStripItem.Text = this.AlertAnnunciatorText;
                    toolStripItem.Visible = false;
                }

                toolStripItem.Invalidate();
            }
        }


        #endregion

        #region " ALERT PROPERTIES "

        /// <summary> The alert level. </summary>
        private TraceEventType _AlertLevel;

        /// <summary>
        /// Gets or sets the alert level. Message <see cref="TraceEventType">levels</see> equal or lower
        /// than this are tagged as alerts and set the <see cref="AlertAnnunciatorEvent">alert
        /// sentinel</see>.
        /// </summary>
        /// <value> The alert level. </value>
        [Category( "Appearance" )]
        [Description( "Level for notifying of alerts" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "TraceEventType.Warning" )]
        public TraceEventType AlertLevel
        {
            get => this._AlertLevel;

            set {
                if ( value != this.AlertLevel )
                {
                    this._AlertLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the sentinel indicating that a alert was announced. </summary>
        /// <value> true if an alert was announced; otherwise, false. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool AlertAnnounced => this.AlertAnnunciatorEvent <= this.AlertLevel;

        /// <summary> The alert annunciator event. </summary>
        private TraceEventType _AlertAnnunciatorEvent;

        /// <summary>
        /// Gets or sets or set the trace event type which the alert annunciator.
        /// </summary>
        /// <value> The alert annunciator event. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType AlertAnnunciatorEvent
        {
            get => this._AlertAnnunciatorEvent;

            set {
                if ( value != this.AlertAnnunciatorEvent )
                {
                    this._AlertAnnunciatorEvent = value;
                    this.NotifyPropertyChanged();
                    this.UpdateAlertAnnunciator( value );
                }
            }
        }

        /// <summary> Gets the sentinel indicating that an alert sound was emitted. </summary>
        /// <value> true if an alert sound was emitted; otherwise, false. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool AlertVoiced => this.AlertSoundEvent <= this.AlertLevel;

        /// <summary> The alert sound event. </summary>
        private TraceEventType _AlertSoundEvent;

        /// <summary> Gets or sets the highest alert event for alert sounds. </summary>
        /// <value> The highest alert event for alert sounds. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType AlertSoundEvent
        {
            get => this._AlertSoundEvent;

            set {
                if ( value != this.AlertSoundEvent )
                {
                    this._AlertSoundEvent = value;
                    this.NotifyPropertyChanged();
                    this.PlayAlertIf( this.AlertVoiced, value );
                }
            }
        }

        /// <summary> Gets or sets the alert sound enabled. </summary>
        /// <value> The alert sound enabled. </value>
        [Category( "Appearance" )]
        [Description( "Enables playing alert sounds" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool AlertSoundEnabled { get; set; }

        private System.Drawing.Color _AlertAnnunciatorBackColor = System.Drawing.SystemColors.Control;
        /// <summary> Gets or sets the background color of the alert annunciator. </summary>
        /// <value> The background color of the alert annunciator. </value>
        [Category( "Appearance" )]
        [Description( "Default annunciator background color" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( typeof( Color ), "0xF0F0F0" )]
        public System.Drawing.Color AlertAnnunciatorBackColor
        {
            get => this._AlertAnnunciatorBackColor;
            set {
                if ( !System.Drawing.Color.Equals( value, this.AlertAnnunciatorBackColor ) )
                {
                    this._AlertAnnunciatorBackColor = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private string _AlertAnnunciatorText;
        /// <summary> Gets or sets the alert annunciator text. </summary>
        /// <value> The alert annunciator text. </value>
        [Category( "Appearance" )]
        [Description( "Default annunciator text" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Trace" )]
        public string AlertAnnunciatorText
        {
            get => this._AlertAnnunciatorText;
            set {
                if ( !string.Equals( value, this.AlertAnnunciatorText ) )
                {
                    this._AlertAnnunciatorText = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " SHOW or HIDE ALERT "

        /// <summary> Play alert if. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="condition"> true to condition. </param>
        /// <param name="level">     The level. </param>
        public void PlayAlertIf( bool condition, TraceEventType level )
        {
            if ( condition && this.AlertSoundEnabled )
            {
                PlayAlert( level );
            }
        }

        /// <summary> Play alert. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="level"> The level. </param>
        public static void PlayAlert( TraceEventType level )
        {
            if ( level == TraceEventType.Critical )
            {
                System.Media.SystemSounds.Exclamation.Play();
            }
            else if ( level == TraceEventType.Error )
            {
                System.Media.SystemSounds.Exclamation.Play();
            }
            else if ( level == TraceEventType.Warning )
            {
                System.Media.SystemSounds.Asterisk.Play();
            }
        }

        /// <summary> Updates the alerts described by level. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="level"> The level. </param>
        protected void UpdateAlerts( TraceEventType level )
        {
            if ( level <= this.AlertLevel )
            {
                // Updates the cached alert level if the level represents a higher alert level.
                if ( level < this.AlertAnnunciatorEvent )
                {
                    this.AlertAnnunciatorEvent = level;
                }

                if ( level < this.AlertSoundEvent )
                {
                    this.AlertSoundEvent = level;
                }
            }
            // Me._AlertsAdded = Not Me.UserVisible AndAlso (Me.AlertsAdded OrElse (level <= Me.AlertLevel))
            // 2016 moved to highest alert level: If Not Me.UserVisible AndAlso Me.AlertsAdded Then Me.PlayAlert(level)
        }

        /// <summary> Clears the alert annunciator if the trace writer is visible. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public void Display()
        {
            this.OnClear( this.AlertAnnunciator, EventArgs.Empty );
        }

        /// <summary> Executes the clear action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void OnClear( object sender, EventArgs e )
        {
            if ( this.UserVisible )
            {
                this.AlertAnnunciatorEvent = TraceEventType.Verbose;
                this.AlertSoundEvent = TraceEventType.Verbose;
            }
        }

        #endregion

        #region " TRACE WRITER VISIBILITY MANAEMENT "

        /// <summary>   Queries if the trace writer is visible to the user. </summary>
        /// <value> True if the trace writer is visible to the user; otherwise false. </value>
        private bool UserVisible => this._TraceWriter.Visible && this._TraceWriter.Height > 10 &&
                       this._TraceWriter.GetContainerControl() is object && this._TraceWriter.GetContainerControl().ActiveControl is object &&
                       this._TraceWriter.GetContainerControl().ActiveControl.Visible;

        #endregion

        #region " EXCEPTION EVENT HANDLER "

        /// <summary>   Event queue for all listeners interested in <see cref="System.Exception"/> events. </summary>
        public event EventHandler<System.Threading.ThreadExceptionEventArgs> ExceptionEventHandler;

        /// <summary>   Raises the  <see cref="ExceptionEventHandler"/> event. </summary>
        /// <remarks>   David, 2021-05-03. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected virtual void OnEventHandlerError( System.Threading.ThreadExceptionEventArgs e )
        {
            this.ExceptionEventHandler?.Invoke( this, e );
        }

        /// <summary>   Raises the  <see cref="ExceptionEventHandler"/> event. </summary>
        /// <remarks>   David, 2021-07-29. </remarks>
        /// <param name="exception">    The exception. </param>
        protected virtual void OnEventHandlerError( System.Exception exception )
        {
            this.OnEventHandlerError( new System.Threading.ThreadExceptionEventArgs( exception ) );
        }

        #endregion

    }
}
