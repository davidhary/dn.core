using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace isr.Core.Tracing.WinForms
{
    /// <summary>   A text box <see cref="isr.Core.Tracing.ITraceEventWriter"/>. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    public class TextBoxTraceEventWriter : isr.Core.Tracing.ITraceEventWriter, INotifyPropertyChanged
    {

        #region " CONSTRUCTION "

        private readonly System.Windows.Forms.TextBox _Target;

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-02-08. </remarks>
        /// <param name="target">   Target for the. </param>
        public TextBoxTraceEventWriter( TextBox target )
        {
            this.Name = System.Guid.NewGuid().ToString();
            this._Target = target;
            this._Lines = new List<string>();
            if ( target.Multiline )
                this._Lines.AddRange( target.Lines );
            this.ResetCount = 200;
            this.PresetCount = 100;
            this.Appending = false;
            this.TabCaption = "Log";
            this.CaptionFormat = "{0} " + System.Text.Encoding.GetEncoding( 1200 ).GetString( new byte[] { 240 } );
            target.VisibleChanged += this.HandleTargetVisibleChanged;
            if ( target.GetContainerControl()?.ActiveControl is Control ctrl )
                ctrl.VisibleChanged += new EventHandler( this.HandleTargetVisibleChanged );
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGE IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " I TRACE EVENT WRITER IMPLEMENTATION "

        /// <summary>   Gets or sets the name of the text writer. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary>
        /// When overridden in a derived class, writes the specified message to the listener you create
        /// in the derived class.
        /// </summary>
        /// <remarks>   David, 2021-02-08. </remarks>
        /// <param name="message">  A message to write. </param>
        public void Write( string message )
        {
            if ( this._Target.InvokeRequired )
                _ = this._Target.BeginInvoke( new Action<string>( this.Write ), new object[] { message } );
            else if ( !this.HandleCreatedCheckEnabled || this._Target.IsHandleCreated )
                this.AddMessageInternal( message );
        }

        /// <summary>
        /// When overridden in a derived class, writes a message to the listener you create in the
        /// derived class, followed by a line terminator.
        /// </summary>
        /// <remarks>   David, 2021-02-08. </remarks>
        /// <param name="message">  A message to write. </param>
        public void WriteLine( string message )
        {
            this.Write( $"{message}{Environment.NewLine}" );
        }

        /// <summary>   Gets or sets the maximum level for tracing an event. </summary>
        /// <remarks> Only events with the same or lower <see cref="TraceEventType"/> are  
        /// allowed (filtered in). </remarks>
        /// <value> The trace level. </value>
        public TraceEventType TraceLevel { get; set; }

        /// <summary>   Trace event. </summary>
        /// <remarks>   David, 2021-03-06. </remarks>
        /// <param name="eventType">    Type of the event. </param>
        /// <param name="message">      The message. </param>
        public void TraceEvent( TraceEventType eventType, string message )
        {
            if ( eventType <= this.TraceLevel )
                if ( this._Target.Multiline )
                    this.Write( message );
                else
                    this.WriteLine( message );
        }

        /// <summary>   Adds a message. </summary>
        /// <remarks>   David, 2021-02-08. </remarks>
        /// <param name="message">  The message. </param>
        private delegate void AddMessage( string message );

        /// <summary>   Adds a message internal. </summary>
        /// <remarks>   David, 2021-02-08. </remarks>
        /// <param name="message">  A message to write. </param>
        private void AddMessageInternal( string message )
        {

            // No need to lock text box as this function will only 
            // ever be executed from the UI thread
            if ( string.IsNullOrWhiteSpace( message ) )
                return;

            if ( this._Target.Multiline )
            {
                if ( this._Target.Lines.Length == 0 )
                    this._Lines.Clear();
                var values = message.Split( new[] { "\r\n", "\r", "\n" },  StringSplitOptions.None );
                if ( values.Length == 1 )
                {
                    if ( this.Appending )
                    {
                        this._Lines.Add( message );
                    }
                    else
                    {
                        this._Lines.Insert( 0, message );
                    }
                }
                else if ( this.Appending )
                {
                    this._Lines.AddRange( values );
                }
                else
                {
                    this._Lines.InsertRange( 0, values );
                }

                if ( this._Lines.Count > this.ResetCount )
                {
                    if ( this.Appending )
                    {
                        this._Lines.RemoveRange( 0, this._Lines.Count - this.PresetCount );
                    }
                    else
                    {
                        this._Lines.RemoveRange( this.PresetCount, this._Lines.Count - this.PresetCount );
                    }
                }

                this._Target.Lines = this._Lines.ToArray();
            }
            else
            {
                if ( this.Appending )
                {
                    this._Target.AppendText( message );
                }
                else
                {
                    _ = this._Target.Text = $"{message}{this._Target.Text}";
                }
            }
            if ( !this.Appending )
            {
                this._Target.SelectionStart = 0;
                this._Target.SelectionLength = 0;
            }

            this.TextLength = this._Target.TextLength;
            this.ObservedTextLength = this.UserVisible ? this.TextLength : this.ObservedTextLength;
            this.UpdateCaption();
        }

        #endregion

        #region " TEXT UPDATE INFORMATION "

        private void HandleClearEvent( object sender, EventArgs e )
        {
            this._Lines.Clear();
        }

        /// <summary>   The lines. </summary>
        private readonly List<string> _Lines;

        /// <summary>   Gets the number of lines. </summary>
        /// <value> The number of lines. </value>
        public int LineCount => this._Lines.Count;

        private bool _HandleCreatedCheckEnabled;

        /// <summary>   Gets or sets a value indicating whether the <see cref="TextBoxTraceEventWriter"/> 
        /// checks if the handlers of the writer control, <see cref="ContainerTreeNode"/> or <see cref="ContainerControl"/>
        /// is created before updating their values. When Enabled, these controls update only after the contrainer form is 
        /// fully loaded. As a result, the controls do not reflect any events that were logged when the form is created. </summary>
        /// <value> True if appending, false if not. </value>
        [Category( "Appearance" )]
        [Description( "True to check for control handles when updating the control values" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool HandleCreatedCheckEnabled
        {
            get => this._HandleCreatedCheckEnabled;
            set {
                if ( !bool.Equals( value, this.HandleCreatedCheckEnabled ) )
                {
                    this._HandleCreatedCheckEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _Appending;
        /// <summary>   Gets or sets a value indicating whether texts are appending or pre-pending. </summary>
        /// <value> True if appending, false if not. </value>
        [Category( "Appearance" )]
        [Description( "True to append; otherwise prepend" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool Appending
        {
            get => this._Appending;
            set {
                if ( !bool.Equals( value, this.Appending ) )
                {
                    this._Appending = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _ResetCount;
        /// <summary> Gets or sets the reset count. </summary>
        /// <remarks>
        /// The text box number of lines get reset to the preset count when the line count exceeds the reset
        /// count.
        /// </remarks>
        /// <value> <c>ResetSize</c>is an integer property. </value>
        [Category( "Appearance" )]
        [Description( "Number of lines at which to reset" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "100" )]
        public int ResetCount
        {
            get => this._ResetCount;
            set {
                if ( !int.Equals( value, this.ResetCount ) )
                {
                    this._ResetCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _PresetCount;

        /// <summary> Gets or sets the preset count. </summary>
        /// <remarks>
        /// The text box number of lines get reset to the preset count when the line count exceeds the reset
        /// count.
        /// </remarks>
        /// <value> <c>PresetSize</c>is an integer property. </value>
        [Category( "Appearance" )]
        [Description( "Number of lines to reset to" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "50" )]
        public int PresetCount
        {
            get => this._PresetCount;

            set {
                if ( this.PresetCount != value )
                {
                    this._PresetCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " VISIBILITY MANAEMENT

        /// <summary> Query if the target is visible to the user. </summary>
        /// <value> The showing. </value>
        protected bool UserVisible => this._Target.Visible && this._Target.Height > 100 &&
                       this._Target.GetContainerControl() is object && this._Target.GetContainerControl().ActiveControl is object &&
                       this._Target.GetContainerControl().ActiveControl.Visible;

        #endregion

        #region " CAPTION "

        /// <summary> Suspend updates and release indicator controls. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public virtual void SuspendUpdatesReleaseIndicators()
        {
            Thread.Sleep( 10 );
            this.ContainerPanel = null;
            this.ContainerTreeNode = null;
        }

        /// <summary>   Gets or sets the observed text length. </summary>
        /// <value> The observed text length. </value>
        public int ObservedTextLength { get; private set; }

        /// <summary>   Gets or sets the text length. </summary>
        /// <value> The length of the text. </value>
        public int TextLength { get; private set; }

        /// <summary> Queries if has unread messages. </summary>
        /// <value> The new messages available. </value>
        public bool HasUnreadMessages => this.TextLength > this.ObservedTextLength;

        /// <summary> The tab caption. </summary>
        private string _TabCaption;

        /// <summary> Gets or sets the tab caption. </summary>
        /// <value> The tab caption. </value>
        [Category( "Appearance" )]
        [Description( "Default title for the parent tab" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Log" )]
        public string TabCaption
        {
            get => this._TabCaption;

            set {
                if ( (this.TabCaption ?? "") != (value ?? "") )
                {
                    this._TabCaption = value;
                    this.NotifyPropertyChanged();
                    this.UpdateCaption();
                }
            }
        }

        private string _CaptionFormat;

        /// <summary> Gets or sets the caption format indicating that messages were added. </summary>
        /// <value> The tab caption format. </value>
        [Category( "Appearance" )]
        [Description( "Formats the tab caption with number of new messages" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "{0} =" )]
        public string CaptionFormat
        {
            get => this._CaptionFormat;

            set {
                if ( string.IsNullOrEmpty( value ) )
                {
                    value = string.Empty;
                }

                if ( !value.Equals( this.CaptionFormat ) )
                {
                    this._CaptionFormat = value;
                    this.NotifyPropertyChanged();
                    this.UpdateCaption();
                }
            }
        }

        /// <summary> The caption. </summary>
        private string _Caption;

        /// <summary> Gets or sets the caption. </summary>
        /// <value> The caption. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string Caption
        {
            get => this._Caption;

            set {
                if ( !(string.IsNullOrEmpty( value ) || string.Equals( value, this.Caption )) )
                {
                    this._Caption = value;
                    this.NotifyPropertyChanged();
                    if ( this.ContainerPanel is object )
                    {
                        this.SafeTextSetter( this.ContainerPanel, this.Caption );
                    }

                    if ( this.ContainerTreeNode is object )
                    {
                        this.SafeTextSetter( this.ContainerTreeNode, this.Caption );
                    }
                }
            }
        }

        /// <summary> Updates the caption. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        private void UpdateCaption()
        {
            this.Caption = true == this.HasUnreadMessages ? string.Format( System.Globalization.CultureInfo.CurrentCulture, this.CaptionFormat, this.TabCaption ) : this.TabCaption;
        }

        /// <summary> Gets or sets the container panel. </summary>
        /// <value> The parent panel. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Panel ContainerPanel { get; set; }

        /// <summary> Gets or sets the container tree node. </summary>
        /// <value> The container tree node. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TreeNode ContainerTreeNode { get; set; }

        /// <summary> Updates the container panel caption. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="panel"> The container panel. </param>
        /// <param name="value"> The value. </param>
        private void SafeTextSetter( Panel panel, string value )
        {
            try
            {
                if ( panel.InvokeRequired )
                {
                    _ = panel.BeginInvoke( new Action<Panel, string>( this.SafeTextSetter ), new object[] { panel, value } );
                }
                else if ( !this.HandleCreatedCheckEnabled || this._Target.IsHandleCreated )
                {
                    panel.Text = value;
                }
            }
            catch
            {
            }
        }

        /// <summary> Updates the container tree node caption. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="node">  The container tree node. </param>
        /// <param name="value"> The value. </param>
        private void SafeTextSetter( TreeNode node, string value )
        {
            try
            {
                if ( node.TreeView?.InvokeRequired == true )
                {
                    _ = (node.TreeView?.BeginInvoke( new Action<TreeNode, string>( this.SafeTextSetter ), new object[] { node, value } ));
                }
                else if ( !this.HandleCreatedCheckEnabled || this._Target.IsHandleCreated )
                {
                    node.Text = value;
                }
            }
            catch
            {
            }
        }

        #endregion

        #region " TARGET EVENTS "

        /// <summary>
        /// Raises the <see cref="E: Control.VisibleChanged" /> event. Updates the display.
        /// </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        private void HandleTargetVisibleChanged( object sender, EventArgs e )
        {
            if ( this.UserVisible )
            {
                this.ObservedTextLength = this.TextLength;
            }
            this.UpdateCaption();
        }

        #endregion

    }
}
