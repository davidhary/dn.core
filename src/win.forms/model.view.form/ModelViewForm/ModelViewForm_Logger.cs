using System.Collections.Generic;
using System.ComponentModel;

using Microsoft.Extensions.Logging;
using isr.Core.TraceLog;
using System.Diagnostics;

namespace isr.Core.WinForms.ModelViewForm
{
    public partial class ModelViewForm
    {

        #region " TEXT  WRITER "

        /// <summary>   Adds a display trace event writer. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="textWriter">   The trace Event tWriter. </param>
        public void AddDisplayTextWriter( Core.Tracing.ITraceEventWriter textWriter )
        {
            textWriter.TraceLevel = this.DisplayTraceEventType ;
            Core.Tracing.TracingPlatform.Instance.AddTraceEventWriter( textWriter );
        }

        /// <summary>   Removes the display text writer described by <paramref name="textWriter"/>. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="textWriter">   The trace Event tWriter. </param>
        public static void RemoveDisplayTextWriter( Core.Tracing.ITraceEventWriter textWriter )
        {
            Core.Tracing.TracingPlatform.Instance.RemoveTraceEventWriter( textWriter );
        }

        #endregion

        #region " DISPLAY LOG LEVEL "
#if false
        // log level does not map to trace event types, which filter the display.

        private KeyValuePair<LogLevel, string> _DisplayLogLevelValueNamePair;
        /// <summary>
        /// Gets or sets the <see cref="Microsoft.Extensions.Logging.LogLevel"/> value name pair for display.
        /// </summary>
        /// <value> The log level value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<LogLevel, string> DisplayLogLevelValueNamePair
        {
            get => this._DisplayLogLevelValueNamePair;

            set {
                if ( !KeyValuePair<LogLevel, string>.Equals( value, this.DisplayLogLevelValueNamePair ) )
                {
                    this._DisplayLogLevelValueNamePair = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private LogLevel _DisplayLogLevel;
        /// <summary>
        /// Gets or sets the <see cref="Microsoft.Extensions.Logging.LogLevel"/> for display.
        /// </summary>
        /// <value> The trace event writer log level. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LogLevel DisplayLogLevel
        {
            get => this._DisplayLogLevel;

            set {
                if ( value != this.DisplayLogLevel )
                {
                    this._DisplayLogLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
#endif
        #endregion

        #region " DISPLAY TRACE EVENT TYPE "

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> value name pair for the global trace event
        /// writer. This level determines the level of all the
        /// <see cref="Core.Tracing.ITraceEventWriter"/>s Trace Listeners. Each trace listener can still listen at a
        /// lower level.
        /// </summary>
        /// <value> The trace event writer trace event value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<TraceEventType, string> TraceEventWriterTraceEventValueNamePair
        {
            get => TraceLogger.Instance.TraceEventWriterTraceEventValueNamePair;

            set {
                if ( !KeyValuePair<TraceEventType, string>.Equals( value, this.TraceEventWriterTraceEventValueNamePair ) )
                {
                    TraceLogger.Instance.TraceEventWriterTraceEventValueNamePair = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> for the global trace event writer. This level
        /// determines the level of all the
        /// <see cref="Core.Tracing.ITextWriter"/>s Trace Listeners. Each trace listener can still listen at a
        /// lower level.
        /// </summary>
        /// <value> The type of the trace event writer trace event. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType TraceEventWriterTraceEventType
        {
            get => TraceLogger.Instance.TraceEventWriterTraceEventType;

            set {
                if ( value != this.TraceEventWriterTraceEventType )
                {
                    TraceLogger.Instance.TraceEventWriterTraceEventType = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> value name pair for display.
        /// </summary>
        /// <value> The log level value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<TraceEventType, string> DisplayTraceEventTypeValueNamePair { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> for display.
        /// </summary>
        /// <value> The trace event writer log level. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType DisplayTraceEventType { get; set; }

        #endregion

        #region " LOGGING LOG LEVEL "

        /// <summary>
        /// Gets or sets the <see cref="Microsoft.Extensions.Logging.LogLevel"/> value name pair for logging. This level
        /// determines the level of the <see cref="ILogger"/>.
        /// </summary>
        /// <value> The log trace event value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<LogLevel, string> LoggingLevelValueNamePair
        {
            get => TraceLogger.Instance.LoggingLevelValueNamePair;

            set {
                if ( !KeyValuePair<LogLevel, string>.Equals( value, this.LoggingLevelValueNamePair ) )
                {
                    TraceLogger.Instance.LoggingLevelValueNamePair = value;
                }
            }
        }

        /// <summary>   Gets or sets <see cref="Microsoft.Extensions.Logging.LogLevel"/> value for logging. </summary>
        /// <value> The trace event type for logging. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LogLevel LoggingLogLevel
        {
            get => TraceLogger.Instance.MinimumLogLevel;

            set {
                if ( value != this.LoggingLogLevel )
                {
                    TraceLogger.Instance.MinimumLogLevel = value;
                }
            }
        }

        #endregion

    }
}
