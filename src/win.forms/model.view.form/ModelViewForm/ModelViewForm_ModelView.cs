using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;

using isr.Core.WinControls;

namespace isr.Core.WinForms.ModelViewForm
{

    /// <summary>
    /// A Mode View Form to host <see cref="ModelViewBase"/> or <see cref="ModelViewLoggerBase"/>
    /// controls.
    /// </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2005-02-07, 2.0.2597 </para>
    /// </remarks>
    public partial class ModelViewForm
    {

        #region " FORM EVENT HANDLERS "

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnClosing( CancelEventArgs e )
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if ( e is object && !e.Cancel )
                {
                    // removes the private text box listener
                    ModelViewForm.RemoveDisplayTextWriter( this.TextBoxTextWriter );
                    if ( this.PropertyNotifyControl is object )
                    {
                        this._Layout.Controls.Remove( this.PropertyNotifyControl );
                    }

                    if ( this._PropertyNotifyControlDisposeEnabled )
                    {
                        this.PropertyNotifyControl.Dispose();
                    }

                    if ( this._UserControlDisposeEnabled && this.UserControl is object )
                    {
                        this.UserControl.Dispose();
                    }

                    this.UserControl = null;
                    if ( this.ModelView is object )
                    {
                        this._Layout.Controls.Remove( this.ModelView );
                        if ( this._ModelViewControlDisposeEnabled )
                        {
                            this.ModelView.Dispose();
                        }
                    }

                    this.ModelView = null;
                }
            }
            finally
            {
                Application.DoEvents();
                base.OnClosing( e );
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Name} loading";
                this.Cursor = Cursors.WaitCursor;
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );

                // set the form caption
                activity = $"{this.Name} displaying title (text)";

                Assembly assembly = Assembly.GetCallingAssembly();
                this.Text = $"{assembly.GetName().Name} release {assembly.GetName().Version}";

                // default to center screen.
                activity = $"{this.Name} loading; centering to screen";
                this.CenterToScreen();
            }
            catch ( Exception ex )
            {
                _ = this.LogError( ex, activity );
                if ( DialogResult.Abort == System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception Occurred", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error ) )
                {
                    Application.Exit();
                }
            }
            finally
            {
                base.OnLoad( e );
                Trace.CorrelationManager.StopLogicalOperation();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnShown( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"showing {this.Name}";
                this.Cursor = Cursors.WaitCursor;
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );

                // allow form rendering time to complete: process all messages currently in the queue.
                Application.DoEvents();
                if ( !this.DesignMode )
                {
                }

                activity = "Ready to open Visa Session";
                _ = TraceLog.TraceLogger.LogVerbose( $"{activity};. " );
            }
            catch ( Exception ex )
            {
                _ = this.LogError( ex, activity );
                if ( DialogResult.Abort == System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception Occurred", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error ) )
                {
                    Application.Exit();
                }
            }
            finally
            {
                base.OnShown( e );
                Trace.CorrelationManager.StopLogicalOperation();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Resize client area. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="title">              The title. </param>
        /// <param name="clientControl">      The client control. </param>
        /// <param name="usingTraceMessages"> True to using trace messages. </param>
        public void ResizeClientArea( string title, Control clientControl, bool usingTraceMessages )
        {
            if ( clientControl is null )
            {
                throw new ArgumentNullException( nameof( clientControl ) );
            }

            if ( usingTraceMessages )
            {
                this.ResizeClientArea( this._Tabs, this._TraceMessagesBox, clientControl );
                this._ViewTabPage.Text = title;
                this._ViewTabPage.ToolTipText = title;
                this._Layout.Controls.Add( clientControl, 1, 1 );
            }
            else
            {
                this.ResizeClientArea( clientControl );
            }
        }

        /// <summary> Resize client area. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="clientControl"> The client control. </param>
        private void ResizeClientArea( Control clientControl )
        {
            if ( clientControl is null )
            {
                throw new ArgumentNullException( nameof( clientControl ) );
            }

            this._Tabs.Visible = false;
            this.Controls.Add( clientControl );
            var clientMargins = new System.Drawing.Size( 0, 0 );
            var controlMargins = new System.Drawing.Size( 0, 0 );
            System.Drawing.Size newClientSize;
            clientControl.BringToFront();
            newClientSize = new System.Drawing.Size( clientControl.Width + clientMargins.Width, this.Height );
            newClientSize = new System.Drawing.Size( newClientSize.Width, clientControl.Height + clientMargins.Height + this.StatusStrip.Height );
            bool resizeRequired = true;
            if ( resizeRequired )
            {
                this.ClientSize = new System.Drawing.Size( newClientSize.Width + controlMargins.Width, newClientSize.Height + controlMargins.Height );
                this.Refresh();
            }
        }

        /// <summary> Resize client area. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="tabControl">    The tab control. </param>
        /// <param name="messagesBox">   The messages box control. </param>
        /// <param name="clientControl"> The client control. </param>
        private void ResizeClientArea( TabControl tabControl, Control messagesBox, Control clientControl )
        {
            if ( messagesBox is null )
            {
                throw new ArgumentNullException( nameof( messagesBox ) );
            }

            if ( tabControl is null )
            {
                throw new ArgumentNullException( nameof( tabControl ) );
            }

            tabControl.BringToFront();
            if ( clientControl is null )
            {
                throw new ArgumentNullException( nameof( clientControl ) );
            }

            var tabsMargins = new System.Drawing.Size( tabControl.Width - messagesBox.Width, tabControl.Height - messagesBox.Height );
            var controlMargins = new System.Drawing.Size( this.ClientSize.Width - this._Layout.Width, this.ClientSize.Height - this._Layout.Height );
            System.Drawing.Size newClientSize;
            newClientSize = new System.Drawing.Size( clientControl.Width + tabsMargins.Width, tabControl.Height );
            newClientSize = new System.Drawing.Size( newClientSize.Width, clientControl.Height + tabsMargins.Height );
            bool resizeRequired = true;
            if ( resizeRequired )
            {
                tabControl.Size = newClientSize;
                tabControl.Refresh();
                this.ClientSize = new System.Drawing.Size( newClientSize.Width + controlMargins.Width, newClientSize.Height + controlMargins.Height );
                this.Refresh();
            }
        }

        #endregion

        #region " USER CONTROL "

        /// <summary> true to enable, false to disable the disposal of the User Control. </summary>
        private bool _UserControlDisposeEnabled;

        /// <summary> The user control. </summary>
        private UserControl UserControl { get; set; }

        /// <summary> Adds a User Control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void AddUserControl( UserControl value )
        {
            this.AddUserControl( "UI", value, true );
        }

        /// <summary> Adds a User Control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title"> The title. </param>
        /// <param name="value"> The value. </param>
        public void AddUserControl( string title, UserControl value )
        {
            this.AddUserControl( title, value, true );
        }

        /// <summary> Adds a User Control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title">             The title. </param>
        /// <param name="value">             The value. </param>
        /// <param name="disposeEnabled">    true to enable, false to disable the dispose. </param>
        /// <param name="showTraceMessages"> True to show, false to hide the trace messages. </param>
        public void AddUserControl( string title, UserControl value, bool disposeEnabled, bool showTraceMessages )
        {
            this.ResizeClientArea( title, value, showTraceMessages );
            this.UserControl = value;
            if ( showTraceMessages )
            {
                this.InitializeTraceListener();
            }

            this._UserControlDisposeEnabled = disposeEnabled;
            this.UserControl.Dock = DockStyle.Fill;
            this.UserControl.TabIndex = 0;
            this.UserControl.BackColor = System.Drawing.Color.Transparent;
            this.UserControl.Font = new System.Drawing.Font( this.Font, System.Drawing.FontStyle.Regular );
            this.UserControl.Name = "_UserControl";
        }

        /// <summary> Adds a User Control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title">          The title. </param>
        /// <param name="value">          The value. </param>
        /// <param name="disposeEnabled"> true to enable, false to disable the dispose. </param>
        public void AddUserControl( string title, UserControl value, bool disposeEnabled )
        {
            this.AddUserControl( title, value, disposeEnabled, true );
        }

        #endregion

        #region " PROPERTY NOTIFY CONTROL "

        /// <summary> true to enable, false to disable the disposal of the property notify control. </summary>
        private bool _PropertyNotifyControlDisposeEnabled;

        /// <summary> The property notify control. </summary>
        private ModelViewBase _PropertyNotifyControl;

        private ModelViewBase PropertyNotifyControl
        {
            [System.Runtime.CompilerServices.MethodImpl( System.Runtime.CompilerServices.MethodImplOptions.Synchronized )]
            get => this._PropertyNotifyControl;

            [System.Runtime.CompilerServices.MethodImpl( System.Runtime.CompilerServices.MethodImplOptions.Synchronized )]
            set {
                if ( this._PropertyNotifyControl != null )
                {
                    this._PropertyNotifyControl.PropertyChanged -= this.PropertyNotifyControlPropertyChanged;
                }

                this._PropertyNotifyControl = value;
                if ( this._PropertyNotifyControl != null )
                {
                    this._PropertyNotifyControl.PropertyChanged += this.PropertyNotifyControlPropertyChanged;
                }
            }
        }

        /// <summary> Adds a property notification control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void AddPropertyNotifyControl( ModelViewBase value )
        {
            this.AddPropertyNotifyControl( "Instrument", value, true );
        }

        /// <summary> Adds a property notification control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title"> The title. </param>
        /// <param name="value"> The value. </param>
        public void AddPropertyNotifyControl( string title, ModelViewBase value )
        {
            this.AddPropertyNotifyControl( title, value, true );
        }

        /// <summary> Adds a property notification control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title">             The title. </param>
        /// <param name="value">             The value. </param>
        /// <param name="disposeEnabled">    true to enable, false to disable the dispose. </param>
        /// <param name="showTraceMessages"> True to show, false to hide the trace messages. </param>
        public void AddPropertyNotifyControl( string title, ModelViewBase value, bool disposeEnabled, bool showTraceMessages )
        {
            this.ResizeClientArea( title, value, showTraceMessages );
            this.PropertyNotifyControl = value;
            if ( showTraceMessages )
            {
                this.InitializeTraceListener();
            }

            this._PropertyNotifyControlDisposeEnabled = disposeEnabled;
            this.PropertyNotifyControl.Dock = DockStyle.Fill;
            this.PropertyNotifyControl.TabIndex = 0;
            this.PropertyNotifyControl.BackColor = System.Drawing.Color.Transparent;
            this.PropertyNotifyControl.Font = new System.Drawing.Font( this.Font, System.Drawing.FontStyle.Regular );
            this.PropertyNotifyControl.Name = "_PropertyNotifyControl";
        }

        /// <summary> Adds a property notification control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title">          The title. </param>
        /// <param name="value">          The value. </param>
        /// <param name="disposeEnabled"> true to enable, false to disable the dispose. </param>
        public void AddPropertyNotifyControl( string title, ModelViewBase value, bool disposeEnabled )
        {
            this.AddPropertyNotifyControl( title, value, disposeEnabled, true );
        }

        /// <summary> Executes the property change action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ModelViewBase sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
            {
                return;
            }

            switch ( propertyName )
            {
                case (nameof( ModelViewBase.StatusPrompt )):
                    {
                        this.StatusPrompt = sender.StatusPrompt;
                        break;
                    }

                default:
                    {
                        break;
                    }
            }
        }

        /// <summary> Property view property changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property Changed event information. </param>
        private void PropertyNotifyControlPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
            {
                return;
            }

            string activity = $"handling {nameof( ModelViewBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.BeginInvoke( new Action<object, PropertyChangedEventArgs>( this.PropertyNotifyControlPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ModelViewBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.LogError( ex, activity );
            }
        }

        #endregion

        #region " Model View Logger "

        /// <summary> true to enable, false to disable the disposal of a Model View control. </summary>
        private bool _ModelViewControlDisposeEnabled;

        private ModelViewLoggerBase _ModelView;

        private ModelViewLoggerBase ModelView
        {
            [System.Runtime.CompilerServices.MethodImpl( System.Runtime.CompilerServices.MethodImplOptions.Synchronized )]
            get => this._ModelView;

            [System.Runtime.CompilerServices.MethodImpl( System.Runtime.CompilerServices.MethodImplOptions.Synchronized )]
            set {
                if ( this._ModelView != null )
                {
                    this._ModelView.PropertyChanged -= this.ModelViewPropertyChanged;
                }

                this._ModelView = value;
                if ( this._ModelView != null )
                {
                    this._ModelView.PropertyChanged += this.ModelViewPropertyChanged;
                }
            }
        }

        /// <summary> Adds a Model View logger control panel. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="value"> The value. </param>
        public void AddModelViewLoggerControl( ModelViewLoggerBase value )
        {
            this.AddModelViewLoggerControl( "View", value, true );
        }

        /// <summary> Adds an Model View logger control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title"> The title. </param>
        /// <param name="value"> The value. </param>
        public void AddModelViewLoggerControl( string title, ModelViewLoggerBase value )
        {
            this.AddModelViewLoggerControl( title, value, true );
        }

        /// <summary> Adds an Model View logger control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title">             The title. </param>
        /// <param name="value">             The value. </param>
        /// <param name="disposeEnabled">    true to enable, false to disable the dispose. </param>
        /// <param name="showTraceMessages"> True to show, false to hide the trace messages. </param>
        public void AddModelViewLoggerControl( string title, ModelViewLoggerBase value, bool disposeEnabled, bool showTraceMessages )
        {
            this.ResizeClientArea( title, value, showTraceMessages );
            this.ModelView = value;
            if ( showTraceMessages )
            {
                this.InitializeTraceListener();
            }

            this._ModelViewControlDisposeEnabled = disposeEnabled;
            this.ModelView.Dock = DockStyle.Fill;
            this.ModelView.TabIndex = 0;
            this.ModelView.BackColor = System.Drawing.Color.Transparent;
            this.ModelView.Font = new System.Drawing.Font( this.Font, System.Drawing.FontStyle.Regular );
            this.ModelView.Name = "_ModelViewLoggerControl";
        }

        /// <summary> Adds an Model View logger control. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="title">          The title. </param>
        /// <param name="value">          The value. </param>
        /// <param name="disposeEnabled"> true to enable, false to disable the dispose. </param>
        public void AddModelViewLoggerControl( string title, ModelViewLoggerBase value, bool disposeEnabled )
        {
            this.AddModelViewLoggerControl( title, value, disposeEnabled, true );
        }

        /// <summary> Executes the property change action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ModelViewLoggerBase sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
            {
                return;
            }

            switch ( propertyName )
            {
                case (nameof( ModelViewBase.StatusPrompt )):
                    {
                        this.StatusPrompt = sender.StatusPrompt;
                        break;
                    }

                default:
                    {
                        break;
                    }
            }

        }

        /// <summary> Model View control property changed. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property Changed event information. </param>
        private void ModelViewPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
            {
                return;
            }

            string activity = $"handling {nameof( ModelViewLoggerBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.BeginInvoke( new Action<object, PropertyChangedEventArgs>( this.ModelViewPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ModelViewLoggerBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.LogError( ex, activity );
            }
        }

        #endregion

        #region " EXPOSED CONTROLS "

        /// <summary> Gets the status strip. </summary>
        /// <value> The status strip. </value>
        protected StatusStrip StatusStrip { get; private set; }

        /// <summary> Gets the status label. </summary>
        /// <value> The status label. </value>
        protected ToolStripStatusLabel StatusLabel { get; private set; }

        /// <summary> Gets or sets the status prompt. </summary>
        /// <value> The status prompt. </value>
        public string StatusPrompt
        {
            get => this.StatusLabel.ToolTipText;

            set {
                if ( !string.Equals( value, this.StatusPrompt, StringComparison.OrdinalIgnoreCase ) )
                {
                    this.StatusLabel.Text = WinForms.CompactExtensions.CompactExtensionMethods.Compact( value, this.StatusLabel );
                    this.StatusLabel.ToolTipText = value;
                }
            }
        }

        #endregion

    }

}
