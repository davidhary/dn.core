using System;
using System.Runtime.CompilerServices;
using System.Windows.Forms;


namespace isr.Core.WinForms.ModelViewForm
{

    /// <summary> A form for hosing model views and a text box listener. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-26, 2.1.5836. </para>
    /// </remarks>
    public partial class ModelViewForm : Form
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Gets the initializing components sentinel. </summary>
        /// <value> The initializing components sentinel. </value>
        protected bool InitializingComponents { get; set; }

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        public ModelViewForm() : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.InitializingComponents = false;
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    // this._TraceMessagesBox.SuspendUpdatesReleaseIndicators();
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #region " CLASS STYLE "

        /// <summary> The enable drop shadow version. </summary>
        public const int EnableDropShadowVersion = 5;

        /// <summary> Gets the class style. </summary>
        /// <value> The class style. </value>
        protected Native.ClassStyleConstants ClassStyle { get; set; } = Native.ClassStyleConstants.None;

        /// <summary> Adds a drop shadow parameter. </summary>
        /// <remarks>
        /// From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx.
        /// </remarks>
        /// <value> Options that control the create. </value>
        protected override CreateParams CreateParams
        {
            get {
                var cp = base.CreateParams;
                cp.ClassStyle |= ( int ) this.ClassStyle;
                return cp;
            }
        }

        #endregion

        #endregion

        #region " SHOW "

        /// <summary>
        /// Shows the <see cref="RichTextBox">rich text box</see> form with these messages.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mdiForm"> The MDI form. </param>
        /// <param name="owner">   The owner. </param>
        public void Show( Form mdiForm, IWin32Window owner )
        {
            if ( mdiForm is object && mdiForm.IsMdiContainer )
            {
                this.MdiParent = mdiForm;
                mdiForm.Show();
            }

            this.Show( owner );
        }

        /// <summary>
        /// Shows the <see cref="RichTextBox">rich text box</see> form with these messages.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mdiForm"> The MDI form. </param>
        public void ShowDialog( Form mdiForm )
        {
            if ( mdiForm is object && mdiForm.IsMdiContainer )
            {
                this.MdiParent = mdiForm;
                mdiForm.Show();
            }

            _ = this.ShowDialog();
        }

        #endregion

        #region " TRACE LISTENER "

        private Core.Tracing.WinForms.TextBoxTraceEventWriter TextBoxTextWriter { get; set; }

        /// <summary>   Initializes the trace listener. </summary>
        /// <remarks>   David, 2021-03-04. </remarks>
        private void InitializeTraceListener()
        {
            this.TextBoxTextWriter = new( this._TraceMessagesBox );
            this.TextBoxTextWriter.ContainerPanel = this._MessagesTabPage;
            this.TextBoxTextWriter.TabCaption = "Log";
            this.TextBoxTextWriter.CaptionFormat = "{0} " + System.Text.Encoding.GetEncoding( 1200 ).GetString( new byte[] { 240 } );
            this.TextBoxTextWriter.ResetCount = 1000;
            this.TextBoxTextWriter.PresetCount = 500;

            this.AddDisplayTextWriter( this.TextBoxTextWriter );

            // Initialize the Alert Control 
#if false
            AllertToggle.AlertSoundEnabled = false;
            AllertToggle.AlertLevel = TraceEventType.Warning;
            AllertToggle.AlertsToggleControl = Me._AlertsToolStripTextBox.TextBox
#endif
        }

        #endregion

        #region " LOG EXCEPTION "

        /// <summary>   Adds an exception data. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="ex">   The exception. </param>
        /// <returns>   True if exception data was added for this exception; otherwise, false. </returns>
        protected virtual bool AddExceptionData( Exception ex )
        {
            return false;
        }

        /// <summary>   Log exception. </summary>
        /// <remarks>
        /// Declared as must override so that the calling method could add exception data before
        /// recording this exception.
        /// </remarks>
        /// <param name="ex">               The ex. </param>
        /// <param name="activity">         The activity. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
        /// <returns>   A String. </returns>
        protected string LogError( Exception ex, string activity, [CallerMemberName] string memberName = "",
                                                                  [CallerFilePath] string sourceFilePath = "",
                                                                  [CallerLineNumber] int sourceLineNumber = 0 )
        {
            _ = this.AddExceptionData( ex );
            return TraceLog.TraceLogger.LogError( ex, activity, memberName, sourceFilePath, sourceLineNumber );
        }

        #endregion
    }

}

namespace isr.Core.WinForms.ModelViewForm.Native
{
    /// <summary> Values that represent class style constants. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    [Flags]
    public enum ClassStyleConstants
    {

        /// <summary> . </summary>
        [System.ComponentModel.Description( "Not Specified" )]
        None = 0,

        /// <summary> . </summary>
        [System.ComponentModel.Description( "No close button" )]
        HideCloseButton = 0x200,

        /// <summary> . </summary>
        [System.ComponentModel.Description( "Drop Shadow" )]
        DropShadow = 0x20000 // 131072
    }

}

