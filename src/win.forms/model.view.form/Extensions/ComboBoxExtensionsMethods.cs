using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace isr.Core.WinForms.ModelViewForm.ComboBoxExtensions
{
    /// <summary>   A combo box extensions methods. </summary>
    /// <remarks>   David, 2021-03-04. </remarks>
    public static class ComboBoxExtensionsMethods
    {

        /// <summary> List trace event levels. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="control"> The control. </param>
        public static void ListTraceEventLevels( this System.Windows.Forms.ComboBox control )
        {
            if ( control is null )
            {
                throw new ArgumentNullException( nameof( control ) );
            }

            bool comboEnabled = control.Enabled;
            control.Enabled = false;
            control.DataSource = null;
            control.ValueMember = nameof( System.Collections.Generic.KeyValuePair<int, String>.Key );
            control.DisplayMember = nameof( System.Collections.Generic.KeyValuePair<int, String>.Value );
            control.Items.Clear();
            control.DataSource = TraceLog.TraceLogger.TraceEventValueNamePairs;
            // This does not seem to work. It lists the items in the combo box, but the control items are empty.
            // control.DataSource = isr.Core.Services.EnumExtensions.ValueDescriptionPairs(TraceEventType.Critical).ToList
            // control.SelectedIndex = -1
            control.Enabled = comboEnabled;
            control.Invalidate();
        }

        /// <summary> Select item. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="value">   The value. </param>
        public static void SelectItem( this System.Windows.Forms.ToolStripComboBox control, TraceEventType value )
        {
            if ( control is object )
            {
                // control.SelectedItem = New KeyValuePair(Of TraceEventType, String)(value, value.ToString)
                control.SelectedItem = TraceLog.TraceLogger.ToTraceEventValueNamePair( value );
            }
        }

        /// <summary> Selected value. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="control">      The control. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> A TraceEventType. </returns>
        public static TraceEventType SelectedValue( this System.Windows.Forms.ToolStripComboBox control, TraceEventType defaultValue )
        {
            if ( control is object && control.SelectedItem is object )
            {
                KeyValuePair<TraceEventType, string> kvp = ( KeyValuePair<TraceEventType, string> ) control.SelectedItem;
                if ( Enum.IsDefined( typeof( TraceEventType ), kvp.Key ) )
                {
                    defaultValue = kvp.Key;
                }
            }

            return defaultValue;
        }

    }
}
