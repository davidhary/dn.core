# About

isr.Json.WinForms.Settings is a .Net library providing a 
Windows forms and controls for editing Json configuration settings.

# How to Use

The following example is taken from the JsonSettingsEditor.Demo
project that is included in the [Core Framework Repository]. 

The project contains a Jason configuration settings file and 'Settings' class.

The editor form is instantiated and opened from the Main method as follows:
```
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.SetHighDpiMode( HighDpiMode.SystemAware );
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            Settings currentSettings = new();
            currentSettings.ReadSettings();
            Application.Run( new isr.Json.JsonSettingsEditorForm( "Settings editor demo", currentSettings ) );
        }
    }
```

# Key Features

* User interface for editing Json based configuration settings.

# Main Types

The main types provided by this library are:

* _JsonSettingsEditorControl_ A JSON configuration settings editor control.
* _JsonSettingsEditorForm_ An editor form for JSON configuration settings.

# Feedback

isr.Json.WinForms.Settings is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Core Framework Repository].

[Core Framework Repository]: https://bitbucket.org/davidhary/dn.core

