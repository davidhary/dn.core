using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Json
{

    /// <summary> An editor form for JSON configuration settings. </summary>
    /// <remarks> David, 2016-03-31. </remarks>
    public partial class JsonSettingsEditorForm : System.Windows.Forms.Form
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Prevents a default instance of the <see cref="JsonSettingsEditorForm" /> class from being
        /// created.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public JsonSettingsEditorForm() : base()
        {
            // This method is required by the Windows Form Designer.
            this.InitializeComponent();
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="JsonSettingsEditorForm" /> class from being created.
        /// </summary>
        /// <remarks>   David, 2021-07-26. </remarks>
        /// <param name="caption">  The caption. </param>
        /// <param name="settings">     The editable settings. </param>
        public JsonSettingsEditorForm( string caption, isr.Json.JsonSettingsBase settings ) : this()
        {
            this.Text = caption;
            this._Settings = settings;
            this.ResetMenuItem.Enabled = settings.Restorable;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="JsonSettingsEditorForm" /> class from being
        /// created.
        /// </summary>
        /// <remarks>   David, 2022-03-24. </remarks>
        /// <param name="caption">      The caption. </param>
        /// <param name="settings">     The editable settings. </param>
        /// <param name="appSettings">  The application settings. </param>
        public JsonSettingsEditorForm( string caption, isr.Json.JsonSettingsBase settings, isr.Json.JsonAppSettingsBase appSettings ) : this()
        {
            this.Text = caption;
            this._Settings = settings;
            this._AppSettings = appSettings;
            this.ResetMenuItem.Enabled = settings.Restorable;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion 

        #region " SHOW "

        /// <summary> Hides the dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="owner"> The owner. </param>
        /// <returns> A DialogResult. </returns>
        [Obsolete( "Illegal Call" )]
        public new System.Windows.Forms.DialogResult ShowDialog( System.Windows.Forms.IWin32Window owner )
        {
            this.Text = "Illegal Call";
            return base.ShowDialog( owner );
        }

        /// <summary> Hides the dialog. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> A System.Windows.Forms.DialogResult. </returns>
        [Obsolete( "Illegal Call" )]
        public new System.Windows.Forms.DialogResult ShowDialog()
        {
            this.Text = "Illegal Call";
            return base.ShowDialog();
        }

        /// <summary>   Shows the dialog. </summary>
        /// <remarks>   David, 202-09-12. </remarks>
        /// <param name="caption">  The caption. </param>
        /// <param name="settings"> Options for controlling the operation. </param>
        /// <returns>   A DialogResult. </returns>
        public System.Windows.Forms.DialogResult ShowDialog( string caption, isr.Json.JsonSettingsBase settings )
        {
            return this.ShowDialog( null, caption, settings );
        }

        /// <summary>   Shows the dialog. </summary>
        /// <remarks>   David, 202-09-12. </remarks>
        /// <param name="mdiForm">  The MDI form. </param>
        /// <param name="caption">  The caption. </param>
        /// <param name="settings"> Options for controlling the operation. </param>
        /// <returns>   A DialogResult. </returns>
        public System.Windows.Forms.DialogResult ShowDialog( System.Windows.Forms.Form mdiForm, string caption, isr.Json.JsonSettingsBase settings )
        {
            this.Show( mdiForm, caption, settings );
            while ( this.Visible )
            {
                System.Windows.Forms.Application.DoEvents();
            }

            return this.DialogResult;
        }

        /// <summary> Displays this dialog with title 'illegal call'. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        [Obsolete( "Illegal Call" )]
        public new void Show()
        {
            this.Text = "Illegal Call";
            base.Show();
        }

        /// <summary>   Shows this form. </summary>
        /// <remarks>   David, 202-09-12. </remarks>
        /// <param name="caption">  The caption. </param>
        /// <param name="settings"> Options for controlling the operation. </param>
        public void Show( string caption, isr.Json.JsonSettingsBase settings )
        {
            this.Show( null, caption, settings );
        }

        /// <summary>   Shows this form. </summary>
        /// <remarks>   David, 202-09-12. </remarks>
        /// <param name="mdiForm">  The MDI form. </param>
        /// <param name="caption">  The caption. </param>
        /// <param name="settings"> Options for controlling the operation. </param>
        public void Show( System.Windows.Forms.Form mdiForm, string caption, isr.Json.JsonSettingsBase settings )
        {
            if ( mdiForm is object && mdiForm.IsMdiContainer )
            {
                this.MdiParent = mdiForm;
                mdiForm.Show();
            }

            this.Text = caption;
            this._Settings = settings;
            base.Show();
        }

        /// <summary>   Edit settings. </summary>
        /// <remarks>   David, 2021-12-08. </remarks>
        /// <param name="caption">  The caption. </param>
        /// <param name="settings"> Options for controlling the operation. </param>
        /// <returns>   A DialogResult. </returns>
        public static DialogResult EditSettings( string caption, isr.Json.JsonSettingsBase settings )
        {
            var editor = new isr.Json.JsonSettingsEditorForm();
            editor.Controls.Add( new isr.Json.JsonSettingsEditorControl() { Settings = settings } );
            return editor.ShowDialog( caption, settings );
        }

        #endregion

        #region " FORM EVENTS "

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnShown( EventArgs e )
        {
            try
            {
                this.PropertyGrid.SelectedObject = this._Settings;
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
            finally
            {
                base.OnShown( e );
            }
        }

        #endregion 

        #region " HANDLERS "

        /// <summary> Cancel button click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void CancelButton_Click( object sender, EventArgs e )
        {
            this.Close();
        }

        /// <summary> Accept button click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AcceptButton_Click( object sender, EventArgs e )
        {
            this.SaveExit( sender, e );
        }

        private isr.Json.JsonSettingsBase _Settings;

        private isr.Json.JsonAppSettingsBase _AppSettings;

        /// <summary> Resets the menu item click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ResetMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                // string message = "Restoring the persisted application settings values to their corresponding default properties; Are you sure?";
                if ( System.Windows.Forms.MessageBox.Show( "Select Yes to restore the application settings from the Application Context settings file",
                    "Restore Setting; Are you sure?!", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button2, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly ) == System.Windows.Forms.DialogResult.Yes )
                {
                    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                    this.PropertyGrid.SelectedObject = null;
                    System.Windows.Forms.Application.DoEvents();
                    System.Threading.Thread.Sleep( 100 );
                    if ( this._AppSettings is object )
                    {
                        this._AppSettings.RestoreSettings();
                    }
                    else
                    {
                        this._Settings.RestoreSettings();
                    }
                    this.PropertyGrid.SelectedObject = this._Settings;
                    System.Windows.Forms.Application.DoEvents();
                }
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception Resetting Settings", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary> Reads menu item click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this.PropertyGrid.SelectedObject = null;
                System.Windows.Forms.Application.DoEvents();
                System.Threading.Thread.Sleep( 100 );
                if ( this._AppSettings is object )
                {
                    this._AppSettings.ReadSettings();
                }
                else
                {
                    this._Settings.ReadSettings();
                }
                this.PropertyGrid.SelectedObject = this._Settings;
                System.Windows.Forms.Application.DoEvents();
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception reading Settings", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary> Saves a menu item click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SaveMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                if ( this._AppSettings is object )
                {
                    this._AppSettings.SaveSettings();
                }
                else
                {
                    this._Settings.SaveSettings( new object[] { this._Settings } );
                }
                System.Windows.Forms.Application.DoEvents();
                System.Threading.Thread.Sleep( 100 );
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception saving Settings", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary> Saves an exit. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SaveExit( object sender, EventArgs e )
        {
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this._Settings.SaveSettings( new object[] { this._Settings } );
                System.Windows.Forms.Application.DoEvents();
                System.Threading.Thread.Sleep( 100 );
                this.Close();
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception saving Settings", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary> Saves an exit menu item click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SaveExitMenuItem_Click( object sender, EventArgs e )
        {
            this.SaveExit( sender, e );
        }

        /// <summary> Exit menu item click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ExitMenuItem_Click( object sender, EventArgs e )
        {
            this.Close();
        }

        #endregion 

    }

}
