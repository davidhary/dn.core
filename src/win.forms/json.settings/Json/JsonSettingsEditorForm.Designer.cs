using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.Json
{

    public partial class JsonSettingsEditorForm
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.PropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.MenuStrip = new System.Windows.Forms.MenuStrip();
            this.FileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ResetMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReadMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveExitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FileSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ExitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OkayButton = new System.Windows.Forms.Button();
            this.IgnoreButton = new System.Windows.Forms.Button();
            this.MenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // PropertyGrid
            // 
            this.PropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertyGrid.Location = new System.Drawing.Point(0, 24);
            this.PropertyGrid.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PropertyGrid.Name = "PropertyGrid";
            this.PropertyGrid.Size = new System.Drawing.Size(427, 335);
            this.PropertyGrid.TabIndex = 0;
            // 
            // MenuStrip
            // 
            this.MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileMenuItem});
            this.MenuStrip.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip.Name = "MenuStrip";
            this.MenuStrip.Size = new System.Drawing.Size(427, 24);
            this.MenuStrip.TabIndex = 1;
            this.MenuStrip.Text = "Menu Strip";
            // 
            // FileMenuItem
            // 
            this.FileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ResetMenuItem,
            this.ReadMenuItem,
            this.SaveMenuItem,
            this.SaveExitMenuItem,
            this.FileSeparator1,
            this.ExitMenuItem});
            this.FileMenuItem.Name = "FileMenuItem";
            this.FileMenuItem.Size = new System.Drawing.Size(37, 20);
            this.FileMenuItem.Text = "&File";
            // 
            // ResetMenuItem
            // 
            this.ResetMenuItem.Name = "ResetMenuItem";
            this.ResetMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ResetMenuItem.Text = "R&eset";
            this.ResetMenuItem.ToolTipText = "Restores the persisted application settings values to their corresponding default" +
    " properties";
            this.ResetMenuItem.Click += new System.EventHandler(this.ResetMenuItem_Click);
            // 
            // ReadMenuItem
            // 
            this.ReadMenuItem.Name = "ReadMenuItem";
            this.ReadMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ReadMenuItem.Text = "&Read";
            this.ReadMenuItem.Click += new System.EventHandler(this.ReadMenuItem_Click);
            // 
            // SaveMenuItem
            // 
            this.SaveMenuItem.Name = "SaveMenuItem";
            this.SaveMenuItem.Size = new System.Drawing.Size(180, 22);
            this.SaveMenuItem.Text = "&Save";
            this.SaveMenuItem.Click += new System.EventHandler(this.SaveMenuItem_Click);
            // 
            // SaveExitMenuItem
            // 
            this.SaveExitMenuItem.Name = "SaveExitMenuItem";
            this.SaveExitMenuItem.Size = new System.Drawing.Size(180, 22);
            this.SaveExitMenuItem.Text = "S&ave and Exit";
            this.SaveExitMenuItem.Click += new System.EventHandler(this.SaveExitMenuItem_Click);
            // 
            // FileSeparator1
            // 
            this.FileSeparator1.Name = "FileSeparator1";
            this.FileSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Name = "ExitMenuItem";
            this.ExitMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ExitMenuItem.Text = "E&xit";
            this.ExitMenuItem.Click += new System.EventHandler(this.ExitMenuItem_Click);
            // 
            // OkayButton
            // 
            this.OkayButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.OkayButton.Location = new System.Drawing.Point(304, 69);
            this.OkayButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.OkayButton.Name = "OkayButton";
            this.OkayButton.Size = new System.Drawing.Size(75, 30);
            this.OkayButton.TabIndex = 2;
            this.OkayButton.Text = "Okay";
            this.OkayButton.UseVisualStyleBackColor = true;
            this.OkayButton.Click += new System.EventHandler(this.AcceptButton_Click);
            // 
            // IgnoreButton
            // 
            this.IgnoreButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.IgnoreButton.Location = new System.Drawing.Point(304, 69);
            this.IgnoreButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.IgnoreButton.Name = "IgnoreButton";
            this.IgnoreButton.Size = new System.Drawing.Size(75, 30);
            this.IgnoreButton.TabIndex = 2;
            this.IgnoreButton.Text = "Cancel";
            this.IgnoreButton.UseVisualStyleBackColor = true;
            this.IgnoreButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // JsonSettingsEditorForm
            // 
            this.AcceptButton = this.OkayButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.IgnoreButton;
            this.ClientSize = new System.Drawing.Size(427, 359);
            this.Controls.Add(this.PropertyGrid);
            this.Controls.Add(this.MenuStrip);
            this.Controls.Add(this.OkayButton);
            this.Controls.Add(this.IgnoreButton);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.MenuStrip;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "JsonSettingsEditorForm";
            this.Text = "Json Settings Editor";
            this.MenuStrip.ResumeLayout(false);
            this.MenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.PropertyGrid PropertyGrid;
        private System.Windows.Forms.MenuStrip MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem FileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReadMenuItem;
        private System.Windows.Forms.ToolStripSeparator FileSeparator1;
        private System.Windows.Forms.ToolStripMenuItem ExitMenuItem;
        private System.Windows.Forms.Button OkayButton;
        private System.Windows.Forms.Button IgnoreButton;
        private System.Windows.Forms.ToolStripMenuItem SaveMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveExitMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ResetMenuItem;

    }
}
