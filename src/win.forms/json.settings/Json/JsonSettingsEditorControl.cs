using System;
using System.Windows.Forms;

namespace isr.Json
{

    /// <summary>   A JSON configuration settings editor control. </summary>
    /// <remarks>   David, 2021-07-27. </remarks>
    public partial class JsonSettingsEditorControl : UserControl
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-07-27. </remarks>
        public JsonSettingsEditorControl()
        {
            this.InitializeComponent();
        }

        #endregion

        #region " HANDLERS "

        private isr.Json.JsonSettingsBase _Settings;
        /// <summary>   Gets or sets the application settings section which to edit. </summary>
        /// <value> The editable settings. </value>
        public isr.Json.JsonSettingsBase Settings
        {
            get => this._Settings;
            set {
                this._Settings = value;
                this.ResetMenuItem.Enabled = value is object && value.Restorable;
            }
        }

        private isr.Json.JsonAppSettingsBase _AppSettings;
        /// <summary>   Gets or sets the application settings which includes the editable application settings. </summary>
        /// <value> The settings. </value>
        public isr.Json.JsonAppSettingsBase AppSettings
        {
            get => this._AppSettings;
            set {
                this._AppSettings = value;
                this.ResetMenuItem.Enabled = value is object && value.Restorable;
            }
        }

        /// <summary> Resets the menu item click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ResetMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                // string message = "Restoring the persisted application settings values to their corresponding default properties; Are you sure?";
                if ( System.Windows.Forms.MessageBox.Show( "Select Yes to restore the application settings from the Application Context settings file",
                    "Restore Setting; Are you sure?!", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button2, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly ) == System.Windows.Forms.DialogResult.Yes )
                {
                    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                    this.PropertyGrid.SelectedObject = null;
                    System.Windows.Forms.Application.DoEvents();
                    System.Threading.Thread.Sleep( 100 );
                    if ( this.AppSettings is object ) {
                        this.AppSettings.RestoreSettings();
                    }
                    else
                    {
                        this.Settings.RestoreSettings();
                    }
                    this.PropertyGrid.SelectedObject = this.Settings;
                    System.Windows.Forms.Application.DoEvents();
                }
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception Resetting Settings", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary> Reads menu item click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this.PropertyGrid.SelectedObject = null;
                System.Windows.Forms.Application.DoEvents();
                System.Threading.Thread.Sleep( 100 );
                if ( this.AppSettings is object )
                {
                    this.AppSettings.ReadSettings();
                }
                else
                {
                    this.Settings.ReadSettings();
                }
                this.PropertyGrid.SelectedObject = this.Settings;
                System.Windows.Forms.Application.DoEvents();
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception reading Settings", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary> Saves a menu item click. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SaveMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                if ( this.AppSettings is object )
                {
                    this.AppSettings.SaveSettings();
                }
                else
                {
                    this.Settings.SaveSettings( new object[] { this.Settings } );
                }
                System.Windows.Forms.Application.DoEvents();
                System.Threading.Thread.Sleep( 100 );
            }
            catch ( Exception ex )
            {
                _ = System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception saving Settings", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion 
    }
}
