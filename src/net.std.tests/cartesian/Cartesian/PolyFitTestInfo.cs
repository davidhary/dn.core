
namespace isr.Core.Cartesian.MSTest
{

    /// <summary> Test information for the Polynomial Fit Tests. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [isr.Json.SettingsSection( nameof( PolyFitTestInfo ) )]
    internal class PolyFitTestInfo : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public PolyFitTestInfo() : base( System.Reflection.Assembly.GetAssembly( typeof( PolyFitTestInfo ) ) )
        { }

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        public virtual bool Verbose { get; set; }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        public virtual bool Enabled { get; set; }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        public virtual bool All { get; set; }

        #endregion

        #region " POLY FIT "

        /// <summary> Gets or sets the number of minimum elements. </summary>
        /// <value> The number of minimum elements. </value>
        public int MinimumElementCount { get; set; }

        #endregion

    }
}
