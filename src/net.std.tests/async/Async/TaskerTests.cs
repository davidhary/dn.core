using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Async.MSTest
{

    /// <summary> A tasker tests. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class TaskerTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " TASKER TESTS  "

        /// <summary> Assert tasker exception. </summary>
        /// <remarks> David, 2020-08-06. </remarks>
        /// <exception cref="OperationCanceledException"> Thrown when an Operation Canceled error condition
        /// occurs. </exception>
        public static void AssertTaskerException()
        {
            var tasker = new Tasker();
            // task should throw an exception
            string activity = "";
            activity = "Task started";
            try
            {
                activity = "Task action";
                tasker.StartAction( () => { throw new OperationCanceledException( "thrown from withing the task action" ); } );
                activity = "Task awaiting task idle";
                Assert.IsTrue( tasker.AwaitTaskIdle( TimeSpan.FromMilliseconds( 200d ) ), $"{nameof( Tasker.AwaitTaskIdle )} should timeout" );
            }
            catch ( Exception ex )
            {
                Console.Out.WriteLine( $"thrown from {activity}" );
                throw new OperationCanceledException( $"thrown from {activity}", ex );
            }
        }

        /// <summary>   Assert tasker asynchronous completed should report exception private. </summary>
        /// <remarks>   David, 2020-08-06. </remarks>
        /// <exception cref="OperationCanceledException">   Thrown when a thread cancels a running
        ///                                                 operation. </exception>
        private static void AssertTaskerAsyncCompletedShouldReportExceptionPrivate( bool expectsException = true )
        {
            var startTime = default( DateTimeOffset );
            var captureTimestamp = default( TimeSpan );
            var exceptionTimestamp = default( TimeSpan );
            var actionExitTimestamp = TimeSpan.Zero;
            TimeSpan taskDoneTimestamp;
            var handled = false;
            var isFaulted = default( bool );
            Exception exception = null;
            var tasker = new Tasker();
            tasker.AsyncCompleted += ( sender, e ) => {
                handled = true;
                isFaulted = e.Error is object;
                exception = e.Error;
                captureTimestamp = DateTimeOffset.Now.Subtract( startTime );
            };
            // task should throw an exception to be captured by the tasker before the wait occurred.
            string activity = "Task started";
            activity = "Task action";
            startTime = DateTimeOffset.Now;
            tasker.StartAction( () => {
                exceptionTimestamp = DateTimeOffset.Now.Subtract( startTime );
                if ( expectsException )
                    throw new OperationCanceledException( $"{activity} thrown from withing the task action" );
            } );
            activity = "Task awaiting task idle";
            Assert.IsTrue( tasker.AwaitTaskIdle( TimeSpan.FromMilliseconds( 400d ) ), $"{nameof( Tasker.AwaitTaskIdle )} should not timeout" );
            Assert.IsTrue( handled, $"{nameof( Tasker.AsyncCompleted )} should be handled" );
            Assert.AreEqual( expectsException, isFaulted, $"{nameof( Tasker.ActionTask )}.{nameof( Task.IsFaulted )} should match" );
            if ( expectsException )
            {
                Assert.IsNotNull( exception, $"{nameof( AsyncCompletedEventArgs )}.{nameof( AsyncCompletedEventArgs.Error )} should not be null" );
                Assert.IsNotNull( exception.InnerException as OperationCanceledException, $"{nameof( AsyncCompletedEventArgs )}.{nameof( AsyncCompletedEventArgs.Error )} should be {nameof( OperationCanceledException )}" );
            }
            else
            {
                Assert.IsNull( exception, $"{nameof( AsyncCompletedEventArgs )}.{nameof( AsyncCompletedEventArgs.Error )} should be null" );
            }
            Assert.IsFalse( tasker.ActionTask.IsFaulted, $"{nameof( Tasker.ActionTask )}.{nameof( Task.IsFaulted )} should match as {nameof( Tasker.ActionTask )} does not register the exception" );
            taskDoneTimestamp = DateTimeOffset.Now.Subtract( startTime );
            // Timestamps: Exception 0; Capture: 1.3379; action exit 0; Done 1.3379
            Console.Out.WriteLine( $"Timestamps {(expectsException ? "with" : "without")} error: Exception {exceptionTimestamp.TotalMilliseconds} ms; Capture: {captureTimestamp.TotalMilliseconds} ms; action exit {actionExitTimestamp.TotalMilliseconds} ms; Done {taskDoneTimestamp.TotalMilliseconds} ms" );
        }

        /// <summary>   Assert tasker asynchronous completed should report exception. </summary>
        /// <remarks>   David, 2020-07-17. </remarks>
        public static void AssertTaskerAsyncCompletedShouldReportException()
        {
            var tasker = new Tasker();
            var timeout = TimeSpan.FromMilliseconds( 100d );
            var delay = timeout.Subtract( TimeSpan.FromMilliseconds( 50d ) );
            tasker.StartAction( () => Thread.Sleep( delay ) );
            Assert.IsTrue( tasker.AwaitTaskIdle( timeout ), $"{nameof( Tasker.AwaitTaskIdle )} should not timeout" );
            delay = timeout.Add( TimeSpan.FromMilliseconds( 50d ) );
            tasker.StartAction( () => Thread.Sleep( delay ) );
            Assert.IsFalse( tasker.AwaitTaskIdle( timeout ), $"{nameof( Tasker.AwaitTaskIdle )} should timeout" );

            // MyAssertProperty.MyAssert.Throws(Of OperationCanceledException)(AddressOf AssertTaskerException)
            AssertTaskerAsyncCompletedShouldReportExceptionPrivate( true );
            AssertTaskerAsyncCompletedShouldReportExceptionPrivate( false );
        }

        /// <summary>
        /// (Unit Test Method) tasker asynchronous completed should report exception.
        /// </summary>
        /// <remarks>   David, 2020-07-17. </remarks>
        [TestMethod()]
        public void TaskerAsyncCompletedShouldReportException()
        {
            AssertTaskerAsyncCompletedShouldReportException();
        }

        /// <summary> Assert tasker of tuple. </summary>
        /// <remarks> David, 2020-07-17. </remarks>
        public static void AssertTaskerResultShouldEqualTuple()
        {
            var tasker = new Tasker<(bool Success, string Details)>();
            var timeout = TimeSpan.FromMilliseconds( 100d );
            var delay = timeout.Subtract( TimeSpan.FromMilliseconds( 50d ) );
            (bool Success, string Details) expectedResult = (true, "Success");
            tasker.StartAction( () => {
                Thread.Sleep( delay );
                return expectedResult;
            } );
            Assert.IsTrue( tasker.AwaitTaskIdle( timeout ), $"{nameof( Tasker.AwaitTaskIdle )} should not timeout" );
            var result = tasker.AwaitCompletion( timeout );
            Assert.AreEqual( TaskStatus.RanToCompletion, result.Status, $"{nameof( TaskStatus )} should match" );
            Assert.AreEqual( expectedResult.Details, result.Result.Details, $"{nameof( Tasker<(bool Success, string Details)> )} Result should match" );
            expectedResult = (false, "Failure");
            tasker.StartAction( () => {
                Thread.Sleep( delay );
                return expectedResult;
            } );
            result = tasker.AwaitCompletion( timeout );
            Assert.AreEqual( TaskStatus.RanToCompletion, result.Status, $"{nameof( TaskStatus )} should match" );
            Assert.AreEqual( expectedResult.Details, result.Result.Details, $"{nameof( Tasker<(bool Success, string Details)> )} Result should match" );
        }

        /// <summary>   (Unit Test Method) tasker result should equal tuple. </summary>
        /// <remarks>   David, 2020-07-17. </remarks>
        [TestMethod()]
        public void TaskerResultShouldEqualTuple()
        {
            AssertTaskerResultShouldEqualTuple();
        }

        #endregion

    }
}
