using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Notifiers.MSTest
{

    /// <summary>   (Unit Test Class) an action notifier tests. </summary>
    /// <remarks>   David, 2020-09-18. </remarks>
    [TestClass()]
    public class ActionNotifierTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " ACTION NOTIFIER TEST "

        /// <summary> The actual notified value. </summary>
        private string _ActualNotifiedValue = string.Empty;

        /// <summary> Handles the action notify described by value. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="value"> The value. </param>
        private void HandleActionNotify( string value )
        {
            this._ActualNotifiedValue = value;
        }

        /// <summary> Registers the action. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="sender">             Source of the event. </param>
        /// <param name="engineFailedAction"> The engine failed action. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        private void RegisterAction( ActionNotifier<string> sender, Action<string> engineFailedAction )
        {
            sender.Register( engineFailedAction );
        }

        /// <summary> (Unit Test Method) tests action notifier handling. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ActionNotifierShouldNotify()
        {
            var sender = new ActionNotifier<string>();
            this.RegisterAction( sender, this.HandleActionNotify );
            string expectedNotificationValue = $"{nameof( ActionNotifier<string>.Invoke )}";
            sender.Invoke( expectedNotificationValue );
            Assert.AreEqual( expectedNotificationValue, this._ActualNotifiedValue, "expects a notification value" );
        }

        #endregion

    }
}
