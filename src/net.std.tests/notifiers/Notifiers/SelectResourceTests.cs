using System;
using System.ComponentModel;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Notifiers.MSTest
{

    /// <summary>   (Unit Test Class) a select resource tests. </summary>
    /// <remarks>   David, 2020-09-22. </remarks>
    [TestClass()]
    public class SelectResourceTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }


        #endregion

        #region " SELECT RESOURCE TESTS "

        private class Selector : SelectResourceBase
        {
            public override BindingList<string> EnumerateResources( bool applyEnabledFilters )
            {
                throw new NotImplementedException();
            }

            public override bool QueryResourceExists( string resourceName )
            {
                throw new NotImplementedException();
            }

            public override (bool Success, string Details) TryValidateResource( string resourceName )
            {
                throw new NotImplementedException();
            }

            protected override void OnResourceNameValidated( string resourceName, EventArgs e )
            {
                throw new NotImplementedException();
            }

            protected override void OnValidatingResourceName( string resourceName, CancelEventArgs e )
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>   (Unit Test Method) select resource should be constructed. </summary>
        /// <remarks>   David, 2020-10-10. </remarks>
        [TestMethod()]
        public void SelectResourceShouldBeConstructed()
        {
            _ = new Selector();
        }

        #endregion

    }
}
