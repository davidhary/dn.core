using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Json.MSTest.Settings
{
    /// <summary>
    /// This is a test class for MyAssemblyInfoTest and is intended
    /// to contain all MyAssemblyInfoTest Unit Tests
    /// </summary>
    [TestClass()]
    public class ApplicationSettingTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        /// <remarks>Use ClassInitialize to run code before running the first test in the class</remarks>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( testContext.FullyQualifiedTestClassName );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #endregion

        /// <summary>   Gets the filename of the application settings context file. </summary>
        /// <value> The filename of the application settings context file. </value>
        public static string AppSettingsContextFileName
        {
            get {
                var name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                return System.IO.Path.Combine( AppContext.BaseDirectory, isr.Json.JsonSettingsBase.BuildAppSettingsFileName( name ) );
            }
        }

        /// <summary>   (Unit Test Method) application settings file name should equal. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        [TestMethod()]
        public void ApplicationSettingsFileNameShouldEqual()
        {
            Settings jsonSettings = new();
            Assert.AreEqual( jsonSettings.AppDataSettingsFullFileName, jsonSettings.FullFileName );
        }

        /// <summary>   (Unit Test Method) application settings section name should equal. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        [TestMethod()]
        public void ApplicationSettingsSectionNameShouldEqual()
        {
            Settings jsonSettings = new();
            Assert.AreEqual( nameof( Settings ), jsonSettings.SectionName );
        }

        /// <summary>   (Unit Test Method) application settings section name should equal. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        [TestMethod()]
        public void ApplicationTestSiteSettingsSectionNameShouldEqual()
        {
            TestSiteSettings jsonSettings = new();
            var sectionName = nameof( TestSiteSettings );
            Assert.AreEqual( sectionName, jsonSettings.SectionName );
        }

        [TestMethod()]
        public void ApplicationTestSiteSettingsShouldSave()
        {
         
            // get the size of the arrays
            string prefixes = AppSettings.Instance.TestSiteSettings.IPv4Prefixes;
            bool verboseValue = AppSettings.Instance.TestSiteSettings.Verbose;
            AppSettings.Instance.SaveSettings();
            AppSettings.Instance.ReadSettings();
            Assert.AreEqual( prefixes, AppSettings.Instance.TestSiteSettings.IPv4Prefixes,
                             $"{nameof( TestSiteSettings.IPv4Prefixes )} should save and read the correct value" );
            Assert.AreEqual( verboseValue, AppSettings.Instance.TestSiteSettings.Verbose,
                             $"{nameof( TestSiteSettings.Verbose )} should save and read the correct value" );
            AppSettings.Instance.TestSiteSettings.IPv4Prefixes = string.Empty;
            AppSettings.Instance.TestSiteSettings.Verbose = !verboseValue;
            Assert.AreEqual( string.Empty, AppSettings.Instance.TestSiteSettings.IPv4Prefixes,
                             $"{nameof( TestSiteSettings.IPv4Prefixes )} should save and read the changed value" );
            Assert.AreEqual( !verboseValue, AppSettings.Instance.TestSiteSettings.Verbose,
                             $"{nameof( TestSiteSettings.Verbose )} should save and read the changed value" );
            AppSettings.Instance.RestoreSettings();
            Assert.AreEqual( prefixes, AppSettings.Instance.TestSiteSettings.IPv4Prefixes,
                             $"{nameof( TestSiteSettings.IPv4Prefixes )} should restore the settings value" );
            Assert.AreEqual( verboseValue, AppSettings.Instance.TestSiteSettings.Verbose,
                             $"{nameof( TestSiteSettings.Verbose )} should restore the settings value" );

        }

        /// <summary>
        /// (Unit Test Method) queries if a given application setting file should exists.
        /// </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void ApplicationSettingFileShouldExists()
        {
            Settings jsonSettings = new();
            var fi = new System.IO.FileInfo( jsonSettings.FullFileName );
            Assert.IsTrue( fi.Exists, $"{fi.FullName} should exists" );
        }

        /// <summary>   (Unit Test Method) application setting file should save. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void ApplicationSettingShouldSave()
        {
            // toggle boolean value and see if it is saved.
            var boolValue = AppSettings.Instance.Settings.CheckUnpublishedMessageLogFileSize;
            AppSettings.Instance.Settings.CheckUnpublishedMessageLogFileSize = !boolValue;
            AppSettings.Instance.SaveSettings();
            AppSettings.Instance.ReadSettings();
            Assert.AreEqual( !boolValue, AppSettings.Instance.Settings.CheckUnpublishedMessageLogFileSize,
                             $"{nameof( Settings.CheckUnpublishedMessageLogFileSize )} Changed value should be saved and read" );
            AppSettings.Instance.Settings.CheckUnpublishedMessageLogFileSize = boolValue;
            AppSettings.Instance.SaveSettings();
            AppSettings.Instance.ReadSettings();
            Assert.AreEqual( boolValue, AppSettings.Instance.Settings.CheckUnpublishedMessageLogFileSize,
                             $"{nameof( Settings.CheckUnpublishedMessageLogFileSize )} Restored value should be saved and read" );
        }

        [TestMethod()]
        public void ApplicationSettingTestInfoShouldSave()
        {
            // toggle boolean value and see if it is saved.
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue = !boolValue;
            AppSettings.Instance.SaveSettings();
            AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue = !AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue;
            AppSettings.Instance.ReadSettings();
            Assert.AreEqual( !boolValue, AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue, $"Changed value should be saved and read" );
            AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue = boolValue;
            AppSettings.Instance.SaveSettings();
            AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue = !AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue;
            AppSettings.Instance.ReadSettings();
            Assert.AreEqual( boolValue, AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue, $"Restored value should be saved and read" );

        }

        /// <summary>   (Unit Test Method) boolean application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void BooleanApplicationSettingShouldEqual()
        {
            AppSettings.Instance.ReadSettings();
            Assert.AreEqual( ApplicationSettingsTestsInfo.BooleanValueExpected, AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue );
        }

        /// <summary>   (Unit Test Method) boolean application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void BooleanApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue = ApplicationSettingsTestsInfo.BooleanValueOther;
            Assert.AreEqual( ApplicationSettingsTestsInfo.BooleanValueOther, AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.BooleanValue = boolValue;
        }

        /// <summary>   (Unit Test Method) string setting value should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void StringSettingValueShouldEqual()
        {
            var expectedDummyValue = "Dummy";
            Assert.AreEqual( expectedDummyValue, AppSettings.Instance.Settings.Dummy );
        }


        /// <summary>   (Unit Test Method) String application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void StringApplicationSettingShouldEqual()
        {
            Assert.AreEqual( ApplicationSettingsTestsInfo.StringValueExpected, AppSettings.Instance.ApplicationSettingsTestsInfo.StringValue );
        }

        /// <summary>   (Unit Test Method) String application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void StringApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.StringValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.StringValue = ApplicationSettingsTestsInfo.StringValueOther;
            Assert.AreEqual( ApplicationSettingsTestsInfo.StringValueOther, AppSettings.Instance.ApplicationSettingsTestsInfo.StringValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.StringValue = boolValue;
        }

        /// <summary>   (Unit Test Method) Integer application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void IntegerApplicationSettingShouldEqual()
        {
            Assert.AreEqual( ApplicationSettingsTestsInfo.IntegerValueExpected, AppSettings.Instance.ApplicationSettingsTestsInfo.IntegerValue );
        }

        /// <summary>   (Unit Test Method) Integer application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void IntegerApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.IntegerValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.IntegerValue = ApplicationSettingsTestsInfo.IntegerValueOther;
            Assert.AreEqual( ApplicationSettingsTestsInfo.IntegerValueOther, AppSettings.Instance.ApplicationSettingsTestsInfo.IntegerValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.IntegerValue = boolValue;
        }

        /// <summary>   (Unit Test Method) Byte application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void ByteApplicationSettingShouldEqual()
        {
            Assert.AreEqual( ApplicationSettingsTestsInfo.ByteValueExpected, AppSettings.Instance.ApplicationSettingsTestsInfo.ByteValue );
        }

        /// <summary>   (Unit Test Method) Byte application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void ByteApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.ByteValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.ByteValue = ApplicationSettingsTestsInfo.ByteValueOther;
            Assert.AreEqual( ApplicationSettingsTestsInfo.ByteValueOther, AppSettings.Instance.ApplicationSettingsTestsInfo.ByteValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.ByteValue = boolValue;
        }

        /// <summary>   (Unit Test Method) Timespan application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void TimespanApplicationSettingShouldEqual()
        {
            Assert.AreEqual( ApplicationSettingsTestsInfo.TimespanValueExpected(), AppSettings.Instance.ApplicationSettingsTestsInfo.TimespanValue );
        }

        /// <summary>   (Unit Test Method) Timespan application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void TimespanApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.TimespanValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.TimespanValue = ApplicationSettingsTestsInfo.TimespanValueOther();
            Assert.AreEqual( ApplicationSettingsTestsInfo.TimespanValueOther(), AppSettings.Instance.ApplicationSettingsTestsInfo.TimespanValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.TimespanValue = boolValue;
        }

        /// <summary>   (Unit Test Method) DateTime application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void DateTimeApplicationSettingShouldEqual()
        {
            Assert.AreEqual( ApplicationSettingsTestsInfo.DateTimeValueExpected(), AppSettings.Instance.ApplicationSettingsTestsInfo.DateTimeValue );
        }

        /// <summary>   (Unit Test Method) DateTime application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void DateTimeApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.DateTimeValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.DateTimeValue = ApplicationSettingsTestsInfo.DateTimeValueOther();
            Assert.AreEqual( ApplicationSettingsTestsInfo.DateTimeValueOther(), AppSettings.Instance.ApplicationSettingsTestsInfo.DateTimeValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.DateTimeValue = boolValue;
        }

        /// <summary>   (Unit Test Method) TraceEventType application setting should equal. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void TraceEventTypeApplicationSettingShouldEqual()
        {
            Assert.AreEqual( ApplicationSettingsTestsInfo.TraceEventTypeValueExpected, AppSettings.Instance.ApplicationSettingsTestsInfo.TraceEventTypeValue );
        }

        /// <summary>   (Unit Test Method) TraceEventType application setting should change. </summary>
        /// <remarks>   David, 2021-01-29. </remarks>
        [TestMethod()]
        public void TraceEventTypeApplicationSettingShouldChange()
        {
            var boolValue = AppSettings.Instance.ApplicationSettingsTestsInfo.TraceEventTypeValue;
            AppSettings.Instance.ApplicationSettingsTestsInfo.TraceEventTypeValue = ApplicationSettingsTestsInfo.TraceEventTypeValueOther;
            Assert.AreEqual( ApplicationSettingsTestsInfo.TraceEventTypeValueOther, AppSettings.Instance.ApplicationSettingsTestsInfo.TraceEventTypeValue );
            AppSettings.Instance.ApplicationSettingsTestsInfo.TraceEventTypeValue = boolValue;
        }

    }
}
