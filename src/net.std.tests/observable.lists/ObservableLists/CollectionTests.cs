using System;
using System.Collections.Specialized;

using isr.Core.ObservableLists;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Lists.MSTest
{

    /// <summary> This is a test class for event handling. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestClass()]
    public class CollectionTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }


        #endregion

        #region " SYNCHRONIZED COLLECTION TESTS "

        /// <summary> The actual action. </summary>
        private NotifyCollectionChangedAction _ActualAction;

        /// <summary> Handles the collection changed. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Notify collection changed event information. </param>
        private void HandleCollectionChanged( object sender, NotifyCollectionChangedEventArgs e )
        {
            if ( !(sender is SynchronizedObservableCollection<string>) )
            {
                return;
            }

            this._ActualAction = e.Action;
        }

        /// <summary> (Unit Test Method) tests property change event handler. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void CollectionChangeHandlingTest()
        {
            using var sender = new SynchronizedObservableCollection<string>();
            sender.CollectionChanged += this.HandleCollectionChanged;
            sender.Add( $"Item#{sender.Count}" );
            var expectedAction = NotifyCollectionChangedAction.Add;
            Assert.AreEqual( expectedAction, this._ActualAction, "actions should equal after add" );
        }

        #endregion

    }
}
