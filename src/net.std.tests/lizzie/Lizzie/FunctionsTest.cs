using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Lizzie.MSTest
{

    /// <summary> Functions tests. </summary>
    /// <remarks>
    /// David, 2019-02-03  <para> https://github.com/polterguy/lizzie/.  </para><para>
    /// Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [TestClass()]
    public class FunctionsTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }


        #endregion

        #region " EVALUATE TESTS "

        /// <summary> (Unit Test Method) evaluates this object. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void Evaluate()
        {
            var functions = new Functions<string>() { () => "1", () => "2", () => "3" };
            string result = string.Empty;
            foreach ( string idx in functions.Evaluate() )
            {
                result += idx;
            }

            Assert.AreEqual( "123", result );
        }

        /// <summary> (Unit Test Method) evaluate parallel. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void EvaluateParallel()
        {
            var functions = new Functions<string>() { () => "1", () => "2", () => "3" };
            using var sync = new Synchronizer<string>( "" );
            foreach ( string idx in functions.EvaluateParallel() )
            {
                sync.Assign( input => input + idx );
            }

            bool outcome = false;
            string result = string.Empty;
            sync.Read( x => result = x );
            outcome = result == "123" || result == "132" || result == "231" || result == "213" || result == "312" || result == "321";
            Assert.IsTrue( outcome );
        }
        #endregion

    }
}
