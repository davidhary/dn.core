using System;

using isr.Core.Models;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Models.MSTest
{

    /// <summary> Event handling tests. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestClass()]
    public class EventHandlingTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.ConcurrentQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.ConcurrentQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }


        #endregion

        #region " VIEW MODEL PROPERTY CHANGE EVENT HANDLING TESTS "

        /// <summary> A property change. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        private class PropertyChange : ViewModelBase
        {
            /// <summary> The expected value. </summary>
            private string _ExpectedValue;

            /// <summary> Gets or sets the expected value. </summary>
            /// <value> The expected value. </value>
            public string ExpectedValue
            {
                get => this._ExpectedValue;

                set {
                    if ( !string.Equals( value, this.ExpectedValue ) )
                    {
                        this._ExpectedValue = value;
                        this.SyncNotifyPropertyChanged();
                    }
                }
            }

            /// <summary> Gets or sets the actual value. </summary>
            /// <value> The actual value. </value>
            public string ActualValue { get; set; }
        }

        /// <summary> Handles the property changed. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        private void HandlePropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( sender is not PropertyChange propertyChangeSender )
            {
                return;
            }

            switch ( e?.PropertyName ?? "" )
            {
                case nameof( PropertyChange.ExpectedValue ):
                    {
                        propertyChangeSender.ActualValue = propertyChangeSender.ExpectedValue;
                        break;
                    }
            }
        }

        /// <summary> (Unit Test Method) tests property change event handler. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        [TestMethod()]
        public void PropertyChangeHandlingTest()
        {
            var sender = new PropertyChange();
            sender.PropertyChanged += this.HandlePropertyChanged;
            sender.ExpectedValue = nameof( PropertyChange.ExpectedValue );
            Assert.AreEqual( sender.ExpectedValue, sender.ActualValue, "should equal after handling the synchronized event" );
        }

        #endregion

    }
}
