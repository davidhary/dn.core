using System;

namespace isr.Core.Models.MSTest
{
    /// <summary>   An application settings. </summary>
    /// <remarks>   David, 2021-01-28. </remarks>
    public class AppSettings : isr.Json.JsonAppSettingsBase
    {

        #region " CONSTRUCTION "

        private AppSettings() : base()
        {
            this.Settings = new Settings();
            base.AddSettingsSection( this.Settings );
            this.TestSiteSettings = new TestSiteSettings();
            base.AddSettingsSection( this.TestSiteSettings );
            this.ReadSettings();
        }

        private static readonly object Instancelock = new object();

        private static AppSettings _Instance = null;

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static AppSettings Instance
        {
            get {
                if ( AppSettings._Instance == null )
                {
                    lock ( AppSettings.Instancelock )
                    {
                        if ( AppSettings._Instance == null )
                        {
                            AppSettings._Instance = new AppSettings();
                        }
                    }
                }
                return AppSettings._Instance;
            }
        }

        #endregion

        /// <summary>   Gets the full name of the application settings file. </summary>
        /// <value> The full name of the application settings file. </value>
        public string AppSettingsFullFileName => this.Settings.SettingsFullFileName;

        /// <summary>   Gets or sets options for controlling the operation. </summary>
        /// <remarks>
        /// Note that the name of the field must match  class name or the name of the section as defined
        /// in the class <see cref="isr.Core.Json.SettingsSectionAttribute"/> or.
        /// </remarks>
        /// <value> The settings. </value>
        internal Settings Settings { get; }

        /// <summary>   Gets or sets the test site settings. </summary>
        /// <remarks>
        /// Note that the name of the field must match  class name or the name of the section as defined
        /// in the class <see cref="isr.Core.Json.SettingsSectionAttribute"/> or.
        /// </remarks>
        /// <value> The test site settings. </value>
        public TestSiteSettings TestSiteSettings { get; }

    }

}
