using System;
using System.Diagnostics;

using isr.Core.EscapeSequencesExtensions;
using isr.Core.SplitExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.MSTest
{

    /// <summary> String extensions tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-14 </para>
    /// </remarks>
    [TestClass()]
    public class StringExtensionsTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " SPLIT EXTENSION TESTS "

        /// <summary> (Unit Test Method) tests base 64 unique identifier. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void SplitWordsTest()
        {
            string input = "CamelCase";
            string expected = "Camel Case";
            string actual = input.SplitWords();
            Assert.AreEqual( expected, actual, $"Split {input}" );
            input = "CamelCASE";
            expected = "Camel CASE";
            actual = input.SplitWords();
            Assert.AreEqual( expected, actual, $"Split {input}" );
            input = "CamelCase123";
            expected = "Camel Case123";
            actual = input.SplitWords();
            Assert.AreEqual( expected, actual, $"Split {input}" );
            input = "CamelIPv4";
            expected = "Camel IPv4";
            actual = input.SplitWords();
            Assert.AreEqual( expected, actual, $"Split {input}" );
        }

        /// <summary> (Unit Test Method) tests base 64 unique identifier. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void SplitTitleCaseTest()
        {
            string input = "CamelCase";
            string expected = "Camel Case";
            string actual = input.SplitCase();
            Assert.AreEqual( expected, actual, $"Split {input}" );
            input = "CamelCase123";
            expected = "Camel Case 123";
            actual = input.SplitCase();
            Assert.AreEqual( expected, actual, $"Split {input}" );
            input = "LocalIPV4";
            expected = "Local IPV 4";
            actual = input.SplitCase();
            Assert.AreEqual( expected, actual, $"Split {input}" );
            input = "LocalREMOTE";
            expected = "Local REMOTE";
            actual = input.SplitCase();
            Assert.AreEqual( expected, actual, $"Split {input}" );
            input = "LocalIPv4";
            expected = "Local I Pv 4";
            actual = input.SplitCase();
            Assert.AreEqual( expected, actual, $"Split {input}" );
            input = "ExpandCamelCaseAPIDescriptorPHP3_5_4Version3_21Beta";
            expected = "Expand Camel Case API Descriptor PHP 3_5_4 Version 3_21 Beta";
            actual = input.SplitCase();
            Assert.AreEqual( expected, actual, $"Split {input}" );
        }

        /// <summary> (Unit Test Method) tests split case. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void SplitCaseTest()
        {
            string input = "CamelCase";
            string expected = "Camel Case";
            string actual = input.SplitCaseSlower();
            Assert.AreEqual( expected, actual, $"Split {input}" );
            input = "CamelCase123";
            expected = "Camel Case 123";
            actual = input.SplitCaseSlower();
            Assert.AreEqual( expected, actual, $"Split {input}" );
            input = "LocalIPV4";
            expected = "Local IPV 4";
            actual = input.SplitCaseSlower();
            Assert.AreEqual( expected, actual, $"Split {input}" );
            input = "LocalREMOTE";
            expected = "Local REMOTE";
            actual = input.SplitCaseSlower();
            Assert.AreEqual( expected, actual, $"Split {input}" );
            input = "LocalIPv4";
            expected = "Local I Pv 4";
            actual = input.SplitCaseSlower();
            Assert.AreEqual( expected, actual, $"Split {input}" );
            input = "ExpandCamelCaseAPIDescriptorPHP3_5_4Version3_21Beta";
            expected = "Expand Camel Case API Descriptor PHP 3_5_4 Version 3_21 Beta";
            actual = input.SplitCaseSlower();
            Assert.AreEqual( expected, actual, $"Split {input}" );
        }

        /// <summary> (Unit Test Method) tests split benchmark. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void SplitBenchmarkTest()
        {
            Stopwatch sw;
            int toleranceFactor = 2;
            int count = 1000;
            switch ( count )
            {
                case var @case when @case > 100000:
                    {
                        toleranceFactor = 2;
                        break;
                    }

                case var case1 when case1 > 10000:
                    {
                        toleranceFactor = 3;
                        break;
                    }

                case var case2 when case2 > 1000:
                    {
                        toleranceFactor = 5;
                        break;
                    }

                default:
                    {
                        toleranceFactor = 40;
                        break;
                    }
            }

            string input = "CamelCase123";
            double expectedMillisecondDuration = 0.00034d;
            var expectedDuration = TimeSpan.FromTicks( ( long ) (toleranceFactor * count * TimeSpan.TicksPerMillisecond * expectedMillisecondDuration) );
            sw = Stopwatch.StartNew();
            string output;
            for ( int i = 1, loopTo = count; i <= loopTo; i++ )
            {
                output = input.SplitWords();
            }

            var actualDuration = sw.Elapsed;
            Assert.IsTrue( actualDuration < expectedDuration, $"{nameof( SplitExtensions.SplitExtensionMethods.SplitWords )} duration {actualDuration.TotalMilliseconds}ms should be lower than {expectedDuration.TotalMilliseconds}ms for {count} iterations" );
            expectedMillisecondDuration = 0.0028d;
            expectedDuration = TimeSpan.FromTicks( ( long ) (toleranceFactor * count * TimeSpan.TicksPerMillisecond * expectedMillisecondDuration) );
            sw = Stopwatch.StartNew();
            for ( int i = 1, loopTo1 = count; i <= loopTo1; i++ )
            {
                output = input.SplitCase();
            }

            actualDuration = sw.Elapsed;
            Assert.IsTrue( actualDuration < expectedDuration, $"{nameof( SplitExtensions.SplitExtensionMethods.SplitCase )} duration {actualDuration.TotalMilliseconds}ms should be lower than {expectedDuration.TotalMilliseconds}ms for {count} iterations" );
            expectedMillisecondDuration = 0.0048d;
            expectedDuration = TimeSpan.FromTicks( ( long ) (toleranceFactor * count * TimeSpan.TicksPerMillisecond * expectedMillisecondDuration) );
            sw = Stopwatch.StartNew();
            for ( int i = 1, loopTo2 = count; i <= loopTo2; i++ )
            {
                output = input.SplitCaseSlower();
            }

            actualDuration = sw.Elapsed;
            Assert.IsTrue( actualDuration < expectedDuration, $"{nameof( SplitExtensions.SplitExtensionMethods.SplitCaseSlower )} duration {actualDuration.TotalMilliseconds}ms should be lower than {expectedDuration.TotalMilliseconds}ms for {count} iterations" );
        }


        #endregion

        #region " ESCAPE SEQUENCE TESTS "

        /// <summary> (Unit Test Method) tests escape sequence. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void EscapeSequenceTest()
        {
            string IdentityQueryCommand = "*IDN?";
            string escapedValue = $"{IdentityQueryCommand}{EscapeSequencesExtensions.EscapeSequencesExtensionMethods.NewLineEscape}";
            string expcetedValue = $"{IdentityQueryCommand}{EscapeSequencesExtensions.EscapeSequencesExtensionMethods.NewLineChar}";
            string actualValue = escapedValue.Replace( isr.Core.EscapeSequencesExtensions.EscapeSequencesExtensionMethods.NewLineEscape,
                                                       Microsoft.VisualBasic.Strings.ChrW( isr.Core.EscapeSequencesExtensions.EscapeSequencesExtensionMethods.NewLineValue ).ToString() );
            Assert.AreEqual( expcetedValue, actualValue, $"Failed replacing escape value in {escapedValue}" );
            actualValue = escapedValue.ReplaceCommonEscapeSequences();
            Assert.AreEqual( expcetedValue, actualValue, $"Extension failed replacing escape value in {escapedValue}" );
            actualValue = actualValue.InsertCommonEscapeSequences();
            Assert.AreEqual( escapedValue, actualValue, $"Extension failed inserting escape value" );
        }

        #endregion

        #region " PARSE "

        /// <summary> (Unit Test Method) extracts the numeric characters test. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void ExtractNumericCharactersTest()
        {
            string compoundValue = ";123.456A";
            string expectedValue = "123456";
            string actualValue = ParseExtensions.ParseExtensionMethods.ExtractNumericCharacters( compoundValue );
            Assert.AreEqual( expectedValue, actualValue, $"Should extract numeric characters from {compoundValue}" );
        }

        /// <summary> (Unit Test Method) extracts the number test. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void ExtractNumberTest()
        {
            var rnd = new Random( DateTime.UtcNow.Millisecond );
            double power;
            double value;
            string compoundValue;
            string expectedValue;
            string actualValue;
            for ( int i = 1; i <= 50; i++ )
            {
                power = rnd.Next() % 10;
                value = (rnd.NextDouble() - 0.5d) * rnd.NextDouble() * Math.Pow( power, 10d );
                compoundValue = $";{value}A";
                expectedValue = value.ToString();
                actualValue = ParseExtensions.ParseExtensionMethods.ExtractExponentialNumber( compoundValue );
                // actualValue = ParseExtensions.Methods.ExtractNumberPart(compoundValue)
                Assert.AreEqual( expectedValue, actualValue, $"test #{i} should extract the number part from {compoundValue}" );
            }
        }

        /// <summary> (Unit Test Method) extracts the exponential number test. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod]
        public void ExtractExponentialNumberTest()
        {
            var rnd = new Random( DateTime.UtcNow.Millisecond );
            double exponenet;
            double value;
            string candidateValue;
            string compoundValue;
            string expectedValue;
            string actualValue;
            for ( int i = 1; i <= 50; i++ )
            {
                exponenet = rnd.Next() % 100 + 1; // - 50
                value = (rnd.NextDouble() - 0.5d) * rnd.NextDouble();
                candidateValue = $"A{value}";
                if ( candidateValue.Contains( "E" ) )
                {
                    compoundValue = candidateValue;
                    expectedValue = $"{value}";
                }
                else
                {
                    compoundValue = $"A{value}E{exponenet}z";
                    expectedValue = $"{value}E{exponenet}";
                }

                actualValue = ParseExtensions.ParseExtensionMethods.ExtractExponentialNumber( compoundValue );
                Assert.AreEqual( expectedValue, actualValue, $"test #{i} should extract the number part from {compoundValue}" );
            }
        }


        #endregion

        #region " TRIM "

        [TestMethod]
        public void CleanTest()
        {
            string dirtyValue = "this  is   the    dirty    string";
            string cleanedValue = "this is the dirty string";
            string actualValue = TrimExtensions.TrimExtensionMethods.Clean( dirtyValue );
            Assert.AreEqual( cleanedValue, actualValue, $"Should replace multiple spaces in '{dirtyValue}' with a single space" );
        }

        #endregion


    }
}
