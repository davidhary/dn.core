using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using isr.Core.AssemblyExtensions;

namespace isr.Core.Framework.MSTest
{
    /// <summary>
    /// This is a test class for <see cref="isr.Core.Framework.AssemblyInfo"/> and <see cref="isr.Core.AssemblyExtensions"/> and is intended
    /// to contain all MyAssemblyInfoTest Unit Tests
    /// </summary>
    [TestClass()]
    public class AssemblyInfoTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " ASSEMBLY INFO TESTS "

        /// <summary>   (Unit Test Method) public key token should match. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        [TestMethod()]
        public void PublicKeyTokenShouldMatch()
        {
            var target = typeof( AssemblyInfoTests ).Assembly;
            string expected = target.GetName().ToString().Split( ',' )[3];
            expected = expected.Substring(expected.IndexOf( "=", StringComparison.OrdinalIgnoreCase ) + 1);
            string actual = target.GetPublicKeyToken();
            Assert.AreEqual( expected, actual, true, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>   The build number of the assembly. </summary>
        /// <remarks>   David, 2021-08-21. </remarks>
        /// <param name="fileName"> Filename of the file. </param>
        /// <returns>   The build number for today. </returns>
        public static int BuildNumber( string fileName )
        {
            System.IO.FileInfo fi = new( fileName );
            return ( int ) Math.Floor( fi.LastWriteTime.ToUniversalTime().Subtract( DateTime.Parse( "2000-01-01" ) ).TotalDays );
        }

        /// <summary>   (Unit Test Method) file version build number should match. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        [TestMethod()]
        public void FileVersionBuildNumberShouldMatch()
        {
            string fileName = @".\version.build.props";
            int expected = AssemblyInfoTests.BuildNumber( fileName );
            System.Reflection.Assembly assembly = (typeof( AssemblyInfoTests ).Assembly);
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo( assembly.Location );
            int actual = fvi.FileBuildPart;
            Assert.AreEqual( expected, actual );
        }

        /// <summary>   (Unit Test Method) assembly version should match. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        [TestMethod()]
        public void AssemblyVersionShouldMatch()
        {
            string fileName = @".\version.build.props";
            int expected = AssemblyInfoTests.BuildNumber( fileName );
            System.Reflection.Assembly assembly = (typeof( AssemblyInfoTests ).Assembly);
            var target = new AssemblyInfo( assembly );
            int actual = target.AssemblyVersion.Build;
            Assert.AreEqual( expected, actual );
        }

        #endregion

    }
}
