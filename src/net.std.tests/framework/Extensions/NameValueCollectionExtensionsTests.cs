using System;
using System.Collections.Specialized;

using isr.Core.CollectionExtensions;

using Microsoft.VisualBasic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.MSTest
{

    /// <summary> A name value collection extensions tests. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass()]
    public class NameValueCollectionExtensionsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name}TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        #endregion

        /// <summary> The collection. </summary>
        private static readonly NameValueCollection Collection = new() { { "ValidEntry", "ValidEntry" } };

        /// <summary> (Unit Test Method) does not throw exception when key does not exist. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void DoesNotThrowExceptionWhenKeyDoesNotExist()
        {
            Assert.AreEqual( Collection.GetValueAs( "InvalidEntry", "DefaultValue" ), "DefaultValue" );
        }

        /// <summary>
        /// (Unit Test Method) queries if a given does not throw exception when key exists.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void DoesNotThrowExceptionWhenKeyExists()
        {
            Assert.AreEqual( Collection.GetValueAs<string>( "ValidEntry" ), "ValidEntry" );
        }

        /// <summary> (Unit Test Method) throws exception when key is null or white space. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ThrowsExceptionWhenKeyIsNullOrWhiteSpace()
        {
            try
            {
                _ = Collection.GetValueAs<string>( null );
            }
            catch ( ArgumentException )
            {
            }
            catch
            {
                Assert.Fail( "expected exception was not thrown" );
            }

            try
            {
                _ = Collection.GetValueAs<string>( null );
            }
            catch ( ArgumentException )
            {
            }
            catch
            {
                Assert.Fail( "expected exception was not thrown" );
            }

            _ = Asserts.Instance.Throws<ArgumentException>( () => Collection.GetValueAs<string>( " " ) );
            _ = Asserts.Instance.Throws<ArgumentException>( () => Collection.GetValueAs<string>( Constants.vbTab ) );
            _ = Asserts.Instance.Throws<ArgumentException>( () => Collection.GetValueAs( null, string.Empty ) );
            _ = Asserts.Instance.Throws<ArgumentException>( () => Collection.GetValueAs( " ", string.Empty ) );
            _ = Asserts.Instance.Throws<ArgumentException>( () => Collection.GetValueAs( Constants.vbTab, string.Empty ) );
        }
    }
}
