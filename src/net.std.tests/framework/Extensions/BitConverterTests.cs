using System;

using isr.Core.BitConverterExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.MSTest
{

    /// <summary> A bit converter tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-08 </para>
    /// </remarks>
    [TestClass()]
    public class BitConverterTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }


        #endregion

        #region " BIT CONVERTER TESTS "

        /// <summary> (Unit Test Method) tests true if little endian. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void TrueIfLittleEndianTest()
        {
            bool isLittleEndian = true;
            bool expected = isLittleEndian;
            bool actual = BitConverter.IsLittleEndian;
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests byte converter short. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ByteConverterShortTest()
        {
            ushort value = 1234;
            var values = value.BigEndInt8();
            ushort expected = values.BigEndUnsignedShort( 0 );
            ushort actual = values.BigEndUnsignedInt16( 0 );
            Assert.AreEqual( value, expected );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests bit converter short. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void BitConverterShortTest()
        {
            ushort value = 1234;
            var values = value.BigEndBytes();
            ushort actual = values.BigEndUnsignedInt16( 0 );
            ushort expected = values.BigEndUnsignedShort( 0 );
            Assert.AreEqual( value, expected );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests bit converter single. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void BitConverterSingleTest()
        {
            float expected = 1234.567f;
            var values = expected.BigEndBytes();
            float actual = values.BigEndSingle( 0 );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests bit converter endianess. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void BitConverterEndianessTest()
        {
            ushort value = 1234;
            var bigEndBytes = value.BigEndBytes();
            var bigEndInt8 = value.BigEndInt8();
            byte actual = bigEndBytes[0];
            byte expected = bigEndInt8[0];
            Assert.AreEqual( expected, actual );
            actual = bigEndBytes[1];
            expected = bigEndInt8[1];
            Assert.AreEqual( expected, actual );
            var littleBytes = BitConverter.GetBytes( value );
            actual = littleBytes[0];
            expected = bigEndBytes[1];
            Assert.AreEqual( expected, actual );
            actual = littleBytes[1];
            expected = bigEndBytes[0];
            Assert.AreEqual( expected, actual );
        }
        #endregion


    }
}
