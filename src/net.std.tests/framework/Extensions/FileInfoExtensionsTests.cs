using System;

using isr.Core.FileInfoExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.MSTest
{

    /// <summary> File information extensions tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-14 </para>
    /// </remarks>
    [TestClass()]
    public class FileInfoExtensionsTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " FILE INFO EXTENSIONS TESTS "

        /// <summary> A test for MoveToFolder. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void MoveToFolderTest()
        {
            string applicationFolderfolder = AppContext.BaseDirectory;
            string fromFolder = System.IO.Path.Combine( applicationFolderfolder, "Assist" );
            string toFolder = System.IO.Path.Combine( applicationFolderfolder, @"..\_log" );
            _ = System.IO.Directory.CreateDirectory( toFolder );
            string fileName = "MoveToFolderTestFile.txt";
            var value = new System.IO.FileInfo( System.IO.Path.Combine( fromFolder, fileName ) );
            Assert.IsTrue( value.Exists, $"{fileName} not found in {fromFolder}" );
            string filePath = System.IO.Path.Combine( toFolder, fileName );
            _ = value.CopyTo( filePath, true );
            value = new System.IO.FileInfo( filePath );
            Assert.IsTrue( value.Exists, $"{fileName} not found in {toFolder}" );
            value.MoveToFolder( fromFolder, true );
            value = new System.IO.FileInfo( System.IO.Path.Combine( fromFolder, fileName ) );
            Assert.IsTrue( value.Exists, $"{fileName} not found in {toFolder}" );
        }

        #endregion

    }
}
