using System;
using System.Diagnostics;

using isr.Core.StackTraceExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.MSTest
{

    /// <summary>
    /// This is a test class for ExtensionsTest and is intended to contain all ExtensionsTest Unit
    /// Tests.
    /// </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class CallStackExtensionsTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " CALL STACK EXTENSION TESTS "

        /// <summary> A test for ParseCallStack for user. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ParseUserCallStackFrameTest()
        {
            var trace = new StackTrace( true );
            var frame = new StackFrame( true );
            var callStackType = CallStackType.UserCallStack;
            string expected = "Tests.ParseUserCallStackFrameTest() in ";
            string actual = trace.ParseCallStack( frame, callStackType ).Trim();
            Assert.IsTrue( actual.Contains( expected ), $@"Actual trace 
{actual}
contains 
{expected}" );
        }

        /// <summary> A test for ParseCallStack for user. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ParseFullCallStackFrameTest()
        {
            var trace = new StackTrace( true );
            var frame = new StackFrame( true );
            var callStackType = CallStackType.FullCallStack;
            string expected = string.Empty;
            string actual;
            actual = trace.ParseCallStack( frame, callStackType );
            Assert.AreNotEqual( expected, actual );
        }

        /// <summary> A test for ParseCallStack. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ParseUserCallStackTest()
        {
            var trace = new StackTrace( true );
            int skipLines = 0;
            int totalLines = 0;
            var callStackType = CallStackType.UserCallStack;
            string expected = "s->   at isr.";
            string actual = trace.ParseCallStack( skipLines, totalLines, callStackType ).Trim();
            Assert.IsTrue( actual.StartsWith( expected, StringComparison.OrdinalIgnoreCase ), $"Trace results Actual: {actual.Substring( 0, expected.Length )} Starts with {expected}" );
        }

        /// <summary> A test for ParseCallStack. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ParseFullCallStackTest()
        {
            var trace = new StackTrace( true );
            int skipLines = 0;
            int totalLines = 0;
            var callStackType = CallStackType.FullCallStack;
            string expected = string.Empty;
            string actual;
            actual = trace.ParseCallStack( skipLines, totalLines, callStackType );
            Assert.AreNotEqual( expected, actual );
        }
        #endregion

    }
}
