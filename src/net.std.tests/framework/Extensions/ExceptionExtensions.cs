using System;

namespace isr.Core.Framework.MSTest.ExceptionExtensions
{

    /// <summary>
    /// Exception methods for adding exception data and building a detailed exception message.
    /// </summary>
    /// <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class ExceptionExtensionMethods
    {

        /// <summary> Adds an exception data. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool AddExceptionDataThis( Exception exception )
        {
            return isr.Core.ExceptionExtensions.ExceptionDataMethods.AddExceptionData( exception );
        }

    }
}
