using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.MSTest
{

    /// <summary>
    /// This is a test class for EngineeringFormatProviderTest and is intended to contain all
    /// EngineeringFormatProviderTest Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestClass()]
    public class EngineeringFormatProviderTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " ENGINEERING FORMAT PROVIDER TESTS "

        /// <summary> A test for Formatting. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="value">                   The value. </param>
        /// <param name="format">                  Describes the format to use. </param>
        /// <param name="usingThousandsSeparator"> true to using thousands separator. </param>
        /// <param name="expectedValue">           The expected value. </param>
        public static void CustomFormatPiTest( double value, string format, bool usingThousandsSeparator, string expectedValue )
        {
            var target = new EngineeringFormatProvider() { UsingThousandsSeparator = usingThousandsSeparator };
            string actual = string.Format( target, format, value );
            Assert.AreEqual( expectedValue, actual );
        }

        /// <summary> A test for Formatting. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <param name="value">         The value. </param>
        /// <param name="format">        Describes the format to use. </param>
        /// <param name="expectedValue"> The expected value. </param>
        public static void CustomFormatPiTest( double value, string format, string expectedValue )
        {
            IFormatProvider target = new EngineeringFormatProvider();
            string actual = string.Format( target, format, value );
            Assert.AreEqual( expectedValue, actual );
        }

        /// <summary> A test for Formatting PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatPiTest()
        {
            CustomFormatPiTest( Math.PI, "{0}", "3.142" );
        }

        /// <summary> A test for Formatting 0.1 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatDeciPiTest()
        {
            CustomFormatPiTest( 0.1d * Math.PI, "{0}", "0.3142" );
        }

        /// <summary> A test for Formatting 0.01 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatCentiPiTest()
        {
            CustomFormatPiTest( 0.01d * Math.PI, "{0}", "31.42e-03" );
        }

        /// <summary> A test for Formatting 0.001 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatMilliPiTest()
        {
            CustomFormatPiTest( 0.001d * Math.PI, "{0}", "3.142e-03" );
        }

        /// <summary> A test for Formatting 0.000001 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatMicroPiTest()
        {
            CustomFormatPiTest( 0.000001d * Math.PI, "{0}", "3.142e-06" );
        }

        /// <summary> A test for Formatting 1000 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatKiloPiTest()
        {
            CustomFormatPiTest( 1000d * Math.PI, "{0}", "3.142e+03" );
        }

        /// <summary> A test for Formatting 1000000 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatMegaPiTest()
        {
            CustomFormatPiTest( 1000000d * Math.PI, "{0}", "3.142e+06" );
        }

        /// <summary> A test for Formatting 1000 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatKiloWithSeparatorPiTest()
        {
            CustomFormatPiTest( 1000d * Math.PI, "{0}", true, "3,142" );
        }

        /// <summary> A test for Formatting 10000 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatDekaWithSeparatorPiTest()
        {
            CustomFormatPiTest( 10000d * Math.PI, "{0}", true, "31,416" );
        }

        /// <summary> A test for Formatting 100000 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatHectoWithSeparatorPiTest()
        {
            CustomFormatPiTest( 100000d * Math.PI, "{0}", true, "314,159" );
        }

        /// <summary> A test for Formatting 1000000 * PI. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void CustomFormatMegaWithSeparatorPiTest()
        {
            CustomFormatPiTest( 1000000d * Math.PI, "{0}", true, "3.142e+06" );
        }
        #endregion

    }
}
