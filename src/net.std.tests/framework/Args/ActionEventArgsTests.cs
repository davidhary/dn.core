using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.MSTest
{

    /// <summary> Action Event Arguments tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-08 </para>
    /// </remarks>
    [TestClass()]
    public class ActionEventArgsTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name}TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        #endregion

        #region " ACTION EVENT ARGMENTS TESTS "

        /// <summary> (Unit Test Method) tests action event. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ActionEventTest()
        {
            var args = new ActionEventArgs( TraceEventType.Error );
            string message = "Information";
            var eventType = TraceEventType.Information;
            args.RegisterOutcomeEvent( eventType, message );
            Assert.IsFalse( args.Failed, $"Action {args.Details} at {eventType} failed" );
            Assert.AreEqual( eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected" );
            Assert.IsTrue( args.HasDetails, $"Action {args.Details} at {eventType} reported not having details" );
            eventType = TraceEventType.Error;
            message = "Error";
            args.RegisterOutcomeEvent( eventType, message );
            Assert.IsTrue( args.Failed, $"Action {args.Details} at {eventType} did not failed" );
            Assert.AreEqual( eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected" );
            eventType = TraceEventType.Critical;
            message = "Critical";
            args.RegisterOutcomeEvent( eventType, message );
            Assert.IsTrue( args.Failed, $"Action {args.Details} at {eventType} did not failed" );
            Assert.AreEqual( eventType, args.OutcomeEvent, $"Action {args.Details} event unexpected" );
        }

        /// <summary> (Unit Test Method) tests action event cancellation. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ActionEventCancellationTest()
        {
            var args = new ActionEventArgs( TraceEventType.Error );
            string message = "Cancel";
            args.RegisterFailure( message );
            Assert.IsTrue( args.Failed, $"Action {args.Details} failed to cancel" );
        }

        #endregion

    }
}
