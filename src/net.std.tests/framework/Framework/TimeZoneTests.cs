using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.MSTest
{

    /// <summary> A time zone tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-14 </para>
    /// </remarks>
    [TestClass()]
    public class TimeZoneTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " TIME ZONE TESTS "

        /// <summary> Query if 'timeZone' is daylight saving time. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="timeZone">  The time zone. </param>
        /// <param name="dateValue"> The date value. </param>
        /// <returns> <c>true</c> if daylight saving time; otherwise <c>false</c> </returns>
        public static bool IsDaylightSavingTime( string timeZone, string dateValue )
        {
            return TimeZoneInfo.FindSystemTimeZoneById( timeZone ).IsDaylightSavingTime( DateTimeOffset.Parse( dateValue ) );
        }

        /// <summary>
        /// (Unit Test Method) Arizona does not have a daylight saving time when California does.
        /// </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ArizonaDoesNotHaveDaylightSavingTimeWhenCaliforniaDoes()
        {
            string dayString = "2017-06-01";
            string timeZone = "US Mountain Standard Time";
            bool expected = false;
            bool actual = IsDaylightSavingTime( timeZone, dayString );
            Assert.AreEqual( expected, actual );
        }

        /// <summary>
        /// (Unit Test Method) Arizona does not have a daylight saving time when California does not.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ArizonaHasNoDaylightSavingTimeWhenCaliforniaDoesNot()
        {
            string dayString = "2017-01-01";
            string timeZone = "US Mountain Standard Time";
            bool expected = false;
            bool actual = IsDaylightSavingTime( timeZone, dayString );
            Assert.AreEqual( expected, actual );
        }

        /// <summary>   (Unit Test Method) date is pacific time zone daylight saving date. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        [TestMethod()]
        public void DateIsPacificTimeZoneDaylightSavingDate()
        {
            string dayString = "2017-06-01";
            string timeZone = "Pacific Standard Time";
            bool expected = true;
            bool actual = IsDaylightSavingTime( timeZone, dayString );
            Assert.AreEqual( expected, actual );
        }

        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void DateIsNotPacificTimeZoneDaylightSavingDate()
        {
            string dayString = "2017-01-01";
            string timeZone = "Pacific Standard Time";
            bool expected = false;
            bool actual = IsDaylightSavingTime( timeZone, dayString );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests get Arizona time zone. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ArizonaTimeZoneIdIsExpected()
        {
            string expected = "US Mountain Standard Time";
            string actual = TimeZoneInfo.FindSystemTimeZoneById( expected ).Id;
            Assert.AreEqual( expected, actual );
        }

        /// <summary>   (Unit Test Method) settings is local pacific standard time. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        [TestMethod()]
        public void SettingsIsLocalPacificStandardTime()
        {
            var tz = TimeZoneInfo.Local;
            string expected = AppSettings.Instance.TestSiteSettings.TimeZone();
            string actual = tz.Id;
            Assert.AreEqual( expected, actual );
        }

        /// <summary> Query if 'timeZone' is daylight saving time. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="dateValue"> The date value. </param>
        /// <returns> <c>true</c> if daylight saving time; otherwise <c>false</c> </returns>
        public static bool IsDaylightSavingTime( string dateValue )
        {
            var tz = TimeZoneInfo.Local;
            return tz.IsDaylightSavingTime( DateTimeOffset.Parse( dateValue ) );
        }

        /// <summary> Query if 'timeZone' is daylight saving time. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <returns> <c>true</c> if daylight saving time; otherwise <c>false</c> </returns>
        public static bool IsDaylightSavingTime()
        {
            var tz = TimeZoneInfo.Local;
            return tz.IsDaylightSavingTime( DateTimeOffset.Now );
        }

        /// <summary>   (Unit Test Method) date is not daylight savings. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        [TestMethod()]
        public void DateIsNotDaylightSavings()
        {
            bool expectedBoolean = false;
            bool actualBoolean = IsDaylightSavingTime( "2017-01-01" );
            Assert.AreEqual( expectedBoolean, actualBoolean );
        }

        /// <summary>   (Unit Test Method) date is daylight savings. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        [TestMethod()]
        public void DateIsDaylightSavings()
        {
            bool expectedBoolean = true;
            bool actualBoolean = IsDaylightSavingTime( "2017-06-01" );
            Assert.AreEqual( expectedBoolean, actualBoolean );
        }

        /// <summary>   (Unit Test Method) UTC time offset should equal settings. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        [TestMethod()]
        public void UtcTimeOffsetShouldEqualSettings()
        {
            var expected = TimeSpan.FromHours( IsDaylightSavingTime()
                            ? AppSettings.Instance.TestSiteSettings.TimeZoneOffset() + 1d
                            : AppSettings.Instance.TestSiteSettings.TimeZoneOffset() );
            var actual = DateTimeOffset.Now.Offset;
            Assert.AreEqual( expected, actual );
        }

        /// <summary>   (Unit Test Method) UTC time can be calculated from date time offset. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        [TestMethod()]
        public void UtcTimeCanBeCalculatedFromDateTimeOffset()
        {
            var timeNow = DateTime.Now;
            var expected = timeNow.ToUniversalTime();
            var actual = timeNow.Subtract( DateTimeOffset.Now.Offset );
            Assert.AreEqual( expected, actual );
        }

        /// <summary>   (Unit Test Method) UTC time can be calculated. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        [TestMethod()]
        public void UtcTimeCanBeCalculated()
        {
            var timeNow = DateTime.Now;
            var expected = timeNow.ToUniversalTime();
            var actual = timeNow.Subtract( DateTimeOffset.Parse( timeNow.Date.ToShortDateString() ).Offset );
            Assert.AreEqual( expected, actual );
        }

        #endregion

    }
}
