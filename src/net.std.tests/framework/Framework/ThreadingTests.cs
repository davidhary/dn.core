using System;
using System.Threading;

using isr.Core.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Framework.MSTest
{

    /// <summary>   tests of threading functions. </summary>
    /// <remarks>   David, 2021-02-12. </remarks>
    [TestClass()]
    public class ThreadingTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( AppSettings.Instance.TestSiteSettings.Exists(), $"{nameof( AppSettings.Instance.TestSiteSettings )} settings file {AppSettings.Instance.AppSettingsFullFileName} should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( AppSettings.Instance.TestSiteSettings.TimeZoneOffset() ) < expectedUpperLimit,
                           $"{nameof( AppSettings.Instance.TestSiteSettings.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " WAIT FOR TESTS  "

        private static int _PollInterval = 100;
        private static int _ProcessDuration = 200;

        private static int ProcessMethod( CancellationToken cancellationToken )
        {
            _PollInterval = _ProcessDuration / 10;
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            while ( sw.ElapsedMilliseconds < _ProcessDuration )
            {
                Thread.Sleep( _PollInterval );
                // co-operative cancellation implies periodically check IsCancellationRequested 
                cancellationToken.ThrowIfCancellationRequested();
            }
            return 123; // the result 
        }

        /// <summary>   (Unit Test Method) process should timeout. </summary>
        /// <remarks>   David, 2021-10-27. </remarks>
        [TestMethod()]
        public void ProcessShouldTimeout()
        {
            _ProcessDuration = 100;
            int expectedResult = 0;
            bool expectedOutcome = false;
            int msTimeout = _ProcessDuration / 2;
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            bool isExcecuted = WaitFor.TryCallWithTimeout(
               ProcessMethod,
               TimeSpan.FromMilliseconds( msTimeout ), // Timeout < Process Duration => ProcessMethod() gets Canceled
                                                       // Timeout > Process Duration => ProcessMethod() gets Executed
               out int result );
            Assert.AreEqual( expectedOutcome, isExcecuted  ,$"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_PollInterval}ms should time out (be canceled)" );
            Assert.AreEqual( expectedResult, result, $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_PollInterval}ms should time out (be canceled) with result of {expectedResult}" );
        }

        /// <summary>   (Unit Test Method) process should execute. </summary>
        /// <remarks>   David, 2021-10-27. </remarks>
        [TestMethod()]
        public void ProcessShouldExecute()
        {
            _ProcessDuration = 100;
            int expectedResult = 123;
            bool expectedOutcome = true;
            int msTimeout = _ProcessDuration * 2;
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            bool isExcecuted = WaitFor.TryCallWithTimeout(
               ProcessMethod,
               TimeSpan.FromMilliseconds( msTimeout ), // Timeout < Process Duration => ProcessMethod() gets Canceled
                                                       // Timeout > Process Duration => ProcessMethod() gets Executed
               out int result );
            Assert.AreEqual( expectedOutcome, isExcecuted, $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_PollInterval}ms should execute (not time out)" );
            Assert.AreEqual( expectedResult, result, $"{nameof( ProcessMethod )}() with timeout of {msTimeout}ms and cancellation polling of {_PollInterval}ms should execute (not time out) with results of {expectedResult}" );
        }

        [TestMethod()]
        public void WaitOneTests()
        {
            var delayTime = TimeSpan.FromMilliseconds( 1 );
            var actualTime = WaitFor.One( delayTime );
            Console.Out.Write( $"{delayTime.TotalMilliseconds}ms delayed for {actualTime.TotalMilliseconds}ms" );
        }

        #endregion

    }
}
