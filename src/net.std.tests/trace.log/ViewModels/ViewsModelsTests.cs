using System;

using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.TraceLog.MSTest
{

    /// <summary> Event handling tests. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    [TestClass()]
    public class ViewModelTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
                // this ensures having the correct log settings file name for the test project, because the test executive may return an empty 
                // Entry Assembly if it is not managed. This problem has been intermittent.
                TraceLog.TraceLogger.LogSettingsFileName = isr.Core.Logging.ILogger.LoggingPlatform.BuildAppSettingsFileName( System.Reflection.Assembly.GetExecutingAssembly() );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }


        #endregion

        #region " VIEW MODEL TESTS "

        private class ViewModel : ViewModelLoggerBase
        {
            protected override bool AddExceptionData( Exception ex )
            {
                return isr.Core.TraceLog.ExceptionExtensions.ExceptionDataMethods.AddExceptionData( ex );
            }

            /// <summary>
            /// Executes the exception and publish on a different thread, and waits for the result.
            /// </summary>
            /// <remarks>   David, 2021-06-30. </remarks>
            /// <returns>   A Tuple. </returns>
            public (bool Success, string Details) InvokeExceptionAndPublish()
            {
                try
                {
                    isr.Core.TraceLog.TraceLogger.Instance.Logger.LogInformation( "Before getting log file name" );
                    Console.WriteLine( $"Log file name: {isr.Core.TraceLog.TraceLogger.FullLogFileName}" );
                    throw new DivideByZeroException();
                }
                catch ( DivideByZeroException ex )
                {
                    try
                    {
                        _ = this.LogError( ex, string.Empty );
                        return (true, string.Empty);
                    }
                    catch ( Exception ex2 )
                    {
                        return (false, $"Exception occurred publishing exception {ex}; details: {ex2}");
                    }
                }
            }
        }

        /// <summary>   (Unit Test Method) view model should be constructed. </summary>
        /// <remarks>   David, 2020-10-10. </remarks>
        [TestMethod()]
        public void ViewModelShouldBeConstructed()
        {
            ViewModel vm = new();
            (bool success, string details) = vm.InvokeExceptionAndPublish();
            Assert.IsTrue( success, details );
            if ( TraceListener.Queue.IsEmpty )
            {

            }
            else
            {
                TraceListener.Queue.Clear();
            }

        }

        #endregion

    }
}
