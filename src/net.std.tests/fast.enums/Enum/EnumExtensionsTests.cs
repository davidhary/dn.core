using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Xunit;
using FluentAssertions;

namespace FastEnums.Tests.Cases
{

    /// <summary>
    /// This is a test class for EnumExtensionsTest and is intended to contain all EnumExtensionsTest
    /// Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    public class EnumExtensionsTests
    {

        #region " ENUM EXTENSION TEST: VALUES "

        /// <summary> (Unit Test Method) tests enum values include. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [Fact]
        public void EnumValuesIncludeTest()
        {
            var allValues = typeof( TraceEventType ).EnumValues();
            Assert.True( allValues.Any(), $"{typeof( TraceEventType )}.EnumValues(of System.Enum) has values" );
            _ = allValues.Count().Should().Be( Enum.GetValues( typeof( TraceEventType ) ).Length, $"{typeof( TraceEventType )} count matches the extension result" );
            Assert.True( allValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the enum values of {typeof( TraceEventType )}" );

            var allLongs = typeof( TraceEventType ).Values();
            Assert.True( allLongs.Any(), $"{typeof( TraceEventType )}.EnumValues(of Long) has values" );
            _ = allValues.Count().Should().Be( Enum.GetValues( typeof( TraceEventType ) ).Length, $"{typeof( TraceEventType )}.EnumValues(of Long) count matches the extension result" );

            long includeMask = ( long ) (TraceEventType.Critical | TraceEventType.Error);
            var filteredValues = allValues.IncludeFilter( includeMask );
            int expectedCount = 2;
            Assert.True( filteredValues.Any(), $"{typeof( TraceEventType )} filtered Values has values" );
            _ = filteredValues.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered values has expected count" );
            Assert.True( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the filtered values of {typeof( TraceEventType )}" );
            Assert.False( filteredValues.Contains( TraceEventType.Information ), $"{TraceEventType.Information} is not included in the filtered values of {typeof( TraceEventType )}" );

            var filteredLongs = allLongs.IncludeFilter( includeMask );
            Assert.True( filteredLongs.Any(), $"{typeof( TraceEventType )} filtered Longs has Longs" );
            _ = filteredLongs.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered Longs has expected count" );
            Assert.True( filteredLongs.Contains( ( long ) TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the filtered Longs of {typeof( TraceEventType )}" );
            Assert.False( filteredLongs.Contains( ( long ) TraceEventType.Information ), $"{TraceEventType.Information} is not included in the filtered Longs of {typeof( TraceEventType )}" );

            IList<Enum> filter = new List<Enum> { TraceEventType.Critical, TraceEventType.Error };
            filteredValues = allValues.IncludeFilter( filter );
            // filteredValues = allValues.IncludeFilter(new Enum[] { TraceEventType.Critical, TraceEventType.Error });
            expectedCount = 2;
            Assert.True( filteredValues.Any(), $"{typeof( TraceEventType )} filtered Values has list of values" );
            _ = filteredValues.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered lists of values has expected count" );
            Assert.True( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the filtered list of values of {typeof( TraceEventType )}" );
            Assert.False( filteredValues.Contains( TraceEventType.Information ), $"{TraceEventType.Information} is not included in the filtered list of values of {typeof( TraceEventType )}" );
        }

        /// <summary> (Unit Test Method) tests enum values exclude. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [Fact]
        public void EnumValuesExcludeTest()
        {
            var allValues = typeof( TraceEventType ).EnumValues();
            Assert.True( allValues.Any(), $"{typeof( TraceEventType )}.EnumValues(of System.Enum) has values" );
            _ = allValues.Count().Should().Be( Enum.GetValues( typeof( TraceEventType ) ).Length, $"{typeof( TraceEventType )} count matches the extension result" );
            Assert.True( allValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the enum values of {typeof( TraceEventType )}" );

            var allLongs = typeof( TraceEventType ).Values();
            Assert.True( allLongs.Any(), $"{typeof( TraceEventType )}.EnumValues(of Long) has values" );
            _ = allValues.Count().Should().Be( Enum.GetValues( typeof( TraceEventType ) ).Length, $"{typeof( TraceEventType )}.EnumValues(of Long) count matches the extension result" );
            long excludeMask = ( long ) (TraceEventType.Critical | TraceEventType.Error);
            var filteredValues = allValues.ExcludeFilter( excludeMask );
            int expectedCount = allValues.Count() - 2;
            Assert.True( filteredValues.Any(), $"{typeof( TraceEventType )} filtered Values has values" );
            _ = filteredValues.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered values has expected count" );
            Assert.False( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the filtered values of {typeof( TraceEventType )}" );
            Assert.True( filteredValues.Contains( TraceEventType.Information ), $"{TraceEventType.Information} is not Excluded in the filtered values of {typeof( TraceEventType )}" );

            var filteredLongs = allLongs.ExcludeFilter( excludeMask );
            Assert.True( filteredLongs.Any(), $"{typeof( TraceEventType )} filtered Longs has Longs" );
            _ = filteredLongs.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered Longs has expected count" );
            Assert.False( filteredLongs.Contains( ( long ) TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the filtered Longs of {typeof( TraceEventType )}" );
            Assert.True( filteredLongs.Contains( ( long ) TraceEventType.Information ), $"{TraceEventType.Information} is not Excluded in the filtered Longs of {typeof( TraceEventType )}" );

            IList<Enum> filter = new List<Enum> { TraceEventType.Critical, TraceEventType.Error };
            filteredValues = allValues.ExcludeFilter( filter );
            // filteredValues = allValues.ExcludeFilter( allValues );
            // filteredValues = allValues.ExcludeFilter( (new TraceEventType[] { TraceEventType.Critical, TraceEventType.Error } );
            expectedCount = allValues.Count() - 2;
            Assert.True( filteredValues.Any(), $"{typeof( TraceEventType )} filtered Values has list of values" );
            _ = filteredValues.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered lists of values has expected count" );
            Assert.False( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the filtered list of values of {typeof( TraceEventType )}" );
            Assert.True( filteredValues.Contains( TraceEventType.Information ), $"{TraceEventType.Information} is not Excluded in the filtered list of values of {typeof( TraceEventType )}" );

            allValues = TraceEventType.Critical.EnumValues();
            Assert.True( allValues.Any(), $"{typeof( TraceEventType )}.EnumValues(of System.Enum) has values" );
            _ = allValues.Count().Should().Be( Enum.GetValues( typeof( TraceEventType ) ).Length, $"{typeof( TraceEventType )} count matches the extension result" );
            Assert.True( allValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is Excluded in the enum values of {typeof( TraceEventType )}" );

            var inclusionMask = TraceEventType.Critical | TraceEventType.Error | TraceEventType.Information | TraceEventType.Warning;
            var exclusionMask = TraceEventType.Verbose;
            expectedCount = 4;
            filteredValues = TraceEventType.Critical.EnumValues().Filter( inclusionMask, exclusionMask );
            Assert.True( filteredValues.Any(), $"{typeof( TraceEventType )} Filtered Enum Values has values" );
            _ = filteredValues.Count().Should().Be( expectedCount, $"{typeof( TraceEventType )} filtered count matches the expected count" );
            Assert.True( filteredValues.Contains( TraceEventType.Critical ), $"{TraceEventType.Critical} is included in the filtered" );
            Assert.False( filteredValues.Contains( TraceEventType.Verbose ), $"{TraceEventType.Verbose} is Excluded in the filtered" );
        }

        private enum TraceEventTypeWithDescription
        {
            [System.ComponentModel.Description( "Critical Trace Event Type" )] Critical,
            [System.ComponentModel.Description( "Error Trace Event Type" )] Error,
            [System.ComponentModel.Description( "Information Trace Event Type" )] Information,
            [System.ComponentModel.Description( "Verbose Trace Event Type" )] Verbose
        }

        /// <summary> (Unit Test Method) tests value descriptions. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [Fact]
        public void ValueDescriptionsTest()
        {
            var pairs = typeof( TraceEventTypeWithDescription ).ValueDescriptionPairs();
            var expectedValue = TraceEventTypeWithDescription.Critical;
            Assert.True( pairs.Any(), $"{typeof( TraceEventTypeWithDescription )}.{nameof( FastEnums.EnumExtensions.ValueDescriptionPairs )} has values" );
            Assert.True( pairs.ContainsKey( ( Enum ) expectedValue ), $"{typeof( TraceEventTypeWithDescription )} contains {expectedValue}" );

            var pair = pairs.SelectPair( expectedValue, expectedValue.Description() );
            // no longer working in Fluent Assertions 6.0: _ = expectedValue.Should().Be( pair.Key, $"{typeof( TraceEventTypeWithDescription )} found {expectedValue}" );
            _ = expectedValue.Should().Be( ( TraceEventTypeWithDescription ) pair.Key, $"{typeof( TraceEventTypeWithDescription )} found {expectedValue}" );

            var defaultValue = TraceEventTypeWithDescription.Error;
            string description = "incorrect Description";
            pair = pairs.SelectPair( defaultValue, "incorrect Description" );
            _ = pair.Key.Should().Be( defaultValue, $"{typeof( TraceEventTypeWithDescription )} found {defaultValue} due to {description}" );

            pairs = typeof( TraceEventTypeWithDescription ).ValueNamePairs();
            expectedValue = TraceEventTypeWithDescription.Critical;
            Assert.True( pairs.Any(), $"{typeof( TraceEventTypeWithDescription )}.{nameof( FastEnums.EnumExtensions.ValueDescriptionPairs )} has value names" );
            Assert.True( pairs.ContainsKey( ( Enum ) expectedValue ), $"{typeof( TraceEventTypeWithDescription )} value names contains {expectedValue}" );

            string expectedDescription = TraceEventTypeWithDescription.Critical.Description();
            var descriptions = TraceEventTypeWithDescription.Critical.Descriptions();
            Assert.True( descriptions.Any(), $"{typeof( TraceEventTypeWithDescription )}.{nameof( descriptions )} has description" );
            Assert.True( descriptions.Contains( expectedDescription ), $"{typeof( TraceEventTypeWithDescription )} value descriptions contains {expectedDescription}" );

            var names = pairs.ToValues();
            Assert.True( names.Any(), $"{typeof( TraceEventTypeWithDescription )}.{nameof( names )} has name" );

            string expectedname = TraceEventTypeWithDescription.Critical.ToString();
            Assert.True( names.Contains( expectedname ), $"{typeof( TraceEventTypeWithDescription )} value names contains {expectedname}" );
        }

        [Fact]
        public void TraceEventTypeValueDescriptionsTest()
        {
            var pairs = typeof( TraceEventType ).ValueDescriptionPairs();
            var expectedValue = TraceEventType.Critical;
            Assert.True( pairs.Any(), $"{typeof( TraceEventType )}.{nameof( FastEnums.EnumExtensions.ValueDescriptionPairs )} has values" );
            Assert.True( pairs.ContainsKey( ( Enum ) expectedValue ), $"{typeof( TraceEventType )} contains {expectedValue}" );

            var pair = pairs.SelectPair( expectedValue, expectedValue.Description() );
            // not working in Fluent assertions 6.0: _ = expectedValue.Should().Be( pair.Key, $"{typeof( TraceEventType )} found {expectedValue}" );
            _ = expectedValue.Should().Be( ( TraceEventType ) pair.Key, $"{typeof( TraceEventType )} found {expectedValue}" );

            var defaultValue = TraceEventType.Error;
            string description = "incorrect Description";
            pair = pairs.SelectPair( defaultValue, "incorrect Description" );
            _ = pair.Key.Should().Be( defaultValue, $"{typeof( TraceEventType )} found {defaultValue} due to {description}" );

            pairs = typeof( TraceEventType ).ValueNamePairs();
            expectedValue = TraceEventType.Critical;
            Assert.True( pairs.Any(), $"{typeof( System.Diagnostics.TraceEventType )}.{nameof( FastEnums.EnumExtensions.ValueDescriptionPairs )} has value names" );
            Assert.True( pairs.ContainsKey( ( Enum ) expectedValue ), $"{typeof( TraceEventType )} value names contains {expectedValue}" );

            string expectedDescription = TraceEventType.Critical.Description();
            var descriptions = TraceEventType.Critical.Descriptions();
            Assert.False( descriptions.Any(), $"{typeof( TraceEventType )}.{nameof( descriptions )} does not have a description" );

            var names = pairs.ToValues();
            Assert.True( names.Any(), $"{typeof( TraceEventType )}.{nameof( names )} has name" );

            string expectedname = TraceEventType.Critical.ToString();
            Assert.True( names.Contains( expectedname ), $"{typeof( TraceEventType )} value names contains {expectedname}" );
        }


        #endregion

        #region " ENUM EXTENSION TESTS: NAME and DESCRIPTION "

        [Flags]
        private enum ArmEvents : long
        {

            /// <summary> An enum constant representing the none option. </summary>
            [System.ComponentModel.Description( "None" )]
            None = 0,

            /// <summary> An enum constant representing the source option. </summary>
            [System.ComponentModel.Description( "Source" )]
            Source = 1 << 1,

            [System.ComponentModel.Description( "Timer" )]
            Timer = 1 << 2

        }


        [Flags]
        private enum TriggerEvents : long
        {

            /// <summary> An enum constant representing the none option. </summary>
            [System.ComponentModel.Description( "None" )]
            None = 1 << 0,

            /// <summary> An enum constant representing the source option. </summary>
            [System.ComponentModel.Description( "Source" )]
            Source = 1 << 1,

            [System.ComponentModel.Description( "Timer" )]
            Timer = 1 << 2,

            [System.ComponentModel.Description( "Blender" )]
            Blender = 1L << 33

        }


        /// <summary>   (Unit Test Method) tests enum names. </summary>
        /// <remarks>   David, 2020-10-28. </remarks>
        [Fact]
        public void EnumNamesTest()
        {
            TraceEventType traceEvent = TraceEventType.Verbose;
            string expectedValue = "Verbose";
            string actualValue = traceEvent.ToString();
            _ = actualValue.Should().Be( expectedValue, $"ToString() of {nameof( System.Diagnostics.TraceEventType.Verbose )}.{nameof( System.Diagnostics.TraceEventType.Verbose )} should match" );

            actualValue = traceEvent.Names();
            _ = actualValue.Should().Be( expectedValue, $"{nameof( FastEnums.EnumExtensions.Names )} of {nameof( System.Diagnostics.TraceEventType.Verbose )}.{nameof( System.Diagnostics.TraceEventType.Verbose )} should match" );

            TriggerEvents triggerEvent = TriggerEvents.Blender;

            expectedValue = "Blender";
            actualValue = triggerEvent.Names();
            _ = actualValue.Should().Be( expectedValue, $"{nameof( FastEnums.EnumExtensions.Names )} of {nameof( TriggerEvents )}.{nameof( TriggerEvents.Blender )} should match" );

            ArmEvents armEvent = ArmEvents.Source;
            expectedValue = "Source";
            actualValue = armEvent.Names();
            _ = actualValue.Should().Be( expectedValue, $"{nameof( FastEnums.EnumExtensions.Names )} of {nameof( ArmEvents )}.{nameof( ArmEvents.Source )} should match" );

            armEvent = ArmEvents.Source | ArmEvents.Timer;
            expectedValue = "Source, Timer";
            actualValue = armEvent.Names();
            _ = actualValue.Should().Be( expectedValue, $"{nameof( FastEnums.EnumExtensions.Names )} of {nameof( ArmEvents )}.({nameof( ArmEvents.Source )} or {nameof( ArmEvents.Timer )}) should match" );
        }

        /// <summary> (Unit Test Method) tests enum description. </summary>
        /// <remarks> David, 2020-10-14. </remarks>
        [Fact]
        public void EnumNameTest()
        {
            TraceEventType traceEvent = TraceEventType.Verbose;
            string expectedValue = "Verbose";
            string actualValue = traceEvent.ToString();
            _ = actualValue.Should().Be( expectedValue, $"ToString() of {nameof( System.Diagnostics.TraceEventType.Verbose )}.{nameof( System.Diagnostics.TraceEventType.Verbose )} should match" );

            TriggerEvents triggerEvent = TriggerEvents.Source;

            expectedValue = "Source";
            // this no longer gives the name of Blender as the Enum value is correctly set.
            actualValue = triggerEvent.ToString();
            _ = actualValue.Should().Be( expectedValue, $"ToString() value of  {nameof( TriggerEvents )}.{nameof( TriggerEvents.Source )} should match" );

            // this gives the name of Source
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
            actualValue = Enum.GetName( typeof( TriggerEvents ), triggerEvent );
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
            _ = actualValue.Should().Be( expectedValue, $"GetName() value of  {nameof( TriggerEvents )}.{nameof( TriggerEvents.Source )} should match" );

        }


        /// <summary> (Unit Test Method) tests enum description. </summary>
        /// <remarks> David, 2020-10-14. </remarks>
        [Fact]
        public void EnumDescriptionTest()
        {
            TraceEventType traceEvent = TraceEventType.Verbose;
            string expectedValue = "Verbose";
            string actualValue = traceEvent.DescriptionOrName();
            _ = actualValue.Should().Be( expectedValue, $"Description or name of {nameof( System.Diagnostics.TraceEventType.Verbose )}.{nameof( System.Diagnostics.TraceEventType.Verbose )} should match" );

            TriggerEvents triggerEvent = TriggerEvents.Source;

            expectedValue = "Source";
            actualValue = triggerEvent.Description();
            _ = actualValue.Should().Be( expectedValue, $"Description of {nameof( TriggerEvents )}.{nameof( TriggerEvents.Source )} should match" );

        }

        #endregion

    }

}

