
using System;

namespace FastEnums.Tests.Cases
{
    [Flags]
    internal enum Int64Enum : long
    {
        MinusOne = -1,
        Zero = 0,
        Max = 0x7FFFFFFFFFFFFFFF
    }
}
