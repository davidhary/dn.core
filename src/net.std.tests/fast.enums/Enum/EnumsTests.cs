using System;
using System.Linq;

using Xunit;

namespace FastEnums.Tests.Cases
{

    /// <summary>   (Unit Test Class) the enums tests. </summary>
    /// <remarks>   David, 2021-02-18. </remarks>
    public class EnumsTests
    {

        /// <summary>   (Unit Test Method) gets values should return singleton. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void GetValuesShouldReturnSingleton()
        {
            var numbers = FastEnum.GetValues<Number>();
            var numbers2 = FastEnum.GetValues<Number>();
            Assert.Same( numbers, numbers2 );
        }

        /// <summary>   (Unit Test Method) gets values should return read only list. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void GetValuesShouldReturnReadOnlyList()
        {
            var numbers = FastEnum.GetValues<Number>();
            _ = Assert.IsAssignableFrom<System.Collections.Generic.IReadOnlyList<Number>>( numbers );
        }

        /// <summary>   (Unit Test Method) gets values should return correct values. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void GetValuesShouldReturnCorrectValues()
        {
            var numbers = FastEnum.GetValues<Number>();
            Assert.True( numbers.SequenceEqual( new[] { Number.One, Number.Two, Number.Three } ) );
        }

        /// <summary>   (Unit Test Method) gets names should return singleton. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void GetNamesShouldReturnSingleton()
        {
            var names = FastEnum.GetNames<Number>();
            var names2 = FastEnum.GetNames<Number>();
            Assert.Same( names, names2 );
        }

        /// <summary>   (Unit Test Method) gets names should return read only list. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void GetNamesShouldReturnReadOnlyList()
        {
            var names = FastEnum.GetNames<Number>();
            _ = Assert.IsAssignableFrom<FastEnums.Internals.ReadOnlyArray<string>>( names );
        }

        /// <summary>   (Unit Test Method) gets names should return correct values. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void GetNamesShouldReturnCorrectValues()
        {
            var names = FastEnum.GetNames<Number>();
            Assert.True( names.SequenceEqual( new[] { "One", "Two", "Three" } ) );
        }

        /// <summary>   (Unit Test Method) flag should be named value. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void FlagShouldBeNamedValue()
        {
            Assert.True( BitFlags.Flag24.IsNamedValue() );
        }

        /// <summary>   (Unit Test Method) enum should be named value. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void EnumShouldBeNamedValue()
        {
            Assert.True( Number.One.IsNamedValue() );
        }

        /// <summary>   (Unit Test Method) flags combination should not be named value. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void FlagsCombinationShouldNotBeNamedValue()
        {
            Assert.False( (BitFlags.Flag1 | BitFlags.Flag2).IsNamedValue() );
        }

        /// <summary>   (Unit Test Method) undefined enum should not be named value. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void UndefinedEnumShouldNotBeNamedValue()
        {
            Assert.False( FastEnumExtensions.IsNamedValue<Number>( 0 ) );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a name non flags should parse from the given data,
        /// returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void TryParseNameNonFlagsShouldParse()
        {
            Assert.True( FastEnum.TryParse<Number>( "One", out Number number ) );
            Assert.Equal( Number.One, number );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a name non flags value should not parse from the given
        /// data, returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void TryParseNameNonFlagsValueShouldNotParse()
        {
            Assert.True( FastEnum.TryParse<Number>( "1", out _ ) );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a name non flags value should equal zero from the given
        /// data, returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void TryParseNameNonFlagsValueShouldEqualZero()
        {
            _ = FastEnum.TryParse<Number>( "1", out Number number );
            Assert.Equal( Number.One, number );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a name non flags unknown should not parse from the given
        /// data, returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void TryParseNameNonFlagsUnknownShouldNotParse()
        {
            Assert.False( FastEnum.TryParse<Number>( "rubbish", out Number number ) );
            Assert.Equal( ( Number ) 0, number );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a name non flags unknown value should be zero from the
        /// given data, returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void TryParseNameNonFlagsUnknownValueShouldBeZero()
        {
            _ = FastEnum.TryParse<Number>( "rubbish", out Number number );
            Assert.Equal( ( Number ) 0, number );
        }


        /// <summary>
        /// (Unit Test Method) attempts to parse a name flags from the given data, returning a default
        /// value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void TryParseNameFlags()
        {
            Assert.True( FastEnum.TryParse<BitFlags>( "Flag24", out BitFlags result ) );
            Assert.Equal( BitFlags.Flag24, result );
            Assert.True( FastEnum.TryParse<BitFlags>( "1", out result ) );
            Assert.Equal( BitFlags.Flag1, result );
            Assert.False( FastEnum.TryParse<BitFlags>( "rubbish", out result ) );
            Assert.Equal( ( BitFlags ) 0, result );
            Assert.False( FastEnum.TryParse<BitFlags>( "Flag2,Flag4", out result ) );
            Assert.Equal( ( BitFlags ) 0, result );
        }

        /// <summary>   (Unit Test Method) parse name invalid value. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void ParseNameInvalidValue()
        {
            _ = Assert.Throws<ArgumentException>( () => FastEnum.Parse<Number>( "rubish" ) );
        }

        /// <summary>   (Unit Test Method) parse name. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void ParseName()
        {
            Assert.Equal( Number.Two, FastEnum.Parse<Number>( "Two" ) );
            Assert.Equal( BitFlags.Flag24, FastEnum.Parse<BitFlags>( "Flag24" ) );
        }

        /// <summary>   (Unit Test Method) gets underlying type shoud equal. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void GetUnderlyingTypeShoudEqual()
        {
            Assert.Equal( typeof( byte ), FastEnum.GetUnderlyingType<ByteEnum>() );
            Assert.Equal( typeof( int ), FastEnum.GetUnderlyingType<Number>() );
            Assert.Equal( typeof( long ), FastEnum.GetUnderlyingType<Int64Enum>() );
            Assert.Equal( typeof( ulong ), FastEnum.GetUnderlyingType<UInt64Enum>() );
        }

        /// <summary>   (Unit Test Method) gets description should equal. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void GetDescriptionShouldEqual()
        {
            Assert.Equal( "First description", Number.One.Description() );
        }

        /// <summary>
        /// (Unit Test Method) gets description when value has no description should be null.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void GetDescriptionWhenValueHasNoDescriptionShouldBeNull()
        {
            Assert.Null( Number.Two.GetDescriptionValue( false ) );
        }

        /// <summary>   (Unit Test Method) gets description for invalid value should throw. </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void GetDescriptionForInvalidValueShouldThrow()
        {
            _ = Assert.Throws<IndexOutOfRangeException>( () => (( Number ) 4).GetDescriptionValue( true ) );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a unique description should succeed from the given data,
        /// returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void TryParseUniqueDescriptionShouldSucceed()
        {
            Assert.True( FastEnum.TryParseDescription<Number>( "Third description", out Number number ) );
            Assert.Equal( Number.Three, number );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a duplicate description should return the first match
        /// from the given data, returning a default value rather than throwing an exception if it
        /// fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void TryParseDuplicateDescriptionShouldReturnTheFirstMatch()
        {
            Assert.True( FastEnum.TryParseDescription<BitFlags>( "Duplicate description", out BitFlags result ) );
            Assert.Equal( BitFlags.Flag2, result );
        }

        /// <summary>
        /// (Unit Test Method) attempts to parse a missing description should be false from the given
        /// data, returning a default value rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-02-18. </remarks>
        [Fact]
        public void TryParseMissingDescriptionShouldBeFalse()
        {
            Assert.False( FastEnum.TryParseDescription<Number>( "Doesn't exist", out _ ) );
        }
    }
}
