
using System;

namespace FastEnums.Tests.Cases
{

    [Flags]
    internal enum UInt64Enum : ulong
    {
        Zero = 0,
        BigValue = 0xFFFFFFFFFFFFFFFF
    }
}
