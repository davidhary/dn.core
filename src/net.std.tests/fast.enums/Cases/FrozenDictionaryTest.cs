﻿using System;
using System.Linq;

using FastEnums.Internals;

using FluentAssertions;

using Xunit;



namespace FastEnums.Tests.Cases
{
    public class FrozenDictionaryTest
    {
        private const int Count = 100;


        [Fact]
        public void GenericsKey()
        {
            var values = Enumerable.Range( 0, Count );
            var dic = values.ToFrozenDictionary( x => x );

            _ = dic.Count.Should().Be( Count );
            _ = dic.TryGetValue( -1, out _ ).Should().BeFalse();
            foreach ( var x in values )
            {
                _ = dic.TryGetValue( x, out var result ).Should().BeTrue();
                _ = result.Should().Be( x );
            }
        }


        [Fact]
        public void IntKey()
        {
            var values = Enumerable.Range( 0, Count );
            var dic = values.ToFrozenInt32KeyDictionary( x => x );

            _ = dic.Count.Should().Be( Count );
            _ = dic.TryGetValue( -1, out _ ).Should().BeFalse();
            foreach ( var x in values )
            {
                _ = dic.TryGetValue( x, out var result ).Should().BeTrue();
                _ = result.Should().Be( x );
            }
        }


        [Fact]
        public void StringKey()
        {
            var values
                = Enumerable.Range( 0, Count )
                .Select( _ => Guid.NewGuid().ToString() )
                .ToArray();
            var dic = values.ToFrozenStringKeyDictionary( x => x );

            _ = dic.Count.Should().Be( Count );
            _ = dic.TryGetValue( string.Empty, out _ ).Should().BeFalse();
            foreach ( var x in values )
            {
                _ = dic.TryGetValue( x, out var result ).Should().BeTrue();
                _ = result.Should().Be( x );
            }
        }
    }
}
