using System;

using FluentAssertions;

using Xunit;

using TEnum = FastEnums.Tests.Models.Int64Enum;
using TUnderlying = System.Int64;



namespace FastEnums.Tests.Cases
{
    public class Int64Test
    {
        [Fact]
        public void GetUnderlyingType()
            => FastEnum.GetUnderlyingType<TEnum>().Should().Be<TUnderlying>();


        [Fact]
        public void GetValues()
        {
            var expect = new[]
            {
                default,
                TUnderlying.MaxValue,
                TUnderlying.MinValue,
            };
            var actual = FastEnum.GetValues<TEnum>();
            // Fluent Assertions 6.0 no longer able to compare 0xFF to 255! _ = actual.Should().BeEquivalentTo( expect );
            _ = actual[0].Should().Be( ( FastEnums.Tests.Models.Int64Enum ) expect[0] );
            _ = actual[1].Should().Be( ( FastEnums.Tests.Models.Int64Enum ) expect[1] );
            _ = actual[2].Should().Be( ( FastEnums.Tests.Models.Int64Enum ) expect[2] );
        }


        [Fact]
        public void GetNames()
        {
            var expect = new[]
            {
                nameof(TEnum.MinValue),
                nameof(TEnum.Zero),
                nameof(TEnum.MaxValue),
            };
            var actual = FastEnum.GetNames<TEnum>();
            _ = actual.Should().BeEquivalentTo( expect );
        }


        [Fact]
        public void GetMembers()
        {
            var expect = new[]
            {
                new Member<TEnum>(nameof(TEnum.Zero)),
                new Member<TEnum>(nameof(TEnum.MaxValue)),
                new Member<TEnum>(nameof(TEnum.MinValue)),
            };
            var actual = FastEnum.GetMembers<TEnum>();

            _ = actual.Count.Should().Be( expect.Length );
            for ( var i = 0; i < expect.Length; i++ )
            {
                var a = actual[i];
                var e = expect[i];
                _ = a.Value.Should().Be( e.Value );
                _ = a.Name.Should().Be( e.Name );
                _ = a.FieldInfo.Should().Be( e.FieldInfo );

                var (name, value) = a;
                _ = value.Should().Be( e.Value );
                _ = name.Should().Be( e.Name );
            }
        }


        [Fact]
        public void GetMinValue()
        {
            var min = FastEnum.GetMinValue<TEnum>();
            _ = min.Should().NotBeNull();
            _ = min.Should().Be( ( TEnum ) TUnderlying.MinValue );
        }


        [Fact]
        public void GetMaxValue()
        {
            var min = FastEnum.GetMaxValue<TEnum>();
            _ = min.Should().NotBeNull();
            _ = min.Should().Be( ( TEnum ) TUnderlying.MaxValue );
        }


        [Fact]
        public void IsEmpty()
            => FastEnum.IsEmpty<TEnum>().Should().Be( false );


        [Fact]
        public void IsContinuous()
            => FastEnum.IsContinuous<TEnum>().Should().Be( false );


        [Fact]
        public void IsFlags()
            => FastEnum.IsFlags<TEnum>().Should().Be( false );


        [Fact]
        public void IsDefined()
        {
            _ = FastEnum.IsDefined<TEnum>( TEnum.MinValue ).Should().BeTrue();
            _ = FastEnum.IsDefined<TEnum>( TEnum.Zero ).Should().BeTrue();
            _ = FastEnum.IsDefined<TEnum>( TEnum.MaxValue ).Should().BeTrue();
            _ = FastEnum.IsDefined<TEnum>( ( TEnum ) 123 ).Should().BeFalse();

            _ = TEnum.MinValue.IsDefined().Should().BeTrue();
            _ = TEnum.Zero.IsDefined().Should().BeTrue();
            _ = TEnum.MaxValue.IsDefined().Should().BeTrue();

            _ = FastEnum.IsDefined<TEnum>( nameof( TEnum.MinValue ) ).Should().BeTrue();
            _ = FastEnum.IsDefined<TEnum>( nameof( TEnum.Zero ) ).Should().BeTrue();
            _ = FastEnum.IsDefined<TEnum>( nameof( TEnum.MaxValue ) ).Should().BeTrue();
            _ = FastEnum.IsDefined<TEnum>( "123" ).Should().BeFalse();
            _ = FastEnum.IsDefined<TEnum>( "minvalue" ).Should().BeFalse();

            _ = FastEnum.IsDefined<TEnum>( TUnderlying.MinValue ).Should().BeTrue();
            _ = FastEnum.IsDefined<TEnum>( TUnderlying.MaxValue ).Should().BeTrue();
            _ = FastEnum.IsDefined<TEnum>( ( TUnderlying ) 123 ).Should().BeFalse();
            _ = FluentActions
                .Invoking( () => FastEnum.IsDefined<TEnum>( ( byte ) 123 ) )
                .Should()
                .Throw<ArgumentException>();
        }


        [Fact]
        public void Parse()
        {
            var parameters = new[]
            {
                (value: TEnum.MinValue, name: nameof(TEnum.MinValue), valueString: ((TUnderlying)TEnum.MinValue).ToString()),
                (value: TEnum.Zero,     name: nameof(TEnum.Zero),     valueString: ((TUnderlying)TEnum.Zero).ToString()),
                (value: TEnum.MaxValue, name: nameof(TEnum.MaxValue), valueString: ((TUnderlying)TEnum.MaxValue).ToString()),
            };
            foreach ( var (value, name, valueString) in parameters )
            {
                _ = FastEnum.Parse<TEnum>( name ).Should().Be( value );
                _ = FastEnum.Parse<TEnum>( valueString ).Should().Be( value );
                _ = FastEnum.Parse<TEnum>( valueString.ToLower() ).Should().Be( value );
                _ = FastEnum.Parse<TEnum>( valueString.ToUpper() ).Should().Be( value );
                _ = FluentActions.Invoking( () => FastEnum.Parse<TEnum>( name.ToLower() ) ).Should().Throw<ArgumentException>();
                _ = FluentActions.Invoking( () => FastEnum.Parse<TEnum>( name.ToUpper() ) ).Should().Throw<ArgumentException>();
            }
            _ = FluentActions.Invoking( () => FastEnum.Parse<TEnum>( "ABCDE" ) ).Should().Throw<ArgumentException>();
        }


        [Fact]
        public void ParseIgnoreCase()
        {
            var parameters = new[]
            {
                (value: TEnum.MinValue, name: nameof(TEnum.MinValue), valueString: ((TUnderlying)TEnum.MinValue).ToString()),
                (value: TEnum.Zero,     name: nameof(TEnum.Zero),     valueString: ((TUnderlying)TEnum.Zero).ToString()),
                (value: TEnum.MaxValue, name: nameof(TEnum.MaxValue), valueString: ((TUnderlying)TEnum.MaxValue).ToString()),
            };
            foreach ( var (value, name, valueString) in parameters )
            {
                _ = FastEnum.Parse<TEnum>( name ).Should().Be( value );
                _ = FastEnum.Parse<TEnum>( name.ToLower(), true ).Should().Be( value );
                _ = FastEnum.Parse<TEnum>( name.ToUpper(), true ).Should().Be( value );
                _ = FastEnum.Parse<TEnum>( valueString ).Should().Be( value );
                _ = FastEnum.Parse<TEnum>( valueString.ToLower(), true ).Should().Be( value );
                _ = FastEnum.Parse<TEnum>( valueString.ToUpper(), true ).Should().Be( value );
            }
            _ = FluentActions.Invoking( () => FastEnum.Parse<TEnum>( "ABCDE", true ) ).Should().Throw<ArgumentException>();
        }


        [Fact]
        public void TryParse()
        {
            var parameters = new[]
            {
                (value: TEnum.MinValue, name: nameof(TEnum.MinValue), valueString: ((TUnderlying)TEnum.MinValue).ToString()),
                (value: TEnum.Zero,     name: nameof(TEnum.Zero),     valueString: ((TUnderlying)TEnum.Zero).ToString()),
                (value: TEnum.MaxValue, name: nameof(TEnum.MaxValue), valueString: ((TUnderlying)TEnum.MaxValue).ToString()),
            };
            foreach ( var (value, name, valueString) in parameters )
            {
                _ = FastEnum.TryParse<TEnum>( name, out var r1 ).Should().BeTrue();
                _ = r1.Should().Be( value );

                _ = FastEnum.TryParse<TEnum>( valueString, out var r2 ).Should().BeTrue();
                _ = r2.Should().Be( value );

                _ = FastEnum.TryParse<TEnum>( valueString.ToLower(), out var r3 ).Should().BeTrue();
                _ = r3.Should().Be( value );

                _ = FastEnum.TryParse<TEnum>( valueString.ToUpper(), out var r4 ).Should().BeTrue();
                _ = r4.Should().Be( value );

                _ = FastEnum.TryParse<TEnum>( name.ToLower(), out _ ).Should().BeFalse();
                _ = FastEnum.TryParse<TEnum>( name.ToUpper(), out _ ).Should().BeFalse();
            }
            foreach ( var x in new[] { "ABCDE", "", null } )
                _ = FastEnum.TryParse<TEnum>( x, out _ ).Should().BeFalse();
        }


        [Fact]
        public void TryParseIgnoreCase()
        {
            var parameters = new[]
            {
                (value: TEnum.MinValue, name: nameof(TEnum.MinValue), valueString: ((TUnderlying)TEnum.MinValue).ToString()),
                (value: TEnum.Zero,     name: nameof(TEnum.Zero),     valueString: ((TUnderlying)TEnum.Zero).ToString()),
                (value: TEnum.MaxValue, name: nameof(TEnum.MaxValue), valueString: ((TUnderlying)TEnum.MaxValue).ToString()),
            };
            foreach ( var (value, name, valueString) in parameters )
            {
                _ = FastEnum.TryParse<TEnum>( name, true, out var r1 ).Should().BeTrue();
                _ = r1.Should().Be( value );

                _ = FastEnum.TryParse<TEnum>( name.ToLower(), true, out var r2 ).Should().BeTrue();
                _ = r2.Should().Be( value );

                _ = FastEnum.TryParse<TEnum>( name.ToUpper(), true, out var r3 ).Should().BeTrue();
                _ = r3.Should().Be( value );

                _ = FastEnum.TryParse<TEnum>( valueString, true, out var r4 ).Should().BeTrue();
                _ = r4.Should().Be( value );

                _ = FastEnum.TryParse<TEnum>( valueString.ToLower(), true, out var r5 ).Should().BeTrue();
                _ = r5.Should().Be( value );

                _ = FastEnum.TryParse<TEnum>( valueString.ToUpper(), true, out var r6 ).Should().BeTrue();
                _ = r6.Should().Be( value );
            }
            foreach ( var x in new[] { "ABCDE", "", null } )
                _ = FastEnum.TryParse<TEnum>( x, true, out _ ).Should().BeFalse();
        }


        [Fact]
        public void ToMember()
        {
            var value = TEnum.MaxValue;
            var name = nameof( TEnum.MaxValue );
            var member = value.ToMember();
            var info = typeof( TEnum ).GetField( name );

            _ = member.Name.Should().Be( name );
            _ = member.Value.Should().Be( value );
            _ = member.FieldInfo.Should().Be( info );
        }


        [Fact]
        public void ToName()
        {
            _ = TEnum.MinValue.ToName().Should().Be( nameof( TEnum.MinValue ) );
            _ = TEnum.Zero.ToName().Should().Be( nameof( TEnum.Zero ) );
            _ = TEnum.MaxValue.ToName().Should().Be( nameof( TEnum.MaxValue ) );
        }


        [Fact]
        public void ToUnderlying()
        {
            var @enum = TEnum.MinValue;
            var value = TUnderlying.MinValue;

            _ = FluentActions.Invoking( () => @enum.ToByte() ).Should().Throw<ArgumentException>();
            _ = FluentActions.Invoking( () => @enum.ToSByte() ).Should().Throw<ArgumentException>();
            _ = FluentActions.Invoking( () => @enum.ToInt16() ).Should().Throw<ArgumentException>();
            _ = FluentActions.Invoking( () => @enum.ToUInt16() ).Should().Throw<ArgumentException>();
            _ = FluentActions.Invoking( () => @enum.ToInt32() ).Should().Throw<ArgumentException>();
            _ = FluentActions.Invoking( () => @enum.ToUInt32() ).Should().Throw<ArgumentException>();
            _ = @enum.ToInt64().Should().Be( value );
            _ = FluentActions.Invoking( () => @enum.ToUInt64() ).Should().Throw<ArgumentException>();
        }
    }
}
