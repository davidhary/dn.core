﻿using FluentAssertions;
using Xunit;
using FastEnums.Tests.Models;



namespace FastEnums.Tests.Cases
{
    public class ContinuousValueTest
    {
        [Fact]
        public void Continuous()
            => FastEnum.IsContinuous<ContinuousValueEnum>().Should().Be( true );


        [Fact]
        public void ContinuousContainsSameValue()
            => FastEnum.IsContinuous<ContinuousValueContainsSameValueEnum>().Should().Be( true );


        [Fact]
        public void NotContinuous()
            => FastEnum.IsContinuous<NotContinuousValueEnum>().Should().Be( false );


        [Fact]
        public void GetName()
        {
            _ = FastEnum.GetName( ContinuousValueEnum.A ).Should().Be( nameof( ContinuousValueEnum.A ) );
            _ = FastEnum.GetName( ContinuousValueEnum.B ).Should().Be( nameof( ContinuousValueEnum.B ) );
            _ = FastEnum.GetName( ContinuousValueEnum.C ).Should().Be( nameof( ContinuousValueEnum.C ) );
            _ = FastEnum.GetName( ContinuousValueEnum.D ).Should().Be( nameof( ContinuousValueEnum.D ) );
            _ = FastEnum.GetName( ContinuousValueEnum.E ).Should().Be( nameof( ContinuousValueEnum.E ) );
        }
    }
}
