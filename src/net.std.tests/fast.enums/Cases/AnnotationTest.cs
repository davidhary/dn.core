using FluentAssertions;

using Xunit;

using TEnum = FastEnums.Tests.Models.AnnotationEnum;

namespace FastEnums.Tests.Cases
{
    public class AnnotationTest
    {
        [Fact]
        public void IsFlags()
            => FastEnum.IsFlags<TEnum>().Should().Be( true );


        [Fact]
        public void EnumMemberAttribute()
        {
            var zero = TEnum.Zero.ToMember().EnumMemberAttribute!;
            _ = zero.Should().NotBeNull();
            _ = zero.IsValueSetExplicitly.Should().BeTrue();
            _ = zero.Value.Should().Be( "_zero_" );

            var one = TEnum.One.ToMember().EnumMemberAttribute!;
            _ = one.Should().NotBeNull();
            _ = one.IsValueSetExplicitly.Should().BeFalse();
            _ = one.Value.Should().BeNull();

            var two = TEnum.Two.ToMember().EnumMemberAttribute;
            _ = two.Should().BeNull();
        }


        [Fact]
        public void GetEnumMemberValue()
        {
            _ = TEnum.Zero.GetEnumMemberValue().Should().Be( "_zero_" );
            _ = TEnum.One.GetEnumMemberValue().Should().BeNull();
            _ = TEnum.Two.GetEnumMemberValue( throwIfNotFound: false ).Should().BeNull();
            _ = FluentActions
                .Invoking( () => TEnum.Two.GetEnumMemberValue( throwIfNotFound: true ) )
                .Should()
                .Throw<NotFoundException>();
        }


        [Fact]
        public void GetLabel()
        {
            _ = TEnum.Zero.GetLabel( 0 ).Should().Be( "ぜろ" );
            _ = TEnum.Zero.GetLabel( 1 ).Should().Be( "零" );

            _ = TEnum.One.GetLabel( 0 ).Should().Be( "いち" );
            _ = TEnum.One.GetLabel( 1 ).Should().Be( "壱" );

            _ = TEnum.Two.GetLabel( 0, throwIfNotFound: false ).Should().BeNull();
            _ = FluentActions
                .Invoking( () => TEnum.Two.GetLabel( 0, throwIfNotFound: true ) )
                .Should()
                .Throw<NotFoundException>();

            _ = TEnum.Four.GetLabel( 2 ).Should().BeNull();
        }
    }
}
