using System;
using System.Reflection;

[assembly: AssemblyDescription( "Unit Tests for the Core Dictionaries Library" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
