using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Primitives.MSTest
{

    /// <summary> tests of data point structure. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    public class DataPointTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }


        #endregion

        #region " Data Point"

        /// <summary>   (Unit Test Method) data points should equal. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        [TestMethod()]
        public void DataPointsShouldEqual()
        {
            var left = new DataPoint<double>( 0d, 0d );
            var right = new DataPoint<double>( 0d, 0d );
            Assert.IsTrue( left == right, $"Left.{left} should == right.{right}" );
        }

        /// <summary>   (Unit Test Method) data points should not be equal. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        [TestMethod()]
        public void DataPointsShouldNotBeEqual()
        {
            var left = new DataPoint<double>( 0d, 0d );
            var right = new DataPoint<double>( 1d, 0d );
            Assert.IsFalse( left == right, $"Left.{left} should != right.{right}" );
        }

        /// <summary>   (Unit Test Method) data points should change. </summary>
        /// <remarks>   David, 2021-02-27. </remarks>
        [TestMethod()]
        public void DataPointsShouldChange()
        {
            var left = new DataPoint<double>( 0d, 0d );
            var right = new DataPoint<double>( left );
            Assert.IsTrue( left == right, $"Initially Left.{left} should == right.{right}" );
            right.SetPoint( 1, 0 );
            Assert.IsFalse( left == right, $"Then, after change, Left.{left} should != right.{right}" );
        }

        #endregion

    }
}
