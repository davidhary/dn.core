namespace isr.Core.Electric.MSTest
{

    /// <summary> A VoltageSource Test Info. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [isr.Json.SettingsSection( nameof( VoltageSourceTestInfo ) )]
    internal class VoltageSourceTestInfo : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public VoltageSourceTestInfo() : base( System.Reflection.Assembly.GetAssembly( typeof( VoltageSourceTestInfo ) ) )
        { }

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        public virtual bool Verbose { get; set; }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        public virtual bool Enabled { get; set; }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        public virtual bool All { get; set; }

        #endregion

        #region " VOLTAGE SOURCE TEST SETTINGS "

        /// <summary> Gets or sets the nominal voltage. </summary>
        /// <value> The nominal voltage. </value>
        public double NominalVoltage { get; set; }

        /// <summary> Gets or sets source resistance. </summary>
        /// <value> The source resistance. </value>
        public double SourceResistance { get; set; }

        /// <summary> Gets or sets the load resistance. </summary>
        /// <value> The load resistance. </value>
        public double LoadResistance { get; set; }

        /// <summary> Gets the load conductance. </summary>
        /// <value> The load conductance. </value>
        public double LoadConductance => Resistor.ToConductance( this.LoadResistance );

        /// <summary> Gets or sets the nominal current. </summary>
        /// <value> The nominal current. </value>
        public double NominalCurrent { get; set; }

        #endregion

        #region " ATTENUATED VOLTAGE SOURCE TEST SETTINGS "

        /// <summary> Gets or sets the series resistance. </summary>
        /// <value> The series resistance. </value>
        public double SeriesResistance { get; set; }

        /// <summary> Gets or sets the parallel resistance. </summary>
        /// <value> The parallel resistance. </value>
        public double ParallelResistance { get; set; }

        /// <summary> Gets the parallel conductance. </summary>
        /// <value> The parallel conductance. </value>
        public double ParallelConductance => Resistor.ToConductance( this.ParallelResistance );

        #endregion

    }
}
