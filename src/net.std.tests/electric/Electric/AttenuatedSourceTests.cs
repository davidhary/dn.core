using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Electric.MSTest
{

    /// <summary> Unit tests for testing attenuated voltage and current sources. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-09-18 </para>
    /// </remarks>
    [TestClass()]
    public class AttenuatedSourceTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " CONVERSION TESTS "

        /// <summary> (Unit Test Method) tests convert attenuated current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ConvertAttenuatedCurrentSourceTest()
        {
            var attenuatedCurrentSource = new AttenuatedCurrentSource( ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent,
                                                                       AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance,
                                                                       AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance );

            // create converted voltage sources using alternative conversions
            var sources = new List<AttenuatedVoltageSource>();
            var descriptions = new List<string>();
            sources.Add( attenuatedCurrentSource.ToAttenuatedVoltageSource( AppSettings.Instance.CurrentSourceTestInfo.NominalVoltage ) );
            descriptions.Add( "Voltage source from attenuated current source" );
            sources.Add( new VoltageSource( attenuatedCurrentSource ).ToAttenuatedVoltageSource( ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalVoltage ) );
            descriptions.Add( "Voltage source from voltage source" );
            sources.Add( attenuatedCurrentSource.ToVoltageSource().ToAttenuatedVoltageSource( ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalVoltage ) );
            descriptions.Add( "Voltage source from current source" );
            double doubleEpsilon = 0.0000000001d;
            AttenuatedVoltageSource source;
            for ( int i = 0, loopTo = sources.Count - 1; i <= loopTo; i++ )
            {
                source = sources[i];
                string description = descriptions[i];
                string item = $"nominal voltage";
                double actualValue = source.NominalVoltage;
                double expectedValue = AppSettings.Instance.CurrentSourceTestInfo.NominalVoltage;
                string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                string message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );

                // the two sources have identical output resistance
                item = $"equivalent resistance";
                actualValue = source.Resistance;
                expectedValue = Conductor.ToResistance( attenuatedCurrentSource.Conductance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );

                // the sources have the same open load voltage
                item = $"open load voltage";
                actualValue = source.LoadVoltage( Resistor.OpenResistance );
                expectedValue = attenuatedCurrentSource.LoadVoltage( Resistor.OpenResistance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );

                // the sources have the same short load voltage
                item = $"short load voltage";
                actualValue = source.LoadVoltage( Resistor.ShortResistance );
                expectedValue = attenuatedCurrentSource.LoadVoltage( Resistor.ShortResistance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );

                // the sources have the same load voltage
                item = $"load voltage";
                actualValue = source.LoadVoltage( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance );
                expectedValue = attenuatedCurrentSource.LoadVoltage( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );
            }
        }

        /// <summary> (Unit Test Method) tests convert attenuated voltage source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ConvertAttenuatedVoltageSourceTest()
        {
            double mimnimumImpedance = AppSettings.Instance.CurrentSourceTestInfo.NominalVoltage / AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent;
            double impedance = AttenuatedVoltageSource.ToEquivalentResistance( AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance, AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance );
            double minImpedanceFactor = 2d * mimnimumImpedance / impedance;
            // make sure that the voltage source impedance is high enough for conversion from 5v to 4 ma.
            var attenuatedVoltageSource = new AttenuatedVoltageSource( ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalVoltage, minImpedanceFactor * AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance, minImpedanceFactor * AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance );

            // create converted current sources using alternative conversions
            var sources = new List<AttenuatedCurrentSource>();
            var descriptions = new List<string>();
            sources.Add( attenuatedVoltageSource.ToAttenuatedCurrentSource( AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent ) );
            descriptions.Add( "Current source from attenuated voltage source" );
            sources.Add( new CurrentSource( attenuatedVoltageSource ).ToAttenuatedCurrentSource( ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent ) );
            descriptions.Add( "Current source from current source" );
            sources.Add( attenuatedVoltageSource.ToCurrentSource().ToAttenuatedCurrentSource( ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent ) );
            descriptions.Add( "Current source from voltage source" );
            double doubleEpsilon = 0.0000000001d;
            AttenuatedCurrentSource source;
            for ( int i = 0, loopTo = sources.Count - 1; i <= loopTo; i++ )
            {
                source = sources[i];
                string description = descriptions[i];
                string item = $"nominal current";
                double actualValue = source.NominalCurrent;
                double expectedValue = AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent;
                string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                string message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );

                // the two sources have identical output resistance
                item = $"equivalent resistance";
                actualValue = Conductor.ToResistance( source.Conductance );
                expectedValue = attenuatedVoltageSource.Resistance;
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );

                // the sources have the same short load current
                item = $"short load current";
                actualValue = source.LoadCurrent( Conductor.ShortConductance );
                expectedValue = attenuatedVoltageSource.LoadCurrent( Resistor.ShortResistance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );

                // the sources have the same open load current
                item = $"open load current";
                actualValue = source.LoadCurrent( Conductor.OpenConductance );
                expectedValue = attenuatedVoltageSource.LoadCurrent( Resistor.OpenResistance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );

                // the sources have the same load current
                item = $"load current";
                actualValue = source.LoadCurrent( Resistor.ToConductance( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance ) );
                expectedValue = attenuatedVoltageSource.LoadCurrent( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance );
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"{description}: Expected {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );
            }
        }

        #endregion

        #region " EXCEPTION CONVERSION TESTS "

        /// <summary> (Unit Test Method) tests low nominal voltage source exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( InvalidOperationException ), "Conversion from an attenuated current source to an attenuated voltage source with low nominal allowed" )]
        public void LowNominalVoltageSourceExceptionTest()
        {
            var currentSource = new AttenuatedCurrentSource( ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent, AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance, AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance );
            _ = currentSource.ToAttenuatedVoltageSource( 0.99d * currentSource.LoadVoltage( Resistor.OpenResistance ) );
        }

        /// <summary> (Unit Test Method) tests low nominal current source exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( InvalidOperationException ), "Conversion from an attenuated voltage source to an attenuated current source with low nominal current allowed" )]
        public void LowNominalCurrentSourceExceptionTest()
        {
            var voltageSource = new AttenuatedVoltageSource( ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalVoltage, AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance, AppSettings.Instance.CurrentSourceTestInfo.ParallelResistance );
            _ = voltageSource.ToAttenuatedCurrentSource( 0.99d * voltageSource.LoadCurrent( Resistor.ShortResistance ) );
        }

        /// <summary> (Unit Test Method) tests null attenuated current source exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentNullException ), "Null Attenuated Current Source inappropriately allowed" )]
        public void NullAttenuatedCurrentSourceExceptionTest()
        {
            AttenuatedCurrentSource currentSource = null;
            var source = new AttenuatedVoltageSource( ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalVoltage, AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance, AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance );
            source.FromAttenuatedCurrentSource( currentSource, ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalVoltage );
        }

        /// <summary> (Unit Test Method) tests null attenuated voltage source exception. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentNullException ), "Null Attenuated Voltage Source inappropriately allowed" )]
        public void NullAttenuatedVoltageSourceExceptionTest()
        {
            AttenuatedVoltageSource VoltageSource = null;
            var source = new AttenuatedCurrentSource( ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent, AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance, AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance );
            source.FromAttenuatedVoltageSource( VoltageSource, ( double ) ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent );
        }


        #endregion

    }
}
