namespace isr.Core.Electric.MSTest
{

    /// <summary>   Information about the current source test. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-11-26 </para>
    /// </remarks>
    [isr.Json.SettingsSection( nameof( LowTempZeroPressureBridgeTestInfo ) )]
    internal class LowTempZeroPressureBridgeTestInfo : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public LowTempZeroPressureBridgeTestInfo() : base( System.Reflection.Assembly.GetAssembly( typeof( LowTempZeroPressureBridgeTestInfo ) ) )
        { }

        #region " CONFIGURATION INFORMATION "

        /// <summary>   Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        public virtual bool Verbose { get; set; }

        /// <summary>   Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        public virtual bool Enabled { get; set; }

        /// <summary>   Gets or sets all. </summary>
        /// <value> all. </value>
        public virtual bool All { get; set; }

        #endregion

        #region " TEST INFO "

        /// <summary>   Gets or sets the layout. </summary>
        /// <value> The layout. </value>
        public int Layout { get; set; }

        /// <summary>   Gets or sets the r 1. </summary>
        /// <value> The r 1. </value>
        public double R1 { get; set; }
        /// <summary>   Gets or sets the r 2. </summary>
        /// <value> The r 2. </value>
        public double R2 { get; set; }
        /// <summary>   Gets or sets the r 3. </summary>
        /// <value> The r 3. </value>
        public double R3 { get; set; }
        /// <summary>   Gets or sets the r 4. </summary>
        /// <value> The r 4. </value>
        public double R4 { get; set; }
        /// <summary>   Gets or sets the supply voltage. </summary>
        /// <value> The supply voltage. </value>
        public double SupplyVoltage { get; set; }
        /// <summary>   Gets or sets the supply voltage drop. </summary>
        /// <value> The supply voltage drop. </value>
        public double SupplyVoltageDrop { get; set; }
        /// <summary>   Gets or sets the output voltage. </summary>
        /// <value> The output voltage. </value>
        public double OutputVoltage { get; set; }
        /// <summary>   Gets or sets the 1 balance. </summary>
        /// <value> The r 1 balance. </value>
        public double R1Balance { get; set; }
        /// <summary>   Gets or sets the 2 balance. </summary>
        /// <value> The r 2 balance. </value>
        public double R2Balance { get; set; }
        /// <summary>   Gets or sets the 3 balance. </summary>
        /// <value> The r 3 balance. </value>
        public double R3Balance { get; set; }
        /// <summary>   Gets or sets the 4 balance. </summary>
        /// <value> The r 4 balance. </value>
        public double R4Balance { get; set; }
        /// <summary>   Gets or sets the equivalent resistance. </summary>
        /// <value> The equivalent resistance. </value>
        public double EquivalentResistance { get; set; }
        /// <summary>   Gets or sets the relative offset epsilon. </summary>
        /// <value> The relative offset epsilon. </value>
        public double RelativeOffsetEpsilon { get; set; }
        /// <summary>   Gets or sets the nominal voltage. </summary>
        /// <value> The nominal voltage. </value>
        public double NominalVoltage { get; set; }
        /// <summary>   Gets or sets the voltage source series resistance. </summary>
        /// <value> The voltage source series resistance. </value>
        public double VoltageSourceSeriesResistance { get; set; }
        /// <summary>   Gets or sets the voltage source parallel resistance. </summary>
        /// <value> The voltage source parallel resistance. </value>
        public double VoltageSourceParallelResistance { get; set; }
        /// <summary>   Gets or sets the nominal full scale voltage. </summary>
        /// <value> The nominal full scale voltage. </value>
        public double NominalFullScaleVoltage { get; set; }
        /// <summary>   Gets or sets the nominal current. </summary>
        /// <value> The nominal current. </value>
        public double NominalCurrent { get; set; }
        /// <summary>   Gets or sets the current source series resistance. </summary>
        /// <value> The current source series resistance. </value>
        public double CurrentSourceSeriesResistance { get; set; }
        /// <summary>   Gets or sets the current source parallel resistance. </summary>
        /// <value> The current source parallel resistance. </value>
        public double CurrentSourceParallelResistance { get; set; }

        #endregion
    }
}
