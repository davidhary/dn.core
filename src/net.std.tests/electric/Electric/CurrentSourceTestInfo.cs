namespace isr.Core.Electric.MSTest
{

    /// <summary> Information about the current source test. </summary>
    /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-11-26 </para></remarks>
    [isr.Json.SettingsSection( nameof( CurrentSourceTestInfo ) )]
    internal class CurrentSourceTestInfo : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public CurrentSourceTestInfo() : base( System.Reflection.Assembly.GetAssembly( typeof( CurrentSourceTestInfo ) ) )
        { }

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        public virtual bool Verbose { get; set; }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        public virtual bool Enabled { get; set; }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        public virtual bool All { get; set; }

        #endregion

        #region " CURRENT SOURCE TEST SETTINGS "

        /// <summary> Gets or sets the nominal current. </summary>
        /// <value> The nominal current. </value>
        public double NominalCurrent { get; set; }

        /// <summary> Gets or sets source conductance. </summary>
        /// <value> The source conductance. </value>
        public double SourceConductance { get; set; }

        /// <summary> Gets or sets the load resistance. </summary>
        /// <value> The load resistance. </value>
        public double LoadResistance { get; set; }

        /// <summary> Gets or sets the nominal voltage. </summary>
        /// <value> The nominal voltage. </value>
        public double NominalVoltage { get; set; }

        #endregion

        #region " ATTENUETED CURRENT SOURCE TEST SETTINGS "

        /// <summary> Gets or sets the series resistance. </summary>
        /// <value> The series resistance. </value>
        public double SeriesResistance { get; set; }

        /// <summary> Gets or sets the parallel resistance. </summary>
        /// <value> The parallel resistance. </value>
        public double ParallelResistance { get; set; }

        /// <summary> Gets the parallel conductance. </summary>
        /// <value> The parallel conductance. </value>
        public double ParallelConductance => Resistor.ToConductance( this.ParallelResistance );

        #endregion

    }
}
