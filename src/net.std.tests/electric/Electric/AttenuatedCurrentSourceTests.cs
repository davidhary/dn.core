using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Electric.MSTest
{

    /// <summary> Unit tests for the attenuated current source. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-12-12 </para>
    /// </remarks>
    [TestClass()]
    public class AttenuatedCurrentSourceTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " CONSTRUCTION TESTS "

        /// <summary> (Unit Test Method) tests build. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void BuildTest()
        {
            double attenuation = AttenuatedCurrentSource.ToAttenuation( AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance, AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance );
            double equivalenctConductance = Conductor.SeriesResistor( AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance, AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance );


            // construct a current source 
            _ = new CurrentSource( AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent, equivalenctConductance );
            AttenuatedCurrentSource attenuatedSource = null;
            double doubleEpsilon = 0.0000000001d;
            string attenuatedSourcetarget = string.Empty;
            string sourceTarget = string.Empty;
            for ( int i = 1; i <= 3; i++ )
            {
                CurrentSource currentSource;
                switch ( i )
                {
                    case 1:
                        {
                            attenuatedSourcetarget = "nominal attenuated current source";
                            attenuatedSource = new AttenuatedCurrentSource( ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent, AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance, AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance );
                            sourceTarget = "current source from nominal";
                            break;
                        }

                    case 2:
                        {
                            attenuatedSourcetarget = "equivalent attenuated current source";
                            attenuatedSource = new AttenuatedCurrentSource( AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent / attenuation, AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance, AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance );
                            sourceTarget = "current source from equivalent";
                            break;
                        }

                    case 3:
                        {
                            attenuatedSourcetarget = "converted attenuated current source";
                            currentSource = new CurrentSource( AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent / attenuation, equivalenctConductance );
                            attenuatedSource = CurrentSource.ToAttenuatedCurrentSource( currentSource, attenuation );
                            sourceTarget = "current source from converted";
                            break;
                        }
                }

                currentSource = attenuatedSource.ToCurrentSource();

                // test the attenuated voltage source
                string target = attenuatedSourcetarget;
                double actualValue = attenuatedSource.NominalCurrent;
                double expectedValue = AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent;
                string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                string item = "nominal current";
                string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );
                target = attenuatedSourcetarget;
                actualValue = attenuatedSource.Current;
                expectedValue = AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent / attenuation;
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                item = "short load current";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );
                target = sourceTarget;
                actualValue = currentSource.Current;
                expectedValue = AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent / attenuation;
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                item = "short load current";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );
                target = attenuatedSourcetarget;
                actualValue = attenuatedSource.Conductance;
                expectedValue = equivalenctConductance;
                item = "equivalent conductance";
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );
                target = sourceTarget;
                actualValue = attenuatedSource.Conductance;
                expectedValue = equivalenctConductance;
                item = "equivalent conductance";
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );
                target = attenuatedSourcetarget;
                actualValue = attenuatedSource.SeriesResistance;
                expectedValue = AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance;
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                item = "series resistance";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );
                target = attenuatedSourcetarget;
                actualValue = attenuatedSource.Attenuation;
                expectedValue = attenuation;
                item = "attenuation";
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );
                target = attenuatedSourcetarget;
                actualValue = attenuatedSource.ParallelConductance;
                expectedValue = AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance;
                item = "parallel conductance";
                outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
                message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
                Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
                System.Diagnostics.Trace.TraceInformation( message );
            }
        }

        #endregion

        #region " EXCEPTION CONVERSION TESTS "

        /// <summary>
        /// (Unit Test Method) tests exception for converting a current source to an attenuated current
        /// source with invalid attenuation.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentOutOfRangeException ), "Attenuation smaller than 1 inappropriately allowed" )]
        public void ConvertAttenuationExceptionTest()
        {
            double attenuation = AttenuatedVoltageSource.MinimumAttenuation - 0.01d;
            var currentSource = new CurrentSource( AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent, AppSettings.Instance.CurrentSourceTestInfo.SourceConductance );
            _ = CurrentSource.ToAttenuatedCurrentSource( currentSource, attenuation );
        }

        /// <summary>
        /// (Unit Test Method) tests exception for converting to an attenuated source using a null source.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        [ExpectedException( typeof( ArgumentNullException ), "Conversion with null source allowed" )]
        public void ConvertNothingExceptionTest()
        {
            double attenuation = AttenuatedVoltageSource.MinimumAttenuation + 0.01d;
            CurrentSource currentSource = null;
            _ = CurrentSource.ToAttenuatedCurrentSource( currentSource, attenuation );
        }

        #endregion

        #region " LOAD AND PROPERTY CHANGE TESTS "

        /// <summary> (Unit Test Method) tests loading an attenuated current source. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void LoadTest()
        {
            var source = new AttenuatedCurrentSource( ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent, AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance, AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance );
            double doubleEpsilon = 0.0000000001d;

            // test the attenuated current source
            string target = "attenuated current source";

            // test open current
            string item = "open load current";
            double actualValue = source.LoadCurrent( Conductor.OpenConductance );
            double expectedValue = 0d;
            string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            item = "short load current";
            actualValue = source.LoadCurrent( Conductor.ShortConductance );
            expectedValue = ( double ) ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent / source.Attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test short voltage
            item = "short voltage";
            actualValue = source.LoadVoltage( 0d );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test load current
            item = "load current";
            actualValue = source.LoadCurrent( Resistor.ToConductance( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance ) );
            expectedValue = AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent *
                            Conductor.OutputCurrent( AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance,
                                                     Resistor.ToConductance( Resistor.ToSeries( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance,
                                                                                                AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test load voltage
            item = "load voltage";
            actualValue = source.LoadVoltage( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance );
            expectedValue = AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent * AppSettings.Instance.CurrentSourceTestInfo.LoadResistance * Conductor.OutputCurrent( AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance, AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
        }

        /// <summary>
        /// (Unit Test Method) tests changing properties of an attenuated voltage source.
        /// </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void PropertiesChangeTest()
        {

            // test changing Attenuated voltage source properties
            var source = new AttenuatedCurrentSource( ( decimal ) AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent, AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance, AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance );
            double doubleEpsilon = 0.0000000001d;

            // test the attenuated voltage source
            string target = "attenuated current source";
            double currentGain = 1.5d;

            // test shot load current
            string item = $"short load current {currentGain}={nameof( currentGain )}";
            source.NominalCurrent *= currentGain;
            double actualValue = source.LoadCurrent( Conductor.ShortConductance );
            double expectedValue = currentGain * AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent / source.Attenuation;
            string outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            string message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test open current
            item = $"open current {currentGain}={nameof( currentGain )}";
            actualValue = source.LoadCurrent( Conductor.OpenConductance );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test load current
            item = $"load current {currentGain}={nameof( currentGain )}";
            actualValue = source.LoadCurrent( Resistor.ToConductance( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance ) );
            expectedValue = currentGain * AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent * Conductor.OutputCurrent( AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance, AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            source.NominalCurrent /= currentGain;
            double seriesResistanceGain = 1.2d;
            source.SeriesResistance *= seriesResistanceGain;
            item = $"short load current {seriesResistanceGain}={nameof( seriesResistanceGain )}";
            actualValue = source.LoadCurrent( Conductor.ShortConductance );
            expectedValue = AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent / source.Attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test short current
            item = $"open load current {seriesResistanceGain}={nameof( seriesResistanceGain )}";
            actualValue = source.LoadCurrent( Conductor.OpenConductance );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test load current
            item = $"load current{seriesResistanceGain}={nameof( seriesResistanceGain )}";
            actualValue = source.LoadCurrent( Resistor.ToConductance( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance ) );
            expectedValue = AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent * Conductor.OutputCurrent( AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance, seriesResistanceGain * AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test load voltage
            item = $"load voltage {seriesResistanceGain}={nameof( seriesResistanceGain )}";
            actualValue = source.LoadVoltage( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance );
            expectedValue = AppSettings.Instance.CurrentSourceTestInfo.LoadResistance * AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent * Conductor.OutputCurrent( AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance, seriesResistanceGain * AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            double parallelConductanceGain = 1.2d;
            source.SeriesResistance /= seriesResistanceGain;
            source.ParallelConductance *= parallelConductanceGain;

            // test short current
            item = $"Short load current {parallelConductanceGain}={nameof( parallelConductanceGain )}";
            actualValue = source.LoadCurrent( Conductor.ShortConductance );
            expectedValue = AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent / source.Attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test open current
            item = $"open load current {parallelConductanceGain}={nameof( parallelConductanceGain )}";
            actualValue = source.LoadCurrent( Conductor.OpenConductance );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test load voltage
            item = $"load voltage {parallelConductanceGain}={nameof( parallelConductanceGain )}";
            actualValue = source.LoadVoltage( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance );
            expectedValue = AppSettings.Instance.CurrentSourceTestInfo.LoadResistance * AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent * Conductor.OutputCurrent( parallelConductanceGain * AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance, AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test load current
            item = $"load current {parallelConductanceGain}={nameof( parallelConductanceGain )}";
            actualValue = source.LoadCurrent( Resistor.ToConductance( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance ) );
            expectedValue = AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent * Conductor.OutputCurrent( parallelConductanceGain * AppSettings.Instance.CurrentSourceTestInfo.ParallelConductance, Resistor.ToConductance( Resistor.ToSeries( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance, AppSettings.Instance.CurrentSourceTestInfo.SeriesResistance ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
            double attenuationChange = 1.2d;
            source.ParallelConductance /= parallelConductanceGain;
            double newAttenuation = source.Attenuation * attenuationChange;
            double parallelR = Conductor.ToResistance( source.Conductance * newAttenuation );
            double seriesR = 1d / source.Conductance - parallelR;
            double newNominal = newAttenuation * AppSettings.Instance.CurrentSourceTestInfo.NominalCurrent / source.Attenuation;
            source.Attenuation *= attenuationChange;

            // test short current
            item = $"short load current {attenuationChange}={nameof( attenuationChange )}";
            actualValue = source.LoadCurrent( Conductor.ShortConductance );
            expectedValue = newNominal / source.Attenuation;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test open load current
            item = $"open load current {attenuationChange}={nameof( attenuationChange )}";
            actualValue = source.LoadCurrent( Conductor.OpenConductance );
            expectedValue = 0d;
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {item} {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test load voltage
            item = $"load voltage {attenuationChange}={nameof( attenuationChange )}";
            actualValue = source.LoadVoltage( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance );
            expectedValue = AppSettings.Instance.CurrentSourceTestInfo.LoadResistance * newNominal * Conductor.OutputCurrent( Resistor.ToConductance( parallelR ), Resistor.ToConductance( Resistor.ToSeries( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance, seriesR ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );

            // test load current
            item = $"load current {attenuationChange}={nameof( attenuationChange )}";
            actualValue = source.LoadCurrent( Resistor.ToConductance( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance ) );
            expectedValue = newNominal * Conductor.OutputCurrent( Resistor.ToConductance( parallelR ), Resistor.ToConductance( Resistor.ToSeries( AppSettings.Instance.CurrentSourceTestInfo.LoadResistance, seriesR ) ) );
            outcome = Math.Abs( expectedValue - actualValue ) < doubleEpsilon ? "=" : "!=";
            message = $"Expected {target} {item} {expectedValue} {outcome} actual {actualValue}";
            Assert.AreEqual( expectedValue, actualValue, doubleEpsilon, message );
            System.Diagnostics.Trace.TraceInformation( message );
        }

        #endregion

    }
}
