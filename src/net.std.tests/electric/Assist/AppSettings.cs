using System;

namespace isr.Core.Electric.MSTest
{
    /// <summary>   An application settings. </summary>
    /// <remarks>   David, 2021-01-28. </remarks>
    public class AppSettings : isr.Json.JsonAppSettingsBase
    {

        #region " CONSTRUCTION "

        private AppSettings() : base()
        {
            this.Settings = new Settings();
            base.AddSettingsSection( this.Settings );
            this.TestSiteSettings = new TestSiteSettings();
            base.AddSettingsSection( this.TestSiteSettings );
            this.CurrentSourceTestInfo = new CurrentSourceTestInfo();
            base.AddSettingsSection( this.CurrentSourceTestInfo );
            this.VoltageSourceTestInfo = new VoltageSourceTestInfo();
            base.AddSettingsSection( this.VoltageSourceTestInfo );
            this.LowTempZeroPressureBridgeTestInfo = new LowTempZeroPressureBridgeTestInfo();
            base.AddSettingsSection( this.LowTempZeroPressureBridgeTestInfo );
            this.ReadSettings();
        }

        /// <summary>   The lazy instance. </summary>
        private static readonly Lazy<AppSettings> LazyInstance = new( () => new AppSettings() );

        /// <summary>   Gets the instance. </summary>
        /// <value> The instance. </value>
        public static AppSettings Instance => LazyInstance.Value;

        #endregion
		
        /// <summary>   Gets the full name of the application settings file. </summary>
        /// <value> The full name of the application settings file. </value>
        public string AppSettingsFullFileName => this.Settings.FullFileName;

        /// <summary>   Gets or sets options for controlling the operation. </summary>
        /// <remarks>
        /// Note that the name of the field must match  class name or the name of the section as defined
        /// in the class <see cref="isr.Json.SettingsSectionAttribute"/> or.
        /// </remarks>
        /// <value> The settings. </value>
        internal Settings Settings { get; }

        /// <summary>   Gets or sets the test site settings. </summary>
        /// <remarks>
        /// Note that the name of the field must match  class name or the name of the section as defined
        /// in the class <see cref="isr.Json.SettingsSectionAttribute"/> or.
        /// </remarks>
        /// <value> The test site settings. </value>
        public TestSiteSettings TestSiteSettings { get; }

        /// <summary>   Gets information describing the current source test. </summary>
        /// <value> Information describing the current source test. </value>
        internal CurrentSourceTestInfo CurrentSourceTestInfo { get; }

        /// <summary>   Gets information describing the voltage source test. </summary>
        /// <value> Information describing the voltage source test. </value>
        internal VoltageSourceTestInfo VoltageSourceTestInfo { get; }

        /// <summary>
        /// Gets information describing the low temporary zero pressure bridge test.
        /// </summary>
        /// <value> Information describing the low temporary zero pressure bridge test. </value>
        internal LowTempZeroPressureBridgeTestInfo LowTempZeroPressureBridgeTestInfo { get; }

    }

}
