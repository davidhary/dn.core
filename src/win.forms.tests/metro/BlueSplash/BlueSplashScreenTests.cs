using System;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.WinForms.Metro.MSTest

{

    /// <summary> This is a test class for the blue splash screen. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestClass()]
    public class BlueSplashScreenTests
    {

        #region " CONSTRUCTION & CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        /// <summary> (Unit Test Method) displays a splash screen test. </summary>
        /// <remarks>
        /// This unit test causes a warning when ran from the test agent:
        /// This is an issue with the way the picture box displays a gif image.
        /// System.AppDomainUnloadedException: Attempted to access an unloaded AppDomain. This can happen
        /// if the test(s) started a thread but did not stop it. Make sure that all the threads started
        /// by the test(s) are stopped before completion.
        /// https://stackoverflow.com/questions/42979071/appdomainunloadedexception-when-unit-testing-a-winforms-form-with-an-animated-gi.
        /// </remarks>
        [TestMethod()]
        public void DisplaySplashScreenTest()
        {
            using var splashScreen = new Blue.BlueSplash();
            splashScreen.Show();
            splashScreen.TopmostSetter( true );
            Display( splashScreen );
        }

        /// <summary> Displays the given splashScreen. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        /// <param name="splashScreen"> The splash screen. </param>
        private static void Display( Blue.BlueSplash splashScreen )
        {
            for ( int i = 1; i <= 10; i++ )
            {
                splashScreen.DisplayMessage( $"Splash message {DateTimeOffset.Now}" );
                Task.Delay( 300 ).Wait();
            }
        }

        /// <summary> Tests the process exception on a another thread. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestMethod()]
        public void DisplaySplashScreenThreadTest()
        {
            var oThread = new System.Threading.Thread( new System.Threading.ThreadStart( this.DisplaySplashScreenTest ) );
            oThread.Start();
            oThread.Join();
        }
    }
}
