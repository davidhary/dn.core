using System;
using System.Diagnostics;
using System.Windows.Forms;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.Tracing.WinForms.MSTest
{

    /// <summary>   (Unit Test Class) trace event writer tests. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [TestClass()]
    public class TraceEventWriterTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void TestClassInitialize( TestContext testContext )
        {
            try
            {
                string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
                TraceListener = new isr.Core.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
                _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    TestClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void TestClassCleanup()
        {
            System.Diagnostics.Trace.Listeners.Remove( TraceListener );
            TraceListener.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            TraceListener.ClearQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-09-23. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            if ( !TraceListener.Queue.IsEmpty )
                Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
        }

        /// <summary>   Gets or sets the trace listener. </summary>
        /// <value> The trace listener. </value>
        private static isr.Core.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " TEXT BOX TEXT WRITER "

        private Core.Tracing.WinForms.TextBoxTraceEventWriter TextBoxTextWriter { get; set; }


        /// <summary>   (Unit Test Method) should trace message. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        [TestMethod()]
        public void ShouldTraceMessage()
        {
            // this is required to create the control handle.
            using var panel = new System.Windows.Forms.Form();
            using var target = new System.Windows.Forms.TextBox();
            target.WordWrap = false;
            target.Multiline = true;
            target.ReadOnly = true;
            target.CausesValidation = false;
            target.ScrollBars = ScrollBars.Both;
            panel.Controls.Add( target );
            // this is required to ensure the controls have handles for consummating the invocation;
            panel.Visible = true;
            bool handleCreated = target.IsHandleCreated;

            this.TextBoxTextWriter = new( target );
            this.TextBoxTextWriter.TabCaption = "Log";
            this.TextBoxTextWriter.CaptionFormat = "{0} " + System.Text.Encoding.GetEncoding( 1200 ).GetString( new byte[] { 240 } );
            this.TextBoxTextWriter.ResetCount = 1000;
            this.TextBoxTextWriter.PresetCount = 500;
            this.TextBoxTextWriter.TraceLevel = TraceEventType.Information;

            // the tracing listener is already registered.
            // _ = System.Diagnostics.Trace.Listeners.Add( Core.Tracing.TracingPlatform.Instance.TraceEventWriterTraceListener );

            // add the text box text writer.
            Core.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.TextBoxTextWriter );

            // this can be used for debugging
            // TracingPlatform.Instance.AddTraceEventWriter( TracingPlatform.Instance.CriticalTraceEventTraceListener );
            // Trace.TraceWarning( "testing a warning" );

            int initialTextLength = this.TextBoxTextWriter.TextLength;
            int initialLineCount = this.TextBoxTextWriter.LineCount;

            // send a trace message and see if it gets recorded. 
            string message = $"Message {DateTimeOffset.UtcNow}";

            Trace.TraceInformation( message );

            // Note: This was using Has Unread Messages; but with making the panel visible, 
            // Has Unread Messages could become false if Trace Writer finds the target text box visible.
            // This was an issue with the Messages Box Tests but not with this test. 
            // Still, the code here was change to determine success based on the actual message length.

            DateTime endTime = DateTime.Now.AddMilliseconds( 200 );
            while ( ( this.TextBoxTextWriter.TextLength <= initialTextLength) && DateTime.Now < endTime )
            {
                System.Threading.Tasks.Task.Delay( 1 ).Wait();
                // this is required otherwise the  text Box Trace Event Writer invoke hangs.
                System.Windows.Forms.Application.DoEvents();
            }

            Assert.IsTrue( this.TextBoxTextWriter.TextLength > initialTextLength, "Text should be added to the messages box" );

            int ExpectedLineCount = initialLineCount + 1;
            Assert.AreEqual( ExpectedLineCount, this.TextBoxTextWriter.LineCount, "The number of messages added to this listener should match the expected value " +
target.Text );

        }

        #endregion

        #region " TRACE ALERT CONTAINER "

        private TraceAlertContainer TraceAlertContainer { get; set; }

        /// <summary>   (Unit Test Method) should trace alert. </summary>
        /// <remarks>   David, 2021-07-29. </remarks>
        [TestMethod()]
        public void ShouldTraceAlert()
        {
            // this is required to create the control handle.
            using var panel = new System.Windows.Forms.Form();
            using var traceWriter = new System.Windows.Forms.TextBox();
            using var alertAnnunciator = new System.Windows.Forms.Button();
            panel.Controls.Add( traceWriter );
            panel.Controls.Add( alertAnnunciator );
            // this is required to ensure the controls have handles for consummating the invocation;
            panel.Visible = true;
            bool handleCreated = traceWriter.IsHandleCreated;
            handleCreated = alertAnnunciator.IsHandleCreated;

            this.TraceAlertContainer = new( alertAnnunciator, traceWriter );
            this.TraceAlertContainer.AlertAnnunciatorText = "Log";
            this.TraceAlertContainer.AlertLevel = TraceEventType.Information;
            this.TraceAlertContainer.AlertSoundEnabled = true;

            // the tracing listener is already registered.
            // _ = System.Diagnostics.Trace.Listeners.Add( Core.Tracing.TracingPlatform.Instance.TraceEventWriterTraceListener );

            // add the text box text writer.
            Core.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.TraceAlertContainer );

            // this can be used for debugging
            // TracingPlatform.Instance.AddTraceEventWriter( TracingPlatform.Instance.CriticalTraceEventTraceListener );
            // Trace.TraceWarning( "testing a warning" );

            // send a trace message and see if it gets recorded. 
            string message = $"Message {DateTimeOffset.UtcNow}";

            Trace.TraceWarning( message );

            DateTime endTime = DateTime.Now.AddMilliseconds( 20000 );
            while ( !( this.TraceAlertContainer.AlertAnnounced & this.TraceAlertContainer.AlertVoiced) && DateTime.Now < endTime )
            {
                System.Threading.Tasks.Task.Delay( 1 ).Wait();
                // this is required otherwise the  trace Alert Container invoke might hang; this actually did not occur when testing.
                // but the thread hang on an invoke that was removed from teh class.
                // System.Windows.Forms.Application.DoEvents();
            }

            Assert.IsTrue( this.TraceAlertContainer.AlertAnnounced, "Alert should be announced" );
            Assert.IsTrue( this.TraceAlertContainer.AlertVoiced, "Alert should be voiced" );
            TraceListener.Queue.Clear();

        }

        #endregion

    }
}
