using System;

namespace isr.Core.WinForms.Views.MSTest
{
    /// <summary> Binding Tests information. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-02-18. </para>
    /// </remarks>
    [isr.Json.SettingsSection( nameof( BindingTestsInfo ) )]
    internal class BindingTestsInfo : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public BindingTestsInfo() : base( System.Reflection.Assembly.GetAssembly( typeof( BindingTestsInfo ) ) )
        { }

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        public virtual bool Verbose { get; set; }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        public virtual bool Enabled { get; set; }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        public virtual bool All { get; set; }

        #endregion

        #region " I7 TESTS SETTINGS "

        /// <summary> Gets or sets the number of items. </summary>
        /// <value> The number of items. </value>
        public int ItemCount { get; set; }

        /// <summary> Gets or sets the 7k 2600 create timespan. </summary>
        /// <value> The i 7k 2600 create timespan. </value>
        public TimeSpan I7k2600CreateTimespan { get; set; }

        /// <summary> Gets or sets the 7k 2600 sort internal timespan. </summary>
        /// <value> The i 7k 2600 sort internal timespan. </value>
        public TimeSpan I7k2600SortInternalTimespan { get; set; }

        /// <summary> Gets or sets the 7k 2600 sort extent time span. </summary>
        /// <value> The i 7k 2600 sort extent time span. </value>
        public TimeSpan I7k2600SortExtTimeSpan { get; set; }

        /// <summary> Gets or sets the 7k 2600 data table sort timespan. </summary>
        /// <value> The i 7k 2600 data table sort timespan. </value>
        public TimeSpan I7k2600DataTableSortTimespan { get; set; }

        /// <summary> Gets or sets the 7k 2600. </summary>
        /// <value> The i 7k 2600. </value>
        public string I7k2600 { get; set; }

        #endregion

        #region " AGGREGATE FEED TESTS SETTINGS "

        private string _NewsOnlineFeedUrl = "http://feeds.bbci.co.uk/news/uk/rss.xml";

        /// <summary>   Gets or sets URL of the news online feed. </summary>
        /// <value> The news online feed URL. </value>
        public string NewsOnlineFeedUrl
        {
            get => this._NewsOnlineFeedUrl;
            set {
                if ( !string.Equals( value, this.NewsOnlineFeedUrl ) )
                {
                    this._NewsOnlineFeedUrl = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets URL of the MSDN channel 9 feed. </summary>
        /// <value> The MSDN channel 9 feed URL. </value>
        public string MsdnChannel9FeedUrl { get; set; }

        /// <summary> Gets or sets URL of the MSDN blogs feed. </summary>
        /// <value> The MSDN blogs feed URL. </value>
        public string MsdnBlogsFeedUrl { get; set; }

        #endregion

    }
}
