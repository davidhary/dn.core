using System;
using System.Reflection;

[assembly: AssemblyDescription( "Unit Tests for the Core Windows Forms Views Library" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
