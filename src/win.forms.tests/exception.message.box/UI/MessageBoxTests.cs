using System;

using isr.Core.WinForms.Dialogs;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Core.WinForms.ExceptionMessageBox.MSTest
{

    /// <summary> SQL Message Box tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-13 </para>
    /// </remarks>
    [TestClass()]
    public class MessageBoxTests
    {

        /// <summary> Shows the dialog abort ignore. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="DivideByZeroException"> Thrown when an attempt is made to divide a number by
        /// zero. </exception>
        public static void ShowDialogException()
        {
            try
            {
                throw new DivideByZeroException();
            }
            catch ( Exception ex )
            {
                var expected = MyDialogResult.Ok;
                MyDialogResult actual;
                ex.Data.Add( "@isr", "Testing exception message" );
                actual = MyMessageBox.ShowDialog( ex );
                Assert.AreEqual( expected, actual );
            }
        }

        /// <summary>
        /// Send the given keys to the active application and then wait for the message to be processed.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="keys"> The keys. </param>
        public static void SendWait( string keys )
        {
            System.Windows.Forms.SendKeys.SendWait( keys );
        }


        /// <summary> (Unit Test Method) tests show exception dialog. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ShowExceptionDialogTest()
        {
            var oThread = new System.Threading.Thread( new System.Threading.ThreadStart( ShowDialogException ) );
            oThread.Start();
            // a long delay was required...
            System.Threading.Tasks.Task.Delay( 2000 ).Wait();
            SendWait( "{ENTER}" );
            // This tabbed into another application.  isr.Core.Services.WindowsForms.SendWait("%{TAB}{ENTER}")
            oThread.Join();
        }

        /// <summary> Shows the dialog abort ignore. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        /// <exception cref="DivideByZeroException"> Thrown when an attempt is made to divide a number by
        /// zero. </exception>
        public static void ShowDialogAbortIgnore()
        {
            try
            {
                throw new DivideByZeroException();
            }
            catch ( DivideByZeroException ex )
            {
                ex.Data.Add( "@isr", "Testing exception message" );
                var result = MyMessageBox.ShowDialogAbortIgnore( ex, MyMessageBoxIcon.Error );
                Assert.IsTrue( result == MyDialogResult.Abort || result == MyDialogResult.Ignore, $"{result} expected {MyDialogResult.Abort} or {MyDialogResult.Ignore}" );
            }
        }

        /// <summary> Tests showing dialog with abort and ignore buttons. </summary>
        /// <remarks> David, 2020-09-18. </remarks>
        [TestMethod()]
        public void ShowDialogAbortIgnoreTest()
        {
            var oThread = new System.Threading.Thread( new System.Threading.ThreadStart( ShowDialogAbortIgnore ) );
            oThread.Start();
            // a long delay was required...
            System.Threading.Tasks.Task.Delay( 2000 ).Wait();
            SendWait( "{ENTER}" );
            // This tabbed into another application.  isr.Core.Services.WindowsForms.SendWait("%{TAB}{ENTER}")
            oThread.Join();
        }
    }
}
