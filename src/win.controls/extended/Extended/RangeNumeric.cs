using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Core.WinControls
{
    /// <summary>   A range numeric. </summary>
    /// <remarks>   David, 2021-09-15. </remarks>
    public partial class RangeNumeric : UserControlBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Clears internal properties. </summary>
        /// <remarks> David, 2021-09-15. </remarks>
        public RangeNumeric() : base()
        {
            this.InitializeComponent();
            this._LowerNumericHorizontalLocation = HorizontalLocation.Left;
            this._LowerNumericVerticalLocation = VerticalLocation.Center;
            this._UpperNumericHorizontalLocation = HorizontalLocation.Right;
            this._UpperNumericVerticalLocation = VerticalLocation.Center;
            this._LowerNumeric.ReadOnly = false;
            this._UpperNumeric.ReadOnly = false;
            this.ArrangeControls();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Forma.UserControlBase and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks> David, 2021-09-15. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                    this.components = null;

                    this.RemoveRangeChangedEventHandler( this.RangeChanged );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " BEHAVIOR "

        /// <summary> Gets or sets the read only property. </summary>
        /// <value> The read only. </value>
        [DefaultValue( false )]
        [Category( "Behavior" )]
        [Description( "Indicates whether the numeric is read only." )]
        public bool ReadOnly
        {
            get => this._LowerNumeric.ReadOnly;

            set {
                if ( this.ReadOnly != value )
                {
                    this._LowerNumeric.ReadOnly = value;
                    this._UpperNumeric.ReadOnly = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets or sets the minimum. </summary>
        /// <value> The minimum value. </value>
        public decimal Minimum
        {
            get => this._UpperNumeric.Minimum;
            set {
                if ( !decimal.Equals( value, this.Minimum ) )
                {
                    this._UpperNumeric.Minimum = value;
                    this._LowerNumeric.Minimum = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets or sets the maximum. </summary>
        /// <value> The maximum value. </value>
        [DefaultValue( 100 )]
        [Category( "Behavior" )]
        [Description( "The maximum value." )]
        public decimal Maximum
        {
            get => this._UpperNumeric.Maximum;
            set {
                if ( !decimal.Equals( value, this.Maximum ) )
                {
                    this._UpperNumeric.Maximum = value;
                    this._LowerNumeric.Maximum = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The lower value. </summary>
        /// <value> The lower. </value>
        [DefaultValue( 0 )]
        [Category( "Behavior" )]
        [Description( "The lower value." )]
        public decimal Lower
        {
            get => this._LowerNumeric.Value;
            set {
                if ( !decimal.Equals( value, this.Lower ) )
                {
                    this._LowerNumeric.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The upper value. </summary>
        /// <value> The upper. </value>
        [DefaultValue( 100 )]
        [Category( "Behavior" )]
        [Description( "The upper value." )]
        public decimal Upper
        {
            get => this._UpperNumeric.Value;
            set {
                if ( !decimal.Equals( value, this.Upper ) )
                {
                    this._UpperNumeric.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets or sets the decimal places. </summary>
        /// <value> The decimal places. </value>
        public int DecimalPlaces
        {
            get => this._UpperNumeric.DecimalPlaces;
            set {
                if ( !int.Equals( value, this.DecimalPlaces ) )
                {
                    this._UpperNumeric.DecimalPlaces = value;
                    this._LowerNumeric.DecimalPlaces = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " LOWER NUMERIC "

        /// <summary>   Gets the lower numeric. </summary>
        /// <value> The lower numeric. </value>
        public NumericUpDown LowerNumeric => this._LowerNumeric;

        /// <summary> The Lower Numeric horizontal location. </summary>
        private HorizontalLocation _LowerNumericHorizontalLocation;

        /// <summary> Gets or sets the Lower Numeric horizontal location. </summary>
        /// <value> The Lower Numeric horizontal location. </value>
        [DefaultValue( typeof( HorizontalLocation ), "Left" )]
        [Category( "Behavior" )]
        [Description( "Horizontal location of the Lower Numeric." )]
        public HorizontalLocation LowerNumericHorizontalLocation
        {
            get => this._LowerNumericHorizontalLocation;

            set {
                if ( value != this.LowerNumericHorizontalLocation )
                {
                    this._LowerNumericHorizontalLocation = value;
                    this.NotifyPropertyChanged();
                    this.ArrangeControls();
                }
            }
        }

        /// <summary> The Lower Numeric vertical location. </summary>
        private VerticalLocation _LowerNumericVerticalLocation;

        /// <summary> Gets or sets the Lower Numeric vertical location. </summary>
        /// <value> The Lower Numeric vertical location. </value>
        [DefaultValue( typeof( VerticalLocation ), "Center" )]
        [Category( "Behavior" )]
        [Description( "Vertical location of the Lower Numeric." )]
        public VerticalLocation LowerNumericVerticalLocation
        {
            get => this._LowerNumericVerticalLocation;

            set {
                if ( value != this.LowerNumericVerticalLocation )
                {
                    this._LowerNumericVerticalLocation = value;
                    this.NotifyPropertyChanged();
                    this.ArrangeControls();
                }
            }
        }

        /// <summary> The Lower Numeric tool tip. </summary>
        private string _LowerNumericToolTip;

        /// <summary> Gets or sets the Lower Numeric toolTip. </summary>
        /// <value> The Lower Numeric toolTip. </value>
        [Category( "Appearance" )]
        [Description( "Lower Numeric Tool Tip" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Lower numeric" )]
        public string LowerNumericToolTip
        {
            get => this._LowerNumericToolTip;

            set {
                if ( !string.Equals( this.LowerNumericToolTip, value ) )
                {
                    this._LowerNumericToolTip = value;
                    this.ToolTip.SetToolTip( this._LowerNumeric, value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " UPPER NUMERIC "

        /// <summary> The Upper Numeric horizontal location. </summary>
        private HorizontalLocation _UpperNumericHorizontalLocation;

        /// <summary> Gets or sets the Upper Numeric horizontal location. </summary>
        /// <value> The Upper Numeric horizontal location. </value>
        [DefaultValue( typeof( HorizontalLocation ), "Right" )]
        [Category( "Behavior" )]
        [Description( "Horizontal location of the Upper Numeric." )]
        public HorizontalLocation UpperNumericHorizontalLocation
        {
            get => this._UpperNumericHorizontalLocation;

            set {
                if ( value != this.UpperNumericHorizontalLocation )
                {
                    this._UpperNumericHorizontalLocation = value;
                    this.NotifyPropertyChanged();
                    this.ArrangeControls();
                }
            }
        }

        /// <summary> The Upper Numeric vertical location. </summary>
        private VerticalLocation _UpperNumericVerticalLocation;

        /// <summary> Gets or sets the Upper Numeric vertical location. </summary>
        /// <value> The Upper Numeric vertical location. </value>
        [DefaultValue( typeof( VerticalLocation ), "Center" )]
        [Category( "Behavior" )]
        [Description( "Vertical location of the Upper Numeric." )]
        public VerticalLocation UpperNumericVerticalLocation
        {
            get => this._UpperNumericVerticalLocation;

            set {
                if ( value != this.UpperNumericVerticalLocation )
                {
                    this._UpperNumericVerticalLocation = value;
                    this.NotifyPropertyChanged();
                    this.ArrangeControls();
                }
            }
        }

        /// <summary> The Upper Numeric tool tip. </summary>
        private string _UpperNumericToolTip;

        /// <summary> Gets or sets the Upper Numeric toolTip. </summary>
        /// <value> The Upper Numeric toolTip. </value>
        [Category( "Appearance" )]
        [Description( "Upper Numeric Tool Tip" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Upper numeric" )]
        public string UpperNumericToolTip
        {
            get => this._UpperNumericToolTip;

            set {
                if ( !string.Equals( this.UpperNumericToolTip, value ) )
                {
                    this._UpperNumericToolTip = value;
                    this.ToolTip.SetToolTip( this._LowerNumeric, value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " ARRANGE CONTROL "

        /// <summary> Gets the row and column for the control within the control layout. </summary>
        /// <remarks> David, 2021-09-15. </remarks>
        /// <param name="verticalLocation">   The vertical location. </param>
        /// <param name="horizontalLocation"> The horizontal location. </param>
        /// <returns> A (Column As Integer, Row As Integer) </returns>
        private static (int Column, int Row) LocateControl( VerticalLocation verticalLocation, HorizontalLocation horizontalLocation )
        {
            int column = 1;
            int row = 1;
            switch ( verticalLocation )
            {
                case VerticalLocation.Bottom:
                    {
                        row = 2;
                        break;
                    }

                case VerticalLocation.Center:
                    {
                        row = 1;
                        break;
                    }

                case VerticalLocation.Top:
                    {
                        row = 0;
                        break;
                    }
            }

            switch ( horizontalLocation )
            {
                case HorizontalLocation.Center:
                    {
                        column = 1;
                        break;
                    }

                case HorizontalLocation.Left:
                    {
                        column = 0;
                        break;
                    }

                case HorizontalLocation.Right:
                    {
                        column = 2;
                        break;
                    }
            }

            return (column, row);
        }

        /// <summary> Arrange the controls within the control layout. </summary>
        /// <remarks> David, 2021-09-15. </remarks>
        private void ArrangeControls()
        {
            var (Column, Row) = LocateControl( this.LowerNumericVerticalLocation, this.LowerNumericHorizontalLocation );
            var secondaryLocation = LocateControl( this.UpperNumericVerticalLocation, this.UpperNumericHorizontalLocation );
            float hundredPercent = 100.0f;

            // clear the styles
            foreach ( ColumnStyle colStyle in this._Layout.ColumnStyles )
            {
                colStyle.SizeType = SizeType.AutoSize;
            }

            foreach ( RowStyle rowStyle in this._Layout.RowStyles )
            {
                rowStyle.SizeType = SizeType.AutoSize;
            }

            this._Layout.RowStyles[Row].SizeType = SizeType.Percent;
            this._Layout.RowStyles[secondaryLocation.Row].SizeType = SizeType.Percent;
            this._Layout.ColumnStyles[Column].SizeType = SizeType.Percent;
            this._Layout.ColumnStyles[secondaryLocation.Column].SizeType = SizeType.Percent;
            int percentCount = 0;
            foreach ( RowStyle rowStyle in this._Layout.RowStyles )
            {
                if ( rowStyle.SizeType == SizeType.Percent )
                {
                    percentCount += 1;
                }
            }

            foreach ( RowStyle rowStyle in this._Layout.RowStyles )
            {
                if ( rowStyle.SizeType == SizeType.Percent )
                {
                    rowStyle.Height = hundredPercent / percentCount;
                }
            }

            percentCount = 0;
            foreach ( ColumnStyle columnStyle in this._Layout.ColumnStyles )
            {
                if ( columnStyle.SizeType == SizeType.Percent )
                {
                    percentCount += 1;
                }
            }

            foreach ( ColumnStyle columnStyle in this._Layout.ColumnStyles )
            {
                if ( columnStyle.SizeType == SizeType.Percent )
                {
                    columnStyle.Width = hundredPercent / percentCount;
                }
            }

            this._Layout.Controls.Clear();
            this._LowerNumeric.Dock = DockStyle.None;
            this._UpperNumeric.Dock = DockStyle.None;
            this._Layout.Controls.Add( this._LowerNumeric, Column, Row );
            this._Layout.Controls.Add( this._UpperNumeric, secondaryLocation.Column, secondaryLocation.Row );
            this._LowerNumeric.Dock = DockStyle.Fill;
            this._UpperNumeric.Dock = DockStyle.Fill;
        }

        #endregion

        #region " EVENTS "

        /// <summary>Occurs when the range value changed. </summary>
        public event EventHandler<EventArgs> RangeChanged;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2021-09-15. </remarks>
        /// <param name="value"> The handler. </param>
        private void RemoveRangeChangedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    this.RangeChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                }
            }
        }

        /// <summary>   Raises the Range Changed event. </summary>
        /// <remarks>   David, 2021-09-15. </remarks>
        protected virtual void OnRangeChanged()
        {
            this.RangeChanged?.Invoke( this, EventArgs.Empty );
        }

        #endregion


    }
}
