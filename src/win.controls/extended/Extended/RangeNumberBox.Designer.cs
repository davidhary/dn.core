
namespace isr.Core.WinControls
{
    partial class RangeNumberBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._Layout = new System.Windows.Forms.TableLayoutPanel();
            this._LowerNumberBox = new isr.Core.WinControls.NumberBox();
            this._UpperNumberBox = new isr.Core.WinControls.NumberBox();
            this._Layout.SuspendLayout();
            this.SuspendLayout();
            // 
            // _Layout
            // 
            this._Layout.BackColor = System.Drawing.Color.Transparent;
            this._Layout.ColumnCount = 3;
            this._Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._Layout.Controls.Add(this._LowerNumberBox, 0, 1);
            this._Layout.Controls.Add(this._UpperNumberBox, 2, 1);
            this._Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._Layout.Location = new System.Drawing.Point(0, 0);
            this._Layout.Name = "_Layout";
            this._Layout.RowCount = 3;
            this._Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this._Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this._Layout.Size = new System.Drawing.Size(222, 42);
            this._Layout.TabIndex = 1;
            // 
            // _LowerNumberBox
            // 
            this._LowerNumberBox.BackColor = System.Drawing.SystemColors.Window;
            this._LowerNumberBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this._LowerNumberBox.Location = new System.Drawing.Point(3, 12);
            this._LowerNumberBox.MaxValue = 1.7976931348623157E+308D;
            this._LowerNumberBox.MinValue = -1.7976931348623157E+308D;
            this._LowerNumberBox.Name = "_LowerNumberBox";
            this._LowerNumberBox.Prefix = null;
            this._LowerNumberBox.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            this._LowerNumberBox.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            this._LowerNumberBox.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            this._LowerNumberBox.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            this._LowerNumberBox.Size = new System.Drawing.Size(100, 20);
            this._LowerNumberBox.Snfs = null;
            this._LowerNumberBox.Suffix = null;
            this._LowerNumberBox.TabIndex = 2;
            this._LowerNumberBox.ValueAsByte = ((byte)(0));
            this._LowerNumberBox.ValueAsDouble = 0D;
            this._LowerNumberBox.ValueAsFloat = 0F;
            this._LowerNumberBox.ValueAsInt16 = ((short)(0));
            this._LowerNumberBox.ValueAsInt32 = 0;
            this._LowerNumberBox.ValueAsInt64 = ((long)(0));
            this._LowerNumberBox.ValueAsSByte = ((sbyte)(0));
            this._LowerNumberBox.ValueAsUInt16 = ((ushort)(0));
            this._LowerNumberBox.ValueAsUInt32 = ((uint)(0u));
            this._LowerNumberBox.ValueAsUInt64 = ((ulong)(0ul));
            // 
            // _UpperNumberBox
            // 
            this._UpperNumberBox.BackColor = System.Drawing.SystemColors.Window;
            this._UpperNumberBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this._UpperNumberBox.Location = new System.Drawing.Point(119, 12);
            this._UpperNumberBox.MaxValue = 1.7976931348623157E+308D;
            this._UpperNumberBox.MinValue = -1.7976931348623157E+308D;
            this._UpperNumberBox.Name = "_UpperNumberBox";
            this._UpperNumberBox.Prefix = null;
            this._UpperNumberBox.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            this._UpperNumberBox.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            this._UpperNumberBox.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            this._UpperNumberBox.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            this._UpperNumberBox.Size = new System.Drawing.Size(100, 20);
            this._UpperNumberBox.Snfs = null;
            this._UpperNumberBox.Suffix = null;
            this._UpperNumberBox.TabIndex = 3;
            this._UpperNumberBox.ValueAsByte = ((byte)(0));
            this._UpperNumberBox.ValueAsDouble = 0D;
            this._UpperNumberBox.ValueAsFloat = 0F;
            this._UpperNumberBox.ValueAsInt16 = ((short)(0));
            this._UpperNumberBox.ValueAsInt32 = 0;
            this._UpperNumberBox.ValueAsInt64 = ((long)(0));
            this._UpperNumberBox.ValueAsSByte = ((sbyte)(0));
            this._UpperNumberBox.ValueAsUInt16 = ((ushort)(0));
            this._UpperNumberBox.ValueAsUInt32 = ((uint)(0u));
            this._UpperNumberBox.ValueAsUInt64 = ((ulong)(0ul));
            // 
            // RangeNumberBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._Layout);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "RangeNumberBox";
            this.Size = new System.Drawing.Size(222, 42);
            this._Layout.ResumeLayout(false);
            this._Layout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel _Layout;
        private NumberBox _LowerNumberBox;
        private NumberBox _UpperNumberBox;
    }
}
