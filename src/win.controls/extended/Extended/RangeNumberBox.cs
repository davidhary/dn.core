using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Core.WinControls
{
    /// <summary>   A range number box. </summary>
    /// <remarks>   David, 2021-09-15. </remarks>
    public partial class RangeNumberBox : UserControlBase
    {
        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Clears internal properties. </summary>
        /// <remarks> David, 2021-09-15. </remarks>
        public RangeNumberBox() : base()
        {
            this.InitializeComponent();
            this._LowerNumberBoxHorizontalLocation = HorizontalLocation.Left;
            this._LowerNumberBoxVerticalLocation = VerticalLocation.Center;
            this._UpperNumberBoxHorizontalLocation = HorizontalLocation.Right;
            this._UpperNumberBoxVerticalLocation = VerticalLocation.Center;
            this._LowerNumberBox.ReadOnly = false;
            this._UpperNumberBox.ReadOnly = false;
            this.ArrangeControls();
            this._LowerNumberBox.TextChanged += new System.EventHandler( this.RangeChanged );
            this._UpperNumberBox.TextChanged += new System.EventHandler( this.RangeChanged );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.Forma.UserControlBase and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks> David, 2021-09-15. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                    this.components = null;

                    this.RemoveRangeChangedEventHandler( this.RangeChanged );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " RANGE NUMBER BOX "

        /// <summary> Gets or sets the read only property. </summary>
        /// <value> The read only. </value>
        [DefaultValue( false )]
        [Category( "NumberBox" )]
        [Description( "Indicates whether the NumberBox is read only." )]
        public bool ReadOnly
        {
            get => this._LowerNumberBox.ReadOnly;

            set {
                if ( this.ReadOnly != value )
                {
                    this._LowerNumberBox.ReadOnly = value;
                    this._UpperNumberBox.ReadOnly = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets or sets the minimum. </summary>
        /// <value> The minimum value. </value>
        [DefaultValue( 100 )]
        [Category( "NumberBox" )]
        [Description( "The minimum value." )]
        public double Minimum
        {
            get => this._UpperNumberBox.MinValue;
            set {
                if ( !double.Equals( value, this.Minimum ) )
                {
                    this._UpperNumberBox.MinValue = value;
                    this._LowerNumberBox.MinValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets or sets the maximum. </summary>
        /// <value> The maximum value. </value>
        [DefaultValue( 100 )]
        [Category( "NumberBox" )]
        [Description( "The maximum value." )]
        public double Maximum
        {
            get => this._UpperNumberBox.MaxValue;
            set {
                if ( !double.Equals( value, this.Maximum ) )
                {
                    this._UpperNumberBox.MaxValue = value;
                    this._LowerNumberBox.MaxValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Standard Numeric Format String D,E,F,N,X, B for binary. </summary>
        /// <value> The Standard numeric format string </value>
        [Category( "NumberBox" ), Description( "Standard Numeric Format String D,E,F,N,X, B for binary" )]
        public string Snfs
        {
            get => this._LowerNumberBox.Snfs;
            set {
                if ( !string.Equals( this.Snfs, value ) )
                {
                    this._LowerNumberBox.Snfs = value;
                    this._UpperNumberBox.Snfs = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Prefix for X and B formats. </summary>
        /// <value> The prefix. </value>
        [Category( "NumberBox" ), Description( "Prefix for X and B formats" )]
        public string Prefix
        {
            get => this._LowerNumberBox.Prefix;
            set {
                if ( !string.Equals( this.Prefix, value ) )
                {
                    this._LowerNumberBox.Prefix = value;
                    this._UpperNumberBox.Prefix = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Suffix for X and B formats. </summary>
        /// <value> The suffix. </value>
        [Category( "NumberBox" ), Description( "Suffix for X and B formats" )]
        public string Suffix
        {
            get => this._LowerNumberBox.Suffix;
            set {
                if ( !string.Equals( this.Suffix, value ) )
                {
                    this._LowerNumberBox.Suffix = value;
                    this._UpperNumberBox.Suffix = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " LOWER NUMBER BOX "

        /// <summary>   Gets the lower number box. </summary>
        /// <value> The lower number box. </value>
        public NumberBox LowerNumberBox => this._LowerNumberBox;

        /// <summary> The Lower NumberBox horizontal location. </summary>
        private HorizontalLocation _LowerNumberBoxHorizontalLocation;

        /// <summary> Gets or sets the Lower NumberBox horizontal location. </summary>
        /// <value> The Lower NumberBox horizontal location. </value>
        [DefaultValue( typeof( HorizontalLocation ), "Left" )]
        [Category( "Behavior" )]
        [Description( "Horizontal location of the Lower NumberBox." )]
        public HorizontalLocation LowerNumberBoxHorizontalLocation
        {
            get => this._LowerNumberBoxHorizontalLocation;

            set {
                if ( value != this.LowerNumberBoxHorizontalLocation )
                {
                    this._LowerNumberBoxHorizontalLocation = value;
                    this.NotifyPropertyChanged();
                    this.ArrangeControls();
                }
            }
        }

        /// <summary> The Lower NumberBox vertical location. </summary>
        private VerticalLocation _LowerNumberBoxVerticalLocation;

        /// <summary> Gets or sets the Lower NumberBox vertical location. </summary>
        /// <value> The Lower NumberBox vertical location. </value>
        [DefaultValue( typeof( VerticalLocation ), "Center" )]
        [Category( "Behavior" )]
        [Description( "Vertical location of the Lower NumberBox." )]
        public VerticalLocation LowerNumberBoxVerticalLocation
        {
            get => this._LowerNumberBoxVerticalLocation;

            set {
                if ( value != this.LowerNumberBoxVerticalLocation )
                {
                    this._LowerNumberBoxVerticalLocation = value;
                    this.NotifyPropertyChanged();
                    this.ArrangeControls();
                }
            }
        }

        /// <summary> The Lower NumberBox tool tip. </summary>
        private string _LowerNumberBoxToolTip;

        /// <summary> Gets or sets the Lower NumberBox toolTip. </summary>
        /// <value> The Lower NumberBox toolTip. </value>
        [Category( "Appearance" )]
        [Description( "Lower NumberBox Tool Tip" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Lower NumberBox" )]
        public string LowerNumberBoxToolTip
        {
            get => this._LowerNumberBoxToolTip;

            set {
                if ( !string.Equals( this.LowerNumberBoxToolTip, value ) )
                {
                    this._LowerNumberBoxToolTip = value;
                    this.ToolTip.SetToolTip( this._LowerNumberBox, value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " UPPER NUMBER BOX "

        /// <summary>   Gets the Upper number box. </summary>
        /// <value> The Upper number box. </value>
        public NumberBox UpperNumberBox => this._UpperNumberBox;

        /// <summary> The Upper NumberBox horizontal location. </summary>
        private HorizontalLocation _UpperNumberBoxHorizontalLocation;

        /// <summary> Gets or sets the Upper NumberBox horizontal location. </summary>
        /// <value> The Upper NumberBox horizontal location. </value>
        [DefaultValue( typeof( HorizontalLocation ), "Right" )]
        [Category( "Behavior" )]
        [Description( "Horizontal location of the Upper NumberBox." )]
        public HorizontalLocation UpperNumberBoxHorizontalLocation
        {
            get => this._UpperNumberBoxHorizontalLocation;

            set {
                if ( value != this.UpperNumberBoxHorizontalLocation )
                {
                    this._UpperNumberBoxHorizontalLocation = value;
                    this.NotifyPropertyChanged();
                    this.ArrangeControls();
                }
            }
        }

        /// <summary> The Upper NumberBox vertical location. </summary>
        private VerticalLocation _UpperNumberBoxVerticalLocation;

        /// <summary> Gets or sets the Upper NumberBox vertical location. </summary>
        /// <value> The Upper NumberBox vertical location. </value>
        [DefaultValue( typeof( VerticalLocation ), "Center" )]
        [Category( "Behavior" )]
        [Description( "Vertical location of the Upper NumberBox." )]
        public VerticalLocation UpperNumberBoxVerticalLocation
        {
            get => this._UpperNumberBoxVerticalLocation;

            set {
                if ( value != this.UpperNumberBoxVerticalLocation )
                {
                    this._UpperNumberBoxVerticalLocation = value;
                    this.NotifyPropertyChanged();
                    this.ArrangeControls();
                }
            }
        }

        /// <summary> The Upper NumberBox tool tip. </summary>
        private string _UpperNumberBoxToolTip;

        /// <summary> Gets or sets the Upper NumberBox toolTip. </summary>
        /// <value> The Upper NumberBox toolTip. </value>
        [Category( "Appearance" )]
        [Description( "Upper NumberBox Tool Tip" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Upper NumberBox" )]
        public string UpperNumberBoxToolTip
        {
            get => this._UpperNumberBoxToolTip;

            set {
                if ( !string.Equals( this.UpperNumberBoxToolTip, value ) )
                {
                    this._UpperNumberBoxToolTip = value;
                    this.ToolTip.SetToolTip( this._LowerNumberBox, value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " ARRANGE CONTROL "

        /// <summary> Gets the row and column for the control within the control layout. </summary>
        /// <remarks> David, 2021-09-15. </remarks>
        /// <param name="verticalLocation">   The vertical location. </param>
        /// <param name="horizontalLocation"> The horizontal location. </param>
        /// <returns> A (Column As Integer, Row As Integer) </returns>
        private static (int Column, int Row) LocateControl( VerticalLocation verticalLocation, HorizontalLocation horizontalLocation )
        {
            int column = 1;
            int row = 1;
            switch ( verticalLocation )
            {
                case VerticalLocation.Bottom:
                    {
                        row = 2;
                        break;
                    }

                case VerticalLocation.Center:
                    {
                        row = 1;
                        break;
                    }

                case VerticalLocation.Top:
                    {
                        row = 0;
                        break;
                    }
            }

            switch ( horizontalLocation )
            {
                case HorizontalLocation.Center:
                    {
                        column = 1;
                        break;
                    }

                case HorizontalLocation.Left:
                    {
                        column = 0;
                        break;
                    }

                case HorizontalLocation.Right:
                    {
                        column = 2;
                        break;
                    }
            }

            return (column, row);
        }

        /// <summary> Arrange the controls within the control layout. </summary>
        /// <remarks> David, 2021-09-15. </remarks>
        private void ArrangeControls()
        {
            var (Column, Row) = LocateControl( this.LowerNumberBoxVerticalLocation, this.LowerNumberBoxHorizontalLocation );
            var secondaryLocation = LocateControl( this.UpperNumberBoxVerticalLocation, this.UpperNumberBoxHorizontalLocation );
            float hundredPercent = 100.0f;

            // clear the styles
            foreach ( ColumnStyle colStyle in this._Layout.ColumnStyles )
            {
                colStyle.SizeType = SizeType.AutoSize;
            }

            foreach ( RowStyle rowStyle in this._Layout.RowStyles )
            {
                rowStyle.SizeType = SizeType.AutoSize;
            }

            this._Layout.RowStyles[Row].SizeType = SizeType.Percent;
            this._Layout.RowStyles[secondaryLocation.Row].SizeType = SizeType.Percent;
            this._Layout.ColumnStyles[Column].SizeType = SizeType.Percent;
            this._Layout.ColumnStyles[secondaryLocation.Column].SizeType = SizeType.Percent;
            int percentCount = 0;
            foreach ( RowStyle rowStyle in this._Layout.RowStyles )
            {
                if ( rowStyle.SizeType == SizeType.Percent )
                {
                    percentCount += 1;
                }
            }

            foreach ( RowStyle rowStyle in this._Layout.RowStyles )
            {
                if ( rowStyle.SizeType == SizeType.Percent )
                {
                    rowStyle.Height = hundredPercent / percentCount;
                }
            }

            percentCount = 0;
            foreach ( ColumnStyle columnStyle in this._Layout.ColumnStyles )
            {
                if ( columnStyle.SizeType == SizeType.Percent )
                {
                    percentCount += 1;
                }
            }

            foreach ( ColumnStyle columnStyle in this._Layout.ColumnStyles )
            {
                if ( columnStyle.SizeType == SizeType.Percent )
                {
                    columnStyle.Width = hundredPercent / percentCount;
                }
            }

            this._Layout.Controls.Clear();
            this._LowerNumberBox.Dock = DockStyle.None;
            this._UpperNumberBox.Dock = DockStyle.None;
            this._Layout.Controls.Add( this._LowerNumberBox, Column, Row );
            this._Layout.Controls.Add( this._UpperNumberBox, secondaryLocation.Column, secondaryLocation.Row );
            this._LowerNumberBox.Dock = DockStyle.Fill;
            this._UpperNumberBox.Dock = DockStyle.Fill;
        }

        #endregion

        #region " EVENTS "

        /// <summary>   Raises the Range Changed event. </summary>
        /// <remarks>   David, 2021-09-15. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnRangeChanged( object sender, EventArgs e )
        {
            this.OnRangeChanged( );
        }

        /// <summary>Occurs when the range value changed. </summary>
        public event EventHandler<EventArgs> RangeChanged;

        /// <summary> Removes event handler. </summary>
        /// <remarks> David, 2021-09-15. </remarks>
        /// <param name="value"> The handler. </param>
        private void RemoveRangeChangedEventHandler( EventHandler<EventArgs> value )
        {
            foreach ( Delegate d in value is null ? (Array.Empty<Delegate>()) : value.GetInvocationList() )
            {
                try
                {
                    this.RangeChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                }
            }
        }

        /// <summary>   Raises the Range Changed event. </summary>
        /// <remarks>   David, 2021-09-15. </remarks>
        protected virtual void OnRangeChanged()
        {
            this.RangeChanged?.Invoke( this, EventArgs.Empty );
        }

        #endregion

    }
}

