using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.WinControls
{
    public partial class DropDownTextBox
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _DropDownToggle = new System.Windows.Forms.CheckBox();
            _DropDownToggle.CheckedChanged += new EventHandler(DropDownToggle_CheckedChanged);
            _TextBox = new TextBox();
            SuspendLayout();
            // 
            // _dropDownToggle
            // 
            _DropDownToggle.Appearance = Appearance.Button;
            _DropDownToggle.Cursor = Cursors.Arrow;
            _DropDownToggle.Dock = DockStyle.Right;
            _DropDownToggle.Image = Extended.Properties.Resources.go_down_8;
            _DropDownToggle.Location = new Point(514, 0);
            _DropDownToggle.Margin = new Padding(0);
            _DropDownToggle.MaximumSize = new Size(24, 25);
            _DropDownToggle.Name = "_dropDownToggle";
            _DropDownToggle.Size = new Size(24, 25);
            _DropDownToggle.TabIndex = 0;
            _DropDownToggle.TextAlign = ContentAlignment.TopCenter;
            _DropDownToggle.UseVisualStyleBackColor = true;
            // 
            // _TextBox
            // 
            _TextBox.BackColor = SystemColors.Window;
            _TextBox.Dock = DockStyle.Fill;
            _TextBox.ForeColor = SystemColors.ControlText;
            _TextBox.Location = new Point(0, 0);
            _TextBox.Margin = new Padding(0);
            _TextBox.Name = "_TextBox";
            _TextBox.ReadOnlyBackColor = SystemColors.Control;
            _TextBox.ReadOnlyForeColor = SystemColors.WindowText;
            _TextBox.ReadWriteBackColor = SystemColors.Window;
            _TextBox.ReadWriteForeColor = SystemColors.ControlText;
            _TextBox.Size = new Size(514, 25);
            _TextBox.TabIndex = 1;
            // 
            // DropDownTextBox
            // 
            Controls.Add(_TextBox);
            Controls.Add(_DropDownToggle);
            Margin = new Padding(0);
            Name = "DropDownTextBox";
            Size = new Size(538, 25);
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.CheckBox _DropDownToggle;
        internal TextBox _TextBox;
    }
}
