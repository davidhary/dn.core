using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.WinControls
{
    public partial class DualFieldComboBox
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _Layout = new TableLayoutPanel();
            _SecondFieldTextBox = new System.Windows.Forms.TextBox();
            _SecondFieldTextBox.Validated += new EventHandler(SecondFieldTextBox_Validated);
            _FirstFieldTextBox = new System.Windows.Forms.TextBox();
            _FirstFieldTextBox.Validated += new EventHandler(FirstFieldTextBox_Validated);
            _SecondFieldTextBoxLabel = new Label();
            _FirstFieldTextBoxLabel = new Label();
            _TextComboBox = new System.Windows.Forms.ComboBox();
            _TextComboBox.SelectedIndexChanged += new EventHandler(TextComboBox_SelectedIndexChanged);
            _TextComboBox.Layout += new LayoutEventHandler(TextComboBox_Layout);
            _Layout.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 2;
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
            _Layout.Controls.Add(_SecondFieldTextBox, 1, 1);
            _Layout.Controls.Add(_FirstFieldTextBox, 0, 1);
            _Layout.Controls.Add(_SecondFieldTextBoxLabel, 1, 0);
            _Layout.Controls.Add(_FirstFieldTextBoxLabel, 0, 0);
            _Layout.Dock = DockStyle.Left;
            _Layout.Location = new Point(0, 0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 2;
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _Layout.Size = new Size(339, 41);
            _Layout.TabIndex = 0;
            // 
            // _SecondFieldTextBox
            // 
            _SecondFieldTextBox.Dock = DockStyle.Top;
            _SecondFieldTextBox.Location = new Point(172, 16);
            _SecondFieldTextBox.Name = "_SecondFieldTextBox";
            _SecondFieldTextBox.Size = new Size(164, 20);
            _SecondFieldTextBox.TabIndex = 3;
            // 
            // _FirstFieldTextBox
            // 
            _FirstFieldTextBox.Dock = DockStyle.Top;
            _FirstFieldTextBox.Location = new Point(3, 16);
            _FirstFieldTextBox.Name = "_FirstFieldTextBox";
            _FirstFieldTextBox.Size = new Size(163, 20);
            _FirstFieldTextBox.TabIndex = 1;
            // 
            // _SecondFieldTextBoxLabel
            // 
            _SecondFieldTextBoxLabel.AutoSize = true;
            _SecondFieldTextBoxLabel.Dock = DockStyle.Top;
            _SecondFieldTextBoxLabel.Location = new Point(172, 0);
            _SecondFieldTextBoxLabel.Name = "_SecondFieldTextBoxLabel";
            _SecondFieldTextBoxLabel.Size = new Size(164, 13);
            _SecondFieldTextBoxLabel.TabIndex = 2;
            _SecondFieldTextBoxLabel.Text = "LAST NAME";
            // 
            // _FirstFieldTextBoxLabel
            // 
            _FirstFieldTextBoxLabel.AutoSize = true;
            _FirstFieldTextBoxLabel.Dock = DockStyle.Top;
            _FirstFieldTextBoxLabel.Location = new Point(3, 0);
            _FirstFieldTextBoxLabel.Name = "_FirstFieldTextBoxLabel";
            _FirstFieldTextBoxLabel.Size = new Size(163, 13);
            _FirstFieldTextBoxLabel.TabIndex = 0;
            _FirstFieldTextBoxLabel.Text = "FIRST NAME";
            // 
            // _TextComboBox
            // 
            _TextComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _TextComboBox.FormattingEnabled = true;
            _TextComboBox.Location = new Point(0, 20);
            _TextComboBox.Name = "_TextComboBox";
            _TextComboBox.Size = new Size(360, 21);
            _TextComboBox.TabIndex = 0;
            // 
            // DualFieldComboBox
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_Layout);
            Controls.Add(_TextComboBox);
            Name = "DualFieldComboBox";
            Size = new Size(360, 41);
            _Layout.ResumeLayout(false);
            _Layout.PerformLayout();
            Resize += new EventHandler(DualFieldComboBox_Resize);
            Validated += new EventHandler(DualFieldComboBox_Validated);
            ResumeLayout(false);
        }

        private TableLayoutPanel _Layout;
        private System.Windows.Forms.ComboBox _TextComboBox;
        private System.Windows.Forms.TextBox _SecondFieldTextBox;
        private System.Windows.Forms.TextBox _FirstFieldTextBox;
        private Label _SecondFieldTextBoxLabel;
        private Label _FirstFieldTextBoxLabel;
    }
}
