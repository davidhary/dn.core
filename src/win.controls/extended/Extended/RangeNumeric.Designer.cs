
namespace isr.Core.WinControls
{
    partial class RangeNumeric
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._Layout = new System.Windows.Forms.TableLayoutPanel();
            this._LowerNumeric = new isr.Core.WinControls.NumericUpDown();
            this._UpperNumeric = new isr.Core.WinControls.NumericUpDown();
            this._Layout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._LowerNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._UpperNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // _Layout
            // 
            this._Layout.BackColor = System.Drawing.Color.Transparent;
            this._Layout.ColumnCount = 3;
            this._Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this._Layout.Controls.Add(this._LowerNumeric, 0, 1);
            this._Layout.Controls.Add(this._UpperNumeric, 2, 1);
            this._Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._Layout.Location = new System.Drawing.Point(0, 0);
            this._Layout.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._Layout.Name = "_Layout";
            this._Layout.RowCount = 3;
            this._Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._Layout.Size = new System.Drawing.Size(92, 46);
            this._Layout.TabIndex = 1;
            // 
            // _LowerNumeric
            // 
            this._LowerNumeric.Location = new System.Drawing.Point(3, 10);
            this._LowerNumeric.Name = "_LowerNumeric";
            this._LowerNumeric.NullValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this._LowerNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            this._LowerNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            this._LowerNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            this._LowerNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            this._LowerNumeric.Size = new System.Drawing.Size(37, 25);
            this._LowerNumeric.TabIndex = 0;
            this._LowerNumeric.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // _UpperNumeric
            // 
            this._UpperNumeric.Location = new System.Drawing.Point(50, 10);
            this._UpperNumeric.Name = "_UpperNumeric";
            this._UpperNumeric.NullValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this._UpperNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            this._UpperNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            this._UpperNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            this._UpperNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            this._UpperNumeric.Size = new System.Drawing.Size(39, 25);
            this._UpperNumeric.TabIndex = 1;
            this._UpperNumeric.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // RangeNumeric
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._Layout);
            this.Name = "RangeNumeric";
            this.Size = new System.Drawing.Size(92, 46);
            this._Layout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._LowerNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._UpperNumeric)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel _Layout;
        private NumericUpDown _LowerNumeric;
        private NumericUpDown _UpperNumeric;
    }
}
