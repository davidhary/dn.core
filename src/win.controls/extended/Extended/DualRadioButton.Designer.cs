using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.WinControls
{
    public partial class DualRadioButton
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _Layout = new TableLayoutPanel();
            _PrimaryRadioButton = new RadioButton();
            _PrimaryRadioButton.CheckedChanged += new EventHandler(PrimaryRadioButtonCheckedChanged);
            _SecondaryRadioButton = new RadioButton();
            _SecondaryRadioButton.CheckedChanged += new EventHandler(SecondaryRadioButtonCheckedChanged);
            _Layout.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.BackColor = Color.Transparent;
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new ColumnStyle());
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle());
            _Layout.Controls.Add(_SecondaryRadioButton, 2, 1);
            _Layout.Controls.Add(_PrimaryRadioButton, 0, 1);
            _Layout.Dock = DockStyle.Fill;
            _Layout.Location = new Point(0, 0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _Layout.Size = new Size(107, 34);
            _Layout.TabIndex = 0;
            // 
            // _PrimaryRadioButton
            // 
            _PrimaryRadioButton.AutoSize = true;
            _PrimaryRadioButton.Checked = true;
            _PrimaryRadioButton.Dock = DockStyle.Top;
            _PrimaryRadioButton.Location = new Point(3, 8);
            _PrimaryRadioButton.Name = "_PrimaryRadioButton";
            _PrimaryRadioButton.Size = new Size(41, 17);
            _PrimaryRadioButton.TabIndex = 1;
            _PrimaryRadioButton.TabStop = true;
            _PrimaryRadioButton.Text = "ON";
            _PrimaryRadioButton.UseVisualStyleBackColor = true;
            // 
            // _SecondaryRadioButton
            // 
            _SecondaryRadioButton.AutoSize = true;
            _SecondaryRadioButton.Dock = DockStyle.Top;
            _SecondaryRadioButton.Location = new Point(59, 8);
            _SecondaryRadioButton.Name = "_SecondaryRadioButton";
            _SecondaryRadioButton.Size = new Size(45, 17);
            _SecondaryRadioButton.TabIndex = 1;
            _SecondaryRadioButton.Text = "OFF";
            _SecondaryRadioButton.UseVisualStyleBackColor = true;
            // 
            // DualRadioButton
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.Transparent;
            Controls.Add(_Layout);
            Name = "DualRadioButton";
            Size = new Size(107, 34);
            _Layout.ResumeLayout(false);
            _Layout.PerformLayout();
            ResumeLayout(false);
        }

        private TableLayoutPanel _Layout;
        private RadioButton _PrimaryRadioButton;
        private RadioButton _SecondaryRadioButton;
    }
}
