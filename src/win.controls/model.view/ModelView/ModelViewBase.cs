using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace isr.Core.WinControls
{

    /// <summary>
    /// A user control base. Supports property change notifications. Useful for a settings publisher.
    /// </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2010-11-02, 1.2.3988 </para>
    /// </remarks>
    public partial class ModelViewBase : UserControl, INotifyPropertyChanged
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Gets the initializing components sentinel. </summary>
        /// <value> The initializing components sentinel. </value>
        protected bool InitializingComponents { get; set; }

        /// <summary>
        /// A private constructor for this class making it not publicly creatable. This ensure using the
        /// class as a singleton.
        /// </summary>
        protected ModelViewBase() : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.InitializingComponents = false;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM LOAD "

        /// <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            try
            {
                if ( !this.DesignMode )
                {
                    this.InfoProvider.Clear();
                    WinForms.ControlCollectionExtensions.ControlCollectionExtensionMethods.ToolTipSetter( this.Controls, this.ToolTip );
                }
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION"

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " CORE PROPERTIES  "

        /// <summary> Gets the information provider. </summary>
        /// <value> The information provider. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        protected InfoProvider InfoProvider { get; private set; }

        /// <summary> Gets the tool tip. </summary>
        /// <value> The tool tip. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        protected ToolTip ToolTip { get; private set; }

        /// <summary> The status prompt. </summary>
        private string _StatusPrompt;

        /// <summary> The status prompt. </summary>
        /// <value> The status prompt. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusPrompt
        {
            get => this._StatusPrompt;

            set {
                if ( !string.Equals( value, this.StatusPrompt ) )
                {
                    this._StatusPrompt = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " EXCEPTION EVENT HANDLER "

        /// <summary>   Event queue for all listeners interested in <see cref="System.Exception"/> events. </summary>
        public event EventHandler<System.Threading.ThreadExceptionEventArgs> ExceptionEventHandler;

        /// <summary>   Raises the  <see cref="ExceptionEventHandler"/> event. </summary>
        /// <remarks>   David, 2021-05-03. </remarks>
        /// <param name="e">                                Event information to send to registered event
        ///                                                 handlers. </param>
        /// <param name="displayMessageBoxIfNoSubscribers"> (Optional) True to display message box if no
        ///                                                 subscribers. </param>
        protected virtual void OnEventHandlerError( System.Threading.ThreadExceptionEventArgs e, bool displayMessageBoxIfNoSubscribers = true )
        {
            this.ExceptionEventHandler?.Invoke( this, e );
            if ( this.ExceptionEventHandler is not object && displayMessageBoxIfNoSubscribers )
            {
                _ = MessageBox.Show( e.Exception.ToString() );
            }
        }

        /// <summary>   Raises the  <see cref="ExceptionEventHandler"/> event. </summary>
        /// <remarks>   David, 2021-07-29. </remarks>
        /// <param name="exception">                        The exception. </param>
        /// <param name="displayMessageBoxIfNoSubscribers"> (Optional) True to display message box if no
        ///                                                 subscribers. </param>
        protected virtual void OnEventHandlerError( System.Exception exception, bool displayMessageBoxIfNoSubscribers = true )
        {
            this.OnEventHandlerError( new System.Threading.ThreadExceptionEventArgs( exception ), displayMessageBoxIfNoSubscribers );
        }

        #endregion

    }
}
