using System;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.WinControls
{

    /// <summary> A Zope button. </summary>
    /// <remarks>
    /// (c) 2017 Pritam Zope, All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-08, 3.1.6276 </para><para>
    /// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
    /// </para>
    /// </remarks>
    public class ZopeButton : Button
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Button" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ZopeButton() : base()
        {
            this.Size = new Size( 31, 24 );
            this.ForeColor = Color.White;
            this.FlatStyle = FlatStyle.Flat;
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 20.25f, FontStyle.Bold, GraphicsUnit.Point, 0 );
            this.Text = "_";
            this._DisplayText = this.Text;
        }

        /// <summary> The display text. </summary>
        private string _DisplayText = "_";

        /// <summary> Gets or sets the display text. </summary>
        /// <value> The display text. </value>
        public string DisplayText
        {
            get => this._DisplayText;

            set {
                this._DisplayText = value;
                this.Invalidate();
            }
        }

        /// <summary> The busy back color. </summary>
        private Color _BusyBackColor = Color.Teal;

        /// <summary> Gets or sets the color of the busy back. </summary>
        /// <value> The color of the busy back. </value>
        public Color BusyBackColor
        {
            get => this._BusyBackColor;

            set {
                this._BusyBackColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The mouse hover color. </summary>
        private Color _MouseHoverColor = Color.FromArgb( 0, 0, 140 );

        /// <summary> Gets or sets the color of the mouse hover. </summary>
        /// <value> The color of the mouse hover. </value>
        public Color MouseHoverColor
        {
            get => this._MouseHoverColor;

            set {
                this._MouseHoverColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The mouse click color. </summary>
        private Color _MouseClickColor = Color.FromArgb( 160, 180, 200 );

        /// <summary> Gets or sets the color of the mouse click. </summary>
        /// <value> The color of the mouse click. </value>
        public Color MouseClickColor
        {
            get => this._MouseClickColor;

            set {
                this._MouseClickColor = value;
                this.Invalidate();
            }
        }

        /// <summary> True to enable, false to disable the mouse colors. </summary>
        private bool _MouseColorsEnabled = true;

        /// <summary> Gets or sets the mouse colors enabled. </summary>
        /// <value> The mouse colors enabled. </value>
        public bool MouseColorsEnabled
        {
            get => this._MouseColorsEnabled;

            set {
                this._MouseColorsEnabled = value;
                this.Invalidate();
            }
        }

        /// <summary> The text location left. </summary>
        private int _TextLocationLeft = 6;

        /// <summary> Gets or sets the text location left. </summary>
        /// <value> The text location left. </value>
        public int TextLocationLeft
        {
            get => this._TextLocationLeft;

            set {
                this._TextLocationLeft = value;
                this.Invalidate();
            }
        }

        /// <summary> The text location top. </summary>
        private int _TextLocationTop = -20;

        /// <summary> Gets or sets the text location top. </summary>
        /// <value> The text location top. </value>
        public int TextLocationTop
        {
            get => this._TextLocationTop;

            set {
                this._TextLocationTop = value;
                this.Invalidate();
            }
        }

        /// <summary> The cached color. </summary>
        private Color _CachedColor;

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.OnMouseEnter(System.EventArgs)" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseEnter( EventArgs e )
        {
            base.OnMouseEnter( e );
            this._CachedColor = this.BusyBackColor;
            this._BusyBackColor = this.MouseHoverColor;
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.OnMouseLeave(System.EventArgs)" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseLeave( EventArgs e )
        {
            base.OnMouseLeave( e );
            if ( this._MouseColorsEnabled )
            {
                this._BusyBackColor = this._CachedColor;
            }
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.Control.OnMouseDown(System.Windows.Forms.MouseEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseDown( MouseEventArgs mevent )
        {
            base.OnMouseDown( mevent );
            if ( this.MouseColorsEnabled )
            {
                this._BusyBackColor = this.MouseClickColor;
            }
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.ButtonBase.OnMouseUp(System.Windows.Forms.MouseEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseUp( MouseEventArgs mevent )
        {
            base.OnMouseUp( mevent );
            if ( this.MouseColorsEnabled )
            {
                this._BusyBackColor = this._CachedColor;
            }
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.ButtonBase.OnPaint(System.Windows.Forms.PaintEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="pevent"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs pevent )
        {
            if ( pevent is null )
            {
                return;
            }

            base.OnPaint( pevent );
            this._DisplayText = this.Text;
            if ( this._TextLocationLeft == 100 && this._TextLocationTop == 25 )
            {
                this._TextLocationLeft = this.Width / 3 + 10;
                this._TextLocationTop = this.Height / 2 - 1;
            }

            var p = new Point( this.TextLocationLeft, this.TextLocationTop );
            using ( var b = new SolidBrush( this._BusyBackColor ) )
            {
                pevent.Graphics.FillRectangle( b, this.ClientRectangle );
            }

            using ( var b = new SolidBrush( this.ForeColor ) )
            {
                pevent.Graphics.DrawString( this.DisplayText, this.Font, b, p );
            }
        }
    }
}
