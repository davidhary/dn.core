using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace isr.Core.WinControls
{

    /// <summary> A shaped button. </summary>
    /// <remarks>
    /// (c) 2017 Pritam Zope, All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-08, 3.1.6276 </para><para>
    /// https://www.codeproject.com/Articles/1068043/Creating-Custom-Windows-Forms-in-Csharp-using-Pane.
    /// </para>
    /// </remarks>
    public class ShapedButton : Button
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Button" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public ShapedButton()
        {
            this.Size = new Size( 100, 40 );
            this.BackColor = Color.Transparent;
            this.FlatStyle = FlatStyle.Flat;
            this.FlatAppearance.BorderSize = 0;
            this.FlatAppearance.MouseOverBackColor = Color.Transparent;
            this.FlatAppearance.MouseDownBackColor = Color.Transparent;
            this._ButtonText = this.Text;
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 20.25f, FontStyle.Bold, GraphicsUnit.Point, 0 );
        }

        #endregion

        #region " SHAPE "

        /// <summary> The radius percent. </summary>
        private int _RadiusPercent = 25;

        /// <summary> Gets or sets the radius percent. </summary>
        /// <value> The radius percent. </value>
        public int RadiusPercent
        {
            get => this._RadiusPercent;

            set {
                this._RadiusPercent = value;
                this.Invalidate();
            }
        }

        /// <summary> The button shape. </summary>
        private ButtonShape _ButtonShape;

        /// <summary> Gets or sets the button shape. </summary>
        /// <value> The button shape. </value>
        public ButtonShape ButtonShape
        {
            get => this._ButtonShape;

            set {
                this._ButtonShape = value;
                this.Invalidate();
            }
        }

        #endregion

        #region " TEXT "

        /// <summary> The button text. </summary>
        private string _ButtonText = string.Empty;

        /// <summary> Gets or sets the button text. </summary>
        /// <value> The button text. </value>
        public string ButtonText
        {
            get => this._ButtonText;

            set {
                this._ButtonText = value;
                this.Invalidate();
            }
        }

        /// <summary> The text location. </summary>
        private Point _TextLocation = new ( 100, 25 );

        /// <summary> Gets or sets the text location. </summary>
        /// <value> The text location. </value>
        public Point TextLocation
        {
            get => this._TextLocation;

            set {
                this._TextLocation = value;
                this.Invalidate();
            }
        }

        /// <summary> True to show, false to hide the button text. </summary>
        private bool _ShowButtonText = true;

        /// <summary> Gets or sets the show button text. </summary>
        /// <value> The show button text. </value>
        public bool ShowButtonText
        {
            get => this._ShowButtonText;

            set {
                this._ShowButtonText = value;
                this.Invalidate();
            }
        }

        #endregion

        #region " BORDER "

        /// <summary> Width of the border. </summary>
        private int _BorderWidth = 2;

        /// <summary> Gets or sets the width of the border. </summary>
        /// <value> The width of the border. </value>
        public int BorderWidth
        {
            get => this._BorderWidth;

            set {
                this._BorderWidth = value;
                this.Invalidate();
            }
        }

        /// <summary> Sets border color. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="bdrColor"> The bdr color. </param>
        private void SetBorderColor( Color bdrColor )
        {
            int red = bdrColor.R - 40;
            int green = bdrColor.G - 40;
            int blue = bdrColor.B - 40;
            if ( red < 0 )
            {
                red = 0;
            }

            if ( green < 0 )
            {
                green = 0;
            }

            if ( blue < 0 )
            {
                blue = 0;
            }

            this._ButtonBorderColor = Color.FromArgb( red, green, blue );
        }

        /// <summary> The button border color. </summary>

        private Color _ButtonBorderColor = Color.FromArgb( 220, 220, 220 );

        /// <summary> The border color. </summary>
        private Color _BorderColor = Color.Transparent;

        /// <summary> Gets or sets the color of the border. </summary>
        /// <value> The color of the border. </value>
        public Color BorderColor
        {
            get => this._BorderColor;

            set {
                this._BorderColor = value;
                if ( this._BorderColor == Color.Transparent )
                {
                    this._ButtonBorderColor = Color.FromArgb( 220, 220, 220 );
                }
                else
                {
                    this.SetBorderColor( this._BorderColor );
                }
            }
        }

        #endregion

        #region " BUTTON COLORS "

        /// <summary> The start color. </summary>
        private Color _StartColor = Color.DodgerBlue;

        /// <summary> Gets or sets the color of the start. </summary>
        /// <value> The color of the start. </value>
        public Color StartColor
        {
            get => this._StartColor;

            set {
                this._StartColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The end color. </summary>
        private Color _EndColor = Color.MidnightBlue;

        /// <summary> Gets or sets the color of the end. </summary>
        /// <value> The color of the end. </value>
        public Color EndColor
        {
            get => this._EndColor;

            set {
                this._EndColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The mouse hover start color. </summary>
        private Color _MouseHoverStartColor = Color.Turquoise;

        /// <summary> Gets or sets the color of the mouse hover start. </summary>
        /// <value> The color of the mouse hover start. </value>
        public Color MouseHoverStartColor
        {
            get => this._MouseHoverStartColor;

            set {
                this._MouseHoverStartColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The mouse hover end color. </summary>
        private Color _MouseHoverEndColor = Color.DarkSlateGray;

        /// <summary> Gets or sets the color of the mouse hover end. </summary>
        /// <value> The color of the mouse hover end. </value>
        public Color MouseHoverEndColor
        {
            get => this._MouseHoverEndColor;

            set {
                this._MouseHoverEndColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The mouse click start color. </summary>
        private Color _MouseClickStartColor = Color.Yellow;

        /// <summary> Gets or sets the color of the mouse click start. </summary>
        /// <value> The color of the mouse click start. </value>
        public Color MouseClickStartColor
        {
            get => this._MouseClickStartColor;

            set {
                this._MouseClickStartColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The mouse click end color. </summary>
        private Color _MouseClickEndColor = Color.Red;

        /// <summary> Gets or sets the color of the mouse click end. </summary>
        /// <value> The color of the mouse click end. </value>
        public Color MouseClickEndColor
        {
            get => this._MouseClickEndColor;

            set {
                this._MouseClickEndColor = value;
                this.Invalidate();
            }
        }

        /// <summary> The start opacity. </summary>
        private int _StartOpacity = 250;

        /// <summary> Gets or sets the start opacity. </summary>
        /// <value> The start opacity. </value>
        public int StartOpacity
        {
            get => this._StartOpacity;

            set {
                this._StartOpacity = value;
                if ( this._StartOpacity > 255 )
                {
                    this._StartOpacity = 255;
                    this.Invalidate();
                }
                else
                {
                    this.Invalidate();
                }
            }
        }

        /// <summary> The end opacity. </summary>
        private int _EndOpacity = 250;

        /// <summary> Gets or sets the end opacity. </summary>
        /// <value> The end opacity. </value>
        public int EndOpacity
        {
            get => this._EndOpacity;

            set {
                this._EndOpacity = value;
                if ( this._EndOpacity > 255 )
                {
                    this._EndOpacity = 255;
                    this.Invalidate();
                }
                else
                {
                    this.Invalidate();
                }
            }
        }

        /// <summary> The gradient angle. </summary>
        private int _GradientAngle = 90;

        /// <summary> Gets or sets the gradient angle. </summary>
        /// <value> The gradient angle. </value>
        public int GradientAngle
        {
            get => this._GradientAngle;

            set {
                this._GradientAngle = value;
                this.Invalidate();
            }
        }

        /// <summary> The cached start color. </summary>

        private Color _CachedStartColor, _CachedEndColor;

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.OnMouseEnter(System.EventArgs)" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseEnter( EventArgs e )
        {
            base.OnMouseEnter( e );
            this._CachedStartColor = this._StartColor;
            this._CachedEndColor = this._EndColor;
            this._StartColor = this._MouseHoverStartColor;
            this._EndColor = this._MouseHoverEndColor;
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.Control.OnMouseLeave(System.EventArgs)" /> event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnMouseLeave( EventArgs e )
        {
            base.OnMouseLeave( e );
            this._StartColor = this._CachedStartColor;
            this._EndColor = this._CachedEndColor;
            this.SetBorderColor( this._BorderColor );
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.Control.OnMouseDown(System.Windows.Forms.MouseEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseDown( MouseEventArgs mevent )
        {
            base.OnMouseDown( mevent );
            this._StartColor = this._MouseClickStartColor;
            this._EndColor = this._MouseClickEndColor;
            this._ButtonBorderColor = this._BorderColor;
            this.SetBorderColor( this._BorderColor );
            this.Invalidate();
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.ButtonBase.OnMouseUp(System.Windows.Forms.MouseEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="mevent"> A <see cref="T:System.Windows.Forms.MouseEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnMouseUp( MouseEventArgs mevent )
        {
            base.OnMouseUp( mevent );
            this.OnMouseLeave( mevent );
            this._StartColor = this._CachedStartColor;
            this._EndColor = this._CachedEndColor;
            this.SetBorderColor( this._BorderColor );
            this.Invalidate();
        }

        /// <summary>
        /// Raises the <see cref="M:System.Windows.Forms.ButtonBase.OnLostFocus(System.EventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLostFocus( EventArgs e )
        {
            base.OnLostFocus( e );
            this._StartColor = this._CachedStartColor;
            this._EndColor = this._CachedEndColor;
            this.Invalidate();
        }

        #endregion

        #region " RENDER "

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Resize" /> event. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnResize( EventArgs e )
        {
            base.OnResize( e );
            this.TextLocation = new Point( this.Width / 3 - 1, this.Height / 3 + 5 );
        }

        /// <summary> Draw circular button. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g"> The Graphics to process. </param>
        private void DrawCircularButton( Graphics g )
        {
            var c1 = Color.FromArgb( this._StartOpacity, this._StartColor );
            var c2 = Color.FromArgb( this._EndOpacity, this._EndColor );
            using ( Brush b = new LinearGradientBrush( this.ClientRectangle, c1, c2, this._GradientAngle ) )
            {
                g.FillEllipse( b, 5, 5, this.Width - 10, this.Height - 10 );
            }

            using ( var b = new SolidBrush( this._ButtonBorderColor ) )
            {
                using var p = new Pen( b );
                for ( int i = 0, loopTo = this._BorderWidth - 1; i <= loopTo; i++ )
                {
                    g.DrawEllipse( p, 5 + i, 5, this.Width - 10, this.Height - 10 );
                }
            }

            if ( this._ShowButtonText )
            {
                var p = new Point( this.TextLocation.X, this.TextLocation.Y );
                using var sb = new SolidBrush( this.ForeColor );
                g.DrawString( this._ButtonText, this.Font, sb, p );
            }
        }

        /// <summary> Draw round rectangular button. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="g"> The Graphics to process. </param>
        private void DrawRoundRectangularButton( Graphics g )
        {
            var c1 = Color.FromArgb( this._StartOpacity, this._StartColor );
            var c2 = Color.FromArgb( this._EndOpacity, this._EndColor );
            var rec = new Rectangle( 5, 5, this.Width, this.Height );

            // create the radius variable and set it equal to 20% the height of the rectangle
            // this will determine the amount of bend at the corners
            int radius = ( int ) Math.Truncate( 0.01d * Math.Min( rec.Height, rec.Height ) * this.RadiusPercent );

            // make sure that we have a valid radius, too small and we have a problem
            if ( radius < 1 )
            {
                radius = 2;
            }

            int diameter = radius * 2;

            // create an x and y variable so that we can reduce the length of our code lines
            int x = rec.Left;
            int y = rec.Top;
            int h = rec.Height;
            int w = rec.Width;
            using ( var _region = new Region( rec ) )
            {
                using ( var path = new GraphicsPath() )
                {
                    path.AddArc( x, y, diameter, diameter, 180f, 90f );
                    path.AddArc( w - diameter - x, y, diameter, diameter, 270f, 90f );
                    path.AddArc( w - diameter - x, h - diameter - y, diameter, diameter, 0f, 90f );
                    path.AddArc( x, h - diameter - y, diameter, diameter, 90f, 90f );
                    path.CloseFigure();
                    _region.Intersect( path );
                }

                using Brush b = new LinearGradientBrush( this.ClientRectangle, c1, c2, this._GradientAngle );
                g.FillRegion( b, _region );
            }

            using ( var p = new Pen( this._ButtonBorderColor ) )
            {
                for ( int i = 0, loopTo = this._BorderWidth - 1; i <= loopTo; i++ )
                {
                    g.DrawArc( p, x + i, y + i, diameter, diameter, 180, 90 );
                    g.DrawLine( p, x + radius, y + i, this.Width - x - radius, y + i );
                    g.DrawArc( p, this.Width - diameter - x - i, y + i, diameter, diameter, 270, 90 );
                    g.DrawLine( p, x + i, y + radius, x + i, this.Height - y - radius );
                    g.DrawLine( p, this.Width - x - i, y + radius, this.Width - x - i, this.Height - y - radius );
                    g.DrawArc( p, this.Width - diameter - x - i, this.Height - diameter - y - i, diameter, diameter, 0, 90 );
                    g.DrawLine( p, x + radius, this.Height - y - i, this.Width - x - radius, this.Height - y - i );
                    g.DrawArc( p, x + i, this.Height - diameter - y - i, diameter, diameter, 90, 90 );
                }
            }

            if ( this._ShowButtonText )
            {
                using var sb = new SolidBrush( this.ForeColor );
                g.DrawString( this.ButtonText, this.Font, sb, this.TextLocation );
            }
        }

        /// <summary>
        /// Raises the
        /// <see cref="M:System.Windows.Forms.ButtonBase.OnPaint(System.Windows.Forms.PaintEventArgs)" />
        /// event.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
            {
                return;
            }

            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            base.OnPaint( e );
            switch ( this._ButtonShape )
            {
                case ButtonShape.Circle:
                    {
                        this.DrawCircularButton( e.Graphics );
                        break;
                    }

                case ButtonShape.RoundRect:
                    {
                        this.DrawRoundRectangularButton( e.Graphics );
                        break;
                    }
            }
        }

        #endregion

    }

    /// <summary> Values that represent button shapes. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public enum ButtonShape
    {

        /// <summary> An enum constant representing the round Rectangle option. </summary>
        RoundRect,

        /// <summary> An enum constant representing the circle option. </summary>
        Circle
    }
}
