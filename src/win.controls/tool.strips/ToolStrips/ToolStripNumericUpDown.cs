using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace isr.Core.WinControls
{

    /// <summary> Tool strip numeric up down. </summary>
    /// <remarks> David, 2014-04-16. </remarks>
    [ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
    public class ToolStripNumericUpDown : ToolStripControlHost
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> Call the base constructor passing in a NumericUpDown instance. </remarks>
        public ToolStripNumericUpDown() : base( new NumericUpDown() )
        {
        }

        #endregion

        #region " NUMERIC UP DOWN "

        /// <summary> Gets the numeric up down control. </summary>
        /// <value> The numeric up down control. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
        public NumericUpDown NumericUpDown => ( NumericUpDown ) this.Control;

        /// <summary> Gets or sets the selected text. </summary>
        /// <value> The selected text. </value>
        [DefaultValue( "" )]
        [Description( "text" )]
        [Category( "Appearance" )]
        public override string Text
        {
            get => this.NumericUpDown.Text;

            set => SafeSetter( this.NumericUpDown, () => this.NumericUpDown.Text = value );
        }

        /// <summary> Safe setter. </summary>
        /// <remarks> David, 2014-04-16. </remarks>
        /// <param name="control"> The control from which to subscribe events. </param>
        /// <param name="setter">  The setter. </param>
        private static void SafeSetter( Control control, Action setter )
        {
            if ( control is object && control.Parent is object )
            {
                if ( control.Parent.InvokeRequired )
                {
                    _ = control.Parent.BeginInvoke( new Action<Control, Action>( SafeSetter ), new object[] { control, setter } );
                }
                else if ( control.Parent.IsHandleCreated )
                {
                    setter.Invoke();
                }
            }
        }

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        [DefaultValue( "" )]
        [Description( "Value" )]
        [Category( "Appearance" )]
        public decimal Value
        {
            get => this.NumericUpDown.Value;

            set => SafeSetter( this.NumericUpDown, () => this.NumericUpDown.Value = value );
        }

        /// <summary> Gets a value indicating whether this object has value. </summary>
        /// <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool HasValue => this.NumericUpDown.HasValue();

        #endregion

    }
}
