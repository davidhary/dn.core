using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace isr.Core.WinControls
{
    /// <summary> Tool strip range number box. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-08. </para>
    /// </remarks>
    [ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
    public class ToolStripRangeNumberBox : ToolStripControlHost
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> Call the base constructor passing in a NumberBox instance. </remarks>
        public ToolStripRangeNumberBox() : base( new RangeNumberBox() )
        {
        }

        #endregion

        #region " RANGE NUMBER BOX "

        /// <summary> Gets the numeric up down control. </summary>
        /// <value> The numeric up down control. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
        public RangeNumberBox RangeNumberBox => ( RangeNumberBox ) this.Control;

        /// <summary> Standard Numeric Format String D,E,F,N,X, B for binary. </summary>
        /// <value> The Standard numeric format string </value>
        [Category( "RangeNumberBox" ), Description( "Standard Numeric Format String D,E,F,N,X, B for binary" )]
        public string Snfs
        {
            get => this.RangeNumberBox.Snfs;
            set => this.RangeNumberBox.Snfs = value;
        }

        /// <summary>   Prefix for X and B formats. </summary>
        /// <value> The prefix. </value>
        [Category( "RangeNumberBox" ), Description( "Prefix for X and B formats" )]
        public string Prefix
        {
            get => this.RangeNumberBox.Prefix;
            set => this.RangeNumberBox.Prefix = value;
        }

        /// <summary>   Suffix for X and B formats. </summary>
        /// <value> The suffix. </value>
        [Category( "RangeNumberBox" ), Description( "Suffix for X and B formats" )]
        public string Suffix
        {
            get => this.RangeNumberBox.Suffix;
            set => this.RangeNumberBox.Suffix = value;
        }

        /// <summary>   Gets/sets Max Value...double. </summary>
        /// <value> The maximum value. </value>
        [Category( "RangeNumberBox" ), Description( "gets/sets Max Value...double" )]
        public double Maximum
        {
            get => this.RangeNumberBox.Maximum;
            set => this.RangeNumberBox.Maximum = value;
        }


        /// <summary>   Gets/sets Min Value...double. </summary>
        /// <value> The minimum value. </value>
        [Category( "RangeNumberBox" ), Description( "gets/sets Min Value...double" )]
        public double Minimum
        {
            get => this.RangeNumberBox.Minimum;
            set => this.RangeNumberBox.Minimum = value;
        }

        #endregion

        #region " LOWER RANGE NUMBER BOX "

        /// <summary>   Gets/sets the Lower Value As Double. </summary>
        /// <value> The value as double. </value>
        [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As Double" ), Browsable( true )]
        public double LowerValueAsDouble
        {
            get => this.RangeNumberBox.LowerNumberBox.ValueAsDouble;
            set => this.RangeNumberBox.LowerNumberBox.ValueAsDouble = value;
        }

        /// <summary>   Gets/sets the Lower Value As Float. </summary>
        /// <value> The value as float. </value>
        [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As Float" ), Browsable( true )]
        public float LowerValueAsFloat
        {
            get => this.RangeNumberBox.LowerNumberBox.ValueAsFloat;
            set => this.RangeNumberBox.LowerNumberBox.ValueAsFloat = value;
        }

        /// <summary>   Gets/sets the Lower Value As Byte. </summary>
        /// <value> The value as byte. </value>
        [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As Byte" ), Browsable( true )]
        public byte LowerValueAsByte
        {
            get => this.RangeNumberBox.LowerNumberBox.ValueAsByte;
            set => this.RangeNumberBox.LowerNumberBox.ValueAsByte = value;
        }

        /// <summary>   Gets/sets the Lower Value As Signed 8-bit integer. </summary>
        /// <value> The value as signed 8-bit integer. </value>
        [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As SByte" ), Browsable( true ), CLSCompliant( false )]
        public sbyte LowerValueAsSByte
        {
            get => this.RangeNumberBox.LowerNumberBox.ValueAsSByte;
            set => this.RangeNumberBox.LowerNumberBox.ValueAsSByte = value;
        }

        /// <summary>   Gets/sets the Lower Value As UInt64. </summary>
        /// <value> The value as unsigned int 64. </value>
        [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As UInt64" ), Browsable( true ), CLSCompliant( false )]
        public ulong LowerValueAsUInt64
        {
            get => this.RangeNumberBox.LowerNumberBox.ValueAsUInt64;
            set => this.RangeNumberBox.LowerNumberBox.ValueAsUInt64 = value;
        }

        /// <summary>   Gets/sets the Lower Value As UInt32. </summary>
        /// <value> The value as unsigned int 32. </value>
        [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As UInt32" ), Browsable( true ), CLSCompliant( false )]
        public uint LowerValueAsUInt32
        {
            get => this.RangeNumberBox.LowerNumberBox.ValueAsUInt32;
            set => this.RangeNumberBox.LowerNumberBox.ValueAsUInt32 = value;
        }

        /// <summary>   Gets/sets the Lower Value As UInt16. </summary>
        /// <value> The value as unsigned int 16. </value>
        [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As UInt16" ), Browsable( true ), CLSCompliant( false )]
        public ushort LowerValueAsUInt16
        {
            get => this.RangeNumberBox.LowerNumberBox.ValueAsUInt16;
            set => this.RangeNumberBox.LowerNumberBox.ValueAsUInt16 = value;
        }

        /// <summary>   Gets/sets the Lower Value As Int64. </summary>
        /// <value> The value as int 64. </value>
        [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As Int64" ), Browsable( true )]
        public long LowerValueAsInt64
        {
            get => this.RangeNumberBox.LowerNumberBox.ValueAsInt64;
            set => this.RangeNumberBox.LowerNumberBox.ValueAsInt64 = value;
        }

        /// <summary>   Gets/sets the Lower Value As Int32. </summary>
        /// <value> The value as int 32. </value>
        [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As Int32" ), Browsable( true )]
        public int LowerValueAsInt32
        {
            get => this.RangeNumberBox.LowerNumberBox.ValueAsInt32;
            set => this.RangeNumberBox.LowerNumberBox.ValueAsInt32 = value;
        }

        /// <summary>   Gets/sets the Lower Value As Int16. </summary>
        /// <value> The value as int 16. </value>
        [Category( "LowerNumberBox" ), Description( "gets/sets the Lower Value As Int16" ), Browsable( true )]
        public short LowerValueAsInt16
        {
            get => this.RangeNumberBox.LowerNumberBox.ValueAsInt16;
            set => this.RangeNumberBox.LowerNumberBox.ValueAsInt16 = value;
        }

        #endregion

        #region " UPPER RANGE NUMBER BOX "

        /// <summary>   Gets/sets the Upper Value As Double. </summary>
        /// <value> The value as double. </value>
        [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As Double" ), Browsable( true )]
        public double UpperValueAsDouble
        {
            get => this.RangeNumberBox.UpperNumberBox.ValueAsDouble;
            set => this.RangeNumberBox.UpperNumberBox.ValueAsDouble = value;
        }

        /// <summary>   Gets/sets the Upper Value As Float. </summary>
        /// <value> The value as float. </value>
        [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As Float" ), Browsable( true )]
        public float UpperValueAsFloat
        {
            get => this.RangeNumberBox.UpperNumberBox.ValueAsFloat;
            set => this.RangeNumberBox.UpperNumberBox.ValueAsFloat = value;
        }

        /// <summary>   Gets/sets the Upper Value As Byte. </summary>
        /// <value> The value as byte. </value>
        [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As Byte" ), Browsable( true )]
        public byte UpperValueAsByte
        {
            get => this.RangeNumberBox.UpperNumberBox.ValueAsByte;
            set => this.RangeNumberBox.UpperNumberBox.ValueAsByte = value;
        }

        /// <summary>   Gets/sets the Upper Value As Signed 8-bit integer. </summary>
        /// <value> The value as signed 8-bit integer. </value>
        [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As SByte" ), Browsable( true ), CLSCompliant( false )]
        public sbyte UpperValueAsSByte
        {
            get => this.RangeNumberBox.UpperNumberBox.ValueAsSByte;
            set => this.RangeNumberBox.UpperNumberBox.ValueAsSByte = value;
        }

        /// <summary>   Gets/sets the Upper Value As UInt64. </summary>
        /// <value> The value as unsigned int 64. </value>
        [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As UInt64" ), Browsable( true ), CLSCompliant( false )]
        public ulong UpperValueAsUInt64
        {
            get => this.RangeNumberBox.UpperNumberBox.ValueAsUInt64;
            set => this.RangeNumberBox.UpperNumberBox.ValueAsUInt64 = value;
        }

        /// <summary>   Gets/sets the Upper Value As UInt32. </summary>
        /// <value> The value as unsigned int 32. </value>
        [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As UInt32" ), Browsable( true ), CLSCompliant( false )]
        public uint UpperValueAsUInt32
        {
            get => this.RangeNumberBox.UpperNumberBox.ValueAsUInt32;
            set => this.RangeNumberBox.UpperNumberBox.ValueAsUInt32 = value;
        }

        /// <summary>   Gets/sets the Upper Value As UInt16. </summary>
        /// <value> The value as unsigned int 16. </value>
        [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As UInt16" ), Browsable( true ), CLSCompliant( false )]
        public ushort UpperValueAsUInt16
        {
            get => this.RangeNumberBox.UpperNumberBox.ValueAsUInt16;
            set => this.RangeNumberBox.UpperNumberBox.ValueAsUInt16 = value;
        }

        /// <summary>   Gets/sets the Upper Value As Int64. </summary>
        /// <value> The value as int 64. </value>
        [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As Int64" ), Browsable( true )]
        public long UpperValueAsInt64
        {
            get => this.RangeNumberBox.UpperNumberBox.ValueAsInt64;
            set => this.RangeNumberBox.UpperNumberBox.ValueAsInt64 = value;
        }

        /// <summary>   Gets/sets the Upper Value As Int32. </summary>
        /// <value> The value as int 32. </value>
        [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As Int32" ), Browsable( true )]
        public int UpperValueAsInt32
        {
            get => this.RangeNumberBox.UpperNumberBox.ValueAsInt32;
            set => this.RangeNumberBox.UpperNumberBox.ValueAsInt32 = value;
        }

        /// <summary>   Gets/sets the Upper Value As Int16. </summary>
        /// <value> The value as int 16. </value>
        [Category( "UpperNumberBox" ), Description( "gets/sets the Upper Value As Int16" ), Browsable( true )]
        public short UpperValueAsInt16
        {
            get => this.RangeNumberBox.UpperNumberBox.ValueAsInt16;
            set => this.RangeNumberBox.UpperNumberBox.ValueAsInt16 = value;
        }

        #endregion

    }
}
