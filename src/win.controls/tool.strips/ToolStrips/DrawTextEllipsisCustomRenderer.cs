using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.WinControls.ToolStrips
{
    /// <summary>   A draw text ellipsis custom renderer. </summary>
    /// <remarks>   https://stackoverflow.com/questions/38155313/statusstrip-label-not-visible-when-text-too-long </remarks>
    public class DrawTextEllipsisCustomRenderer : ToolStripProfessionalRenderer
    {
        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderItemText" /> event.
        /// </summary>
        /// <remarks>   https://stackoverflow.com/questions/38155313/statusstrip-label-not-visible-when-text-too-long </remarks>
        /// <param name="e">    A <see cref="T:System.Windows.Forms.ToolStripItemTextRenderEventArgs" />
        ///                     that contains the event data. </param>
        protected override void OnRenderItemText( ToolStripItemTextRenderEventArgs e )
        {
            if ( e.Item is ToolStripStatusLabel )
                TextRenderer.DrawText( e.Graphics, e.Text, e.TextFont,
                    e.TextRectangle, e.TextColor, Color.Transparent,
                    e.TextFormat | TextFormatFlags.EndEllipsis );
            else
                base.OnRenderItemText( e );
        }
    }

}
