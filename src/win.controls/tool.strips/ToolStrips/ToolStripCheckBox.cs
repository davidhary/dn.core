using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace isr.Core.WinControls
{

    /// <summary> Tool strip check box. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-01-23 </para>
    /// </remarks>
    [ToolStripItemDesignerAvailability( ToolStripItemDesignerAvailability.ToolStrip )]
    public class ToolStripCheckBox : ToolStripControlHost
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> Call the base constructor passing in a CheckBox instance. </remarks>
        public ToolStripCheckBox() : base( new CheckBox() )
        {
        }

        #endregion

        #region " CHECK BOX "

        /// <summary>   Gets the check box. </summary>
        /// <value> The check box. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
        public CheckBox CheckBox => ( CheckBox ) this.Control;

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public bool Checked
        {
            get => this.CheckBox.Checked;

            set => this.CheckBox.Checked = value;
        }

        #endregion

    }
}
