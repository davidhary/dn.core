using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Core.WinControls
{

    /// <summary>   User log in/out interface. </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2008-10-30, 1.00.3225 </para>
    /// </remarks>
    [Description( "User Log On Control" )]
    [ToolboxBitmap( typeof( LoginControl ) )]
    [System.Runtime.InteropServices.ComVisible( false )]
    public partial class LoginControl : UserControl
    {

        #region " CONSTRUCTION "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        public LoginControl()
        {

            // This call is required by the designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            this._ErrorProvider = new ErrorProvider( this.components );
            (( ISupportInitialize ) this._ErrorProvider).BeginInit();
            this._ErrorProvider.ContainerControl = this;
            (( ISupportInitialize ) this._ErrorProvider).EndInit();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.Core.WinControls.LoginControl and optionally
        /// releases the managed resources.
        /// </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="disposing">    true to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    if ( this._Popup is object )
                    {
                        this._Popup.Dispose();
                        this._Popup = null;
                    }

                    this._UserLogin = null;
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " USER ACTIONS "

        /// <summary>   True if is authenticated, false if not. </summary>
        private bool _IsAuthenticated;

        /// <summary>   Gets or sets a value indicating whether this object is authenticated. </summary>
        /// <value> <c>true</c> if this object is Authenticated; otherwise <c>false</c>. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool IsAuthenticated
        {
            get => this._IsAuthenticated;

            set {
                this._IsAuthenticated = value;
                this.UpdateCaptions();
            }
        }

        /// <summary>   The user login. </summary>
        private LoginBase _UserLogin;

        /// <summary>   Gets or sets reference to the user login implementation object. </summary>
        /// <value> The user login. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public LoginBase UserLogin
        {
            get => this._UserLogin;

            set {
                this._UserLogin = value;
                this.IsAuthenticated = false;
            }
        }

        #endregion

        #region " ALLOWED USER ROLES "

        /// <summary> The allowed user roles. </summary>

        /// <summary>   Gets or sets the allowed user roles. </summary>
        /// <value> The allowed user roles. </value>
        public ArrayList AllowedUserRoles { get; private set; }

        /// <summary>   Adds a user role to the list of allowed user roles. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="value">    Specifies a user role to add to the list of allowed user roles. </param>
        public void AddAllowedUserRole( string value )
        {
            if ( this.AllowedUserRoles is null )
            {
                this.AllowedUserRoles = new ArrayList();
            }

            if ( !this.AllowedUserRoles.Contains( value ) )
            {
                _ = this.AllowedUserRoles.Add( value );
            }
        }

        /// <summary>   Adds a user roles. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="values">   The values. </param>
        public void AddAllowedUserRoles( string[] values )
        {
            if ( this.AllowedUserRoles is null )
            {
                this.AllowedUserRoles = new ArrayList();
            }

            this.AllowedUserRoles.AddRange( values );
        }

        /// <summary>   Clears the allowed user roles. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        public void ClearAllowedUserRoles()
        {
            this.AllowedUserRoles = new ArrayList();
        }

        /// <summary>   Authenticated roles. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <returns>   An ArrayList. </returns>
        public ArrayList AuthenticatedRoles()
        {
            return DomainLogin.EnumerateUserRoles( this.UserLogin.UserRoles, this.AllowedUserRoles );
        }

        /// <summary>   Returns true if the specified role is allowed. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="value">    Specifies a user role to check against the list of allowed user
        ///                         roles. </param>
        /// <returns>   <c>true</c> if user role allowed; otherwise <c>false</c>. </returns>
        public bool IsUserRoleAllowed( string value )
        {
            return this.AllowedUserRoles is object && this.AllowedUserRoles.Contains( value );
        }

        #endregion

        #region " GUI "

        /// <summary>   Logs the user on. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        private void LoginThis()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                System.Net.NetworkCredential userCredential = new( this._UserNameTextBox.Text.Trim(), this._PasswordTextBox.Text.Trim() );
                this.IsAuthenticated = this.AllowedUserRoles is null || this.AllowedUserRoles.Count == 0
                                       ? this.UserLogin.Authenticate( userCredential )
                                       : this.UserLogin.Authenticate( userCredential, this.AllowedUserRoles );
                if ( !this.IsAuthenticated )
                {
                    this._ErrorProvider.SetError( this._LoginLinkLabel, this.UserLogin.ValidationMessage );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>   Logs on link label link clicked. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Link label link clicked event information. </param>
        private void LoginLinkLabel_LinkClicked( object sender, LinkLabelLinkClickedEventArgs e )
        {
            this._ErrorProvider.SetError( this._LoginLinkLabel, string.Empty );
            if ( this.IsAuthenticated )
            {
                this.IsAuthenticated = false;
                this.UserLogin.Invalidate();
            }
            else
            {
                this.LoginThis();
            }

            if ( this._Popup is object )
            {
                this._Popup.Hide();
            }
        }

        /// <summary>   The login caption. </summary>
        private string _LoginCaption = "Log In";

        /// <summary>   Gets or sets the login caption. </summary>
        /// <value> The login caption. </value>
        [Category( "Appearance" )]
        [Description( "Caption for login" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Log In" )]
        public string LoginCaption
        {
            get => this._LoginCaption;

            set {
                this._LoginCaption = value;
                this.UpdateCaptions();
            }
        }

        /// <summary>   The log out caption. </summary>
        private string _LogOffCaption = "Log Out";

        /// <summary>   Gets or sets the log out caption. </summary>
        /// <value> The log out caption. </value>
        [Category( "Appearance" )]
        [Description( "Caption for log out" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( "Log Out" )]
        public string LogOffCaption
        {
            get => this._LogOffCaption;

            set {
                this._LogOffCaption = value;
                this.UpdateCaptions();
            }
        }

        /// <summary>   Initializes the user interface and tool tips. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void UserLoginControl_Load( object sender, EventArgs e )
        {

            // disable when loaded.
            this._UserNameTextBox.Enabled = false;
            this._UserNameTextBox.Text = string.Empty;
            this._PasswordTextBox.Enabled = false;
            this._UserNameTextBox.Text = string.Empty;
            this._LoginLinkLabel.Enabled = false;
            this._LoginLinkLabel.Text = this.LoginCaption;
        }

        /// <summary>   Logs on control visible changed. </summary>
        /// <remarks>   This in fact enables the control when in a pop-up container. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void LoginControl_VisibleChanged( object sender, EventArgs e )
        {
            this.UpdateCaptions();
        }

        /// <summary>   Updates the captions. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        private void UpdateCaptions()
        {
            this._LoginLinkLabel.Text = this._IsAuthenticated ? this.LogOffCaption : this.LoginCaption;
            if ( this._UserLogin is object )
            {
                // enable the user name and password boxes
                this._UserNameTextBox.Enabled = true;
                this._PasswordTextBox.Enabled = true;
                this._LoginLinkLabel.Enabled = true;
            }
        }

        #endregion

        #region " POPUP "

        /// <summary>   The popup. </summary>
        private Login.PopupContainer _Popup;

        /// <summary>   Shows the pop-up. </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="control">  The control. </param>
        /// <param name="location"> The location. </param>
        public void ShowPopup( Control control, Point location )
        {
            this._Popup = new Login.PopupContainer( this );
            this._Popup.Show( control, location );
        }

        #endregion

    }
}
