using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.WinControls
{

    public partial class LoginControl
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _PasswordTextBox = new System.Windows.Forms.TextBox();
            _PasswordTextBoxLabel = new Label();
            _UserNameTextBox = new System.Windows.Forms.TextBox();
            _UserNameTextBoxLabel = new Label();
            _ErrorProvider = new ErrorProvider(components);
            _LoginLinkLabel = new LinkLabel();
            _LoginLinkLabel.LinkClicked += new LinkLabelLinkClickedEventHandler(LoginLinkLabel_LinkClicked);
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            SuspendLayout();
            // 
            // _passwordTextBox
            // 
            _PasswordTextBox.Location = new Point(95, 31);
            _PasswordTextBox.Margin = new Padding(3, 4, 3, 4);
            _PasswordTextBox.Name = "_passwordTextBox";
            _PasswordTextBox.PasswordChar = '*';
            _PasswordTextBox.Size = new Size(81, 25);
            _PasswordTextBox.TabIndex = 3;
            // 
            // _passwordTextBoxLabel
            // 
            _PasswordTextBoxLabel.Location = new Point(5, 30);
            _PasswordTextBoxLabel.Name = "_passwordTextBoxLabel";
            _PasswordTextBoxLabel.Size = new Size(90, 26);
            _PasswordTextBoxLabel.TabIndex = 2;
            _PasswordTextBoxLabel.Text = "Password: ";
            _PasswordTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _userNameTextBox
            // 
            _UserNameTextBox.Location = new Point(96, 4);
            _UserNameTextBox.Margin = new Padding(3, 4, 3, 4);
            _UserNameTextBox.Name = "_userNameTextBox";
            _UserNameTextBox.Size = new Size(81, 25);
            _UserNameTextBox.TabIndex = 1;
            _UserNameTextBox.Text = "David";
            // 
            // _userNameTextBoxLabel
            // 
            _UserNameTextBoxLabel.Location = new Point(6, 3);
            _UserNameTextBoxLabel.Name = "_userNameTextBoxLabel";
            _UserNameTextBoxLabel.Size = new Size(89, 26);
            _UserNameTextBoxLabel.TabIndex = 0;
            _UserNameTextBoxLabel.Text = "User Name: ";
            _UserNameTextBoxLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // _LoginLinkLabel
            // 
            _LoginLinkLabel.Location = new Point(113, 60);
            _LoginLinkLabel.Name = "_LoginLinkLabel";
            _LoginLinkLabel.Size = new Size(59, 17);
            _LoginLinkLabel.TabIndex = 4;
            _LoginLinkLabel.TabStop = true;
            _LoginLinkLabel.Text = "Log In";
            _LoginLinkLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // LoginControl
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.Transparent;
            BorderStyle = BorderStyle.Fixed3D;
            Controls.Add(_LoginLinkLabel);
            Controls.Add(_PasswordTextBox);
            Controls.Add(_PasswordTextBoxLabel);
            Controls.Add(_UserNameTextBox);
            Controls.Add(_UserNameTextBoxLabel);
            Margin = new Padding(3, 4, 3, 4);
            Name = "LoginControl";
            Size = new Size(179, 81);
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            Load += new EventHandler(UserLoginControl_Load);
            VisibleChanged += new EventHandler(LoginControl_VisibleChanged);
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.TextBox _PasswordTextBox;
        private Label _PasswordTextBoxLabel;
        private System.Windows.Forms.TextBox _UserNameTextBox;
        private Label _UserNameTextBoxLabel;
        private ErrorProvider _ErrorProvider;
        private LinkLabel _LoginLinkLabel;
    }
}
