using System;

// In project file: [assembly: AssemblyDescription( "Core Framework Windows Library" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Core.WinControls.Selectors.MSTest,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey )]
