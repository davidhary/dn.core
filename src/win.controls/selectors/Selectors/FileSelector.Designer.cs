using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.WinControls
{

    public partial class FileSelector
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        internal ToolTip _toolTip;
        private System.Windows.Forms.TextBox _FilePathTextBox;
        private Button _BrowseButton;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _toolTip = new ToolTip(components);
            _BrowseButton = new Button();
            _BrowseButton.Click += new EventHandler(BrowseButton_Click);
            _FilePathTextBox = new System.Windows.Forms.TextBox();
            _FilePathTextBox.Validating += new System.ComponentModel.CancelEventHandler(FilePathTextBox_Validating);
            SuspendLayout();
            // 
            // _BrowseButton
            // 
            _BrowseButton.BackColor = SystemColors.Control;
            _BrowseButton.Cursor = Cursors.Default;
            _BrowseButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _BrowseButton.ForeColor = SystemColors.ControlText;
            _BrowseButton.Location = new Point(516, 1);
            _BrowseButton.Name = "_BrowseButton";
            _BrowseButton.RightToLeft = RightToLeft.No;
            _BrowseButton.Size = new Size(29, 22);
            _BrowseButton.TabIndex = 0;
            _BrowseButton.Text = "...";
            _BrowseButton.TextAlign = ContentAlignment.TopCenter;
            _toolTip.SetToolTip(_BrowseButton, "Browses for a file");
            _BrowseButton.UseMnemonic = false;
            _BrowseButton.UseVisualStyleBackColor = true;
            // 
            // _FilePathTextBox
            // 
            _FilePathTextBox.AcceptsReturn = true;
            _FilePathTextBox.BackColor = SystemColors.Window;
            _FilePathTextBox.Cursor = Cursors.IBeam;
            _FilePathTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold);
            _FilePathTextBox.ForeColor = SystemColors.WindowText;
            _FilePathTextBox.Location = new Point(0, 0);
            _FilePathTextBox.MaxLength = 0;
            _FilePathTextBox.Name = "_FilePathTextBox";
            _FilePathTextBox.RightToLeft = RightToLeft.No;
            _FilePathTextBox.Size = new Size(510, 25);
            _FilePathTextBox.TabIndex = 1;
            // 
            // FileSelector
            // 
            BackColor = Color.Transparent;
            Controls.Add(_FilePathTextBox);
            Controls.Add(_BrowseButton);
            Name = "FileSelector";
            Size = new Size(547, 25);
            Resize += new EventHandler(UserControl_Resize);
            ResumeLayout(false);
            PerformLayout();
        }
    }
}
