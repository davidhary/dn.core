using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using isr.Core.Notifiers;
using isr.Core.WinControls.ErrorProviderExtensions;

namespace isr.Core.WinControls
{

    /// <summary> A control for selecting and opening a resource defined by a resource name. </summary>
    /// <remarks>
    /// David, 2006-02-20, 1.0.2242.x <para>
    /// David, 2018-12-11, 3.4.6819.x. from VI resource selector connector. </para><para>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    [Description( "Resource Selector and Opener Base Control" )]
    [ToolboxBitmap( typeof( SelectorOpener ), "SelectorOpener" )]
    [ToolboxItem( true )]
    public partial class SelectorOpener : ModelViewBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        public SelectorOpener() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._ClearButton.Visible = true;
            this._ToggleOpenButton.Visible = true;
            this._EnumerateButton.Visible = true;
            this._ToggleOpenButton.Enabled = false;
            this._ClearButton.Enabled = false;
            this._EnumerateButton.Enabled = true;
            this._ResourceNamesComboBox.ComboBox.SelectedValueChanged += this.ResourceNamesComboBoxValueChanged;
            this._ResourceNamesComboBox.ComboBox.Validated += this.ResourceNamesComboBoxValueChanged;
        }

        /// <summary> Creates a new <see cref="SelectorOpener"/> </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <returns> A <see cref="SelectorOpener"/>. </returns>
        public static SelectorOpener Create()
        {
            SelectorOpener selectorOpener = null;
            try
            {
                selectorOpener = new SelectorOpener();
                return selectorOpener;
            }
            catch
            {
                selectorOpener.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the views are unbound in case the form is closed without closing the views.
                    this.AssignSelectorViewModel( null );
                    this.AssignOpenerViewModel( null );
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " BROWSABLE PROPERTIES "

        /// <summary>
        /// Gets or sets the value indicating if the clear button is visible and can be enabled. An item
        /// can be cleared only if it is open.
        /// </summary>
        /// <value> The clearable. </value>
        [Category( "Appearance" )]
        [Description( "Shows or hides the Clear button" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( true )]
        public bool Clearable
        {
            get => this._ClearButton.Visible;

            set {
                if ( !this.Clearable.Equals( value ) )
                {
                    this._ClearButton.Visible = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the value indicating if the open button is visible and can be enabled. An item
        /// can be opened only if it is validated.
        /// </summary>
        /// <value> The openable state. </value>
        [Category( "Appearance" )]
        [Description( "Shows or hides the open button" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [RefreshProperties( RefreshProperties.All )]
        [DefaultValue( true )]
        public bool Openable
        {
            get => this._ToggleOpenButton.Visible;

            set {
                if ( !this.Openable.Equals( value ) )
                {
                    this._ToggleOpenButton.Visible = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the condition determining if the control can be searchable. The elements can be
        /// searched only if not open.
        /// </summary>
        /// <value> The searchable. </value>
        [Category( "Appearance" )]
        [Description( "Shows or hides the Search (Find) button" )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( true )]
        public bool Searchable
        {
            get => this._EnumerateButton.Visible;

            set {
                if ( !this.Searchable.Equals( value ) )
                {
                    this._EnumerateButton.Visible = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " SELECTOR VIEW MODEL "

        /// <summary> Gets the selector view model. </summary>
        /// <value> The selector view model. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SelectResourceBase SelectorViewModel { get; private set; }

        /// <summary> Assign the selector view model. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="viewModel"> The view model. </param>
        public void AssignSelectorViewModel( SelectResourceBase viewModel )
        {
            if ( this.SelectorViewModel is object )
            {
                this.BindEnumerateButton( false, this.SelectorViewModel );
                this.BindResourceNamesCombo( false, this.SelectorViewModel );
                this.SelectorViewModel = null;
            }

            if ( viewModel is object )
            {
                this.SelectorViewModel = viewModel;
                // this.SelectorViewModel.SearchImage = Selectors.Properties.Resources.Find_22x22;
                this.BindEnumerateButton( true, this.SelectorViewModel );
                this.BindResourceNamesCombo( true, this.SelectorViewModel );
            }
        }

        /// <summary> Bind enumerate button. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="viewModel"> The view model. </param>
        private void BindEnumerateButton( bool add, SelectResourceBase viewModel )
        {
            Binding binding = this.AddRemoveBinding( this._EnumerateButton, add, nameof( ToolStripButton.Visible ), viewModel,
                                                                          nameof( SelectResourceBase.Searchable ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;

            binding = this.AddRemoveBinding( this._EnumerateButton, add, nameof( ToolStripButton.Enabled ), viewModel,
                                             nameof( SelectResourceBase.SearchEnabled ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;

            binding = this.AddRemoveBinding( this._EnumerateButton, add, nameof( ToolStripButton.ToolTipText ), viewModel,
                                             nameof( SelectResourceBase.SearchToolTip ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;

            // binding = this.AddRemoveBinding( this._EnumerateButton, add, nameof( ToolStripButton.Image ), viewModel, nameof( SelectResourceBase.SearchImage ) );
            this._EnumerateButton.Image = Selectors.Properties.Resources.Find_22x22;

            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
        }

        /// <summary> Bind resource names combo. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="viewModel"> The view model. </param>
        private void BindResourceNamesCombo( bool add, SelectResourceBase viewModel )
        {
            Binding binding = this.AddRemoveBinding( this._ResourceNamesComboBox.ComboBox, add,
                                                                          nameof( ComboBox.Enabled ), viewModel, nameof( SelectResourceBase.SearchEnabled ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;

            // this binding has caused cross thread exceptions. occasionally. even though the binding is set from the control using the thread safe property change manager.
            this._ResourceNamesComboBox.ComboBox.DataSource = add ? viewModel.ResourceNames : null;

            // this binding has caused cross thread exceptions. occasionally. even though the binding is set from the control using the thread safe property change manager.
            _ = this.AddRemoveBinding( this._ResourceNamesComboBox.ComboBox, add, nameof( ComboBox.Text ), viewModel,
                                       nameof( SelectResourceBase.CandidateResourceName ) );
        }

        /// <summary> Attempts to validate the specified resource name. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="resourceName"> The value. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        private (bool Success, string Details) TryValidateResourceName( string resourceName )
        {
            if ( string.IsNullOrWhiteSpace( resourceName ) )
            {
                resourceName = string.Empty;
            }

            (bool Success, string Details) result = (true, string.Empty);
            if ( string.Equals( this.OpenerViewModel.ValidatedResourceName, resourceName, StringComparison.OrdinalIgnoreCase ) )
            {
                return result;
            }

            string activity = string.Empty;
            var sender = this._ResourceNamesComboBox;
            try
            {
                this._ErrorProvider.Clear();
                this.Cursor = Cursors.WaitCursor;
                activity = $"validating {resourceName}";
                Trace.TraceInformation( $"{activity};. " );
                result = this.SelectorViewModel.TryValidateResource( resourceName );
                if ( result.Success )
                {
                    if ( !string.IsNullOrEmpty( result.Details ) )
                    {
                        activity = $"{activity};. reported {result.Details}";
                        Trace.TraceInformation( $"{activity};. " );
                    }
                    // this enables opening 
                    if ( this.OpenerViewModel is object )
                    {
                        this.OpenerViewModel.ValidatedResourceName = this.SelectorViewModel.ValidatedResourceName;
                    }
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, result.Details );
                    activity = $"Failed {activity};. {result.Details}";
                    Trace.TraceWarning( $"{activity};. " );
                    // this disables opening 
                    if ( this.OpenerViewModel is object )
                    {
                        this.OpenerViewModel.ValidatedResourceName = string.Empty;
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.Message );
                activity = $"{activity};. {ex}";
                Trace.TraceError( activity );
                result = (false, activity);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

            return result;
        }

        /// <summary> Gets the number of selected value changes. </summary>
        /// <value> The number of selected value changes. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int SelectedValueChangeCount { get; set; }

        /// <summary> Event handler called by the validated and selected index changed events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ResourceNamesComboBoxValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
            {
                return;
            }

            var control = this._ResourceNamesComboBox; // TryCast(sender, ToolStripSpringComboBox)
            if ( !(control is null || string.IsNullOrWhiteSpace( control.Text )) )
            {
                this.SelectedValueChangeCount += 1;
                _ = this.TryValidateResourceName( control.Text );
            }
        }

        /// <summary> Event handler. Called by _EnumerateButton for click events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void EnumerateButtonClick( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this._ErrorProvider.Clear();
                this.Cursor = Cursors.WaitCursor;
                activity = "Enumerating resources";
                Trace.TraceInformation( $"{activity};. " );
                // enumerate resource with filtering when enumerating from the GUI.
                _ = this.SelectorViewModel.EnumerateResources( true );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.Message );
                Trace.TraceError( $"Exception {activity};. {ex}" );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " OPENER VIEW MODEL "

        /// <summary> Gets the opener view model. </summary>
        /// <value> The opener view model. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public OpenResourceBase OpenerViewModel { get; private set; }

        /// <summary> Assigns the opener view model. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="viewModel"> The view model. </param>
        public void AssignOpenerViewModel( OpenResourceBase viewModel )
        {
            if ( this.OpenerViewModel is object )
            {
                this.BindClearButton( false, this.OpenerViewModel );
                this.BindOpenButton( false, this.OpenerViewModel );
                this.OpenerViewModel = null;
            }

            if ( viewModel is object )
            {
                this.OpenerViewModel = viewModel;
                //this.OpenerViewModel.OpenedImage = Selectors.Properties.Resources.Connected_22x22;
                //this.OpenerViewModel.ClosedImage = Selectors.Properties.Resources.Disconnected_22x22;
                //this.OpenerViewModel.ClearImage = Selectors.Properties.Resources.Clear_22x22;
                this.BindClearButton( true, this.OpenerViewModel );
                this.BindOpenButton( true, this.OpenerViewModel );
            }
        }

        /// <summary> Binds the Clear button. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="viewModel"> The view model. </param>
        private void BindClearButton( bool add, OpenResourceBase viewModel )
        {
            Binding binding = this.AddRemoveBinding( this._ClearButton, add,
                                                                          nameof( ToolStripButton.Enabled ), viewModel, nameof( OpenResourceBase.IsOpen ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;

            binding = this.AddRemoveBinding( this._ClearButton, add,
                                             nameof( ToolStripButton.Visible ), viewModel, nameof( OpenResourceBase.Clearable ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;

            binding = this.AddRemoveBinding( this._ClearButton, add,
                                             nameof( ToolStripButton.ToolTipText ), viewModel, nameof( OpenResourceBase.ClearToolTip ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
        }

        /// <summary> Bind open button. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="add">       True to add; otherwise, remove. </param>
        /// <param name="viewModel"> The view model. </param>
        private void BindOpenButton( bool add, OpenResourceBase viewModel )
        {
            Binding binding = this.AddRemoveBinding( this._ToggleOpenButton, add, nameof( ToolStripButton.Visible ), viewModel, nameof( OpenResourceBase.Openable ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;

            binding = this.AddRemoveBinding( this._ToggleOpenButton, add, nameof( ToolStripButton.Enabled ), viewModel, nameof( OpenResourceBase.OpenEnabled ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
            binding.ControlUpdateMode = ControlUpdateMode.OnPropertyChanged;

            binding = this.AddRemoveBinding( this._ToggleOpenButton, add, nameof( ToolStripButton.ToolTipText ), viewModel, nameof( OpenResourceBase.OpenToolTip ) );
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;

            // binding = this.AddRemoveBinding( this._ToggleOpenButton, add, nameof( ToolStripButton.Image ), viewModel, nameof( OpenResourceBase.OpenImage ) );
            this._ToggleOpenButton.Image = Selectors.Properties.Resources.Connected_22x22;
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
            viewModel.NotifyOpenChanged();
        }

        /// <summary> Executes the toggle open  action. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        private void ToggleOpen( ToolStripItem sender )
        {
            if ( sender is null )
            {
                return;
            }

            (bool Success, string Details) result;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this._ErrorProvider.Clear();
                if ( this.OpenerViewModel.IsOpen )
                {
                    activity = $"closing {this.OpenerViewModel.ResourceNameCaption}";
                    Trace.TraceInformation( $"{activity};. " );
                    result = this.OpenerViewModel.TryClose();
                }
                else
                {
                    activity = $"opening {this.OpenerViewModel.ResourceTitleCaption}";
                    Trace.TraceInformation( $"{activity};. " );
                    result = this.OpenerViewModel.TryOpen();
                }

                if ( !result.Success )
                {
                    Trace.TraceWarning( result.Details );
                    _ = this._ErrorProvider.Annunciate( sender, result.Details );
                }
                else
                {
                    // this ensures that the control refreshes.
                    this._OverflowLabel.Text = ".";
                    Application.DoEvents();
                    this._OverflowLabel.Text = string.Empty;
                    Application.DoEvents();
                    activity = $"{this.OpenerViewModel.ResourceNameCaption} is {(this.OpenerViewModel.IsOpen ? "open" : "close")}";
                    Trace.TraceInformation( $"{activity};. " );
                }
                // this disables search when the resource is open
                if ( this.SelectorViewModel is object )
                {
                    this.SelectorViewModel.IsOpen = this.OpenerViewModel.IsOpen;
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.Message );
                Trace.TraceError( $"{activity};. {ex}" );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Toggle open button click. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of this
        /// <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        private void ToggleOpenButtonClick( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
            {
                return;
            }

            this.ToggleOpen( sender as ToolStripItem );
        }

        /// <summary> Requests a clear. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ClearButtonClick( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this._ErrorProvider.Clear();
                activity = $"checking {this.OpenerViewModel.ResourceNameCaption} open status";
                if ( this.OpenerViewModel.IsOpen )
                {
                    activity = $"clearing {this.OpenerViewModel.ResourceNameCaption}";
                    (bool Success, String Details) = this.OpenerViewModel.TryClearActiveState();
                    if ( !Success )
                    {
                        Trace.TraceWarning( Details );
                        _ = this._ErrorProvider.Annunciate( sender, Details );
                    }
                }
                else
                {
                    activity = $"failed {activity}; not open";
                    Trace.TraceWarning( activity );
                    _ = this._ErrorProvider.Annunciate( sender, activity );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.Message );
                Trace.TraceWarning( $"Exception {activity};. {ex}" );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by _ToolStrip for double click events. </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ToolStripDoubleClick( object sender, EventArgs e )
        {
            // this ensures that the control refreshes.
            this._OverflowLabel.Text = ".";
            Application.DoEvents();
            this._OverflowLabel.Text = string.Empty;
            Application.DoEvents();
        }

        #endregion

        #region " UNIT TESTS INTERNALS "

        /// <summary> Gets the number of internal resource names. </summary>
        /// <value> The number of internal resource names. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        internal int ResourceNamesCount => this._ResourceNamesComboBox.ComboBox is null ? 0 : this._ResourceNamesComboBox.Items.Count;

        /// <summary> Gets the name of the internal selected resource. </summary>
        /// <value> The name of the internal selected resource. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        internal string SelectedResourceName => this._ResourceNamesComboBox.ComboBox is null ? string.Empty : this._ResourceNamesComboBox.Text;

        #endregion

    }
}
