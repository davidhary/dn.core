using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.WinControls
{

    public partial class SelectorOpener
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ErrorProvider = new ErrorProvider(components);
            _ToolStrip = new ToolStrip();
            _ToolStrip.DoubleClick += new EventHandler(ToolStripDoubleClick);
            _ClearButton = new ToolStripButton();
            _ClearButton.Click += new EventHandler(ClearButtonClick);
            _ResourceNamesComboBox = new ToolStripSpringComboBox();
            _ResourceNamesComboBox.DoubleClick += new EventHandler(ToolStripDoubleClick);
            _ToggleOpenButton = new ToolStripButton();
            _ToggleOpenButton.Click += new EventHandler(ToggleOpenButtonClick);
            _EnumerateButton = new ToolStripButton();
            _EnumerateButton.Click += new EventHandler(EnumerateButtonClick);
            _OverflowLabel = new System.Windows.Forms.ToolStripLabel();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            _ToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // _ToolStrip
            // 
            _ToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            _ToolStrip.Items.AddRange(new ToolStripItem[] { _ClearButton, _ResourceNamesComboBox, _ToggleOpenButton, _EnumerateButton, _OverflowLabel });
            _ToolStrip.Location = new Point(0, 0);
            _ToolStrip.Name = "_ToolStrip";
            _ToolStrip.Size = new Size(474, 29);
            _ToolStrip.TabIndex = 0;
            _ToolStrip.Text = "Selector Opener";
            // 
            // _ClearButton
            // 
            _ClearButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _ClearButton.Image = Selectors.Properties.Resources.Clear_22x22;
            _ClearButton.ImageScaling = ToolStripItemImageScaling.None;
            _ClearButton.ImageTransparentColor = Color.Magenta;
            _ClearButton.Name = "_ClearButton";
            _ClearButton.Overflow = ToolStripItemOverflow.Never;
            _ClearButton.Size = new Size(26, 26);
            _ClearButton.Text = "Clear known state";
            // 
            // _ResourceNamesComboBox
            // 
            _ResourceNamesComboBox.Name = "_ResourceNamesComboBox";
            _ResourceNamesComboBox.Overflow = ToolStripItemOverflow.Never;
            _ResourceNamesComboBox.Size = new Size(362, 29);
            _ResourceNamesComboBox.ToolTipText = "Available resource names";
            // 
            // _ToggleOpenButton
            // 
            _ToggleOpenButton.Alignment = ToolStripItemAlignment.Right;
            _ToggleOpenButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _ToggleOpenButton.Image = Selectors.Properties.Resources.Disconnected_22x22;
            _ToggleOpenButton.ImageScaling = ToolStripItemImageScaling.None;
            _ToggleOpenButton.ImageTransparentColor = Color.Magenta;
            _ToggleOpenButton.Name = "_ToggleOpenButton";
            _ToggleOpenButton.Overflow = ToolStripItemOverflow.Never;
            _ToggleOpenButton.Size = new Size(26, 26);
            _ToggleOpenButton.ToolTipText = "Click to open";
            // 
            // _EnumerateButton
            // 
            _EnumerateButton.Alignment = ToolStripItemAlignment.Right;
            _EnumerateButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _EnumerateButton.Image = Selectors.Properties.Resources.Find_22x22;
            _EnumerateButton.ImageScaling = ToolStripItemImageScaling.None;
            _EnumerateButton.ImageTransparentColor = Color.Magenta;
            _EnumerateButton.Name = "_EnumerateButton";
            _EnumerateButton.Overflow = ToolStripItemOverflow.Never;
            _EnumerateButton.Size = new Size(26, 26);
            _EnumerateButton.ToolTipText = "Enumerate resources";
            // 
            // _OverflowLabel
            // 
            _OverflowLabel.Name = "_OverflowLabel";
            _OverflowLabel.Size = new Size(0, 26);
            // 
            // SelectorOpener
            // 
            BackColor = Color.Transparent;
            Controls.Add(_ToolStrip);
            Margin = new Padding(0);
            Name = "SelectorOpener";
            Size = new Size(474, 29);
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            _ToolStrip.ResumeLayout(false);
            _ToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private ErrorProvider _ErrorProvider;
        private ToolStripButton _ClearButton;
        private ToolStripSpringComboBox _ResourceNamesComboBox;
        private ToolStripButton _EnumerateButton;
        private ToolStripButton _ToggleOpenButton;
        private ToolStrip _ToolStrip;
        private System.Windows.Forms.ToolStripLabel _OverflowLabel;
    }
}
