using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.WinControls
{
    public partial class FolderFileSelector
    {
        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(FolderFileSelector));
            _FileListBoxLabel = new Label();
            _FileNamePatternCheckBoxLabel = new Label();
            _FolderTextBoxLabel = new Label();
            _FullFileNameTextBoxLabel = new Label();
            _FullFileNameTextBox = new System.Windows.Forms.TextBox();
            _FullFileNameTextBox.TextChanged += new EventHandler(FileTitleTextBox_TextChanged);
            _FullFileNameTextBox.Enter += new EventHandler(EditTextBox_Enter);
            _SelectFolderButton = new Button();
            _SelectFolderButton.Click += new EventHandler(SelectFolderButton_Click);
            _FolderTextBox = new System.Windows.Forms.TextBox();
            _FolderTextBox.Enter += new EventHandler(EditTextBox_Enter);
            _FolderTextBox.TextChanged += new EventHandler(FolderTextBox_TextChanged);
            _FilePatternComboBox = new System.Windows.Forms.ComboBox();
            _FilePatternComboBox.SelectedIndexChanged += new EventHandler(FilePatternComboBox_SelectedIndexChanged);
            _ToolTip = new ToolTip(components);
            _DataFileListBox = new FileListBox();
            _DataFileListBox.SelectedIndexChanged += new EventHandler(DataFileListBox_SelectedIndexChanged);
            _DataFileListBox.DoubleClick += new EventHandler(DataFileListBox_DoubleClick);
            _FileInfoTextBox = new RichTextBox();
            _FileInfoTextBoxLabel = new Label();
            _RefreshButton = new Button();
            _RefreshButton.Click += new EventHandler(RefreshButton_Click);
            _Annunciator = new ErrorProvider(components);
            ((System.ComponentModel.ISupportInitialize)_Annunciator).BeginInit();
            SuspendLayout();
            // 
            // _FileListBoxLabel
            // 
            _FileListBoxLabel.BackColor = Color.Transparent;
            _FileListBoxLabel.Cursor = Cursors.Default;
            _FileListBoxLabel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _FileListBoxLabel.ForeColor = SystemColors.WindowText;
            _FileListBoxLabel.Location = new Point(2, 94);
            _FileListBoxLabel.Name = "_FileListBoxLabel";
            _FileListBoxLabel.RightToLeft = RightToLeft.No;
            _FileListBoxLabel.Size = new Size(72, 18);
            _FileListBoxLabel.TabIndex = 5;
            _FileListBoxLabel.Text = "&Files:";
            _FileListBoxLabel.TextAlign = ContentAlignment.BottomLeft;
            // 
            // _FileNamePatternCheckBoxLabel
            // 
            _FileNamePatternCheckBoxLabel.BackColor = Color.Transparent;
            _FileNamePatternCheckBoxLabel.Cursor = Cursors.Default;
            _FileNamePatternCheckBoxLabel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _FileNamePatternCheckBoxLabel.ForeColor = SystemColors.WindowText;
            _FileNamePatternCheckBoxLabel.Location = new Point(3, 342);
            _FileNamePatternCheckBoxLabel.Name = "_FileNamePatternCheckBoxLabel";
            _FileNamePatternCheckBoxLabel.RightToLeft = RightToLeft.No;
            _FileNamePatternCheckBoxLabel.Size = new Size(72, 18);
            _FileNamePatternCheckBoxLabel.TabIndex = 7;
            _FileNamePatternCheckBoxLabel.Text = "File T&ype: ";
            _FileNamePatternCheckBoxLabel.TextAlign = ContentAlignment.BottomLeft;
            // 
            // _FolderTextBoxLabel
            // 
            _FolderTextBoxLabel.AutoSize = true;
            _FolderTextBoxLabel.BackColor = Color.Transparent;
            _FolderTextBoxLabel.Cursor = Cursors.Default;
            _FolderTextBoxLabel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _FolderTextBoxLabel.ForeColor = SystemColors.WindowText;
            _FolderTextBoxLabel.Location = new Point(3, 3);
            _FolderTextBoxLabel.Name = "_FolderTextBoxLabel";
            _FolderTextBoxLabel.RightToLeft = RightToLeft.No;
            _FolderTextBoxLabel.Size = new Size(48, 17);
            _FolderTextBoxLabel.TabIndex = 0;
            _FolderTextBoxLabel.Text = "F&older:";
            _FolderTextBoxLabel.TextAlign = ContentAlignment.BottomLeft;
            // 
            // _FullFileNameTextBoxLabel
            // 
            _FullFileNameTextBoxLabel.AutoSize = true;
            _FullFileNameTextBoxLabel.BackColor = Color.Transparent;
            _FullFileNameTextBoxLabel.Cursor = Cursors.Default;
            _FullFileNameTextBoxLabel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _FullFileNameTextBoxLabel.ForeColor = SystemColors.WindowText;
            _FullFileNameTextBoxLabel.Location = new Point(3, 51);
            _FullFileNameTextBoxLabel.Name = "_FullFileNameTextBoxLabel";
            _FullFileNameTextBoxLabel.RightToLeft = RightToLeft.No;
            _FullFileNameTextBoxLabel.Size = new Size(30, 17);
            _FullFileNameTextBoxLabel.TabIndex = 3;
            _FullFileNameTextBoxLabel.Text = "F&ile:";
            _FullFileNameTextBoxLabel.TextAlign = ContentAlignment.BottomLeft;
            // 
            // _FullFileNameTextBox
            // 
            _FullFileNameTextBox.AcceptsReturn = true;
            _FullFileNameTextBox.BackColor = SystemColors.Window;
            _FullFileNameTextBox.Cursor = Cursors.IBeam;
            _FullFileNameTextBox.Font = new Font("Arial", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _FullFileNameTextBox.ForeColor = SystemColors.WindowText;
            _FullFileNameTextBox.Location = new Point(3, 70);
            _FullFileNameTextBox.MaxLength = 0;
            _FullFileNameTextBox.Name = "_FullFileNameTextBox";
            _FullFileNameTextBox.RightToLeft = RightToLeft.No;
            _FullFileNameTextBox.Size = new Size(246, 22);
            _FullFileNameTextBox.TabIndex = 4;
            _ToolTip.SetToolTip(_FullFileNameTextBox, "The name of the file that was selected or entered");
            _FullFileNameTextBox.WordWrap = false;
            // 
            // _SelectFolderButton
            // 
            _SelectFolderButton.Font = new Font("Segoe UI Black", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _SelectFolderButton.Location = new Point(609, 22);
            _SelectFolderButton.Name = "_SelectFolderButton";
            _SelectFolderButton.Size = new Size(36, 26);
            _SelectFolderButton.TabIndex = 2;
            _SelectFolderButton.Text = "...";
            _ToolTip.SetToolTip(_SelectFolderButton, "Click to select a folder");
            // 
            // _FolderTextBox
            // 
            _FolderTextBox.AcceptsReturn = true;
            _FolderTextBox.BackColor = SystemColors.Window;
            _FolderTextBox.Cursor = Cursors.IBeam;
            _FolderTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _FolderTextBox.ForeColor = SystemColors.WindowText;
            _FolderTextBox.Location = new Point(3, 22);
            _FolderTextBox.MaxLength = 0;
            _FolderTextBox.Name = "_FolderTextBox";
            _FolderTextBox.RightToLeft = RightToLeft.No;
            _FolderTextBox.Size = new Size(604, 25);
            _FolderTextBox.TabIndex = 1;
            _ToolTip.SetToolTip(_FolderTextBox, "The name of the file you have selected or entered");
            // 
            // _FilePatternComboBox
            // 
            _FilePatternComboBox.BackColor = SystemColors.Window;
            _FilePatternComboBox.Cursor = Cursors.Default;
            _FilePatternComboBox.Font = new Font("Arial", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _FilePatternComboBox.ForeColor = SystemColors.WindowText;
            _FilePatternComboBox.Items.AddRange(new object[] { "Data Files (*.dat)", "All Files (*.*)" });
            _FilePatternComboBox.Location = new Point(3, 362);
            _FilePatternComboBox.Name = "_FilePatternComboBox";
            _FilePatternComboBox.RightToLeft = RightToLeft.No;
            _FilePatternComboBox.Size = new Size(184, 24);
            _FilePatternComboBox.TabIndex = 8;
            _FilePatternComboBox.Text = "*.dat";
            _ToolTip.SetToolTip(_FilePatternComboBox, "Determines the type listed in the file list");
            // 
            // _DataFileListBox
            // 
            _DataFileListBox.BackColor = SystemColors.Window;
            _DataFileListBox.Cursor = Cursors.Default;
            _DataFileListBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, 0);
            _DataFileListBox.ForeColor = Color.Black;
            _DataFileListBox.FormattingEnabled = true;
            _DataFileListBox.ItemHeight = 17;
            _DataFileListBox.Location = new Point(3, 113);
            _DataFileListBox.Name = "_DataFileListBox";
            _DataFileListBox.DirectoryPath = string.Empty;
            _DataFileListBox.SearchPattern = "*.dat";
            _DataFileListBox.ReadOnlyBackColor = SystemColors.Control;
            _DataFileListBox.ReadOnlyForeColor = SystemColors.WindowText;
            _DataFileListBox.ReadWriteBackColor = SystemColors.Window;
            _DataFileListBox.ReadWriteForeColor = SystemColors.ControlText;
            _DataFileListBox.SelectionMode = SelectionMode.MultiExtended;
            _DataFileListBox.Size = new Size(248, 225);
            _DataFileListBox.TabIndex = 6;
            _ToolTip.SetToolTip(_DataFileListBox, "Select file(s)");
            // 
            // _FileInfoTextBox
            // 
            _FileInfoTextBox.Location = new Point(273, 70);
            _FileInfoTextBox.Name = "_FileInfoTextBox";
            _FileInfoTextBox.Size = new Size(371, 316);
            _FileInfoTextBox.TabIndex = 10;
            _FileInfoTextBox.Text = string.Empty;
            _ToolTip.SetToolTip(_FileInfoTextBox, "Displays file information if available");
            // 
            // _FileInfoTextBoxLabel
            // 
            _FileInfoTextBoxLabel.AutoSize = true;
            _FileInfoTextBoxLabel.Location = new Point(272, 51);
            _FileInfoTextBoxLabel.Name = "_FileInfoTextBoxLabel";
            _FileInfoTextBoxLabel.Size = new Size(56, 17);
            _FileInfoTextBoxLabel.TabIndex = 11;
            _FileInfoTextBoxLabel.Text = "File info:";
            // 
            // _RefreshButton
            // 
            _RefreshButton.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            _RefreshButton.Image = (Image)resources.GetObject("_RefreshButton.Image");
            _RefreshButton.Location = new Point(214, 352);
            _RefreshButton.Name = "_RefreshButton";
            _RefreshButton.Size = new Size(35, 34);
            _RefreshButton.TabIndex = 9;
            _ToolTip.SetToolTip(_RefreshButton, "Click to select a folder");
            _RefreshButton.UseCompatibleTextRendering = true;
            // 
            // _Annunciator
            // 
            _Annunciator.ContainerControl = this;
            // 
            // FolderFileSelector
            // 
            AutoScaleMode = AutoScaleMode.Inherit;
            Controls.Add(_FileInfoTextBoxLabel);
            Controls.Add(_FileInfoTextBox);
            Controls.Add(_FileListBoxLabel);
            Controls.Add(_FileNamePatternCheckBoxLabel);
            Controls.Add(_FolderTextBoxLabel);
            Controls.Add(_FullFileNameTextBoxLabel);
            Controls.Add(_FullFileNameTextBox);
            Controls.Add(_FilePatternComboBox);
            Controls.Add(_RefreshButton);
            Controls.Add(_SelectFolderButton);
            Controls.Add(_FolderTextBox);
            Controls.Add(_DataFileListBox);
            Name = "FolderFileSelector";
            Size = new Size(650, 390);
            ((System.ComponentModel.ISupportInitialize)_Annunciator).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private Label _FileListBoxLabel;
        private Label _FileNamePatternCheckBoxLabel;
        private Label _FolderTextBoxLabel;
        private ToolTip _ToolTip;
        private Label _FullFileNameTextBoxLabel;
        private System.Windows.Forms.TextBox _FullFileNameTextBox;
        private Button _RefreshButton;
        private Button _SelectFolderButton;
        private System.Windows.Forms.TextBox _FolderTextBox;
        private System.Windows.Forms.ComboBox _FilePatternComboBox;
        private FileListBox _DataFileListBox;
        private ErrorProvider _Annunciator;
        private Label _FileInfoTextBoxLabel;
        private RichTextBox _FileInfoTextBox;
    }
}
