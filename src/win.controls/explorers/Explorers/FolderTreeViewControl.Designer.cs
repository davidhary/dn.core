using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.WinControls
{

    public partial class FolderTreeViewControl
    {
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(FolderTreeViewControl));
            _PathTextBox = new System.Windows.Forms.TextBox();
            _PathTextBox.KeyUp += new KeyEventHandler(PathTextBox_KeyUp);
            _GoToDirectoryButton = new Button();
            _GoToDirectoryButton.Click += new EventHandler(GoToDirectoryButtonClick);
            _ImageList = new ImageList(components);
            _ToolTip = new ToolTip(components);
            _FolderTreeView = new FolderTreeView();
            _FolderTreeView.PathChanged += new EventHandler<EventArgs>(FolderTreeView_PathChanged);
            _CurrentPathLayoutPanel = new TableLayoutPanel();
            _ToolStrip = new ToolStrip();
            _HomeToolStripButton = new System.Windows.Forms.ToolStripButton();
            _HomeToolStripButton.Click += new EventHandler(HomeToolStripButton_Click);
            _FolderUpButton = new System.Windows.Forms.ToolStripButton();
            _FolderUpButton.Click += new EventHandler(FolderUpButton_Click);
            _BackToolStripButton = new System.Windows.Forms.ToolStripButton();
            _BackToolStripButton.Click += new EventHandler(BackToolStripButton_Click);
            _ForwardToolStripButton = new System.Windows.Forms.ToolStripButton();
            _ForwardToolStripButton.Click += new EventHandler(ForwardToolStripButton_Click);
            _AddFolderButton = new System.Windows.Forms.ToolStripButton();
            _AddFolderButton.Click += new EventHandler(AddFolderButton_Click);
            _RefreshToolStripButton = new System.Windows.Forms.ToolStripButton();
            _RefreshToolStripButton.Click += new EventHandler(RefreshButtonClick);
            _CurrentPathLayoutPanel.SuspendLayout();
            _ToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _PathTextBox
            // 
            _PathTextBox.Dock = DockStyle.Fill;
            _PathTextBox.Location = new Point(3, 3);
            _PathTextBox.Name = "_PathTextBox";
            _PathTextBox.Size = new Size(276, 23);
            _PathTextBox.TabIndex = 0;
            _ToolTip.SetToolTip(_PathTextBox, "Current directory");
            // 
            // _GoToDirectoryButton
            // 
            _GoToDirectoryButton.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            _GoToDirectoryButton.Cursor = Cursors.Hand;
            _GoToDirectoryButton.FlatStyle = FlatStyle.Flat;
            _GoToDirectoryButton.ForeColor = Color.White;
            _GoToDirectoryButton.ImageIndex = 21;
            _GoToDirectoryButton.ImageList = _ImageList;
            _GoToDirectoryButton.Location = new Point(285, 3);
            _GoToDirectoryButton.Name = "_GoToDirectoryButton";
            _GoToDirectoryButton.Size = new Size(20, 22);
            _GoToDirectoryButton.TabIndex = 1;
            _ToolTip.SetToolTip(_GoToDirectoryButton, "Go to the directory");
            // 
            // _ImageList
            // 
            _ImageList.ImageStream = (ImageListStreamer)resources.GetObject("_ImageList.ImageStream");
            _ImageList.TransparentColor = Color.Transparent;
            _ImageList.Images.SetKeyName(0, "");
            _ImageList.Images.SetKeyName(1, "");
            _ImageList.Images.SetKeyName(2, "");
            _ImageList.Images.SetKeyName(3, "");
            _ImageList.Images.SetKeyName(4, "");
            _ImageList.Images.SetKeyName(5, "");
            _ImageList.Images.SetKeyName(6, "");
            _ImageList.Images.SetKeyName(7, "");
            _ImageList.Images.SetKeyName(8, "");
            _ImageList.Images.SetKeyName(9, "");
            _ImageList.Images.SetKeyName(10, "");
            _ImageList.Images.SetKeyName(11, "");
            _ImageList.Images.SetKeyName(12, "");
            _ImageList.Images.SetKeyName(13, "");
            _ImageList.Images.SetKeyName(14, "");
            _ImageList.Images.SetKeyName(15, "");
            _ImageList.Images.SetKeyName(16, "");
            _ImageList.Images.SetKeyName(17, "");
            _ImageList.Images.SetKeyName(18, "");
            _ImageList.Images.SetKeyName(19, "");
            _ImageList.Images.SetKeyName(20, "");
            _ImageList.Images.SetKeyName(21, "");
            _ImageList.Images.SetKeyName(22, "");
            _ImageList.Images.SetKeyName(23, "");
            _ImageList.Images.SetKeyName(24, "");
            _ImageList.Images.SetKeyName(25, "");
            _ImageList.Images.SetKeyName(26, "");
            _ImageList.Images.SetKeyName(27, "");
            _ImageList.Images.SetKeyName(28, "");
            // 
            // _FolderTreeView
            // 
            _FolderTreeView.Dock = DockStyle.Fill;
            _FolderTreeView.ImageIndex = 0;
            _FolderTreeView.Location = new Point(0, 57);
            _FolderTreeView.Name = "_FolderTreeView";
            _FolderTreeView.SelectedImageIndex = 2;
            _FolderTreeView.ShowLines = false;
            _FolderTreeView.ShowRootLines = false;
            _FolderTreeView.Size = new Size(308, 382);
            _FolderTreeView.TabIndex = 2;
            // 
            // _CurrentPathLayoutPanel
            // 
            _CurrentPathLayoutPanel.ColumnCount = 2;
            _CurrentPathLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _CurrentPathLayoutPanel.ColumnStyles.Add(new ColumnStyle());
            _CurrentPathLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
            _CurrentPathLayoutPanel.Controls.Add(_GoToDirectoryButton, 1, 0);
            _CurrentPathLayoutPanel.Controls.Add(_PathTextBox, 0, 0);
            _CurrentPathLayoutPanel.Dock = DockStyle.Top;
            _CurrentPathLayoutPanel.Location = new Point(0, 25);
            _CurrentPathLayoutPanel.Name = "_CurrentPathLayoutPanel";
            _CurrentPathLayoutPanel.RowCount = 2;
            _CurrentPathLayoutPanel.RowStyles.Add(new RowStyle());
            _CurrentPathLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _CurrentPathLayoutPanel.Size = new Size(308, 32);
            _CurrentPathLayoutPanel.TabIndex = 1;
            // 
            // _ToolStrip
            // 
            _ToolStrip.GripMargin = new Padding(0);
            _ToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            _ToolStrip.Items.AddRange(new ToolStripItem[] { _HomeToolStripButton, _FolderUpButton, _BackToolStripButton, _ForwardToolStripButton, _AddFolderButton, _RefreshToolStripButton });
            _ToolStrip.Location = new Point(0, 0);
            _ToolStrip.Name = "_ToolStrip";
            _ToolStrip.Size = new Size(308, 25);
            _ToolStrip.TabIndex = 0;
            _ToolStrip.Text = "ToolStrip1";
            // 
            // _HomeToolStripButton
            // 
            _HomeToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _HomeToolStripButton.Image = Explorers.Properties.Resources.go_up_8;
            _HomeToolStripButton.ImageTransparentColor = Color.Magenta;
            _HomeToolStripButton.Name = "_HomeToolStripButton";
            _HomeToolStripButton.Size = new Size(23, 22);
            _HomeToolStripButton.Text = "Home";
            // 
            // _FolderUpButton
            // 
            _FolderUpButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _FolderUpButton.Image = Explorers.Properties.Resources.go_up_8;
            _FolderUpButton.ImageTransparentColor = Color.Magenta;
            _FolderUpButton.Name = "_FolderUpButton";
            _FolderUpButton.Size = new Size(23, 22);
            _FolderUpButton.Text = "Up";
            // 
            // _BackToolStripButton
            // 
            _BackToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _BackToolStripButton.Image = Explorers.Properties.Resources.go_up_8;
            _BackToolStripButton.ImageTransparentColor = Color.Magenta;
            _BackToolStripButton.Name = "_BackToolStripButton";
            _BackToolStripButton.Size = new Size(23, 22);
            _BackToolStripButton.Text = "Back";
            // 
            // _ForwardToolStripButton
            // 
            _ForwardToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _ForwardToolStripButton.Image = Explorers.Properties.Resources.go_up_8;
            _ForwardToolStripButton.ImageTransparentColor = Color.Magenta;
            _ForwardToolStripButton.Name = "_ForwardToolStripButton";
            _ForwardToolStripButton.Size = new Size(23, 22);
            _ForwardToolStripButton.Text = "Forward";
            // 
            // _AddFolderButton
            // 
            _AddFolderButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _AddFolderButton.Image = Explorers.Properties.Resources.go_up_8;
            _AddFolderButton.ImageTransparentColor = Color.Magenta;
            _AddFolderButton.Name = "_AddFolderButton";
            _AddFolderButton.Size = new Size(23, 22);
            _AddFolderButton.Text = "Add";
            // 
            // _RefreshToolStripButton
            // 
            _RefreshToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _RefreshToolStripButton.Image = Explorers.Properties.Resources.go_up_8;
            _RefreshToolStripButton.ImageTransparentColor = Color.Magenta;
            _RefreshToolStripButton.Name = "_RefreshToolStripButton";
            _RefreshToolStripButton.Size = new Size(23, 22);
            _RefreshToolStripButton.Text = "Refresh";
            // 
            // FolderTreeViewControl
            // 
            BackColor = SystemColors.Control;
            Controls.Add(_FolderTreeView);
            Controls.Add(_CurrentPathLayoutPanel);
            Controls.Add(_ToolStrip);
            Font = new Font("Segoe UI", 9.0f, FontStyle.Regular, GraphicsUnit.Point, 0);
            Name = "FolderTreeViewControl";
            Size = new Size(308, 439);
            _CurrentPathLayoutPanel.ResumeLayout(false);
            _CurrentPathLayoutPanel.PerformLayout();
            _ToolStrip.ResumeLayout(false);
            _ToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private Button _GoToDirectoryButton;
        private ToolTip _ToolTip;
        private ImageList _ImageList;
        private System.Windows.Forms.TextBox _PathTextBox;
        private TableLayoutPanel _CurrentPathLayoutPanel;
        private System.Windows.Forms.ToolStripButton _FolderUpButton;
        private FolderTreeView _FolderTreeView;
        private ToolStrip _ToolStrip;
        private System.Windows.Forms.ToolStripButton _HomeToolStripButton;
        private System.Windows.Forms.ToolStripButton _BackToolStripButton;
        private System.Windows.Forms.ToolStripButton _ForwardToolStripButton;
        private System.Windows.Forms.ToolStripButton _AddFolderButton;
        private System.Windows.Forms.ToolStripButton _RefreshToolStripButton;
    }
}
