using System.ComponentModel;

namespace isr.Core.WinControls
{
    [DefaultEvent("Click")]
    [Designer(typeof(WizardPageDesigner))]
    public partial class WizardPage
    {
    }
}
