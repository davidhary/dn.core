#region " Copyright ©2005, Cristi Potlog - All Rights Reserved"
// ------------------------------------------------------------------- *
// *                            Cristi Potlog                             *
// *                  Copyright ©2005 - All Rights reserved               *
// *                                                                      *
// * THIS SOURCE CODE IS PROVIDED "AS IS" WITH NO WARRANTIES OF ANY KIND, *
// * EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE        *
// * WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR   *
// * PURPOSE, NONINFRINGEMENT, OR ARISING FROM A COURSE OF DEALING,       *
// * USAGE OR TRADE PRACTICE.                                             *
// *                                                                      *
// * THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.             *
// * ------------------------------------------------------------------- 
#endregion

using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Core.WinControls
{

    public partial class Wizard : UserControl
    {

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    components?.Dispose();                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            _CancelButton = new Button();
            _NextButton = new Button();
            _BackButton = new Button();
            _HelpButton = new Button();
            SuspendLayout();
            // 
            // _CancelButton
            // 
            _CancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _CancelButton.DialogResult = DialogResult.Cancel;
            _CancelButton.FlatStyle = FlatStyle.System;
            _CancelButton.Location = new Point(344, 224);
            _CancelButton.Name = "_CancelButton";
            _CancelButton.TabIndex = 8;
            _CancelButton.Text = "Cancel";
            _CancelButton.Click += new EventHandler( CancelButtonClick );
            // 
            // _NextButton
            // 
            _NextButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _NextButton.FlatStyle = FlatStyle.System;
            _NextButton.Location = new Point(260, 224);
            _NextButton.Name = "_NextButton";
            _NextButton.TabIndex = 7;
            _NextButton.Text = "&Next >";
            _NextButton.Click += new EventHandler( NextButtonClick );
            // 
            // _BackButton
            // 
            _BackButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _BackButton.FlatStyle = FlatStyle.System;
            _BackButton.Location = new Point(184, 224);
            _BackButton.Name = "_BackButton";
            _BackButton.TabIndex = 6;
            _BackButton.Text = "< &Back";
            _BackButton.Click += new EventHandler( BackButtonClick );
            // 
            // _HelpButton
            // 
            _HelpButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            _HelpButton.FlatStyle = FlatStyle.System;
            _HelpButton.Location = new Point(8, 224);
            _HelpButton.Name = "_HelpButton";
            _HelpButton.TabIndex = 9;
            _HelpButton.Text = "&Help";
            _HelpButton.Visible = false;
            _HelpButton.Click += new EventHandler( HelpButtonClick );
            // 
            // Wizard
            // 
            Controls.Add(_HelpButton);
            Controls.Add(_CancelButton);
            Controls.Add(_NextButton);
            Controls.Add(_BackButton);
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Inherit;
            Font = new Font(SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            AutoSizeMode = AutoSizeMode.GrowAndShrink;
            Name = "Wizard";
            Size = new Size(428, 256);
            ResumeLayout(false);
        }

        private Button _CancelButton;
        private Button _NextButton;
        private Button _BackButton;
        private Button _HelpButton;
    }
}
