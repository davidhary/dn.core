using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace isr.Core.WinControls
{

    public partial class RichTextEditControl
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(RichTextEditControl));
            _RichTextBox = new WinControls.RichTextBox();
            _MainMenu = new System.Windows.Forms.MenuStrip();
            _FileMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FilePageSetupMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FilePrintPreviewMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FilePrintMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FileSeparatorMenuItem = new System.Windows.Forms.ToolStripSeparator();
            _FileExitMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontStyleMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatBoldMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatItalicMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatUnderlinedMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontArialMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontCourierMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontLucidaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontSegoeUIMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontTimesMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontSizeMenu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontSize8Menu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontSize10Menu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontSize12Menu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontSize18Menu = new System.Windows.Forms.ToolStripMenuItem();
            _FormatFontSize24Menu = new System.Windows.Forms.ToolStripMenuItem();
            _PrintDocument = new System.Drawing.Printing.PrintDocument();
            _PageSetupDialog = new System.Windows.Forms.PageSetupDialog();
            _PrintPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            _PrintDialog = new System.Windows.Forms.PrintDialog();
            _MainMenu.SuspendLayout();
            SuspendLayout();
            // 
            // _RichTextBox
            // 
            _RichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            _RichTextBox.Location = new Point(0, 24);
            _RichTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _RichTextBox.Name = "_RichTextBox";
            _RichTextBox.Size = new Size(560, 298);
            _RichTextBox.TabIndex = 0;
            _RichTextBox.Text = string.Empty;
            // 
            // _MainMenu
            // 
            _MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _FileMenu, _FormatMenu });
            _MainMenu.Location = new Point(0, 0);
            _MainMenu.Name = "_MainMenu";
            _MainMenu.Size = new Size(560, 24);
            _MainMenu.TabIndex = 0;
            _MainMenu.Text = "MenuStrip1";
            // 
            // _FileMenu
            // 
            _FileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _FilePageSetupMenu, _FilePrintPreviewMenu, _FilePrintMenu, _FileSeparatorMenuItem, _FileExitMenu });
            _FileMenu.Name = "_FileMenu";
            _FileMenu.Size = new Size(37, 20);
            _FileMenu.Text = "&File";
            // 
            // _FilePageSetupMenu
            // 
            _FilePageSetupMenu.Name = "_FilePageSetupMenu";
            _FilePageSetupMenu.Size = new Size(180, 22);
            _FilePageSetupMenu.Text = "Page &Setup...";
            _FilePageSetupMenu.Click += new EventHandler( FilePageSetupMenu_Click );
            // 
            // _FilePrintPreviewMenu
            // 
            _FilePrintPreviewMenu.Name = "_FilePrintPreviewMenu";
            _FilePrintPreviewMenu.Size = new Size(180, 22);
            _FilePrintPreviewMenu.Text = "Print Pre&view...";
            _FilePrintPreviewMenu.Click += new EventHandler( FilePrintPreviewMenu_Click );
            // 
            // _FilePrintMenu
            // 
            _FilePrintMenu.Name = "_FilePrintMenu";
            _FilePrintMenu.Size = new Size(180, 22);
            _FilePrintMenu.Text = "&Print...";
            _FilePrintMenu.Click += new EventHandler( FilePrintMenu_Click );
            // 
            // _FileSeparatorMenuItem
            // 
            _FileSeparatorMenuItem.Name = "_FileSeparatorMenuItem";
            _FileSeparatorMenuItem.Size = new Size(177, 6);
            // 
            // _FileExitMenu
            // 
            _FileExitMenu.Name = "_FileExitMenu";
            _FileExitMenu.Size = new Size(180, 22);
            _FileExitMenu.Text = "E&xit";
            _FileExitMenu.Click += new EventHandler( this.FileExitMenu_Click );
            // 
            // _FormatMenu
            // 
            _FormatMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _FormatFontStyleMenu, _FormatFontMenu, _FormatFontSizeMenu });
            _FormatMenu.Name = "_FormatMenu";
            _FormatMenu.Size = new Size(57, 20);
            _FormatMenu.Text = "F&ormat";
            // 
            // _FormatFontStyleMenu
            // 
            _FormatFontStyleMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _FormatBoldMenu, _FormatItalicMenu, _FormatUnderlinedMenu });
            _FormatFontStyleMenu.Name = "_FormatFontStyleMenu";
            _FormatFontStyleMenu.Size = new Size(180, 22);
            _FormatFontStyleMenu.Text = "Font St&yle";
            // 
            // _FormatBoldMenu
            // 
            _FormatBoldMenu.Name = "_FormatBoldMenu";
            _FormatBoldMenu.Size = new Size(132, 22);
            _FormatBoldMenu.Text = "&Bold";
            _FormatBoldMenu.Click += new EventHandler( FormatBoldMenu_Click );
            // 
            // _FormatItalicMenu
            // 
            _FormatItalicMenu.Name = "_FormatItalicMenu";
            _FormatItalicMenu.Size = new Size(132, 22);
            _FormatItalicMenu.Text = "&Italic";
            _FormatItalicMenu.Click += new EventHandler( FormatItalicMenu_Click );
            // 
            // _FormatUnderlinedMenu
            // 
            _FormatUnderlinedMenu.Name = "_FormatUnderlinedMenu";
            _FormatUnderlinedMenu.Size = new Size(132, 22);
            _FormatUnderlinedMenu.Text = "&Underlined";
            _FormatUnderlinedMenu.Click += new EventHandler( FormatUnderlinedMenu_Click );
            // 
            // _FormatFontMenu
            // 
            _FormatFontMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _FormatFontArialMenu, _FormatFontCourierMenu, _FormatFontLucidaMenuItem, _FormatFontSegoeUIMenu, _FormatFontTimesMenu });
            _FormatFontMenu.Name = "_FormatFontMenu";
            _FormatFontMenu.Size = new Size(180, 22);
            _FormatFontMenu.Text = "&Font";
            // 
            // _FormatFontArialMenu
            // 
            _FormatFontArialMenu.Name = "_FormatFontArialMenu";
            _FormatFontArialMenu.Size = new Size(174, 22);
            _FormatFontArialMenu.Text = "Arial";
            _FormatFontArialMenu.Click += new EventHandler( FormatFontArialMenu_Click );
            // 
            // _FormatFontCourierMenu
            // 
            _FormatFontCourierMenu.Name = "_FormatFontCourierMenu";
            _FormatFontCourierMenu.Size = new Size(174, 22);
            _FormatFontCourierMenu.Text = "Courier";
            _FormatFontCourierMenu.Click += new EventHandler( FormatFontCourierMenu_Click );
            // 
            // _FormatFontLucidaMenuItem
            // 
            _FormatFontLucidaMenuItem.Name = "_FormatFontLucidaMenuItem";
            _FormatFontLucidaMenuItem.Size = new Size(174, 22);
            _FormatFontLucidaMenuItem.Text = "Lucida Console";
            _FormatFontLucidaMenuItem.Click += new EventHandler( FormatFontLucidaMenuItem_Click );
            // 
            // _FormatFontSegoeUIMenu
            // 
            _FormatFontSegoeUIMenu.Name = "_FormatFontSegoeUIMenu";
            _FormatFontSegoeUIMenu.Size = new Size(174, 22);
            _FormatFontSegoeUIMenu.Text = "Segoe UI";
            _FormatFontSegoeUIMenu.Click += new EventHandler( FormatFontSegoeMenuItem_Click );
            // 
            // _FormatFontTimesMenu
            // 
            _FormatFontTimesMenu.Name = "_FormatFontTimesMenu";
            _FormatFontTimesMenu.Size = new Size(174, 22);
            _FormatFontTimesMenu.Text = "Times New Roman";
            _FormatFontTimesMenu.Click += new EventHandler( FormatFontTimesMenu_Click );
            // 
            // _FormatFontSizeMenu
            // 
            _FormatFontSizeMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _FormatFontSize8Menu, _FormatFontSize10Menu, _FormatFontSize12Menu, _FormatFontSize18Menu, _FormatFontSize24Menu });
            _FormatFontSizeMenu.Name = "_FormatFontSizeMenu";
            _FormatFontSizeMenu.Size = new Size(180, 22);
            _FormatFontSizeMenu.Text = "Font &Size";
            // 
            // _FormatFontSize8Menu
            // 
            _FormatFontSize8Menu.Name = "_FormatFontSize8Menu";
            _FormatFontSize8Menu.Size = new Size(86, 22);
            _FormatFontSize8Menu.Text = "8";
            _FormatFontSize8Menu.Click += new EventHandler( FormatFontSize8Menu_Click );
            // 
            // _FormatFontSize10Menu
            // 
            _FormatFontSize10Menu.Name = "_FormatFontSize10Menu";
            _FormatFontSize10Menu.Size = new Size(86, 22);
            _FormatFontSize10Menu.Text = "10";
            _FormatFontSize10Menu.Click += new EventHandler( FormatFontSize10Menu_Click );
            // 
            // _FormatFontSize12Menu
            // 
            _FormatFontSize12Menu.Name = "_FormatFontSize12Menu";
            _FormatFontSize12Menu.Size = new Size(86, 22);
            _FormatFontSize12Menu.Text = "12";
            _FormatFontSize12Menu.Click += new EventHandler( FontSize12Menu_Click );
            // 
            // _FormatFontSize18Menu
            // 
            _FormatFontSize18Menu.Name = "_FormatFontSize18Menu";
            _FormatFontSize18Menu.Size = new Size(86, 22);
            _FormatFontSize18Menu.Text = "18";
            _FormatFontSize18Menu.Click += new EventHandler( FormatFontSize18Menu_Click );
            // 
            // _FormatFontSize24Menu
            // 
            _FormatFontSize24Menu.Name = "_FormatFontSize24Menu";
            _FormatFontSize24Menu.Size = new Size(86, 22);
            _FormatFontSize24Menu.Text = "24";
            _FormatFontSize24Menu.Click += new EventHandler( FormatFontSize24Menu_Click );
            // 
            // _PrintDocument
            // 
            _PrintDocument.BeginPrint += new System.Drawing.Printing.PrintEventHandler( PrintDocument_BeginPrint );
            _PrintDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler( PrintDocument_PrintPage );
            _PrintDocument.EndPrint += new System.Drawing.Printing.PrintEventHandler( PrintDocument_EndPrint );
            // 
            // _PageSetupDialog
            // 
            _PageSetupDialog.Document = _PrintDocument;
            // 
            // _PrintPreviewDialog
            // 
            _PrintPreviewDialog.AutoScrollMargin = new Size(0, 0);
            _PrintPreviewDialog.AutoScrollMinSize = new Size(0, 0);
            _PrintPreviewDialog.ClientSize = new Size(400, 300);
            _PrintPreviewDialog.Document = _PrintDocument;
            _PrintPreviewDialog.Enabled = true;
            _PrintPreviewDialog.Icon = (Icon)resources.GetObject("_PrintPreviewDialog.Icon");
            _PrintPreviewDialog.Name = "_PrintPreviewDialog";
            _PrintPreviewDialog.Visible = false;
            // 
            // _PrintDialog
            // 
            _PrintDialog.Document = _PrintDocument;
            // 
            // RichTextEditControl
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_RichTextBox);
            Controls.Add(_MainMenu);
            Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            Name = "RichTextEditControl";
            Size = new Size(560, 322);
            _MainMenu.ResumeLayout(false);
            _MainMenu.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private WinControls.RichTextBox _RichTextBox;
        private System.Drawing.Printing.PrintDocument _PrintDocument;
        private System.Windows.Forms.PageSetupDialog _PageSetupDialog;
        private System.Windows.Forms.PrintPreviewDialog _PrintPreviewDialog;
        private System.Windows.Forms.PrintDialog _PrintDialog;
        private System.Windows.Forms.MenuStrip _MainMenu;
        private System.Windows.Forms.ToolStripMenuItem _FileMenu;
        private System.Windows.Forms.ToolStripMenuItem _FilePageSetupMenu;
        private System.Windows.Forms.ToolStripMenuItem _FilePrintPreviewMenu;
        private System.Windows.Forms.ToolStripMenuItem _FilePrintMenu;
        private System.Windows.Forms.ToolStripMenuItem _FileExitMenu;
        private System.Windows.Forms.ToolStripSeparator _FileSeparatorMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _FormatMenu;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontStyleMenu;
        private System.Windows.Forms.ToolStripMenuItem _FormatBoldMenu;
        private System.Windows.Forms.ToolStripMenuItem _FormatItalicMenu;
        private System.Windows.Forms.ToolStripMenuItem _FormatUnderlinedMenu;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontSizeMenu;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontSize8Menu;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontSize10Menu;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontSize12Menu;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontSize18Menu;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontSize24Menu;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontMenu;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontArialMenu;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontCourierMenu;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontLucidaMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontSegoeUIMenu;
        private System.Windows.Forms.ToolStripMenuItem _FormatFontTimesMenu;
    }
}
