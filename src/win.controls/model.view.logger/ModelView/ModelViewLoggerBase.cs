using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.TraceLog;

using Microsoft.Extensions.Logging;

namespace isr.Core.WinControls
{

    /// <summary>
    /// A user control base. Supports property change notification and logger.
    /// </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-26, 2.1.5836. </para>
    /// </remarks>
    public partial class ModelViewLoggerBase : ModelViewBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// A protected constructor for this class making it not publicly creatable. This ensure using
        /// the class as a base class.
        /// </summary>
        /// <remarks>   David, 2020-09-24. </remarks>
        protected ModelViewLoggerBase() : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.InitializingComponents = false;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-09-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #region " Windows Form Designer generated code "

        // Required by the Windows Form Designer
#pragma warning disable IDE1006 // Naming Styles
        private IContainer components;
#pragma warning restore IDE1006 // Naming Styles

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ModelViewLoggerBase
            // 
            this.AutoScaleDimensions = new SizeF( 7.0f, 17.0f );
            this.AutoScaleMode = AutoScaleMode.Inherit;
            this.Font = new Font( SystemFonts.MessageBoxFont.FontFamily, 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0 );
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.Name = "ModelViewLoggerBase";
            this.Size = new Size( 175, 173 );
            this.ResumeLayout( false );
        }

        #endregion

        #endregion

        #region " TEXT  WRITER "

        /// <summary>   Adds a display trace event writer. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="textWriter">   The trace Event tWriter. </param>
        public void AddDisplayTextWriter( Core.Tracing.ITraceEventWriter textWriter )
        {
            textWriter.TraceLevel = this.DisplayTraceEventType;
            Core.Tracing.TracingPlatform.Instance.AddTraceEventWriter( textWriter );
        }

        /// <summary>   Removes the display text writer described by <paramref name="textWriter"/>. </summary>
        /// <remarks>   David, 2021-02-23. </remarks>
        /// <param name="textWriter">   The trace Event tWriter. </param>
        public void RemoveDisplayTextWriter( Core.Tracing.ITraceEventWriter textWriter )
        {
            Core.Tracing.TracingPlatform.Instance.RemoveTraceEventWriter( textWriter );
        }

        #endregion

#region " DISPLAY LOG LEVEL "
#if false
        // log level does not map to trace event types, which filter the display.

        private KeyValuePair<LogLevel, string> _DisplayLogLevelValueNamePair;
        /// <summary>
        /// Gets or sets the <see cref="Microsoft.Extensions.Logging.LogLevel"/> value name pair for display.
        /// </summary>
        /// <value> The log level value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<LogLevel, string> DisplayLogLevelValueNamePair
        {
            get => this._DisplayLogLevelValueNamePair;

            set {
                if ( !KeyValuePair<LogLevel, string>.Equals( value, this.DisplayLogLevelValueNamePair ) )
                {
                    this._DisplayLogLevelValueNamePair = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private LogLevel _DisplayLogLevel;
        /// <summary>
        /// Gets or sets the <see cref="Microsoft.Extensions.Logging.LogLevel"/> for display.
        /// </summary>
        /// <value> The trace event writer log level. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LogLevel DisplayLogLevel
        {
            get => this._DisplayLogLevel;

            set {
                if ( value != this.DisplayLogLevel )
                {
                    this._DisplayLogLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }
#endif
#endregion

#region " DISPLAY TRACE EVENT TYPE "

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> value name pair for the global trace event
        /// writer. This level determines the level of all the
        /// <see cref="isr.Core.Tracing.ITraceEventWriter"/>s Trace Listeners. Each trace listener can still listen at a
        /// lower level.
        /// </summary>
        /// <value> The trace event writer trace event value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<TraceEventType, string> TraceEventWriterTraceEventValueNamePair
        {
            get => TraceLogger.Instance.TraceEventWriterTraceEventValueNamePair;

            set {
                if ( !KeyValuePair<TraceEventType, string>.Equals( value, this.TraceEventWriterTraceEventValueNamePair ) )
                {
                    TraceLogger.Instance.TraceEventWriterTraceEventValueNamePair = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> for the global trace event writer. This level
        /// determines the level of all the
        /// <see cref="isr.Core.Tracing.ITraceEventWriter"/>s Trace Listeners. Each trace listener can still listen at a
        /// lower level.
        /// </summary>
        /// <value> The type of the trace event writer trace event. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType TraceEventWriterTraceEventType
        {
            get => TraceLogger.Instance.TraceEventWriterTraceEventType;

            set {
                if ( value != this.TraceEventWriterTraceEventType )
                {
                    TraceLogger.Instance.TraceEventWriterTraceEventType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private KeyValuePair<TraceEventType, string> _DisplayTraceEventTypeValueNamePair;
        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> value name pair for display.
        /// </summary>
        /// <value> The log level value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<TraceEventType, string> DisplayTraceEventTypeValueNamePair
        {
            get => this._DisplayTraceEventTypeValueNamePair;

            set {
                if ( !KeyValuePair<TraceEventType, string>.Equals( value, this.DisplayTraceEventTypeValueNamePair ) )
                {
                    this._DisplayTraceEventTypeValueNamePair = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private TraceEventType _DisplayTraceEventType;
        /// <summary>
        /// Gets or sets the <see cref="TraceEventType"/> for display.
        /// </summary>
        /// <value> The trace event writer log level. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TraceEventType DisplayTraceEventType
        {
            get => this._DisplayTraceEventType;

            set {
                if ( value != this.DisplayTraceEventType )
                {
                    this._DisplayTraceEventType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " LOGGING LOG LEVEL "

        /// <summary>
        /// Gets or sets the <see cref="Microsoft.Extensions.Logging.LogLevel"/> value name pair for logging. This level
        /// determines the level of the <see cref="ILogger"/>.
        /// </summary>
        /// <value> The log trace event value name pair. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public KeyValuePair<LogLevel, string> LoggingLevelValueNamePair
        {
            get => TraceLogger.Instance.LoggingLevelValueNamePair;

            set {
                if ( !KeyValuePair<LogLevel, string>.Equals( value, this.LoggingLevelValueNamePair ) )
                {
                    TraceLogger.Instance.LoggingLevelValueNamePair = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets or sets <see cref="Microsoft.Extensions.Logging.LogLevel"/> value for logging. </summary>
        /// <value> The trace event type for logging. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LogLevel LoggingLogLevel
        {
            get => TraceLogger.Instance.MinimumLogLevel;

            set {
                if ( value != this.LoggingLogLevel )
                {
                    TraceLogger.Instance.MinimumLogLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

#endregion

#region " LOG EXCEPTION "

        /// <summary>   Adds an exception data. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="ex">   The exception. </param>
        /// <returns>   True if exception data was added for this exception; otherwise, false. </returns>
        protected virtual bool AddExceptionData( Exception ex )
        {
            return false;
        }

        /// <summary>   Log exception. </summary>
        /// <remarks>
        /// Declared as must override so that the calling method could add exception data before
        /// recording this exception.
        /// </remarks>
        /// <param name="ex">               The ex. </param>
        /// <param name="activity">         The activity. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
        /// <returns>   A String. </returns>
        protected string LogError( Exception ex, string activity, [CallerMemberName] string memberName = "",
                                                                  [CallerFilePath] string sourceFilePath = "",
                                                                  [CallerLineNumber] int sourceLineNumber = 0 )
        {
            _ = this.AddExceptionData( ex );
            return TraceLog.TraceLogger.LogError( ex, activity, memberName, sourceFilePath, sourceLineNumber );
        }

#endregion

    }
}
