# About

isr.Core.WinControls.Binding is a .Net library providing biding extensions for Windows Forms controls.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Core.WinControls.Binding is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Core Framework Repository].

[Core Framework Repository]: https://bitbucket.org/davidhary/dn.core

