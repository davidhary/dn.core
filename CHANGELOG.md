# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [7.1.8189] - 2022-06-03
* Use Tuples to implement GetHashCode().

## [7.1.8119] - 2022-03-25
* Use the new Json application settings base class.

## [7.1.8109] - 2022-03-15
* Use the ?. operator (directly, without making a copy of the delegate) 
to check if a delegate is non-null and invoke it in a thread-safe way.

## [7.1.8102] - 2022-03-07
* Repackage logging and trace log.

## [7.1.8099] - 2022-03-05
* Repackage.

## [7.1.8089] - 2022-02-26
* Fast Enum: add .NET 4.72 and Core 5 and 6 targets.

## [7.1.8076] - 2022-02-09
* Trace Log: Add Log Event.

## [7.1.8069] - 2022-02-03
* Adds read write lock simple class.
* Remove unused references.

## [7.1.8067] - 2022-02-01
* Targeting Visual Studio 2022, C# 10 and .NET 6.0. 
* Dictionaries: Add name value collection. 

## [7.1.7987] - 2021-11-13
* Update time span extension methods. 

## [7.1.7972] - 2021-10-29
* Adds Wait For class. 
* Remove binding from extended tool strips in favor of biding to the internal control.

## [7.0.7911] - 2021-08-28
* Logging and tracing: allow singletons to create if disposed. 
* Json: add file scope and save settings to application data folder

## [7.0.7830] - 2021-06-09
* Adds .NET 4.72 targets.

## [7.0.7704] - 2021-02-02
* Conversion of isr.Core to .NET 5.0.

(C) 2012 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
[7.1.8119]: https://bitbucket.org/davidhary/dn.core/src/main/
[7.0.7704]: https://bitbucket.org/davidhary/dn.core/commits/04bfeaebdeefa1f02bc7843a311566f538d1e310
